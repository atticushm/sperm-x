%% SPX MAIN

% Code by Atticus L. Hall-McNair, 2021.
% Control options for Sperm-X (SPX).

% Running this file will perform a single simulation with parameters and
% options defined below.

close all;
addFilesToPath
clc

%% setup

% choosing problem defines some presets
problem = 'SwimmingCell';

% define modelling options
options = setOptions(problem, ...
    'Species',      'human', ...        % 'mouse' type not fully implemented!
    'BodyType',     'sunanda', ...      % see setOptions for options (!)
    'NonLocal',     true, ...           % toggle nonlocal hydrodynamics
    'MechTwist',    true, ...           % toggle mechanical twist
    'StiffModel',   'varying', ...      % stiffness model
    'ActModel',     'trig', ...        % actuation model
    'EndPieceModel','heaviside', ...    % endpiece model
    'ForcePlanar',  false, ...          % toggle planar forcing
    'RunProfiler',  false, ...          % toggle MATLAB performance profiler
    'RunDebug',     false, ...          % toggle debug functions
    'SilentMode',   false, ...          % toggle silent mode (reduced cmd output)
    'ProcType',     'cpu', ...          % matrix build location
    'UseBlakelets', true, ...           % toggle blakelet image system
    'PlaneOfBeat',  'xz', ...           % set plane of beat
    'MaxIt',        25, ...             % set max allowed nonlinear iterations
    'SaveOnFail',   false, ...          % toggle workspace saving on failure
    'SaveVideo',    true, ...           % toggle video generation
    'DrawFrame',    false ...           % toggle to draw director frame in video
    );

% define modelling parameters
params = setParameters(options, ...
    'ittol',        1e-4, ...           % tolerance of iterative solver
    'head_ax',      [1;1;1]/50, ...     % only used if BodyType='custom'
    ...
    'NF',           201, ...            % number of flagellum nodes
    'Ht',           4, ...              % body traction discr parameter
    'Hq',           8, ...              % body quadrature discr parameter
    'dt',           2*pi/100, ...       % time step
    'q',            0.1, ...            % local region size
    ...
    'calS',         12, ...             % swimming parameter
    'calM1',        0.00*1e-2, ...      % actuation parameters
    'calM2',        1.50*1e-2, ...      %      planar bend moment 
    'calM3',        1.50*1e-4, ...      %      twist moment
    'Gam_s',        4, ...              % twist/bend stiffness ratio    
    'Gam_d',        10^4, ...           % bend/twist drag ratio                                 
    ...
    'k_bend',       4*pi, ...           % wavenumber of bend moment
    'k_twist',      4*pi, ...           % wavenumber of twist moment
    'om_bend',      1, ...              % time scale for bend moment
    'om_twist',     1, ...              % time scale for twist moment
    ...
    's_crit',       0.5, ...            % threshold value for varying stiffness
    'ell',          0.05, ...           % length of end piece
    'epsilon',      0.01, ...           % regularisation parameter
    'lambda',       2e2, ...            % damping parameter
    ...
    'h0',           0.2, ...            % height above backstep
    'h',            0.0, ...            % height of backstep
    ...
    'NumBeats',     20, ...             % number of beats to simulate
    'WarmUp',       1, ...              % number of beats to warm up actuation                                                                      
    'tmax',         'auto', ...         % max time
    'NumSlices',    20, ...             % number of slices per beats
    'NumSave',      'auto' ...          % approx number of timesteps to save
    );    

% set cell initial condition
int = setInitialCondition(options, params, ...
    'IntCond',      'line', ...         % see function for options
    'Phi',          pi/4, ...           % initial cell rotation in plane of beat
    'X0',           [0;0;0] ...         % initial X0 location
    );  

%% run simulation

outs = runSPX(int, params, options);                               
            
% END OF SCRIPT

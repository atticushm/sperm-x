function humanCellsOverWall(flowType)

% simulate a human sperm swimming over a flat boundary using regularised
% stokeslets and blakelets

% flowType should be 'stokeslets' or 'blakelets' and determines which
% kernel is used in the hydrodynamic model

close all;

% check in correct directory (bluebear call will start in subdir, so we 
% need to move up)
dir = pwd;
if strcmpi(dir(end-17:end),'cell_over_boundary')
    cd ..
end
addFilesToPath;

% setup folders
if ~exist('./cell_over_boundary/human_wall_data/','dir')
    mkdir('./cell_over_boundary/human_wall_data/')
end

% define head axes (via Sunanda et al 2018)
head_ax = [4.6; 1.03; 2.6]./2/50;

% starting point
head_X0 = zeros(3,1);

% choose swimming parameter value
calS = 14;

% choose actuation parameter value
calM2 = 0.015;

% initial height of cell over boundary
h0 = 0.2;

% flat boundary, so height of backstep is zero
h = 0;

% number of beats
num_beats = 100;

% determine procType to use
procType = 'cpu';

% plane of beat -- xy is perpendicular to wall, xz parallel to wall
plane = 'xz';

% save video?
save_vid = false;

%% run simulations 

% cell over wall w/ stokeslets/blakelets
tic;
[outs,options,params]  = cellOverBoundary('human', calS, calM2, h0, h,      ...
    plane, procType, flowType, head_ax, head_X0, num_beats, save_vid);
simtime = toc;

% record walltime
params.simtime = simtime;

% save data
save_str = sprintf("./cell_over_boundary/human_wall_data/human_ax=[%.3f,%.3f,%.3f]_h=%.2g_h0=%.2g_S=%g_M2=%g_%s_%s.mat", ...
    head_ax(1),head_ax(2),head_ax(3), h, h0, calS, calM2, plane, flowType);
parsave(save_str,outs,options,params);

end % function

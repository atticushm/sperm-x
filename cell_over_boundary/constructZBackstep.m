function X = constructZBackstep(h, L, options, varargin)

% constructs a rigid surface, initially distance h0 below a swimmer. 
% after distance dx, the surface drops by height dh in the z-direction.
% the total wall is width W and length L

% the backstep here is generated assuming the swimmer travels towards the
% negative x axis

% backstep is such that the lower level is the plane z=0

% discretisation parameters
if isempty(varargin)
    x_step = 0.05;
    y_step = 0.05;
    z_step = 0.05;
else
    x_step = varargin{1};
    y_step = x_step;
    z_step = x_step;
end

% 'heights' of each level
z_0 = 0;
z_1 = h;

% start of bottom level
x_0 = -4;

% cliff x position
x_1 = -0.5;

% plateau end position
x_2 =  2;

% y coordinates
y_min = -1;
y_max =  1;
dy = y_min : y_step : y_max;
% dy = linspace(y_min, y_max, 2000);

% 'base' nodes in the plane z=0
if options.UseBlakelets
    % not needed due to reg blakelet image system, but generate a couple of
    % nodes for plotting purposes
    [x_base,y_base,z_base] = meshgrid([x_0,x_2],[dy(1),dy(end)],z_0);
else
    dx_base = x_0 : x_step : x_1;
    % dx_base = linspace(x_0, x_1, 2000);
    [x_base,y_base,z_base] = meshgrid(dx_base,dy,z_0);
end

% 'cliff' nodes in the plane x=x_1
if options.UseBlakelets && (h==0)
    x_cliff=[]; y_cliff=[]; z_cliff=[];
else
    dz_cliff = z_0 : z_step : z_1;
    % dz_cliff = linspace(z_0, z_1, 2000);
    [x_cliff,y_cliff,z_cliff] = meshgrid(x_1,dy,dz_cliff);
end

% 'plateau' nodes in the plane z=h*L=y_1
if options.UseBlakelets && (h==0)
    x_plat=[]; y_plat=[]; z_plat=[];
else
    dx_plat = x_1 : x_step : x_2;
    % dx_plat = linspace(x_1, x_2, 2000);
    [x_plat,y_plat,z_plat] = meshgrid(dx_plat,dy,z_1);
end

% collate points and remove duplicates
X_mat = [x_plat(:)', x_cliff(:)', x_base(:)'; ...
         y_plat(:)', y_cliff(:)', y_base(:)'; ...
         z_plat(:)', z_cliff(:)', z_base(:)'];

X_unq = unique(X_mat','rows')';
X     = vec(X_unq);

end % function
function plotCellOverWall()

close all;

% paths to .mat files
ax = [0.046,0.010,0.026];
calS = 14;
calM = 0.015;
h0 = 0.2;
path_stkts = sprintf('./cell_over_boundary/human_wall_data/human_ax=[%.3f,%.3f,%.3f]_h=0_h0=%g_S=%g_M2=%g_xz_stokeslets.mat', ...
    ax(1), ax(2), ax(3), h0, calS, calM);
path_blkts = sprintf('./cell_over_boundary/human_wall_data/human_ax=[%.3f,%.3f,%.3f]_h=0_h0=%g_S=%g_M2=%g_xz_blakelets.mat', ...
    ax(1), ax(2), ax(3), h0, calS, calM);

% compute trajectories
[traj_s, smth_s] = loadCellTrajectory(path_stkts);
[traj_b, smth_b] = loadCellTrajectory(path_blkts);

% plot trajectories
x = linspace(-4,0);
z = 0*x;

figure(1); hold on; box on;
plot(traj_s(1,:), traj_s(3,:),'LineWidth',1.5);
% plot(smth_s(1,:), smth_s(3,:),'m', 'LineWidth', 1.5);

plot(traj_b(1,:), traj_b(3,:),'LineWidth',1.5);
% plot(smth_b(1,:), smth_s(3,:),'m--','LineWidth',1.5);

plot(x,z,'k','LineWidth',3);

xlabel('$x$', 'FontSize',12, 'Interpreter','latex');
ylabel('$z$', 'FontSize',12, 'Interpreter','latex');

axis equal
axis([-4,0,-0.1,0.5]);

legend('reg. stokelsets','reg. blakelets', 'FontSize',12, 'Location','southeast', 'Interpreter','latex');

% scale figures
figure(1); set(gcf,'Position',[588 971 1154 306])

% save figure
figure(1); save2pdf('./cell_over_boundary/figures/trajectory_comparison.pdf')

end % function
function humanCellsOverBackstep(method, i, itot)

% batch simulate different human sperm cells swimming over backsteps of
% different sizes.

% the total number of simulations is split into ITOT batches. This function
% runs the iTH batch.

% check in correct directory (bluebear call will start in subdir, so we 
% need to move up)
dir = pwd;
if strcmpi(dir(end-17:end),'cell_over_boundary')
    cd ../..
end
addFilesToPath;

% setup folders
if ~exist('./cell_over_boundary/backstep_data/','dir')
    mkdir('./cell_over_boundary/backstep_data/')
end

%% setup SPX

% generate cell heads in same manner to eif head morphology study
switch method
    case 'fixed'
        % generate heads with fixed 'ideal' volume
        num_per_dir = 10;
        num_per_case = num_per_dir^2;
        num_case = 3;
        num_heads = num_case * num_per_case;
        [~, X0, ax, ~] = generateEllipsoidalHeads(num_heads, 4);
        
    case 'scaled'
        % generate heads from scaling volume (includes abnormal heads)
        [~, X0, ax, ~, ~] = generateScaledHeads(5, false);           
        
    otherwise
        warning('head generation type not specified!')
        return
end

% initial height of cell over boundary
h0 = 0.2;

% backstep values
h_vals = linspace(0,0.5,6);
num_h  = length(h_vals);

% choose swimming parameter value
calS = 14;

% choose actuation parameter value
calM2 = 0.015;

% determine procType to use
procType = 'cpu';

% determine flow model to use
flowType = 'blakelets';

% plane in which to beat
plane = 'xz';

% number of beats to simulate
num_beats = 100;

%% split into itot batches

% combine all variables 
par_combs = combvec(h_vals, [ax{:}]);
num_combs = size(par_combs, 2);         % total number of simulations

% combs varies h first, i.e. each ax is repeated num_h times before the
% next ax appears, etc. so, to match the correct X0 to each ax, we can kron
X0_combs = kron([X0{:}],ones(1,num_h));

% determine number of simulations per block
sims_per_block = ceil(num_combs/itot);

%% run simulations

% set limits of loop, depending on which block has been called
start = (i-1)*sims_per_block+1;
if i < itot
    fin = i*sims_per_block;
else
    fin = num_combs;        % in case num_combs doesn't divide cleanly by itot
end

% loop
parfor j = start:fin
    
    % set parameters
    hj      = par_combs(1,j);
    head_ax = par_combs(2:4,j);
    head_X0 = X0_combs(:,j);
    
    % solve problem
    [outs,params,options] = cellOverBoundary('human', calS, calM2, h0, hj,  ...
        plane, procType, flowType, head_ax, head_X0, num_beats, false);
    
    % save data
    save_str = sprintf('./cell_over_boundary/backstep_data/%s_%g_of_%g.mat', ...
        method, j, itot);
    parsave(save_str, outs, params, options);

end

end % function
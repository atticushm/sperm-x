function [outs,params,options] = cellOverBoundary(species, calS, calM2, h0, ...
    h, plane, procType, flowType, head_ax, head_X0, num_beats, save_vid)

% runs simulations for a cell swimming over a boundary of varying backstep
% size

% determine flowType
switch flowType
    case 'stokeslets'
        flowBool = false;
    case 'blakelets'
        flowBool = true;
end

% set model options
options = setOptions('CellOverBackstep', ...
    'Species',          species, ...                % human or mouse sperm
    'BodyType',         'custom', ...               % body type flag
    'NonLocal',         true, ...                   % include nonlocal hydrodynamics
    'StiffFunc',        'varying', ...              % constant or varying stiffness
    'MechTwist',        false, ...                  % enable mechanical twist terms
    'RunProfiler',      false, ...                  % enable matlab profiler
    'ProcType',         procType, ...               % use cpu or gpu for arrays
    'UseBlakelets',     flowBool, ...               % use blakelets or stokeslets
    'PlaneOfBeat',      plane, ...                  % plane of beating
    'SaveVideo',        save_vid ...                % save video?
    );

% set model parameters
params = setParameters(options, ...
    'ittol',            1e-4, ...                   % tolerance of iterative solver
    'head_ax',          head_ax, ...                % axes of cell head
    'NF',               201, ...                    % # of flagellum nodes
    'Ht',               5, ...
    'Hq',               10, ...
    'dt',               1e-1, ...                   % time step
    'calS',             calS, ...                   % swimming parameter
    'calM2',            calM2, ...                  % actuation parameter
    'ell',              0.03, ...                   % size of end piece
    'epsilon',          0.01, ...                   % regularisation parameter
    'lambda',           2e2, ...                    % damping parameter for tension
    'h0',               h0, ...                     % initial height over boundary
    'h',                h, ...                      % height of wall backstep
    'NumBeats',         num_beats, ...              % number of beats to simulate
    'tmax',             'auto', ...                 % max time from NumBeats
    'NumSave',          1e3 ...                     % number of timesteps to save
    );    

% set initial condition
intCond = setInitialCondition(options,params, ...
    'IntCond',          'line', ...
    'X0',               head_X0 ...
    );  

% run simulation 
outs = runSPX(intCond, params, options);

end % function
function generateShFilesCellOverBoundary(method, itot)

% generates multiple sh files for running helical beat parameter search
% simulations

% make subfolder for .sh files
if ~exist(sprintf('./cell_over_boundary/sh/%s',method),'dir')
    mkdir(sprintf('./cell_over_boundary/sh/%s',method))
end

% generate .sh filenames
for i = 1:itot
    fname{i} = sprintf('./cell_over_boundary/sh/%s/bbBoundary_%g_of_%g.sh', ...
        method, i, itot);
end

% write to each file
for i = 1:itot
   
    % open file for writing
    fID = fopen(fname{i},'w+');
    
    % print lines to files
    fprintf(fID, '#!/bin/bash\n');
    fprintf(fID, '#SBATCH --ntasks 10\n');
    fprintf(fID, '#SBATCH --time 7-00:00:0\n');
    fprintf(fID, '#SBATCH --qos bbdefault\n');
    fprintf(fID, '#SBATCH --mail-type NONE\n');
    fprintf(fID, '#SBATCH --mem 100G\n\n');
    
    fprintf(fID, 'set -e\n\n');
    fprintf(fID, 'module purge; module load bluebear\n');
    fprintf(fID, 'module load MATLAB/2020a\n\n');
    
    % cd to code on bluebear directory and run
    fprintf(fID, 'cd ~/02_SPERMX/git/sperm-x/cell_over_boundary/\n');
    fprintf(fID, 'matlab -nodisplay -r "humanCellsOverBackstep(%s,%g,%g)"\n', method, i, itot);
    
    % close file
    fclose(fID);
end

end % function
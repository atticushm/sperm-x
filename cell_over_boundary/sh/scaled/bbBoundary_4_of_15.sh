#!/bin/bash
#SBATCH --ntasks 10
#SBATCH --time 7-00:00:0
#SBATCH --qos bbdefault
#SBATCH --mail-type NONE
#SBATCH --mem 100G

set -e

module purge; module load bluebear
module load MATLAB/2020a

cd ~/02_SPERMX/git/sperm-x/cell_over_boundary/
matlab -nodisplay -r "humanCellsOverBackstep(scaled,4,15)"

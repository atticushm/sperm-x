function opt = setOptions(problem,varargin)

% Sets modelling options given a specified "problem" input.

% expected results for parameters with options
exp_localtype = {'rss','lgl'};                      % reg. stokeslet segments or L.G-L.
exp_stiffness = {'constant','varying'};             % constant stiffness or varying stiffness
exp_problem   = {'RelaxingFilament','RelaxingCell',...
                 'SwimmingFilament','SwimmingCell',...
                 'CellOverBackstep','CellOverWall'};
exp_celltype  = {'human','mouse'};
exp_proctype  = {'cpu','gpu'};                      % build stokeslets on cpu or gpu
exp_plane     = {'xy','xz'};                        % defines plane of planar beat
exp_epm       = {'heaviside','tanh','csaps'};       % model for passive end piece    

human_types = { ...
    'cw1','cw2','cw3','gskb1','gskb2','gskb3','tdmj','sph1','sph2','sph3',  ...
    'prl1','prl2','prl3', 'sunanda', 'custom' ...
    };
mouse_types = { ...
    'BALBc','C57Bl6','CBA','CD1','DBA','FVB','MF1YRIII','Mmdomesticus',     ...
    'Mmmusculus','Mspretus' ...
    };
exp_bodytype = horzcat('none',human_types, mouse_types);

% set default options
default_localtype   = 'rss';           % model for the local region coefficients
default_nonlocal    = true;            % toggle for nonlocal hydrodynamics
default_prof        = false;           % toggle for profiling mode
default_video       = false;           % toggle for making video
default_species     = 'human';         % model human or mouse sperm
default_proctype    = 'cpu';           % build arrays on cpu or gpu
default_plane       = 'xy';            % plane of beating
default_beattype    = 'planar';        % actuation beat type
default_actmodel    = 'trig';          % actuation model
default_silent      = false;           % silent mode toggle
default_mechtwist   = true;            % model mechanical twist of flagellum
default_saveonfail  = false;           % save solutions on fail
default_debug       = false;           % enable debug checks
default_epm         = 'heaviside';     % model for passive end piece
default_maxit       = 20;              % maximum number of iterations permitted
default_drawframe   = true;            % draw director frame in videos

% check problem is valid before parsing later on
any(validatestring(problem,exp_problem));
           
% define parseable inputs
p = inputParser;
addRequired(p, 'problem',                           @(x) any(validatestring(x,exp_problem)));
addParameter(p,'NonLocal',      default_nonlocal,   @islogical);
addParameter(p,'LocalType',     default_localtype,  @(x) any(validatestring(x,exp_localtype)));
addParameter(p,'RunProfiler',   default_prof,       @islogical);
addParameter(p,'SaveVideo',     default_video,      @islogical);
addParameter(p,'Species',       default_species,    @(x) any(validatestring(x,exp_celltype)));
addParameter(p,'ProcType',      default_proctype,   @(x) any(validatestring(x,exp_proctype)));
addParameter(p,'PlaneOfBeat',   default_plane,      @(x) any(validatestring(x,exp_plane)));
addParameter(p,'ActModel',      default_actmodel)
addParameter(p,'BeatType',      default_beattype);
addParameter(p,'SilentMode',    default_silent,     @islogical);
addParameter(p,'MechTwist',     default_mechtwist,  @islogical);
addParameter(p,'SaveOnFail',    default_saveonfail, @islogical);
addParameter(p,'RunDebug',      default_debug,      @islogical);
addParameter(p,'EndPieceModel', default_epm,        @(x) any(validatestring(x,exp_epm)));
addParameter(p,'MaxIt',         default_maxit,      @isnumeric);
addParameter(p,'DrawFrame',     default_drawframe,  @islogical);

% other options are problem dependent, so
switch problem
    case 'RelaxingFilament'
        default_bodytype = 'none';          % model for cell head
        default_stiffness = 'constant';     % model for flagellar stiffness
        default_forceplanar = false;        % force planarity?
        default_backstep = false;           % toggle for including backstep
        default_useblakelets = false;       % toggle for using blakelets
    case 'SwimmingFilament'
        default_bodytype = 'none';
        default_stiffness = 'varying';
        default_forceplanar = false;
        default_backstep = false;
        default_useblakelets = false;
    case 'RelaxingCell'
        default_bodytype = 'gskb1';
        default_stiffness = 'constant';
        default_forceplanar = false;
        default_backstep = false;
        default_useblakelets = false;
    case 'SwimmingCell'
        default_bodytype = 'gskb1';
        default_stiffness = 'varying';
        default_forceplanar = false;
        default_backstep = false;
        default_useblakelets = false;
    case {'CellOverBackstep','CellOverWall'}
        default_bodytype = 'gskb1';
        default_stiffness = 'varying';
        default_forceplanar = false;
        default_backstep = true;
        default_useblakelets = true;
end
addParameter(p,'UseBlakelets',  default_useblakelets);
addParameter(p,'BodyType',      default_bodytype,       @(x) any(validatestring(x,exp_bodytype)));
addParameter(p,'ForcePlanar',   default_forceplanar,    @islogical);
addParameter(p,'StiffModel',    default_stiffness,      @(x) any(validatestring(x,exp_stiffness)));
addParameter(p,'Backstep',      default_backstep,       @islogical);

% parse inputs
parse(p, problem, varargin{:});
opt = p.Results;

end % function
function plotTwistBend()

% plots all figures relating to the "twist and bend" section of my thesis.

printLinebreak
close all

%% setup directories

if ~exist('./spx_twist_bend/figures/infinite fluid','dir')
    mkdir('./spx_twist_bend/figures/infinite fluid')
end

%% load data
% preprocessed in preprocTwistBendData.m
% variables loaded are sorted by simulation number, so require additional
% sorting on a per plot basis depending what we want to plot
%   - X_vals:       flagellum coordinates
%   - y_vals:       head coordinates (traction discretisation)
%   - t_vals:       time point values
%   - vars:         identifies variables changed in each simulation
%   - fail_ids:     indices of simulations in vars that failed
%   - params:       contains constant parameters for SPX
%   - options:      contains constant options for SPX
%   - int:          contains model initial conditions
%   - VAL:          velocity along a line, for each beat simulated
%   - eff:          Lighthill efficiency, for each beat simulated
%   - dNP:          range of 'nonplanar' movement

% in vars, the variables are ordered as follows
%   1 - calM3
%   2 - Gamma_d
%   3 - rho (called om in spxTwistBendParameterSweep.m)
%   4 - k (wave number)

fprintf('Loading data from ./spx_twist_bend/preproc/paramSweep_calS=12.mat... ')
load('./spx_twist_bend/preproc/paramSweep_calS=12.mat')
fprintf('complete!\n')

%% identify failed simulations

% variables used in failed simulations
fail_vars = vars(:,fail_ids);

%% recover variables varied over simulations

calM3_vals = unique(vars(1,:));
Gamd_vals  = unique(vars(2,:));
rho_vals   = unique(vars(3,:));
k_vals     = unique(vars(4,:));

num_calM3 = length(calM3_vals);
num_Gamd  = length(Gamd_vals);
num_rho   = length(rho_vals);
num_k     = length(k_vals);

num_sims = size(vars,2);

%% some sorting to facilitate easier plots later on

% average VAL and eta over all beats; store as row vector
VALav = mean(VAL');
effav = mean(eff');

% determine time varying d10, d20, d30 (proximal/head frames)
N = size(X_vals{1},1)/3;
for i = 1:num_sims
    if ~isempty(d1_vals{i})
        d10_vals{i} = [d1_vals{i}(1,:); d1_vals{i}(N+1,:); d1_vals{i}(2*N+1,:)]; 
        d20_vals{i} = [d2_vals{i}(1,:); d2_vals{i}(N+1,:); d2_vals{i}(2*N+1,:)]; 
        d30_vals{i} = [d3_vals{i}(1,:); d3_vals{i}(N+1,:); d3_vals{i}(2*N+1,:)]; 
    end
end

% group by wave number
for i = 1:num_k
    id = find(vars(4,:)==k_vals(i));
    ksrt(i).id    = id;
    ksrt(i).vars  = vars(:,id);
    ksrt(i).calM3 = vars(1,id);
    ksrt(i).Gamd  = vars(2,id);
    ksrt(i).rho   = vars(3,id);
    ksrt(i).k     = unique(vars(4,id));
    ksrt(i).X     = X_vals(id);
    ksrt(i).y     = y_vals(id);
    ksrt(i).d1    = d1_vals(id);
    ksrt(i).d2    = d2_vals(id);
    ksrt(i).d3    = d3_vals(id);
    ksrt(i).d10   = d10_vals(id);
    ksrt(i).d20   = d20_vals(id);
    ksrt(i).d30   = d30_vals(id);
    ksrt(i).eff   = eff(id,:)';
    ksrt(i).effav = effav(id);
    ksrt(i).VAL   = VAL(id,:)';
    ksrt(i).VALav = VALav(id);

    ksrt(i).ptraj_full = ptraj_full(id);
    ksrt(i).dtraj_full = dtraj_full(id);
end

% for each wave number, sort by average VAL (for VAL plots)
for i = 1:num_k
    [~,id] = sort(ksrt(i).VALav);
    ksrt(i).VALsrt.id    = id;
    ksrt(i).VALsrt.VALav = ksrt(i).VALav(id);
    ksrt(i).VALsrt.eff   = ksrt(i).eff(id);
    ksrt(i).VALsrt.X     = ksrt(i).X(:,id);
    ksrt(i).VALsrt.y     = ksrt(i).y(:,id);
    ksrt(i).VALsrt.calM3 = ksrt(i).calM3(id);
    ksrt(i).VALsrt.Gamd  = ksrt(i).Gamd(id);
    ksrt(i).VALsrt.rho   = ksrt(i).rho(id);
end

% for each wave number, sort by Gamma_d 
for i = 1:num_k
    [~,id] = sort(ksrt(i).Gamd);
    ksrt(i).Gsrt.id    = id;
    ksrt(i).Gsrt.VALav = ksrt(i).VALav(id);
    ksrt(i).Gsrt.effav = ksrt(i).effav(id);
    ksrt(i).Gsrt.X     = ksrt(i).X(:,id);
    ksrt(i).Gsrt.y     = ksrt(i).y(:,id);
    ksrt(i).Gsrt.calM3 = ksrt(i).calM3(id);
    ksrt(i).Gsrt.Gamd  = ksrt(i).Gamd(id);
    ksrt(i).Gsrt.rho   = ksrt(i).rho(id);
end

%% set figure axes fonts

% latex and font size for tick labels
set(groot, 'defaultAxesTickLabelInterpreter','latex');
set(groot, 'defaultAxesFontSize',14);

% define marker styles to use
mkrs = {'s','d','o','^','>'};

%% average rot rate for cells in wall approach simulations

% compute here for easy access
idG=find(vars(2,:)==Gamd_vals(2));
idk=find(vars(4,:)==4*pi);
idr=find(vars(3,:)==1);
idM=find(abs(vars(1,:)-1.5e-4)<1e-10);
id = intersect(intersect(idG,idk),intersect(idr,idM));
wall_avrot(1) = rollRate(id);
idG=find(vars(2,:)==Gamd_vals(3));
id = intersect(intersect(idG,idk),intersect(idr,idM));
wall_avrot(2) = rollRate(id);

%% fourier transform of end tip trajectory

% consider k=4*pi, all Gam_d (produces variety of planar -> nonplanar
% beats), and calM3=1.5e-4

% for each simulation, plot the full flagellum tip trajectory in both
% pinned and clamped frames. Then, project onto yz plane, and compute
% fourier transform

% find corresponding simulation data
idk = find(vars(4,:)==4*pi);
idM = find(abs(vars(1,:)-1.5e-4)<1e-10);
idr = find(vars(3,:)==1);
idi = intersect(intersect(idk,idr),idM);

% start from i=1 (exclude boring planar case for stiff flagellum)
start_beat = 3;
for i = 2:num_Gamd
    % select data, from after warm up
    aa = start_beat*params{idi(i)}.WarmUp*params{idi(i)}.NumSlices+1;
    Xi = X_vals{idi(i)}(:,aa:end);
    traji = dtraj_full{idi(i)}(:,aa:end);
    tps = t_vals{idi(i)}(aa:end);

    % pinned trajectory
    f1 = figure(1); pkf_pinned{i} = plotTipTraceProj(Xi, traji, tps, 'pinned');
    set(gcf, 'Position',[537 386 519 177])
    save_str = sprintf('./spx_twist_bend/figures/infinite fluid/dtraj pinned Gid=%g',i);
    save2pdf(save_str, f1, 300);

    % clamped trajectory
    d10i = d10_vals{idi(i)}(:,aa:end);
    d20i = d20_vals{idi(i)}(:,aa:end);
    d30i = d30_vals{idi(i)}(:,aa:end);
    f2 = figure(2); pkf_clamped{i} = plotTipTraceProj(Xi, traji, tps, 'clamped', d10i, d20i, d30i);
    set(gcf, 'Position',[537 386 519 177])
    save_str = sprintf('./spx_twist_bend/figures/infinite fluid/dtraj clamped Gid=%g',i);
    save2pdf(save_str, f2, 300);

    % reset
    close all
end

% group frequencies by number
for i = 2:num_Gamd
    pk1_pin_freq(i-1) = pkf_pinned{i}(1);
    pk2_pin_freq(i-1) = pkf_pinned{i}(2);
    pk3_pin_freq(i-1) = pkf_pinned{i}(3);
    pk1_cmp_freq(i-1) = pkf_clamped{i}(1);
    pk2_cmp_freq(i-1) = pkf_clamped{i}(2);
end

% plot peak frequencies
f1 = figure(1);
tiledlayout('flow','TileSpacing','compact')
nexttile; box on;
semilogx(Gamd_vals(2:end), pk1_pin_freq, 's-', 'LineWidth',1.2); grid on; hold on;
semilogx(Gamd_vals(2:end), pk2_pin_freq, 'd-', 'LineWidth',1.2);
semilogx(Gamd_vals(2:end), pk3_pin_freq, '^-', 'LineWidth',1.2);
xlabel('$\Gamma_d$' ,'FontSize',14,'Interpreter','latex')
ylabel('$f$', 'FontSize',14','Interpreter','latex')
ax = gca; ax.YAxis.Exponent = -2;
ylim([0,1e-1])
lgd = legend('1st','2nd','3rd', 'FontSize 12', 'Interpreter','latex', ...
    'Location','southoutside', 'EdgeColor','none');
lgd.NumColumns = 3;
% lgd.Position = [0.1387 0.0531 0.7127 0.0793];

nexttile; box on;
semilogx(Gamd_vals(2:end), pk1_cmp_freq, 's-', 'LineWidth',1.2); grid on; hold on;
semilogx(Gamd_vals(2:end), pk2_cmp_freq, 'd-', 'LineWidth',1.2);
xlabel('$\Gamma_d$' ,'FontSize',14,'Interpreter','latex')
ax = gca; ax.YAxis.Exponent = -2;
ylim([0,1e-1])

% resize and save
set(gcf, 'Position',[767 740 353 207])
save2pdf('./spx_twist_bend/figures/infinite fluid/fourier endp freqs', f1, 300)
close all

% plot mean roll rate against each peak frequency
rr = rollRate(idi(2:end));
f1 = figure(1); 
tiledlayout('flow','TileSpacing','compact'); nexttile; box on;
plot(rr, pk1_pin_freq ,'s-', 'LineWidth',1.2); grid on; hold on;
plot(rr, pk2_pin_freq ,'d-', 'LineWidth',1.2);
plot(rr, pk3_pin_freq ,'^-', 'LineWidth',1.2);
xlabel('$\omega$', 'FontSize',14,'Interpreter','latex')
ylabel('$f$', 'FontSize',14,'Interpreter','latex')
ax = gca; ax.YAxis.Exponent = -2;
ylim([0,1e-1])
lgd = legend('1st','2nd','3rd', 'FontSize 12', 'Interpreter','latex', ...
    'Location','southoutside', 'EdgeColor','none');
lgd.NumColumns = 3;

nexttile;
plot(rr, pk1_cmp_freq ,'s-', 'LineWidth',1.2); grid on; hold on;
plot(rr, pk2_cmp_freq ,'d-', 'LineWidth',1.2);
ax = gca; ax.YAxis.Exponent = -2;
ylim([0,1e-1])
set(gcf, 'Position',[767 740 353 207])
save2pdf('./spx_twist_bend/figures/infinite fluid/fourier endp freqs roll', f1, 300)
close all

%% cell rolling number

cols = parula(num_calM3+1);

% num_k panels, each with num_calM3 lines showing log(Gam_d) against
% rollRate
% illustrates how changes in stiffness and activity affect cell rolling
for i = 1:num_k
    
    nexttile; 
    for j = 1:num_calM3

        % find data corresponding to selected calM3 and chosen rho
        idM = find(abs(vars(1,:)-calM3_vals(j))<=1e-10); 
        idr = find(vars(3,:) == 1);
        idk = find(vars(4,:) == k_vals(i));
        idi = intersect(intersect(idM,idr), idk);
        xx = vars(2,idi);
        yy = numRoll(idi);

        % remove any data corresponding to zero VAL -- these have failed
        zid = find(VALav(idi)==0);
        yy(zid) = [];
        xx(zid) = [];

        % remove any NaN
        nanid = isnan(yy);
        yy(nanid) = [];
        xx(nanid) = [];

        % plot on semilog axis
        semilogx(xx, yy, 'LineStyle','-', 'Marker',mkrs{j}, 'LineWidth',1.2, ...
            'Color',cols(j,:));
        box on; grid on; hold on;

        % set axes
        xlim([Gamd_vals(1),Gamd_vals(end)])
        ylim([0,10])
    end

end

% dummy legend (to be filled in in edit)
lgd = legend('m_3','m_3','m_3','m_3','m_3', 'EdgeColor','none');
lgd.Position = [0.6713 0.1851 0.0858 0.2106];

% set figure size
set(gcf, 'Position',[207 557 580 390])

% save
save2pdf('./spx_twist_bend/figures/infinite fluid/rollNum overview.pdf')
close all

%% cell rolling rate

cols = parula(num_calM3+1);

% num_k panels, each with num_calM3 lines showing log(Gam_d) against
% rollRate
% illustrates how changes in stiffness and activity affect cell rolling
for i = 1:num_k
    
    nexttile; 
    for j = 1:num_calM3

        % find data corresponding to selected calM3 and chosen rho
        idM = find(abs(vars(1,:)-calM3_vals(j))<=1e-10); 
        idr = find(vars(3,:) == 1);
        idk = find(vars(4,:) == k_vals(i));
        idi = intersect(intersect(idM,idr), idk);
        xx = vars(2,idi);
        yy = rollRate(idi);

        % remove any data corresponding to zero VAL -- these have failed
        zid = find(VALav(idi)==0);
        yy(zid) = [];
        xx(zid) = [];

        % remove any NaN
        nanid = isnan(yy);
        yy(nanid) = [];
        xx(nanid) = [];

        % plot on semilog axis
        semilogx(xx, yy, 'LineStyle','-', 'Marker',mkrs{j}, 'LineWidth',1.2, ...
            'Color',cols(j,:));
        box on; grid on; hold on;

        % set axes
        xlim([Gamd_vals(1),Gamd_vals(end)])
        ylim([0,1])
        ax = gca; ax.YAxis.Exponent = -1;
    end

end

% dummy legend (to be filled in in edit)
lgd = legend('m_3','m_3','m_3','m_3','m_3', 'EdgeColor','none');
lgd.Position = [0.6713 0.1851 0.0858 0.2106];

% set figure size
set(gcf, 'Position',[207 557 580 390])

% save
save2pdf('./spx_twist_bend/figures/infinite fluid/rollRate overview.pdf')
close all

%% cell rolling against VAL

cols = parula(num_calM3+1);
for i = 1:num_k-1 % k=5pi case is very planar, will throw error in fitting so exclude
    nexttile;
    for j = 1:num_calM3

        % find data corresponding to selected calM3 and chosen rho
        idM = find(abs(vars(1,:)-calM3_vals(j))<=1e-10); 
        idr = find(vars(3,:) == 1);
        idk = find(vars(4,:) == k_vals(i));
        idi = intersect(intersect(idM,idr), idk);
        yy = VALav(idi);
        xx = rollRate(idi);

       % remove any data corresponding to zero VAL -- these have failed
        zid = find(VALav(idi)==0);
        yy(zid) = [];
        xx(zid) = [];

        % remove any NaN
        nanid = isnan(xx);
        yy(nanid) = [];
        xx(nanid) = [];

        % plot markers
        [xx,idx] = sort(xx);
        yy = yy(idx);
        plot(xx, yy, 'LineStyle','-', 'Marker',mkrs{j}, 'LineWidth',1.2, ...
           'Color',cols(j,:));

        % fit line of best fit
        %pl{j} = scatter(xx, yy, 'Marker',mkrs{j}, 'MarkerEdgeColor',cols(j,:), 'LineWidth',1.2);
        box on; grid on; hold on;

        %{
        xq = linspace(0,max(xx));
        F0 = fit(xx',yy','linearinterp');
        yq = F0(xq);
        plot(xq, yq, 'LineWidth',1.2, 'Color',cols(j,:));
        %}

        % set axes
        %xlim([Gamd_vals(1),Gamd_vals(end)])
        ylim([0,4]*1e-3)
        ax = gca; ax.YAxis.Exponent = -3;
    end

end

% dummy legend (to be filled in in edit)
% lgd = legend({'m_3','m_3','m_3','m_3','m_3'}, 'EdgeColor','none');
% lgd.Position = [0.6713 0.1851 0.0858 0.2106];

% set figure size
set(gcf, 'Position',[207 748 665 199])

% save
save2pdf('./spx_twist_bend/figures/infinite fluid/rollVAL overview.pdf')
close all

%% varying rho

%{
% choose Gam_d=10^3, calM3=1.5e-4, k=4*pi

% find data corresponding to selected values
kid = 3;
idM = find(abs(ksrt(kid).calM3-1.5e-4) < 1e-10);
idG = find(ksrt(kid).Gamd == 10^4); 
idr(1,:) = find(ksrt(kid).rho == 1/2);
idr(2,:) = find(ksrt(kid).rho == 1/4);

% plot cells =============================================================
for i = 1:size(idr,1)
    nv = 1;
    idi  = intersect(intersect(idM,idG), idr(i,:));
    Xi   = ksrt(kid).X{idi};        Xi   = Xi(:,(end-nv*params.NumSlices)+1:end);
    yi   = ksrt(kid).y{idi};        yi   = yi(:,(end-nv*params.NumSlices)+1:end);
    d10i = ksrt(kid).d10{idi};      d10i = d10i(:,(end-nv*params.NumSlices)+1:end);
    d20i = ksrt(kid).d20{idi};      d20i = d20i(:,(end-nv*params.NumSlices)+1:end);
    d30i = ksrt(kid).d30{idi};      d30i = d30i(:,(end-nv*params.NumSlices)+1:end);
    ptji = ksrt(kid).ptraj_full{idi}(:,params.NumSlices+1:end);

    % cell pinned at X0 (can pivot and roll) -----------------------------
    % easier to plot seperately and combine in editor!!
    f1 = figure(1); plotRelativeTraceSPX(Xi, yi, d10i, false, true, ...
        true, false, true, ptji);
    xlim([-0.2 1.0]); ylim([-0.15 0.15]); zlim([-0.2 0.2])
    set(gcf, 'Position',[608 708 512 239])
    save_str = sprintf('./spx_twist_bend/figures/twb rho %g xyz',i);
    save2pdf(save_str, f1, 300)
    
    f2 = figure(2); plotRelativeTraceSPX(Xi, yi, d10i, false, true, ...
        false, true, false); 
    view([90,0]);     % yz plane 
    ylim([-0.2 0.2]); zlim([-0.2 0.2])
    set(gcf, 'Position',[413 726 395 171])
    save_str = sprintf('./spx_twist_bend/figures/twb rho pinned %g yz',i);
    save2pdf(save_str, f2, 300)
    
    f3 = figure(3); plotRelativeTraceSPX(Xi, yi, d10i, false, true, ...
        true, false, false); 
    view(2);         % xy plane
    xlim([-0.2 1.0]); ylim([-0.2 0.2])
    set(gcf, 'Position',[247 508 439 138])
    save_str = sprintf('./spx_twist_bend/figures/twb rho pinned %g xy',i);
    save2pdf(save_str, f3, 300)
 
    f4 = figure(4); plotRelativeTraceSPX(Xi, yi, d10i, false, true, ...
        true, false, false); 
    view([0,-1,0]);   % xz plane
    xlim([-0.2 1.0]); zlim([-0.2 0.2])
    set(gcf, 'Position',[247 508 439 138])
    save_str = sprintf('./spx_twist_bend/figures/twb rho pinned %g xz',i);
    save2pdf(save_str, f4, 300)

    close all
    
    % cell clamped at head (no pivot or roll) ----------------------------
    f1 = figure(1); plotRelativeTraceSPX(Xi, yi, d10i, true, true, ...
        true, false, false, d20i, d30i);
    xlim([-0.2 1.0]); ylim([-0.15 0.15]); zlim([-0.2 0.2])
    set(gcf, 'Position',[608 708 512 239])
    save_str = sprintf('./spx_twist_bend/figures/twb rho clamped %g xyz',i);
    save2pdf(save_str, f1, 300)
    
    f2 = figure(2); plotRelativeTraceSPX(Xi, yi, d10i, true, true, ...
        false, true, false, d20i, d30i); 
    view([90,0]);     % yz plane 
    ylim([-0.2 0.2]); zlim([-0.2 0.2])
    set(gcf, 'Position',[413 726 395 171])
    save_str = sprintf('./spx_twist_bend/figures/twb rho clamped %g yz',i);
    save2pdf(save_str, f2, 300)
    
    f3 = figure(3); plotRelativeTraceSPX(Xi, yi, d10i, true, true, ...
        true, false, false, d20i, d30i); 
    view(2);         % xy plane
    xlim([-0.2 1.0]); ylim([-0.2 0.2])
    set(gcf, 'Position',[247 508 439 138])
    save_str = sprintf('./spx_twist_bend/figures/twb rho clamped %g xy',i);
    save2pdf(save_str, f3, 300)
 
    f4 = figure(4); plotRelativeTraceSPX(Xi, yi, d10i, true, true, ...
        true, false, false, d20i, d30i); 
    view([0,-1,0]);   % xz plane
    xlim([-0.2 1.0]); zlim([-0.2 0.2])
    set(gcf, 'Position',[247 508 439 138])
    save_str = sprintf('./spx_twist_bend/figures/twb rho clamped %g xz',i);
    save2pdf(save_str, f4, 300)

    close all
    
end
%}

%% stiff vs flexible cell
% i.e. comparing a low value of Gam_d to a high value of Gam_d

% consider k=4*pi, where clear disctinction between hi and lo Gam_d is
% observed in VAL and eff.
% VAL and eff largely invariant to choices of calM3, so pick 1.5e-4 (middle
% value)

% find data corresponding to selected calM3, rho, and Gam_d
idk = find(vars(4,:)==4*pi);
idM = find(abs(vars(1,:)-1.5e-4)<1e-10);
idr = find(vars(3,:)==1);
idG{1} = find(vars(2,:)==10^3); % stiff cell
idG{2} = find(vars(2,:)==10^4);
idG{3} = find(vars(2,:)==10^5); % flexible cell

% plot cells =============================================================
% view over last <nv> beats
for i = 1:size(idG,1)
    nv = 1;
    idi  = intersect(intersect(idk,idM), intersect(idr, idG{i}));
    Xi   = ksrt(kid).X{idi}(:,(end-nv*params.NumSlices)+1:end);
    yi   = ksrt(kid).y{idi}(:,(end-nv*params.NumSlices)+1:end);
    d10i = ksrt(kid).d10{idi}(:,(end-nv*params.NumSlices)+1:end);
    d20i = ksrt(kid).d20{idi}(:,(end-nv*params.NumSlices)+1:end);
    d30i = ksrt(kid).d30{idi}(:,(end-nv*params.NumSlices)+1:end);
    ptji = ksrt(kid).ptraj_full{idi}(:,params.NumSlices+1:end);
    
    % cell pinned at X0 (can pivot and roll) -----------------------------
    % easier to plot seperately and combine in editor!!
    f1 = figure(1); plotRelativeTraceSPX(Xi, yi, d10i, false, true, ...
        true, false, true, ptji);
    xlim([-0.2 1.0]); ylim([-0.15 0.15]); zlim([-0.2 0.2])
    set(gcf, 'Position',[608 708 512 239])
    save_str = sprintf('./spx_twist_bend/figures/twb stiff comp pinned %g xyz',i);
    save2pdf(save_str, f1, 300)
    
    f2 = figure(2); plotRelativeTraceSPX(Xi, yi, d10i, false, true, ...
        false, true, false); 
    view([90,0]);     % yz plane 
    ylim([-0.2 0.2]); zlim([-0.2 0.2])
    set(gcf, 'Position',[413 726 395 171])
    save_str = sprintf('./spx_twist_bend/figures/twb stiff comp pinned %g yz',i);
    save2pdf(save_str, f2, 300)
    
    f3 = figure(3); plotRelativeTraceSPX(Xi, yi, d10i, false, true, ...
        true, false, false); 
    view(2);         % xy plane
    xlim([-0.2 1.0]); ylim([-0.2 0.2])
    set(gcf, 'Position',[247 508 439 138])
    save_str = sprintf('./spx_twist_bend/figures/twb stiff comp pinned %g xy',i);
    save2pdf(save_str, f3, 300)
 
    f4 = figure(4); plotRelativeTraceSPX(Xi, yi, d10i, false, true, ...
        true, false, false); 
    view([0,-1,0]);   % xz plane
    xlim([-0.2 1.0]); zlim([-0.2 0.2])
    set(gcf, 'Position',[247 508 439 138])
    save_str = sprintf('./spx_twist_bend/figures/twb stiff comp pinned %g xz',i);
    save2pdf(save_str, f4, 300)

    close all
    
    % cell clamped at head (no pivot or roll) ----------------------------
    f1 = figure(1); plotRelativeTraceSPX(Xi, yi, d10i, true, true, ...
        true, false, false, d20i, d30i);
    xlim([-0.2 1.0]); ylim([-0.15 0.15]); zlim([-0.2 0.2])
    set(gcf, 'Position',[608 708 512 239])
    save_str = sprintf('./spx_twist_bend/figures/twb stiff comp clamped %g xyz',i);
    save2pdf(save_str, f1, 300)
    
    f2 = figure(2); plotRelativeTraceSPX(Xi, yi, d10i, true, true, ...
        false, true, false, d20i, d30i); 
    view([90,0]);     % yz plane 
    ylim([-0.2 0.2]); zlim([-0.2 0.2])
    set(gcf, 'Position',[413 726 395 171])
    save_str = sprintf('./spx_twist_bend/figures/twb stiff comp clamped %g yz',i);
    save2pdf(save_str, f2, 300)
    
    f3 = figure(3); plotRelativeTraceSPX(Xi, yi, d10i, true, true, ...
        true, false, false, d20i, d30i); 
    view(2);         % xy plane
    xlim([-0.2 1.0]); ylim([-0.2 0.2])
    set(gcf, 'Position',[247 508 439 138])
    save_str = sprintf('./spx_twist_bend/figures/twb stiff comp clamped %g xy',i);
    save2pdf(save_str, f3, 300)
 
    f4 = figure(4); plotRelativeTraceSPX(Xi, yi, d10i, true, true, ...
        true, false, false, d20i, d30i); 
    view([0,-1,0]);   % xz plane
    xlim([-0.2 1.0]); zlim([-0.2 0.2])
    set(gcf, 'Position',[247 508 439 138])
    save_str = sprintf('./spx_twist_bend/figures/twb stiff comp clamped %g xz',i);
    save2pdf(save_str, f4, 300)

    close all
end

%% beat variation with wave number

% choose Gamd=1e4 (which permits some nonplanar motion), calM3=0.5e-4 (has
% values for all k), rho=1 (as above)

% find data corresponding to above specified values for each k value
for i = 1:num_k
    idG = find(vars(2,:)==10^4);
    idM = find(vars(1,:)==0.5e-4);
    idr = find(vars(3,:)==1);
    idk = find(vars(4,:)==k_vals(i));
    idi(i) = intersect(intersect(idG,idM),intersect(idr,idk));
end

% only consider integer multiples of pi, more than 3 plots wont fit on a
% page in the thesis.
kints = 3:0.5:5;
for i = 1:2:num_k
    nv = 1;
    Xi   = X_vals{idi(i)}(:,(end-nv*params.NumSlices)+1:end);
    yi   = y_vals{idi(i)}(:,(end-nv*params.NumSlices)+1:end);
    d10i = d10_vals{idi(i)}(:,(end-nv*params.NumSlices)+1:end);
    d20i = d20_vals{idi(i)}(:,(end-nv*params.NumSlices)+1:end);
    d30i = d30_vals{idi(i)}(:,(end-nv*params.NumSlices)+1:end);
    ptji = ptraj_full{idi(i)}(:,params.NumSlices+1:end);
    
    % cell pinned at X0 (can pivot and roll) -----------------------------
    % easier to plot seperately and combine in editor!!
    f1 = figure(1); plotRelativeTraceSPX(Xi, yi, d10i, false, true, ...
        true, false, true, ptji);
    xlim([-0.2 1.0]); ylim([-0.15 0.15]); zlim([-0.2 0.2])
    set(gcf, 'Position',[608 708 512 239])
    save_str = sprintf('./spx_twist_bend/figures/twb k=%gpi pinned xyz',kints(i));
    save2pdf(save_str, f1, 300)
    
    f2 = figure(2); plotRelativeTraceSPX(Xi, yi, d10i, false, true, ...
        false, true, false); 
    view([90,0]);     % yz plane 
    ylim([-0.2 0.2]); zlim([-0.2 0.2])
    set(gcf, 'Position',[413 726 395 171])
    save_str = sprintf('./spx_twist_bend/figures/twb k=%gpi pinned yz',kints(i));
    save2pdf(save_str, f2, 300)
    
    f3 = figure(3); plotRelativeTraceSPX(Xi, yi, d10i, false, true, ...
        true, false, false); 
    view(2);         % xy plane
    xlim([-0.2 1.0]); ylim([-0.2 0.2])
    set(gcf, 'Position',[247 508 439 138])
    save_str = sprintf('./spx_twist_bend/figures/twb k=%gpi pinned xy',kints(i));
    save2pdf(save_str, f3, 300)
 
    f4 = figure(4); plotRelativeTraceSPX(Xi, yi, d10i, false, true, ...
        true, false, false); 
    view([0,-1,0]);   % xz plane
    xlim([-0.2 1.0]); zlim([-0.2 0.2])
    set(gcf, 'Position',[247 508 439 138])
    save_str = sprintf('./spx_twist_bend/figures/twb k=%gpi pinned xz',kints(i));
    save2pdf(save_str, f4, 300)

    close all
    
    % cell clamped at head (no pivot or roll) ----------------------------
    f1 = figure(1); plotRelativeTraceSPX(Xi, yi, d10i, true, true, ...
        true, false, false, d20i, d30i);
    xlim([-0.2 1.0]); ylim([-0.15 0.15]); zlim([-0.2 0.2])
    set(gcf, 'Position',[608 708 512 239])
    save_str = sprintf('./spx_twist_bend/figures/twb k=%gpi clamped xyz',kints(i));
    save2pdf(save_str, f1, 300)
    
    f2 = figure(2); plotRelativeTraceSPX(Xi, yi, d10i, true, true, ...
        false, true, false, d20i, d30i); 
    view([90,0]);     % yz plane 
    ylim([-0.2 0.2]); zlim([-0.2 0.2])
    set(gcf, 'Position',[413 726 395 171])
    save_str = sprintf('./spx_twist_bend/figures/twb k=%gpi clamped yz',kints(i));
    save2pdf(save_str, f2, 300)
    
    f3 = figure(3); plotRelativeTraceSPX(Xi, yi, d10i, true, true, ...
        true, false, false, d20i, d30i);  
    view(2);         % xy plane
    xlim([-0.2 1.0]); ylim([-0.2 0.2])
    set(gcf, 'Position',[247 508 439 138])
    save_str = sprintf('./spx_twist_bend/figures/twb k=%gpi clamped xy',kints(i));
    save2pdf(save_str, f3, 300)
 
    f4 = figure(4); plotRelativeTraceSPX(Xi, yi, d10i, true, true, ...
        true, false, false, d20i, d30i); 
    view([0,-1,0]);   % xz plane
    xlim([-0.2 1.0]); zlim([-0.2 0.2])
    set(gcf, 'Position',[247 508 439 138])
    save_str = sprintf('./spx_twist_bend/figures/twb k=%gpi clamped xz',kints(i));
    save2pdf(save_str, f4, 300)

    close all
end

%% VAL 
% for different k, calM3, and Gam_d
% VALs are stored as: each row a simulation, columns the VAL over that beat

cols = parula(num_calM3+1);

% num_k panels, each with num_calM3 lines showing log(Gam_d) against VALav
% illustrates how changes in stiffness and activity affect VAL
for i = 1:num_k
    
    nexttile; 
    for j = 1:num_calM3

        % find data corresponding to selected calM3 and chosen rho
        rho = rho_vals(3);
        idM = find(ksrt(i).Gsrt.calM3 == calM3_vals(j));
        idr = find(ksrt(i).Gsrt.rho == rho);
        idi = intersect(idM,idr);
        xx = ksrt(i).Gsrt.Gamd(idi);
        yy = ksrt(i).Gsrt.VALav(idi);

        % remove any zeros from vectors -- these correspond to failed sims
        nzr = find(yy~=0);
        xx  = xx(nzr);
        yy  = yy(nzr);

        % plot on semilog axis
        semilogx(xx, yy, 'LineStyle','-', 'Marker',mkrs{j}, 'LineWidth',1.2, ...
            'Color',cols(j,:));
        box on; grid on; hold on;

        % set axes
        xlim([Gamd_vals(1),Gamd_vals(end)])
        ylim([1,4]*1e-3)
    end

end

% dummy legend (to be filled in in edit)
lgd = legend('m_3','m_3','m_3','m_3','m_3', 'EdgeColor','none');
lgd.Position = [0.6713 0.1851 0.0858 0.2106];

% set figure size
set(gcf, 'Position',[207 557 580 390])

% save
save2pdf('./spx_twist_bend/figures/twb VAL overview.pdf')
close all

%% Lighthill efficiency
% for different k, calM3, and Gam_d

% num_k panels, each with num_calM3 lines showing log(Gam_d) against VALav
% illustrates how changes in stiffness and activity affect eta
for i = 1:num_k
    
    nexttile; 
    for j = 1:num_calM3

        % find data corresponding to selected calM3 and chosen rho
        rho = rho_vals(3);
        idM = find(ksrt(i).Gsrt.calM3 == calM3_vals(j));
        idr = find(ksrt(i).Gsrt.rho == rho);
        idi = intersect(idM,idr);
        xx = ksrt(i).Gsrt.Gamd(idi);
        yy = ksrt(i).Gsrt.effav(idi);

        % remove any zeros from vectors -- these correspond to failed sims
        nzr = find(yy~=0);
        xx  = xx(nzr);
        yy  = yy(nzr);

        % plot on semilog axis
        semilogx(xx, yy, 'LineStyle','-', 'Marker',mkrs{j}, 'LineWidth',1.2, ...
            'Color',cols(j,:));
        box on; grid on; hold on;

        % set axes
        xlim([Gamd_vals(1),Gamd_vals(end)])
        ylim([0,13]*1e-6)
    end

end

% dummy legend (to be filled in in edit)
lgd = legend('m_3','m_3','m_3','m_3','m_3', 'EdgeColor','none');
lgd.Position = [0.6713 0.1851 0.0858 0.2106];

% set figure size
set(gcf, 'Position',[207 557 580 390])

% save
save2pdf('./spx_twist_bend/figures/twb eta overview.pdf')
close all

%% complete

fprintf('Plots completed!\n')
printLinebreak

end % function
function spxTwistBendParameterSweep(it, ittot)

% for a single fixed calS and calM2 value, we vary twist parameters Gam_s
% and calM3 to generate cells swimming with a twist & bend-type beat

% check in correct directory (bluebear call will start in subdir, so we 
% need to move up)
dir = pwd;
if strcmpi(dir(end-13:end),'spx_twist_bend')
    cd ..
end
addFilesToPath;
printLinebreak

% folder for data
if ~exist('./spx_twist_bend/data','dir')
    mkdir('./spx_twist_bend/data')
end

% define fixed values
calS  = 12;
calM2 = 0.015;

% define values to vary across simulations
calM3_vals = (0.5:0.5:2.5) * 1e-4;      % moment amplitude
Gamd_vals = logspace(3,5,5);            % bend/twist drag ratio
om_vals = [1,1/2,1/4];                  % relative twist timescale
k_vals = (3:0.5:5) * pi;                % actuation wave number

% variables for simulations
vars = combvec(calM3_vals, Gamd_vals, om_vals, k_vals);
num_sims = size(vars,2);

%% split into batches

% determine number of simulations per batch
sims_per_block = ceil(num_sims/ittot);

% set limits of simulation loop depending on which block has been called
start = (it-1)*sims_per_block+1;
if it < ittot
    fin = it*sims_per_block;
else
    fin = num_sims;        % in case num_combs doesn't divide cleanly by itot
end

%% run simulations

parfor k = start:fin
    
    % extract variables
    calM3k = vars(1,k);
    Gamdk = vars(2,k);
    omk = vars(3,k);
    kk = vars(4,k);
    
    % set options
    options = setOptions('SwimmingCell', 'Species','human', 'BodyType','sunanda', ...
        'NonLocal',true, 'MechTwist',true, 'StiffModel','varying', 'SilentMode',true, ...
        'PlaneOfBeat','xz', 'SaveVideo',false, 'ActModel','trig', 'EndpieceModel','heaviside');

    % set parameters
    params = setParameters(options, 'ittol',1e-4, 'NF',251, 'dt',2*pi/60, 'q',0.1, ...
        'calS',calS, 'calM1',0, 'calM2',calM2, 'calM3',calM3k, 'Gam_s',4, ...
        'Gam_d',Gamdk, 'k_bend',kk, 'k_twist',kk, 'ell',0.05, 'epsilon',0.01, ...
        'om_bend',1, 'om_twist',omk, 'NumBeats',10, 'WarmUp',1, 'tmax','auto', ...
        'NumSave','auto', 'NumSlices',20, 's_crit',0.5);

    % set initial condition
    int = setInitialCondition(options, params, 'IntCond','line', 'X0',[0;0;0]);

    try 
        % run simulation
        outs = runSPX(int, params, options);
    catch
        % if simulation fails (by nonconvergence etc), set empty outputs
        outs = [];
    end

    % save data
    save_str = sprintf('./spx_twist_bend/data/paramSweep_calS=%02g_%03g_of_%03g.mat',calS,k,num_sims);
    parsave(save_str, outs, params, options, int, vars(:,k));
    
end % parfor


end % function
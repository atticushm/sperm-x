function spxCellApproachingWall(it, ittot)

% for some cells of varying calS, we consider cells approaching an infinite
% plane boundary at different angles of incidence Phi

% check in correct directory (bluebear call will start in subdir, so we 
% need to move up)
dir = pwd;
if strcmpi(dir(end-13:end),'spx_twist_bend')
    cd ..
end
addFilesToPath;
printLinebreak

% folder for data
if ~exist('./spx_twist_bend/data','dir')
    mkdir('./spx_twist_bend/data')
end

% define fixed values
h0 = 0.25;           % initial height above wall
calM3 = 1.5e-4;     % twist actuation parameter
numbeats = 40;      % number of beats to simulate
wavk = 4*pi;

% define values to vary
calS_vals = 12;                 % swimming parameter
Phi_vals  = pi/16:pi/16:pi/4;       % angle of indidence
Gamd_vals = logspace(3,4,3);    % bend/twist drag ratio

% variables for simulations
vars = combvec(calS_vals, Phi_vals, Gamd_vals);
num_sims = size(vars,2);

% slice vars to avoid overhead moaning by MATLAB
vars1 = vars(1,:);
vars2 = vars(2,:);
vars3 = vars(3,:);

%% split into batches

% determine number of simulations per batch
sims_per_block = ceil(num_sims/ittot);

% set limits of simulation loop depending on which block has been called
start = (it-1)*sims_per_block+1;
if it < ittot
    fin = it*sims_per_block;
else
    fin = num_sims;        % in case num_combs doesn't divide cleanly by itot
end

%% run simulations

parfor k = start:fin
    
    % extract variables
    calSk = vars1(k);
    Phik = vars2(k);
    Gamdk = vars3(k);

    % set calM2 according to calSk
    % values are 30% less than planar optimisers to minimise chance of self
    % intersection
    if (calSk==9)
        calM2k = 0.0299 -(0.2*0.0299);
    elseif (calSk==12)
        % calM2k = 0.0202 -(0.2*0.0202);
        calM2k = 0.015; 
    elseif (calSk==15)
        calM2k = 0.0142 -(0.2*0.0142);
    elseif (calSk==18)
        calM2k = 0.0126 -(0.2*0.0126);
    end
    
    % set options
    options = setOptions('SwimmingCell', 'Species','human', 'BodyType','sunanda', ...
        'NonLocal',true, 'MechTwist',true, 'StiffModel','varying', 'SilentMode',true, ...
        'PlaneOfBeat','xz', 'SaveVideo',false, 'ActModel','trig', 'EndpieceModel','heaviside', ...
        'UseBlakelets',true);

    % set parameters
    params = setParameters(options, 'ittol',1e-4, 'NF',251, 'dt',2*pi/100, 'q',0.1, ...
        'calS',calSk, 'calM1',0, 'calM2',calM2k, 'calM3',calM3, 'Gam_s',4, ...
        'Gam_d',Gamdk, 'k_bend',wavk, 'k_twist',wavk, 'ell',0.05, 'epsilon',0.01, ...
        'om_bend',1, 'om_twist',1, 'NumBeats',numbeats, 'WarmUp',1, 'tmax','auto', ...
        'NumSave','auto', 'NumSlices',20, 's_crit',0.5, 'h0',h0);
    
    % set initial condition
    int = setInitialCondition(options, params, 'IntCond','line', ...
        'X0',[0;0;0], 'Phi',Phik);

    try 
        % run simulation
        outs = runSPX(int, params, options);
    catch
        % if simulation fails (by nonconvergence etc), set empty outputs
        outs = [];
    end

    % save data
    save_str = sprintf('./spx_twist_bend/data/wallApproach_calS=%02g_%03g_of_%03g.mat',calSk,k,num_sims);
    parsave(save_str, outs, params, options, int, vars(:,k));
    
end % parfor

end % function
function generateShFilesOverWall(itot, test)

% generates multiple sh files for running twist/bend beating cells swimming
% over a planar wall

% itot is the number of batch functions to make
% test (bool) defines what bluebear qos to use: true=bbshort, false=bbdefault

printLinebreak

% make subfolder for .sh files
if ~exist('./spx_twist_bend/sh/','dir')
    mkdir('./spx_twist_bend/sh/')
end

% generate .sh filenames
for i = 1:itot
    fname{i} = sprintf('./spx_twist_bend/sh/bbOverWall_%02g_of_%02g', ...
       i, itot);
    if test
        fname{i} = [fname{i},'_test'];
    end
    fname{i} = [fname{i},'.sh'];
end

% write to each file
for i = 1:itot
   
    % open file for writing
    fID = fopen(fname{i},'w+');
    
    % print lines to files
    fprintf(fID, '#!/bin/bash\n');
    fprintf(fID, '#SBATCH --ntasks 12\n');
    if test
        fprintf(fID, '#SBATCH --time 0-00:10:0\n');
        fprintf(fID, '#SBATCH --qos bbshort\n');
    else
        % default should be 2 days -- reduce manually if using fin_ovr in
        % optimisation function (more likely to run sooner)
        fprintf(fID, '#SBATCH --time 2-00:00:0\n');
        fprintf(fID, '#SBATCH --qos bbdefault\n');
    end
    fprintf(fID, '#SBATCH --mail-type NONE\n');
    fprintf(fID, '#SBATCH --mem 100G\n\n');
    
    fprintf(fID, 'set -e\n\n');
    fprintf(fID, 'module purge; module load bluebear\n');
    fprintf(fID, 'module load MATLAB/2020a\n\n');
    
    % cd to code on bluebear directory and run
    fprintf(fID, 'cd ~/02_SPERMX/git/sperm-x/\n');
    fprintf(fID, 'matlab -nodisplay -r "addFilesToPath;spxCellOverWall(%g,%g)"\n', i, itot);
    
    % close file
    fclose(fID);

    % announce to command line
    fprintf('File written to %s\n',fname{i})
end
printLinebreak;

end % function
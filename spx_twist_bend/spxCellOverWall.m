function spxCellOverWall(it, ittot)

% for some cells of varying calS, we consider cells over an infinite
% plane boundary at different angles of incidence Phi

% check in correct directory (bluebear call will start in subdir, so we 
% need to move up)
dir = pwd;
if strcmpi(dir(end-13:end),'spx_twist_bend')
    cd ..
end
addFilesToPath;
printLinebreak

% folder for data
if ~exist('./spx_twist_bend/data','dir')
    mkdir('./spx_twist_bend/data')
end

% define fixed values
calM3 = 1.5e-4;     % twist actuation parameter
numbeats = 30;      % number of beats to simulate
wavk = 4*pi;        % wave number of actuation
Phi = 0;            % incidence angle over wall
Gam_d = 10^4;       % drag ratio

% define values to vary
% calS_vals = [9,12,15];          % swimming parameter
calS_vals = 12;
h_vals = [0.1,0.2,0.4,0.8,1.6];     % height above surface

% variables for simulations
vars = combvec(calS_vals, h_vals);

% add simulations without wall, as control, by appending to vars
vars = [vars; true(1,size(vars,2))]; 
controls = combvec(calS_vals,h_vals);
controls = [controls; false(1,size(controls,2))];
vars = [vars, controls];
num_sims = size(vars,2);

% slice vars to avoid overhead moaning by MATLAB
vars1 = vars(1,:);
vars2 = vars(2,:);
vars3 = vars(3,:);

%% split into batches

% determine number of simulations per batch
sims_per_block = ceil(num_sims/ittot);

% set limits of simulation loop depending on which block has been called
start = (it-1)*sims_per_block+1;
if it < ittot
    fin = it*sims_per_block;
else
    fin = num_sims;        % in case num_combs doesn't divide cleanly by itot
end

%% run simulations

parfor k = start:fin
    
    % extract variables
    calSk = vars1(k);
    hk = vars2(k);
    blkt = vars3(k);

    % set calM2 according to calSk
    % values are 20% less than planar optimisers to minimise chance of self
    % intersection
    if (calSk==9)
        calM2k = 0.0299 -(0.3*0.0299);
    elseif (calSk==12)
        calM2k = 0.0202 -(0.3*0.0202);
    elseif (calSk==15)
        calM2k = 0.0142 -(0.3*0.0142);
    elseif (calSk==18)
        calM2k = 0.0126 -(0.3*0.0126);
    end
    
    % set options
    options = setOptions('SwimmingCell', 'Species','human', 'BodyType','sunanda', ...
        'NonLocal',true, 'MechTwist',true, 'StiffModel','varying', 'SilentMode',true, ...
        'PlaneOfBeat','xz', 'SaveVideo',false, 'ActModel','trig', 'EndpieceModel','heaviside', ...
        'UseBlakelets',blkt);

    % set parameters
    params = setParameters(options, 'ittol',1e-4, 'NF',251, 'dt',2*pi/60, 'q',0.1, ...
        'calS',calSk, 'calM1',0, 'calM2',calM2k, 'calM3',calM3, 'Gam_s',4, ...
        'Gam_d',Gam_d, 'k_bend',wavk, 'k_twist',wavk, 'ell',0.05, 'epsilon',0.01, ...
        'om_bend',1, 'om_twist',1, 'NumBeats',numbeats, 'WarmUp',1, 'tmax','auto', ...
        'NumSave','auto', 'NumSlices',20, 's_crit',0.5, 'h0',hk );
    
    % set initial condition
    int = setInitialCondition(options, params, 'IntCond','line', ...
        'X0',[0;0;0], 'Phi',Phi);

    try 
        % run simulation
        outs = runSPX(int, params, options);
    catch
        % if simulation fails (by nonconvergence etc), set empty outputs
        outs = [];
    end

    % save data
    save_str = sprintf('./spx_twist_bend/data/wallOver_calS=%02g_%03g_of_%03g.mat',calSk,k,num_sims);
    parsave(save_str, outs, params, options, int, vars(:,k));
    
end % parfor

end % function
function preprocWallApproachData()

% preprocess simulation data for twist/bend beat cells approaching a planar 
% wall, from files stored at spx_twist_bend/data

printLinebreak

%% determine .mat filenames

% find filenames
calS = 12; % hardcode
prefix = sprintf('./spx_twist_bend/data/wallApproach_calS=%02g_3/', calS);
dname = dir([prefix, 'wallApproach*.mat']);

% dir will detect any zip folders in the directory -- neglect these
ii = 1;
for i = 1:length(dname)
    if ~strcmp(dname(i).name(end-3:end),'.zip')
        fname{ii} = dname(i).name;
        ii = ii+1;
    end
end
num_sims = length(fname);

% track indices of failed simulations
fail_ids = [];

fprintf('Loading data and computing metrics... ')
for i = 1:num_sims

    %% load data

    % load
    clearvars varargin
    load([prefix,fname{i}], 'varargin')

    % load SPX setup structures
    outs{i} = varargin{1};
    params{i} = varargin{2};
    options{i} = varargin{3};
    int{i} = varargin{4};

    if isempty(varargin{1})

        % store failed simulation ids, and variables
        fail_ids = [fail_ids, i];
        vars(:,i) = varargin{end};

    else

        % extract solution data
        X_vals{i}  = [varargin{1}.X];
        y_vals{i}  = [varargin{1}.y];
        t_vals{i}  = [varargin{1}.t];
        d1_vals{i} = [varargin{1}.d1];
        d2_vals{i} = [varargin{1}.d2];
        d3_vals{i} = [varargin{1}.d3];
    
        % force values 
        f_vals{i} = [varargin{1}.f];
        phi_vals{i} = [varargin{1}.phi];
    
        % parameters that were varied
        %   vars(1,:) is calS values
        %   vars(2,:) is Phi_k values
        %   vars(3,:) is k values
        %   vars(4,:) is Gamma_d values   
        vars(:,i) = varargin{end};

        %% compute metrics 
        % only compute if simulation successfully completed

        % indices of time points at the start of a beat
        % tps chosen as fractions of 2pi, so zero check allowable
        [~,idx] = find(mod(t_vals{i}, 2*pi)==0);
        num_beat = length(idx);
        assert(num_beat==params{i}.NumBeats, 'Incorrect number of beats found, check tolerance')

        % X0 trajectory, full and sampled at start of each beat
        ptraj = [];
        for k = 1:length(t_vals{i})
            ptraj_full{i}(:,k) = proxp(X_vals{i}(:,k));
            if mod(t_vals{i}(k), 2*pi)==0
                ptraj{i} = [ptraj, proxp(X_vals{i}(:,k))];
            end
        end

        % flag tip trajectory, full and sampled at start of each beat
        dtraj = [];
        for k = 1:length(t_vals{i})
            dtraj_full{i}(:,k) = distp(X_vals{i}(:,k));
            if mod(t_vals{i}(k), 2*pi)==0
                dtraj{i} = [dtraj, distp(X_vals{i}(:,k))];
            end
        end

        % VAL over each full beat (recall WarmUpBeats=1)
        for j = 1:num_beat-1
            Xj = X_vals{i}(:,idx(j):idx(j+1));
            tj = t_vals{i}(idx(j):idx(j+1));
            VAL(i,j) = calculateVAL(Xj, tj, 'spx');
        end

        % average work over each beat
        dt = params{i}.dt;
        for k = 1:length(t_vals{i})-1
            dX = (X_vals{i}(:,k+1)-X_vals{i}(:,k))/dt;
            dY = (y_vals{i}(:,k+1)-y_vals{i}(:,k))/dt;
            W(i,k) = calculateWork('spx', t_vals{i}(k), f_vals{i}(:,k),  ...
                phi_vals{i}(:,k), dX, dY);
        end
        for j = 1:num_beat-1
            if j < num_beat-1
                start = idx(j); fin = idx(j+1);
            else
                start = idx(j); fin = length(W(i,:));
            end
            Wav(i,j) = mean(W(start:fin));
        end

        % efficiency over each beat
        eff(i,:) = VAL(i,:).^2./Wav(i,:);

        % rate of rolling
        N = size(X_vals{i},1)/3;
        d10_vals{i} = [d1_vals{i}(1,:); d1_vals{i}(N+1,:); d1_vals{i}(2*N+1,:)];
        d20_vals{i} = [d2_vals{i}(1,:); d2_vals{i}(N+1,:); d2_vals{i}(2*N+1,:)];
        d30_vals{i} = [d3_vals{i}(1,:); d3_vals{i}(N+1,:); d3_vals{i}(2*N+1,:)];
        [numRoll(i),rollRate(i),rollRateFull{i},rollTh(i,:),rollThFull(i,:),rollIdx{i}] ...
            = calculateRollRate(d10_vals{i}, d20_vals{i}, d30_vals{i}, t_vals{i}, params{i});

        % deflection angles
        [th_xz(i,:), th_xy(i,:)] = calculateDeflectionAngles(ptraj_full{i}, params{i});
        rth_xz(i) = range(th_xz(i,:));
        rth_xy(i) = range(th_xy(i,:));

    end

end
fprintf('complete!\n')

%% save and export

% make directory 
save_dir = './spx_twist_bend/preproc/';
if ~exist(save_dir,'dir')
    mkdir(save_dir)
end

% save
save_str = sprintf([save_dir,'wallApproach_calS=%02g.mat'],calS);
save(save_str,'X_vals','y_vals','t_vals','vars','fail_ids', ...
    'd1_vals','d2_vals','d3_vals', 'ptraj_full', 'dtraj_full', 'ptraj', 'dtraj', ...
    'outs','params','options','int','VAL','eff', 'numRoll','rollRate',...
    'rollRateFull','rollTh','rollThFull','rollIdx', ...
    'd10_vals','d20_vals','d30_vals', 'th_xz','th_xy','rth_xy','rth_xz');
fprintf(['Preprocessed data for %g simulations saved at ',save_str,'\n'], num_sims)
fprintf('%g simulations failed\n', length(fail_ids))
printLinebreak

end % function
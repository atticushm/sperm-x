function plotWallApproach()

% plots figures relating to the "nonplanar cell approaches wall" section of
% my thesis

printLinebreak
close all;
calS = 12; % hardcode

%% setup directories

if ~exist('./spx_twist_bend/figures/','dir')
    mkdir('./spx_twist_bend/figures')
end

%% load data
% preprocessed in preprocTwistBendData.m
% variables loaded are sorted by simulation number, so require additional
% sorting on a per plot basis depending what we want to plot
%   - X_vals:       flagellum coordinates
%   - y_vals:       head coordinates (traction discretisation)
%   - t_vals:       time point values
%   - vars:         identifies variables changed in each simulation
%   - fail_ids:     indices of simulations in vars that failed
%   - params:       contains constant parameters for SPX
%   - options:      contains constant options for SPX
%   - int:          contains model initial conditions
%   - VAL:          velocity along a line, for each beat simulated
%   - eff:          Lighthill efficiency, for each beat simulated
%   - dNP:          range of 'nonplanar' movement

% in vars, the variables are ordered as follows
%   1 - calS
%   2 - Phi_k
%   3 - rho (called om in spxTwistBendParameterSweep.m)
%   4 - k (wave number)

fprintf('Loading data from ./spx_twist_bend/preproc/wallApproach_calS=%02g.mat... ', calS)
load(sprintf('./spx_twist_bend/preproc/wallApproach_calS=%02g.mat', calS))
fprintf('complete!\n')

%% identify failed simulations

% variables used in failed simulations
fail_vars = vars(:,fail_ids);

% turn fail_ids into logical
for i = 1:size(vars,2)
    if any(i==fail_ids)
        fids(i) = true;
    else
        fids(i) = false;
    end
end

% remove Phi = pi/4 simulations (one failed.. for consistency remove all)
idr = find(vars(2,:)==pi/4);
for i = 1:length(idr)
    fids(idr(i)) = true;
end

% drop from all data
d1_vals = d1_vals(~fids);
d2_vals = d2_vals(~fids);
d3_vals = d3_vals(~fids);
d10_vals = d10_vals(~fids);
d20_vals = d20_vals(~fids);
d30_vals = d30_vals(~fids);
rollTh = rollTh(~fids,:);
rollThFull = rollThFull(~fids,:);
rollRate = rollRate(~fids);
rollRateFull = rollRateFull(~fids);
rollIdx = rollIdx(~fids);
numRoll = numRoll(~fids);
thXY = th_xy(~fids,:);
thXZ = th_xz(~fids,:);
rthXY = rth_xy(~fids);
rthXZ = rth_xz(~fids);
dtraj = dtraj(~fids);
dtraj_full = dtraj_full(~fids);
eff = eff(~fids,:);
ptraj = ptraj(~fids);
ptraj_full = ptraj_full(~fids);
VAL = VAL(~fids,:);
vars = vars(:,~fids);
X_vals = X_vals(~fids);
y_vals = y_vals(~fids);

outs = outs(~fids);
params = params(~fids);
options = options(~fids);

% number of happy simulations
num_sims = size(vars,2);

%% recover variables varied over simulations

Phi_vals  = unique(vars(2,:));
Gamd_vals = unique(vars(3,:));

num_Phi   = length(Phi_vals);
num_Gamd  = length(Gamd_vals);

%% set figure axes fonts

% latex and font size for tick labels
set(groot, 'defaultAxesTickLabelInterpreter','latex');
set(groot, 'defaultAxesFontSize',14);

% define marker styles to use
mkrs = {'s','d','o','^','>'};

% dpi for figures
dpi = 300;

% vector of two pi increments for axis
num_tpi = 80;
for i = 1:num_tpi
    twopivec(i) = (i-1)*2*pi;
    if (i==1)
        twopistr(i) = "0";
    else
        twopistr(i) = sprintf("$%g \\pi$", (i-1)*2);
    end
end

%% deflection angles on wall approach

% fix k=4*pi (seemed to be good in parameter sweep), choose three
% stiffnesses (10^3,10^4,10^5)

% find indexes of results we're interested in 
idP    = find(vars(2,:)>=0);
idG{1} = find(vars(3,:)==Gamd_vals(1));
idG{2} = find(vars(3,:)==Gamd_vals(2));
idG{3} = find(vars(3,:)==Gamd_vals(3));

% colors for lines
cols = parula(10);

% sample from 2nd beat to penultimate beat (removes noise from
% initialisation and moving mean)
aa = 3*params{1}.NumSlices;
bb = (params{1}.NumBeats-1)*params{1}.NumSlices;

for k = 1:2
    f{k} = figure;
    tiledlayout('flow','TileSpacing','compact')
    for i = 1:length(idG)
        idi = intersect(idP, idG{i});
        if ~isempty(idi)
            % select data
            thXYi = thXY(idi,aa:bb);
            thXZi = thXZ(idi,aa:bb);
            rollThi = rollThFull(idi,aa:bb);
    
            % plot angles
            for ii = 1:size(thXYi,1)
                nexttile; box on;
                if k==1
                    xi = t_vals{i}(aa:bb);
                else
                    N = params{1}.NF;
                    xi = X_vals{idi(ii)}(2*N+1,aa:bb);
                    xi = smooth(xi, 200); % smooth trajectory
                end 
    
                % right yaxis (roll angle)
                yyaxis right
                plot(xi, rollThi(ii,:), 'LineWidth',0.6);
                if (ii==3)
                    ylabel('roll angle')
                end
                yticks([-pi,0,pi])
                ylim([-pi,pi])
                yticklabels({'$-\pi$','0','$\pi$'})
    
                % left yaxis (lab angles)
                yyaxis left
                [up1,lo1] = envelope(thXYi(ii,:), 100,'peak');
                [up2,lo2] = envelope(thXZi(ii,:), 100,'peak');
                plot(xi, up1, '-', 'LineWidth',1.2, 'Color',cols(2,:)); hold on
                plot(xi, lo1, '-', 'LineWidth',1.2, 'Color',cols(2,:)); 
                plot(xi, up2, '-', 'LineWidth',1.2, 'Color',cols(4,:));
                plot(xi, lo2, '-', 'LineWidth',1.2, 'Color',cols(4,:));
                if (ii==1)
                    ylabel('lab angles')
                end
                ylim([-pi/2-0.5 pi/2+0.5])
                yticks([-pi/2,0,pi/2])
                yticklabels({'$-\pi/2$','0','$\pi/2$'})

                if (k==1)
                    if (i==3)
                        xlabel('$t$', 'FontSize',14,'Interpreter','latex');
                    end
                    int = 10;
                    xlim([4*pi,80*pi]);
                    xticks(twopivec(1:int:end));
                    xticklabels(twopistr(1:int:end));
                else
                    if (i==3)
                        xlabel('$h$', 'FontSize',14,'Interpreter','latex');
                    end
                    xlim([min(xi),max(xi)])
                    set(gca, 'XDir','reverse')
                    % ax = gca; ax.XAxis.Exponent = -2;
                end
            end
        end
    end

    % scale and save
    set(gcf, 'Position',[560 453 581 494])
    if k==1
        str = 'angles time';
    else
        str = 'angles height';
    end
    save2pdf(['./spx_twist_bend/figures/approaching wall/',str],f{k},dpi)
    close all

end

%% effect of wall on rotation rate 

% fix k=4*pi (seemed to be good in parameter sweep), choose three
% stiffnesses (10^4,10^5), examine traj and waveform upon approaching
% wall

% find indexes of results we're interested in 
idP    = find(vars(2,:)>=0);
idG{1} = find(vars(3,:)==Gamd_vals(1));
idG{2} = find(vars(3,:)==Gamd_vals(2));
idG{3} = find(vars(3,:)==Gamd_vals(3));

% sample from 2nd beat to penultimate beat (removes noise from
% initialisation and moving mean)
aa = 2*params{1}.NumSlices;
bb = (params{1}.NumBeats-1)*params{1}.NumSlices;

%{
% plots: height above wall against roll angle
f1 = figure;
tiledlayout('flow', 'TileSpacing','compact')
for g = 1:length(idG)
    idi = intersect(idP, idG{g});
    nexttile; hold on; box on;
    for ii = 1:length(idi)

        % select data
        Xi = X_vals{idi(ii)}(:,aa:bb); N = size(Xi,1)/3;
        hi = Xi(2*N+1,:); % perpendicular height of X0 above z=0
        xi = t_vals{i}(aa:bb);

        yi = cumsum(rollThFull(idi(ii), aa:bb));

        % plot
        plot(xi,yi, 'LineWidth',1.2, 'Color',cols(ii,:)); hold on; grid on
        % plot(movmean(hi,params{idi(ii)}.NumSlices), Thii, 'LineWidth',1.4, 'Color',cols(ii,:))

        % axes
%         xlabel('$h$', 'FontSize',14, 'Interpreter','latex')
%         if g == 1
%             yticks(twopivec)
%             yticklabels(twopistr)
%             ylabel('$\sum\theta_{\textup{rev}}$', 'FontSize',14, 'Interpreter','latex')
%         else
%             int = 4;
%             yticks(twopivec(1:int:end))
%             yticklabels(twopistr(1:int:end)) % skip some ticks, too many!
%         end

        % axes
        %xlim([0,0.15])
        %set(gca,'XDir','reverse')
    end
end

% scale and save
set(gcf, 'Position',[778 749 545 198])
save2pdf('./spx_twist_bend/figures/approaching wall/height cumroll',f1,dpi)
close all
%}

cols = lines(3);

% average rates of revolution, input manually from plotTwistBend wall_avrot
inf_rr(2) = 0.5790; % for Gamma_d=3.2e3
inf_rr(3) = 0.6620; % for Gamma_d=1e4

% plots: revolution index against revolution rate (for rolling cells)
f2 = figure;
tiledlayout('flow', 'TileSpacing','compact')
for g = 2:length(idG)
    idi = intersect(idP, idG{g});
    nexttile; hold on; box on;
    for ii = 1:length(idi)

        % select data
        rri = rollRateFull{idi(ii)}(1:end-1); % absolute rolling rate

        % compute relative percentage difference to cell in infinite fluid
        rel_rri{g,ii} = (rri-inf_rr(g))/inf_rr(g) * 100;

        % plot
        % sz = normalize(-rel_rri{g,ii},'range',[10,75]);
        sz = 50;
        scatter(1:1:length(rri), rri, sz,'filled','Marker',mkrs{ii}, ...
           'Color',cols(ii,:)); hold on

        %sz = 75;
        %scatter(1:1:length(rri), rri, sz, 'filled','CData',rel_rri, 'Marker',mkrs{ii});
        %hold on;
        %colormap(tricolor2); 

        % labels
        xlabel('Rev. no.','FontSize',14 ,'Interpreter','latex')
        if (g==2)
            ylabel('$\omega_{\textup{rev}}$','FontSize',14,'Interpreter','latex')
        end

        % axes
        ax = gca; ax.YAxis.Exponent = -1;   
        ylim([0.3 0.8])

    end
end

% scale and save
set(gcf, 'Position',[560 789 486 158])
save2pdf('./spx_twist_bend/figures/approaching wall/roll rates',f2,dpi)
% close all

% plots: rev index % diff against rot rate
f4 = figure; 
tiledlayout('flow','TileSpacing','compact')
for g = 2:3
    idi = intersect(idP, idG{g});
    nexttile; box on;
    for ii = 1:length(idi)

        % plot
        rri = rollRateFull{idi(ii)}(1:end-1); % absolute rolling rate
        rel_rri = (rri-inf_rr(g))/inf_rr(g) * 100;
        x1 = 1:length(rel_rri);
        plot(x1,rel_rri, '.-', 'LineWidth',1.2, 'Color',cols(ii,:)); 
        hold on; 

        % axes
        ylim([-50 25])

        % labels
        xlabel('Rev. no.','FontSize',14 ,'Interpreter','latex')
        if (g==2)
           ylabel('\% $\omega_{\textup{rev}}$ diff.', 'FontSize',14, 'Interpreter','latex') 
        end
    end
end

% scale and save
set(gcf, 'Position',[560 789 486 158])
save2pdf('./spx_twist_bend/figures/approaching wall/rol rate diff num',f4,dpi)
close all

% plots: smoothed trajectory against % rot rate diff
f3 = figure; 
tiledlayout('flow','TileSpacing','compact')
for g = 2:3
    idi = intersect(idP, idG{g});
    nexttile; box on;
    for ii = 1:length(idi)

        % plot
        rri = rollRateFull{idi(ii)}(1:end-1); % absolute rolling rate
        rel_rri = (rri-inf_rr(g))/inf_rr(g) * 100;
        xi = X_vals{idi(ii)}(2*N+1,rollIdx{idi(ii)});
        xi = smooth(xi, 200); % smooth trajectory
        plot(xi(2:end-1),rel_rri, '-', 'LineWidth',1.2, 'Color',cols(ii,:), 'Marker',mkrs{ii}); 
        %semilogx(xi(2:end-1),rel_rri, '-', 'LineWidth',1.2, 'Color',cols(ii,:), 'Marker',mkrs{ii}); 
        hold on; 

        % axes
        ylim([-50 25])
        set(gca, 'XDir','reverse')

        % labels
        xlabel('$h$','FontSize',14 ,'Interpreter','latex')
        if (g==2)
           ylabel('\% $\omega_{\textup{rev}}$ diff.', 'FontSize',14, 'Interpreter','latex') 
        end
    end
end

% scale and save
set(gcf, 'Position',[560 789 486 158])
save2pdf('./spx_twist_bend/figures/approaching wall/rol rate diff height',f3,dpi)
close all

%% waveforms and trajectory upon approaching wall

% fix k=4*pi (seemed to be good in parameter sweep), choose three
% stiffnesses (10^3,10^4,10^5), examine traj and waveform upon approaching
% wall

% find indexes of results we're interested in 
idP    = find(vars(2,:)>=0);
idG{1} = find(vars(3,:)==Gamd_vals(1));
idG{2} = find(vars(3,:)==Gamd_vals(2));
idG{3} = find(vars(3,:)==Gamd_vals(3));

% colors for lines
cols = lines(num_Phi);

% plot trajectories ======================================================
for i = 1:length(idG)
    idi = intersect(idP, idG{i});
    if ~isempty(idi) 
        % find data
        ptraji  = ptraj_full(idi);
        dtraji  = dtraj_full(idi);
        Xi      = X_vals(idi);
        yi      = y_vals(idi);
        d10i    = d10_vals(idi);
        d20i    = d20_vals(idi);
        d30i    = d30_vals(idi);
        d1i     = d1_vals(idi);
        d2i     = d2_vals(idi);
        d3i     = d3_vals(idi);
        num_lin = length(ptraji);
    
        % plot overlaid trajectories -------------------------------------
        f1 = figure; hold on;
        for j = 1:num_lin
            % trajectory
            trp{j} = plot3(ptraji{j}(1,:), ptraji{j}(2,:), ptraji{j}(3,:), ...
                'LineWidth',1.4, 'Color',cols(j,:)); hold on

            % xz projection
            Ones = ones(size(ptraji{j}(1,:)));
            plot3(ptraji{j}(1,:), 0.05*Ones, ptraji{j}(3,:), ':', ...
                'LineWidth',1.2, 'Color',[cols(j,:),0.4])

            % xy projection
            plot3(ptraji{j}(1,:), ptraji{j}(2,:), 0*Ones, ':', ...
                'LineWidth',1.2, 'Color',[cols(j,:),0.4])

            % start/end points
            scatter3(ptraji{j}(1,1),ptraji{j}(2,1),ptraji{j}(3,1), 60, ...
                'filled', 'o', 'MarkerFaceColor',cols(j,:))
            scatter3(ptraji{j}(1,end),ptraji{j}(2,end),ptraji{j}(3,end), 120, ...
                'filled', 's', 'MarkerEdgeColor',cols(j,:), 'MarkerFaceColor',cols(j,:))

            % shade in z=0 plane
            patch([-0.6 0.3 0.3 -0.6], [-0.15 -0.15 0.05 0.05], [0 0 0 0], ...
                'k', 'FaceAlpha', 0.08)

            % axes
            setXYZLabels
            view(3); grid on; box on; axis equal
            xlim([-0.6 0.3]); ylim([-0.15 0.05]); zlim([0 0.15])
        end

        % legend
        lgd = legend([trp{1},trp{2},trp{3}],{'$\pi/16$','$\pi/8$','$3\pi/16$'}, 'FontSize',14', ...
            'Interpreter','latex', 'EdgeColor','none');
        lgd.Location = 'none';
        lgd.Position = [0.6899 0.2407 0.1384 0.1319];

        % save
        save_str = sprintf('./spx_twist_bend/figures/approaching wall/traj Gid=%g',i);
        save2pdf(save_str, f1, dpi);

        % plot cell waveforms --------------------------------------------
        for j = 1:num_lin
            % plot between beats a and b, at intervals int
            a = 3; b = params{1}.NumBeats; int = 20;
            aa = (a-1)*params{1}.NumSlices+1; bb = b*params{1}.NumSlices;
            Xii = Xi{j}(:,aa:int:bb);
            yii = yi{j}(:,aa:int:bb);
            d10ii = d10i{j}(:,aa:int:bb);
            d20ii = d20i{j}(:,aa:int:bb);
            d30ii = d30i{j}(:,aa:int:bb);
            ptjii = ptraji{j}(:,aa:int:bb);

            % draw clamped waveform --------------------------------------
            f1 = figure; 
            plotRelativeTraceSPX(Xii, yii, d10ii, true, true, true, false, ...
                false, d20ii, d30ii);
            save_str = sprintf('./spx_twist_bend/figures/approaching wall/clamped Gid=%g Pid=%g xyz',i,j);
            save2pdf(save_str, f1, dpi);

%             f2 = figure; 
%             plotRelativeTraceSPX(Xii, yii, d10ii, true, true, true, false, ...
%                 false, d20ii, d30ii);
%             view([90,0]);     % yz plane 
%             ylim([-0.2 0.2]); zlim([-0.2 0.2])
%             set(gcf, 'Position',[413 726 395 171])
%             save_str = sprintf('./spx_twist_bend/figures/approaching wall/clamped Gid=%g Pid=%g yz',i,j);
%             save2pdf(save_str, f2, dpi);

            f3 = figure; 
            plotRelativeTraceSPX(Xii, yii, d10ii, true, true, true, false, ...
                false, d20ii, d30ii);
            view(2);         % xy plane
            xlim([-0.2 1.0]); ylim([-0.3 0.3])
            set(gcf, 'Position',[247 508 439 138])
            save_str = sprintf('./spx_twist_bend/figures/approaching wall/clamped Gid=%g Pid=%g xy',i,j);
            save2pdf(save_str, f3, dpi);

            f4 = figure; 
            plotRelativeTraceSPX(Xii, yii, d10ii, true, true, true, false, ...
                false, d20ii, d30ii);
            view([0,-1,0]);   % xz plane
            xlim([-0.2 1.0]); zlim([-0.3 0.3])
            set(gcf, 'Position',[247 508 439 138])
            save_str = sprintf('./spx_twist_bend/figures/approaching wall/clamped Gid=%g Pid=%g xz',i,j);
            save2pdf(save_str, f4, dpi);

            % reset
            close all

            % draw pinned waveform ---------------------------------------
            f1 = figure;
            plotRelativeTraceSPX(Xii, yii, d10ii, false, true, true, false, ...
                true, ptjii);
            xlim([-0.2 1.0]); ylim([-0.15 0.15]); zlim([-0.1 0.5])
            set(gcf, 'Position',[608 708 512 239])
            save_str = sprintf('./spx_twist_bend/figures/approaching wall/pinned Gid=%g Pid=%g xyz',i,j);
            save2pdf(save_str, f1, dpi);

%             f2 = figure;
%             plotRelativeTraceSPX(Xii, yii, d10ii, false, true, true, false, ...
%                 true, ptjii);
%             view([90,0]);     % yz plane 
%             ylim([-0.2 0.2]); zlim([-0.2 0.2])
%             set(gcf, 'Position',[413 726 395 171])
%             save_str = sprintf('./spx_twist_bend/figures/approaching wall/pinned Gid=%g Pid=%g yz',i,j);
%             save2pdf(save_str, f2, dpi);

            f3 = figure;
            plotRelativeTraceSPX(Xii, yii, d10ii, false, true, true, false, ...
                false);
            view(2);         % xy plane
            xlim([-0.2 1.0]); ylim([-0.1 0.5])
            set(gcf, 'Position',[247 508 439 138])
            save_str = sprintf('./spx_twist_bend/figures/approaching wall/pinned Gid=%g Pid=%g xy',i,j);
            save2pdf(save_str, f3, dpi);

            f4 = figure;
            plotRelativeTraceSPX(Xii, yii, d10ii, false, true, true, false, ...
                false);
            view([0,-1,0]);   % xz plane
            xlim([-0.2 1.0]); zlim([-0.1 0.5])
            set(gcf, 'Position',[247 508 439 138])
            save_str = sprintf('./spx_twist_bend/figures/approaching wall/pinned Gid=%g Pid=%g xz',i,j);
            save2pdf(save_str, f4, dpi);

            % reset
            close all
        end

        % plot dominant planes of beating --------------------------------
        for j = 1:num_lin
            % plot between beats a and b, at intervals int
            a = 3; b = params{1}.NumBeats; int = 41;
            aa = (a-1)*params{1}.NumSlices+1; bb = b*params{1}.NumSlices;
            Xii = Xi{j}(:,aa:int:bb);
            yii = yi{j}(:,aa:int:bb);
            d1ii = d1i{j}(:,aa:int:bb);
            d2ii = d2i{j}(:,aa:int:bb);
            d3ii = d3i{j}(:,aa:int:bb);
            ptjii = ptraji{j}(:,aa:int:bb);

            % calculate dominant dir and lab beat directions
            [V,lab_dom,dir_dom] = calcDominantBeatDirs(Xii, d1ii,d2ii,d3ii);

        end
    end
end

close all;
printLinebreak;

end % function
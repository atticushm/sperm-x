function plotCellOverWall()

% plots figures relating to the "nonplanar cell approaches wall" section of
% my thesis

printLinebreak
close all;

%% setup directories

if ~exist('./spx_twist_bend/figures/','dir')
    mkdir('./spx_twist_bend/figures')
end

%% load data
% preprocessed in preprocTwistBendData.m
% variables loaded are sorted by simulation number, so require additional
% sorting on a per plot basis depending what we want to plot
%   - X_vals:       flagellum coordinates
%   - y_vals:       head coordinates (traction discretisation)
%   - t_vals:       time point values
%   - vars:         identifies variables changed in each simulation
%   - fail_ids:     indices of simulations in vars that failed
%   - params:       contains constant parameters for SPX
%   - options:      contains constant options for SPX
%   - int:          contains model initial conditions
%   - VAL:          velocity along a line, for each beat simulated
%   - eff:          Lighthill efficiency, for each beat simulated
%   - dNP:          range of 'nonplanar' movement

% in vars, the variables are ordered as follows
%   1 - calS
%   2 - Phi_k
%   3 - rho (called om in spxTwistBendParameterSweep.m)
%   4 - k (wave number)

calS = 12;
fprintf('Loading data from ./spx_twist_bend/preproc/wallOver_calS=%02g.mat... ', calS)
load(sprintf('./spx_twist_bend/preproc/wallOver_calS=%02g.mat', calS))
fprintf('complete!\n')

%% recover variables varied over simulations

calS_vals = unique(vars(1,:));
h_vals    = unique(vars(2,:));

num_calS  = length(calS_vals);
num_h     = length(h_vals);

% number of happy simulations
num_sims = size(vars,2);

%% some additional computation

% determine time varying d10, d20, d30 (proximal/head frames)
N = size(X_vals{1},1)/3;
for i = 1:num_sims
    if ~isempty(d1_vals{i})
        d10_vals{i} = [d1_vals{i}(1,:); d1_vals{i}(N+1,:); d1_vals{i}(2*N+1,:)]; 
        d20_vals{i} = [d2_vals{i}(1,:); d2_vals{i}(N+1,:); d2_vals{i}(2*N+1,:)]; 
        d30_vals{i} = [d3_vals{i}(1,:); d3_vals{i}(N+1,:); d3_vals{i}(2*N+1,:)]; 
    end
end

%% set figure axes fonts

% latex and font size for tick labels
set(groot, 'defaultAxesTickLabelInterpreter','latex');
set(groot, 'defaultAxesFontSize',14);

% define marker styles to use
mkrs = {'s','d','o','^','>'};

% dpi for figures
dpi = 300;

%% set some global plotting options

% number of beat at which to start sampling solutions
% in previous plots, nv=4 seems to be sufficient to have established
% nonplanar beating
start = 4;
fin = 15;
aa = start*params{1}.NumSlices+1;
bb = fin*params{1}.NumSlices;

% interval at which to sample solutions -- 20 slices * 30 beats it too many
% lines in a single figure!
pint = 2;

%% 'control' simulation i.e. swimming with no wall

% find those simulations where blakelets were not enabled
idb = find(vars(3,:)==0);

% plot cell ==============================================================
% variation in initial condition will not yield difference in beat, so only
% plot one solution
i = 1;
% select solution data
Xi = X_vals{idb(i)}(:, aa:pint:bb);
yi = y_vals{idb(i)}(:, aa:pint:bb);
d10i = d10_vals{idb(i)}(:, aa:pint:bb);
d20i = d20_vals{idb(i)}(:, aa:pint:bb);
d30i = d30_vals{idb(i)}(:, aa:pint:bb);
ptji = ptraj_full{idb(i)}(:, aa:pint:bb);

% pinned cell ---------------------------------------------------------
f1 = figure(1); plotRelativeTraceSPX(Xi, yi, d10i, false, true, ...
    true, false, true, ptji);
xlim([-0.2 1.0]); ylim([-0.15 0.15]); zlim([-0.2 0.2])
set(gcf, 'Position',[608 708 512 239])
save_str = './spx_twist_bend/figures/wall control pinned xyz';
save2pdf(save_str, f1, 300)

f2 = figure(2); plotRelativeTraceSPX(Xi, yi, d10i, false, true, ...
    false, true, false); 
view([90,0]);     % yz plane 
ylim([-0.2 0.2]); zlim([-0.2 0.2])
set(gcf, 'Position',[413 726 395 171])
save_str = './spx_twist_bend/figures/wall control pinned yz';
save2pdf(save_str, f2, 300)

f3 = figure(3); plotRelativeTraceSPX(Xi, yi, d10i, false, true, ...
    true, false, false); 
view(2);         % xy plane
xlim([-0.2 1.0]); ylim([-0.2 0.2])
set(gcf, 'Position',[247 508 439 138])
save_str = './spx_twist_bend/figures/wall control pinned xy';
save2pdf(save_str, f3, 300)

f4 = figure(4); plotRelativeTraceSPX(Xi, yi, d10i, false, true, ...
    true, false, false); 
view([0,-1,0]);   % xz plane
xlim([-0.2 1.0]); zlim([-0.2 0.2])
set(gcf, 'Position',[247 508 439 138])
save_str = './spx_twist_bend/figures/wall control pinned xz';
save2pdf(save_str, f4, 300)

close all

% clamped cell --------------------------------------------------------
f1 = figure(1); plotRelativeTraceSPX(Xi, yi, d10i, true, true, ...
    true, false, false, d20i, d30i);
xlim([-0.2 1.0]); ylim([-0.15 0.15]); zlim([-0.2 0.2])
set(gcf, 'Position',[608 708 512 239])
save_str = './spx_twist_bend/figures/wall control clamped xyz';
save2pdf(save_str, f1, 300)

f2 = figure(2); plotRelativeTraceSPX(Xi, yi, d10i, true, true, ...
    false, true, false, d20i, d30i); 
view([90,0]);     % yz plane 
ylim([-0.2 0.2]); zlim([-0.2 0.2])
set(gcf, 'Position',[413 726 395 171])
save_str = './spx_twist_bend/figures/wall control clamped yz';
save2pdf(save_str, f2, 300)

f3 = figure(3); plotRelativeTraceSPX(Xi, yi, d10i, true, true, ...
    true, false, false, d20i, d30i); 
view(2);         % xy plane
xlim([-0.2 1.0]); ylim([-0.2 0.2])
set(gcf, 'Position',[247 508 439 138])
save_str = './spx_twist_bend/figures/wall control clamped xy';
save2pdf(save_str, f3, 300)

f4 = figure(4); plotRelativeTraceSPX(Xi, yi, d10i, true, true, ...
    true, false, false, d20i, d30i); 
view([0,-1,0]);   % xz plane
xlim([-0.2 1.0]); zlim([-0.2 0.2])
set(gcf, 'Position',[247 508 439 138])
save_str = './spx_twist_bend/figures/wall control clamped xz';
save2pdf(save_str, f4, 300)

close all

%% waveforms for cells swimming at different heights

for i = 1:num_h
    % select solution data
    Xi = X_vals{i}(:, aa:pint:bb);
    yi = y_vals{i}(:, aa:pint:bb);
    d10i = d10_vals{i}(:, aa:pint:bb);
    d20i = d20_vals{i}(:, aa:pint:bb);
    d30i = d30_vals{i}(:, aa:pint:bb);
    ptji = ptraj_full{i}(:, aa:pint:bb);
    
    % pinned cell ---------------------------------------------------------
    f1 = figure(1); plotRelativeTraceSPX(Xi, yi, d10i, false, true, ...
        true, false, true, ptji);
    xlim([-0.2 1.0]); ylim([-0.15 0.15]); zlim([-0.2 0.2])
    set(gcf, 'Position',[608 708 512 239])
    save_str = sprintf('./spx_twist_bend/figures/wall pinned xyz hid=%g',i);
    save2pdf(save_str, f1, 300)
    
    f2 = figure(2); plotRelativeTraceSPX(Xi, yi, d10i, false, true, ...
        false, true, false); 
    view([90,0]);     % yz plane 
    ylim([-0.2 0.2]); zlim([-0.2 0.2])
    set(gcf, 'Position',[413 726 395 171])
    save_str = sprintf('./spx_twist_bend/figures/wall pinned yz hid=%g',i);
    save2pdf(save_str, f2, 300)
    
    f3 = figure(3); plotRelativeTraceSPX(Xi, yi, d10i, false, true, ...
        true, false, false); 
    view(2);         % xy plane
    xlim([-0.2 1.0]); ylim([-0.2 0.2])
    set(gcf, 'Position',[247 508 439 138])
    save_str = sprintf('./spx_twist_bend/figures/wall pinned xy hid=%g',i);
    save2pdf(save_str, f3, 300)
    
    f4 = figure(4); plotRelativeTraceSPX(Xi, yi, d10i, false, true, ...
        true, false, false); 
    view([0,-1,0]);   % xz plane
    xlim([-0.2 1.0]); zlim([-0.2 0.2])
    set(gcf, 'Position',[247 508 439 138])
    save_str = sprintf('./spx_twist_bend/figures/wall pinned xz hid=%g',i);
    save2pdf(save_str, f4, 300)
    
    close all
    
    % clamped cell --------------------------------------------------------
    f1 = figure(1); plotRelativeTraceSPX(Xi, yi, d10i, true, true, ...
        true, false, false, d20i, d30i);
    xlim([-0.2 1.0]); ylim([-0.15 0.15]); zlim([-0.2 0.2])
    set(gcf, 'Position',[608 708 512 239])
    save_str = sprintf('./spx_twist_bend/figures/wall clamped xyz hid=%g',i);
    save2pdf(save_str, f1, 300)
    
    f2 = figure(2); plotRelativeTraceSPX(Xi, yi, d10i, true, true, ...
        false, true, false, d20i, d30i); 
    view([90,0]);     % yz plane 
    ylim([-0.2 0.2]); zlim([-0.2 0.2])
    set(gcf, 'Position',[413 726 395 171])
    save_str = sprintf('./spx_twist_bend/figures/wall clamped yz hid=%g',i);
    save2pdf(save_str, f2, 300)
    
    f3 = figure(3); plotRelativeTraceSPX(Xi, yi, d10i, true, true, ...
        true, false, false, d20i, d30i); 
    view(2);         % xy plane
    xlim([-0.2 1.0]); ylim([-0.2 0.2])
    set(gcf, 'Position',[247 508 439 138])
    save_str = sprintf('./spx_twist_bend/figures/wall clamped xy hid=%g',i);
    save2pdf(save_str, f3, 300)
    
    f4 = figure(4); plotRelativeTraceSPX(Xi, yi, d10i, true, true, ...
        true, false, false, d20i, d30i); 
    view([0,-1,0]);   % xz plane
    xlim([-0.2 1.0]); zlim([-0.2 0.2])
    set(gcf, 'Position',[247 508 439 138])
    save_str = sprintf('./spx_twist_bend/figures/wall clamped xz hid=%g',i);
    save2pdf(save_str, f4, 300)
    
    close all
end

%% finish

close all;
printLinebreak

end % function
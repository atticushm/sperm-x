function spxFindPlanarOpt()

% function for determining the optimal calM2 value for swimming speed, for
% a given calS and ell value, for 'trig' actuation and 'smthsquare'
% actuation

% check in correct directory (bluebear call will start in subdir, so we 
% need to move up)
dir = pwd;
if strcmpi(dir(end-9:end),'spx_planar')
    cd ..
end
addFilesToPath;
printLinebreak

% add code to path
addFilesToPath()

% folder for data
if ~exist('./spx_planar/data','dir')
    mkdir('./spx_planar/data')
end

% choose parameters
calS_vals = [9,12,15,18];
ell  = 0.05;

% set bounds for calM2
bds = [1.0, 3.0] * 1e-2;

for i = 1:length(calS_vals)
    
    % choose calS
    calSi = calS_vals(i);

    % set SPX options -- calM2 and ActModel will be overwritten inside
    % runSPX... anonymous function
    options = setOptions('SwimmingCell', 'Species','human', 'BodyType','sunanda', ...
        'NonLocal',true, 'MechTwist',false, 'StiffModel','varying', 'SilentMode',true, ...
        'PlaneOfBeat','xy', 'SaveVideo',false, 'ActModel','trig', 'EndpieceModel','heaviside');
    params = setParameters(options, 'ittol',1e-4, 'NF',251, 'dt',10e-2, 'q',0.1, ...
        'calS',calSi, 'calM1',0, 'calM2',0, 'calM3',0, 'Gam_s',1, 'Gam_d',10^4, ...
        'wavk',4*pi, 'ell',ell, 'epsilon',0.01, 'NumBeats',4, 'WarmUp',1, ...
        'tmax','auto', 'NumSave','auto');
    int = setInitialCondition(options, params, 'IntCond','line', 'X0',[0;0;0]);

    % run optimisation algorithm
    % options for fminbnd
    foptions = optimset('Display','iter');

    % set functions
    func_trig = @(x) runSPXforPlanOptVAL(x, 'trig', params, options, int);
    func_sqre = @(x) runSPXforPlanOptVAL(x, 'smthsquare', params, options, int);

    % initial guesses from calM2 bounds
    exit = 'fail';
    lb = bds(1);    
    ub = bds(2);

    % trig actuation
    while strcmpi(exit, 'fail')
        try
            opt = fminbnd(func_trig, lb, ub, foptions);
            exit = 'ok';

        catch   % in case of failure
            fprintf('Reducing upper bound...\n')
            ub = ub-0.0025;
            exit = 'fail';

        end
    end
    calM2_opt_trig = opt;

    % smoothed squarewave actuation
    exit = 'fail';  % reset
    while strcmpi(exit, 'fail')
        try
            opt = fminbnd(func_sqre, lb, ub, foptions);
            exit = 'ok';

        catch   % in case of failure
            fprintf('Reducing upper bound...\n')
            ub = ub-0.0025;
            exit = 'fail';

        end
    end
    calM2_opt_sqre = opt;

    % save
    save_str = sprintf('./spx_planar/data/planarOpts_calS_%02g_ell=%g.mat', calSi, ell);
    parsave(save_str, calSi, ell, calM2_opt_trig, calM2_opt_sqre)

end % function

fprintf('Complete!\n');
printLinebreak
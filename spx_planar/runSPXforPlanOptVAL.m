function iVAL = runSPXforPlanOptVAL(calM2, actModel, params, options, int)

% function, to be called by SPXFINDPLANAROPT, which runs an SPX simulation
% defined by inputs params, options, int and calM2, and returns the inverse
% VAL value, to be minimised by a function such as fminbnd

%% run SPX

% overwrite options/parameters
options.ActModel = actModel;
params.calM2 = calM2;

% run SPX
outs = runSPX(int, params, options);

%% compute VAL

% time values
tps = [outs.t];

% coordinates at time values
X = [outs.X];

% compute VAL over final beat
VAL = calculateVAL(X, tps, 'spx', params);

% inverse VAL (for minimisation algorithm)
iVAL = 1/VAL;

end % function
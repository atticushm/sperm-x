function spxPlanarActuationModelComparison()

% function for running varying simulations to compare actuation models for
% a planar beating sperm cell.

% check in correct directory (bluebear call will start in subdir, so we 
% need to move up)
dir = pwd;
if strcmpi(dir(end-9:end),'spx_planar')
    cd ..
end
addFilesToPath;
printLinebreak

% add code to path
addFilesToPath()

% folder for data
if ~exist('./spx_planar/data','dir')
    mkdir('./spx_planar/data')
end

% define values to vary across simulations
calS_vals = [9,12,15,18];
calM_vals = 0.01:0.001:0.017;
actmod_vals = [1,2];

% variables for simulations
vars = combvec(calS_vals, calM_vals, actmod_vals);
num_sims = size(vars, 2);

% run simulations for each (calS,calM) and actuation model triad
parfor k = 1:num_sims
    
    % extract variables
    calSk = vars(1,k);
    calMk = vars(2,k);
    if vars(3,k)==1
        actk = 'trig';
    elseif vars(3,k)==2
        actk = 'smthsquare';
    end

    % set options
    options = setOptions('SwimmingCell', 'Species','human', 'BodyType','sunanda', ...
        'NonLocal',true, 'MechTwist',false, 'StiffModel','varying', 'SilentMode',true, ...
        'PlaneOfBeat','xz', 'SaveVideo',false, 'ActModel',actk, 'EndpieceModel','heaviside');

    % set parameters
    params = setParameters(options, 'ittol',1e-4, 'NF',251, 'dt',5e-2, 'q',0.1, ...
        'calS',calSk, 'calM1',0, 'calM2',calMk, 'calM3',0, 'Gam_s',1, 'Gam_d',14^(2.4), ...
        'wavk',4*pi, 'ell',0.05, 'epsilon',0.01, 'NumBeats',6, 'WarmUp',1, ...
        'tmax','auto', 'NumSave','auto');

    % set initial condition
    int = setInitialCondition(options, params, 'IntCond','line', 'X0',[0;0;0]);

    try
        % run simulation
        outs = runSPX(int, params, options);
    catch
        % if simulation fails (likely due to selfintersection), set outs to empty
        outs = [];
    end

    % save data
    save_str = sprintf('./spx_planar/data/actComp_%02g_of_%02g.mat',k,num_sims);
    parsave(save_str, outs, {calSk,calMk,actk});

end

end % function
# Sperm-X

## Introduction

This repository contains all MATLAB code associated with the **Sperm-X** (or *SPX*) method, developed and introduced by Atticus L. Hall-McNair (me) as part of his/my PhD thesis.

The code is designed to simulate human sperm cells swimming in three dimensions, actuated by active moments per unit length acting about the local orthonormal frame at each point. The method couples a Kirchhoff rod model for a 3D elastic flagellum (including bend and twist) to a nonlocal fluid model in the method of regularised stokeslets. The code has options for a variety of cell head shapes, and can replace stokeslets with blakelets to simulate cells swimming over a plane wall.

## Basic setup

To start I recommend having a look at the script ``spxControlPanel.m``. For one-off simulations, this should be the only file a user needs to interface with -- simply set your options and parameters, and run the script. This script explicitly defines most variables and options available in the code, though sensible defaults have been defined for *most* variables should you forget to define something in your own codes. 

The main mathematical model is run through ``runSPX.m``. To run a simulation, one needs to define structures for ``options`` (containing code options), ``params`` (containing model parameters), and ``int`` (containing initial conditions). You can then run a single simulation with the defined options and parameters through

    outs = runSPX(int, params, options);

The output ``outs`` is a structure containing all the solutions at each requested time point. For more details on the structure of the code please see the relevant sections in the associated thesis (Hall-McNair, 2022).

Videos can be generated in one of two ways; either by setting ``SaveVideo=true`` when calling ``setParameters()``, or by directly invoking the functions 

    frames = frameFromOutputs(outs, params, options);
    saveMovie(frames, fps, filename)

where ``fps`` is the desired framerate of the movie and ``filename`` contains the path and name of the video save location. By default these functions plot the cell in a moving frame centred on the cell, in 3D and projected onto each Cartesian axis.

## Using this code for your own research

This code is provided open-source for others to use and play with as they'd like. Whilst I've attempted to keep everything as user-friendly and easy to read as possible, there's quite a lot of code here and so I cannot promise that there aren't redundant or not-fully-working options and toggles hiding about. If you have any questions about the code, please do contact me. If you're thinking of using the code for your own research projects, or using elements of it in your own code -- let me know! All I ask for is credit where it may be due.

## Credits

The vast majority of the code was written by Hall-McNair. The code for computing the nearest-neighbour matrices for the nearest-neighbour regularised stokeslet method was developed by Smith & Gallagher. Code in the ``endpiece/`` directory was developed by Neal & Hall-McNair.
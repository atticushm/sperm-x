function I = computeFlagForceIntegrals(f)

% computes integrals over [s,L] ds' of the hydrodynamic force

N = length(f)/3;
ds = 1/(N-1);

% calculate using matrix operator onto force densities (trapezium rule)
id  = triu(2*ones(N),1) + diag(ones(N,1)); 
id(:,end) = ones(N,1); id(end,:) = zeros(1,N);

% force integrals
I = ds/2 * kron(eye(3),id) * f;

end % function
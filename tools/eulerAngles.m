function [alpha,beta,gamma] = EulerAngles(X)

% determines the euler angles describing the curve X(s,t).

% arclength discretistaion:
N = length(X)/3;
s = linspace(0,1,N);

% compute tangents t = X':
tt = TangentsToCurve(X,s);
tt_mat = VectorToMatrix(tt);

% compute euler angles:
alpha = acos(-tt_mat(2,:)./sqrt(1-tt_mat(3,:).^2));
beta  = acos(tt_mat(3,:));

end % function
function [c_perp, c_para, gamma] = calcResistanceCoefficients(mu, q, b, type)

% dimesional resistance coefficients:
switch type
    case 'lgl'
        a = 2;
        d_perp = 1+2*log(a*q/b);
        d_para = -2+4*log(a*q/b);
        c_perp = 8*pi*mu/d_perp;
        c_para = 8*pi*mu/d_para;
        gamma = c_perp/c_para;
        
    case {'rss','reg-stokeslets'}
        
        % deltas is the half length of the integrated segment.
        deltas = q;
        eps = 1e-2;
        arg = 1+(eps/deltas)^2;
        D11 = 4*atanh(arg^(-1/2));
        D22 = 2*(arg)^(-1/2) + 2*atanh(arg^(-1/2));
        c_perp = 8*pi*mu/D22;
        c_para = 8*pi*mu/D11;
        gamma = c_perp/c_para;
end

end % function
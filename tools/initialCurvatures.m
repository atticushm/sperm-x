function [kap1,kap2,kap3,d01] = InitialCurvatures(X_int)

% computes the curvatures of X_int **provided it is an d1d3 planar curve**.
% assume no initial twist.

% number of nodes:
N = length(X_int)/3;

% arclength discretisation:
s = linspace(0,1,N);

% components:
[~,x2,~] = extractComponents(X_int);

% check planarity:
assert(max(x2)==0,'X_int is not d1d3 planar!')

% d3 is derivative of curve:
D = finiteDifferences(s,1); D_s = blkdiag(D{1},D{1},D{1});
d3 = D_s*X_int;

% d2 is out of plane:
d2 = kron([0;1;0],ones(N,1));

% d1=d2xd3:
d3_mat = VectorToMatrix(d3);
d2_mat = VectorToMatrix(d2);
d1_mat = cross(d2_mat,d3_mat);
d1 = Normalise(MatrixToVector(d1_mat));
d01 = proxp(d1);

% rates of change of basis vectors:
d1_s = D_s*d1;
d2_s = D_s*d2;
d3_s = D_s*d3;

% curvatures are thus:
kap1 = DotProduct(d2_s,d3);
kap2 = DotProduct(d3_s,d1);
kap3 = DotProduct(d1_s,d2);

end % function
function [E,E_s,E_ss] = varyingStiffness(s, params, varargin)

% Computes stiffness values along the flagellum, using experimental values
% typical for a human spermatazoa.

% set proximal and distal stiffness values:
if isempty(varargin)        % use human sperm experimental values:
    Ep = 8e-20;
    Ed = 2.2e-21;
    L  = 49e-6;
    % L = 60e-6;
else                        % use inputted values:
    Ep = varargin{1};
    Ed = varargin{2};
    L  = varargin{3};
end

% find critical arclength value
sd = params.s_crit*L; 
s = s*L;

s_lt = s(s<=sd);       % s values less than/equal to s_d.
s_gt = s(s>sd);        % s values greater than s_d.

% compute values:
Elt = (Ep-Ed)*((s_lt-sd)/sd).^2 +Ed;
Egt = Ed*ones(length(s_gt),1);
E = [Elt(:); Egt(:)]/Ed;

% finite difference matrix:
D = finiteDifferences(s, 1:2);
d_s = D{1};
d_ss = D{2};

% approximate derivatives of E:
E_s = d_s*E;
E_ss = d_ss*E;

end
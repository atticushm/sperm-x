function tt = TangentsToCurve(X, s)

% computes tangents to the curve via t = X', where prime denotes
% differentiation with respect to arclength s.

% get finite difference matrix:
D = finiteDifferences(s, 1);
diff1 = D{1};

% compute derivatives to curve:
tt = blkdiag(diff1,diff1,diff1) * X;

end % function
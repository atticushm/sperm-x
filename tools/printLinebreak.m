function printLinebreak(CHAR)

%LINEBREAK Prints a linebreak to the command window.
% printLinebreak(CHAR) prints the string CHAR repeatedly to the command window
% to make a useful linebreak in the command window output.
%
% See also REPMAT.

line_length = 75;   % equal to MATLAB editor default linebreak guide.

if nargin == 0
    CHAR = '-';
end

str = repmat(CHAR,1,line_length);
disp(str);
    
end
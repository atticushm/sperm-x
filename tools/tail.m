function out = tail(X,varargin)

if size(X,2) == 1
    X = mat(X);
end

if isempty(varargin)
    out = (X(:,end-9:end));
else
    out = (X(:,end-varargin{1}:end));
end

end % function
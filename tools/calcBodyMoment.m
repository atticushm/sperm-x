function M = calcBodyMoment(X, r, f, NN)

% X is a 3Qx1 vector of quadrature nodes.
% r is 3x1 centre of rotation.
% f is a 3Nx1 vector of forces.
% NN is the 3Qx3N nearest-neighbour matrix.

% calculate moment
[X1,X2,X3]=extractComponents(X);
dX1=X1-r(1);
dX2=X2-r(2);
dX3=X3-r(3);

x=[dX1', dX2', dX3']*NN;
[x1,x2,x3]=extractComponents(x); ze=0*x1;

AM=[ze -x3 x2; x3 ze -x1; -x2 x1 ze];
M=AM*f;

end % function
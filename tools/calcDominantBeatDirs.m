function [V, lab_dom, dir_dom] = calcDominantBeatDirs(X, d1, d2, d3)

% this function takes temporal flagellum shape and through PCR analysis
% determines the dominant director and lab frame directions of the beat at
% each instant

% OUTPUTS ARE:
% NPM is 'non planarity measure'
% dir_dom is the dominant director (local) vector of the beating at each
%   instant
% lab_dom is the dominant lab (fixed) vector of the beating at each
%   instant

% at each instant we want to find the principle components of the flagellum
% coordinates
num_t = size(X,2);
for n = 1:num_t
    % select data
    Xn = X(:,n);
    d1n = d1(:,n);
    d2n = d2(:,n);
    d3n = d3(:,n);

    % for pca, columns should be variables
    Xnp = mat(Xn)';

    % pca coefficients
    cX = pca(Xnp);

    % eigenvectors and values
    [EV, ~] = eig(cX);
    V{n} = real(EV);

    % find dominant beat plane in lab frame by dotting with e1,e2,e3
    e1 = [0;0;1]; e2 = [0;1;0]; e3 = [1;0;0];
    lab_dom(:,n) = [dot(V{n}(:,1),e1); dot(V{n}(:,2),e2); dot(V{n}(:,3),e3)];

    % find dominant beat plane in directory frame by dotting with head
    % frame directors
    d10 = proxp(d1n); d20 = proxp(d2n); d30 = proxp(d3n);
    dir_dom(:,n) = [dot(V{n}(:,1),d10); dot(V{n}(:,2),d20); dot(V{n}(:,3),d30)];

end

end
function [T,N,B,kappa,tau] = FrenetSerretBasis(X,s)

% Computes the Frenet-Serret basis for a given curve described by
% coordinates X along it's arclength s.

% TODO: expand to allow nonzero torsion.

%{
Qp1 = length(X)/3;

% Differentiate spline object to get first and second derivative of curve:
[d1,d2,~,~,~] = CurveDerivatives(X,s);

% Tangents to curve are the first derivatives of the curve:
d1M = VectorToMatrix(d1(:));
t = MatrixToVector(d1M./vecnorm(d1M));    
tM = VectorToMatrix(t);

% Second derivative to curve:
d2M = VectorToMatrix(d2(:));

% Filament curvature is:
kappa = vecnorm(d2M); kappa = kappa(:);
ikap = 1./kappa;

% Assume zero torsion (i.e. assume planar curve):
tau = zeros(Qp1,1);

% Normals to the curve via Frenet-Serret equations:
n = repmat(ikap,3,1).*d2(:);
nM = VectorToMatrix(n);

% Binormals:
bM = cross(tM,nM); 
b = MatrixToVector(bM);

%% Hard code case of straight filament (along x axis):

if tM == repmat([1;0;0],1,Qp1)
    nM = repmat([0;1;0],1,Qp1);
    bM = repmat([0;0;1],1,Qp1);
end
n = MatrixToVector(nM);
b = MatrixToVector(bM);

%% Outputs:

T = t(:);
N = n(:);
B = b(:);
%}

%% Test: alternate formulation #1:

% % Find derivatives and apply Gram-Schmidt process to obtain TNB frame.
% 
% [d1,d2,d3,~,~] = CurveDerivatives(X,s);
% 
% d1M = VectorToMatrix(d1);
% TM = d1M./vecnorm(d1M);
% 
% d2M = VectorToMatrix(d2);
% d3M = VectorToMatrix(d3);
% 
% NM = cross(d1M, cross(d2M,d1M))./(vecnorm(d1M).*vecnorm(cross(d2M,d1M)));
% BM = cross(d1M, d2M)./(vecnorm(cross(d1M,d2M)));
% 
% KAP = vecnorm(cross(d1M, d2M))./vecnorm(d1M).^3;
% % TAU = 

%% Test: alternate formulation #2:

% Compute tangents to the curve:
[d1,d2,~,~,~] = CurveDerivatives(X,s);
d1M = VectorToMatrix(d1);
TM = d1M./vecnorm(d1M);

% Curvature and torsion (assumed to be zero):
kappa = vecnorm(VectorToMatrix(d2)); kappa = kappa(:);
tau = zeros(length(kappa),1);

% Obtain a vector different to T at each point:
vM = circshift(TM,1);

% ** need to check for cases where all 3 entries are the same, in which
% case insert a zero entry **

% Build a unit vector from perturbed vector:
wM = vM./vecnorm(vM);

% Then normal and binormal are:
NM = cross(wM, TM);
BM = cross(NM, TM);

% Unit normal and binormal:
NM = NM./vecnorm(NM);
BM = BM./vecnorm(BM);

% Convert to vector output:
T = MatrixToVector(TM);
N = MatrixToVector(NM);
B = MatrixToVector(BM);

end
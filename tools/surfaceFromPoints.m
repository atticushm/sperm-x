function out = surfaceFromPoints(X)

% Draws a surface using coordinate data.
% X is a 3Nx1 vector of node positions.
% Outputs a structure containing everything needed to plot using trisurf,
% in the form
%   trisurf(out.hull, out.x, out.y, out.z, <options>)

% Extract coordinate components:
[x, y, z] = extractComponents(X);

% Calculate surface:
hull = convhull(x, y, z);

% Create output structure:
out.hull = hull;
out.x = x;
out.y = y;
out.z = z;

end % function
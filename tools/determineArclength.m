function [s,ds] = DetermineArclength(X)

% Computes the arclength of a curve given by coordinates in X.
% Assumes nodes are equally spaced.

% Extract components:
[x,y,z] = extractComponents(X(:));

% Compute segment lengths:
ds = sqrt(diff(x).^2 + diff(y).^2 + diff(z).^2);

% Determine arclength nodes (not necessarily evenly spaced):
s = [0; cumsum(ds(:))];

end % function
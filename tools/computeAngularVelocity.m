function w = ComputeAngularVelocity(d1,d2,d3,d1n,d2n,d3n,params)

% compute the angular velocity of the frame vectors
% as w = (d2_t.d3)d1 + (d3_t.d1)d2 + (d1_t.d2)d3

dt = params.dt;

% time derivatives
d1_t = (d1-d1n)/dt;
d2_t = (d2-d2n)/dt;
d3_t = (d3-d3n)/dt;

% compute angular velocity 
w = DotProduct(d2_t,d3).*d1 + ...
    DotProduct(d3_t,d1).*d2 + ...
    DotProduct(d1_t,d2).*d3;

end % function
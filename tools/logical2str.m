function str = logical2str(bool)

% converts logical variable bool to string 'on' or 'off'.

if bool == true
    str = 'on';
elseif bool == false
    str = 'off';
else
    warning('input not logical?')
end

end
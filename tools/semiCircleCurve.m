function varargout = SemiCircleCurve(N)

%SEMICIRCLECURVE Generate a semicircular curve.
% X = SemiCircularCurve(N) generates a semicircular curve comprised of N 3D
% coordinates.
%
% [X,s] = SemiCircularCurve(N) returns the 3Nx1 vector of curve coordinates
% and the Nx1 arclength parametrization of the coordinates.
%
% [x, y, z, s] = SemiCircularCurve(N) returns each component of the curve
% coordinates separately in Nx1 vectors.
%
% See also INITIALCONDITION, PARABOLICCURVE, SINUSOIDALCURVE

% Compute number of segments:
Q = N-1;

x       = linspace(-1,1,1e6);
y       = sqrt(1-x.^2);

ds      = sqrt((x(2:end) - x(1:end-1)).^2 + (y(2:end) - y(1:end-1)).^2);
s       = [0,cumsum(ds)];
delta   = max(s)/Q;
ss      = 0:delta:max(s);

yy      = spline(s,y,ss);
xx      = spline(s,x,ss);
x1      = xx/max(s); y1 = yy/max(s);

% xy planar curve:
x = x1(:);
y = -y1(:);
z = zeros(length(x),1);

s = ss(:)/max(s);

X = [x; y; z];

% Outputs
nOut = nargout;
if nOut <= 1
    varargout{1} = X(:);
elseif nOut == 2
    varargout{1} = X(:);
    varargout{2} = s(:);
elseif nOut == 4
    varargout{1} = x(:);
    varargout{2} = y(:);
    varargout{3} = z(:);
    varargout{4} = s(:);
end

end % function


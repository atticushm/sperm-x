function id = NearestNodesSurface(X, Y, r, ineq)

% identifies nodes within distance r of each coordinate given in X.
% X should be a 3*M vector of coordinates.
% ineq determines the inequality by which to identify nodes.

% Get coordinate components:
[x_1,y_1,z_1] = extractComponents(X(:));
[x_2,y_2,z_2] = extractComponents(Y(:));

% Compute distances:
dx = x_1' - x_2(:);
dy = y_1' - y_2(:);
dz = z_1' - z_2(:);
dist = sqrt(dx.^2 + dy.^2 + dz.^2);

% Identify nodes, choosing inequality from `sign':
switch ineq
    case '<'
        id = (dist < r);
    case '>'
        id = (dist > r);
    case '<='
        id = (dist <= r);
    case '>='
        id = (dist >= r);
    case '='
        id = (dist == r);
end
% id = blkdiag(id,id,id);
% id = repmat(id,3,3);

end % function
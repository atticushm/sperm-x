function I = regStokesletNonlocalNumericalIntegrals(x, X, q, epsilon, proc)

% x are field points
% X are source points
% q is half-length of local region
% epsilon is regularisation parameter
% proc indicates where to build stokeslets (cpu or gpu)

% matrix representations
x_mat = mat(x);

% number of force nodes, and interior weight
N = length(X)/3;
ds = 1/(N-1);

% number of field points
M = length(x)/3;

% local region is q=Q*ds, so 
Q = q/ds;

% for each field point, identify source points contributing to nonlocal
% integral. The first and last 'interior' intervals need tweaked so that
% there is not a singular point (i.e. no half segment) at the
% proximal/distal end respectively
id = zeros(M,N);
for m = 1:M
    sq = (m-1)*ds;
    if (sq < q)
        id(m,m+Q:end) = 1;
        type{m} = 'prox';
    elseif (sq > 1-q)
        id(m,1:m-Q) = 1;
        type{m} = 'dist';
    else
        id(m,1:m-Q) = 1;
        id(m,m+Q:end) = 1;
        if (m==Q+1)
            id(m,1) = 0;
            type{m} = 'prox';
        elseif (m==M-Q)
            id(m,M) = 0;
            type{m} = 'dist';
        else
            type{m} = 'int';
        end
    end
    Y{m} = X(logical(repmat(id(m,:)',3,1)));
end

% for each case, compute integrals
for m = 1:M
    xq = x_mat(:,m);        % field point
    Y_mat = mat(Y{m});      % nonlocal source points
    switch type{m}
        case {'prox','dist'}
            % first and last nodes will be half-weighted (either end of
            % q-interval and end of filament, or start of filament and
            % start of q-interval, respectively)
            Yp_midp = 1/2 * (Y_mat(:,1) + 1/2*(Y_mat(:,1)+Y_mat(:,2)));
            Yd_midp = 1/2 * (1/2*(Y_mat(:,end-1)+Y_mat(:,end)) + Y_mat(:,end));
            %Yp_midp = 1/2 * (Y_mat(:,1) + Y_mat(:,2));
            %Yd_midp = 1/2 * (Y_mat(:,end-1) + Y_mat(:,end));
            
            % proximal quadrature 
            %Ip{m} = ds/4 * (regStokeslet(xq,Y_mat(:,1),epsilon,proc) + ...
            %                regStokeslet(xq,Yp_midp,epsilon,proc));
            Ip{m} = ds/2 * regStokeslet(xq, Yp_midp, epsilon, proc);
            
            % distal quadrature 
            %Id{m} = ds/4 * (regStokeslet(xq, Yd_midp, epsilon, proc) + ...
            %                regStokeslet(xq, Y_mat(:,end), epsilon, proc));
            Id{m} = ds/2 * regStokeslet(xq, Yd_midp, epsilon, proc);
            
            % interior quadrature 
            Ii{m} = ds * regStokeslet(xq,vec(Y_mat(:,2:end-1)),epsilon,proc);
            
        case 'int'
            % first and last nodes will be half-weighted (these are the
            % nodes at the ends of the filament)
            Yp = 1/2 * (Y_mat(:,1) + 1/2*(Y_mat(:,1)+Y_mat(:,2)));
            Yd = 1/2 * (1/2*(Y_mat(:,end-1)+Y_mat(:,end)) + Y_mat(:,end));
            %Yp_midp = 1/2 * (Y_mat(:,1) +Y_mat(:,2));
            %Yd_midp = 1/2 * (Y_mat(:,end-1) + Y_mat(:,end));
            
            % nodes at each end of the q-interval will also be
            % half-weighted.
            % identify the location first zero in id matrix -- then the 
            % coordinates at the ends of the q-inteval will be loc-1 and loc
            ze_id  = find(id(m,:)==0);
            Yq_p   = Y_mat(:,ze_id(1)-1);
            Yq_pm1 = Y_mat(:,ze_id(1)-2);
            Yq_d   = Y_mat(:,ze_id(1));
            Yq_dp1 = Y_mat(:,ze_id(1)+1);
            
            % midpoints for the half-intervals either end of the q-region
            Yq_p_midp = 1/2*(Yq_p + 1/2*(Yq_p + Yq_pm1));
            Yq_d_midp = 1/2*(Yq_d + 1/2*(Yq_d + Yq_dp1));
            
            % proximal end quadrature 
            %Ip{m} = ds/4 * (regStokeslet(xq,Y_mat(:,1),epsilon,proc) + ...
            %                regStokeslet(xq,Yp_midp,epsilon,proc));
            Ip{m} = ds/2 * regStokeslet(xq, Yp, epsilon, proc);
            
            % left-hand q-region quadrature 
            %Iq_p{m} = ds/4 * (regStokeslet(xq,Yq_p_midp,epsilon,proc) + ...
            %                  regStokeslet(xq,Yq_p,epsilon,proc));
            Iq_p{m} = ds/2 * regStokeslet(xq, Yq_p_midp, epsilon, proc);
            
            % right-hand q-region quadrature 
            %Iq_d{m} = ds/4 * (regStokeslet(xq,Yq_d,epsilon,proc) + ...
            %                  regStokeslet(xq,Yq_d_midp,epsilon,proc));
            Iq_d{m} = ds/2 * regStokeslet(xq, Yq_d_midp, epsilon, proc); 
            
            % distal end quadrature 
            %Id{m} = ds/4 * (regStokeslet(xq,Y_mat(:,end),epsilon,proc) + ...
            %                regStokeslet(xq,Yd_midp,epsilon,proc));
            Id{m} = ds/2 * regStokeslet(xq, Yd, epsilon, proc);
            
            % interior quadratures
            % remove all half-weight nodes from Y_mat
            Y_mat_int = [Y_mat(:,2:ze_id(1)-2), Y_mat(:,ze_id(1)+1:end-1)];
            Y_int = vec(Y_mat_int);            
            Ii{m} = ds * regStokeslet(xq,Y_int,epsilon,proc);
    end 
end

% build integral operator in blocks -- 'prox' and 'dist' field points are
% straightforward to restack.
% preallocate
IXX = zeros(N); IXY = 0*IXX; IXZ = 0*IXX;
IYX = zeros(N); IYY = 0*IYX; IYZ = 0*IYX;
IZX = zeros(N); IZY = 0*IZX; IZZ = 0*IZX;

% proximal field points
for i = 1:Q+1
    P = size(Ii{i},2)/3;
    IXX_ins  = [Ip{i}(1,1),Ii{i}(1,1:P),Id{i}(1,1)];
    IXX(i,:) = [zeros(1,N-length(IXX_ins)), IXX_ins];
    
    IXY_ins  = [Ip{i}(1,2),Ii{i}(1,P+1:2*P),Id{i}(1,2)];
    IXY(i,:) = [zeros(1,N-length(IXY_ins)), IXY_ins];
    
    IXZ_ins  = [Ip{i}(1,3),Ii{i}(1,2*P+1:3*P),Id{i}(1,3)];
    IXZ(i,:) = [zeros(1,N-length(IXZ_ins)), IXZ_ins];
    
    IYX_ins  = [Ip{i}(2,1),Ii{i}(2,1:P),Id{i}(2,1)];
    IYX(i,:) = [zeros(1,N-length(IYX_ins)), IYX_ins];
    
    IYY_ins  = [Ip{i}(2,2),Ii{i}(2,P+1:2*P),Id{i}(2,2)];
    IYY(i,:) = [zeros(1,N-length(IYY_ins)), IYY_ins];
    
    IYZ_ins  = [Ip{i}(2,3),Ii{i}(2,2*P+1:3*P),Id{i}(2,3)];
    IYZ(i,:) = [zeros(1,N-length(IYZ_ins)), IYZ_ins];
    
    IZX_ins  = [Ip{i}(3,1),Ii{i}(3,1:P),Id{i}(3,1)];
    IZY(i,:) = [zeros(1,N-length(IZX_ins)), IZX_ins];
    
    IZY_ins  = [Ip{i}(3,2),Ii{i}(3,P+1:2*P),Id{i}(3,2)];
    IZY(i,:) = [zeros(1,N-length(IZY_ins)), IZY_ins];
    
    IZZ_ins  = [Ip{i}(3,3),Ii{i}(3,2*P+1:3*P),Id{i}(3,3)];
    IZZ(i,:) = [zeros(1,N-length(IZZ_ins)), IZZ_ins];
end

% distal field points can be populated in a similar way
for i = M-Q:M
    P = size(Ii{i},2)/3;
    IXX_ins  = [Ip{i}(1,1),Ii{i}(1,1:P),Id{i}(1,1)];
    IXX(i,:) = [IXX_ins, zeros(1,N-length(IXX_ins))];
    
    IXY_ins  = [Ip{i}(1,2),Ii{i}(1,P+1:2*P),Id{i}(1,2)];
    IXY(i,:) = [IXY_ins, zeros(1,N-length(IXY_ins))];
    
    IXZ_ins  = [Ip{i}(1,3),Ii{i}(1,2*P+1:3*P),Id{i}(1,3)];
    IXZ(i,:) = [IXZ_ins, zeros(1,N-length(IXZ_ins))];
    
    IYX_ins  = [Ip{i}(2,1),Ii{i}(2,1:P),Id{i}(2,1)];
    IYX(i,:) = [IYX_ins, zeros(1,N-length(IYX_ins))];
    
    IYY_ins  = [Ip{i}(2,2),Ii{i}(2,P+1:2*P),Id{i}(2,2)];
    IYY(i,:) = [IYY_ins, zeros(1,N-length(IYY_ins))];
    
    IYZ_ins  = [Ip{i}(2,3),Ii{i}(2,2*P+1:3*P),Id{i}(2,3)];
    IYZ(i,:) = [IYZ_ins, zeros(1,N-length(IYZ_ins))];
    
    IZX_ins  = [Ip{i}(3,1),Ii{i}(3,1:P),Id{i}(3,1)];
    IZY(i,:) = [IZX_ins, zeros(1,N-length(IZX_ins))];
    
    IZY_ins  = [Ip{i}(3,2),Ii{i}(3,P+1:2*P),Id{i}(3,2)];
    IZY(i,:) = [IZY_ins, zeros(1,N-length(IZY_ins))];
    
    IZZ_ins  = [Ip{i}(3,3),Ii{i}(3,2*P+1:3*P),Id{i}(3,3)];
    IZZ(i,:) = [IZZ_ins, zeros(1,N-length(IZZ_ins))];
end

% interior field points require bit more care
j=0;
for i = Q+2:M-Q-1
    ze = zeros(1,2*Q-1);
    [Iix1,Iix2,Iix3] = extractComponents(Ii{i}(1,:));
    [Iiy1,Iiy2,Iiy3] = extractComponents(Ii{i}(2,:));
    [Iiz1,Iiz2,Iiz3] = extractComponents(Ii{i}(3,:));
    
    IXX(i,:) = [Ip{i}(1,1),  Iix1(1:j),    Iq_p{i}(1,1), ze, ...
                Iq_d{i}(1,1),Iix1(j+1:end),Id{i}(1,1)];
    IXY(i,:) = [Ip{i}(1,2),  Iix2(1:j),    Iq_p{i}(1,2), ze, ...
                Iq_d{i}(1,2),Iix2(j+1:end),Id{i}(1,2)];
    IXZ(i,:) = [Ip{i}(1,3),  Iix3(1:j),    Iq_p{i}(1,3), ze, ...
                Iq_d{i}(1,3),Iix3(j+1:end),Id{i}(1,3)];
            
    IYX(i,:) = [Ip{i}(2,1),  Iiy1(1:j),    Iq_p{i}(2,1), ze, ...
                Iq_d{i}(2,1),Iiy1(j+1:end),Id{i}(2,1)];
    IYY(i,:) = [Ip{i}(2,2),  Iiy2(1:j),    Iq_p{i}(2,2), ze, ...
                Iq_d{i}(2,2),Iiy2(j+1:end),Id{i}(2,2)];
    IYZ(i,:) = [Ip{i}(2,3),  Iiy3(1:j),    Iq_p{i}(2,3), ze, ...
                Iq_d{i}(2,3),Iiy3(j+1:end),Id{i}(2,3)];
            
    IZX(i,:) = [Ip{i}(3,1),  Iiz1(1:j),    Iq_p{i}(3,1), ze, ...
                Iq_d{i}(3,1),Iiz1(j+1:end),Id{i}(3,1)];
    IZY(i,:) = [Ip{i}(3,2),  Iiz2(1:j),    Iq_p{i}(3,2), ze, ...
                Iq_d{i}(3,2),Iiz2(j+1:end),Id{i}(3,2)];
    IZZ(i,:) = [Ip{i}(3,3),  Iiz3(1:j),    Iq_p{i}(3,3), ze, ...
                Iq_d{i}(3,3),Iiz3(j+1:end),Id{i}(3,3)];
    
    j = j+1;
end

% build final integral operator matrix
I = [IXX, IXY, IXZ; ...
     IYX, IYY, IYZ; ...
     IZX, IZY, IZZ];

end % function
function [U, Om] = ComputeRigidMotionParams(Xi, Xk, Uk, Omk, dt)

%% using finite differences:

% determine some parameters:
N = length(Xi)/3;
s = linspace(0,1,N);

% compute leading point:
X0i = proxp(Xi);
X0k = proxp(Xk);

% head translational velocity is velocity of X0:
U = (X0i-X0k)/dt;

% omega is computed via
% omega = (e1_t.e2)e3 + (e2_t.e3)e1 + (e3_t.e1)e2 
% compute orthormal basis of cell head:
[tt,nn,bb] = TNBFrame(Xi, s);
tH = proxp(tt);
nH = proxp(nn);
bH = proxp(bb);



end % function
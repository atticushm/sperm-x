function W = crossProdOp(X)

% Computes the operator form for a cross product, that is, this code
% returns the matrix W where:
%       XxY == W Y

% Extract coordinates and convert to row vectors:
[x1,x2,x3] = extractComponents(X);
x1 = diag(x1(:));    
x2 = diag(x2(:));
x3 = diag(x3(:));
N  = length(X)/3;

% Build operator:
W = [zeros(N), -x3, x2; 
     x3, zeros(N), -x1;
     -x2, x1, zeros(N)];

end % function
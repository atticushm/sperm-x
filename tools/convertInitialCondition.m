function Y_int = convertInitialCondition(X_int, ver)

%CONVERTINITIALCONDITION converts the position formulation initial
%condition given by X0 (used in Sperm-X) into the angle formulation (that
%is, X0 + theta) used in EIF.

% convert to matrix representation
X_mat = mat(X_int);

% determine tangent angles
theta = atan(diff(X_mat(2,:))./diff(X_mat(1,:)));

% determine leading point
X0 = X_mat(:,1);

switch ver
    case 'endpiece'
        % initial condition suitable for endpiece (Neal) code
        Y_int = [X0(1); X0(2); theta(1); theta(:)];
    case 'eif'
        % initial condition suitable for EIF (Hall-McNair) code
        Y_int = [X0(1); X0(2); theta(:)];
end 

end % function
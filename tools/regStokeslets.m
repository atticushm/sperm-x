function S = RegStokeslets(x_field,X_source,eps)
% x is field points.
% X is source points.
% eps is regularization parameter.
% Code by D.J.Smith

x_field=x_field(:);
X_source=X_source(:);
M=length(x_field)/3;
Q=length(X_source)/3;
r1=      x_field(1:M)*ones(1,Q)-ones(M,1)*      X_source(1:Q)';
r2=  x_field(M+1:2*M)*ones(1,Q)-ones(M,1)*  X_source(Q+1:2*Q)';
r3=x_field(2*M+1:3*M)*ones(1,Q)-ones(M,1)*X_source(2*Q+1:3*Q)';
rsq=r1.^2+r2.^2+r3.^2;
ireps3=1./(sqrt((rsq+eps^2)).^3);
isotropic=kron(eye(3),(rsq+2.0*eps^2).*ireps3);
dyadic=[r1.*r1 r1.*r2 r1.*r3; r2.*r1 r2.*r2 r2.*r3; ...
        r3.*r1 r3.*r2 r3.*r3].*kron(ones(3,3),ireps3);

S=(1.0/(8.0*pi))*(isotropic+dyadic);
% S=(isotropic+dyadic);

end % function
function [Yt, Hfr] = HeadSurfaceNodes(X, Y, hdims, n0)

% X is flagellum nodes.
% Y is a reference surface discretisation.
% hdims are the lengths of the axes of the surface.
% Hfr0 contains the TNB frame of the reference cell head.

Qp1 = length(X)/3;
M = length(Y)/3;

XM = VectorToMatrix(X);
R = RotationMatrix(X);
R0 = [R(:,1),R(:,Qp1+1),R(:,2*Qp1+1)]; 
X0 = XM(:,1);

%% Determine head nodes:

% Rotate points about X0:
% Yr = RotatePoints(Yt, R0, X0);
YMr = R0\VectorToMatrix(Y);
Yr = MatrixToVector(YMr);

% Translate head so that X0 is the centroid.
Yc = Yr+kron(X0,ones(M,1));

% Vector from head centroid to joint location is:
dc = kron(R0\[hdims(1);0;0],ones(M,1));

% Shift nodes so that X0 is on the surface (not centroid):
Yt = Yc-dc;

%% Determine head frame:

Hfr.t0 = R0\Hfr0.t0;
Hfr.n0 = R0\Hfr0.n0;
Hfr.b0 = R0\Hfr0.b0;

end
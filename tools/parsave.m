function parsave(filename, varargin)

% saves a variable with path and name FILENAME.
% to be called inside PARFOR.

% for i = 1:length(varargin)
%     var{i} = varargin{i};
% end

if ~exist(filename,'file')
    save(filename,'-v7.3')
    printBanner('FILE SAVED')
else
    save(filename)
    printBanner('FILE SAVED (OVERWRITE EXISTING FILE)')
end

end % function
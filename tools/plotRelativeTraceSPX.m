function plotRelativeTraceSPX(X, Y, d10, rot, head, dir, dtraj, ptraj, varargin)

% plots trace of beat given solution data, relative to motion of the cell
% body. In SPX this is the same as relative to the motion of X0 

% this function should only be supplied data which you want to plot -- it
% does not do any filtering/sampling of the solution data.

% the boolean toggles are as follows
%   - rot:      plot trace relative to head motion? i.e. body frame trace
%   - head:     plot head?
%   - dir:      plot proximal directors? if yes, include d20, d30 in 
%               varargin{1} and varargin{2}
%   - dtraj:    plot trajectory of distal flagellum tip?
%   - ptraj:    plot trajectory of X0 ~over full simulation~? if yes,
%               include X0 to be plotted in varargin{1}

% this function is specific to SPX model

num_tps = size(X,2);
qscale = 0.07;

% positions of X0
N = size(X,1)/3;
M = size(Y,1)/3;
X0 = [X(1,:); X(N+1,:); X(2*N+1,:)];
X0N = kron(X0, ones(N,1));
X0M = kron(X0, ones(M,1));

% X relative to X0
Xr = X-X0N;

% positions of XN relative to X0
XN = [X(N,:); X(2*N,:); X(3*N,:)];
XNr = XN-X0;
XNi = [];

% Y relative to X0
Yr = Y-X0M;

% setup colours
cols = parula(num_tps);

% extract d20, d30 from varargin if needed
if rot
    d20 = varargin{1};
    d30 = varargin{2};
end

% draw traces
for i = 1:num_tps

    Xi = Xr(:,i);
    Yi = Yr(:,i);

    % if rot=true: rotate all coordinates so that proximal director frame
    % is aligned with x,y,z axis
    if rot
        % find rotation matrix
        R = findRotationMatrix(d30(:,i),d20(:,i),d10(:,i));

        % rotate X
        Xim = mat(Xi); Ximr = R*Xim;
        Xi = vec(Ximr);
        XNi = [XNi, distp(Xi)];     % to store relative distal trajectory

        % rotate Y
        Yim = mat(Yi); Yimr = R*Yim;
        Yi = vec(Yimr);

    end

    % flagellum shape relative to X0
    [x1,x2,x3] = extractComponents(Xi);

    % plot flagellum
    box on; hold on; 
    plot3(x1,x2,x3, 'LineWidth',1.2, 'Color',cols(i,:));

    % plot head
    if (i==1) && head==true
        % [y1,y2,y3] = extractComponents(Yi);
        % scatter3(y1,y2,y3, '.', 'MarkerEdgeColor','k');
        out = surfaceFromPoints(Yi);
        trisurf(out.hull, out.x, out.y, out.z, 'EdgeColor','none');
        colormap('gray'); shading interp
    end

    % plot d10
    if ~rot && dir==true
        quiver3(0,0,0, d10(1,i),d10(2,i),d10(3,i), qscale, 'Color',cols(i,:), ...
            'LineWidth',1.5, 'MaxHeadSize',0.3);
    end

end

% if ptraj==true, plot X0 trajectory, projected on 'walls' of enclosing box
% only allow for pinned mode -- surely not interesting for body frame plots
if ptraj && ~rot
    % shift full trajectory 'back' to end at [0;0;0]
    traj = varargin{1};
    trajr = traj - traj(:,end);

    % projection onto xy wall
    Ones = ones(1,size(trajr,2));
    scatter3(trajr(1,1), trajr(2,1), -0.1, 'o','filled', ...
        'MarkerFaceColor','m', 'MarkerEdgeColor','m');
    scatter3(trajr(1,end), trajr(2,end), -0.1, 's','filled', ...
        'MarkerFaceColor','m', 'MarkerEdgeColor','m');
    plot3(trajr(1,:), trajr(2,:), -0.1*Ones, 'm', 'LineWidth',1.3)

    % projection onto xz wall
    scatter3(trajr(1,1), 0.15, trajr(3,1), 'o','filled', ...
        'MarkerFaceColor','m', 'MarkerEdgeColor','m');
    scatter3(trajr(1,end), 0.15, trajr(3,end), 's','filled', ...
        'MarkerFaceColor','m', 'MarkerEdgeColor','m');
    plot3(trajr(1,:), 0.15*Ones, trajr(3,:), 'm', 'LineWidth',1.3)
end

% if dtraj==true, plot distal tip trajectory
% as viewed from yz axis, add integer to y,z coordinate to draw 'on top'
if dtraj
    if rot 
        scatter3(XNi(1,1)+3, XNi(2,1), XNi(3,1), 'o','filled', ...
            'MarkerFaceColor','m', 'MarkerEdgeColor','m');
        scatter3(XNi(1,end)+3, XNi(2,end), XNi(3,end), 's','filled', ...
            'MarkerFaceColor','m', 'MarkerEdgeColor','m');
        plot3(XNi(1,:)+2, XNi(2,:), XNi(3,:), 'LineWidth',1.4, 'Color','m')
    else
        scatter3(XNr(1,1)+3, XNr(2,1), XNr(3,1), 'o','filled', ...
            'MarkerFaceColor','m', 'MarkerEdgeColor','m');
        scatter3(XNr(1,end)+3, XNr(2,end), XNr(3,end), 's','filled', ...
            'MarkerFaceColor','m', 'MarkerEdgeColor','m');
        plot3(XNr(1,:)+2, XNr(2,:), XNr(3,:), 'LineWidth',1.4, 'Color','m')
    end
end

% set axes
view(3); axis equal
% xlim([-0.2 1.0])
% ylim([-0.05, 0.05])
% zlim([-0.2,0.2])

% set view
ax = gca;
ax.View = [-51.8629, 15.2246];
ax.SortMethod = 'depth';

% labels
xlabel('$x$', 'FontSize',14, 'Interpreter','latex')
ylabel('$y$', 'FontSize',14, 'Interpreter','latex')
zlabel('$z$', 'FontSize',14, 'Interpreter','latex')

end % function
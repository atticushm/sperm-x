function pkf = plotTipTraceProj(X, traj, tps, method, varargin)

% number of time points traj given at
num_tps = length(tps);
assert(num_tps==size(traj,2),'Number of time points and traj points differ!')

% determine X0 at each time step
N = size(X,1)/3;
X0 = [X(1,:); X(N+1,:); X(2*N+1,:)];

%% compute relative trajectory

switch method
    % cell is pinned at X0 ===============================================
    case 'pinned'

        % find positions relative to X0 at each time step
        trajr = traj-X0;


    % cell head is clamped ===============================================
    case 'clamped'

        % find positions relative to fixed axis at each time step
        % requires d10,d20,d30 data, given in varargin
        d10 = varargin{1}; d20 = varargin{2}; d30 = varargin{3};
        trajr = [];
        for k = 1:num_tps
            % find rotation matrix
            R = findRotationMatrix(d30(:,k), d20(:,k), d10(:,k));

            % find rotated relative trajectory
            trajr = [trajr, R*(traj(:,k)-X0(:,k))];
        end
end

%% plot projection onto yz plane
% colour line by time value

nexttile(1,[2,2]); box on

% plot
x = trajr(2,:); y = trajr(3,:); z = 0*x;
surface([x;x],[y;y],[z;z],[tps;tps], 'FaceColor','none', 'EdgeColor','interp', ...
    'LineWidth',2);
hold on;

% indicate start and end
scatter(x(1),y(1), 'om','filled')
scatter(x(end),y(end), 'sm','filled')

% axes
% xlabel('$y$', 'FontSize',14, 'Interpreter','latex')
% ylabel('$z$', 'FontSize',14, 'Interpreter','latex')
xlim([-0.2 0.2])
ylim([-0.2 0.2])

% colorbar
cbar = colorbar;
cbar.TickLabelInterpreter = 'latex';
cbar.Ticks = [0,2,4,6,8,10]*2*pi;
cbar.TickLabels = {'0','$4\pi$','$8\pi$','$12\pi$','$16\pi$','$20\pi$'};

%% plot fourier curves

nexttile([1,2]); 
plot(tps, x, 'LineWidth',1.2); hold on;
plot(tps, y, 'LineWidth',1.2)
axis tight

% axes
T = 2*pi;
xticks((1:10)*T);
for i = 1:10
    tk{i} = sprintf('$%g\\pi $',i);
end
xticklabels(tk)
ax = gca;
ylim([-0.18 0.18])
ax.YAxis.Exponent = -1;

% plot
nexttile([1,2]);
fftx = fft(x);
ffty = fft(y);
nf = length(fftx);
xdat = abs(fftx(1:nf/2));
ydat = abs(ffty(1:nf/2));

Fs = tps(2)-tps(1);
freq = (0:1:length(xdat)-1)*Fs/length(xdat);

plot(freq, xdat, 'LineWidth',1.2); hold on
plot(freq, ydat, 'LineWidth',1.2)
axis tight

% determine peaks
thld = 0.1;
[pkx,idx] = findpeaks(xdat, 'Threshold',thld);
[pky,idy] = findpeaks(ydat, 'Threshold',thld);
xstr = '$f$ (Hz)'; %; peaks at';
for n = 1:length(pkx)
    pkf(n) = freq(idx(n));
    % xstr = [xstr,sprintf(' %.2gHz,',pkf(n))];
end
% xlabel(xstr, 'FontSize',12, 'Interpreter','latex')

% simplify y label
ax = gca;
ylim([0,ceil(ax.YLim(2))])

end % function
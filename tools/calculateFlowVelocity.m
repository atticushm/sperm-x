function ux = calculateFlowVelocity(xfield, f, phi, codebase, varargin)

% requires end piece code to be in path to function.

% add endpiece code to path if not already on path
if (exist(',/endpiece_code','dir')~=7)
    addpath(genpath('./endpiece_code'))
end

% regularisation parameter
eps = 0.01;

% determine contributions
switch codebase
    case 'endpiece'
        Z     = varargin{1};
        model = varargin{2};
        
        [head,flag,NN] = GetProblemData(Z, model);
        
        th  = flag.th;
        ang = model.swimmer.ang;
        Q   = model.disc.Q;
        ds  = 1/Q;
        
        Rot = [cos(th'+ang) -sin(th'+ang) zeros(1,Q);
               sin(th'+ang)  cos(th'+ang) zeros(1,Q); 
               zeros(1,Q)    zeros(1,Q)   ones(1,Q)];
             
        SF = RegStokesletAnalyticIntegrals(xfield, flag.Xm, ds/2, Rot, eps);
        SH = RegStokeslet(xfield, head.XQuad, eps, 'cpu') * NN;
                
    case 'spermx'
        Xflag = varargin{1};
        Xhead = varargin{2};
        
        NNF = NearestNeighbourMatrix(Xflag, xfield);
        NNH = NearestNeighbourMatrix(Xhead, xfield);
        
        SF = RegStokeslet(xfield, Xflag, eps, 'cpu') * NNF;
        SH = RegStokeslet(xfield, Xhead, eps, 'cpu') * NNH;
end

% velocities are
ux = -SF*f -SH*phi;

% reset paths
AddFilesToPath;

end % function
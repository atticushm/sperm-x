function cf = finiteDifferenceCoefficients(stencil, deriv_order)

% Computes the finite difference coefficients for an equispaced finite
% difference scheme approximating a <deriv_order> derivative on a grid
% defined by <stencil>.

% Based on notes by C.R.Taylor (MIT) - see https://web.media.mit.edu/~crtaylor/calculator.html

h = diff(stencil); h = h(1);

N = length(stencil);
d = deriv_order;

if d >= N
    warning('Stencil does not have enough points for requested derivative!')
    cf = nan;
    return
end

% Left hand side:
S = zeros(N,length(stencil));
for i = 1:N
    S(i,:) = stencil.^(i-1);
end

% Right hand side:
b = zeros(N,1); b(d+1) = factorial(d);

% Solve system for coefficients:
cf = (1/h^d * S)\b;
cf = cf';

end % function
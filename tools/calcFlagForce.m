function F = calcFlagForce(f)

% midpoint rule about nodes
%{
N = length(f)/3;
ds = 1/(N-1);

f_mat = mat(f);

F = ds/2*f_mat(:,1) + sum(ds*f_mat(:,2:N-1),2) + ds/2*f_mat(:,N);
%}

% using trapezium rule
I = computeFlagForceIntegrals(f);
F = proxp(I);

end % function
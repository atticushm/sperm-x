function bool = checkSelfIntersection(X)

% checks if a 3d curve self intersects anywhere along its length.
% returns TRUE or FALSE

% matrix representation
X_mat = vectorToMatrix(X);

N = length(X)/3;
s = linspace(0,1,N);

% obtain high resolution spline of curve
Nq = 1000;
sq = linspace(0,1,Nq);

xq = spline(s,X_mat(1,:),sq);
yq = spline(s,X_mat(2,:),sq);
zq = spline(s,X_mat(3,:),sq);
Xq_mat = [xq;yq;zq]';

% closeness tolerance
tol = 1e-5;
Xq_unq = uniquetol(Xq_mat, tol, 'ByRows', 1);
N_unq  = size(Xq_unq,1);

% detect removed entries, indicating self intersection
if N_unq < Nq
    bool = true;
else
    bool = false;
end

end % function
function Y = VectorToMatrix(X)

% transforms a 3Nx1 vector of coordinate data into a 3xN matrix in which
% each column forms coordinate data.

X = X(:);
Y = reshape(X',[],3)';

end % function
function err = meanSqError(x,y,varargin)

% Calculate the mean squared error between 2 input vectors x and y.
% sqr is boolean to indicate whether square rooted MSE should be
% calculated.

x = x(:);
y = y(:);
N_x = length(x);
N_y = length(y);

% Sample larger vector if x, y are not equal length:
if N_x ~= N_y
    % Obtain parametrisations:
    ds_x = diff(x); s_x = [0; cumsum(ds_x)];
    ds_y = diff(y); s_y = [0; cumsum(ds_y)]; 
    if N_x < N_y % interp x to finer y
        x = interp1(s_x, x, s_y);
        N_x = length(x);
    elseif N_y < N_x % interp y to finer x
        y = interp1(s_y, y, s_x);
        N_y = length(y);
    end
end 
    
% Compute mean squared error:
x = x(1:end-1);
y = y(1:end-1);
err = (1/N_x)*sum((x-y).^2);

if ~isempty(varargin) && varargin{1} == 1
    err = err.^(1/2);
end

end % function
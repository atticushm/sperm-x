function [d1,d2,d3] = reconstructFrame(X, k3, d10, params, options)
 
% RECONSTRUCTFRAME builds the orthonormal material frame to a curve given the 
% known quantities X (positions), k3 (twist curvature) and d10 (material 
% normal at s=0).

% scaling matrices
% persistent P R C

% number of nodes
NF = params.NF;

% finite difference operators
D = finiteDifferences(linspace(0,1,NF), 1:2);
D_s = kron(eye(3),D{1});
D_ss = kron(eye(3),D{2});

% derivatives of the curve
X_s  = normalise(D_s*X);
X_ss = D_ss*X;

% components of derivatives:
[x1_s,x2_s,x3_s] = extractComponents(X_s);
[x1_ss,x2_ss,x3_ss] = extractComponents(X_ss);

% d3 is known since d3=Xs;
d3 = X_s;

% d2 and d1 and determined by solving a linear system, which we construct
% now.

%% compute planar frame

% assuming planarity - d1 is normal, d3 is tangent, and d2 is out of plane
% constant vector

if options.ForcePlanar
    d2 = kron([0;0;1],ones(NF,1));
    d1 = crossProdOp(d2)*d3;
end

%% compute director frame

% we have d3=X_s, and d1_s = k3*d2-k2*d3, with k2 = d3_s.d1 and d2=d3xd1
% new method -- encode as differential equation rather than integral
% equation
% unknowns are d1, d2 and k2

if ~options.ForcePlanar
    
    %% encode d1_s + d3*k2 -k3*d2 = 0
    % operator onto d1
    Ad1 = D_s;

    % operator onto d2
    Ad2 = -kron(eye(3),diag(k3));

    % operator onto k2
    Ak2 = [diag(x1_s); diag(x2_s); diag(x3_s)];

    % matrix operator
    M1 = [Ad1, Ad2, Ak2];

    % right hand side
    bA = zeros(3*NF,1);

    % apply boundary condition
    M1(1,:) = [1, zeros(1,NF-1), zeros(1,6*NF)];
    M1(NF+1,:) = [zeros(1,NF), 1, zeros(1,NF-1), zeros(1,5*NF)];
    M1(2*NF+1,:) = [zeros(1,2*NF), 1, zeros(1,NF-1), zeros(1,4*NF)];
    bA(1) = d10(1);
    bA(NF+1) = d10(2);
    bA(2*NF+1) = d10(3);

    %% encode d2 - d3xd1 = 0
    % operator onto d2
    Bd2 = eye(3*NF);

    % operator onto d1
    Bd1 = -crossProdOp(d3);

    % matrix operator
    M2 = [Bd1, Bd2, zeros(3*NF,NF)];

    % right hand side
    bB = zeros(3*NF,1);

    %% encode k2 - d3_s.d1 = k2 - X_ss.d1 = 0
    % operator onto k2
    Ck2 = eye(NF);

    % operator onto d1
    Cd1 = -[diag(x1_ss), diag(x2_ss), diag(x3_ss)];

    % matrix operator
    M3 = [Cd1, zeros(NF,3*NF), Ck2];

    % right hand side
    bC = zeros(NF,1);
    
    %% build and solve 
    M = [M1; M2; M3];
    b = [bA; bB; bC];
    z = M\b;

    % extract solutions
    d1 = z(1:3*NF,1);
    d2 = z(3*NF+1:6*NF,1);
    % k2 = z(6*NF+1:7*NF,1);
    
    % normalise
    d1 = normalise(d1);
    d2 = normalise(d2);
end

%% compute director frame (old method)

%{ 
if ~options.ForcePlanar
    % number of segments/midpoints:
    Q = NF-1;

    % known variables at midpoints.
    % use the *mp suffix to denote a value at an interval midpoint:
    k3mp = 0.5*(k3(1:NF-1)+k3(2:NF));
    Xs_mp = midpoints(X_s);
    Xss_mp= midpoints(X_ss);
    sm = 0.5*(s(1:NF-1)+s(2:NF));

    % extract components:
    [xsmp,ysmp,zsmp] = extractComponents(Xs_mp);
    [xssmp,yssmp,zssmp] = extractComponents(Xss_mp);

    %% d1 rows:
    % via d1 = d10 + int_0^s (k3*d2-k2*d3)ds'

    % operator onto d1m:
    Ad1d1 = eye(3*Q);                                           % multiplies onto d1m

    % integral operator onto d2m:
    int = tril(repmat(ds*k3mp',Q,1),-1) + ds/2*diag(k3mp);
    Ad1d2 = -blkdiag(int,int,int);                              % multiplies onto d2m

    % integral operator onto k2:
    intx = tril(repmat(ds*xsmp',Q,1),-1) + ds/2*diag(xsmp);
    inty = tril(repmat(ds*ysmp',Q,1),-1) + ds/2*diag(ysmp);
    intz = tril(repmat(ds*zsmp',Q,1),-1) + ds/2*diag(zsmp);
    Ad1k2 = [intx; inty; intz];                                 % multiplies onto k2m

    % matrix operator:
    Ad1 = [Ad1d1, Ad1d2, Ad1k2];

    % right hand side:
    bd1 = kron(d10,ones(Q,1));

    %% d2 rows:
    % via d2 = d3xd1

    % cross product operator:
    dg1 = diag(xsmp); 
    dg2 = diag(ysmp); 
    dg3 = diag(zsmp); 
    ze = 0*dg1;
    Ad2d1 = -[ze,-dg3,dg2; dg3,ze,-dg1; -dg2,dg1,ze];           % multiplies onto d1m
    Ad2d2 = eye(3*Q);                                           % multiplies onto d2m

    % matrix operator:
    Ad2 = [Ad2d1, Ad2d2, zeros(3*Q,Q)];

    % right hand side:
    bd2 = zeros(3*Q,1);

    %% k2 rows:
    % via k2 = d3_s.d1 = X_ss.d1

    % left hand side:
    Ak2k2 = eye(Q);                                             % multiplies onto k2m
    Ak2d1 = -[diag(xssmp),diag(yssmp),diag(zssmp)];             % multiplies onto d1m

    % matrix operator:
    Ak2 = [Ak2d1, 0*Ak2d1, Ak2k2];

    % right hand side:
    bk2 = zeros(Q,1);

    %% build system and solve:

    % matrix operator:
    A = [Ad1; Ad2; Ak2];

    % right hand side:
    b = [bd1; bd2; bk2];

    % solve system:
    z = A\b;

    % extract solutions:
    d1m = z(1:3*Q);
    d2m = z(3*Q+1:6*Q);
    k2m = z(6*Q+1:7*Q);      

    %% spline to get variables at nodes:

    % extract components:
    [d1mpx,d1mpy,d1mpz] = extractComponents(d1m);
    [d2mpx,d2mpy,d2mpz] = extractComponents(d2m);

    % cubic spline:
    d1x = spline(sm,d1mpx,s);
    d1y = spline(sm,d1mpy,s);
    d1z = spline(sm,d1mpz,s);
    d2x = spline(sm,d2mpx,s);
    d2y = spline(sm,d2mpy,s);
    d2z = spline(sm,d2mpz,s);

    % rebuild d1 and d2:
    d1 = [d1x(:); d1y(:); d1z(:)];
    d2 = [d2x(:); d2y(:); d2z(:)];

    % normalise:
    d1 = normalise(d1);
    d2 = normalise(d2);
end
%}

end % function
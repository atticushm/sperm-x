function [T,N,B,k,tau] = DirectorToFrenet(d1,d2,d3,k1,k2,k3)

% converts the director basis, with bending curvatures k1 and k2 about the
% d1 and d2 axes, into the Frenet-Serret normal and binormal.
% additionally returns the Frenet-Serret curvature k and torsion tau.

% ensure column vectors:
d1=d1(:); d2=d2(:); d3=d3(:); 
k1=k1(:); k2=k2(:); k3=k3(:);

% compute curvature:
k = sqrt(k1.^2 + k2.^2);

% ratio terms:
kk1 = k1./k;        kk1_rep = repmat(kk1(:),3,1);
kk2 = k2./k;        kk2_rep = repmat(kk2(:),3,1);

% frenet-serret normals are:
N = kk2_rep.*d1 - kk1_rep.*d2;

% frenet-serret binormals are:
B = kk1_rep.*d1 + kk2_rep.*d2;

% tangents are the same:
T = d3;

% get finite difference operators:
M = length(d1)/3;
s = linspace(0,1,M);
D = finiteDifferences(s,1);
d_s = D{1};

% local curvature derivatives:
k1_s = d_s*k1(:);
k2_s = d_s*k2(:);

% compute torsion:
numer = k1.*k2_s - k1_s.*k2;
k2 = k.^2;
tau = -k3 - numer./k2;

end
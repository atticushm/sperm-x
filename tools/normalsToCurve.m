function [nn,bb] = NormalsToCurve(X, s, method)

% Computes the normals and binormals to the curve parametrised by s. 
% X is the vector of coordinates X(s,t), position vectors to material
% points on the curve centerline.

% number of nodes:
N = length(X)/3;

% matrix representation of coordinates:
X_mat = VectorToMatrix(X);

switch method
    case {'frenet-serret','fs'}
        % Normals are computed via the equations:
        %       t  = X'
        %       t' = C * n
        %       n' = -C * t
        % where C is the planar curvature of the filament, computed through 
        %       C  = |X''|
        % where ' denotes differentiation with respect to arclength.
        % These equations stem from the Frenet-Serret formulas and are ill defined
        % in cases of zero curvature
        
        % tangents to the curve, and their rate of change:
        tt = TangentsToCurve(X, s);
        tt_mat = VectorToMatrix(tt);        % matrix representation.
        tt_s = RatesTangents(tt, s);
        tt_s_mat = VectorToMatrix(tt_s);    % matrix representation.
        
        % curvature is norm of X_ss=t_s:
        C = vecnorm(tt_s_mat); 
        C_rep = repmat(C(:),3,1);           
        
        % normals are thus:
        nn = tt_s./C_rep;
        
        % binormals are b=tXn:
        nn_mat = VectorToMatrix(n);
        bb_mat = cross(tt_mat, nn_mat);
        bb = MatrixToVector(bb_mat);
        
    case {'euler-angles','euler','ea'}
        % Normals are computed via
        %       t = X'
        %       t' = C * n
        % with the filament curvature defined in terms of the euler angles
        % theta, phi, psi of the curve between t and the e_x, e_y, e_z
        % basis vectors respectively.
        
        % setup basis vectors:
        e_x = [1;0;0];      e_y = [0;1;0];      e_z = [0;0;1];
        
        % tangents to the curve:
        tt = TangentsToCurve(X, s);
        
        % euler angles:
        [alpha,beta,gamma] = EulerAngles(X);
        
    case {'director-planar'}
        % Normals are computed via cross products with the fixed binormal
        % bb = [0;0;1]. If:
        %       b = t X n,
        % then, crossing by t from the left and using the triple vector
        % product identity,
        %       n = -t X b
        % provides the normals.
        
        % prescribe out of plane binormals:
        bb_mat = repmat([0;0;1], 1, N);
        bb = MatrixToVector(bb_mat);
        
        % tangents to the curve:
        tt = TangentsToCurve(X, s);
        tt_mat = VectorToMatrix(tt);
        
        % normals to the curve are thus:
        nn_mat = -cross(tt_mat, bb_mat);
        nn = MatrixToVector(nn_mat);

end % function

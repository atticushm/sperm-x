function a_proj = PlaneProjection(a, n)

% Project the vector/vector of coordinates onto a three dimensional plane
% defined by the surface normal n.

N = length(a)/3;
nrep = kron(n,ones(N,1));

a_proj = a - repmat(DotProduct(a,nrep),3,1).*nrep;

end
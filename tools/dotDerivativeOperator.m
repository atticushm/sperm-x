function A = dotDerivativeOperator(a,D)

%DOTDERIVATIVEOPERATOR Build the matrix for the operator dot(a,D*Y), where
%Y is a vector of unknowns and D is a finite difference matrix computing
%derivatives of Y.
%
% Example:
% 
% We want to compute the dot product between a 3Nx1 vector of coordinates a
% and a 3Nx1 vector of unknown derivatives. The derivatives are computed
% using a finite difference matrix D so that dX = D*X, with X being unknown
% coordinate data. This function builds the operator so that dot(a,dX) can
% be computed through a and D. The output A is such that
%   A*X = dot(a,D*X) 
% for each coordinate in a and X.

% number of points:
N = length(a)/3;

% if D is for 3D derivatives, extract single component operator:
if length(D) == 3*N
    D = D(1:N,1:N);
end

% extract components of X:
[a1,a2,a3] = extractComponents(a);

% create derivative operators:
D_a1 = a1.*D;
D_a2 = a2.*D;
D_a3 = a3.*D;
A = [D_a1,D_a2,D_a3];

end % function
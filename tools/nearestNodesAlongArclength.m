function nn = NearestNodesAlongArclength(X_field, q)

% Determines which nodes from X_centres are furthest/nearest to each
% X_field node (directionality determined by string input ineq) at a
% distance of q *measured along a curve arclength*. In this contextm
% X_field are the endpoints of nodes discretising this curve and X_centres
% are the coordinate midpoints.

[x_f, ~, ~] = extractComponents(X_field);
[s_c, ~] = SegmentMidpoints(X_field);

N = length(x_f);            % number of field nodes.
Q = length(s_c);            % number of segments.
% s = linspace(0,1,Q+1);      % nodes along arclength.
[s,~] = DetermineArclength(X_field);

% Arclength distance from each field node to each source/segment midpoint:
dist_s = abs(s(:) - s_c(:)');

% Identify nearest nodes:
nn = zeros(N,Q);
nn(dist_s <= q) = 1;

end % function
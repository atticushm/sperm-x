function ZL = distp(Z)

N = length(Z)/3;
ZL = [Z(N); Z(2*N); Z(3*N)];

end % function
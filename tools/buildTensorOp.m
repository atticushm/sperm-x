function TT = buildTensorOp(varargin)

%buildTensorOp Build the dyadic (tensor) product operator between two vectors T1
%and T2.
%
%See also KRON.

% compute kron:
if nargin == 1
    
    T = varargin{1};
    T1T2 = kron(T,T');
    N = length(T)/3;
      
elseif nargin == 2
  
    T1 = varargin{1}; 
    T2 = varargin{2};
    assert(length(T1)==length(T2),'Inputs are not equal length!')
    T1T2 = kron(T1,T2');
    N = length(T1)/3;
    
end

% extract block matrices:
TTXX = T1T2(1:N,1:N);        
TTXY = T1T2(1:N,N+1:2*N);        
TTXZ = T1T2(1:N,2*N+1:3*N);

TTYX = T1T2(N+1:2*N,1:N);
TTYY = T1T2(N+1:2*N,N+1:2*N);
TTYZ = T1T2(N+1:2*N,2*N+1:3*N);

TTZX = T1T2(2*N+1:3*N,1:N);
TTZY = T1T2(2*N+1:3*N,N+1:2*N);
TTZZ = T1T2(2*N+1:3*N,2*N+1:3*N);

% build dyadic operator:
TT = [diag(diag(TTXX)), diag(diag(TTXY)), diag(diag(TTXZ)); ...
      diag(diag(TTYX)), diag(diag(TTYY)), diag(diag(TTYZ)); ...
      diag(diag(TTZX)), diag(diag(TTZY)), diag(diag(TTZZ))];

end % function
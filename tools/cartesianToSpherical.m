function [theta, phi] = CartesianToSpherical(X0, X1)

% Computed the two angles theta and phi describing the orientation of the
% line joining X0 and X1 in three dimensional space.

% Amounts to changing coordinates from Cartiesian to spherical.

% Vector connecting X0 to X1 is:
v = X1(:) - X0(:);

% Convert to spherical coordinates.
rho = sqrt(v(1)^2 + v(2)^2 + v(3)^2);
theta = atan(v(2)/v(1));                % angle formed with x axis.
phi = acos(v(3)/rho);                   % angle formed with z axis.

end % function
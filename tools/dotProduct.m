function dp = dotProduct(X,Y)

%DOTPRODUCT Performs the scalar product between X and Y, where X and Y are 3Nx1
% vectors of coordinates.

N = length(X)/3;
dp = repmat(eye(N),1,3)*(X.*Y);

end
function Xc = calcFlagCentroid(X)

[X1,X2,X3] = extractComponents(X);
Xc = [mean(X1); mean(X2); mean(X3)];

end % function
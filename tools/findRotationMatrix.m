function RotMat = findRotationMatrix(d1,d2,d3, varargin)

% determine the rotation matrix R such that 

% define lab frame
xx = [1;0;0];
yy = [0;1;0];
zz = [0;0;1];

% express as matrices
d1mat = mat(d1);         % each column is a director vector for the 
d2mat = mat(d2);         %   corresponding flagella node
d3mat = mat(d3);
N = size(d1mat,2);                  % # of nodes

if isempty(varargin)
    method = 'tp';
else
    method = varargin{1};
end

switch method
    case 'ea'
        % not working?
        
        for n = 1:N
            d1n = d1mat(:,n);
            d2n = d2mat(:,n);
            d3n = d3mat(:,n);
            [phi,theta,psi] = FindEulerAngles(d1n,d2n,d3n);
            RotMat(    n,    n) = cos(psi)*cos(theta)*cos(phi) - sin(psi)*sin(phi);
            RotMat(    n,  N+n) = -(cos(psi)*sin(phi) + sin(psi)*cos(theta)*cos(phi));
            RotMat(    n,2*N+n) = sin(phi)*sin(theta);
            RotMat(  N+n,    n) = cos(psi)*cos(theta)*sin(phi) + sin(psi)*cos(phi);
            RotMat(  N+n,  N+n) = cos(psi)*cos(phi) - sin(psi)*cos(theta)*sin(phi);
            RotMat(  N+n,2*N+n) = sin(theta)*sin(phi);
            RotMat(2*N+n,    n) = -cos(psi)*sin(theta);
            RotMat(2*N+n,  N+n) = sin(psi)*sin(theta);
            RotMat(2*N+n,2*N+n) = cos(theta);
        end
        
    case 'tp'
        % see http://solidmechanics.org/Text/Chapter10_2/Chapter10_2.php
        
        for n = 1:N
            d1n = d1mat(:,n);
            d2n = d2mat(:,n);
            d3n = d3mat(:,n);
            R = kron(d1n,xx') + kron(d2n,yy') + kron(d3n,zz');
            R = R';
            RotMat(:,    n) = R(:,1);
            RotMat(:,  N+n) = R(:,2);
            RotMat(:,2*N+n) = R(:,3);
        end
        
    case 'dp'
        % also not working?
        % see http://solidmechanics.org/Text/Chapter10_2/Chapter10_2.php
        
        for n = 1:N
            d1n = d1mat(:,n);
            d2n = d2mat(:,n);
            d3n = d3mat(:,n);
            RotMat(    n,    n) = dot(xx,d1n);
            RotMat(    n,  N+n) = dot(xx,d2n);
            RotMat(    n,2*N+n) = dot(xx,d3n);
            RotMat(  N+n,    n) = dot(yy,d1n);
            RotMat(  N+n,  N+n) = dot(yy,d2n);
            RotMat(  N+n,2*N+n) = dot(yy,d3n);
            RotMat(2*N+n,    n) = dot(zz,d1n);
            RotMat(2*N+n,  N+n) = dot(zz,d2n);
            RotMat(2*N+n,2*N+n) = dot(zz,d3n);
        end
        
end 

end % function
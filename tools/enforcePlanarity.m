function [X,f,om0,d10] = enforcePlanarity(X,f,om0,d10,options)

% forces planarity in variables that, through numerical errors, can become
% non-planar and cause problems.
% planar bending is assumed to be happening in the xy plane (so that
% z-components of variables should be zero).

% number of nodes:
N = length(X)/3;

switch options.PlaneOfBeat
    case 'xy'
        % xy cell
        X(2*N+1:3*N)    = zeros(N,1);
        f(2*N+1:3*N)    = zeros(N,1);
        om0(1:2)        = zeros(2,1);
        d10(3)          = 0;
    case 'xz'
        % xz cell
        X(N+1:2*N)    = zeros(N,1);
        f(N+1:2*N)    = zeros(N,1);
        om0(1)        = 0;
        om0(3)        = 0;
        d10(2)        = 0;
end

end % function
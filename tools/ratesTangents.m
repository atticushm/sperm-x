function tt_s = RatesTangents(tt, s)

% computes the rate of change of the tangent along arclength s by
% differentiating with respect to s.

% get finite difference matrix:
D = finiteDifferences(s, 1);
diff1 = D{1};

% compute derivatives:
tt_s = blkdiag(diff1,diff1,diff1) * tt;

end
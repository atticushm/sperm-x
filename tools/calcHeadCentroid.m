function Yc = calcHeadCentroid(Y)

[Y1,Y2,Y3] = extractComponents(Y);
Yc = [mean(Y1); mean(Y2); mean(Y3)];

end % function
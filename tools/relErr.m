function E = relErr(a,b)

% computes the relative error between two quantities

%{x
if any(vecnorm(b) <= 1e-3)
    % use altered 'relative-ish' norm for small quantities
    E = vecnorm(abs(a-b))./(vecnorm(b)+1);
else
    % standard relative error
    E = vecnorm(a-b)./vecnorm(b);
end
%}

%{
N = size(a,2);
for n = 1:N
    an = a(:,n);
    bn = b(:,n);
%     E(n,1) = norm(abs(an-bn))./(max(norm(an),norm(bn))+1);
    E(n,1) = 2*norm(abs(an-bn))/norm(abs(an)+abs(bn));
end
%}

end % function
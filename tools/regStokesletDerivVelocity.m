function dS = RegStokesletDerivVelocity(X,Y,ep)

%% Compute difference terms:
% Assuming that in the general case X and Y are not of equal length.

N = length(X(:))/3;
M = length(Y(:))/3;

[X1, X2, X3] = extractComponents(X);
[Y1, Y2, Y3] = extractComponents(Y);

r1 = (X1 - Y1')/8/pi;
r2 = (X2 - Y2')/8/pi;
r3 = (X3 - Y3')/8/pi;

rsq = r1.^2 + r2.^2 + r3.^2;
reps = sqrt(rsq + ep^2);
reps2 = rsq + ep^2;
reps5 = reps.^5;

eps2 = ep^2;

%% Derivatives of isotropic term (dX_k A_{ij}):

t1  = -((rsq + 4*eps2))./reps5;
dA1 = r1.*t1;
dA2 = r2.*t1;
dA3 = r3.*t1;

%% Derivatives of dyadic term (dX_k B_{ij}):

r1sq = r1.^2;
r2sq = r2.^2;
r3sq = r3.^2;

t2 = ((reps2 - 3*r1sq))./reps5;
t3 = ((reps2 - 3*r2sq))./reps5;
t4 = ((reps2 - 3*r3sq))./reps5;
t5 = (-3*r1)./reps5;
t6 = (-3*r2)./reps5;
t7 = (-3*r3)./reps5;

dB(1:N,1:M,1)               = (r1.*(2*reps2 - 3*r1sq))./reps5;
dB(1:N,M+1:2*M,1)           = r2.*t2;
dB(1:N,2*M+1:3*M,1)         = r3.*t2;
dB(N+1:2*N,M+1:2*M,1)       = r2sq.*t5;
dB(N+1:2*N,2*M+1:3*M,1)     = r2.*r3.*t5;
dB(2*N+1:3*N,2*M+1:3*M,1)   = r3sq.*t5;

dB(1:N,1:M,2)               = r1sq.*t6;
dB(1:N,M+1:2*M,2)           = r1.*t3;
dB(1:N,2*M+1:3*M,2)         = r1.*r3.*t6;
dB(N+1:2*N,M+1:2*M,2)       = (r2.*(2*reps2 - 3*r2sq))./reps5;
dB(N+1:2*N,2*M+1:3*M,2)     = r3.*t3;
dB(2*N+1:3*N,2*M+1:3*M,2)   = r3sq.*t6;

dB(1:N,1:M,3)               = r1sq.*t7;
dB(1:N,M+1:2*M,3)           = r1.*r2.*t7;
dB(1:N,2*M+1:3*M,3)         = r1.*t4;
dB(N+1:2*N,M+1:2*M,3)       = r2sq.*t7;
dB(N+1:2*N,2*M+1:3*M,3)     = r2.*t4;
dB(2*N+1:3*N,2*M+1:3*M,3)   = (r3.*(2*reps2 - 3*r3sq))./reps5;

%% Combine to find full derivative w.r.t. X(s):

% Add components:
dS{1,1,1} = dA1 + dB(1:N,1:M,1);
dS{1,2,1} =       dB(1:N,M+1:2*M,1);
dS{1,3,1} =       dB(1:N,2*M+1:3*M,1);
dS{2,1,1} =       dS{1,2,1};
dS{2,2,1} = dA1 + dB(N+1:2*N,M+1:2*M,1);
dS{2,3,1} =       dB(N+1:2*N,2*M+1:3*M,1);
dS{3,1,1} =       dS{1,3,1};
dS{3,2,1} =       dS{2,3,1};
dS{3,3,1} = dA1 + dB(2*N+1:3*N,2*M+1:3*M,1);

dS{1,1,2} = dA2 + dB(1:N,1:M,2);
dS{1,2,2} =       dB(1:N,M+1:2*M,2);
dS{1,3,2} =       dB(1:N,2*M+1:3*M,2);
dS{2,1,2} =       dS{1,2,2};
dS{2,2,2} = dA2 + dB(N+1:2*N,M+1:2*M,2);
dS{2,3,2} =       dB(N+1:2*N,2*M+1:3*M,2);
dS{3,1,2} =       dS{1,3,2};
dS{3,2,2} =       dS{2,3,2};
dS{3,3,2} = dA2 + dB(2*N+1:3*N,2*M+1:3*M,2);

dS{1,1,3} = dA3 + dB(1:N,1:M,3);
dS{1,2,3} =       dB(1:N,M+1:2*M,3);
dS{1,3,3} =       dB(1:N,2*M+1:3*M,3);
dS{2,1,3} =       dS{1,2,3};
dS{2,2,3} = dA3 + dB(N+1:2*N,M+1:2*M,3);
dS{2,3,3} =       dB(N+1:2*N,2*M+1:3*M,3);
dS{3,1,3} =       dS{1,3,3};
dS{3,2,3} =       dS{2,3,3};
dS{3,3,3} = dA3 + dB(2*N+1:3*N,2*M+1:3*M,3);

end
function E = relativeError(X,Y)

% Computes the relative error: E = max(|X-Y|/Y)

E = max(abs(X-Y)./Y);

end % function
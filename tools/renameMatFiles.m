function renameMatFiles(calS)

% number of heads considered = number of mat files per calS
num_fixvol = 300;

% find file names
mname_pre = './head_morphology_eif/data/';
dname = dir([mname_pre, sprintf('fixed_data_%02g/*.mat', calS)]);

for i = 1:num_fixvol
    mname = dname(i).name;
    
    % extract numbers
    nums = sscanf(mname,'fixed_%d_%d.mat');
    
    % new filename
    fname = sprintf('fixed_%02g_%03g.mat', nums(1), nums(2));
    
    % rename file
    old_dir = [pwd,sprintf('/head_morphology_eif/data/fixed_data_%02g/',calS), mname];
    new_dir = [pwd,sprintf('/head_morphology_eif/data/fixed_data_%02g/',calS), fname];
    if ~strcmpi(old_dir,new_dir)
        movefile(old_dir, new_dir, 'f')
    end
end

end
function out = finiteDifferences(x, n, varargin)

% Builds the matrices arising from approximating the one-dimensional 
% derivatives 1 thru 5 of a function f(x) with respect to grid points defined in x.

% n is the order(s) of derivtive requested.

% some special cases (not used in SPX code)
if isempty(varargin) || isempty(varargin{1})
    opt = [];
elseif strcmp(varargin{1},'no_head')
    opt = [];
elseif strcmp(varargin{1},'head_tangent')
    opt = 'head_tangent';
elseif strcmp(varargin{1},'head_node')
    opt = 'head_node';
else
    warning('Unrecognised optional input.')
    return
end

%% automatic coefficient generation

% define persistent variables, so that differences matrices are only
% generated once
persistent D_1 D_2 D_3 D_4 D_5 M
if isempty(M)
    M = length(x);
end

% if s changes (between runSPX calls, for example), difference matrices
% need rebuilt
if M ~= length(x)
    M = length(x);
    D_1 = []; D_2 = []; D_3 = []; D_4 = []; D_5 = [];
end

% define parameters
h = 1/(M-1);                % constant grid spacing
out = cell(length(n),1);    % output structure

% build operators
if any(n==1)
    if isempty(D_1)
        D_1 = 1/h * buildDifferenceScheme(1,M,opt);
    end
    out{n==1} = D_1;
end
if any(n==2)
    if isempty(D_2)
        D_2 = 1/h^2 * buildDifferenceScheme(2,M,opt);
    end
    out{n==2} = D_2;
end
if any(n==3)
    if isempty(D_3)
        D_3 = 1/h^3 * buildDifferenceScheme(3,M,opt);
    end
    out{n==3} = D_3;
end
if any(n==4)
    if isempty(D_4)
        D_4 = 1/h^4 * buildDifferenceScheme(4,M,opt);
    end
    out{n==4} = D_4;
end
if any(n==5)
    if isempty(D_5)
        D_5 = 1/h^5 * buildDifferenceScheme(5,M,opt);
    end
    out{n==5} = D_5;
end

%% hard coded difference schemes

%{
M = length(x);
h = 1/(M-1);
out = cell(length(n),1);

if any(n==1)
    D_1 = -1/2 * diag(ones(M-1,1),-1) + ...
           1/2 * diag(ones(M-1,1), 1) ; 
    D_1(1,:) = [-3/2, 2, -1/2, zeros(1,M-3)];
    D_1(M,:) = -fliplr(D_1(1,:));
    D_1 = 1/h * D_1;
    out{n==1} = D_1;
end
if any(n==2)
    D_2 =  1 * diag(ones(M-1,1),-1) + ...
          -2 * diag(ones(M,1),0) + ...
           1 * diag(ones(M-1,1), 1) ;
    D_2(1,:) = [35/12, -26/3, 19/2, -14/3, 11/12, zeros(1,M-5)];
    D_2(M,:) = fliplr(D_2(1,:));
    D_2 = 1/h^2 * D_2;
    out{n==2} = D_2;
end
if any(n==3)
    D_3 = -1/2 * diag(ones(M-2,1),-2) + ...
             1 * diag(ones(M-1,1),-1) + ...
            -1 * diag(ones(M-1,1), 1) + ...
           1/2 * diag(ones(M-2,1),2) ;
    D_3(1,:) = [-17/4, 71/4, -59/2, 49/2, -41/4, 7/4, zeros(1,M-6)];
    D_3(2,:) = 1/8*[-15, 56, -83, 64, -29, 8, -1, zeros(1,M-7)];
    D_3(M,:) = -fliplr(D_3(1,:));
    D_3(M-1,:) = -fliplr(D_3(2,:));
    D_3 = 1/h^3 * D_3;
    out{n==3} = D_3;
end
if any(n==4)
    D_4 =  1 * diag(ones(M-2,1),-2) + ...
          -4 * diag(ones(M-1,1),-1) + ...
           6 * diag(ones(M,1),0) + ...
          -4 * diag(ones(M-1,1),1) + ...
           1 * diag(ones(M-2,1),2);
    D_4(1,:) = [35/6, -31, 137/2, -242/3, 107/2, -19, 17/6, zeros(1,M-7)];
    D_4(2,:) = 1/6*[21, -112, 255, -324, 251, -120, 33, -4, zeros(1,M-8)];
    D_4(M,:) = fliplr(D_4(1,:));
    D_4(M-1,:) = fliplr(D_4(2,:));
    D_4 = 1/h^4 * D_4;
    out{n==4} = D_4;
end
if any(n==5)
    D_5 = -1/2 * diag(ones(M-3,1),-3) + ...
             2 * diag(ones(M-2,1),-2) + ...
          -5/2 * diag(ones(M-1,1),-1) + ...
           5/2 * diag(ones(M-1,1),+1) + ...
            -2 * diag(ones(M-2,1),+2) + ...
           1/2 * diag(ones(M-3,1),+3) ;
    D_5(1,:) = [-7/2, 20, -95/2, 60, -85/2, 16, -5/2, zeros(1,M-7)];
    D_5(2,:) = 1/6*[-35, 234, -685, 1150, -1215, 830, -359, 90, -10, zeros(1,M-9)];
    D_5(3,:) = 1/6*[-10, 55, -126, 155, -110, 45, -10, 1, zeros(1,M-8)];
    D_5(M,:) = -fliplr(D_5(1,:));
    D_5(M-1,:) = -fliplr(D_5(2,:));
    D_5(M-2,:) = -fliplr(D_5(3,:));
    D_5 = 1/h^5 * D_5;
    out{n==5} = D_5;
end
%}

end % function
function setXYZLabels(varargin)

% get axes object
if ~isempty(varargin)
    ax = varargin{1};
else
    ax = gca;
end

% set labels
ax.XLabel.String = '$x$';
ax.YLabel.String = '$y$';
ax.ZLabel.String = '$z$';

% set font size
ax.XLabel.FontSize = 14;
ax.YLabel.FontSize = 14;
ax.ZLabel.FontSize = 14;

% set latex interpreter
ax.XLabel.Interpreter = 'latex';
ax.YLabel.Interpreter = 'latex';
ax.ZLabel.Interpreter = 'latex';

end % function
function [y,Y] = GenerateReferenceNodes(H,hdims)

%GENERATEREFERENCENODES generates the cell head traction and
%quadrature/kernel discretisations, y and Y repsectively, given H, the
%number of nodes per face of a cubic for cubic projection, and hdims, a 3x1
%vector indicating the length of the axes of the cell head.

% build spherical discretisations (will be for the head):
Sph_t = GenerateSphereDisc(H,1); 
H_q = floor(3*H);
Sph_q = GenerateSphereDisc(H_q,1);

% odd H will duplicate X0: remove from discretisations:
if mod(H,2)==1
    Sph_t = RemoveNode(Sph_t, [1;0;0]);
    disp('Node removed from traction disc.')
end
if mod(H_q,2)==1
    Sph_q = RemoveNode(Sph_q, [1;0;0]);
    disp('Node removed from quadrature disc.')
end

% head surface traction discretisation:
M_t = length(Sph_t)/3;          
y = kron(hdims(:),ones(M_t,1)).*Sph_t(:);  

% head surface quadrature discretistaion:
M_q = length(Sph_q)/3;
Y = Sph_q.*kron(hdims(:),ones(M_q,1));

end
function out = head(X,varargin)

if size(X,2) == 1
    X = mat(X);
end

if isempty(varargin)
    out = (X(:,1:10));
else
    out = (X(:,1:varargin{1}));
end

end % function
function [S_s, S_X] = RegStokesletNumericalDerivatives(X_field, X_source, epsilon)

% Function for calculating the derivatives of the regularised stokeslet
% numerically (using finite differences), for use in testing the
% analytically calculated regularised stokeslet derivative codes.

% X_field is a 3Nx1 vector of coordinates of field points.
% X_source is a 3Mx1 vector of coordinates of source points.
% epsilon is the regularisation parameter.

%% Compute regularised stokeslets:

S = RegStokeslets(X_field, X_source, epsilon);

%% Generate finite difference operator:

N_field = length(X_field)/3;
s_field = linspace(0,1,N_field);
d_s = cell2mat(BuildFiniteDifferenceMatrices(s_field, 1, 'equispaced'));

D_s = blkdiag(d_s,d_s,d_s);    % repeat scheme on each matrix component of S.

%% Numerically differentiate:

S_s = D_s * S;

S_X = [];

end
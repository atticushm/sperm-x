function D = buildDifferenceScheme(order,N,varargin)

% order is the order of the derivative to be approximated.
% N is the number of nodes
% varargin allows for some special cases - see below

% set order of schemes in interior and boundaries
n_p = order+2;          % sufficient for order 2 accuracy in interior.
n_b = n_p+1;            % higher-order differences at the boundaries.

% set stencil indexing
if mod(n_p,2)==0
    int_stencil = -n_p/2:n_p/2;
else
    int_stencil = -floor(n_p/2):floor(n_p/2);
end

% compute coefficients from stencil
int_cf = finiteDifferenceCoefficients(int_stencil,order);

%% Interior scheme:

int_D = zeros(N);
for i = 1:length(int_stencil)
    int_D = int_D + diag(repmat(int_cf(i),1,N-abs(int_stencil(i))),int_stencil(i));
end

%% Add boundary scheme:
    
% Higher order derivatives require additional boundary condition
% schemes - compute as many as needed.
for i = 1:floor(n_p/2)
    prox_stencil = -(i-1):n_b-(i-1);
    dist_stencil = -n_b+(i-1):(i-1);
    prox_cf = finiteDifferenceCoefficients(prox_stencil, order);
    dist_cf = finiteDifferenceCoefficients(dist_stencil, order);
    bc_D(i).prox = [prox_cf, zeros(1,N-length(prox_cf))];
    bc_D(i).dist = [zeros(1,N-length(dist_cf)), dist_cf];
end

D = int_D;
for i = 1:n_p/2
    D(i,:) = bc_D(i).prox;
    D(N-(i-1),:) = bc_D(i).dist;
end

if strcmp(varargin{1},'head_tangent')
    
    % Including a cell head requires using a phantom point within the head.
    % Rather than using this in the finite difference scheme (see 'extra'
    % mode), we want to use the head tangent tH.
    
    % The phantom point is Y* = X0 - ds*tH. By appropriately editing the
    % scheme the derivative at X0 will use tH in its computation rather
    % than Y*.
    
    % Only derivatives of first point should multiply onto point in the
    % head (so that equations on the interior dont require head point to
    % calculate derivatives of)
    
    % Extend stencil and compute coefficients:
    prox_stencil = -1:n_b+1;
%     prox_stencil = -1:n_p;
    prox_cf = FiniteDifferenceCoefficients(prox_stencil, order);
    
    h = 1/(N-1);
    a = prox_cf(1);
    b = prox_cf(2);
    prox_cf(1) = -h*a;       % multiplies onto tH
    prox_cf(2) = a+b;       % multiplies onto X0
    
    % Augment D:
    D = [zeros(N,1), D];
    D(1,:) = [prox_cf, zeros(1,N+1-length(prox_cf))];
    
elseif strcmp(varargin{1},'head_node')
    
    % Including a cell head introduces an additional interior head node
    % Yds with which to compute derivatives at the proximal end. 
    % Distal conditions remain unchanged.
    
    % Only derivatives of first point should multiply onto point in the
    % head (so that equations on the interior dont require head point to
    % calculate derivatives of)
    
    % Replace first row with augemented stencil
    prox_stencil = -1:n_b+1;
    prox_cf = FiniteDifferenceCoefficients(prox_stencil, order);
    
    D = [zeros(N,1), D];
    D(1,:) = [prox_cf, zeros(1,N+1-length(prox_cf))];
    
end

end
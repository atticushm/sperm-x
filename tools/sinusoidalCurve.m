function varargout = sinusoidalCurve(k, a, M)

% Generates a sinusoidal curve reminiscent of a sperm beat.
% k is the wavenumber of the curve.
% a is the amplitude of the wave.
% M is the desired number of nodes.

% Extract variables from model:
Q = M - 1;

% Define high-resolution sine curve:
x = linspace(0,1,1e6);    x = x(:);
% y = a*x.*sin(k*x(:));
y = a*sin(k*x(:));
z = zeros(1e6,1);

% Scale helix to be of unit arclength:
ds = sqrt(diff(x).^2 + diff(y).^2 + diff(z).^2);
s = [0; cumsum(ds(:))];
delta = max(s)/Q;
ss = 0:delta:max(s);

xx = spline(s,x,ss);
yy = spline(s,y,ss);
zz = spline(s,z,ss);

x = xx/max(s); 
y = yy/max(s);
z = zz/max(s);
X = [x(:); y(:); z(:)];

% Outputs
nOut = nargout;
if nOut <= 1
    varargout{1} = X(:);
elseif nOut == 2
    varargout{1} = X(:);
    varargout{2} = ss(:)/max(s);
elseif nOut == 3
    varargout{1} = x(:);
    varargout{2} = y(:);
    varargout{3} = z(:);
elseif nOut == 4
    varargout{1} = X(:);
    varargout{2} = x(:);
    varargout{3} = y(:);
    varargout{4} = z(:);
end

end % function
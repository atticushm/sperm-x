function [act_curr, act_next] = ActiveMoment(t, params)

% extract parameters from model struct:
N = params.NF;              % number of flagellar nodes.
k = params.wavk;                 % wave number of actuating beat.
calS4 = params.calS4;            % fourth power of swimming parameter.

% time at current time step:
t_curr = t;
t_next = t + params.dt;

% inactive end region length:
ell = params.ell;

% arclength discretisation:
s = linspace(0,1,N); 
s = s(:);

% 'warm up' by steadily increasing m0:
warm_up_beats = params.warm_up;
if t < warm_up_beats*k
    calM = params.calM * (t/warm_up_beats/k);
else
    calM = params.calM;
end

for i = 1:2
    
    % select current or next time step:
    if i == 1
        t = t_curr;
    else
        t = t_next;
    end

    % w/o inactive endpiece:
    % simple travelling wave.
    if ell == 0
        % moment per unit length:
        m = cos(k*s(:) -t);  

        % analytical derivative:
        m_s = -k*sin(k*s(:)-t);

        % analytical integral across arclength:
        M = 1/k * (sin(k-t)+sin(t));
    end

    % w/ inactive endpiece:
    % convolve with tanh to model cutoff.
    if ell > 0
        % choose slope parameter to nicely model cutoff:
        a = 40;

        % actuation function (moment per unit length):
        m = -1/2 * cos(k*s - t) .* (tanh(a*s - a*(1-5/2*ell)) -1);

        % analytical derivative:
        %m_s = k/2 * (tanh(a*s - a*(1-5/2*ell)) -1) .* sin(k*s-t) - ...
        %                     a/2 * cos(k*s-t) .* (sech(a*s - a*(1-5/2*ell))).^2;
        
        % numerical derivative:
        D = finiteDifferences(s, 1);
        d1 = D{1};
        m_s = d1*m;

        % numerically integrate (midpoint rule):
        ds = 1/(N-1);
        M = sum(0.5*ds*(m(1:end-1)+m(2:end)));
    end

    % scale appropriately:
    m   = calS4 * calM * m;
    m_s = calS4 * calM * m_s;
    M   = calS4 * calM * M;
    
    % store in output structures:
    if i == 1
        act_curr = struct('t',t,'m',m(:),'m_s',m_s(:),'M',M);
    else
        act_next = struct('t',t,'m',m(:),'m_s',m_s(:),'M',M);
    end
    
end

end % function
function err = ConvergenceMSE(X, s_x, Y, s_y, varargin)

Nx = length(X);
Ny = length(Y);

% Interpolate if not the same length:
if Nx ~= Ny
    % Relabel so that the smaller vector is labelled X:
    if Ny < Nx
        Z = Y; Y = X; X = Z; clear Z
        s_z = s_y; s_y = s_x; s_x = s_z; clear s_z
    end
    [x_1,x_2,x_3] = extractComponents(X);

    x_i = interp1(s_x, x_1, s_y);
    y_i = interp1(s_x, x_2, s_y);
    z_i = interp1(s_x, x_3, s_y);

    X_i = [x_i(:); y_i(:); z_i(:)];
    X = X_i(:);
end

% Compute MSE:
err = (1/Nx) * sum((X-Y).^2);

if ~isempty(varargin) && varargin{1} == 1
    err = err.^(1/2);
end

end % function
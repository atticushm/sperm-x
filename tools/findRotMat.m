function R = findRotMat(a,b)

% finds the rotation matrix mapping unit vector a onto unit vector b
% method via https://math.stackexchange.com/a/476311

assert(norm(a)==1, 'a is not unit!')
assert(norm(b)==1, 'b in not unit!')

v = cross(a,b);
c = dot(a,b);

% skew-symmetric cross product operator of v
vx = [0,-v(3),v(2); v(3),0,-v(1); -v(2),v(1),0];

% rotation matrix is
R = eye(3) + vx + vx^2*(1/(1+c));

end % function
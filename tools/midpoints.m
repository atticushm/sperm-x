function Xm = Midpoints(X)

%MIDPOINTS Compute the midpoints of 3D coordinate data.
% Xm = Midpoints(X) returns midpoint values of the vector of coordinates X.
% If X is length 3Nx1, the output Xm will be 3(N-1)x1.
%
% See also extractComponents.

N = length(X)/3;

[x1,x2,x3] = extractComponents(X);

xm1 = 0.5*(x1(1:N-1)+x1(2:N));
xm2 = 0.5*(x2(1:N-1)+x2(2:N));
xm3 = 0.5*(x3(1:N-1)+x3(2:N));

Xm = [xm1(:); xm2(:); xm3(:)];

end
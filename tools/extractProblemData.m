function [head,flag,NN] = extractProblemData(Y,model)

% get flagellum coordinates
flag = extractFlagellumData(Y,model);

% get head coordinates
head = extractHeadData(Y,model);

% construct nearest-neighbour matrix
NN   = NearestNeighbourMatrix(head.XQuad(:),head.XTrac(:),'cpu');

end
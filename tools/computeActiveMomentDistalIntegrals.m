function I = computeActiveMomentDistalIntegrals(d1,d2,d3,m1,m2,m3,params)

% discretisation parameters
N = length(d1)/3;
ds = 1/(N-1);

% modelling parameters
calS4 = params.calS4;
calM1 = params.calM1;
calM2 = params.calM2;
calM3 = params.calM3;

% components of directors
[d1x,d1y,d1z] = extractComponents(d1);
[d2x,d2y,d2z] = extractComponents(d2);
[d3x,d3y,d3z] = extractComponents(d3);

%% active moment integral

% arguments of integral
argx = calS4 * (calM1*m1.*d1x + calM2*m2.*d2x + calM3*m3.*d3x);
argy = calS4 * (calM1*m1.*d1y + calM2*m2.*d2y + calM3*m3.*d3y);
argz = calS4 * (calM1*m1.*d1z + calM2*m2.*d2z + calM3*m3.*d3z);

% integrals by trapezium rule
id  = triu(2*ones(N),1) + diag(ones(N,1)); id(:,end) = ones(N,1);
id(end,:) = zeros(1,N);

Ix = ds/2 * id*argx(:);
Iy = ds/2 * id*argy(:);
Iz = ds/2 * id*argz(:);

% vector integral
I = [Ix(:); Iy(:); Iz(:)];

%% summation using GL

%{
% arguments of integral at flag nodes
argx = calS4 * (calM1*m1.*d1x + calM2*m2.*d2x + calM3*m3.*d3x);
argy = calS4 * (calM1*m1.*d1y + calM2*m2.*d2y + calM3*m3.*d3y);
argz = calS4 * (calM1*m1.*d1z + calM2*m2.*d2z + calM3*m3.*d3z);

% flag nodes
s = linspace(0,1,N);
for n = 1:N-1
    % interval of integration
    a = s(n);
    b = s(N);
    
    % quad nodes and weights
    M = floor(80*(b-a))+20;
    [sq, w] = lgwt(M,a,b);
    
    % spline to get args at quad nodes
    agx = spline(s, argx, sq);
    agy = spline(s, argy, sq);
    agz = spline(s, argz, sq);
    
    % sum to integrate
    Im(:,n) = [sum(w.*agx); sum(w.*agy); sum(w.*agz)];
end
Im(:,N) = zeros(3,1);
I = vec(Im);
%}

end
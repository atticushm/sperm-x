function varargout = solveResistanceProblem(x, X, r, eps, coeff, kernel)

% number of traction/force nodes
N = length(x)/3; 

% nearest neighbour matrix
NN = nearestNeighbourMatrix(X, x, 'cpu');

% hydrodynamic interaction matrix
switch kernel
    case 'stokeslets'
        S = regStokeslet(x, X, eps, 'cpu');
    case 'blakelets'
        S = regBlakelet(x, X, eps);
end    

% left hand sides for resistance problems
LHS_U = kron(eye(3),ones(N,1));
dx = x(:)-kron(r,ones(N,1));
dxM = mat(dx);
LHS_Omg = zeros(3*N,3);
for ii = 1:N
    cp_1 = cross([1;0;0],dxM(:,ii));
    cp_2 = cross([0;1;0],dxM(:,ii));
    cp_3 = cross([0;0;1],dxM(:,ii));
    
    LHS_Omg(ii,1)       = cp_1(1);
    LHS_Omg(ii+N,1)     = cp_1(2);
    LHS_Omg(ii+2*N,1)   = cp_1(3);
    LHS_Omg(ii,2)       = cp_2(1);
    LHS_Omg(ii+N,2)     = cp_2(2);
    LHS_Omg(ii+2*N,2)   = cp_2(3);
    LHS_Omg(ii,3)       = cp_3(1);  
    LHS_Omg(ii+N,3)     = cp_3(2);
    LHS_Omg(ii+2*N,3)   = cp_3(3);
end
LHS = coeff*[LHS_U, LHS_Omg];

% solve for force density distribution
f = -(S*NN)\LHS;

% compute total force and moment 
for k = 1:6
    F(:,k) = calcBodyForce(f(:,k), NN);
    M(:,k) = calcBodyMoment(X, r, f(:,k), NN);
end

% assign F and M to resistance matrices
RFU = F(:,1:3);  % force-velocity resistance
RFO = F(:,4:6);  % force-rotation coupling
RMU = M(:,1:3);  % moment-velocity coupling
RMO = M(:,4:6);  % moment-rotation resistance

% compile into grand resistance matrix
R = [RFU, RFO; RMU, RMO];

% outputs
if nargout == 1
    varargout{1} = R;
elseif nargout == 4
    varargout{1} = RFU;
    varargout{2} = RFO;
    varargout{3} = RMU;
    varargout{4} = RMO;
end

end % function
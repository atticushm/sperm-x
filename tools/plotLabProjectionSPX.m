function plotLabProjectionSPX(X, Y)

% plots a swimming cell in a fixed lab frame axis, viewed from each plane,
% to facilitate comparisons to cells viewed through a microscope, for
% example.

% this function should only be supplied data which you want to plot -- it
% does not do any filtering/sampling of the solution data.

% this function is specific to SPX model

% number of time points/solutions provided
num_tps = size(X,2);

% numbers of nodes
N = size(X,1);
M = size(Y,1);

% setup colors
cols = parula(num_tps);

% define 'lab frame' axes -- this will need manual editing come final
% figure generation to get looking nice
xlim_lab = [-0.5, 1];
ylim_lab = [-0.3, 0.3];
zlim_lab = [-0.3, 0.3];

end % function
function X = integrateTheta(X0, theta)

% compute coordinates X=[x1;x2;x3] from leading point X0 and tangent angles
% theta
% curve is assumed to be xy planar

% segment length
Q  = length(theta);
ds = 1/Q;

% integrate using midpoint rule
x1(1) = X0(1);
x2(1) = X0(2);
for i = 1:Q
    x1(i+1) = x1(i) + ds*cos(theta(i));
    x2(i+1) = x2(i) + ds*sin(theta(i));
end
x3 = X0(3)*ones(Q+1,1);

% stack components
X = [x1(:); x2(:); x3(:)];

end % function
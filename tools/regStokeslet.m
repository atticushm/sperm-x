function S=regStokeslet(x,X,eps,procFlag)
% x is a vector of field points:  3*M
% X is a vector of source points: 3*Q
% eps is regularisation parameter
% outputs an array of regularised stokeslets between field and source points
% blocks are [Sxx, Sxy, Sxz; Syx, Syy, Syz; Szx, Szy, Szz] where Sxx is M x Q etc.

% x=x(:);
% X=X(:);
% M=length(x)/3;
% Q=length(X)/3;
% r1=      x(1:M) - X(1:Q)';
% r2=  x(M+1:2*M) - X(Q+1:2*Q)';
% r3=x(2*M+1:3*M) - X(2*Q+1:3*Q)';
% rsq=r1.^2+r2.^2+r3.^2;
% ireps3=1./(sqrt((rsq+eps^2)).^3);
% isotropic=kron(eye(3),(rsq+2.0*eps^2).*ireps3);
% dyadic=[r1.*r1 r1.*r2 r1.*r3; r2.*r1 r2.*r2 r2.*r3; ...
%     r3.*r1 r3.*r2 r3.*r3].*kron(ones(3,3),ireps3);

x=x(:);
X=X(:);
M=length(x)/3;
Q=length(X)/3;

switch procFlag
    case 'cpu'
        r1=      x(1:M) - X(1:Q)';
        r2=  x(M+1:2*M) - X(Q+1:2*Q)';
        r3=x(2*M+1:3*M) - X(2*Q+1:3*Q)';
        rsq=r1.^2+r2.^2+r3.^2;
        ireps3=1./(sqrt((rsq+eps^2)).^3);
        
        if M*Q > 1e5
            % kron will be huge -- needs split
            id = (rsq+2.0*eps^2).*ireps3;
            ze = 0*id;
            ix = [id, ze, ze];
            iy = [ze, id, ze];
            iz = [ze, ze, id];
            
            kr = kron(ones(1,3),ireps3);
            dx = [r1.*r1 r1.*r2 r1.*r3].*kr;
            dy = [r2.*r1 r2.*r2 r2.*r3].*kr;
            dz = [r3.*r1 r3.*r2 r3.*r3].*kr;
            
            Sx = 1/8/pi * (ix+dx);
            Sy = 1/8/pi * (iy+dy);
            Sz = 1/8/pi * (iz+dz);
            S  = [Sx; Sy; Sz];
        else
            % kron will be below 8gb
            isotropic=kron(eye(3),(rsq+2.0*eps^2).*ireps3);
            dyadic=[r1.*r1 r1.*r2 r1.*r3; r2.*r1 r2.*r2 r2.*r3; ...
                r3.*r1 r3.*r2 r3.*r3].*kron(ones(3,3),ireps3);
            S=(1.0/(8.0*pi))*(isotropic+dyadic);
        end
        
    case 'gpu'
        isotropic = kron(eye(3),((x(1:M) - X(1:Q)').^2 ...
            + (x(M+1:2*M) - X(Q+1:2*Q)').^2 ...
            + (x(2*M+1:3*M) - X(2*Q+1:3*Q)').^2 + 2.0*eps^2) ...
            ./(sqrt(( ...
            (x(1:M) - X(1:Q)').^2 ...
            + (x(M+1:2*M) - X(Q+1:2*Q)').^2 ...
            + (x(2*M+1:3*M) - X(2*Q+1:3*Q)').^2+eps^2)).^3));
        
        dyadic = [(x(1:M) - X(1:Q)').*(x(1:M) - X(1:Q)') , ...
            (x(1:M) - X(1:Q)').*(x(M+1:2*M) - X(Q+1:2*Q)') , ...
            (x(1:M) - X(1:Q)').*(x(2*M+1:3*M) - X(2*Q+1:3*Q)') ; ...
            (x(1:M) - X(1:Q)').*(x(M+1:2*M) - X(Q+1:2*Q)') , ...
            (x(M+1:2*M) - X(Q+1:2*Q)').*(x(M+1:2*M) - X(Q+1:2*Q)') , ...
            (x(M+1:2*M) - X(Q+1:2*Q)').*(x(2*M+1:3*M) - X(2*Q+1:3*Q)') ; ...
            (x(1:M) - X(1:Q)').*(x(2*M+1:3*M) - X(2*Q+1:3*Q)') , ...
            (x(M+1:2*M) - X(Q+1:2*Q)').*(x(2*M+1:3*M) - X(2*Q+1:3*Q)') , ...
            (x(2*M+1:3*M) - X(2*Q+1:3*Q)').*(x(2*M+1:3*M) - X(2*Q+1:3*Q)')] ...
            .*kron(ones(3,3),1./(sqrt(( ...
            (x(1:M) - X(1:Q)').^2 ...
            + (x(M+1:2*M) - X(Q+1:2*Q)').^2 ...
            + (x(2*M+1:3*M) - X(2*Q+1:3*Q)').^2+eps^2)).^3));
        
        S=(1.0/(8.0*pi))*(isotropic+dyadic);
        
        clearvars isotropic dyadic
    otherwise
        error('PROCFLAG NOT DEFINED IN REGSTOKESLET.M')
end

end

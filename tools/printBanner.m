function printBanner(str,varargin)

if isempty(varargin)
    char = '-';
else
    char = varargin{1};
end

% print banner:
printLinebreak(char)
disp(upper(str))
printLinebreak(char)

end
function R = RotateGRM(R_ref, nH, nH_ref)

% Transforms the grand resistance matrix R_ref, with normal nH_ref, to
% obtain the grand resistance matrix R with normal nH.

% compute rotation matrix:
Reflection = @(u,n) u-2*n*(n'*u)/(n'*n);
S = Reflection(eye(3),nH_ref+nH);
RotMat = Reflection(S, nH);

% transform grand resistance matrix:
R = TransformGRM(R_ref, RotMat, zeros(3,1));

end
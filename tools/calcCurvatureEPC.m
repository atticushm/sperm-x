function kap = calcCurvatureEPC(Z)

% computes the curvature from tangent angle data
% relative to the cell head

% specific to endpiece/EIF angle formulation methods!

% number of time points
num_tbeat = size(Z,2);

% get body frame tangent angles
for i = 1:num_tbeat
    th = Z(3:end,i);
    N  = length(th);
    ds = 1/(N-1);
    kap(:,i) = diff(th)/ds;
end

end % function
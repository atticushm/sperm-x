function UX = normalise(X)

% normalises a given input vector of coordinates

XM = vectorToMatrix(X);
XM = XM./vecnorm(XM);
UX = matrixToVector(XM);

end % function
function varargout = SegmentMidpoints(X)

% Returns segment midpoints coordinates and arclength values at midpoints
% given a centreline discretisation X.

    
% Get coordinate components and arclength:
N = length(X)/3;
s = linspace(0,1,N);

% Arclength midpoints:
s_c = 0.5*( s(1:end-1) + s(2:end) );
out{1} = s_c;

% Compute midpoints:
[x,y,z] = extractComponents(X);
x_c = 0.5 * (x(2:end)+x(1:end-1));
y_c = 0.5 * (y(2:end)+y(1:end-1));
z_c = 0.5 * (z(2:end)+z(1:end-1));
X_c = [x_c(:); y_c(:); z_c(:)];

out{2} = X_c;

% Tangents at midpoints:
D = finiteDifferences(s, 1); D1 = D{1};
dx = D1 * x(:); 
dy = D1 * y(:);
dz = D1 * z(:);
dx_c = 0.5 * (dx(2:end)+dx(1:end-1));
dy_c = 0.5 * (dy(2:end)+dy(1:end-1));
dz_c = 0.5 * (dz(2:end)+dz(1:end-1));
dX_c = [dx_c(:); dy_c(:); dz_c(:)];

out{3} = dX_c;
    
%% Outputs:

for n = 1:nargout
    varargout{n} = out{n};
end

end % function
function varargout = LinearInitialCondition( M )

% Generates a linear initial condition along the lab frame x-axis.
% M is the desired number of nodes.

% Extract variables from model:
ds = 1/(M - 1);
ds = repmat(ds, M-1, 1);

x = [0; cumsum(ds)];
y = zeros(M,1);
z = zeros(M,1);

X = [x(:); y(:); z(:)];

% Outputs
nOut = nargout;
if nOut <= 1
    varargout{1} = X(:);
elseif nOut == 3
    varargout{1} = x(:);
    varargout{2} = y(:);
    varargout{3} = z(:);
elseif nOut == 4
    varargout{1} = X(:);
    varargout{2} = x(:);
    varargout{3} = y(:);
    varargout{4} = z(:);
end

end % function
function I = regBlakeletNumericalIntegrals(x, X, epsilon)

% computes the regularised blakelet integral at field points x driven by
% forces at source points X using the repeated midpoint rule, with
% intervals chosen so that the absciccae are the field points.
% x is field points
% X is source points

M = length(x)/3;
N = length(X)/3;
ds = 1/(N-1);

%% numerical integration (midpoint rule)

%{
% number of source points
N = length(X)/3;

% number of interior points 
M = N-2;

% 'full' segment length is 
ds = 1/(N-1);

% matrix representations
X_mat = mat(X);

% proximal half segment
% abscissa is at X(h/4)
Xm0 = 1/2*(X_mat(:,1) + 1/2*(X_mat(:,1)+X_mat(:,2)));
I1  = ds/2 * regBlakelet(x, Xm0, epsilon);

% interior full segments, centred about nodes
X_mat = mat(X);
X_int = vec(X_mat(:,2:N-1));
Iint  = ds * regBlakelet(x, X_int, epsilon);

% distal half segment
XmN = 1/2*(X_mat(:,N) + 1/2*(X_mat(:,N)+X_mat(:,N-1)));
IN  = ds/2 * regBlakelet(x, XmN, epsilon);

% build stokeslet integral operator
D = [I1(:,1), Iint(:,1:M), IN(:,1) , ...
     I1(:,2), Iint(:,M+1:2*M), IN(:,2) , ...
     I1(:,3), Iint(:,2*M+1:3*M), IN(:,3) ];
%}

%% trapezium rule

% stokeslets and submatrices
B = regBlakelet(x,X,epsilon);
BXX = B(1:M,1:N);
BXY = B(1:M,N+1:2*N);
BXZ = B(1:M,2*N+1:3*N);
BYX = B(M+1:2*M,1:N);
BYY = B(M+1:2*M,N+1:2*N);
BYZ = B(M+1:2*M,2*N+1:3*N);
BZX = B(2*M+1:3*M,1:N);
BZY = B(2*M+1:3*M,N+1:2*N);
BZZ = B(2*M+1:3*M,2*N+1:3*N);

% integrator blocks
id  = repmat([1,2*ones(1,N-2),1],M,1);
IXX = ds/2 * id.*BXX;
IXY = ds/2 * id.*BXY;
IXZ = ds/2 * id.*BXZ;
IYX = ds/2 * id.*BYX;
IYY = ds/2 * id.*BYY;
IYZ = ds/2 * id.*BYZ;
IZX = ds/2 * id.*BZX;
IZY = ds/2 * id.*BZY;
IZZ = ds/2 * id.*BZZ;

% integrator operator
I = [IXX,IXY,IXZ; IYX,IYY,IYZ; IZX,IZY,IZZ];
    
end % function 
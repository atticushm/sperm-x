%{
function S = combineMatrices(S1, S2)

Combines the two matrices S1 and S2.
S1 and S2 must have the same number of rows.
TODO: generalise to N input matrices?

% Check compatability:
if size(S1,1) ~= size(S2,1)
    warning('Matrices have nonequal numbers of rows, expect to fail!')
end

% Get size data from each matrix:
dim_S1 = size(S1);  dim_S2 = size(S2);
n_r = dim_S1(1)/3;     m_r = dim_S2(1)/3;     % number of coordinate rows.
n_c = dim_S1(2)/3;     m_c = dim_S2(2)/3;     % number of coordinate columns.

% Extract components from each matrix:
S1_xx = S1(      1:n_r,         1:n_c); 
S1_xy = S1(      1:n_r,     n_c+1:2*n_c); 
S1_xz = S1(      1:n_r,   2*n_c+1:3*n_c);
S1_yx = S1(  n_r+1:2*n_r,       1:n_c); 
S1_yy = S1(  n_r+1:2*n_r,   n_c+1:2*n_c);
S1_yz = S1(  n_r+1:2*n_r, 2*n_c+1:3*n_c);
S1_zx = S1(2*n_r+1:3*n_r,       1:n_c);
S1_zy = S1(2*n_r+1:3*n_r,   n_c+1:2*n_c);
S1_zz = S1(2*n_r+1:3*n_r, 2*n_c+1:3*n_c);

S2_xx = S2(      1:m_r,         1:m_c); 
S2_xy = S2(      1:m_r,     m_c+1:2*m_c); 
S2_xz = S2(      1:m_r,   2*m_c+1:3*m_c);
S2_yx = S2(  m_r+1:2*m_r,       1:m_c); 
S2_yy = S2(  m_r+1:2*m_r,   m_c+1:2*m_c);
S2_yz = S2(  m_r+1:2*m_r, 2*m_c+1:3*m_c);
S2_zx = S2(2*m_r+1:3*m_r,       1:m_c);
S2_zy = S2(2*m_r+1:3*m_r,   m_c+1:2*m_c);
S2_zz = S2(2*m_r+1:3*m_r, 2*m_c+1:3*m_c);

% Build combined matrix:
S = [S1_xx, S2_xx, S1_xy, S2_xy, S1_xz, S2_xz; ...
     S1_yx, S2_yx, S1_yy, S2_yy, S1_yz, S2_yz; ...
     S1_zx, S2_zx, S1_zy, S2_zy, S1_zz, S2_zz];
%}
 
function S = combineMatrices(varargin)

% get number of inputs
N_mat = length(varargin);

% assign inputs 
for i = 1:N_mat
    A{i} = varargin{i};
end

% get sizes of each input matrix
for i = 1:N_mat
    dim = size(A{i});
    n_r{i} = dim(1)/3;      % # of coordinate rows
    n_c{i} = dim(2)/3;      % # of coordinate columns
end

% extract components from each matrix
for i = 1:N_mat
    S_xx{i} = A{i}(           1:n_r{i},              1:n_c{i}); 
    S_xy{i} = A{i}(           1:n_r{i},     n_c{i}+1:2*n_c{i}); 
    S_xz{i} = A{i}(           1:n_r{i},   2*n_c{i}+1:3*n_c{i});
    S_yx{i} = A{i}(  n_r{i}+1:2*n_r{i},            1:n_c{i}); 
    S_yy{i} = A{i}(  n_r{i}+1:2*n_r{i},   n_c{i}+1:2*n_c{i});
    S_yz{i} = A{i}(  n_r{i}+1:2*n_r{i}, 2*n_c{i}+1:3*n_c{i});
    S_zx{i} = A{i}(2*n_r{i}+1:3*n_r{i},          1:n_c{i});
    S_zy{i} = A{i}(2*n_r{i}+1:3*n_r{i},   n_c{i}+1:2*n_c{i});
    S_zz{i} = A{i}(2*n_r{i}+1:3*n_r{i}, 2*n_c{i}+1:3*n_c{i});
end

% build combined matrix
S = [[S_xx{:}], [S_xy{:}], [S_xz{:}]; ...
     [S_yx{:}], [S_yy{:}], [S_yz{:}]; ...
     [S_zx{:}], [S_zy{:}], [S_zz{:}]];

end % function
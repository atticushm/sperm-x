function M = calcFlagMoment(X, X0, f)

I = computeFlagMomentIntegrals(X,f);
M = proxp(I);

%{
% matrix representations
X_mat = mat(X);
f_mat = mat(f);

% proximal integral (midpoint rule)
Xm = 1/2*(X_mat(:,1)+ 1/2*(X_mat(:,1)+X_mat(:,2)));
dX = (Xm-X0);
A1 = ds/2 * [0,-dX(3),dX(2); dX(3),0,-dX(1); -dX(2),dX(1),0];
M1 = A1*f_mat(:,1);

% interior integrals (midpoint rule)
M2 = zeros(3,1);
for n = 2:N-1
    dX = X_mat(:,n)-X0;
    A2 = ds * [0,-dX(3),dX(2); dX(3),0,-dX(1); -dX(2),dX(1),0];
    M2 = M2 + A2*f_mat(:,n);
end

% distal integral (midpoint rule)
Xm = 1/2*(1/2*(X_mat(:,N-1)+X_mat(:,N)) + X_mat(:,N));
dX = (Xm-X0);
A3 = ds/2 * [0,-dX(3),dX(2); dX(3),0,-dX(1); -dX(2),dX(1),0];
M3 = A3*f_mat(:,N);

% total moment is then
M = M1+M2+M3;
%}

end % function
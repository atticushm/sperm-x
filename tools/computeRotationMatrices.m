function R = computeRotationMatrices(d1, d2, d3)

% computes the rotation matrix between each frame (defined through entries
% in d1, d2 and d3) and the Cartesian xyz frame

% number of matrices to compute
N = length(d1)/3;

% matrix representations
d1_mat = mat(d1);
d2_mat = mat(d2);
d3_mat = mat(d3);

% local frame 
e1 = [0;1;0];
e2 = [0;0;1];
e3 = [1;0;0];

% via Bower: e_i = R^T * d_i, where R = kron(m_i, e_i)
for n = 1:N
    Rot         = kron(d1_mat(:,n),e1') + ...
                  kron(d2_mat(:,n),e2') + ...
                  kron(d3_mat(:,n),e3');
    R(:,n)      = Rot(:,1);
    R(:,N+n)    = Rot(:,2);
    R(:,2*N+n)  = Rot(:,3);
end

end % function
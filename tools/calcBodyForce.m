function F = calcBodyForce(f, NN)

% f is a 3Nx1 vector of forces.
% NN is the 3Qx3N nearest-neighbour matrix.
% F is a 3x1 vector of the total force.

fq         = NN*f;
[f1,f2,f3] = extractComponents(fq);
F          = [sum(f1); sum(f2); sum(f3)];

end % function
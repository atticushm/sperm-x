function kap_bf = calcRelCurvatureEPC(Z)

% computes the relative curvature from tangent angle data
% relative to the cell head

% specific to endpiece/EIF angle formulation methods!

% number of time points
num_tbeat = size(Z,2);

% get body frame tangent angles
for i = 1:num_tbeat
    th_bf = Z(3:end,i)-Z(3,i);
    kap_bf(:,i) = diff(th_bf);
end

end % function
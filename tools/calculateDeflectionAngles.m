function [th_xz, th_xy] = calculateDeflectionAngles(X0, params)

% computes angles of deflection relative to each cartesian plane of cell
% approaching a plane wall at z=0.

%% xz angle
% i.e. wall approach angle

% xz projection points
xx = X0(1,:);
yy = X0(3,:);

% moving mean, averaging position over each beat
xxm = movmean(xx, params.NumSlices);
yym = movmean(yy, params.NumSlices);

% compute tangent angle
th_xz = atan(diff(yym)./diff(xxm));

% remove initilisation and end wobble due to moving mean
% th_xz = th_xz(3*params.NumSlices:end-params.NumSlices);

%% xy angle
% i.e. banking angle

% xy projection points
xx = X0(1,:);
yy = X0(2,:);

% moving mean, averaging position over each beat
xxm = movmean(xx, params.NumSlices);
yym = movmean(yy, params.NumSlices);

% compute tangent angle
th_xy = atan(diff(yym)./diff(xxm));

% remove initilisation and end wobble due to moving mean
%th_xy = th_xy(3*params.NumSlices:end-params.NumSlices);

end % function
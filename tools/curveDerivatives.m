function varargout = CurveDerivatives(X,s,varargin)

% Finds the derivatives of the curve defined by nodes X with respect to s
% at arclength points s_eval.

% *** IMPORTANT NOTE ***
% For efficiency this function should only be called ONCE, and in that case
% the inputs need to be the NODES and NOT midpoints.

persistent d

%% Compute with finite differences:

%{x
if isempty(d)
    
    if isempty(varargin)
        opt = [];
        Yds = [];
    elseif strcmp(varargin{1},'head')
        opt = 'head';
        Yds = varargin{2};
    else
        warning('Unrecognised optional input.')
        return
    end
    
    % Get finite difference matrices:
    D = finiteDifferences(s,1:5,opt);
    D_1 = D{1};
    D_2 = D{2};
    D_3 = D{3};
    D_4 = D{4};
    D_5 = D{5};
    
    XM = VectorToMatrix(X);
    ZM = [Yds(:), XM];
    Z = MatrixToVector(ZM);

    % Compute derivatives:
    d{1} = blkdiag(D_1,D_1,D_1) * Z(:);
    d{2} = blkdiag(D_2,D_2,D_2) * Z(:);
    d{3} = blkdiag(D_3,D_3,D_3) * Z(:);
    d{4} = blkdiag(D_4,D_4,D_4) * Z(:);
    d{5} = blkdiag(D_5,D_5,D_5) * Z(:);
end
%} 

%% Outputs:

for n = 1:nargout
    varargout{n} = MatrixToVector(d{n});
end

end % function
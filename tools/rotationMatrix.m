function R_c = RotationMatrix(X,X0)

% Computes the rotation matrix/matrices rotating coodinates given in X onto
% the vector X0.
    
% More info on this method here: https://math.stackexchange.com/questions/180418/calculate-rotation-matrix-to-align-vector-a-to-vector-b-in-3d/2161631#2161631
% and in ModelFormulation writeup.

Qp1 = length(X)/3; 
Q = Qp1-1;
s = linspace(0,1,Qp1);

% Tangents at nodes:
D = finiteDifferences(s,1,'no_head');
X_s = blkdiag(D{1},D{1},D{1}) * X(:);
XM_s = VectorToMatrix(X_s);
TM = XM_s./vecnorm(XM_s);

% Compute rotation matrices:
Reflection = @(u,n) u-2*n*(n'*u)/(n'*n);
R_c = zeros(3,3*Q);
for i = 1:Qp1
    S = Reflection(eye(3),TM(:,i)+X0);
    r = Reflection(S, TM(:,i));
    R_c(:,i) = r(:,1);
    R_c(:,i+Qp1) = r(:,2);
    R_c(:,i+2*Qp1) = r(:,3);
end

end % function
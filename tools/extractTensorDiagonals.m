function D = ExtractTensorDiagonals(A)

N = size(A,1)/3; 
M = size(A,2)/3;

% Extract terms:
S{1,1} = A(1:N,1:M);
S{1,2} = A(1:N,M+1:2*M);
S{1,3} = A(1:N,2*M+1:3*M);
S{2,1} = A(N+1:2*N,1:M);
S{2,2} = A(N+1:2*N,M+1:2*M);
S{2,3} = A(N+1:2*N,2*M+1:3*M);
S{3,1} = A(2*N+1:3*N,1:M);
S{3,2} = A(2*N+1:3*N,M+1:2*M);
S{3,3} = A(2*N+1:3*N,2*M+1:3*M);

% Extract diagonals from each term:
D{1,1} = diag(diag(S{1,1}));
D{1,2} = diag(diag(S{1,2}));
D{1,3} = diag(diag(S{1,3}));
D{2,1} = diag(diag(S{2,1}));
D{2,2} = diag(diag(S{2,2}));
D{2,3} = diag(diag(S{2,3}));
D{3,1} = diag(diag(S{3,1}));
D{3,2} = diag(diag(S{3,2}));
D{3,3} = diag(diag(S{3,3}));

% Convert to double for output:
D = cell2mat(D);

end % function
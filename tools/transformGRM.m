function R_trans = TransformGRM(GRM_0, B, r)

% Compute the grand resistance matrix R for a body following a rotation B.
% R_0 is the 6x6 grand resistance matrix before rotation.
% B is the 3x3 rotation matrix to applied to the body.
% r is the displacement vector between the two positions the shapes were rotated around.

% Extract submatrices from R_0
RFU = GRM_0(1:3,1:3);
RFO = GRM_0(1:3,4:6);
RMU = GRM_0(4:6,1:3);
RMO = GRM_0(4:6,4:6);

% The force-velocity coupling matrix transforms through B:
RFU_trans = B * RFU * B';

% Transform the coupling matrix (via Wegener):
RMU_trans = RMU - [cross(r, RFU(:,1)), cross(r, RFU(:,2)), cross(r, RFU(:,3))];
RMU_trans = B * RMU_trans * B';
RFO_trans = RMU_trans';

% Transform the moment-rotation matrix (via Wegener):
double_prod = [cross(RFU(:,1), r), cross(RFU(:,2), r), cross(RFU(:,3), r)];
triple_prod = [cross(r, double_prod(:,1)), cross(r, double_prod(:,2)), cross(r, double_prod(:,3))];
RMO_trans = RMO - triple_prod + ...
            [cross(RMU(:,1),r), cross(RMU(:,2),r), cross(RMU(:,3),r)] - ...
            [cross(r,RFO(:,1)), cross(r,RFO(:,2)), cross(r,RMO(:,3))];
RMO_trans = B * RMO_trans * B';

% Build transformed R:
R_trans = [RFU_trans, RFO_trans;...
           RMU_trans, RMO_trans];

end % function
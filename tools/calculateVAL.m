function VAL = calculateVAL(Z, tps, code, varargin)

% compute VAL for a *planar* swimming cell
% Z contains solution data across simulation
% wavk is wavenumber of actuated beat
% tps contains time points corresponding to each solution
% code is a string indicating which code the solutions are from 

switch code
    case 'endpiece'
        
        % find time indices corresponding to the start of a beat
        T = 2*pi;
        m = mod(tps, T);
        [~,idx] = find(m < 1e-2);
        
        % X0=[x0,y0] positions
        x0 = Z(1,:);
        y0 = Z(2,:);
        
        % X0 positions at start of beats
        x0i = x0(idx);
        y0i = y0(idx);
        
        % VAL across beat
        X0i = [x0i; y0i];
        VAL = vecnorm(X0i(:,2:end)-X0i(:,1:end-1))/T;
        
        % average VAL across established beat (beat 3 onwards)
        %VAL = mean(VAL(3:end));
        
    case 'spx'
        
        % extract parameter from varargin
        % params = varargin{1};
        
        % find time indices corresponding to the start of a beat
        % T = params.T;
        T = 2*pi;
        m = mod(tps, T);
        [~, idx] = find(m < 0.1);   % tolerance may need changed if NumSave changes
        
        % sample shape over final recorded beat
        % Z in this case is a matrix of coordinate data
        if length(idx)>=2
            Xidx = Z(:, idx(end-1:end));
        else
            Xidx = [Z(:,1),Z(:,end)];
        end
        
        % X0 positions
        X0idx = [proxp(Xidx(:,1)), proxp(Xidx(:,2))];
        
        % VAL across final beat
        VAL = vecnorm(X0idx(:,2)-X0idx(:,1))/T;
        
end

end % function
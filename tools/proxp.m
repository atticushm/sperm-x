function Z0 = proxp(Z)

N = length(Z)/3;
Z0 = [Z(1); Z(1+N); Z(1+2*N)];

end % function

function [phi,theta,psi] = FindEulerAngles(d1,d2,d3)

% computes the Euler angles of a rotated coordinated system based on the
% director frame for a given system.
% based on `FindEulerAngles' f90 subroutine by TDMJ

% define lab x and z directions
xx = [1;0;0];
zz = [0;0;1];

% phi is positive if d2 contains a positive
if d2(2) >= 0
    phi = acos(dot(xx, d2));
else
    phi = -acos(dot(xx, d2));
end

% theta is positive if the xy plane is rotated positively about the
% binormal (so that d1,d2,d3 follow a right hand rule).
if (phi > 0) && (d1(1) >= 0)
    theta = acos(dot(zz, d1));
else
    theta = -acos(dot(zz, d1));
end

% psi is positive if theta and the z-component of d3>0
if (theta >= 0 && d3(3) >= 0) || (theta < 0 && d3(3) < 0)
    psi = acos(dot(d2,d3));
else
    psi = -acos(dot(d2,d3));
end

end % function
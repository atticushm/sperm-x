function GRM = resistanceProlateSpheroid(d, a, c)

% computes the GRM for a prolate spheroid via the resistance functions
% given in Kim & Karilla pg 64.
% - d is the vector of the long axis of the body
% - a is the radius of the long axis
% - c is the radius of the short axis

e = sqrt(a^2-c^2)/a;          % eccentricity (pg 61)
e2 = e^2;
e3 = e^3;

L = log((1+e)/(1-e));

XA = 8/3*e3*1/(-2*e+(1+e2)*L);
YA = 16/3*e3*1/(2*e+(3*e2-1)*L);
XC = 4/3*e3*(1-e2)*1/(2*e-(1-e2)*L);
YC = 4/3*e3*(2-e2)*1/(-2*e+(1+e2)*L);

U = eye(3);
Om = eye(3);

U_inf = zeros(3,1);
Om_inf = zeros(3,1);

didj = kron(d,d');
I = eye(3);

for i = 1:3
    % force
    F(:,i) = 6*pi*a*(XA*didj + YA*(I-didj))*(U_inf-U(:,i));
    
    % torque
    T(:,i) = 8*pi*a^3*(XC*didj + YC*(I-didj))*(Om_inf-Om(:,i));
end

GRM = blkdiag(F, T);

end % function
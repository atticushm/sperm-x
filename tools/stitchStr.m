function STR = StitchStr(DELIM,varargin)

%STITCHSTR Generate a string via concatenation of inputs.
% STR = StitchStr(DELIM,IN_1,...,IN_N) generates an output string by
% concatenating the inputs IN_1,...,IN_N. Inputs can be either strings or
% numerical, and are separated in the output by the character given in DELIM.
%
% Examples:
%
% % Example 1: string inputs
%     DELIM = '_';    % use underscore for delimiter.
%     STR = StitchStr(DELIM,'hello','world');
%     
%     % returns 'hello_world'
%
% % Example 2: mixed inputs
%     DELIM = '-';    % use dash for delimiter.
%     STR = StitchStr(DELIM,'results_number',1024);
%     
%     % returns 'results_number_1024'
%
% See also ISSTR.

N = length(varargin);

if isempty(DELIM)
    DELIM = '_';
end

STR = '';
for n = 1:N
    if isstr(varargin{n})
        STR = [STR,varargin{n}];
    elseif isnumeric(varargin{n})
        STR = [STR,num2str(varargin{n})];
    end
    if n == N
        return
    else
        STR = [STR,DELIM];
    end
end

end
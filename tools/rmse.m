function E = rmse(a,b)

% computes the root mean squared error between vectors a and b

N = length(a);
E = sqrt(1/N * sum((a-b).^2));

end
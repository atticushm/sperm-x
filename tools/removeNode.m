function Y = removeNode(X,X0)

% Removes the point X0 from the vector of coordinates X.

XM = vectorToMatrix(X(:));
diff = vecnorm(abs(XM-X0));

tol = 1e-5;
XM(:,diff<tol) = [];

Y = matrixToVector(XM);

end
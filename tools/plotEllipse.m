function plotEllipse(X1, X2, e, varargin)

% plots an ellipse defined by X1, X2, the ellipse vertices, and e, its
% eccentricity.
% varargin can contain (in this order)
%   - linewidth
%   - colour

% code by Roger Stafford via 
% https://uk.mathworks.com/matlabcentral/answers/86615-how-to-plot-an-ellipse#answer_96132

a = 1/2*sqrt((x2-x1)^2+(y2-y1)^2);
b = a*sqrt(1-e^2);
t = linspace(0,2*pi);
X = a*cos(t);
Y = b*sin(t);
w = atan2(y2-y1,x2-x1);
x = (x1+x2)/2 + X*cos(w) - Y*sin(w);
y = (y1+y2)/2 + X*sin(w) + Y*cos(w);
plot(x,y,'y-')
axis equal

end
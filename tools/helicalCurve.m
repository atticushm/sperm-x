function varargout = HelicalCurve(k, c, M)

% Generates a helix along the lab frame x-axis.
% k is the wavenumber of the helix (how many loops).
% c is a constant defining the vertical separation of the helix's loops.
% r is the tapering constant;
% M is the desired number of nodes.

% Extract variables from model:
Q = M - 1;

% Define high-resolution helix parametrically:
t = linspace(0,k*pi,1e6);
x = c * t;
r = 0.1.*x;
% r = 0.1;
y = r .* sin(t);
z = r .* cos(t);

% Scale helix to be of unit arclength:
ds = sqrt(diff(x).^2 + diff(y).^2 + diff(z).^2);
s = [0,cumsum(ds)];
delta = max(s)/Q;
ss = 0:delta:max(s);

xx = spline(s,x,ss);
yy = spline(s,y,ss);
zz = spline(s,z,ss);

x = xx/max(s); 
y = yy/max(s);
z = zz/max(s);
X = [x(:); y(:); z(:)];

% Outputs
nOut = nargout;
if nOut <= 1
    varargout{1} = X(:);
elseif nOut == 3
    varargout{1} = x(:);
    varargout{2} = y(:);
    varargout{3} = z(:);
elseif nOut == 4
    varargout{1} = X(:);
    varargout{2} = x(:);
    varargout{3} = y(:);
    varargout{4} = z(:);
end

end % function
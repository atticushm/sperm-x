function D = RegStokesletNumericalDerivativeIntegrals(X_field, X_theta, X_a, X_b, seg_angle, N, ds, epsilon)

% Computes numerically the integral of the regularised stokeslet
% derivatives.
% Stokeslet derivatives are approximated using finite differences.
% Integrals are approximated using a suitable quadrature.

% X_field is a 3Mx1 vector of coordinates of field points.
% X_theta are the tangent angles at each field point.
% X_a and X_b are the points at each end of the segment.
% seg_theta is the tangent angle of the segment.
% N is the discretisation parameter for numerical integration.
% ds is the discretisation parameter for numerical differentiation.
% epsilon is the regularisation parameter.


% Discretise line segment:
x = linspace(X_a(1), X_b(1), N);
y = linspace(X_a(2), X_b(2), N);
z = linspace(X_a(3), X_b(3), N);

% Length of segment:
h = norm(X_b - X_a);

% For numerical differentiation, we approximate X(s+ds) and X(s-ds):
% The tangent at the field point is:
t_f = [cos(X_theta), -sin(X_theta), 0]';

% X(s+h) = X(s)+ds X'(s)


%% Outputs:

D = [];

end
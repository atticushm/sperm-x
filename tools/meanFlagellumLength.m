function L = meanFlagellumLength()

% mean flagellum+midpiece length from Cummins 1985
L1 = 4.7+47.13;
L2 = 4.18+56.16;
L3 = 4+48;
L  = mean([L1,L2,L3]);

end % function

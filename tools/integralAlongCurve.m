function X = integralAlongCurve(X0,d3)

% determines the position vectors X(s) = [x(s),y(s),z(s)] by integrating
% along the curve defined by X0 the starting point and d3(s) the axial
% direction vector.

% number of nodes:
N = length(d3)/3;
ds = 1/(N-1);

% arclength parametrisation:
s = linspace(0,1,N);

%% midpoint rule:

%{x
% director at midpoints:
d3m = midpoints(d3);

% integrate along curve:
A = tril(ds*ones(N-1),-1) + ds/2*eye(N-1);
int = blkdiag(A,A,A)*d3m;

% position vectors of midpoints:
Xm = kron(X0,ones(N-1,1)) + int;

% spline to obtain nodal values:
[xm,ym,zm] = extractComponents(Xm);
sm = 0.5*(s(2:N)+s(1:N-1));
x = spline(sm,xm,s); y = spline(sm,ym,s); z = spline(sm,zm,s);
X = [x(:); y(:); z(:)];
%}

%% trapezoidal rule:

%{
[dx,dy,dz] = extractComponents(d3);

% integral along tangent:
Ones = ones(N); 
A    = (ds/2)*(tril(Ones,-1)+tril(Ones));
intx = A*dx;    %intx = [0;intx(2:N)];
inty = A*dy;    %inty = [0;inty(2:N)];
intz = A*dz;    %intz = [0;intz(2:N)];
int  = [intx(:); inty(:); intz(:)];

% position vectors:
X = kron(X0,ones(N,1)) + int;
%}

end % function
function diff_coeffs = ComputeFiniteDifferenceCoefficients(M, N, x0, a, varargin)

% Computes the finite difference coefficients approximating d^m f(x) / dx^m
% at x=x0 at N+1 grid points.
% N is the maximum order of derivative to calculate.
% M is the order of the highest derivative we wish to approximate.
% s are the grid points.
% ds is the Nx1 vector of 1D node separations.

% Rows of the output matrices d_s, ..., d_ssss correspond to the order of
% derivative approximation.

% Code based upon the algorithm in Fornberg 1988.

%% Algorithm (via Fornberg):

% Max. order of accuracy can be optionally input:
% if ~isempty(varargin)
%     N = varargin{1};
% else
%     N = length(a)-1;
%     if N > 8
%         N = 4; % cutoff N if needlessly large.
%     end
% end

% Preallocate:
for n = 0:M
    w(z(n)) = struct('delta',zeros(N+1));
end

% Compute weights:
w(z(0)).delta(z(0),z(0)) = 1;
c1 = 1;
for n = 1:N
    c2 = 1;
    for v = 0:(n-1)
        c3 = a(z(n)) - a(z(v));
        c2 = c2 * c3;
        if n <= M
            w(z(n)).delta(z(n-1),z(v)) = 0;
        end
        for m = 0:min(n,M)
            t1 = (a(z(n)) - x0)*w(z(m)).delta(z(n-1),z(v));
            if m == 0
                t2 = 0;
            else
                t2 = -m*w(z(m-1)).delta(z(n-1),z(v));
            end
            w(z(m)).delta(z(n),z(v)) = (t1 + t2)/c3;
            clear t1 t2
        end % next m
    end % next v
    for m = 0:min(n,M)
        if m == 0
            t1 = 0;
        else
            t1 = m*w(z(m-1)).delta(z(n-1),z(n-1));
        end
        t2 = -(a(z(n-1))-x0)*w(z(m)).delta(z(n-1),z(n-1));
        w(z(m)).delta(z(n),z(n)) = (c1/c2)*(t1 + t2);
        clear t1 t2
    end % next m
    c1 = c2;
    clear t1 t2
end % next n

%% Outputs

% Extract operators from w:
for n = 1:M
    if n == 1
        d_s = w(2).delta;
        d_s = d_s(~all(d_s==0,2),:); % remove empty rows.
        d_s = [d_s, zeros(size(d_s,1),length(a)-size(d_s,2))];
        d_s = [a(:)'; d_s];
    end
    if n == 2
        d_ss = w(3).delta;
        d_ss = d_ss(~all(d_ss==0,2),:);
        d_ss = [d_ss, zeros(size(d_ss,1),length(a)-size(d_ss,2))];
        d_ss = [a(:)'; d_ss];
    end
    if n == 3
        d_sss = w(4).delta;
        d_sss = d_sss(~all(d_sss==0,2),:);
        d_sss = [d_sss, zeros(size(d_sss,1),length(a)-size(d_sss,2))];
        d_sss = [a(:)'; d_sss];
    end
    if n == 4
        d_ssss = w(5).delta;
        d_ssss = d_ssss(~all(d_ssss==0,2),:);
        d_ssss = [d_ssss, zeros(size(d_ssss,1),length(a)-size(d_ssss,2))];
        d_ssss = [a(:)'; d_ssss];
    end
    if n >= 5
        warning('Higher order derivatives have no defined output, see code!')
        return
    end
end

diff_coeffs{1} = d_s;
diff_coeffs{2} = d_ss;
diff_coeffs{3} = d_sss;
diff_coeffs{4} = d_ssss;

end % function

function idx = z(m)
% Function to help with zero indexing (used in the Fornberg paper).
idx = m+1;
end
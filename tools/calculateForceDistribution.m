function [f,phi] = calculateForceDistribution(t, Y, model)

% requires end piece code to be in path to function.

% add endpiece code to path if not already on path
if (exist(',/endpiece_code','dir')~=7)
    addpath(genpath('./endpiece_code'))
end

% get all problem data
[head,flag,NN] = GetProblemData(Y,model);

% construct hydrodynamics block
AH = ConstructHydrodynamicsMatrix(head,flag,model,NN);

% construct kinematics block
AK = ConstructKinematicsMatrix(head,flag,model);

% construct elastodynamics block
AE = ConstructElasticsMatrix(head,flag,model,NN);

% calculate active moment integral
b  = ConstructRHS(t,head,flag,model);

% construct linear system (matrix and rhs vector)
AZ = zeros(size(AE,1),size(AK,2));
A  = [AZ AE ; AK AH];

% solve system
dZ = A\b;

% extract forces
Q = model.disc.Q;
N = length(model.y)/3;

dZ_forces = dZ(Q+4:end);
dZ_mat    = mat(dZ_forces);
phi_mat   = dZ_mat(:, 1:N);
f_mat     = dZ_mat(:, N+1:end);

f   = vec(f_mat);
phi = vec(phi_mat);

% reset paths
AddFilesToPath;

end % function
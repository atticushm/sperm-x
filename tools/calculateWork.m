function [W] = calculateWork(code, tps, varargin)

switch code
    case 'endpiece'
        
        % requires end piece code to be in path to function.
        % model variables
        Z = varargin{1};
        model = varargin{2};

        % get all problem data
        [head,flag,NN] = GetProblemData(Z,model);
        
        % construct hydrodynamics block
        AH = ConstructHydrodynamicsMatrix(head,flag,model,NN);
        
        % construct kinematics block
        AK = ConstructKinematicsMatrix(head,flag,model);
        
        % construct elastodynamics block
        AE = ConstructElasticsMatrix(head,flag,model,NN);
        
        % calculate active moment integral
        b  = ConstructRHS(tps,head,flag,model);
        
        % construct linear system (matrix and rhs vector)
        AZ = zeros(size(AE,1),size(AK,2));
        A  = [AZ AE ; AK AH];
        
        % solve system
        dZ = A\b;
        
        % extract solution dY=[dx0dt dy0dt dth1dt dth2dt ... dthQdt]
        dY = dZ(1:model.disc.Q+3);
        
        % calculate work done
        F  = dZ((model.disc.Q+4):end);
        dX = AK*dY;
        W  = F'*dX;

    case 'spx'

        % extract from varargin
        f = varargin{1};
        phi = varargin{2};
        uX = varargin{3};
        uY = varargin{4};

        % arguments of integrals
        flag_arg = dotProduct(uX,f);
        head_arg = dotProduct(uY,phi);

        % integrate
        ds = 1/(length(f)/3);
        dS = 1/(length(phi)/3); 
        W = ds*trapz(flag_arg) + dS*trapz(head_arg);

end

end

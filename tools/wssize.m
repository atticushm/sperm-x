% WSSIZE
%
% Display size of variables in the base workspace using B, KB, MB and GB
function wssize

    % Obtain base workspace contents
    ws_contents = evalin('base', 'whos');
    
    % Loop through contents and display size on screen
    for i = 1:length(ws_contents)
        cur_size = ws_contents(i).bytes;
        if cur_size > 1024^3;
            fprintf('%-15s: %8.3f GB\n', ws_contents(i).name, cur_size/1024^3);
        elseif cur_size > 1024^2;
            fprintf('%-15s: %8.3f MB\n', ws_contents(i).name, cur_size/1024^2);
        elseif cur_size > 1024;
            fprintf('%-15s: %8.3f KB\n', ws_contents(i).name, cur_size/1024);
        else
            fprintf('%-15s: %8.3f  B\n', ws_contents(i).name, cur_size);
        end
    end
end
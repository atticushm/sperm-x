function V = computeBodyVolume(input)

% input can be (a) model structure, from EIF code or (b) 3x1 vector of axis
% radii 

% determine what form input is
type = class(input);

switch type
    case 'double'
        V = 4/3*pi*input(1)*input(2)*input(3);
    case 'struct'
        ax = [input.swimmer.a1; input.swimmer.a2; input.swimmer.a3];
        V  = 4/3*pi*ax(1)*ax(2)*ax(3);
end

end % function
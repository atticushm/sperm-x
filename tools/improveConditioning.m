function [A,b,C] = ImproveConditioning(M, c)

%IMPROVECONDITIONING multiples rows of the mass matrix M and right hand side 
%vector cimplementing the nonlinear elastohydrodynamic system of equations 
%to improve the condition number of the problem, particularly when dt is small.
%
%See also EQUILIBRATE.

% using equilibrate:
[P,R,C] = equilibrate(M);
A = R*P*M*C;
b = R*P*c;

end % function
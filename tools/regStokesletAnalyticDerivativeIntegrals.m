function varargout = RegStokesletAnalyticDerivativeIntegrals(X_field, X_centres, T_field, R_centres, h, epsilon, varargin)

% Computes the integrals with respect to xi of the derivatives of the
% regularised stokeslet with respect to s. These integrals have been
% calculated analytically by rotating each segment (length 2*h) so that it
% is parallel to the x-axis.

% X_field is a 3Nx1 vector of coordinates of field points.
% X_centres is a 3Qx1 vector of coordinates of source points, which are the
%   centres of each line segment.
% RM is the 3x3Q rotation matrix.
% h is the half length of integration.
% epsilon is the regularisation parameter.

% This code could probably be neatened up with more judicious use of cell
% arrays.

X = X_field(:);
Y = X_centres(:);

N = length(X(:))/3;
Q = length(Y(:))/3;

% Extract coordinate components:
[X1, X2, X3] = extractComponents(X);
[Y1, Y2, Y3] = extractComponents(Y);

%% Extract unit tangents at field points:

[t{1},t{2},t{3}] = extractComponents(T_field);

%% Rotate into local coordinates:

% Extract components of R:
RM{1,1} = repmat(R_centres(1,      1:Q),N,1);
RM{1,2} = repmat(R_centres(1,  Q+1:2*Q),N,1);
RM{1,3} = repmat(R_centres(1,2*Q+1:3*Q),N,1);

RM{2,1} = repmat(R_centres(2,      1:Q),N,1);
RM{2,2} = repmat(R_centres(2,  Q+1:2*Q),N,1);
RM{2,3} = repmat(R_centres(2,2*Q+1:3*Q),N,1);

RM{3,1} = repmat(R_centres(3,      1:Q),N,1);
RM{3,2} = repmat(R_centres(3,  Q+1:2*Q),N,1);
RM{3,3} = repmat(R_centres(3,2*Q+1:3*Q),N,1);

% Point differences (in the lab frame):
xi1 = X1*ones(1,Q) - ones(N,1)*Y1';
xi2 = X2*ones(1,Q) - ones(N,1)*Y2';
xi3 = X3*ones(1,Q) - ones(N,1)*Y3';

% Transform differences into the local frame:
x = RM{1,1}.*xi1 + RM{2,1}.*xi2 + RM{3,1}.*xi3;
y = RM{1,2}.*xi1 + RM{2,2}.*xi2 + RM{3,2}.*xi3;
z = RM{1,3}.*xi1 + RM{2,3}.*xi2 + RM{3,3}.*xi3;

% % Transform unit tangents into local frame:
tL{1} = RM{1,1}.*t{1} + RM{2,1}.*t{2} + RM{3,1}.*t{3};
tL{2} = RM{1,2}.*t{1} + RM{2,2}.*t{2} + RM{3,2}.*t{3};
tL{3} = RM{1,3}.*t{1} + RM{2,3}.*t{2} + RM{3,3}.*t{3};

% Some substitutions for readability:
a = y.^2 + z.^2 + epsilon.^2;
b = epsilon;
reps_ph = (a + (h-x).^2).^(3/2);
reps_mh = (a + (-h-x).^2).^(3/2);
ireps_ph = 1./reps_ph;
ireps_mh = 1./reps_mh;
ireps_ph_a = 1./(a.*reps_ph);
ireps_mh_a = 1./(a.*reps_mh);
ireps_ph_a2 = 1./(a.^2.*reps_ph);
ireps_mh_a2 = 1./(a.^2.*reps_mh);

%% Integrals of isotropic term (I_{ijk}^A):

% Indefinite integrals:
%{
A_ij{1} = @(s) (-(a + b^2 + (s-x).^2))./((a + (s-x).^2).^(3/2));
A_ij{2} = @(s) (-y.*(s-x).*(a.^2 + a.*(3*b^2 + (s-x).^2) + 2*b^2*(s-x).^2))./(a.^2 .* (a + (s-x).^2).^(3/2));
A_ij{3} = @(s) (-z.*(s-x).*(a.^2 + a.*(3*b^2 + (s-x).^2) + 2*b^2*(s-x).^2))./(a.^2 .* (a + (s-x).^2).^(3/2));

% Definite integrals:

IA = cell(3,3,3);
IA(:,:,:) = {zeros(N,Q)};
for k = 1:3
    IA{1,1,k} = A_ij{k}(h) - A_ij{k}(-h);
    IA{2,2,k} = A_ij{k}(h) - A_ij{k}(-h);
    IA{3,3,k} = A_ij{k}(h) - A_ij{k}(-h);
end
%}

% Without using anonymous functions:
int_1 = (-(a + b^2 + (h-x).^2)).*ireps_ph -  (-(a + b^2 + (-h-x).^2)).*ireps_mh;
int_2 = (-y.*(h-x).*(a.^2 + a.*(3*b^2 + (h-x).^2) + 2*b^2*(h-x).^2)).*ireps_ph_a2 - (-y.*(-h-x).*(a.^2 + a.*(3*b^2 + (-h-x).^2) + 2*b^2*(-h-x).^2)).*ireps_mh_a2;
int_3 = (-z.*(h-x).*(a.^2 + a.*(3*b^2 + (h-x).^2) + 2*b^2*(h-x).^2)).*ireps_ph_a2 - (-z.*(-h-x).*(a.^2 + a.*(3*b^2 + (-h-x).^2) + 2*b^2*(-h-x).^2)).*ireps_mh_a2;

IA = cell(3,3,3);
IA(:,:,:) = {zeros(N,Q)};
IA{1,1,1} = int_1;
IA{2,2,1} = int_1;
IA{3,3,1} = int_1;
IA{1,1,2} = int_2;
IA{2,2,2} = int_2;
IA{3,3,2} = int_2;
IA{1,1,3} = int_3;
IA{2,2,3} = int_3;
IA{3,3,3} = int_3;

%% Integrals of dyadic term (I_{ijk}^B):

% Indefinite integrals:
%{
B_11{1} = @(s) (-(s-x).^2)./((a + (s-x).^2).^(3/2));
B_12{1} = @(s) (y.*(s-x))./((a + (s-x).^2).^(3/2));
B_13{1} = @(s) (z.*(s-x))./((a + (s-x).^2).^(3/2));
B_21{1} = B_12{1};
B_22{1} = @(s) (-y.^2)./((a + (s-x).^2).^(3/2));
B_23{1} = @(s) (-y.*z)./((a + (s-x).^2).^(3/2));
B_31{1} = B_13{1};
B_32{1} = B_23{1};
B_33{1} = @(s) (-z.^2)./((a + (s-x).^2).^(3/2));

B_11{2} = @(s) (-y.*(s-x).^3)./(a.*(a + (s-x).^2).^(3/2));
B_12{2} = @(s) (a + (s-x).^2 -y.^2)./((a + (s-x).^2).^(3/2));
B_13{2} = @(s) (-y.*z)./((a + (s-x).^2).^(3/2));
B_21{2} = B_12{2};
B_22{2} = @(s) (y.*(s-x).*(2*a.*(a+(s-x).^2) -y.^2.*(3*a + 2*(s-x).^2)))./(a.^2.*(a + (s-x).^2).^(3/2));
B_23{2} = @(s) (z.*(s-x).*(a.*(a+(s-x).^2) -y.^2.*(3*a + 2*(s-x).^2)))./(a.^2.*(a + (s-x).^2).^(3/2));
B_31{2} = B_13{2};
B_32{2} = B_23{2};
B_33{2} = @(s) (-y.*z.^2.*(s-x).*(3*a + 2*(s-x).^2))./(a.^2.*(a + (s-x).^2).^(3/2));

B_11{3} = @(s) (-z.*(s-x).^3)./(a.*(a + (s-x).^2).^(3/2));
B_12{3} = @(s) (-y.*z)./((a + (s-x).^2).^(3/2));
B_13{3} = @(s) ((s-x).^2 +y.^2 + b^2)./((a + (s-x).^2).^(3/2));
B_21{3} = B_12{3};
B_22{3} = @(s) (-y.^2.*z.*(s-x).*(3*a + 2*(s-x).^2))./(a.^2.*(a + (s-x).^2).^(3/2));
B_23{3} = @(s) (y.*(s-x).*(a.*(a + (s-x).^2) -z.^2.*(3*a + 2*(s-x).^2)))./(a.^2.*(a + (s-x).^2).^(3/2));
B_31{3} = B_13{3};
B_32{3} = B_23{3};
B_33{3} = @(s) (z.*(s-x).*(2*a.*(a + (s-x).^2) -z.^2.*(3*a + 2*(s-x).^2)))./(a.^2.*(a + (s-x).^2).^(3/2));

% Definite integrals:

IB = cell(3,3,3);
for k = 1:3
    IB{1,1,k} = B_11{k}(h) - B_11{k}(-h);
    IB{1,2,k} = B_12{k}(h) - B_12{k}(-h);
    IB{1,3,k} = B_13{k}(h) - B_13{k}(-h);
    IB{2,1,k} = B_21{k}(h) - B_21{k}(-h);
    IB{2,2,k} = B_22{k}(h) - B_22{k}(-h);
    IB{2,3,k} = B_23{k}(h) - B_23{k}(-h);
    IB{3,1,k} = B_31{k}(h) - B_31{k}(-h);
    IB{3,2,k} = B_32{k}(h) - B_32{k}(-h);
    IB{3,3,k} = B_33{k}(h) - B_33{k}(-h);
end
%}

% Without anonymous functions:

IB{1,1,1} = (-(h-x).^2).*ireps_ph - (-(-h-x).^2).*ireps_mh;
IB{1,2,1} = (y.*(h-x)).*ireps_ph - (y.*(-h-x)).*ireps_mh;
IB{1,3,1} = (z.*(h-x)).*ireps_ph - (z.*(-h-x)).*ireps_mh;
IB{2,1,1} = IB{1,2,1};
IB{2,2,1} = (-y.^2).*(ireps_ph - ireps_mh);
IB{2,3,1} = (-y.*z).*(ireps_ph - ireps_mh);
IB{3,1,1} = IB{1,3,1};
IB{3,2,1} = IB{2,3,1};
IB{3,3,1} = (-z.^2).*(ireps_ph - ireps_mh);

IB{1,1,2} = (-y.*(h-x).^3).*ireps_ph_a - (-y.*(-h-x).^3).*ireps_mh_a;
IB{1,2,2} = (a + (h-x).^2 -y.^2).*ireps_ph - (a + (-h-x).^2 -y.^2).*ireps_mh;
IB{1,3,2} = (-y.*z).*(ireps_ph - ireps_mh);
IB{2,1,2} = IB{1,2,2};
IB{2,2,2} = (y.*(h-x).*(2*a.*(a+(h-x).^2) -y.^2.*(3*a + 2*(h-x).^2))).*ireps_ph_a2 - (y.*(-h-x).*(2*a.*(a+(-h-x).^2) -y.^2.*(3*a + 2*(-h-x).^2))).*ireps_mh_a2;
IB{2,3,2} = (z.*(h-x).*(a.*(a+(h-x).^2) -y.^2.*(3*a + 2*(h-x).^2))).*ireps_ph_a2 - (z.*(-h-x).*(a.*(a+(-h-x).^2) -y.^2.*(3*a + 2*(-h-x).^2))).*ireps_mh_a2;
IB{3,1,2} = IB{1,3,2};
IB{3,2,2} = IB{2,3,2};
IB{3,3,2} = (-y.*z.^2.*(h-x).*(3*a + 2*(h-x).^2)).*ireps_ph_a2 - (-y.*z.^2.*(-h-x).*(3*a + 2*(-h-x).^2)).*ireps_mh_a2;

IB{1,1,3} = (-z.*(h-x).^3).*ireps_ph_a - (-z.*(-h-x).^3).*ireps_mh_a;
IB{1,2,3} = (-y.*z).*(ireps_ph - ireps_mh);
IB{1,3,3} = ((h-x).^2 +y.^2 + b^2).*ireps_ph - ((-h-x).^2 +y.^2 + b^2).*ireps_mh;
IB{2,1,3} = IB{1,2,3};
IB{2,2,3} = (-y.^2.*z.*(h-x).*(3*a + 2*(h-x).^2)).*ireps_ph_a2 - (-y.^2.*z.*(-h-x).*(3*a + 2*(-h-x).^2)).*ireps_mh_a2;
IB{2,3,3} = (y.*(h-x).*(a.*(a + (h-x).^2) -z.^2.*(3*a + 2*(h-x).^2))).*ireps_ph_a2 - (y.*(-h-x).*(a.*(a + (-h-x).^2) -z.^2.*(3*a + 2*(-h-x).^2))).*ireps_mh_a2;
IB{3,1,3} = IB{1,3,3};
IB{3,2,3} = IB{2,3,3};
IB{3,3,3} = (z.*(h-x).*(2*a.*(a + (h-x).^2) -z.^2.*(3*a + 2*(h-x).^2))).*ireps_ph_a2 - (z.*(-h-x).*(2*a.*(a + (-h-x).^2) -z.^2.*(3*a + 2*(-h-x).^2))).*ireps_mh_a2;

%% Build integrals:

% Combine terms:
% I = mat2cell(cell2mat(IA(:,:,:)) + cell2mat(IB(:,:,:)),[N,N,N],[Q,Q,Q],[1,1,1]);

AL{1,1} = IA{1,1,1}.*tL{1} + IA{1,1,2}.*tL{2} + IA{1,1,3}.*tL{3};
AL{1,2} = IA{1,2,1}.*tL{1} + IA{1,2,2}.*tL{2} + IA{1,2,3}.*tL{3};
AL{1,3} = IA{1,3,1}.*tL{1} + IA{1,3,2}.*tL{2} + IA{1,3,3}.*tL{3};
AL{2,1} = IA{2,1,1}.*tL{1} + IA{2,1,2}.*tL{2} + IA{2,1,3}.*tL{3};
AL{2,2} = IA{2,2,1}.*tL{1} + IA{2,2,2}.*tL{2} + IA{2,2,3}.*tL{3};
AL{2,3} = IA{2,3,1}.*tL{1} + IA{2,3,2}.*tL{2} + IA{2,3,3}.*tL{3};
AL{3,1} = IA{3,1,1}.*tL{1} + IA{3,1,2}.*tL{2} + IA{3,1,3}.*tL{3};
AL{3,2} = IA{3,2,1}.*tL{1} + IA{3,2,2}.*tL{2} + IA{3,2,3}.*tL{3};
AL{3,3} = IA{3,3,1}.*tL{1} + IA{3,3,2}.*tL{2} + IA{3,3,3}.*tL{3};

BL{1,1} = IB{1,1,1}.*tL{1} + IB{1,1,2}.*tL{2} + IB{1,1,3}.*tL{3};
BL{1,2} = IB{1,2,1}.*tL{1} + IB{1,2,2}.*tL{2} + IB{1,2,3}.*tL{3};
BL{1,3} = IB{1,3,1}.*tL{1} + IB{1,3,2}.*tL{2} + IB{1,3,3}.*tL{3};
BL{2,1} = IB{2,1,1}.*tL{1} + IB{2,1,2}.*tL{2} + IB{2,1,3}.*tL{3};
BL{2,2} = IB{2,2,1}.*tL{1} + IB{2,2,2}.*tL{2} + IB{2,2,3}.*tL{3};
BL{2,3} = IB{2,3,1}.*tL{1} + IB{2,3,2}.*tL{2} + IB{2,3,3}.*tL{3};
BL{3,1} = IB{3,1,1}.*tL{1} + IB{3,1,2}.*tL{2} + IB{3,1,3}.*tL{3};
BL{3,2} = IB{3,2,1}.*tL{1} + IB{3,2,2}.*tL{2} + IB{3,2,3}.*tL{3};
BL{3,3} = IB{3,3,1}.*tL{1} + IB{3,3,2}.*tL{2} + IB{3,3,3}.*tL{3};

DL{1,1} = AL{1,1} + BL{1,1};
DL{1,2} = AL{1,2} + BL{1,2};
DL{1,3} = AL{1,3} + BL{1,3};
DL{2,1} = AL{2,1} + BL{2,1};
DL{2,2} = AL{2,2} + BL{2,2};
DL{2,3} = AL{2,3} + BL{2,3};
DL{3,1} = AL{3,1} + BL{3,1};
DL{3,2} = AL{3,2} + BL{3,2};
DL{3,3} = AL{3,3} + BL{3,3};

% D:

D{1,1}=(RM{1,1}.*DL{1,1}+RM{1,2}.*DL{2,1}+RM{1,3}.*DL{3,1}).*RM{1,1}...
      +(RM{1,1}.*DL{1,2}+RM{1,2}.*DL{2,2}+RM{1,3}.*DL{3,2}).*RM{1,2}...
      +(RM{1,1}.*DL{1,3}+RM{1,2}.*DL{2,3}+RM{1,3}.*DL{3,3}).*RM{1,3};

D{1,2}=(RM{1,1}.*DL{1,1}+RM{1,2}.*DL{2,1}+RM{1,3}.*DL{3,1}).*RM{2,1}...
      +(RM{1,1}.*DL{1,2}+RM{1,2}.*DL{2,2}+RM{1,3}.*DL{3,2}).*RM{2,2}...
      +(RM{1,1}.*DL{1,3}+RM{1,2}.*DL{2,3}+RM{1,3}.*DL{3,3}).*RM{2,3};

D{1,3}=(RM{1,1}.*DL{1,1}+RM{1,2}.*DL{2,1}+RM{1,3}.*DL{3,1}).*RM{3,1}...
      +(RM{1,1}.*DL{1,2}+RM{1,2}.*DL{2,2}+RM{1,3}.*DL{3,2}).*RM{3,2}...
      +(RM{1,1}.*DL{1,3}+RM{1,2}.*DL{2,3}+RM{1,3}.*DL{3,3}).*RM{3,3};

D{2,1}=(RM{2,1}.*DL{1,1}+RM{2,2}.*DL{2,1}+RM{2,3}.*DL{3,1}).*RM{1,1}...
      +(RM{2,1}.*DL{1,2}+RM{2,2}.*DL{2,2}+RM{2,3}.*DL{3,2}).*RM{1,2}...
      +(RM{2,1}.*DL{1,3}+RM{2,2}.*DL{2,3}+RM{2,3}.*DL{3,3}).*RM{1,3};

D{2,2}=(RM{2,1}.*DL{1,1}+RM{2,2}.*DL{2,1}+RM{2,3}.*DL{3,1}).*RM{2,1}...
      +(RM{2,1}.*DL{1,2}+RM{2,2}.*DL{2,2}+RM{2,3}.*DL{3,2}).*RM{2,2}...
      +(RM{2,1}.*DL{1,3}+RM{2,2}.*DL{2,3}+RM{2,3}.*DL{3,3}).*RM{2,3};

D{2,3}=(RM{2,1}.*DL{1,1}+RM{2,2}.*DL{2,1}+RM{2,3}.*DL{3,1}).*RM{3,1}...
      +(RM{2,1}.*DL{1,2}+RM{2,2}.*DL{2,2}+RM{2,3}.*DL{3,2}).*RM{3,2}...
      +(RM{2,1}.*DL{1,3}+RM{2,2}.*DL{2,3}+RM{2,3}.*DL{3,3}).*RM{3,3};

D{3,1}=(RM{3,1}.*DL{1,1}+RM{3,2}.*DL{2,1}+RM{3,3}.*DL{3,1}).*RM{1,1}...
      +(RM{3,1}.*DL{1,2}+RM{3,2}.*DL{2,2}+RM{3,3}.*DL{3,2}).*RM{1,2}...
      +(RM{3,1}.*DL{1,3}+RM{3,2}.*DL{2,3}+RM{3,3}.*DL{3,3}).*RM{1,3};

D{3,2}=(RM{3,1}.*DL{1,1}+RM{3,2}.*DL{2,1}+RM{3,3}.*DL{3,1}).*RM{2,1}...
      +(RM{3,1}.*DL{1,2}+RM{3,2}.*DL{2,2}+RM{3,3}.*DL{3,2}).*RM{2,2}...
      +(RM{3,1}.*DL{1,3}+RM{3,2}.*DL{2,3}+RM{3,3}.*DL{3,3}).*RM{2,3};

D{3,3}=(RM{3,1}.*DL{1,1}+RM{3,2}.*DL{2,1}+RM{3,3}.*DL{3,1}).*RM{3,1}...
      +(RM{3,1}.*DL{1,2}+RM{3,2}.*DL{2,2}+RM{3,3}.*DL{3,2}).*RM{3,2}...
      +(RM{3,1}.*DL{1,3}+RM{3,2}.*DL{2,3}+RM{3,3}.*DL{3,3}).*RM{3,3};

%% Additional outputs: suppressed for efficiency:
%{

% A:

A{1,1}=(RM{1,1}.*AL{1,1}+RM{1,2}.*AL{2,1}+RM{1,3}.*AL{3,1}).*RM{1,1}...
      +(RM{1,1}.*AL{1,2}+RM{1,2}.*AL{2,2}+RM{1,3}.*AL{3,2}).*RM{1,2}...
      +(RM{1,1}.*AL{1,3}+RM{1,2}.*AL{2,3}+RM{1,3}.*AL{3,3}).*RM{1,3};

A{1,2}=(RM{1,1}.*AL{1,1}+RM{1,2}.*AL{2,1}+RM{1,3}.*AL{3,1}).*RM{2,1}...
      +(RM{1,1}.*AL{1,2}+RM{1,2}.*AL{2,2}+RM{1,3}.*AL{3,2}).*RM{2,2}...
      +(RM{1,1}.*AL{1,3}+RM{1,2}.*AL{2,3}+RM{1,3}.*AL{3,3}).*RM{2,3};

A{1,3}=(RM{1,1}.*AL{1,1}+RM{1,2}.*AL{2,1}+RM{1,3}.*AL{3,1}).*RM{3,1}...
      +(RM{1,1}.*AL{1,2}+RM{1,2}.*AL{2,2}+RM{1,3}.*AL{3,2}).*RM{3,2}...
      +(RM{1,1}.*AL{1,3}+RM{1,2}.*AL{2,3}+RM{1,3}.*AL{3,3}).*RM{3,3};

A{2,1}=(RM{2,1}.*AL{1,1}+RM{2,2}.*AL{2,1}+RM{2,3}.*AL{3,1}).*RM{1,1}...
      +(RM{2,1}.*AL{1,2}+RM{2,2}.*AL{2,2}+RM{2,3}.*AL{3,2}).*RM{1,2}...
      +(RM{2,1}.*AL{1,3}+RM{2,2}.*AL{2,3}+RM{2,3}.*AL{3,3}).*RM{1,3};

A{2,2}=(RM{2,1}.*AL{1,1}+RM{2,2}.*AL{2,1}+RM{2,3}.*AL{3,1}).*RM{2,1}...
      +(RM{2,1}.*AL{1,2}+RM{2,2}.*AL{2,2}+RM{2,3}.*AL{3,2}).*RM{2,2}...
      +(RM{2,1}.*AL{1,3}+RM{2,2}.*AL{2,3}+RM{2,3}.*AL{3,3}).*RM{2,3};

A{2,3}=(RM{2,1}.*AL{1,1}+RM{2,2}.*AL{2,1}+RM{2,3}.*AL{3,1}).*RM{3,1}...
      +(RM{2,1}.*AL{1,2}+RM{2,2}.*AL{2,2}+RM{2,3}.*AL{3,2}).*RM{3,2}...
      +(RM{2,1}.*AL{1,3}+RM{2,2}.*AL{2,3}+RM{2,3}.*AL{3,3}).*RM{3,3};

A{3,1}=(RM{3,1}.*AL{1,1}+RM{3,2}.*AL{2,1}+RM{3,3}.*AL{3,1}).*RM{1,1}...
      +(RM{3,1}.*AL{1,2}+RM{3,2}.*AL{2,2}+RM{3,3}.*AL{3,2}).*RM{1,2}...
      +(RM{3,1}.*AL{1,3}+RM{3,2}.*AL{2,3}+RM{3,3}.*AL{3,3}).*RM{1,3};

A{3,2}=(RM{3,1}.*AL{1,1}+RM{3,2}.*AL{2,1}+RM{3,3}.*AL{3,1}).*RM{2,1}...
      +(RM{3,1}.*AL{1,2}+RM{3,2}.*AL{2,2}+RM{3,3}.*AL{3,2}).*RM{2,2}...
      +(RM{3,1}.*AL{1,3}+RM{3,2}.*AL{2,3}+RM{3,3}.*AL{3,3}).*RM{2,3};

A{3,3}=(RM{3,1}.*AL{1,1}+RM{3,2}.*AL{2,1}+RM{3,3}.*AL{3,1}).*RM{3,1}...
      +(RM{3,1}.*AL{1,2}+RM{3,2}.*AL{2,2}+RM{3,3}.*AL{3,2}).*RM{3,2}...
      +(RM{3,1}.*AL{1,3}+RM{3,2}.*AL{2,3}+RM{3,3}.*AL{3,3}).*RM{3,3};

% B:

B{1,1}=(RM{1,1}.*BL{1,1}+RM{1,2}.*BL{2,1}+RM{1,3}.*BL{3,1}).*RM{1,1}...
      +(RM{1,1}.*BL{1,2}+RM{1,2}.*BL{2,2}+RM{1,3}.*BL{3,2}).*RM{1,2}...
      +(RM{1,1}.*BL{1,3}+RM{1,2}.*BL{2,3}+RM{1,3}.*BL{3,3}).*RM{1,3};

B{1,2}=(RM{1,1}.*BL{1,1}+RM{1,2}.*BL{2,1}+RM{1,3}.*BL{3,1}).*RM{2,1}...
      +(RM{1,1}.*BL{1,2}+RM{1,2}.*BL{2,2}+RM{1,3}.*BL{3,2}).*RM{2,2}...
      +(RM{1,1}.*BL{1,3}+RM{1,2}.*BL{2,3}+RM{1,3}.*BL{3,3}).*RM{2,3};

B{1,3}=(RM{1,1}.*BL{1,1}+RM{1,2}.*BL{2,1}+RM{1,3}.*BL{3,1}).*RM{3,1}...
      +(RM{1,1}.*BL{1,2}+RM{1,2}.*BL{2,2}+RM{1,3}.*BL{3,2}).*RM{3,2}...
      +(RM{1,1}.*BL{1,3}+RM{1,2}.*BL{2,3}+RM{1,3}.*BL{3,3}).*RM{3,3};

B{2,1}=(RM{2,1}.*BL{1,1}+RM{2,2}.*BL{2,1}+RM{2,3}.*BL{3,1}).*RM{1,1}...
      +(RM{2,1}.*BL{1,2}+RM{2,2}.*BL{2,2}+RM{2,3}.*BL{3,2}).*RM{1,2}...
      +(RM{2,1}.*BL{1,3}+RM{2,2}.*BL{2,3}+RM{2,3}.*BL{3,3}).*RM{1,3};

B{2,2}=(RM{2,1}.*BL{1,1}+RM{2,2}.*BL{2,1}+RM{2,3}.*BL{3,1}).*RM{2,1}...
      +(RM{2,1}.*BL{1,2}+RM{2,2}.*BL{2,2}+RM{2,3}.*BL{3,2}).*RM{2,2}...
      +(RM{2,1}.*BL{1,3}+RM{2,2}.*BL{2,3}+RM{2,3}.*BL{3,3}).*RM{2,3};

B{2,3}=(RM{2,1}.*BL{1,1}+RM{2,2}.*BL{2,1}+RM{2,3}.*BL{3,1}).*RM{3,1}...
      +(RM{2,1}.*BL{1,2}+RM{2,2}.*BL{2,2}+RM{2,3}.*BL{3,2}).*RM{3,2}...
      +(RM{2,1}.*BL{1,3}+RM{2,2}.*BL{2,3}+RM{2,3}.*BL{3,3}).*RM{3,3};

B{3,1}=(RM{3,1}.*BL{1,1}+RM{3,2}.*BL{2,1}+RM{3,3}.*BL{3,1}).*RM{1,1}...
      +(RM{3,1}.*BL{1,2}+RM{3,2}.*BL{2,2}+RM{3,3}.*BL{3,2}).*RM{1,2}...
      +(RM{3,1}.*BL{1,3}+RM{3,2}.*BL{2,3}+RM{3,3}.*BL{3,3}).*RM{1,3};

B{3,2}=(RM{3,1}.*BL{1,1}+RM{3,2}.*BL{2,1}+RM{3,3}.*BL{3,1}).*RM{2,1}...
      +(RM{3,1}.*BL{1,2}+RM{3,2}.*BL{2,2}+RM{3,3}.*BL{3,2}).*RM{2,2}...
      +(RM{3,1}.*BL{1,3}+RM{3,2}.*BL{2,3}+RM{3,3}.*BL{3,3}).*RM{2,3};

B{3,3}=(RM{3,1}.*BL{1,1}+RM{3,2}.*BL{2,1}+RM{3,3}.*BL{3,1}).*RM{3,1}...
      +(RM{3,1}.*BL{1,2}+RM{3,2}.*BL{2,2}+RM{3,3}.*BL{3,2}).*RM{3,2}...
      +(RM{3,1}.*BL{1,3}+RM{3,2}.*BL{2,3}+RM{3,3}.*BL{3,3}).*RM{3,3};
%}

%% Output

if nargout >= 1
    D = cell2mat(D)/8.0/pi;
    varargout{1} = D;
end
if nargout > 1
    
    A = cell2mat(A)/8.0/pi;
    B = cell2mat(B)/8.0/pi;
    
    varargout{2} = A;
    varargout{3} = B;
end
    
end % function

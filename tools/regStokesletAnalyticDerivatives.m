function varargout = RegStokesletAnalyticDerivatives(X, Y, epsilon, T_X)

% Computes the derivatives with respect to s of the regularised Stokeslet.
% X and Y are 3Nx1 and 3Mx1 vectors of coordinates of field points and
% source points respectively.
% epsilon is the regularisation parameter.
% T is the 3Nx1 vector coordinates defining the unit tangent at each field
% point.

% Returns both the derivative of X with respect to (a) arclength s, and (b)
% curve X(s).

%% Compute difference terms:
% Assuming that in the general case X and Y are not of equal length.

N = length(X(:))/3;
M = length(Y(:))/3;

[X1, X2, X3] = extractComponents(X);
[Y1, Y2, Y3] = extractComponents(Y);

r1 = X1 - Y1';
r2 = X2 - Y2';
r3 = X3 - Y3';

rsq = r1.^2 + r2.^2 + r3.^2;
reps = sqrt(rsq + epsilon^2);
reps2 = rsq + epsilon^2;
reps5 = reps.^5;

eps2 = epsilon^2;

%% Compute unit tangents using known flagellar shape (via theta):

t{1} = T_X(1:N);
t{2} = T_X(N+1:2*N);
t{3} = T_X(2*N+1:3*N);

%% Derivatives of isotropic term (dX_k A_{ij}):

dA(:,:,1) = -kron(eye(3),(r1.*(rsq + 4*eps2))./reps5);
dA(:,:,2) = -kron(eye(3),(r2.*(rsq + 4*eps2))./reps5);
dA(:,:,3) = -kron(eye(3),(r3.*(rsq + 4*eps2))./reps5);

%% Derivatives of dyadic term (dX_k B_{ij}):

dB(1:N,1:M,1)               = (r1.*(2*reps2 - 3*r1.^2))./reps5;
dB(1:N,M+1:2*M,1)           = (r2.*(reps2 - 3*r1.^2))./reps5;
dB(1:N,2*M+1:3*M,1)         = (r3.*(reps2 - 3*r1.^2))./reps5;
dB(N+1:2*N,1:M,1)           = dB(1:N,M+1:2*M,1);
dB(N+1:2*N,M+1:2*M,1)       = (-3*r1.*r2.^2)./reps5;
dB(N+1:2*N,2*M+1:3*M,1)     = (-3*r1.*r2.*r3)./reps5;
dB(2*N+1:3*N,1:M,1)         = dB(1:N,2*M+1:3*M,1);
dB(2*N+1:3*N,M+1:2*M,1)     = dB(N+1:2*N,2*M+1:3*M,1);
dB(2*N+1:3*N,2*M+1:3*M,1)   = (-3*r1.*r3.^2)./reps5;

dB(1:N,1:M,2)               = (-3*r1.^2.*r2)./reps5;
dB(1:N,M+1:2*M,2)           = (r1.*(reps2 - 3*r2.^2))./reps5;
dB(1:N,2*M+1:3*M,2)         = (-3*r1.*r2.*r3)./reps5;
dB(N+1:2*N,1:M,2)           = dB(1:N,M+1:2*M,2);
dB(N+1:2*N,M+1:2*M,2)       = (r2.*(2*reps2 - 3*r2.^2))./reps5;
dB(N+1:2*N,2*M+1:3*M,2)     = (r3.*(reps2 - 3*r2.^2))./reps5;
dB(2*N+1:3*N,1:M,2)         = dB(1:N,2*M+1:3*M,2);
dB(2*N+1:3*N,M+1:2*M,2)     = dB(N+1:2*N,2*M+1:3*M,2);
dB(2*N+1:3*N,2*M+1:3*M,2)   = (-3*r2.*r3.^2)./reps5;

dB(1:N,1:M,3)               = (-3*r1.^2.*r3)./reps5;
dB(1:N,M+1:2*M,3)           = (-3*r1.*r2.*r3)./reps5;
dB(1:N,2*M+1:3*M,3)         = (r1.*(reps2 - 3*r3.^2))./reps5;
dB(N+1:2*N,1:M,3)           = dB(1:N,M+1:2*M,3);
dB(N+1:2*N,M+1:2*M,3)       = (-3*r2.^2.*r3)./reps5;
dB(N+1:2*N,2*M+1:3*M,3)     = (r2.*(reps2 - 3*r3.^2))./reps5;
dB(2*N+1:3*N,1:M,3)         = dB(1:N,2*M+1:3*M,3);
dB(2*N+1:3*N,M+1:2*M,3)     = dB(N+1:2*N,2*M+1:3*M,3);
dB(2*N+1:3*N,2*M+1:3*M,3)   = (r3.*(2*reps2 - 3*r3.^2))./reps5;

%% Combine to find full derivative w.r.t. X(s):

% Add components:
dS{1,1,1} =              dA(1:N,1:M,1) + dB(1:N,1:M,1);
dS{1,2,1} =          dA(1:N,M+1:2*M,1) + dB(1:N,M+1:2*M,1);
dS{1,3,1} =        dA(1:N,2*M+1:3*M,1) + dB(1:N,2*M+1:3*M,1);
dS{2,1,1} =          dA(N+1:2*N,1:M,1) + dB(N+1:2*N,1:M,1);
dS{2,2,1} =      dA(N+1:2*N,M+1:2*M,1) + dB(N+1:2*N,M+1:2*M,1);
dS{2,3,1} =    dA(N+1:2*N,2*M+1:3*M,1) + dB(N+1:2*N,2*M+1:3*M,1);
dS{3,1,1} =        dA(2*N+1:3*N,1:M,1) + dB(2*N+1:3*N,1:M,1);
dS{3,2,1} =    dA(2*N+1:3*N,M+1:2*M,1) + dB(2*N+1:3*N,M+1:2*M,1);
dS{3,3,1} =  dA(2*N+1:3*N,2*M+1:3*M,1) + dB(2*N+1:3*N,2*M+1:3*M,1);

dS{1,1,2} =              dA(1:N,1:M,2) + dB(1:N,1:M,2);
dS{1,2,2} =          dA(1:N,M+1:2*M,2) + dB(1:N,M+1:2*M,2);
dS{1,3,2} =        dA(1:N,2*M+1:3*M,2) + dB(1:N,2*M+1:3*M,2);
dS{2,1,2} =          dA(N+1:2*N,1:M,2) + dB(N+1:2*N,1:M,2);
dS{2,2,2} =      dA(N+1:2*N,M+1:2*M,2) + dB(N+1:2*N,M+1:2*M,2);
dS{2,3,2} =    dA(N+1:2*N,2*M+1:3*M,2) + dB(N+1:2*N,2*M+1:3*M,2);
dS{3,1,2} =        dA(2*N+1:3*N,1:M,2) + dB(2*N+1:3*N,1:M,2);
dS{3,2,2} =    dA(2*N+1:3*N,M+1:2*M,2) + dB(2*N+1:3*N,M+1:2*M,2);
dS{3,3,2} =  dA(2*N+1:3*N,2*M+1:3*M,2) + dB(2*N+1:3*N,2*M+1:3*M,2);

dS{1,1,3} =              dA(1:N,1:M,3) + dB(1:N,1:M,3);
dS{1,2,3} =          dA(1:N,M+1:2*M,3) + dB(1:N,M+1:2*M,3);
dS{1,3,3} =        dA(1:N,2*M+1:3*M,3) + dB(1:N,2*M+1:3*M,3);
dS{2,1,3} =          dA(N+1:2*N,1:M,3) + dB(N+1:2*N,1:M,3);
dS{2,2,3} =      dA(N+1:2*N,M+1:2*M,3) + dB(N+1:2*N,M+1:2*M,3);
dS{2,3,3} =    dA(N+1:2*N,2*M+1:3*M,3) + dB(N+1:2*N,2*M+1:3*M,3);
dS{3,1,3} =        dA(2*N+1:3*N,1:M,3) + dB(2*N+1:3*N,1:M,3);
dS{3,2,3} =    dA(2*N+1:3*N,M+1:2*M,3) + dB(2*N+1:3*N,M+1:2*M,3);
dS{3,3,3} =  dA(2*N+1:3*N,2*M+1:3*M,3) + dB(2*N+1:3*N,2*M+1:3*M,3);

% S_X is a rank-3 tensor.
% Ouput:
S_X = cell2mat(dS)/8.0/pi;
% S_X = 1/8/pi * dS;

%% Use product rule to find the derivative w.r.t. s:

% S_s is a rank-2 tensor.
% Sum over k to reduce to a rank-2 tensor:
i = [1,2,3]; j = [1,2,3]; ij = combvec(i,j);
for n = 1:9
    S_s{ij(1,n),ij(2,n)} = 0;
    for k = [1,2,3]
        S_s{ij(1,n),ij(2,n)} = S_s{ij(1,n),ij(2,n)} + t{k}.*(dS{ij(1,n),ij(2,n),k});
    end
end

% Output:
S_s = cell2mat(S_s)/8.0/pi;

%% Outputs:

% Allow for single variable output, by default the derivative with respect
% to s.

if nargout >= 0
    varargout{1} = S_s;
end
if nargout >= 2
    varargout{2} = S_X;
end

end % function

function [idx,idy] = findIndex(arr, type)

% find indices of extreme (either 'min' or 'max') from the 2D array arr

switch type
    case 'min'
        val = min(min(arr));
    case 'max'
        val = max(max(arr));
end

[idx,idy] = find(arr==val);

end % function
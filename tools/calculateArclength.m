function L = calculateArclength(X)

% Check the arclength of the curve defined through Cartesian coordinates x
% and y.

[x,y,z] = extractComponents(X);

dx = diff(x);
dy = diff(y);
dz = diff(z);
L  = sum(vecnorm([dx(:)';dy(:)';dz(:)'],2,1));

end % function
function Z = vecdot(X,Y)

% Computes the dot (scalar) products of each coordinate defined in the 3Mx1
% column vectors X and Y. 
% Output Z is an Mx1 vector.

Z = sum(VectorToMatrix(X.*Y))';

end
function [d1,d2,d3] = buildDirectorFrame(k1,k2,k3,d10,d20,d30)

% computes the director frame from given curvatures k1, k2, k3 
% and starting values (at s=0) d01, d02, d03.

% number of nodes:
N = length(k1);

% grid spacing:
ds = 1/(N-1);

% ensure column vectors:
k1 = k1(:); k2 = k2(:); k3 = k3(:);

%% midpoint rule:

%{x
% arclength parameterisation:
s = linspace(0,1,N);

% midpoint values:
sm = 0.5*(s(1:N-1)+s(2:N));
kappa = [k1(:); k2(:); k3(:)];
[k1m,k2m,k3m] = extractComponents(midpoints(kappa));

% identity operators:
I = eye(3*(N-1));
A11 = I;    A22 = I;    A33 = I;

% integral operators:
Ones = ones(N-1);
i12 = -ds*tril(k3m.*Ones,-1) - ds/2*diag(k3m);     
i13 =  ds*tril(k2m.*Ones,-1) + ds/2*diag(k2m);
i21 =  ds*tril(k3m.*Ones,-1) + ds/2*diag(k3m);      
i23 = -ds*tril(k1m.*Ones,-1) - ds/2*diag(k1m);
i31 = -ds*tril(k2m.*Ones,-1) - ds/2*diag(k2m);     
i32 =  ds*tril(k1m.*Ones,-1) + ds/2*diag(k1m);
A12 = blkdiag(i12,i12,i12); 
A13 = blkdiag(i13,i13,i13);
A21 = blkdiag(i21,i21,i21); 
A23 = blkdiag(i23,i23,i23);
A31 = blkdiag(i31,i31,i31); 
A32 = blkdiag(i32,i32,i32);
A = [A11,A12,A13; A21,A22,A23; A31,A32,A33];

% right hand sides:
RHS1 = kron(d10,ones(N-1,1));
RHS2 = kron(d20,ones(N-1,1));
RHS3 = kron(d30,ones(N-1,1));
b    = [RHS1(:); RHS2(:); RHS3(:)];

% solve:
[d1m,d2m,d3m] = extractComponents(A\b);

% spline to obtain values at nodes:
[d1m1,d1m2,d1m3] = extractComponents(d1m);
[d2m1,d2m2,d2m3] = extractComponents(d2m);
[d3m1,d3m2,d3m3] = extractComponents(d3m);
d11 = interp1(sm, d1m1, s, 'linear', 'extrap');    
d12 = interp1(sm, d1m2, s, 'linear', 'extrap');    
d13 = interp1(sm, d1m3, s, 'linear', 'extrap');
d21 = interp1(sm, d2m1, s, 'linear', 'extrap');    
d22 = interp1(sm, d2m2, s, 'linear', 'extrap');    
d23 = interp1(sm, d2m3, s, 'linear', 'extrap');
d31 = interp1(sm, d3m1, s, 'linear', 'extrap');    
d32 = interp1(sm, d3m2, s, 'linear', 'extrap');    
d33 = interp1(sm, d3m3, s, 'linear', 'extrap');
d1 = [d11(:); d12(:); d13(:)];
d2 = [d21(:); d22(:); d23(:)];
d3 = [d31(:); d32(:); d33(:)];

% normalise values:
d1 = normalise(d1);
d2 = normalise(d2);
d3 = normalise(d3);
%}

%% trapezoidal rule:

%{
% right hand side
RHS1 = kron(d10,ones(N,1));
RHS2 = kron(d20,ones(N,1));
RHS3 = kron(d30,ones(N,1));
b    = [RHS1(:); RHS2(:); RHS3(:)];

% integral operators:
Ones = ones(N);
i12 = -ds/2*(tril(k3.*Ones)+tril(k3.*Ones,-1));
i13 =  ds/2*(tril(k2.*Ones)+tril(k2.*Ones,-1));
i21 =  ds/2*(tril(k3.*Ones)+tril(k3.*Ones,-1));
i23 = -ds/2*(tril(k1.*Ones)+tril(k1.*Ones,-1));
i31 = -ds/2*(tril(k2.*Ones)+tril(k2.*Ones,-1));
i32 =  ds/2*(tril(k1.*Ones)+tril(k1.*Ones,-1));
A11 = eye(3*N);
A22 = A11;
A33 = A11;
A12 = blkdiag(i12,i12,i12); 
A13 = blkdiag(i13,i13,i13);
A21 = blkdiag(i21,i21,i21); 
A23 = blkdiag(i23,i23,i23);
A31 = blkdiag(i31,i31,i31); 
A32 = blkdiag(i32,i32,i32);
A = [A11,A12,A13; A21,A22,A23; A31,A32,A33];

% solve
[d1,d2,d3] = extractComponents(A\b);

% normalise
d1 = normalise(d1);
d2 = normalise(d2);
d3 = normalise(d3);
%}

end % function
function ClearFunctions

% Clears functions and variables which have persistent variables defined
% within them.
% Clearing is required to avoid data from previous time steps being
% erroneously used in subsequent time steps.

% NB: CurveDerivatives removed from this list as that function is used to
% find tangents at both midpoints and nodes, and one set of values was
% overwriting the other!

clear SplineCurve  

end 
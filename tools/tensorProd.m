function TT = tensorProd(T)

%TENSORPROD Build the dyadic (tensor) product operator for a vector with
%itself.

%{
TT2 = kron(T,T');
N = length(T)/3;

TTXX = TT2(1:N,1:N);        
TTXY = TT2(1:N,N+1:2*N);        
TTXZ = TT2(1:N,2*N+1:3*N);

TTYX = TT2(N+1:2*N,1:N);
TTYY = TT2(N+1:2*N,N+1:2*N);
TTYZ = TT2(N+1:2*N,2*N+1:3*N);

TTZX = TT2(2*N+1:3*N,1:N);
TTZY = TT2(2*N+1:3*N,N+1:2*N);
TTZZ = TT2(2*N+1:3*N,2*N+1:3*N);

TT = [diag(diag(TTXX)), diag(diag(TTXY)), diag(diag(TTXZ)); ...
      diag(diag(TTYX)), diag(diag(TTYY)), diag(diag(TTYZ)); ...
      diag(diag(TTZX)), diag(diag(TTZY)), diag(diag(TTZZ))];
%}

N  = length(T)/3;
TT = repmat(eye(N),3,3).*kron(T,T');

end
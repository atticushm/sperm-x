function fm_elas = ElasticForce(X, T, m, m_s, nn, model)

% computes the elastic force per unit length acting on the filament,
% derived from equilibrium equations for a bending rod.

% extract parameters from model:
N = model.discr.N;

% dimensionless groups:
calS4 = model.calS4;
calM = model.calM;

% stiffness parameters:
E = model.E;            E_rep = repmat(E,3,1);
E_s = model.E_s;        E_s_rep = repmat(E_s,3,1);
E_ss = model.E_ss;      E_ss_rep = repmat(E_ss,3,1);

% arclength discretisation:
s = linspace(0,1,N);

% finite difference matrices:
D = finiteDifferences(s, 1:4);

% finite difference operators:
D_s = blkdiag(D{1},D{1},D{1}); D_ss = blkdiag(D{2},D{2},D{2});
D_sss = blkdiag(D{3},D{3},D{3}); D_ssss = blkdiag(D{4},D{4},D{4});

% derivatives of curve:
X_s = D_s * X;
X_ss = D_ss * X;
X_sss = D_sss * X;
X_ssss = D_ssss * X;

% derivatives of normals:
nn_s = D_s * nn;

% derivatives of tension:
d_s = D{1};
T_s = d_s * T;          
T_s_rep = repmat(T_s,3,1);
T_rep = repmat(T,3,1);

% active moment terms:
m_rep = repmat(m,3,1);
m_s_rep = repmat(m_s,3,1);

% elastic force per unit length is then:
f_elas = E_rep.*X_ssss - 2*E_s_rep.*X_sss + E_ss_rep.*X_ss ...
         - T_rep.*X_ss - T_s_rep.*X_s ...
         - calS4*calM*(m_rep.*nn_s - m_s_rep.*nn);
     
% values at midpoints:
fm_elas = Midpoints(f_elas);

end % function
function [numRoll, rollRate, rollRateFull, rollTh, rollThFull, rollIdx] ...
    = calculateRollRate(d10, d20, d30, tps, params)

% d10 is matrix of d1(0) i.e. head material normal across the simulation.
% similarly for d20, d30
% tps is vector of time points correspond to d10 slices

% we need to rotate head frames so that d3 is aligned with the x axis at
% each instant.
% compute this rotation matrix and rotate each frame
xx = [1;0;0]; yy = [0;1;0]; zz = [0;0;1];
for n = 1:length(tps)
    S = reflection(eye(3), d30(:,n)+xx);
    R = reflection(S, xx);

    rd10(:,n) = R * d10(:,n);
    rd20(:,n) = R * d20(:,n);
    rd30(:,n) = R * d30(:,n); % should be xx, good to check!
end

% determine angle with z axis of d1 at each instant
% (z axis since cell is xz planar, at least initially, but shouldn't
% matter)
% for n = 1:length(tps)
%     rollTh(n) = acos(dot(d10(:,n), zz));
% end
for n = 1:length(tps)
    v1 = rd20(2:3,n); x1=v1(1); y1=v1(2);
    v2 = [1;0]; x2=v2(1); y2=v2(2);
    rollThFull(n) = atan2(x1*y2-y1*x2, x1*x2+y1*y2);
end
rollTh = wrapTo2Pi(rollThFull); % wrap to [0,2pi] interval

% find number of rotations and average rate of rotation
tol = 0.02;
[pks, idx] = findpeaks(rollThFull, 'Threshold',tol, 'MinPeakDistance',20);
numRoll = length(pks);
if numRoll == 0
    rollRate = 0;
    rollRateFull = 0;
else
    rollRateFull = 2*pi./diff(tps(idx)); % rate of each revolution
    rollRate = mean(rollRateFull); % average roll rate
end
rollIdx = idx;


end % function
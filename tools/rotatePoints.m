function X_R = rotatePoints(X, R, Xc)

% Rotate N points around the point Xc according to the rotation matrix R.
% X is a 3Nx1 vector of coordinates to be rotated.
% R is a 3x3 rotation matrix.
% Xc is the point around which to rotate.

N = length(X)/3;

% translate points
X_t = X(:) - kron(Xc,ones(N,1));
XM_t = mat(X_t);

% rotate by R
XM_r = R * XM_t;
X_r = vec(XM_r);

% undo translation
X_R = X_r + kron(Xc,ones(N,1));

end % function
function v = extractPath(X)

% given NF coordinates of cell at NT time points, returns the path of the
% head-flagellum join X0

NT = size(X,2);
NF = size(X,1)/3;

% path of leading point
v = [X(1,:); X(1+NF,:); X(1+2*NF,:)];

end % function
function err = bcDebug(X, Y, T, d1, d2, k3, f, phi, t, params, options)

% See notes "Fixing Hydrodynamic Model" for corresponding equations checked
% at each step.

% discretisation parameters
NF  = params.NF;                % # of flagellum nodes
NNB = params.NNB;               % NN matrix for head

% extract parameters
Gam_s = params.Gam_s;
Gam_d = params.Gam_d;
calM3 = params.calM3;

% compute actuation values
[m1,m2,m3,~,~] = computeActuationValues(t, params, options);

% bending stiffness values
[E,E_s,E_ss] = computeStiffness(d1, d2, options);

% arclength discretisation
s = linspace(0,1,NF);

% generate finite difference schemes
D  = finiteDifferences(s, 1:4);
D1 = kron(eye(3),D{1});  
D2 = kron(eye(3),D{2}); 
D3 = kron(eye(3),D{3});
D4 = kron(eye(3),D{4});

% derivatives
X_s = normalise(D1*X); 
X_ss = D2*X;
X_sss = D3*X;
X_ssss = D4*X;
T_s = D{1}*T;
k3_s = D{1}*k3;

% head-flagellum join and derivates there
X0 = proxp(X); 
X0_s = proxp(X_s);

% compute actuation values
[mvec, mvec_s, mact] = computeActuationVector(t, d1, d2, X_s, params, options);

% repmats
Trep = repmat(T,3,1);
T_srep = repmat(T_s,3,1);
k3_rep = repmat(k3,3,1);
k3_srep = repmat(k3_s,3,1);

% body force and moment
Fbody = calcBodyForce(phi, NNB);
Mbody = calcBodyMoment(Y,X0,phi,NNB);

%% (A) FORCE BOUNDARY CONDITION

% these errors may be large when checking an untwisting filament, since
% force values are very small -- don't worry about it.

% as implemented
lhs = Fbody;
rhs = proxp(-E.*X_sss - E_s.*X_ss + Trep.*X_s ...
    + Gam_s*k3_rep.*crossProdOp(X_s)*X_ss + crossProdOp(X_s)*mvec);
err.a1 = relErr(lhs,rhs);

% equivalency of Fint_s and f
Fint = -E.*X_sss - E_s.*X_ss + Trep.*X_s + ...
    Gam_s*k3_rep.*crossProdOp(X_s)*X_ss + crossProdOp(X_s)*mvec;
lhs = D1 * Fint;
rhs = f;
err.a2 = relErr(lhs, rhs);

% replace head integral with negative of flag integral
lhs = -calcFlagForce(f);
rhs = proxp(-E.*X_sss - E_s.*X_ss + Trep.*X_s ...
    + Gam_s*k3_rep.*crossProdOp(X_s)*X_ss + crossProdOp(X_s)*mvec);
err.a3 = relErr(lhs,rhs);

% replace Fint(0) with integral over head
% this is total hydrodynamic force balance
lhs = Fbody;
rhs = -calcFlagForce(f);
If  = computeFlagForceIntegrals(f);
err.a4 = norm(abs(lhs-rhs))/mean(vecnorm(mat(If)));

%% (B) MOMENT BOUNDARY CONDITION

% as implemented
[MHx,MHy,MHz] = buildMBCrossOp(X,Y,X0,X_s,params,'debug');
lhs = [MHx; MHy; MHz]*phi;
rhs = proxp(E.*X_ss) - cross(mact,X0_s);
err.b1 = relErr(lhs,rhs);

% replace matrix operator with direct evaluation
lhs = cross(Mbody, X0_s);
rhs = proxp(E.*X_ss) - cross(mact,X0_s);
err.b2 = relErr(lhs,rhs);

% vector product with X0_s from the left
lhs = Mbody - dot(Mbody, X0_s)*X0_s;
rhs = proxp(crossProdOp(E.*X_s)*X_ss) - mact + dot(mact,X0_s)*X0_s;
err.b3 = relErr(lhs,rhs);

% sub compat boundary condition to recover 'full' moment boundary condition
lhs = Mbody;
rhs = proxp(crossProdOp(E.*X_s)*X_ss + Gam_s*k3_rep.*X_s) - mact;
err.b4 = relErr(lhs,rhs);

% moment equilibrium equation
Mint_s = crossProdOp(E.*X_s)*X_sss + crossProdOp(E_s.*X_s)*X_ss ...
    + Gam_s*(k3_rep.*X_ss + k3_srep.*X_s);
% Mint   = crossProdOp(E.*X_s)*X_ss + Gams*k3_rep.*X_s;
% Mint_s = D1 * Mint;
XsFint = crossProdOp(X_s)*Fint;
if max(mvec)==0
    lhs = Mint_s;
    rhs = -XsFint;
else
    lhs = Mint_s + XsFint;
    rhs = -mvec;
end
err.b5 = vecnorm(abs(mat(lhs)-mat(rhs)))./mean(vecnorm(mat(rhs)));

% distal integral of equilibrium equation, using Fint_s
Fint_s = -E.*X_ssss + Trep.*X_ss + T_srep.*X_s ...
    -2*E_s.*X_sss - E_ss.*X_ss ...
    +Gam_s*(k3_srep.*crossProdOp(X_s)*X_ss + k3_rep.*crossProdOp(X_s)*X_sss) ...
    +crossProdOp(X_ss)*mvec + crossProdOp(X_s)*mvec_s;
if max(mvec)==0
    lhs = -crossProdOp(E.*X_s)*X_ss - Gam_s*k3_rep.*X_s;
    rhs = computeFlagMomentIntegrals(X,Fint_s);
else
    lhs = -crossProdOp(E.*X_s)*X_ss - Gam_s*k3_rep.*X_s - computeFlagMomentIntegrals(X,Fint_s);
    rhs = -computeActiveMomentDistalIntegrals(d1,d2,X_s,m1,m2,m3,params);
end
err.b6 = relErr(mat(rhs), mat(lhs));
err.b7 = vecnorm(abs(mat(lhs)-mat(rhs)))./mean(vecnorm(mat(rhs)));

% swap Fint_s=f, from force equilibrium equation
if max(mvec)==0
    lhs = -crossProdOp(E.*X_s)*X_ss - Gam_s*k3_rep.*X_s;
    rhs = computeFlagMomentIntegrals(X,f);
else
    lhs = -crossProdOp(E.*X_s)*X_ss - Gam_s*k3_rep.*X_s - computeFlagMomentIntegrals(X,f);
    rhs = -computeActiveMomentDistalIntegrals(d1,d2,X_s,m1,m2,m3,params);
end
err.b8 = vecnorm(abs(mat(lhs)-mat(rhs)))./mean(vecnorm(mat(rhs)));
err.b9 = max(err.b8);

% evaluate above at s=0
err.b10 = err.b8(1);

% substitute to above LHS for head moment
% this is total hydrodynamic moment balance
lhs =  calcBodyMoment(Y,X0,phi,NNB); 
rhs = -calcFlagMoment(X,X0,f);
Im  =  computeFlagMomentIntegrals(X,f);
err.b11 = norm(abs(lhs-rhs))/mean(vecnorm(mat(Im)));

%% (C) COMPATABILITY BOUNDARY CONDITIONS

% moment dot tangent operator
Mdot = buildMBDotOp(X,Y,X0,X_s,params,'debug');
lhs  = Mdot * phi;
rhs  = dot(calcBodyMoment(Y,X0,phi,NNB), proxp(X_s));
err.c1 = abs(lhs-rhs)/(abs(rhs)+1);

% proximal boundary condition
lhs = Mdot * phi;
rhs = Gam_s * k3(1) - dot(mact,proxp(X_s));
err.c2 = abs(lhs-rhs)/(abs(rhs)+1);

% twist moment/curvature condition
% derives from moment equilib equation with Fint subbed in
lhs = k3_s(:);
rhs = calM3 * Gam_s * Gam_d * m3;
err.c3 = relErr(rhs,lhs);

%% (D) MISC CONDITIONS
% any other conditions, derived from model equations, that we may wish to
% check

% moment boundary condition pre crossing/dotting by X_s
lhs = calcBodyMoment(Y,X0,phi,NNB);
rhs = proxp(E.*crossProdOp(X_s)*X_ss + Gam_s*k3_rep.*X_s) - mact;
err.d1 = relErr(rhs,lhs);

end % function
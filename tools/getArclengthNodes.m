function s = GetArclengthNodes(X)

% Computes the arclength values corresponding the vector of coordinates X
% parametrized by arclength.

[x,y,z] = extractComponents(X);
ds = sqrt(diff(x).^2 + diff(y).^2 + diff(z).^2);
s = [0; cumsum(ds(:))];

end % function
function printProgressBar(label,n,N,mode,options)

% Draws a simple progress bar into the command window.

global update_str

if ~isempty(options)
    switch options
        % disable percentage marker:
        case 'noperc'   
            if isempty(update_str)
                update_str = [label ,num2str(n),'/',num2str(N)];
                delete_str = '';
            else
                delete_str = repmat('\b',1,length(update_str)-1);
                update_str = [label ,num2str(n),'/',num2str(N)];
            end
    end
else
    % use numerical counter & percentage marker:
    if isempty(update_str)
        update_str = [label ,num2str(n),'/',num2str(N),' - (',num2str(floor(n/N*100)),'%%)'];
        %perc       = floor(n/N*100);
        %update_str = [label, sprintf('%5.g/%5.g - (%g%%)',n,N,perc)];
        delete_str = '';
    else
        delete_str = repmat('\b',1,length(update_str)-1);
        update_str = [label ,num2str(n),'/',num2str(N),' - (',num2str(floor(n/N*100)),'%%)'];
    end
end

% delete previous string (overwrite mode)?:
switch mode
    case {'','delete'}
        print_str = [delete_str,update_str];
        fprintf(print_str);

        if n == N
            clear update_str
            fprintf('\n')
        end
        
    case 'nodelete'
        fprintf(update_str);
        fprintf('\n')

end

end
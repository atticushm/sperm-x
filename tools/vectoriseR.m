function RV = VectoriseR(R)

M = size(R,1)/3;

Rc{1,1} = R(1:M,1);         Rc{1,2} = R(1:M,2);         Rc{1,3} = R(1:M,3);
Rc{2,1} = R(M+1:2*M,1);     Rc{2,2} = R(M+1:2*M,2);     Rc{2,3} = R(M+1:2*M,3); 
Rc{3,1} = R(2*M+1:3*M,1);   Rc{3,2} = R(2*M+1:3*M,2);   Rc{3,3} = R(2*M+1:3*M,3);

RV = [diag(Rc{1,1}),    diag(Rc{1,2}),  diag(Rc{1,3}); ...
      diag(Rc{2,1}),    diag(Rc{2,2}),  diag(Rc{2,3}); ...
      diag(Rc{3,1}),    diag(Rc{3,2}),  diag(Rc{3,3})];

end
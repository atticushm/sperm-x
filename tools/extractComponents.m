function [x1,x2,x3] = extractComponents(x)

%extractComponents Extract coordinate components from 3D vectors of
%coordinates.
% [x1,x2,x3] = extractComponents(x) returns the Nx1 coordinate components
% of the 3Nx1 vector of coordinates input x.

N = length(x)/3;
x1 = x(1:N);
x2 = x(N+1:2*N);
x3 = x(2*N+1:3*N);

end % function
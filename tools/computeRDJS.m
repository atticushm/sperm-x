function varargout = ComputeRDJS(x, X, r, eps)

%COMPUTER Compute the grand resistance matrix for a rigid body.
% R = ComputeR(x,X,r,eps) returns the grand resistance matrix R for a cell 
% by finding the total force and moment upon the body when U = e_j and 
% Omg = e_j for j=1,2,3 (producing 6 resistance problems which are solved 
% separately).
%
% Each resistance problem is solved using the method of regularized
% stokeslets (Cortez 2005) employing NEAREST (Smith, Gallagher, 2018).
% Inputs x and X are vectors of coordinates defining the cell traction
% discretisation and the cell quadrature/kernel discretisation
% respectively. eps is the regularization parameter.
%
% [RFU,RFO,RMU,RMO] = ComputeR(x,X,r,eps) returns each 3x3 resistance
% matrix as a seperate output.
%
% See also NEARESTNEIGHBOURMATRIX, COMPUTENNFORCE, COMPUTENNMOMENT

%% using DJS code
% via github @ https://github.com/djsmithbham/NearestStokeslets

domain = 'i';
blockSize = 0.2;
[FTr1,MTr1,~,~]=SolveRigidResistance(x,X,r,[1,0,0],[0,0,0],eps,domain,blockSize);
[FTr2,MTr2,~,~]=SolveRigidResistance(x,X,r,[0,1,0],[0,0,0],eps,domain,blockSize);
[FTr3,MTr3,~,~]=SolveRigidResistance(x,X,r,[0,0,1],[0,0,0],eps,domain,blockSize);
[FRo1,MRo1,~,~]=SolveRigidResistance(x,X,r,[0,0,0],[1,0,0],eps,domain,blockSize);
[FRo2,MRo2,~,~]=SolveRigidResistance(x,X,r,[0,0,0],[0,1,0],eps,domain,blockSize);
[FRo3,MRo3,~,~]=SolveRigidResistance(x,X,r,[0,0,0],[0,0,1],eps,domain,blockSize);

RFU = [FTr1,FTr2,FTr3];
RFO = [FRo1,FRo2,FRo3];
RMU = [MTr1,MTr2,MTr3];
RMO = [MRo1,MRo2,MRo3];

% GRM_djs_0 = [FTr1,FTr2,FTr3,FRo1,FRo2,FRo3; MTr1,MTr2,MTr3,MRo1,MRo2,MRo3];
R = [RFU, RFO; RMU, RMO];

%% Outputs:

if nargout == 1
    varargout{1} = R;
elseif nargout == 4
    varargout{1} = RFU;
    varargout{2} = RFO;
    varargout{3} = RMU;
    varargout{4} = RMO;
end

end % function
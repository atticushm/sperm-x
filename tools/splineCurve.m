function [splx,sply,splz] = splineCurve(X,s)

% Splines the curve represented by a vector of coordinates X.

persistent sx sy sz

if isempty(sx) 
    % Extract components:
    [x,y,z] = extractComponents(X(:));

    % Compute splines:
    sx = spapi(6,s,x);
    sy = spapi(6,s,y);
    sz = spapi(6,s,z);
end

splx = sx;
sply = sy;
splz = sz;

end % function
function I = regStokesletNumericalIntegrals(x, X, epsilon, proc)

% computes the regularised stokeslet integral at field points x driven by
% forces at source points X using the repeated midpoint rule, with
% intervals chosen so that the absciccae are the field points.
% x is field points
% X is source points

M = length(x)/3;
N = length(X)/3;
ds = 1/(N-1);

%% numerical integration (midpoint rule)

%{
% number of interior points 
M = N-2;

% matrix representations
X_mat = mat(X);

% 'full' segment length is 
ds = 1/(N-1);

% proximal half segment
Xm0 = 1/2*(X_mat(:,1) + 1/2*(X_mat(:,1)+X_mat(:,2)));
I1  = ds/2 * regStokeslet(x, Xm0, epsilon, proc);
% Xm0 = 1/2 * (X_mat(:,1) + X_mat(:,2)); 
% I1  = ds/4 * (regStokeslet(x, X_mat(:,1), epsilon, proc) + ...
%              regStokeslet(x, Xm0, epsilon, proc));

% interior full segments, centred about nodes
X_mat = mat(X);
X_int = vec(X_mat(:,2:N-1));
Iint  = ds * regStokeslet(x, X_int, epsilon, proc);

% distal half segment
XmN = 1/2*(X_mat(:,N) + 1/2*(X_mat(:,N)+X_mat(:,N-1)));
IN  = ds/2 * regStokeslet(x, XmN, epsilon, proc);
% XmN = 1/2 * (X_mat(:,N-1) + X_mat(:,N));
% IN  = ds/4 * (regStokeslet(x, XmN, epsilon, proc) + ...
%               regStokeslet(x, X_mat(:,N), epsilon, proc));

% build integral operator
I = [I1(:,1), Iint(:,1:M), IN(:,1) , ...
     I1(:,2), Iint(:,M+1:2*M), IN(:,2) , ...
     I1(:,3), Iint(:,2*M+1:3*M), IN(:,3) ];  
%}
 
%% trapezium rule

%{x
% stokeslets and submatrices
S = regStokeslet(x,X,epsilon,proc);
SXX = S(1:M,1:N);
SXY = S(1:M,N+1:2*N);
SXZ = S(1:M,2*N+1:3*N);
SYX = S(M+1:2*M,1:N);
SYY = S(M+1:2*M,N+1:2*N);
SYZ = S(M+1:2*M,2*N+1:3*N);
SZX = S(2*M+1:3*M,1:N);
SZY = S(2*M+1:3*M,N+1:2*N);
SZZ = S(2*M+1:3*M,2*N+1:3*N);

% integrator blocks
id  = repmat([1,2*ones(1,N-2),1],M,1);
IXX = ds/2 * id.*SXX;
IXY = ds/2 * id.*SXY;
IXZ = ds/2 * id.*SXZ;
IYX = ds/2 * id.*SYX;
IYY = ds/2 * id.*SYY;
IYZ = ds/2 * id.*SYZ;
IZX = ds/2 * id.*SZX;
IZY = ds/2 * id.*SZY;
IZZ = ds/2 * id.*SZZ;

% integrator operator
I = [IXX,IXY,IXZ; IYX,IYY,IYZ; IZX,IZY,IZZ];
%}

end % function 
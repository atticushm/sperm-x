function I = computeFlagMomentIntegrals(X, f)

% computes the integrals over [s,L] ds' of the hydrodynamic moment 

% discretisation parameters
N = length(X)/3;
ds = 1/(N-1);

%% hydrodynamic moment intergral

% components of coordinates
[x,y,z] = extractComponents(X);

%%  as matrix operator onto forces using trapezium rule

dx = x'-x;
dy = y'-y;
dz = z'-z;

IXX = zeros(N);
IYY = zeros(N);
IZZ = zeros(N);

id  = triu(2*ones(N),1) + diag(ones(N,1)); id(:,end) = ones(N,1); id(end,:) = zeros(1,N);
IXY = ds/2 * id.*(-dz);
IXZ = ds/2 * id.*( dy);
IYX = ds/2 * id.*( dz);
IYZ = ds/2 * id.*(-dx);
IZX = ds/2 * id.*(-dy);
IZY = ds/2 * id.*( dx);

IOp = [IXX,IXY,IXZ; IYX,IYY,IYZ; IZX,IZY,IZZ];
I  = IOp * f;

%% through summation using trapezium rule

%{
% determine arguments
for n = 1:N
    for m = n:N
        % differences
        dx = x(m)-x(n);
        dy = y(m)-y(n);
        dz = z(m)-z(n);
        
        % arguments
        fm = [f(m); f(m+N); f(m+2*N)];
        argx(n,m) = -dz*fm(2) + dy*fm(3);
        argy(n,m) =  dz*fm(1) - dx*fm(3);
        argz(n,m) = -dy*fm(1) + dx*fm(2);
    end
end

% integrals by trapezium rule
for n = 1:N
    Ix(n) = 0;
    Iy(n) = 0;
    Iz(n) = 0;
    for m = n:N-1       
        Ix(n) = Ix(n) + ds/2*(argx(n,m)+argx(n,m+1));
        Iy(n) = Iy(n) + ds/2*(argy(n,m)+argy(n,m+1));
        Iz(n) = Iz(n) + ds/2*(argz(n,m)+argz(n,m+1));    
    end
end
 
% vector integral
I = [Ix(:); Iy(:); Iz(:)];
%}

%% through summation using GL

%{
[fx,fy,fz] = extractComponents(f);

s = linspace(0,1,N);
for n = 1:N-1
    % interval of integration
    a = s(n);
    b = s(N);
    
    % quad nodes and weights
    M = floor(30*(b-a))+20;
    [sq, w] = lgwt(M,a,b);
    
    % arguments at flag nodes
    dx = x-x(n);
    dy = y-y(n);
    dz = z-z(n);
    argx = -dz.*fy + dy.*fz;
    argy =  dz.*fx - dx.*fz;
    argz = -dy.*fx + dx.*fy;
    
    % spline to get args at quad nodes
    argx = spline(s, argx, sq);
    argy = spline(s, argy, sq);
    argz = spline(s, argz, sq);
    
    % sum to integrate
    Im(:,n) = [sum(w.*argx); sum(w.*argy); sum(w.*argz)];
    
end
Im(:,N) = zeros(3,1);
I = vec(Im);
%}

end % function
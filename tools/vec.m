function X = vec(Y)

% Transforms a 3xN matrix, where each column is a 3D coordinate vector,
% into a 3Nx1 column vector.
%
% See MATRIXTOVECTOR.

Y = Y';
X = Y(:);

end % function
function Y = HeadNodes(X0, Y_ref, hdims, tH)

NY = length(Y_ref)/3;

%% Rotate from reference to current orientation:

% Using head tangent:
tH_0 = [1;0;0];

% Compute rotation matrix:
Reflection = @(u,n) u-2*n*(n'*u)/(n'*n);
S = Reflection(eye(3),tH_0+tH);
R = Reflection(S, tH);

% Identify node to which X0 will connect:
YX0 = [hdims(1);0;0];

% Rotated nodes:
RY = R*VectorToMatrix(Y_ref);
RYX0 = R*YX0;

% Translate to connect X0 to head surface:
dY = RYX0 - X0;
Y = MatrixToVector(RY) - kron(dY,ones(NY,1));

end
function int = setInitialCondition(options,params,varargin)

% Sets initial condition given options in "opt" structure and parameters in
% "param" structure.
%
% See also SETOPTIONS, SETPARAMETERS.

%% parse inputs:

% begin parsing
p = inputParser;
problem = options.problem;

% parse for initial condition type
switch problem
    case {'RelaxingFilament','RelaxingCell'}
        default_itype = 'semicirc';

    otherwise
        default_itype = 'line';
        
end
% exp_itype = {'semicirc','parabola','corkscrew','spiral','line','bend','worm'};
addParameter(p,'IntCondType',default_itype);

% parse for X0
default_X0 = zeros(3,1);
addParameter(p,'X0',default_X0);

% parse for d10,d20,d30
switch problem
    case {'RelaxingFilament','RelaxingCell'}
        default_d10 = [1;0;0];
        default_d20 = [0;0;1];
        default_d30 = [0;-1;0];

    case {'SwimmingFilament','SwimmingCell','CellOverBackstep'}
        switch options.PlaneOfBeat
            case 'xy'
                % for xy planar cell
                default_d10 = [0;1;0];
                default_d20 = [0;0;1];
                default_d30 = [1;0;0];

            case 'xz'
                % for xz planar cell
                default_d10 = [0;0;1];
                default_d20 = [0;1;0];
                default_d30 = [1;0;0];

        end
end
addParameter(p,'d10',default_d10);
addParameter(p,'d20',default_d20);
addParameter(p,'d30',default_d30);

% parse for global rotation (used only with line initial condition)
default_phi = 0;
addParameter(p,'Phi',default_phi);

% build structure
parse(p,varargin{:});
int = p.Results;

%% generate initial conditions

% difference matrices
D = finiteDifferences(linspace(0,1,params.NF),1);
D_s = kron(eye(3),D{1});

% if Phi has been set, orientate to be h0 radial distance from [0;0;0]
H = params.h + params.h0;
Phi = int.Phi;
if int.Phi > 0 && options.UseBlakelets
    int.X0 = [H*cos(Phi);0;H*sin(Phi)];
else
    int.X0 = [0;0;H];
end

% compute in terms of position
switch int.IntCondType
    case 'line'         % <~~~ used for all swimming problems
        % compute coordinates
        X0 = int.X0;
        X = [linspace(0,1,params.NF), ...
             zeros(1,params.NF)     , ...
             zeros(1,params.NF)     ]';
        X  = X + kron(X0,ones(params.NF,1)); ...         
               %+ kron([0;0;params.h+params.h0],ones(params.NF,1));

        % rotate by phi, depending on plane of beat
        switch options.PlaneOfBeat
            case 'xy'
                phi = int.Phi;
                rotMat = [cos(phi),-sin(phi),0; sin(phi),cos(phi),0; 0,0,1];

            case 'xz'
                phi = int.Phi;
                rotMat = [cos(phi),0,-sin(phi); 0,1,0; sin(phi),0,cos(phi)];

        end
        X = rotatePoints(X, rotMat, X0);

        % compute frame
        d3 = normalise(D_s*X);
        d2 = kron(int.d20,ones(params.NF,1));
        d1 = normalise(crossProdOp(d2)*d3);
        k3 = zeros(params.NF,1);

    case 'sinusoidal'
        X = sinusoidalCurve(params.wavk, 0.1, params.NF);
        d3 = normalise(D_s*X);
        d2 = kron([0;0;1],ones(params.NF,1));
        d1 = normalise(crossProdOp(d2)*d3);
        k3 = zeros(params.NF,1);

    case 'parabolic'
        X = parabolicCurve(0.5, params.NF);
        d3 = normalise(D_s*X);
        d2 = kron([0;0;1],ones(params.NF,1));
        d1 = normalise(crossProdOp(d2)*d3);
        k3 = zeros(params.NF,1);

    case 'lowampparab'
        X = parabolicCurve(1e-6, params.NF);
        d3 = normalise(D_s*X);
        d2 = kron([0;0;1],ones(params.NF,1));
        d1 = normalise(crossProdOp(d2)*d3);
        k3 = zeros(params.NF,1);

    case 'semicirc'
        k1 = zeros(params.NF,1);
        k2 = pi*ones(params.NF,1);
        k3 = zeros(params.NF,1);
        [d1,d2,d3] = buildDirectorFrame(k1,k2,k3,int.d10,int.d20,int.d30);
        X = integralAlongCurve(int.X0, d3);

    case 'corkscrew'
        k1 = zeros(params.NF,1);
        k2 = zeros(params.NF,1);
        k3 = 2*pi*ones(params.NF,1);
        [d1,d2,d3] = buildDirectorFrame(k1,k2,k3,int.d10,int.d20,int.d30);
        X = integralAlongCurve(int.X0, d3);

    case 'spiral'
        k1 = cos(4*pi*s);
        k2 = sin(4*pi*s);
        k3 = zeros(params.NF,1);
        [d1,d2,d3] = buildDirectorFrame(k1,k2,k3,int.d10,int.d20,int.d30);
        X = integralAlongCurve(int.X0, d3);

end

% assign to fields
int.k3 = k3;
int.d1 = d1;    int.d10 = proxp(d1);
int.d2 = d2;    int.d20 = proxp(d2);
int.d3 = d3;    int.d30 = proxp(d3);
int.X  = X;

% sort fields
int = orderfields(int);

end % function
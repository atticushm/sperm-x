% RelaxingComparison ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

% Comparison between EIF and Sperm-X.
% Modelling a relaxing passive cell (including head).

% This function calls the EIF code in eif/ (by Hall-McNair) et al to
% compare a relaxing filament (i.e. headless cell).

% clear workspace;
clc; clear all; close all;

%% setup parameters:

% load Sperm-X options for passively relaxing cell:
RelaxingOptions;

% head hydrodynamics model:
head_model = 'sbt';

% size of local region:
q = 0.1;

% number of nodes:
N = 161;

% maximum time:
tmax = 0.06;

% initial condition:
X_int = ParabolicCurve(0.5,N);

% convert position initial condition to X0+theta initial condition:
Y_int = ConvertInitialCondition(X_int, 'endpiece');

%% run sperm-x simulation: LGL parameters:

% local coefficient model:
local_type = 'lgl';

% create Sperm-X model structure:
% note that ell=0, stiff_func is constant, calS=1, calM=0.
spx_model = GroupModelParams(calS, lambda, q, ax, calM, wavk, ell,          ...
                stiff_func, epsilon, dt, tmax, N, H, it_tol, ts_scheme,     ...
                force_planarity, head_model, non_local, frame_model,        ...
                local_type, debug, prof, silent);

% sperm-x simulation:
spx_lgl = runSPX(X_int, spx_model);

%% run sperm-x simulation: RSS parameters:

% local coefficient model:
local_type = 'rss';

% create Sperm-X model structure:
% note that ell=0, stiff_func is constant, calS=1, calM=0.
spx_model = GroupModelParams(calS, lambda, q, ax, calM, wavk, ell,          ...
                stiff_func, epsilon, dt, tmax, N, H, it_tol, ts_scheme,     ...
                force_planarity, head_model, non_local, frame_model,        ...
                local_type, debug, prof, silent);

% sperm-x simulation:
spx_rss = runSPX(X_int, spx_model);

%% run eif/endpiece simulation:

% add endpiece code to path:
addpath(genpath('benchmark/endpiece/'))

% additional EIF parameters:
Q = N-1;
Ed = 1; Ep = 1; sd = 1; L = 1;
X0 = proxp(X_int);
showProg = 1;
th0 = [];
Ht = H; Hq = 3*Ht;
tps = 0:dt:tmax;
nt = length(tps);

% eif simulation:
[Z,Z0,eif_outs,model_eif] = runSingleSim(calS, wavk, calM, stiff_func, [], ell, ...
                                   th0, Q, Hq, Ht, Ed, Ep, sd, L, tmax, showProg,...
                                   X0(1:2), ax, nt, Y_int);

% extract eif solutions:
eif = ExtractDataEIF(Z, model_eif, nt);
                               
% remove endpiece code from path:
rmpath(genpath('benchmark/endpiece/'))

%% plots:

% close other plots:
close all;

% set latex as default text interpreter:
SetLatexInterpreter(true)

% initial and final config plot:
for kk = 1:3
    subplot(2,3,kk); box on; hold on;
    
    % set variables:
    if kk == 1
        id = 1;
        tp = 0; 
    elseif kk == 2
        id = floor(length(tps)/2);
        tp = tps(id);
    elseif kk == 3
        id = length(tps);
        tp = tmax;
    end
    
    % extract components:
    [x1,x2,~] = extractComponents(spx_lgl.X(:,id));
    [y1,y2,~] = extractComponents(spx_rss.X(:,id));
    [z1,z2,~] = extractComponents(eif(id).X);
    
    % plot:
    plot(x1,x2,'-','LineWidth',1.4); 
    plot(y1,y2,'--','LineWidth',1.4);
    plot(z1,z2,'-.','LineWidth',1.4);
    axis equal; axis([-0.6 0.6 -0.2 0.4])
    
    % label axes:
    xlabel('$x$'); ylabel('$y$'); 
    title(sprintf('$t=%.2g$',tp));
    
    % legend:
    legend('Sperm-X (LGL)','Sperm-X (RSS)', 'EIF')
end

% maximum error over time:
subplot(2,3,[4,6]); box on; hold on;
for ii = 1:length(tps)
    Y = eif(ii).X;
    mse_lgl(ii) = mean(abs(spx_lgl.X(:,ii)-Y).^2);
    mse_rss(ii) = mean(abs(spx_rss.X(:,ii)-Y).^2);
end
plot(tps, mse_lgl, 'LineWidth', 1.5);
plot(tps, mse_rss, 'LineWidth', 1.5);
xlabel('$t$'); ylabel('MSE')
legend('SPX(LGL) v. EIF','SPX(RSS) v. EIF', 'location', 'southeast');

% title:
sgtitle('Sperm-X v. EIF; relaxing sperm (head not drawn)')
set(gcf,'Position',[316 423 1461 526]);

% save to pdf:
save_str = [sprintf('./benchmark/figures/RelaxingSpermComparison_q=%g_N=%g_tmax=%.2g',q,N,tmax),'.pdf'];
save2pdf(save_str)


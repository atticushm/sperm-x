% RelaxingComparison ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

% Comparison between EIF and Sperm-X.
% Modelling a relaxing passive filament, for varying local region size q.

% clear workspace;
clc; clear all; close all;

%% run sperm-x simulations for decreasing q:

% load Sperm-X options for passively relaxing cell:
RelaxingComparisonOptions;

% vector of varying q:
q_vals = [0.2; 0.1; 0.05; 0.025; 0.0125];

for ii = 1:length(q_vals)
    printLinebreak('#')
    fprintf('Convergence test: %g/%g\n',ii,length(q_vals));   
    
    % build model structure:
    spx_model = GroupModelParams(Sp, lambda, q_vals(ii), ax, m0, wavk, ell, ...
                stiff_func, epsilon, dt, tmax, N, H, it_tol, ts_scheme,     ...
                force_planarity, head_model, non_local, debug, prof, silent);
    
    % sperm-x simulation:
    spx(ii) = runSPX(X_int, spx_model);
    
    % analogous calS:
    calS(ii) = (1/spx_model.c_perp)^(0.25)*Sp;
    
    printLinebreak('#')
end
                               
%% high-resolution eif simulations:

% setup variables for EIF:
Q = 160;
calM = 0;
Ed = 1; Ep = 1; sd = 1; L = 1;
X0 = proxp(X_int);
showProg = 1;
th0 = [];
Ht = H; Hq = 3*Ht;
t = 0:dt:tmax;
nt = length(t);

% convert position initial condition to X0+theta initial condition:
X_int = InitialCondition('parabolic',Q+1);
Y_int = ConvertInitialCondition(X_int);

for ii = 1:length(calS)
    % eif simulation:
    [Z,Z0,eif_outs,model_eif] = runSingleSim(calS(ii), wavk, calM, stiff_func, [], ell, ...
                                       th0, Q, Hq, Ht, Ed, Ep, sd, L, tmax, showProg,...
                                       X0(1:2), ax, nt, Y_int);

    % extrate coordinate data from angle formulation solutions:
    data_extract = ExtractDataEIF(Z, model_eif, nt);
    eif{ii} = data_extract;
end
    
%% plots:

% close other plots:
close all;

% set latex as default text interpreter:
SetLatexInterpreter(true)

% initial and final config plot:
subplot(1,2,1); box on; hold on;

% extract components and plot:
q_id = 2;
[x1,x2,~] = extractComponents(spx(q_id).X(:,1));
[y1,y2,~] = extractComponents(eif{q_id}(1).X);
plot(x1,x2,'b-','LineWidth',1.2); 
plot(y1,y2,'b--','LineWidth',1.2);

[x1,x2,~] = extractComponents(spx(q_id).X(:,end));
[y1,y2,~] = extractComponents(eif{q_id}(end).X);
plot(x1,x2,'r-','LineWidth',1.2); 
plot(y1,y2,'r--','LineWidth',1.2);

axis equal; axis([-0.6 0.6 -0.2 0.4])

% label axes:
xlabel('$x$'); ylabel('$y$'); 
title(sprintf('$q=%.2g$',q_vals(q_id)));

% legend:
legend('Sperm-X', 'EIF')

% mean squared error at t=tmax:
for ii = 1:length(spx)
    X0_err(nn) = mean((spx(ii).X(:,end)-eif{ii}(end).X).^2);
end
subplot(1,2,2); box on; hold on; 
plot(flipud(q_vals), X0_err);
xlabel('$q$'); ylabel(sprintf('MSE at t=%.2g',tmax))

% plot title and save:
sgtitle('Sperm-X v. EIF convergence; relaxing cell (head ommitted)')
set(gcf,'Position',[874 552 882 284]);
save_str = sprintf('./benchmark/figures/RelaxingConvergenceComparison_q=%.2g_N=%g_tmax=%.2g.pdf',q_vals(q_id),N,tmax);
save2pdf(save_str)

% save workspace:
mat_str = sprintf('./benchmark/workspaces/RelaxingConvergenceComparison_N=%g_tmax=%.2g.mat',N,tmax);
save(mat_str)

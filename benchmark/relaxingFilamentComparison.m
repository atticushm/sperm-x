% RelaxingComparison ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

% Comparison between EIF and SPX.
% Modelling a relaxing passive filament.

% EIF code from the public repo: git@gitlab.com:atticushm/eif.git

% clear workspace
clc; clearvars; close all;

% print banner
printBanner('RELAXING FILAMENT COMPARISON','#');

%% setup sperm-x parameters

% define model options
options = setOptions( ...
    'RelaxingFilament', ...
    'BodyType',     'none', ...
    'NonLocal',     true, ...
    'MechTwist',    false, ...
    'StiffFunc',    'constant' ...
    );

% define modelling parameters
params = setParameters( ...
    options,        ...
    'NF',           201, ...
    'dt',           1e-4, ...
    'tmax',         'auto', ...
    'NumSave',      50 ...
    );             

% initial condition
[int, params] = setInitialCondition(options, params, 'IntCond', 'semicirc');
X_int = int.X;

% completion message:
fprintf('Sperm-X model parameters initialised!\n')
            
%% setup eif parameters: 
% add and remove eif directory to avoid conflicts with functions inside
% Sperm-X code.

% number of segments equal to SPX
Q = params.NF-1;

% setup variables for EIF (ALHM version)
addpath(genpath('benchmark/eif/'))
eif_model = CreateModelStruct(Q,[1,1],[0,0],'relaxing');
rmpath(genpath('benchmark/eif/'))

% convert position initial condition to X0+theta initial condition
Y_int = convertInitialCondition(X_int, 'eif');

% completion message
fprintf('EIF model parameters initialised!\n')

%% run sperm-x simulation: LGL parameters

% local coefficient model
options.LocalType = 'lgl';

% SPX simulation
spx_lgl = runSPX(int, params, options);

%% run sperm-x simulation: RSS parameters

% stokeslet integral coefficient model
options.LocalType = 'rss';

% SPX simulation
spx_rss = runSPX(int, params, options);

%% run eif simulation

% eif simulation
addpath(genpath('benchmark/eif/'))
[sol, tps, crds] = FilamentsRelaxingFunction(eif_model, Y_int, tmax, dt);
rmpath(genpath('benchmark/eif/'))

% make format short again:
format short
                               
%% plots:

% close other plots
close all;

% initial and final config plot
for kk = 1:3
    subplot(2,3,kk); box on; hold on;
    
    % set variables:
    if kk == 1
        id = 1;
        tp = 0; 
    elseif kk == 2
        id = floor(length(tps)/2);
        tp = tps(id);
    elseif kk == 3
        id = length(tps);
        tp = tmax;
    end
    
    % extract components:
    [x1,x2,~] = extractComponents(spx_lgl.X(:,id));
    [y1,y2,~] = extractComponents(spx_rss.X(:,id));
    z1 = crds(id).x;
    z2 = crds(id).y;
    
    % plot:
    plot(x1,x2,'-','LineWidth',1.4); 
    plot(y1,y2,'--','LineWidth',1.4);
    plot(z1,z2,'-.','LineWidth',1.4);
    axis equal; axis([-0.6 0.6 -0.2 0.4])
    
    % label axes:
    xlabel('$x$'); ylabel('$y$'); 
    title(sprintf('$t=%.2g$',tp));
    
    % legend:
    legend('Sperm-X (LGL)','Sperm-X (RSS)', 'EIF')
end

% maximum error over time:
subplot(2,3,[4,6]); box on; hold on;
for ii = 1:length(tps)
    Y = [crds(ii).x(:); crds(ii).y(:); zeros(N,1)];
    mse_lgl(ii) = mean(abs(spx_lgl.X(:,ii)-Y).^2);
    mse_rss(ii) = mean(abs(spx_rss.X(:,ii)-Y).^2);
end
plot(tps, mse_lgl, 'LineWidth', 1.5);
plot(tps, mse_rss, 'LineWidth', 1.5);
xlabel('$t$'); ylabel('MSE')
legend('SPX(LGL) v. EIF','SPX(RSS) v. EIF', 'location', 'southeast');

% title:
sgtitle('Sperm-X v. EIF; relaxing filament (no head)')
set(gcf,'Position',[316 423 1461 526]);

% save to pdf:
save_str = [sprintf('./benchmark/figures/RelaxingFilamentComparison_q=%g_N=%g_tmax=%.2g',q,N,tmax),'.pdf'];
save2pdf(save_str)


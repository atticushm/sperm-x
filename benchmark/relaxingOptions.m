% OPTIONS FOR SPERM-X

% number of flagellar nodes:
N = 101;

% spatial grid spacing:
ds = 1/(N-1);

% head discretisation parameter:
H = 4;

% sperm number:
calS = 1.0;

% dimensionless actuation strength:
calM = 0;

% dimensionless actuation wave number:
wavk = 4*pi;

% dimensionless inactive distal region length (see Neal et al. 2020):
ell = 0.0;

% choose stiffness function: 'constant' or 'varying':
stiff_func = 'constant';

% scaled dimensions of the cell head:
ax = [2;1;1.6]/45;

% choose initial shape: 'parabolic', 'flat', 'sinusoidal', or 'helical':
X_int = InitialCondition('parabolic',N);

% size of inner region:
q = 0.1;

% use LGL 'lgl' or reg stokeslet segments 'rss' local parameters?
local_type = 'rss';

% tension damping parameter:
lambda = 1e3;

% time step size: 'auto' determines dt from 2000 steps per beat.
dt = 1e-5;

% max time:
tmax = 0.06;

% regularisation parameter:
epsilon = 1e-2;

% tolerance for the nonlinear solver:
it_tol = 1e-5;

% choose time-stepping scheme: 'cn' (crank-nicolson) or 'be' (backward-euler):
ts_scheme = 'cn';

% compute nonlocal hydrodyanmics?
non_local = true;

% enforce planarity (to avoid underflow errors)?
force_planarity = true;

% modelling method for the head: 'stokes' (use stokes' law) or 'sbt'
% (slender body theory) or 'none' (no head):
head_model = 'sbt';

% model for the basis frame of the filament:
frame_model = 'director-planar';

% draw plots and save video?
draw_plots = false;

% debugging mode?:
debug = false;

% profiling mode?:
prof = false;

% silent mode? (disables command window outputs):
silent = false;

% SwimmingComparison ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

% Comparison between EIF and Sperm-X.
% Modelling a relaxing passive filament.

% clear workspace;
clc; clear all; close all;

%% setup parameters:

% load Sperm-X options for passively relaxing cell:
RelaxingComparisonOptions;

% create Sperm-X model structure:
% note that ell=0, stiff_func is constant, Sp=1, m0=0.
spx_model = GroupModelParams(Sp, c_perp, gamma, lambda, q, ax, m0, wavk, ell,   ...
                stiff_func, epsilon, dt, tmax, N, H, it_tol, ts_scheme,         ...
                force_planarity, head_model, non_local, debug, prof);
            
% setup variables for EIF:
mu = 1; Q = spx_model.discr.Q;
calS = (mu/spx_model.c_perp)^(0.25)*Sp;
calM = 0;
Ed = 1; Ep = 1; sd = 1; L = 1;
X0 = proxp(X_int);
showProg = 1;
th0 = [];
Ht = H; Hq = 3*Ht;
nt = spx_model.discr.num_tsteps;

% convert position initial condition to X0+theta initial condition:
Y_int = ConvertInitialCondition(X_int);

%% run simulations:

% sperm-x simulation:
spx = runSPX(X_int, spx_model);

% eif simulation:
[Z,Z0,eif_outs,model_eif] = runSingleSim(calS, wavk, calM, stiff_func, [], ell, ...
                                   th0, Q, Hq, Ht, Ed, Ep, sd, L, tmax, showProg,...
                                   X0(1:2), ax, nt, Y_int);
                               
%% extract solutions:

% extrate coordinate data from angle formulation solutions:
t = linspace(0,tmax,nt);
fprintf('Extracting data...')
for nn = 1:nt
    
    % extract data:
    [head,flag,~] = GetProblemData(Z(:,nn),model_eif);
    
    % flagellar nodes:
    x1 = flag.x(:);
    x2 = flag.y(:);
    x3 = flag.z(:);
    eif(nn).X = [x1; x2; x3];
    eif(nn).x1 = x1;
    eif(nn).x2 = x2;
    eif(nn).x3 = x3;
    
    % head nodes:
    y1 = head.xTrac(:);
    y2 = head.yTrac(:);
    y3 = head.zTrac(:);
    eif(nn).Y = [y1; y2; y3];
    eif(nn).y1 = y1;
    eif(nn).y2 = y2;
    eif(nn).y3 = y3;
    
    % forces:
    %eif(nn).t = t(nn);
    %[eif(nn).f,eif(nn).phi,eif(nn).FH,eif(nn).MH] = CalculateEIFForces(eif(nn).t,Z(:,nn),model);
    %[eif(nn).f1,eif(nn).f2,eif(nn).f3] = extractComponents(eif(nn).f);
    %[eif(nn).phi1,eif(nn).phi2,eif(nn).phi3] = extractComponents(eif(nn).phi);
    
end
eif = orderfields(eif);
fprintf(' complete!\n')

%% plots:

% close other plots:
close all;

% set latex as default text interpreter:
set(0,'defaultTextInterpreter','latex');
set(0,'defaultAxesTickLabelInterpreter','latex');
set(0,'defaultLegendInterpreter','latex');

% initial and final config plot:
for kk = 1:2
    subplot(1,2,kk); box on; hold on;
    
    % set variables:
    if kk == 1
        id = 1;
        tp = 0; 
    else
        id = nt;
        tp = tmax;
    end
    
    % extract components and plot:
    [x1,x2,~] = extractComponents(spx.X(:,id));
    [y1,y2,~] = extractComponents(eif(id).X);
    plot(x1,x2,'-','LineWidth',1.4); 
    plot(y1,y2,'--','LineWidth',1.4);
    axis equal; axis([-0.6 0.6 -0.2 0.4])
    
    % label axes:
    xlabel('$x$'); ylabel('$y$'); 
    title(sprintf('$t=%.2g$',tp));
    
    % legend:
    legend('Sperm-X', 'EIF')
end
sgtitle('Sperm-X v. EIF; relaxing cell (head ommitted)')
set(gcf,'Position',[785 667 992 282]);
save2pdf(sprintf('./benchmark/RelaxingComparison_q=%g_N=%g_tmax=%.2g.pdf',q,N,tmax))


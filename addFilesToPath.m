function addFilesToPath

% adds required files and folders to path for a successful run of Sperm-X.

% add all files:
addpath(genpath('./'))

% remove depreciated files:
rmpath(genpath('./depreciated'))

% remove git folder
rmpath(genpath('./.git'))

% remove eif and endpiece code folders:
rmpath(genpath('./benchmark/eif'))
rmpath(genpath('./convergence/eif_ext'))
rmpath(genpath('./convergence/eif'))
rmpath(genpath('./endpiece_code'))

% remove mouse folders (might need changed if mouse heads dont build)
rmpath(genpath('./mouse/lib/repos'))

% remove "cell over boundary folder"
rmpath(genpath('./cell_over_boundary'))

end % function
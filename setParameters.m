function params = setParameters(options,varargin)

% Sets modelling parameters given options in "opt" struct, given by
% setOptions.m

%% parse inputs:

% begin parsing
p = inputParser;
problem = options.problem;

% parse for spatial discretisation
default_NF = 151;
addParameter(p,'NF',default_NF,@isnumeric);

% parse for head discretisation
switch options.Species
    case 'human'
        default_Ht = 4;
        default_Hq = 8;
    case 'mouse'
        default_Ht = 50;
        default_Hq = 150;
end
addParameter(p, 'Ht', default_Ht, @isnumeric);
addParameter(p, 'Hq', default_Hq, @isnumeric);

% parse for temporal discretisation
check_dt = @(x) isnumeric(x) || strcmp(x,'auto');
switch problem
    case {'RelaxingFilament','RelaxingCell'}
        default_dt = 1e-4;
    otherwise
        default_dt = 'auto';
end
addParameter(p,'dt',default_dt,check_dt);

% parse for number of beats
switch problem
    case {'RelaxingFilament','RelaxingCell'}
        default_numbeats = 1;
    case {'SwimmingFilament','SwimmingCell'}
        default_numbeats = 4;
    case {'CellOverBackstep','CellOverWall'}
        default_numbeats = 20;
end
addParameter(p,'NumBeats',default_numbeats,@isnumeric);

% parse for tmax
switch problem
    case {'RelaxingFilament','RelaxingCell'}
        default_tmax = 0.06;
    otherwise
        default_tmax = 'auto';
end
addParameter(p,'tmax',default_tmax);

% parse for iterative solver tolerance
default_ittol = 1e-4;
addParameter(p,'ittol',default_ittol,@isnumeric);

% parse for damping parameter
default_lambda = 1e2;
addParameter(p,'lambda',default_lambda,@isnumeric);

% parse for wave numbers 
default_k = 4*pi;
addParameter(p,'k_bend',default_k,@isnumeric);
addParameter(p,'k_twist',default_k,@isnumeric);

% parse for time scales
default_om = 1;
addParameter(p,'om_bend',default_om,@isnumeric);
addParameter(p,'om_twist',default_om,@isnumeric);

% parse for arclength stiffness threshold
default_scrit = 0.65;
addParameter(p,'s_crit',default_scrit,@isnumeric);

% parse for half local region size (as a multiple of segment length)
default_q = 0.1;
addParameter(p,'q',default_q);

% parse for inactive end piece length
switch problem
    case {'RelaxingFilament','RelaxingCell'}
        default_ell = 0;
    otherwise
        default_ell = 0.03;
end
addParameter(p,'ell',default_ell);

% parse for dimensionless parameters
switch problem
    case {'RelaxingFilament','RelaxingCell'}
        default_calS  = 1;
        default_calM1 = 0;
        default_calM2 = 0;
        default_calM3 = 0;
        default_Gam_d = 0;
        default_Gam_s = 1;
    otherwise
        default_calS   = 14;
        default_calM1  = 0;
        default_calM2  = 0.015;
        default_calM3  = 0;
        default_Gam_d = 0;
        default_Gam_s = 1;
end
addParameter(p, 'calS',  default_calS,  @isnumeric);
addParameter(p, 'calM1', default_calM1, @isnumeric);
addParameter(p, 'calM2', default_calM2, @isnumeric);
addParameter(p, 'calM3', default_calM3, @isnumeric);
addParameter(p, 'Gam_d', default_Gam_d, @isnumeric);
addParameter(p, 'Gam_s', default_Gam_s, @isnumeric);

% parse for backstep height and position of cell above plateau
switch problem 
    case 'CellOverBackstep'
        default_h0 = 0.2;       % height of cell over boundary
        default_h  = 0.2;       % height of 'cliff'
    case 'CellOverWall'
        default_h0 = 0.2;
        default_h  = 0;
    otherwise
        default_h0 = 0.5;
        default_h  = 0;
end
addParameter(p,'h0',default_h0);
addParameter(p,'h', default_h);

% parse for regularisation parameter
default_epsilon = 1e-2;
addParameter(p,'epsilon',default_epsilon);

% parse for custom head axes (used when options.BodyType='custom')
default_ax = [];
addParameter(p,'head_ax',default_ax);

% parse for number of slices per beat
default_numslices = 20;
addParameter(p,'NumSlices',default_numslices)

% parse for number of timesteps to save
switch problem
    case {'CellOverBackstep','CellOverWall'}
        default_NumSave = 1e4;
    otherwise
        default_NumSave = 1;
end
addParameter(p,'NumSave',default_NumSave);

% parse for number of warm up beats
switch problem
    case {'RelaxingFilament','RelaxingCell'}
        default_WarmUp = 0;
    otherwise
        default_WarmUp = 0.5;
end
addParameter(p,'WarmUp',default_WarmUp);

% parse inputs
parse(p,varargin{:});
params = p.Results;

%% determine parameters

% fourth power of swimming parameter
params.calS4 = params.calS^4;

% determine local region size
% must be a multiple of ds -- check and round if otherwise
params.ds = 1/(params.NF-1);
params.Q = params.q/params.ds;
if params.Q ~= floor(params.Q)
    params.Q = round(params.Q);
    params.q = params.Q*params.ds;
end

% if tmax=auto, set tmax from num_beats
if strcmpi(problem,'RelaxingCell')
    params.T    = 0;
elseif strcmpi(problem,'RelaxingFilament')
    params.tmax = 0.06;
    params.T    = 0;
else
    params.T    = 2*pi;
end
if strcmpi(params.tmax,'auto')
    params.tmax = params.NumBeats*params.T;
end

% if dt=auto, determine dt
if strcmpi(params.dt,'auto')
    if options.ForcePlanar
        tsteps_per_beat = 350;                         % time steps per beat.
    else
        tsteps_per_beat = 3000;
    end
    params.num_tstep = round(params.NumBeats*tsteps_per_beat);
    params.dt = params.tmax/params.num_tstep;          % automatic time step size.
else
    params.num_tstep = length(0:params.dt:params.tmax);% number of tsteps if dt prescribed.
end

% specify some convenient intervals for NumSave
if      strcmpi(params.NumSave,'all')
    params.NumSave = params.num_tstep;
elseif  strcmpi(params.NumSave,'half')
    params.NumSave = floor(params.num_tstep/2);
elseif  strcmpi(params.NumSave,'quart')
    params.NumSave = floor(params.num_tstep/4);
elseif  strcmpi(params.NumSave,'auto')
    params.NumSave = floor(params.NumBeats * params.NumSlices);
end

% compute resistance parameters
b = 1e-2;  
[c_perp, c_para, gamma] = calcResistanceCoefficients(1,params.q,b,options.LocalType);
params.c_perp = c_perp; 
params.c_para = c_para;
params.gamma  = gamma;

% generate head discretisations for cell problems
switch problem
    case {'RelaxingCell','SwimmingCell','CellOverBackstep'}
        
        % no head
        if strcmpi(options.BodyType,'none')
            y_ref = [];
            Y_ref = [];
            X0_ref = [0;0;0];
            ax = [];
            
        % custom human head dimensions
        elseif strcmpi(options.BodyType,'custom')
            [y_ref, Y_ref, X0_ref, ax] = generateBodyNodes(params.Ht, params.Hq,    ...
                options.Species, options.BodyType, params.head_ax);
            
        % automatic head dimensions from id string
        else
            [y_ref, Y_ref, X0_ref, ax] = generateBodyNodes(params.Ht, params.Hq,    ...
                options.Species, options.BodyType);
        end
        
        NBt   = length(y_ref)/3;
        NBq   = length(Y_ref)/3;
        NNB   = nearestNeighbourMatrix(Y_ref, y_ref,options.ProcType);
    otherwise
        
        NBt = 0;
        NBq = 0;
        X0_ref = [0;0;0];
        y_ref = []; 
        Y_ref = []; 
        NNB = [];
end
params.NBt     = NBt;           % # of body traction nodes
params.NBq     = NBq;           % # of body quadrature nodes
params.y_ref   = y_ref;         % body nodes in body coordinates
params.Y_ref   = Y_ref;         % as above, but quadrature discretisation
params.X0_ref  = X0_ref;        % body/flagellum joint in body coordinates
params.NNB     = NNB;           % nearest neighour matrix
params.head_ax = ax;            % body axes

% generate boundary surface for backstep problems
if options.Backstep
    dx_w_trac = 0.1;           % traction discretisation parameter
    dx_w_quad = 0.05;          % quadrature discretisation parameter
    
    L  = 0.5;
    xw = constructZBackstep(params.h, L, options, dx_w_trac); 
    Xw = constructZBackstep(params.h, L, options, dx_w_quad); 
    
    NW = length(xw)/3;          % # of wall traction nodes
    wepsilon = 0.05;            % epsilon for wall nodes
else
    xw = [];
    Xw = [];
    NW = 0;
    wepsilon = 0.01;
end
params.xw = xw;
params.Xw = Xw;
params.NW = NW;
params.NNW = nearestNeighbourMatrix(Xw,xw,options.ProcType);
params.wepsilon = wepsilon;

% determine interval at which to save solutions to output
params.SaveInt = floor(params.num_tstep/params.NumSave);

% total number of unknowns
params.num_unknowns = 7*params.NF + 3*params.NBt + 3*params.NW + 6;

% generate filename
filename = sprintf([...
    options.BodyType, ...
    '_S=%.3g', ...
    '_M1=%.3g', ...
    '_M2=%.3g', ...
    '_M3=%.3g', ...
    '_Gams=%.3g', ...
    '_Gamd=%.3g', ...
    '_NF=%g', ...
    '_dt=%.e', ...
    '_tmax=%.2g', ...
    '_ell=%g' ...
    ], params.calS, params.calM1, params.calM2, params.calM3, ...
    params.Gam_s, params.Gam_d, params.NF, params.dt, params.tmax, params.ell);
                
filename = [datestr(now,'dd-mm_HH_MM'),'__',filename];
params.filename = filename;

%% sort output:

params = orderfields(params);

end % function
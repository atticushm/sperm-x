function spxHelicalParameterSweep()

% for a selection of calS values and calM2 values, choose calM1<=calM2 to
% generate a number of quasi-helical through to helical swimmers, and save
% data

% check in correct directory (bluebear call will start in subdir, so we 
% need to move up)
dir = pwd;
if strcmpi(dir(end-10:end),'spx_helical')
    cd ..
end
addFilesToPath;
printLinebreak

% add code to path
addFilesToPath()

% folder for data
if ~exist('./spx_helical/data','dir')
    mkdir('./spx_helical/data')
end

% define values to vary across simulations
calS_vals  = [9,12,15,18];
calM2_vals = 0.01:0.0015:0.017;
actmod_vals = [1,2];

% choose calM1 to generate quasi-helical beats, calM1<=calM2 s.t. calM1 are
% fractions of calM2
num_calM1 = length(calM2_vals);
for i = 1:num_calM1
    calM1_vals(i,:) = linspace(0,calM2_vals(i),num_calM1);
end

% build vars in blocks, so that correct calM1 values pair to calM2 values
vars = [];
num_calM2 = length(calM2_vals);
for i = 1:num_calM2
    tmp  = combvec(calS_vals,calM2_vals(i),calM1_vals(i,:),actmod_vals);
    vars = [vars,tmp];    
end
num_sims = size(vars,2);

% run simulations
parfor k = 1:num_sims
    
    % extract variables
    calSk  = vars(1,k);
    calM2k = vars(2,k);
    calM1k = vars(3,k);
    if vars(4,k)==1
        actk = 'trig';
    elseif vars(4,k)==2
        actk = 'smthspline';
    end
    
    % set options
    options = setOptions('SwimmingCell', 'Species','human', 'BodyType','sunanda', ...
        'NonLocal',true, 'MechTwist',false, 'StiffModel','varying', 'SilentMode',true, ...
        'PlaneOfBeat','xz', 'SaveVideo',false, 'ActModel',actk, 'EndpieceModel','heaviside');

    % set parameters
    params = setParameters(options, 'ittol',1e-4, 'NF',251, 'dt',5e-2, 'q',0.1, ...
        'calS',calSk, 'calM1',calM1k, 'calM2',calM2k, 'calM3',0, 'Gam_s',1, ...
        'Gam_d',10^4, 'wavk',4*pi, 'ell',0.05, 'epsilon',0.01, 'NumBeats',10, ...
        'WarmUp',1, 'tmax','auto', 'NumSave','auto');

    % set initial condition
    int = setInitialCondition(options, params, 'IntCond','line', 'X0',[0;0;0]);

    try
        % run simulation
        outs = runSPX(int, params, options);
    catch
        outs = []; % in case of error due to e.g. nonconvergence
    end

    % save data
    save_str = sprintf('./spx_helical/data/paramSweep_%03g_of_%03g.mat',k,num_sims);
    parsave(save_str, outs, {calSk,calM1k,calM2k,actk});
    
end % parfor


end % function
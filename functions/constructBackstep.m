function X = ConstructBackstep(h, L, options, varargin)

% constructs a rigid surface, initially distance h0 below a swimmer. 
% after distance dx, the surface drops by height dh in the z-direction.
% the total wall is width W and length L

% the backstep here is generated assuming the swimmer travels towards the
% negative x axis

% backstep is such that the lower level is the plane z=0

% discretisation parameters
if isempty(varargin)
    x_step = 0.05;
    y_step = 0.05;
    z_step = 0.05;
else
    x_step = varargin{1};
    y_step = x_step;
    z_step = x_step;
end

% 'heights' of each level
y_0 = 0;
y_1 = h*L;

% start of bottom level
x_0 = -L-3;

% cliff x position
x_1 = -L/4;

% plateau end position
x_2 =  L+0.2;

% z coordinates
z_min = -L/2;
z_max =  L/2;
dz = z_min : z_step : z_max;

% 'base' nodes in the plane y=0
if options.UseBlakelets
    % not needed due to reg blakelet image system, but generate a couple of
    % nodes for plotting purposes
    [x_base,y_base,z_base] = meshgrid([x_0,x_2],0,[dz(1),dz(end)]);
%     x_base=[]; y_base=[]; z_base=[];
else
    dx_base = x_0 : x_step : x_1;
    [x_base,y_base,z_base] = meshgrid(dx_base,y_0,dz);
end

% 'cliff' nodes in the plane x=x_1
if options.UseBlakelets && (h==0)
    x_cliff=[]; y_cliff=[]; z_cliff=[];
else
    dy_cliff = y_0 : y_step : y_1;
    [x_cliff,y_cliff,z_cliff] = meshgrid(x_1,dy_cliff,dz);
end

% 'plateau' nodes in the plane y=h*L=y_1
if options.UseBlakelets && (h==0)
    x_plat=[]; y_plat=[]; z_plat=[];
else
    dx_plat = x_1 : x_step : x_2;
    [x_plat,y_plat,z_plat] = meshgrid(dx_plat,y_1,dz);
end

% collate points and remove duplicates
X_mat = [x_plat(:)', x_cliff(:)', x_base(:)'; ...
         y_plat(:)', y_cliff(:)', y_base(:)'; ...
         z_plat(:)', z_cliff(:)', z_base(:)'];

X_unq = unique(X_mat','rows')';
X     = MatrixToVector(X_unq);

end % function
function [Mx,My,Mz] = buildMFCrossOp(X, Y, X0, d3, params)

% constructs the operator in each x,y,z direction for inclusion in the
% proximal EHD boundary conditions, computing MB x X0_s by multiplying 
% onto the unknown forces phi.

% there's probably an easier way of doing this - the method below derives
% from full expansion of M x d3 in components using the Levi-Civita symbol.

NN  = params.NNB;       % nearest neighbour matrix between head discretisations
NF  = params.NF;        % # of flagellum nodes
NBt = params.NBt;       % # of head traction nodes
NBq = params.NBq;       % # of head quadrature nodes
NW  = params.NW;        % # of wall nodes

X0_s          = proxp(d3);
[t1,t2,t3]    = extractComponents(X0_s);

% half segments at each end have midpoints not on a node
Xm = mat(X);
X1 = 1/2*(Xm(:,1)+1/2*(Xm(:,1)+Xm(:,2)));
XN = 1/2*(Xm(:,end)+1/2*(Xm(:,end-1)+Xm(:,end)));
X  = vec([X1, Xm(:,2:end-1), XN]);

% calculate differences
[X1,X2,X3] = extractComponents(X);
r1 = X1'-X0(1);
r2 = X2'-X0(2);
r3 = X3'-X0(3);

% weights
ds  = 1/(NF-1);
wgt = diag([ds/2, ds*ones(1,NF-2), ds/2]);

% compute using loop - probably a way to vectorise...
for m = 1:NF
    wgtm = wgt(:,m);
    
    % ~~~ x component
    % -e_1jk*t_j*M_k = -e_1jk*t_j*(int{e_klm*dX_l*f_m}ds)
    M11(1,m) =  t2*r2*wgtm + t3*r3*wgtm;     % mults onto f_x
    M12(1,m) = -t2*r1*wgtm;                 % mults onto f_y
    M13(1,m) = -t3*r1*wgtm;                 % mults onto f_z, etc 
    
    % ~~~ y component
    % -e_2jk*t_j*M_k = -e_2jk*t_j*(int{e_klm*dX_l*f_m}ds)
    M21(1,m) = -t1*r2*wgtm;
    M22(1,m) =  t1*r1*wgtm + t3*r3*wgtm;
    M23(1,m) = -t3*r2*wgtm;
    
    % ~~~ z component
    % -e_3jk*t_j*M_k = -e_3jk*t_j*(int{e_klm*dX_l*f_m}ds)
    M31(1,m) = -t1*r3*wgtm;
    M32(1,m) = -t2*r3*wgtm;
    M33(1,m) =  t1*r1*wgtm + t2*r2*wgtm;
end

% zero vectors
zeNB = zeros(1,NBt);
zeNW = zeros(1,NW);

% build operator onto forces
Mx = [M11,zeNB,zeNW, M12,zeNB,zeNW, M13,zeNB,zeNW];
My = [M21,zeNB,zeNW, M22,zeNB,zeNW, M23,zeNB,zeNW];
Mz = [M31,zeNB,zeNW, M32,zeNB,zeNW, M33,zeNB,zeNW];

% Mx = [M11, M12, M13];
% My = [M21, M22, M23];
% Mz = [M31, M32, M33];

end % function
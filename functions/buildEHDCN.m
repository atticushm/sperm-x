function [A, b] = buildEHDCN(X, Y, T, V, d1, d2, IXX, IXY, IXW, k3, d10,    ...
    d20, Xn, Tn, Vn, d1n, d2n, k3n, t, params, options)
           
% encode the EHD/nonlocal sperm equation using Crank-Nicolson time
% stepping.

% discretisation parameters
NF = params.NF;
NB = params.NBt;
NW = params.NW;
dt = params.dt;

% modelling parameters
calS4 = params.calS4;
cperp = params.c_perp;
gamma = params.gamma;
Gamma = params.Gamma;

% bending stiffness values
[E,E_s,E_ss] = computeStiffness(d1, d2, options);

% arclength discretisation
s = linspace(0,1,NF);

% generate finite difference schemes
D = finiteDifferences(s, 1:4);

% build finite difference operators
D1 = kron(eye(3),D{1});  
D2 = kron(eye(3),D{2}); 
D3 = kron(eye(3),D{3});
D4 = kron(eye(3),D{4}); 

% approximate derivatives to curve
X_s = normalise(D1*X); 
X_ss = D2*X;

% extract components of derivatives
[x1_s,x2_s,x3_s] = extractComponents(X_s);
[x1_ss,x2_ss,x3_ss] = extractComponents(X_ss);

% compute actuation values
[mvec, mvec_s, mint] = computeActuationVector(t, d1, d2, X_s, params, options);

% derivatives of twist
k3_s = D{1}*k3;

%% construct matrix

% coefficient from finite difference scheme
if (t-dt < dt)
    coeff = dt/cperp;
else
    coeff = dt/2/cperp;
end

% operators onto position
XX1 = calS4 * eye(3*NF);
XX2 = coeff * (E_ss.*D2);
XX3 = coeff * (2*E_s.*D3);
XX4 = coeff * (E.*D4);
XX5 = coeff * (2*(gamma-1)*tensorProd(X_s)*(E_s.*D3));
XX6 = coeff * ((gamma-1)*tensorProd(X_s)*(E.*D4));
XX  = XX1+XX2+XX3+XX4+XX5+XX6;

% twist terms onto position 
zeN   = zeros(NF);
XsDss = [zeN, -x3_s(:).*D{2}, x2_s(:).*D{2}; ...
         x3_s(:).*D{2}, zeN, -x1_s(:).*D{2}; ...
         -x2_s(:).*D{2}                                             , x1_s(:).*D{2}, zeN];

XsDsss = [zeN, -x3_s(:).*D{3}, x2_s(:).*D{3}; ...
          x3_s(:).*D{3}, zeN, -x1_s(:).*D{3}; ...
          -x2_s(:).*D{3}, x1_s(:).*D{3}, zeN];

TW1 = -coeff * Gamma .* repmat(k3_s,3,1) .* XsDss;
TW2 = -coeff * Gamma .* repmat(k3,3,1) .* XsDsss;
XX  = XX+TW1+TW2;

% operators onto forces (to compute V)
if options.NonLocal
    XF = combineMatrices(-coeff*IXX,-coeff*IXY,-coeff*IXW);
else
    XF = zeros(3*NF, 3*NF+3*NB+3*NW);
end

% operators onto tension
XT1 = -coeff*[diag(x1_ss);diag(x2_ss);diag(x3_ss)];
XT2 = -coeff*gamma*[x1_s(:).*D{1}; x2_s(:).*D{1}; x3_s(:).*D{1}];
XT  = XT1+XT2;

% build operator
A = [XX, XT, XF, zeros(3*NF,6)];

%% construct right hand side

% from crank nicolson difference
b1 = calS4*Xn;

% active bending terms
b2 = coeff * crossProdOp(X_ss)* mvec;
b3 = coeff * crossProdOp(X_s)* mvec_s;
b4 = coeff * (gamma-1)*tensorProd(X_s)*(crossProdOp(X_ss)*mvec);

% combine 
b = b1+b2+b3+b4;

% values from previous time step
if (t-dt > 1e-8)
    b5 = dt/2 * calcExplicitContrib(Xn, Tn, Vn, k3n, d1n, d2n, t-dt, params, options);
    b  = b + b5;
end

%% apply boundary conditions

% matrix representation for easier boundary condtion implementation
b_mat = mat(b);

% zeros vectors 
zeNF  = zeros(1,NF); 
ze3   = zeros(1,3);  
ze3NF = zeros(1,3*NF); 
ze3NB = zeros(1,3*NB); 
ze3NW = zeros(1,3*NW);

% stiffness at X0
E0   = proxp(E);
E0_s = proxp(E_s);

% stiffness at XL
EL   = distp(E);
EL_s = distp(E_s);

% values at s=0
X0 = proxp(X);
X0_s = proxp(X_s);

% values at s=L
XL_s = distp(X_s);

%% proximal force bc

% bending force terms
d3x = E0(1)*D3(1,1:NF);
d3y = E0(2)*D3(1,1:NF);
d3z = E0(3)*D3(1,1:NF);

% extra bend terms from varying stiffness
d2x = E0_s(1)*D2(1,1:NF);
d2y = E0_s(2)*D2(1,1:NF);
d2z = E0_s(3)*D2(1,1:NF);

% twist force terms
c2x = Gamma * k3(1) * X0_s(1)*D2(1,1:NF);
c2y = Gamma * k3(1) * X0_s(2)*D2(1,1:NF);
c2z = Gamma * k3(1) * X0_s(3)*D2(1,1:NF);

% force from head
[FHx,FHy,FHz] = buildFBOp(params);

% update rows
A(1,:)      = [-d3x-d2x, -c2z, c2y, zeNF, -FHx, ze3, ze3]; 
A(NF+1,:)   = [c2z, -d3y-d2y, -c2x, zeNF, -FHy, ze3, ze3];
A(2*NF+1,:) = [-c2y, c2x, -d3z-d2z, zeNF, -FHz, ze3, ze3];
b_mat(:,1)  = -T(1)*X0_s - proxp(crossProdOp(X_s)*mvec);

%% distal force bc

% bending force terms
d3x = EL(1)*D3(NF,1:NF);
d3y = EL(2)*D3(NF,1:NF);
d3z = EL(3)*D3(NF,1:NF);

% extra bend terms from varying stiffness
d2x = EL_s(1)*D2(NF,1:NF);
d2y = EL_s(2)*D2(NF,1:NF);
d2z = EL_s(3)*D2(NF,1:NF);

% twist force terms
c2x = -Gamma * k3(end) * XL_s(1)*D2(NF,1:NF);
c2y = -Gamma * k3(end) * XL_s(2)*D2(NF,1:NF);
c2z = -Gamma * k3(end) * XL_s(3)*D2(NF,1:NF);

% update rows
A(NF,:)     = [d3x+d2x, -c2z, c2y, zeNF, ze3NF, ze3NB, ze3NW, ze3, ze3]; 
A(2*NF,:)   = [c2z, d3y+d2y, -c2x, zeNF, ze3NF, ze3NB, ze3NW, ze3, ze3];
A(3*NF,:)   = [-c2y, c2x, d3z+d2z, zeNF, ze3NF, ze3NB, ze3NW, ze3, ze3];
b_mat(:,NF) = distp(crossProdOp(X_s)*mvec);

%% promixal moment bc

% bending moment terms
d2x = E0(1)*D2(1,1:NF);
d2y = E0(2)*D2(1,1:NF);
d2z = E0(3)*D2(1,1:NF);

% moment from head
[MHx,MHy,MHz] = buildMBCrossOp(X, Y, X0, X_s, params);

% update rows
A(2,:)      = [-d2x, zeNF, zeNF, zeNF, MHx, ze3, ze3]; 
A(NF+2,:)   = [zeNF, -d2y, zeNF, zeNF, MHy, ze3, ze3];
A(2*NF+2,:) = [zeNF, zeNF, -d2z, zeNF, MHz, ze3, ze3];
b_mat(:,2)  = -cross(mint, X0_s);

%% distal moment bc:

% bending moment terms
d2x = EL(1)*D2(NF,1:NF);
d2y = EL(2)*D2(NF,1:NF);
d2z = EL(3)*D2(NF,1:NF);

% update rows
A(NF-1,:)     = [d2x, zeNF, zeNF, zeNF, ze3NF, ze3NB, ze3NW, ze3, ze3];
A(2*NF-1,:)   = [zeNF, d2y, zeNF, zeNF, ze3NF, ze3NB, ze3NW, ze3, ze3];
A(3*NF-1,:)   = [zeNF, zeNF, d2z, zeNF, ze3NF, ze3NB, ze3NW, ze3, ze3];
b_mat(:,NF-1) = zeros(3,1);

%% output

% augment system if mechanical twist enabled
if options.MechTwist
    A = [A, zeros(3*NF,NF)];
end
b = vec(b_mat);

end % function
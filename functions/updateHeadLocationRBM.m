function [yi, Yi] = updateHeadLocationRBM(X, Xn, Om, y, Y, yn, Yn, params)

% update head positions by rigid body motion 

dt  = params.dt;
NBt = params.NBt;
NBq = params.NBq;

X0  = proxp(X);
X0n = proxp(Xn);
UB  = (X0-X0n)/dt;

% translational component
dy = y-kron(X0,ones(NBt,1));
dY = Y-kron(X0,ones(NBq,1));

% rotational component
dy_mat = mat(dy);
dY_mat = mat(dY);

crOp = [0,-Om(3),Om(2); Om(3),0,-Om(1);  -Om(2),Om(1),0];
y_pr = crOp * dy_mat;
Y_pr = crOp * dY_mat;

yi = yn + dt*kron(UB,ones(NBt,1)) + dt*vec(y_pr);
Yi = Yn + dt*kron(UB,ones(NBq,1)) + dt*vec(Y_pr);

end
function [A, b] = buildKinematics(X, Om, Xn, k3, d10, d20, d10n, d20n, t, params, options)

% modelling parameters
calT = params.calT;
calT2 = calT^2;
Gammad = params.Gammad;
calM3 = params.calM3;

% discretisation parameters
dt = params.dt;
NF = params.NF;
NB = params.NBt;
NW = params.NW;

% arclength parametrisation
s = linspace(0,1,NF);

% finite difference operators
D = finiteDifferences(s,1:2);

% curve derivatives (equivalent to d3)
d3  = normalise(kron(eye(3),D{1}) * X);
d3n = normalise(kron(eye(3),D{1}) * Xn);

d30  = proxp(d3);
d30n = proxp(d3n);

% compute twist actuation values
[~ ,~, m3, ~, ~, m3_s] = computeActuationValues(t, params, options);

% curvature derivative
k3_s = D{1} * k3;

% zero matrices
zeNF  = zeros(3,NF); 
ze3NF = zeros(3,3*NF); 
ze3   = zeros(3);
ze4NF = zeros(3,4*NF);
ze3NB = zeros(3,3*NB);
ze3NW = zeros(3,3*NW);

%% head angular velocity equations

% via Om = Xs x Xst + Om_3*Xs
% with Om3 = C/xiR*k3_s + 1/xiR*m3 (and nondimensionalised)
%{
% operator onto Om
OmOp = dt * calT2 * eye(3);

% operator onto X
d_s = D{1}(1,1:NF);
ze  = 0*d_s;
XOp = -calT2 * [ze, -d30(3)*d_s, d30(2)*d_s; ...
                d30(3)*d_s, ze, -d30(1)*d_s; ...
                -d30(2)*d_s, d30(1)*d_s, ze];
    
% operator onto k3 (only build if k3 is a model unknown)
if options.MechTwist
    k3Op = -dt * d30.*repmat(d_s,3,1);
else
    k3Op = [];
end

% build operator
AO = [XOp, zeNF,ze3NF,ze3NB,ze3NW, OmOp, ze3, k3Op];

% right hand side
bO1 = -calT2 * cross(d30,d30n);
bO2 = dt * Gammad * calM3 * m3(1) * d30;
bO  = bO1 + bO2;
%}

% via Om = (d2_t.Xs)d1 + (X_st.d1)d2 + Om3*d3 
% with Om3 = C/xiR*k3_s + 1/xiR*m3
%{x
% operator onto Om
OmOp = calT2 * dt * eye(3);

% operator onto d10
d1Op = kron(-calT2 * dotProduct(d20-d20n, d30n), eye(3));

% operator onto X
d0 = D{1}(1,1:NF);
XOp = - calT2*d20.*repmat([d10(1)*d0,d10(2)*d0,d10(3)*d0],3,1);

% operator onto k3
if options.MechTwist
    k3Op = - dt * d30.*repmat(d0,3,1);
else
    k3Op = [];
end

% build operator 
AO = [XOp, zeros(3,4*NF+3*NB+3*NW), OmOp, d1Op, k3Op];

% right hand side
b1 = calT2 * dotProduct(-d30n,d10) * d20;
b2 = dt * calMT * d30;  % <~~ active twist term not included
bO = b1 + b2;
%}

% as above, with alternate linearisation
%{
% operator onto Om
OmOp = calT2 * dt * eye(3);

% operator onto X
d0 = D{1}(1,1:NF);
XOp1 = -calT2*d20.*repmat([d10(1)*d0,d10(2)*d0,d10(3)*d0],3,1);
ded2 = d20-d20n;
XOp2 = -calT2*d10.*repmat([ded2(1)*d0,ded2(2)*d0,ded2(3)*d0],3,1);

% operator onto k3
k3Op = - dt * d30.*repmat(d0,3,1);

% build operator 
AO = [XOp1+XOp2, zeros(3,4*NF+3*NB+3*NW), OmOp, zeros(3), k3Op];

% right hand side
b1 = calT2 * dotProduct(-d30n,d10) * d20;
b2 = dt * calMT * d30;  % <~~ active twist term not included
bO = b1 + b2;
%}

% via X_st = Om x X_s
%{
% operator onto Om
OmOp = dt * [0, -d30(3), d30(2); ...
            d30(3), 0, -d30(1); ...
            -d30(2), d30(1), 0];

% operators onto X
d_s = D{1}(1,1:NF);
XOp = blkdiag(d_s, d_s, d_s);

% build operator
AO = [XOp, zeNF, ze3NF, ze3NB, ze3NW, OmOp, ze3];

% augment if twist enabled
if options.MechTwist
    AO = [AO, zeros(3,NF)];
end

% right hand side
bO = d30n;

%}

% from old code
%{
Ones  = ones(NF,1);
d2mDs = dotDerivativeOperator(kron(d20-d20n,Ones),kron(eye(3),D{1}));
OX1   = d10.*repmat(d2mDs(1,:),3,1);
d1mDs = dotDerivativeOperator(kron(d10,Ones),kron(eye(3),D{1}));
OX2   = d20.*repmat(d1mDs(1,:),3,1);
d_s0  = D{1}(1,:);
OX3   = dot(d10-d10n,d20)*blkdiag(d_s0,d_s0,d_s0);
OX    = OX1+OX2+OX3;
OO    = -dt*eye(3);
AO  =  [OX, zeros(3,4*NF+3*NB+3*NW), OO, zeros(3)];
if options.MechTwist
    AO = [AO, zeros(3,NF)];
end
bO  = dot(d20n, d10).*d20;
%}

%% material normal equations
% via d10_t = omega X d10

% operators onto d10
DD1 = eye(3); 

crOp = [0, -Om(3), Om(2) ; ...
        Om(3), 0, -Om(1) ; ...
        -Om(2), Om(1), 0];
DD2 = -dt * crOp;

% operator onto Om
W  = [0, -d10(3), d10(2) ; ...
      d10(3), 0, -d10(1) ; ...
      -d10(2), d10(1), 0];
DO = dt*W;

% left hand side operator
% AD = [ze4NF, ze3NF, ze3NB, ze3NW, DO, DD1];
AD = [ze4NF, ze3NF, ze3NB, ze3NW, ze3, DD1+DD2];
if options.MechTwist
    AD = [AD, zeros(3,NF)];
end

% right hand side:
bD = d10n;
  
%% build block system:

% left hand side block matrix:
A = [AO; AD];
b = [bO; bD];

end % function
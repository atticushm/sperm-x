function [A, b] = buildKinematicsFluid(X, Y, d10, d10n, params, options)

% time step size:
dt = params.dt;

% number of nodes
NF = params.NF;
NB = params.NBt;
NW = params.NW;

% zero matrices
ze3NF = zeros(3,3*NF); 
ze4NF = zeros(3,4*NF);
ze3NB = zeros(3,3*NB);
ze3NW = zeros(3,3*NW);

%% head angular velocity equations:
% via w = 1/2 curl(u) with u the regularised stokeslet fluid velocity

X0  = proxp(X);
eps = params.epsilon;

% ~~ flagellum integrals ~~
% terms multiplying onto f
Xq  = splineFlagellum(X);
NNF = NearestNeighbourMatrix(Xq,X,options.ProcType);
dSXX = RegStokesletDerivVelocity(X0, Xq, eps);
fx = 1/2 * (-[dSXX{3,:,2}] + [dSXX{2,:,3}])*NNF;
fy = 1/2 * ( [dSXX{3,:,1}] - [dSXX{1,:,3}])*NNF;
fz = 1/2 * (-[dSXX{2,:,1}] + [dSXX{1,:,2}])*NNF;

% ~~ head integrals ~~
% terms multiplying onto phi
NNB  = params.NNB;
dSXY = RegStokesletDerivVelocity(X0, Y, eps);
phix = 1/2 * (-[dSXY{3,:,2}] + [dSXY{2,:,3}])*NNB;
phiy = 1/2 * ( [dSXY{3,:,1}] - [dSXY{1,:,3}])*NNB;
phiz = 1/2 * (-[dSXY{2,:,1}] + [dSXY{1,:,2}])*NNB;

% ~~ wall integrals ~~
if options.Backstep
    NNW  = params.NNW;
    Xw   = params.Xw;
    dSXW = RegStokesletDerivVelocity(X0, Xw, eps);
    phix = 1/2 * (-[dSXW{3,:,2}] + [dSXW{2,:,3}])*NNW;
    phiy = 1/2 * ( [dSXW{3,:,1}] - [dSXW{1,:,3}])*NNW;
    phiz = 1/2 * (-[dSXW{2,:,1}] + [dSXW{1,:,2}])*NNW;
else
    psix = [];
    psiy = [];
    psiz = [];
end

% ~~ build operator ~~
[fx1,fx2,fx3] = extractComponents(fx);
[fy1,fy2,fy3] = extractComponents(fy);
[fz1,fz2,fz3] = extractComponents(fz);
[px1,px2,px3] = extractComponents(phix);
[py1,py2,py3] = extractComponents(phiy);
[pz1,pz2,pz3] = extractComponents(phiz);
[qx1,qx2,qx3] = extractComponents(psix);
[qy1,qy2,qy3] = extractComponents(psiy);
[qz1,qz2,qz3] = extractComponents(psiz);

AF = [zeros(1,4*NF), fx1,px1,qx1,fx2,px2,qx2,fx3,px3,qx3, -1,0,0, 0,0,0; ...
      zeros(1,4*NF), fy1,py1,qy1,fy2,py2,qy2,fy3,py3,qy3, 0,-1,0, 0,0,0; ...
      zeros(1,4*NF), fz1,pz1,qz1,fz2,pz2,qz2,fz3,pz3,qz3, 0,0,-1, 0,0,0];
bF = zeros(3,1);

%% material normal equations:
% via d10_t = omega X d10

% ~ backwards difference time stepping ~~
% left hand side terms:
DD = eye(3);     
W  = [0, -d10(3), d10(2) ; ...
      d10(3), 0, -d10(1) ; ...
      -d10(2), d10(1), 0];
DO = dt*W;   

% left hand side operator:
AD = [ze4NF, ze3NF, ze3NB, ze3NW, DO, DD];

% right hand side:
bD = d10n;
  
%% build block system:

% left hand side block matrix:
A = [AF; AD];
b = [bF; bD];

end % function
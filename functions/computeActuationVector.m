function [mvec, mvec_s, mint] = computeActuationVector(t, d1, d2, d3, params, options)

% dimensionless parameters
calS4 = params.calS4;
calM1 = params.calM1;
calM2 = params.calM2;
calM3 = params.calM3;

% compute values of scalar functions
[m1, m2, m3, m1_s, m2_s, m3_s] = computeActuationValues(t, params, options);

% warm up over a prescribed period
warm_up_beats = params.WarmUp;
if t < warm_up_beats*params.T
    calM1 = calM1 * (t/warm_up_beats/params.T);
    calM2 = calM2 * (t/warm_up_beats/params.T);
    calM3 = calM3 * (t/warm_up_beats/params.T);
end

% moment vector 
mvec = calS4*(calM1*repmat(m1,3,1).*d1 + ...
              calM2*repmat(m2,3,1).*d2 + ...
              calM3*repmat(m3,3,1).*d3 );
          
% finite difference operator
D = finiteDifferences(linspace(0,1,length(d1)/3),1);
D1 = kron(eye(3), D{1});
          
% derivative of frame
d1_s = D1*d1;
d2_s = D1*d2;
d3_s = D1*d3; 
          
% moment derivative vector (actually equivalent to D1*mvec, but still...)
mvec_s = calS4*(calM1*(repmat(m1_s,3,1).*d1 + repmat(m1,3,1).*d1_s) + ...
                calM2*(repmat(m2_s,3,1).*d2 + repmat(m2,3,1).*d2_s) + ...
                calM3*(repmat(m3_s,3,1).*d3 + repmat(m3,3,1).*d3_s));

% integral
mint = computeActuationIntegrals(m1,m2,m3,d1,d2,d3,t,params);
            
end
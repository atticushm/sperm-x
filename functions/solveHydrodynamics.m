function [V,fm] = SolveHydrodynamics(X, X_t, tt, params)

%SOLVEHYDRODYNAMICS solves the hydrodynamics problem at each iteration for
%the unknown force denisity fm, and returns the nonlocal contribution to
%the node velocities in V.
%This function assumes there is no nonlocal interaction between the cell
%head and flagellum; that is, only flagellum-flagellum nonlocal
%hydrodynamic interactions are considered.
%
%See also REGSTOKESLETANALYTICINTEGRALS, buildTensorOp, TNBFRAME

% discretisation parameters:
N = params.NF;
Q = N-1;

% modelling parameters:
calS4 = params.calS4;
gamma = params.gamma;
q = params.q;
epsilon = params.epsilon;
c_perp = params.c_perp;
icp = 1/c_perp;

% arclength discretisation:
s = linspace(0,1,N);

% tangents at segment midpoints:
ttm = Midpoints(tt);

% velocities at midpoints:
Xm_t = Midpoints(X_t);

% local operator onto f_vis:
TT = buildTensorOp(ttm);
AH_inner_LGL = icp*(eye(3*Q) +(gamma-1)*TT);

% curve midpoints:
Xm = Midpoints(X);

% rotation matrices:
Rot = RotationMatrix(Xm, [1;0;0]);

% nonlocal operator onto f_vis:
ds = 1/Q;
SXX = RegStokesletAnalyticIntegrals(Xm,Xm,ds/2,Rot,epsilon);

% 'furthest neighbours' matrix for implementing limits of nonlocal
% integral operator:
sm = 0.5*(s(1:end-1)+s(2:end));
dxi = abs(sm-sm');
fn = zeros(Q);
fn(dxi>q) = 1;
fn_rep = repmat(fn,3,3);
AH_outer = SXX.*fn_rep;

% build and solve for f_vis:
AH = -AH_inner_LGL -AH_outer;
bH = calS4 * Xm_t;
fm = AH\bH;

% nonlocal velocity contribution at segment midpoints is thus:
Vm = -AH_outer*fm;

% spline to obtain contributions at nodes:
[Vm1,Vm2,Vm3] = extractComponents(Vm);
V1 = spline(sm,Vm1,s);   
V2 = spline(sm,Vm2,s);     
V3 = spline(sm,Vm3,s);
V = [V1(:); V2(:); V3(:)];

end % function
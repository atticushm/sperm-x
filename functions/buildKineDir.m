function [A, b] = buildKineDir(d10, d10n, Om, params, options)

% encodes the kinematic equation describing the rotation of the d1 director
% at the head-flagellum join, relating d1(0) and Om(0)

% discretisation parameters
NF = params.NF;
NB = params.NBt;
NW = params.NW;
dt = params.dt;

%% build matrix

% operators onto d1
dOp1 = 1/dt * eye(3);

% operators onto Om
% OmOp = [0,-d10(3),-d10(2); d10(3),0,-d10(1); -d10(2),d10(1),0];
% dOp2 = [];

% alternatively, operator onto d10
OmOp = zeros(3);
dOp2 = -[0,-Om(3),Om(2); Om(3),0,-Om(1); -Om(2),Om(1),0];

% build matrix
if options.MechTwist
    A = [zeros(3,7*NF+3*NB+3*NW), OmOp, dOp1+dOp2, zeros(3,NF)];
else
    A = [zeros(3,7*NF+3*NB+3*NW), OmOp, dOp1+dOp2];
end 

%% buld rhs

b = d10n/dt;

end % function
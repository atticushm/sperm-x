function [m1,m2,m3] = actTrig(t, params)

% actuation functions from a travelling trig (sin/cos) wave

% parameters
NF = params.NF;
k_bend = params.k_bend;
k_twist = params.k_twist;
om_bend = params.om_bend;
om_twist = params.om_twist;

% arclength
s = linspace(0,1,NF)';

% travelling trig bending wave
m1 = -sin(k_bend*s - om_bend*t);
m2 =  cos(k_bend*s - om_bend*t);
m3 = -cos(k_twist*s - om_twist*t);

end % function
function [head_force, flag_force, head_moment, flag_moment, F_tot_rel_err, M_tot_rel_err] ...
    = checkPhysics(X, Y, f, phi, T, k3, d1, d2, d10i, t, params, options)

% checks that total force and total moment are zero, amongst other things.
% note that f is a force per unit length, but phi is a force (from NEAREST,
% phi = phi/L^2 * weight)

% discretisation parameters
NF  = params.NF;                % # of flagellum nodes
NNB = params.NNB;               % NN matrix for head

% modelling parameters
Gamma = params.Gamma;
calS4 = params.calS4;
calM1 = params.calM1;
calM2 = params.calM2;
calM3 = params.calM3;

% head-flagellum join 
X0 = proxp(X); 

% compute actuation values
[m1,m2,m3] = computeActuationValues(t, params, options);

% bending stiffness values
[E,E_s,~] = computeStiffness(d1, d2, options);

% arclength discretisation
s = linspace(0,1,NF);

% generate finite difference schemes
D  = finiteDifferences(s, 1:4);
D1 = kron(eye(3),D{1});  
D2 = kron(eye(3),D{2}); 
D3 = kron(eye(3),D{3});

% approximate derivatives to curve
X_s = normalise(D1*X); 
X_ss = D2*X;
X_sss = D3*X;

% compute actuation values
[mvec, mvec_s, mint] = computeActuationVector(t, d1, d2, X_s, params, options);

% total flagellum force and moment
flag_force = calcFlagForce(f);
flag_moment = calcFlagMoment(X, X0, f);

% head force and moment
head_force  = calcBodyForce(phi, NNB);
head_moment = calcBodyMoment(Y, X0, phi, NNB);

% total force (and relative error)
F_tot = flag_force + head_force;
a = flag_force;
b = -head_force;
F_tot_rel_err = norm(a-b)/norm(b);

% total moment (and relative error)
M_tot = flag_moment + head_moment;
a = flag_moment;
b = -head_moment;
M_tot_rel_err = norm(a-b)/(norm(b)+1);  % plus 1 to avoid small value errors

% proximal force boundary condition 
Fint = -E.*X_sss - E_s.*X_ss ...
       +repmat(T,3,1).*X_s ...
       +crossProdOp(X_s)*mvec ...
       +Gamma*repmat(k3,3,1).*crossProdOp(X_s)*X_ss;
a = proxp(Fint);
b = head_force;
F_bc_rel_err = norm(a-b)/norm(b);

% proximal moment boundary condition 
Mint = E.*crossProdOp(X_s)*X_ss + Gamma*repmat(k3,3,1).*X_s;
a = proxp(Mint) - M0;
b = head_moment;
M_bc_rel_err = norm(a-b)/norm(b);

% proximal tension boundary condition
Targ = dotProduct(X_ss, E.*X_ss) + T;
a = Targ(1);
b = dot(head_force, proxp(X_s));
T_bc_rel_err = norm(a-b)/norm(b);

% force equilibrium equation
% we should have that F_{int,s} = f
Fint_s = D1 * Fint; 
F_eq   = Fint_s - f;

% moment equilibrium equation
Mint_s = D1 * Mint;
M_eq   = Mint_s + crossProdOp(X_s) * Fint ...
         + calS4*(calM1*repmat(m1,3,1).*d1 + calM2*repmat(m2,3,1).*d2 + calM3*repmat(m3,3,1).*X_s);
% (for plotting):
    M_eq_1 = Mint_s + crossProdOp(X_s)*Fint;
    M_eq_2 = -calS4 * (calM1*repmat(m1,3,1).*d1 + calM2*repmat(m2,3,1).*d2 + calM3*repmat(m3,3,1).*X_s);

end % function
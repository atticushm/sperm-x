function I = regStokesletNonlocalNumericalIntegralsNew(X, q, epsilon, proc)

% X are field and source points along flagellum
% q is half-length of local region 
% epsilon is regularisation parameter
% proc is build location (cpu or gpu)

% number of nodes
N = length(X)/3;
M = N;

% segment size
ds = 1/(N-1);

% local region is q=Q*ds, so 
Q = q/ds;

% weights for trapezium rule numerical integration
id = triu(2*ones(N),Q+2) + tril(2*ones(N),-(Q+2)) + ...
     diag(ones(N-(Q+1),1),Q+1) + diag(ones(N-(Q+1),1),-(Q+1));
id(Q+3:end,1) = ones(N-(Q+2),1);
id(1:N-(Q+2),end) = ones(N-(Q+2),1);

% stokeslets and submatrices
S = regStokeslet(X,X,epsilon,proc);
SXX = S(1:M,1:N);
SXY = S(1:M,N+1:2*N);
SXZ = S(1:M,2*N+1:3*N);
SYX = S(M+1:2*M,1:N);
SYY = S(M+1:2*M,N+1:2*N);
SYZ = S(M+1:2*M,2*N+1:3*N);
SZX = S(2*M+1:3*M,1:N);
SZY = S(2*M+1:3*M,N+1:2*N);
SZZ = S(2*M+1:3*M,2*N+1:3*N);

% integrators
IXX = ds/2 * id.*SXX;
IXY = ds/2 * id.*SXY;
IXZ = ds/2 * id.*SXZ;
IYX = ds/2 * id.*SYX;
IYY = ds/2 * id.*SYY;
IYZ = ds/2 * id.*SYZ;
IZX = ds/2 * id.*SZX;
IZY = ds/2 * id.*SZY;
IZZ = ds/2 * id.*SZZ;

% integrator operator
I = [IXX,IXY,IXZ; IYX,IYY,IYZ; IZX,IZY,IZZ];

end % function 
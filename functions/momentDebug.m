function rerr = momentDebug(X, Y, T, d1, d2, f, phi, t, params, options)

% SUPER DEBUG TIME

% discretisation parameters
NF  = params.NF;                % # of flagellum nodes
NNB = params.NNB;               % NN matrix for head

% compute actuation values
[m1,m2,m3,m1_s,m2_s] = computeActuationValues(t, params, options);

% bending stiffness values
[E,E_s,~] = computeStiffness(d1, d2, options);

% arclength discretisation
s = linspace(0,1,NF);

% generate finite difference schemes
D  = finiteDifferences(s, 1:4);
D1 = kron(eye(3),D{1});  
D2 = kron(eye(3),D{2}); 
D3 = kron(eye(3),D{3});
D4 = kron(eye(3),D{4});

% approximate derivatives to curve
X_s = normalise(D1*X); 
X_ss = D2*X;
X_sss = D3*X;
X_ssss = D4*X;
T_s = D{1}*T;

% head-flagellum join and derivates there
X0 = proxp(X); 
X0_s = proxp(X_s);
X0_ss = proxp(X_ss);

% compute actuation values
[mvec, mvec_s, mint] = computeActuationVector(t, d1, d2, X_s, params, options);

% repmats
Trep = repmat(T,3,1);
T_srep = repmat(T_s,3,1);

%% compute Mint

% analytical expression
Mint   = E.*crossProdOp(X_s)*X_ss;
Mint_s = E.*crossProdOp(X_s)*X_sss;

% numerical derivative
Mint_s_num = D1 * Mint;

%% compute Fint

% analytical expression
Fint   = -E.*X_sss + Trep.*X_s + crossProdOp(X_s)*mvec;
Fint_s = -E.*X_ssss + T_srep.*X_s + Trep.*X_ss + crossProdOp(X_ss)*mvec + crossProdOp(X_s)*mvec_s;
     
% numerical derivative
Fint_s_num = D1 * Fint;

%% NUMERICAL AND ANALYTICAL CONTACT MOMENT

a = mat(Mint_s);
b = mat(Mint_s_num);
rerr{1} = vecnorm(a-b)./vecnorm(b);

%% NUMERICAL AND ANALYTIC CONTACT FORCE

a = mat(Fint_s);
b = mat(Fint_s_num);
rerr{2} = vecnorm(a-b)./vecnorm(b);

%% FORCE EQUILIBRIUM EQUATION

a = mat(Fint_s);
b = mat(f);
rerr{3} = vecnorm(a-b)./vecnorm(b);

%% MOMENT EQUILIBRIUM EQUATION

a = mat(Mint_s + crossProdOp(X_s)*Fint);
b = mat(-mvec);
rerr{4} = vecnorm(a-b)./vecnorm(b);

%% TOTAL HYDRODYNAMIC FORCE BALANCE

a = calcFlagForce(f);
b = -calcBodyForce(phi, NNB);
rerr{5} = vecnorm(a-b)./(vecnorm(b)+1);

%% TOTAL HYDRODYNAMIC MOMENT BALANCE

a = calcFlagMoment(X,X0,f);
b = -calcBodyMoment(Y, X0, phi, NNB);
rerr{6} = vecnorm(a-b)./(vecnorm(b)+1);

%% PROXIMAL FORCE BOUNDARY CONDITION

a = calcBodyForce(phi, NNB);
b = proxp(Fint);
rerr{7} = vecnorm(a-b)./vecnorm(b);

%% DISTAL FORCE BOUNDARY CONDTION

a = distp(Fint);
b = zeros(3,1);
rerr{8} = vecnorm(a-b)./(vecnorm(b)+1);

%% PROXIMAL MOMENT BOUNDARY CONDITION

a = cross(calcBodyMoment(Y,X0,phi,NNB), X0_s);
b = proxp(E.*X_ss) - cross(mint,X0_s);
rerr{9} = vecnorm(a-b)./vecnorm(b);

%% DISTAL MOMENT BOUNDARY CONDITION

a = distp(E.*X_ss);
b = zeros(3,1);
rerr{10} = vecnorm(a-b)./(vecnorm(b)+1);

%% PROXIMAL TENSION BOUNDARY CONDITION

a = dot(calcBodyForce(phi,NNB), proxp(X_s));
b = dot(proxp(E).*X0_ss, X0_ss) + T(1);
rerr{11} = vecnorm(a-b)./vecnorm(b);

%% DISTAL TENSION BOUNDARY CONDITION

a = T(end);
b = 0;
rerr{12} = vecnorm(a-b)./(vecnorm(b)+1);

%% INTEGRATED MOMENT EQUILIBRIUM EQUATION (A)

% a = mat(-Mint);
% b = mat(computeMomentIntegrals(X,Fint_s) - ...
%         computeActiveMomentDistalIntegrals(d1,d2,X_s,m1,m2,m3,params));

a = mat(-Mint + computeActiveMomentDistalIntegrals(d1,d2,X_s,m1,m2,m3,params));
b = mat(computeMomentIntegrals(X,Fint_s));

rerr{13} = vecnorm(a-b)./(vecnorm(b)+1);

%% INTEGRATED MOMENT EQUILIBRIUM EQUATION (B)

% a = mat(-Mint);
% b = mat(computeMomentIntegrals(X,f) - ...
%         computeActiveMomentDistalIntegrals(d1,d2,X_s,m1,m2,m3,params));

a = mat(-Mint + computeActiveMomentDistalIntegrals(d1,d2,X_s,m1,m2,m3,params));
b = mat(computeMomentIntegrals(X,f));

rerr{14} = vecnorm(a-b)./(vecnorm(b)+1);

end
function X0 = InitialCondition(TYPE, N)

%INITIALCONDITION Generates an initial curve for a Sperm-X/Fil-X problem.
% X0 = InitialCondition(TYPE,N) builds the vector of N coordinates for an 
% initial condition specified by string input TYPE.
%
% TYPE can be one of the following: 'parabolic', 'semicircular', 'flat',
% 'sinusoidal', 'overextended'.
%
% See also PARABOLICCURVE, SEMICIRCLECURVE, HELICALCURVE, SINUSOIDALCURVE

switch TYPE
    case {'semi','semicircular','semicircle'}
        X0 = SemiCircleCurve(N);
    case {'parabolic','parabola'}
        grad = 0.5;         % gradient of planar parabola.
        X0 = ParabolicCurve(grad,N);
    case {'flat','line'}
        grad = 1e-6;
        X0 = ParabolicCurve(grad,N);
    case {'helical','helix'}
        X0 = HelicalCurve(2*pi,0.1,N);
    case 'sinusoidal'
        wavk = 2*pi;
        int_k = wavk;       % wave number.
        int_A = 0.1;        % wave amplitude.
        X0 = SinusoidalCurve(int_k, int_A, N);
    case 'overextended'
        ds_ext = 1/(Q-5);
        x_ext = 0:ds_ext:Q*ds_ext;
        X_ext = [x_ext(:); zeros(2*N,1)];
        X0 = X_ext;
    otherwise
        warning('No valid initial condition selected!')
        return
end

end
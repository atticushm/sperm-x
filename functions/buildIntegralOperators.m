function [IXX,IXY,IXW] = buildIntegralOperators(X,Y,params,options)

% builds integrator matrices for interactions between the flagellum and (a)
% itself, (b) the cell body and (c) the wall.

epsilon  = params.epsilon;      % regularisation parameter for head/tail interactions
wepsilon = params.wepsilon;     % regularisation parameter for wall interactions
Xw       = params.Xw;           % wall nodes
proc     = options.ProcType;    % build on cpu or gpu
N        = params.NF;           % # of flagellum nodes

% segment length
ds = 1/(N-1);

% local region is q=Q*ds, so 
q = params.q;                   % half-length of local region size
Q = q/ds;

% nearest neighbour matrices
NNB = params.NNB;
NNW = params.NNW;

% weights for trapezium rule numerical integration
id = triu(2*ones(N),Q+2) + tril(2*ones(N),-(Q+2)) + ...
     diag(ones(N-(Q+1),1),Q+1) + diag(ones(N-(Q+1),1),-(Q+1));
id(Q+3:end,1) = ones(N-(Q+2),1);
id(1:N-(Q+2),end) = ones(N-(Q+2),1);
id3d = ds/2 * repmat(id,3,3);

if options.UseBlakelets
    %% use regularised blakelets 
    % nonlocal flagellum-flagellum interactions
    IXX = id3d.*regBlakelet(X,X,epsilon);

    % flagellum-body interactions
    IXY = regBlakelet(X,Y,epsilon)*NNB;

    % flagellum-wall interactions
    IXW = regBlakelet(X,Xw,wepsilon)*NNW;    

else
    %% use regularised stokeslets 
    
    % flagellum-flagellum interactions
    IXX = id3d.*regStokeslet(X,X,epsilon,proc);
    
    % flagellum-body interactions
    IXY = regStokeslet(X,Y,epsilon,proc)*NNB;

    % flagellum-wall interactions
    IXW = regStokeslet(X,Xw,wepsilon,proc)*NNW;
end

end % function
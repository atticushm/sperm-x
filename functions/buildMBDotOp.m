function MDot = buildMBDotOp(X, Y, X0, X_s, params, varargin)

% constructs the operator for inclusion in the proximal compatibility
% equation boundary conditions, computing MB.X0_s by multiplying onto the
% unknown forces phi, where MB is the hydrodynamic head moment.

NN = params.NNB;    % nearest neighbour matrix between head discretisations
NB = params.NBt;    % # of head traction nodes
NF = params.NF;     % # of flagellum nodes
NW = params.NW;     % # of wall nodes

% components
[t1,t2,t3] = extractComponents(proxp(X_s));
[Y1,Y2,Y3] = extractComponents(Y);

% differences
r1 = Y1-X0(1);
r2 = Y2-X0(2);
r3 = Y3-X0(3);
R  = [r1',r2',r3']*NN;
[R1,R2,R3] = extractComponents(R); 

% build operators onto phii
Mx = t2*R3-t3*R2;
My = -t1*R3+t3*R1;
Mz = t1*R2-t2*R1;

% zero vectors
zeNF = zeros(1,NF);
zeNW = zeros(1,NW);

% build operator onto forces
if ~isempty(NN)
    MDot = [zeNF, Mx, zeNW, ...
            zeNF, My, zeNW, ...
            zeNF, Mz, zeNW  ];
else
    MDot = zeros(1,3*NF+3*NW+3*NB);
end

% alternative form for debugging purposes
if ~isempty(varargin) && strcmpi(varargin{1},'debug')
    MDot = [Mx, My, Mz];
end

end % function
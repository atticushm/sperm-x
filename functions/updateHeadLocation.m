function [y,Y] = updateHeadLocation(d1, d2, d3, X0, params, options)

% rotates and translates a reference body according to the orthonormal
% triad (director basis vectors) d1,d2,d3.
% d1, d2, d3 are the lab frame basis for the head
% X0 is lab frame position of head/flagellum join

%{
% local head normal is ey
ey = [0;1;0];

% compute rotation matrix from ey to d10 (head normal)
Reflection = @(u,n) u-2*n*(n'*u)/(n'*n);
S = Reflection(eye(3),ey+d10);
RotMat = Reflection(S, d10);

% identify phantom point to which X0 will connect
join = [params.ax(1);0;0];

% local head nodes
y_ref = params.y_ref;
Y_ref = params.Y_ref;

% rotate points
yM_rot = RotMat*VectorToMatrix(y_ref);
YM_rot = RotMat*VectorToMatrix(Y_ref);
j_rot = RotMat*join;

% translate rotated nodes to connect head to flagellum at X0
dy = j_rot-X0;
y = MatrixToVector(yM_rot-dy);
Y = MatrixToVector(YM_rot-dy);
%}

% check d1, d2, d3 are the right size
N = params.NF;
if length(d1) > 3
    if length(d1) == 3*N
        d1 = proxp(d1);
        d2 = proxp(d2);
        d3 = proxp(d3);
    else
        warning('d1,d2,d3 are not the right size! (3x1)')
    end
end

% local head coordinates
yloc = params.y_ref;
Yloc = params.Y_ref;

% local head basis vectors
e1 = [1;0;0];
e2 = [0;1;0];
e3 = [0;0;1];
% e1 = [0;1;0];
% e2 = [0;0;1];
% e3 = [1;0;0];

% via Bower: e_i = R^T * d_i, where R = kron(m_i, e_i)
% compute rotation matrix
% switch options.Species
%     case 'human'
%         % rotate about axis so plane of head is normal to wall
%         R = kron(d1,e2') + kron(d2,e1') + kron(d3,e3');
%     case 'mouse'
%         % no head rotation
%         R = kron(d1,e1') + kron(d2,e2') + kron(d3,e3');
% end
R = kron(d1,e3') + kron(d2,e2') + kron(d3,e1');

% join location in body coordinates is
% rloc = [params.ax(1);0;0];
rloc = params.X0_ref;

% rotate body frame coordinates 
yrot_mat = R * mat(yloc);
Yrot_mat = R * mat(Yloc);
rrot     = R * rloc;

% translate rotated nodes to connect to flagellum at X0
dy = rrot-X0;
y  = vec(yrot_mat-dy);
Y  = vec(Yrot_mat-dy);

% ~~~ testing ~~~
%{
clf;
[x1,x2,x3] = extractComponents(yloc);
[X1,X2,X3] = extractComponents(y);
scatter3(x1,x2,x3,'.'); axis equal; hold on; scatter3(X1,X2,X3,'.')
xlabel('x'); ylabel('y'); zlabel('z');
%}

end % function
function [A,b] = buildCompat(X, Y, d1, d2, Xn, k3n, t, params, options)

% builds the linear system corresponding to the linearised compatibility
% equations relating curvature and angular velocity.

% discretisation parameters
dt = params.dt;
NF = params.NF;
NB = params.NBt;
NW = params.NW;
s  = linspace(0,1,NF);

% modelling parameters
calT2 = params.calT^2;
Gammad = params.Gammad;
Gammas = params.Gammas;
calM3 = params.calM3;

% finite difference matrices
D = finiteDifferences(s, 1:2);

% derivatives 
X_s = normalise(kron(eye(3),D{1}) * X);
X_ss = kron(eye(3),D{2}) * X;
Xn_s = normalise(kron(eye(3),D{1}) * X);

% tangent at X0
t0 = proxp(X_s);

% compute actuation values
[~, ~, ~, ~, ~, m3_s] = computeActuationValues(t, params, options);
[~, ~, mint] = computeActuationVector(t, d1, d2, X_s, params, options);

%% build linear operators

%{x
% operators onto k3
K1 = calT2 * eye(NF);
K2 = -dt * D{2};
AK = K1 + K2;

% operators on X
% operator encoding (X_s x X_ss).()_st
Xs_x_Xss = crossProdOp(X_s) * X_ss;
[x1,x2,x3] = extractComponents(Xs_x_Xss);
AX = - calT2 * [x1(:).*D{1}, x2(:).*D{1}, x3(:).*D{1}];

% right hand side terms
b1 = calT2 * k3n;
b2 = -calT2 * dotProduct(crossProdOp(X_s)*X_ss, Xn_s);
b3 = dt * Gammad * calM3 * calT2 * m3_s; 
b  = b1 + b2 + b3; 
%}

%{
% operators onto k3
K1 = calT2/dt * eye(NF);
K2 = - D{2};
AK = K1 + K2;

% operators onto X
dXs = (X_s-Xn_s)/dt;
dXs_x_Xs = crossProdOp(dXs) * X_s;
[x1,x2,x3] = extractComponents(dXs_x_Xs);
AX = -calT2 * [x1(:).*D{2}, x2(:).*D{2}, x3(:).*D{2}];

% right hand side
b1 = calT2/dt * k3n;
b2 = calMT * calM3 * calT2 * m3_s; 
b  = b1 + b2;
%}

%% apply boundary conditions

% operator for proximal boundary condition M.Xs onto forces
X0   = proxp(X);
MDot = buildMBDotOp(X, Y, X0, X_s, params);

% build matrix
A = [AX, zeros(NF), zeros(NF,3*NF+3*NB+3*NW), zeros(NF,6), AK];

% proximal conditions 
A(1,:) = [zeros(1,4*NF), MDot, zeros(1,6), -Gammas, zeros(1,NF-1)]; 
b(1)   = -dot(mint, t0);

% distal conditions into matrix
A(NF,:) = [zeros(1,4*NF), zeros(1,3*NB+3*NF+3*NW), zeros(1,6), zeros(1,NF-1), 1]; 
b(NF)   = 0;

end % function
function [z, X, T, f, phi, psi, Om, d10, k3] = solveMatrixSystem(A, b, NF, NB, NW, t, options)

% equilibrate, solve and extract unknowns of the iterative system

% scale matrix to improve conditioning
% only generate on first time step, and reuse to save on walltime
persistent P R C
if isempty(P) || (t==0)
    if strcmpi(options.ProcType,'cpu')
        [P,R,C] = equilibrate(A);
    else
        [P,R,C] = equilibrate(gather(A));
    end
end
Ae = R*P*A*C;
be = R*P*b;

% solve using mldivide 
ze = Ae\be;

% recover solutions to original problem
z = C*ze;

% extract solutions
X   = z(1         : 3*NF);                      % flagellum positions
T   = z(3*NF+1    : 4*NF);                      % tension values

zf_mat  = mat(z(4*NF+1 : 7*NF+3*NB+3*NW));
f_mat   = zf_mat(:,1:NF);
phi_mat = zf_mat(:,NF+1:NF+NB);
psi_mat = zf_mat(:,NF+NB+1:NF+NB+NW);

f   = vec(f_mat);                               % flagellum force per unit length
phi = vec(phi_mat);                             % head force per unit area
psi = vec(psi_mat);                             % wall force per unit area

Om  = z(7*NF+3*NB+3*NW+1 : 7*NF+3*NB+3*NW+3);   % head angular velocity
d10 = z(7*NF+3*NB+3*NW+4 : 7*NF+3*NB+3*NW+6);   % head director 

if options.MechTwist
    k3 = z(end-NF+1 : end);                     % twist curvature
else
    k3 = zeros(NF,1);
end

% ensure d10 is unit
d10 = d10/norm(d10);

% enforce planarity?
if options.ForcePlanar
   [X, f, Om, d10] = enforcePlanarity(X, f, Om, d10, options);
end

end 
function [Mx,My,Mz] = buildMBCrossOp(X, Y, X0, d3, params, varargin)

% constructs the operator in each x,y,z direction for inclusion in the
% proximal EHD boundary conditions, computing MB x X0_s by multiplying 
% onto the unknown forces phi, where MB is the hydrodynamic head moment.

% there's probably an easier way of doing this - the method below derives
% from full expansion of M x d3 in components using the Levi-Civita symbol.

NN  = params.NNB;       % nearest neighbour matrix between head discretisations
NF  = params.NF;        % # of flagellum nodes
NBt = params.NBt;       % # of head traction nodes
NBq = params.NBq;       % # of head quadrature nodes
NW  = params.NW;        % # of wall nodes

[t1,t2,t3] = extractComponents(proxp(d3));

% calculate differences
[Y1,Y2,Y3] = extractComponents(Y);
r1 = Y1'-X0(1);
r2 = Y2'-X0(2);
r3 = Y3'-X0(3);

% compute using loop - probably a way to vectorise...
for m = 1:NBt
    NNm      =  NN(1:NBq,m);
    
    % ~~~ x component
    % -e_1jk*t_j*M_k = -e_1jk*t_j*(int{e_klm*dY_l*phi_m}dS)
    M11(1,m) =  t2*r2*NNm + t3*r3*NNm;     % mults onto phi_x
    M12(1,m) = -t2*r1*NNm;                 % mults onto phi_y
    M13(1,m) = -t3*r1*NNm;                 % mults onto phi_z, etc 
    
    % ~~~ y component
    % -e_2jk*t_j*M_k = -e_2jk*t_j*(int{e_klm*dY_l*phi_m}dS)
    M21(1,m) = -t1*r2*NNm;
    M22(1,m) =  t1*r1*NNm + t3*r3*NNm;
    M23(1,m) = -t3*r2*NNm;
    
    % ~~~ z component
    % -e_3jk*t_j*M_k = -e_3jk*t_j*(int{e_klm*dY_l*phi_m}dS)
    M31(1,m) = -t1*r3*NNm;
    M32(1,m) = -t2*r3*NNm;
    M33(1,m) =  t1*r1*NNm + t2*r2*NNm;
end

% zero vectors
zeNF = zeros(1,NF);
zeNW = zeros(1,NW);

% build operator onto forces
if ~isempty(NN)
    Mx = [zeNF,M11,zeNW, zeNF,M12,zeNW, zeNF,M13,zeNW];
    My = [zeNF,M21,zeNW, zeNF,M22,zeNW, zeNF,M23,zeNW];
    Mz = [zeNF,M31,zeNW, zeNF,M32,zeNW, zeNF,M33,zeNW];
else
    Mx = repmat([zeNF,zeNW],1,3);
    My = Mx;
    Mz = Mx;
end

if ~isempty(varargin) && strcmp(varargin{1},'debug')
    % reduced form for debugging
    Mx = [M11, M12, M13];
    My = [M21, M22, M23];
    Mz = [M31, M32, M33];
end

end % function
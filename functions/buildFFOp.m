function [Fx,Fy,Fz] = buildFFOp(params)

% constructs the operator in each x,y,z direction for inclusion in the
% proximal EHD boundary conditions, generating FF by multiplying onto the
% unknown forces f.

% numbers of nodes
NF = params.NF;
NB = params.NBt;
NW = params.NW;

% weights for flagellum forces per unit length
ds = 1/(NF-1);
wgt = [ds/2, ds*ones(1,NF-2), ds/2];

% zero row vectors
zeNB = zeros(1,NB);
zeNW = zeros(1,NW);

% build operator onto forces
Fx = [wgt, zeNB, zeNW, zeros(1,2*NF+2*NB+2*NW)];
Fy = [zeros(1,NF+NB+NW), wgt, zeNB, zeNW, zeros(1,NF+NB+NW)];
Fz = [zeros(1,2*NF+2*NB+2*NW), wgt, zeNB, zeNW];

end % function
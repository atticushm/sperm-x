function k3 = ComputeTwistCurvature(t, N, params)

% computes the twist curvature from a prescribed twisting function R(s,t)
% though
%   k3 = int_s^1 R/ET ds 
% where ET is the twist modulus;

ET = params.calS;

% arclength 
s  = linspace(0,1,N);
ds = 1/(N-1);

% active twist function
R  = @(s) sin(pi * s - t)/ET*2;
Rs = R(s);

% twist at base is zero
k30 = 0;

% compute k3 
k3 = k30 + flipud(cumsum(ds*(Rs(1:end-1)+cumsum(Rs(2:end)))));
k3 = [0;k3(:)];

end % function
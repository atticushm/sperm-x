function [FH,MH] = UpdateHeadForceAndMoment(Xi, Yi, phi, params)

X0i = proxp(Xi);
NN = params.NN;

% ~~~~ total force on the head
FH = ComputeNNForce(phi, NN);

% ~~~~ total moment on the head
MH = ComputeNNMoment(Yi,X0i,phi,NN);

end % function
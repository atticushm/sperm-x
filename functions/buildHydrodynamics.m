function [A, b] = buildHydrodynamics(X, Y, y, Xn, IXX, IXY, IXW, params,    ...
    options)    

% discretisation parameters
NF = params.NF;             % # of flagellar nodes
NB = params.NBt;            % # of body nodes
NW = params.NW;             % # of wall nodes
dt = params.dt;             % time step

% modelling parameters
eps   = params.epsilon;     % reg parameter for head/tail interactions
weps  = params.wepsilon;    % reg parameter for wall interactions
calS4 = params.calS4;       % swimming parameter
cperp = params.c_perp;      % resistance coefficient
gam   = params.gamma;       % resistance ratio

% build location
proc = options.ProcType;

% wall nodes
xw = params.xw;
Xw = params.Xw;

% nearest neighbour matrices
NNB = params.NNB;
NNW = params.NNW;

% difference matrices
s = linspace(0,1,NF);
D = finiteDifferences(s, 1);

% tangents to flagellar curve at nodes
d3 = normalise(kron(eye(3),D{1}) * X);

%% flagellum forces

% terms onto unknown positions
AFX = calS4 * eye(3*NF);

% local operator onto force per unit length f
AXX_L = -dt/cperp * (eye(3*(NF)) + (gam-1)*tensorProd(d3));

% nonlocal operator onto f
AXX_NL = -dt * IXX;

% operator onto force per unit lengths is then 
AXX = AXX_L + AXX_NL;

% operators onto head & wall forces
AXY = -dt * IXY;
AXW = -dt * IXW;

%% head forces

% operator onto unknown positions
op  = [ones(NB,1),zeros(NB,NF-1)];
AHX = calS4 * blkdiag(op,op,op);

% operator onto unknown angular velocity
X0 = proxp(X);
dy = y-kron(X0,ones(NB,1)); 
[dy1,dy2,dy3] = extractComponents(dy); ze  = 0*dy1;
AHO = -dt * calS4 * [ze,-dy3,dy2; dy3,ze,-dy1; -dy2,dy1,ze];

% stokeslet operators onto forces
if options.UseBlakelets
    IYX = regBlakeletNumericalIntegrals(y,X,eps);
    IYY = regBlakelet(y,Y,eps)*NNB;
    IYW = regBlakelet(y,Xw,weps)*NNW;
else
    IYX = regStokesletNumericalIntegrals(y,X,eps,proc);
    IYY = regStokeslet(y,Y,eps,proc)*NNB;
    IYW = regStokeslet(y,Xw,weps,proc)*NNW;
end
AYX = -dt * IYX;
AYY = -dt * IYY;
AYW = -dt * IYW;

%% wall forces 

if ~isempty(Xw)
    if options.UseBlakelets
        IWX = regBlakeletNumericalIntegrals(xw,X,eps);
        IWY = regBlakelet(xw,Y,eps)*NNB;
        IWW = regBlakelet(xw,Xw,weps)*NNW;    
    else
        IWX = regStokesletNumericalIntegrals(xw,X,eps,proc);
        IWY = regStokeslet(xw,Y,eps,proc)*NNB;
        IWW = regStokeslet(xw,Xw,weps,proc)*NNW;
    end
else
    IWX = [];
    IWY = [];
    IWW = [];
end
AWX = -dt * IWX;
AWY = -dt * IWY;
AWW = -dt * IWW;

%% right hand sides

bF = calS4 * Xn; 
bH = calS4 * kron(proxp(Xn),ones(NB,1));
bW = zeros(3*NW,1);

%% build linear system 

% build matrix
A = [AFX, zeros(3*NF,NF), combineMatrices(AXX,AXY,AXW), zeros(3*NF,6),    ; ...
     AHX, zeros(3*NB,NF), combineMatrices(AYX,AYY,AYW), AHO, zeros(3*NB,3); ...
     zeros(3*NW,4*NF),    combineMatrices(AWX,AWY,AWW), zeros(3*NW,6),    ];

% augment if twist enabled
if options.MechTwist
    A = [A, zeros(3*NF+3*NB+3*NW, NF)];
end
 
% build right hand side
b = [bF; bH; bW];

end % function
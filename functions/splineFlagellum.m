function Xq = splineFlagellum(Xt, varargin)

% Xt are traction nodes.
% varargin can contain the traction node multiplier for generating the
% quadrature discretisation.

Nt = length(Xt)/3;      % # of traction nodes

if isempty(varargin)
    Nq = 4*Nt;          % # of quadrature nodes
else
    Nq = varargin{1} * Nt;
end

% arclength discretisations
st = linspace(0,1,Nt);
sq = linspace(0,1,Nq);

% determine nodes by splining
[x,y,z] = extractComponents(Xt);
xq = spline(st,x,sq);
yq = spline(st,y,sq);
zq = spline(st,z,sq);

Xq = [xq(:); yq(:); zq(:)];

end % function
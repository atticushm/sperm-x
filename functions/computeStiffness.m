function [E,E_s,E_ss] = computeStiffness(d1,d2,params,options)

% number of flagellum nodes
N = length(d1)/3;

% arclength discretisation
s = linspace(0,1,N);

% compute bending stiffness values
switch options.StiffModel
    case 'constant'
        % constant stiffness 
        E = ones(3*N,1);      
        E_s = zeros(3*N,1);   
        E_ss = zeros(3*N,1);  

    case 'varying'
        % varying stiffness for human sperm
        [E,E_s,E_ss] = varyingStiffness(s, params);    
        E = repmat(E,3,1);
        E_s = repmat(E_s,3,1);
        E_ss = repmat(E_ss,3,1);

    case 'ani'
        % experimental attempt to add anisotropic bending stiffness back
        % into SPX post-hoc
        % probably not allowed, proceed with caution!!
        [E,E_s,E_ss] = varyingStiffness(s, params);
        E = repmat(E,3,1).*d1 + 2*repmat(E,3,1).*d2;
        E_s = repmat(E_s,3,1);
        E_ss = repmat(E_ss,3,1);
end

end % function
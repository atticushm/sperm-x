function [A, b] = buildTension(X, V, d1, d2, k3, t, params, options)

% discretisation paramters
NF = params.NF;             % # of flagellar nodes
NB = params.NBt;            % # of head/body nodes
NW = params.NW;             % # of wall nodes

% modelling paramters
calS4  = params.calS4;
cperp  = params.c_perp;
lambda = params.lambda;
gam    = params.gamma;
Gam_s  = params.Gam_s;

% bending stiffness values    
[E,E_s,E_ss] = computeStiffness(d1, d2, params, options);

% arclength discretisation
s = linspace(0,1,NF);

% generate finite difference schemes
D = finiteDifferences(s, 1:4);

% build finite difference operators
D1 = kron(eye(3),D{1}); 
D2 = kron(eye(3),D{2}); 
D3 = kron(eye(3),D{3}); 
D4 = kron(eye(3),D{4});

% approximate derivatives to curve
X_s = normalise(D1*X); 
X_ss = D2*X;
X_sss = D3*X;

% derivative of nonlocal velocity contributions
if options.NonLocal
    V_s = D1 * V;
else
    V_s = 0*V;
end

% compute actuation values
[mvec, mvec_s] = computeActuationVector(t, d1, d2, X_s, params, options);

% derivatives of twist
k3_s = D{1} * k3;     

%% construct matrix 

% operators onto position from time stepping
TD = calS4 * cperp * lambda * dotDerivativeOperator(X_s, D1);

% operators onto position from bending terms
TX1 = (1+2*gam) *  dotDerivativeOperator(X_ss, E_ss.*D2);
TX2 = -3*gam * dotDerivativeOperator(X_s, E_s.*D4);
TX3 = -2*(gam-1) * dotDerivativeOperator(X_ss, E_s.*D3);
TX4 = 3*gam * dotDerivativeOperator(X_sss, E.*D3);
TX5 = (1+3*gam) * dotDerivativeOperator(X_ss, E.*D4);

% operators onto position from nonlocal contribution
TV = cperp*dotDerivativeOperator(V_s, D1);

% combined operator onto positions
TX = TX1+TX2+TX3+TX4+TX5 + TD + TV;

% operators onto positions from twist
Xs_x_Xss = crossProdOp(X_s)*X_ss;
[x1,x2,x3] = extractComponents(Xs_x_Xss);
TW = Gam_s * k3_s(:).*[x1(:).*D{3}, x2(:).*D{3}, x3(:).*D{3}];
TX = TX + TW;

% operators onto tension
TT1 = gam*D2(1:NF,1:NF);
TT2 = -diag(dotProduct(X_ss,X_ss));

TT  = TT1+TT2;

% build operator
A = [TX, TT, zeros(NF,3*NF+3*NB+3*NW), zeros(NF,6)];

%% construct right hand side

% contribution from inextensibility
b1 = cperp * (calS4*lambda); % - dotProduct(V_s, X_s));

% actuation terms
b2 = -gam * dotProduct(crossProdOp(X_sss)*mvec, X_s);
b3 = -(gam+1) * dotProduct(crossProdOp(X_ss)*mvec_s, X_s);

% combined rhs
b  = b1+b2+b3;

%% proximal boundary condition

% zero vectors
ze3   = zeros(1,3);
zeNF  = zeros(1,NF);
zeNW  = zeros(1,NW);

% terms for boundary condition
t0     = proxp(X_s);
XssDss = dotDerivativeOperator(X_ss,E.*D2);

% operator onto forces
if ~isempty(params.NNB)
    [FBx,FBy,FBz] = extractComponents(sum(params.NNB));
else
    FBx=[]; FBy=[]; FBz=[];
end
Fx = [zeNF, t0(1)*FBx, zeNW];
Fy = [zeNF, t0(2)*FBy, zeNW];
Fz = [zeNF, t0(3)*FBz, zeNW];

% update rows of linear system
A(1,:) = [-XssDss(1,:), -1, zeros(1,NF-1), Fx, Fy, Fz, ze3, ze3];
b(1)   = 0;

%% distal boundary condition 

% update rows of linear system
A(NF,:) = [zeros(1,3*NF), zeros(1,NF-1), 1, zeros(1,3*NF+3*NB+3*NW+6)];
b(NF)   = 0;

if options.MechTwist
    A = [A, zeros(NF)];
end

end % function
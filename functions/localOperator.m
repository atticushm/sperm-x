function [R,R_s] = LocalOperator(X_field, s_field, q, epsilon)

% This code computes:
%       d/ds R(x(s),X(s')) = d/ds int S(x(s),X(s')) ds'
% which requires application of the Leibniz rule.

%% Information from inputs:

num_field = length(X_field)/3; 
Q = num_field-1;
ds = 1/Q;

L = CheckArclength(X_field);
[x_field, y_field, z_field] = extractComponents(X_field);
XM_field = VectorToMatrix(X_field(:));

% Obtain segment midpoints:
X_midp = Midpoints(X_field);

% Obtain tangent vectors:
T_field = TangentsToCurve(X_field, s_field);
TM_field = VectorToMatrix(T_field);

% Rotation matrix for segments:
e1 = [1;0;0];
R_midp = RotationMatrix(X_field,e1);

% Identify midpoints 'local' to each field point:
nn = NearestNodesAlongArclength(X_field, q);
nn = repmat(nn,3,3);

%% Compute R: 
% R is the integral of regularised stokeslets over the region local to each
% field point.
% The integrals across the curve are approximated by taking the sum of
% analytical integrals across straight line rods aligned with each
% segment midpoint.

R_full = RegStokesletAnalyticIntegrals(X_field, X_midp, ds/2, R_midp, epsilon);
R_nn = R_full .* nn;
R = R_nn * kron(eye(3),ones(Q,1));

%% Compute dR/ds:
% dR/ds is the derivative with respect to the arclength s of R. This
% requires using the Leibniz rule.

if nargout == 2 % only compute if requested
    
    % Identify what the limits of integration a(s) and b(s) are for each field
    % point:
    limits = zeros(num_field, 2);

    % a(s):
    limits(s_field<q,1) = 0;
    limits(q<=s_field,1) = s_field(q<=s_field)-q;

    % b(s):
    limits(s_field<=L-q,2) = s_field(s_field<=L-q) + q;
    limits(s_field>L-q,2) = 1;

    % Derivatives of limits:
    dlimits = zeros(num_field,2);
    dlimits(q<=s_field) = 1;        % da(s)/ds
    dlimits(s_field<=L-q,2) = 1;    % db(s)/ds

%     % Coordinates at s_a i.e. X(s_a):
%     ppx = spline(s_field,x_field); ppy = spline(s_field,y_field); ppz = spline(s_field,z_field);
%     x_a = ppval(ppx,limits(:,1)); y_a = ppval(ppy,limits(:,1)); z_a = ppval(ppz,limits(:,1));
%     X_a = [x_a(:); y_a(:); z_a(:)];     XM_a = VectorToMatrix(X_a);
% 
%     % Coordinates at s_b i.e. X(s_b):
%     x_b = ppval(ppx,limits(:,2)); y_b = ppval(ppy,limits(:,2)); z_b = ppval(ppz,limits(:,2));
%     X_b = [x_b(:); y_b(:); z_b(:)];     XM_b = VectorToMatrix(X_b);

     nds = q/ds;
     if nds ~= floor(nds)
         warning('q is not a multiple of ds!')
         return
     end
     for i = 1:Q+1
        if i-nds <= 0
            x_a(i,1) = x_field(1);
            y_a(i,1) = y_field(1);
            z_a(i,1) = z_field(1);
        else
            x_a(i,1) = x_field(i-nds);
            y_a(i,1) = y_field(i-nds);
            z_a(i,1) = z_field(i-nds);
        end
        if i+nds >= 1
            x_b(i,1) = 1;
            y_b(i,1) = 1;
            z_b(i,1) = 1;
        else
            x_b(i,1) = x_field(i+nds);
            y_b(i,1) = y_field(i+nds);
            z_b(i,1) = z_field(i+nds);
        end
     end
     X_a = [x_a(:); y_a(:); z_a(:)];    XM_a = VectorToMatrix(X_a);
     X_b = [x_b(:); y_b(:); z_b(:)];    XM_b = VectorToMatrix(X_b);

    %{
    % Stokeslet terms:
    S_a = RegStokeslets(X_field, X_a, epsilon) .* kron(dlimits(:,1),ones(3,3*num_field));
    S_b = RegStokeslets(X_field, X_b, epsilon) .* kron(dlimits(:,2),ones(3,3*num_field));

    S_a = ExtractTensorDiagonals(S_a);  S_b = ExtractTensorDiagonals(S_b);

    % Integral term:
    int_full = RegStokesletAnalyticDerivativeIntegrals(X_field, X_midp, T_field, R_midp, ds/2, epsilon);
    int_nn = int_full .* nn;
    int_sum = int_nn * kron(eye(3),ones(Q,1));

    % Compute R_s:
    R_s_2 = S_a + S_b + kron(int_sum, ones(1,num_field));
    %}

    % Term by term... let's get this right:
    nn = NearestNodesAlongArclength(X_field, q);
    N = num_field;
    for n = 1:N
        nn_n = repmat(nn(n,:),3,3);
        S_a = RegStokeslets(XM_field(:,n), XM_a(:,n), epsilon) .* dlimits(n,1);
        S_b = RegStokeslets(XM_field(:,n), XM_b(:,n), epsilon) .* dlimits(n,2);

        int = nn_n.*RegStokesletAnalyticDerivativeIntegrals(XM_field(:,n), X_midp, TM_field(:,n), R_midp, ds/2, epsilon);
        int_sum = int * kron(eye(3),ones(Q,1));

        diff_n = S_b - S_a + int_sum;

        R_s(n,:) = diff_n(1,:);
        R_s(n+N,:) = diff_n(2,:);
        R_s(n+2*N,:) = diff_n(3,:);
    end
end

end
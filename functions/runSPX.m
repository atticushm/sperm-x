function outs = runSPX(int, params, options)

% RUNSPX Simulates a swimming sperm cell using a 3D position
% formulation. Solutions outputted in a struct object OUTS.
%
% Code by Atticus L. Hall-McNair (2021).

% setup log
if ~exist('./logs/','dir')
    mkdir('./logs')
end
dpath = ['./logs/',params.filename,'.txt'];
diary(dpath)
printBanner('    SPERM-X','~')
disp(datetime('now'))

% reset matlab warning counter 
lastwarn('')

% discretisation parameters
dt = params.dt;                     % time step size
NF = params.NF;                     % # of flagellum nodes
NB = params.NBt;                    % # of head traction nodes
NW = params.NW;                     % # of wall nodes
tmax = params.tmax;                 % simulation max time

% print some key parameters
fprintf('dt=%g // NF=%g // calS=%g // calM=[%g,%g,%g]\nh=%g // h0=%g // lambda=%g\n',  ...
    dt, NF, params.calS, params.calM1, params.calM2, params.calM3, params.h,...
    params.h0, params.lambda);

% turn on profiler if enabled in options
prof = options.RunProfiler;
if prof == true
    profile on
    fprintf('MATLAB performance profiling enabled.\n')
end
printLinebreak

%% initialisation

% time interval
t = 0:dt:tmax;
num_t = length(t);

% begin timer
sim_time = tic;

% preallocate some variables
L = zeros(num_t-1,1);
outs{1} = struct('exit','OK');

% initial values for unknowns of the iterative solver
X(:,1) = int.X;             % positions
T(:,1) = zeros(NF,1);       % tension 
U(:,1) = zeros(3*NF,1);     % flagellum velocities
V(:,1) = zeros(3*NF,1);     % nonlocal velocity contribution

f(:,1)   = zeros(3*NF,1);   % viscous force on fluid from flagellum
phi(:,1) = zeros(3*NB,1);   % viscous force on fluid from head
psi(:,1) = zeros(3*NW,1);   % viscous force on fluid from wall

Om(:,1) = zeros(3,1);       % angular velocity of frame at s=0
k3(:,1) = int.k3;           % twist of frame

d1(:,1) = int.d1;           % director frame
d2(:,1) = int.d2;           %       |
d3(:,1) = int.d3;           %       |
d10(:,1) = int.d10;         % director frame at X0
d20(:,1) = int.d20;         %       |
d30(:,1) = int.d30;         %       |

% initial head location
X0 = proxp(X(:,1));
[y(:,1), Y(:,1)] = updateHeadLocation(d10,d20,d30,X0,params,options);

% store variables on GPU, if enabled in options
if strcmp(options.ProcType,'gpu')
    X = gpuArray(X);
    T = gpuArray(T);
    V = gpuArray(V);
    y = gpuArray(y);
    Y = gpuArray(Y);
end

% total number of solves counter
num_evals = 0;

%% time stepping

nn = 1;
for n = 1:num_t
    
    % check filament arclength
    L(n) = calculateArclength(X(:,n));
    
    % update progress bar
    if ~options.SilentMode
        printProgressBar('Time stepping: ',n,num_t,'nodelete',[])
        fprintf('L is %4.4f \n',L(n));
    end
    
    % initial guesses 
    Xi = X(:,n);
    Ti = T(:,n);
    Vi = V(:,n);
    
    Omi  = Om(:,n);
    d10i = d10(:,n);
    k3i  = k3(:,n);

    yi = y(:,n);
    Yi = Y(:,n);
    
    fi = f(:,n);
    phii = phi(:,n);
    psii = psi(:,n);
    
    [fx,fy,fz] = extractComponents(fi);
    [phx,phy,phz] = extractComponents(phii);
    [psx,psy,psz] = extractComponents(psii);
    Zi = [Xi; Ti; fx;phx;psx; fy;phy;psy; fz;phz;psz; Omi; d10i];
    if options.MechTwist
        Zi = [Zi; k3i];
    end

    % start timer
    tstep_tic = tic;
    
    % iterations
    res_X = 1; res_Z = 1; m = 1;
    while (res_X > params.ittol)
        
        % display progress
        Li = calculateArclength(Xi);
        if ~options.SilentMode
            fprintf('  %2g // L = %4.4f', m,Li);
        end
        
        % compute director frame along flagellum
        if (m == 1)
            [d1i,d2i,d3i] = reconstructFrame(Xi, k3i, d10i, params, options);
            d20i = proxp(d2i);    
            d30i = proxp(d3i);
        end
        
        % generate integrator matrices on first iteration of first time step
        if (m == 1) && (n == 1)
            [IXX,IXY,IXW] = buildIntegralOperators(Xi,Yi,params,options);
        end
        
        % build matrix system
        [A, b] = buildLinearSystem(Xi, Ti, Vi, d1i, d2i, k3i, d10i, d20i,   ...
            d30i, Omi, yi, Yi, t(n)+dt, X(:,n), T(:,n), V(:,n), y(:,n), d1(:,n),    ...
            d2(:,n), k3(:,n), d10(:,n), d20(:,n), Om(:,n), f(:,n), phi(:,n),...
            psi(:,n), IXX, IXY, IXW,   ...
            params, options);

        % solve system
        Zim = Zi; Xim = Xi; yim = yi;
        [Zi, Xi, Ti, fi, phii, psii, Omi, d10i, k3i] = solveMatrixSystem(   ...
            A, b, NF, NB, NW, t(n), options);
        
        % update director basis
        [d1i,d2i,d3i] = reconstructFrame(Xi, k3i, d10i, params, options);
        d20i = proxp(d2i);
        d30i = proxp(d3i);
        
        % update head nodes
        % [yi, Yi] = updateHeadLocationRBM(Xi, X(:,n), Omi, yi, Yi, y(:,n),   ...
        %    Y(:,n), params);
        [yi, Yi] = updateHeadLocation(d10i,d20i,d30i,proxp(Xi),params,options);
        
        % update stokeslet matrices
        [IXX, IXY, IXW] = buildIntegralOperators(Xi, Yi, params, options);
        
        % update nonlocal velocity (for use in tension equation)
        if options.NonLocal
            Vi = updateNonlocalVelocity(Xi, fi, phii, psii, IXX, IXY, IXW, options);
        else
            Vi = zeros(3*NF,1);
        end
        
        % check convergence of iterations
        res_Z = max(abs(Zi-Zim));
        res_X = max(abs(Xi-Xim));
        res_y = max(abs(yi-yim));
        if ~options.SilentMode
            fprintf(' // X res = %8.3g , y res = %8.3g , Z res = %8.3g\n',    ...
               res_X, res_y, res_Z);
        end
        
        % move to next iteration
        m = m+1; 
        num_evals = num_evals+1;
        
        % check for nonconvergence
        if (m>=options.MaxIt)
            outs{end}.exit = 'NONCONVERGENCE';
        end
        
        % check for self intersection
        if checkSelfIntersection(Xi)
            outs{end}.exit = 'SELF INTERSECTION';
        end
        
        % if any other warning, return 
        if ~strcmpi(outs{end}.exit, 'OK')
            lastwarn('')
            if options.SaveOnFail
                fprintf('Saving current workspace... ')
                str = ['FAIL_',params.filename,'.mat'];
                save(str,'-v7.3')
                fprintf('saved!\n')
            end
            if strcmpi(outs{end}.exit,'OK')
                outs{end}.exit = 'OTHERFAIL';
            end
            outs = [outs{:}];
            error('%s detected, aborting simulation!', outs(end).exit)
        end
        
    end % iteration

    % record time step time
    tstep_time = toc(tstep_tic);
        
    % update quantities of interest
    Z(:,n+1) = Zi;
    X(:,n+1) = Xi;    
    T(:,n+1) = Ti;   
    V(:,n+1) = Vi; 

    Om(:,n+1) = Omi;
    k3(:,n+1) = k3i;       
    
    f(:,n+1)   = fi;  
    phi(:,n+1) = phii;  
    psi(:,n+1) = psii; 
    
    y(:,n+1)   = yi; 
    Y(:,n+1)   = Yi; 
    
    d1(:,n+1)  = d1i; 
    d2(:,n+1)  = d2i; 
    d3(:,n+1)  = d3i; 
    d10(:,n+1) = d10i;  
    d20(:,n+1) = d20i;   
    d30(:,n+1) = d30i; 
       
    % debug/check variables if enabled in options
    if options.RunDebug
        err = bcDebug(Xi, Yi, Ti, d1i, d2i, k3i, fi, phii, t(n)+dt, params, options);
    else
        err = 'run debug';
    end

    % save outputs
    if mod(n,params.SaveInt)==0
        if options.SilentMode
            printProgressBar('Time stepping: ',n,num_t,'nodelete',[])
        end
        outs{nn} = struct( ...
                'X', X(:,end), 'y', y(:,end), 't', t(n)+dt,                 ...
                'f', f(:,end), 'phi', phi(:,end), 'Om', Om(:,end),          ...
                'k3', k3(:,end), 'd10', d10(:,end), 'd1', d1(:,end),        ...
                'd2', d2(:,end), 'd3', d3(:,end), 'num_evals', num_evals,   ...
                'err', err, 'exit', 'OK', 'tstep_time', tstep_time);
        nn = nn+1;
    end
    
end % time step

% end timer
simtime = toc(sim_time);
stimer = sprintf(['Complete in %g minutes (%g seconds)!\nNumber of total ', ...
    'iterations: %g\nTime at completion is ', ...
    datestr(now)], round(simtime/60), simtime, outs{end}.num_evals);
printBanner(stimer)
    
% save profiler results
if options.RunProfiler
    pdir = ['./profiles/',params.filename];
    profsave(profile('info'), pdir)
    fprintf(['Profiler results saved in ',pdir])
    profile off
end

% close diary
diary off

% concatenate
outs = [outs{:}];

%% video 

if options.SaveVideo 
    fprintf('Video requested...\n');
    
    % check folder exists
    if ~exist('./videos','dir')
        mkdir('./videos')
    end
    
    % generate frames
    frames = framesFromOutputs(outs, params, options);
    vpath  = ['./videos/',params.filename];  
    
    % save movie
    saveMovie(frames,12,vpath); 
    printLinebreak
end

end % function
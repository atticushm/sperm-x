function [V,fm,Phi,Rot] = SolveFullHydrodynamics(X, UX, nn, tt, U, Om, y, Y, params)

%SOLVEFULLHYDRODYNAMICS solves the hydrodynamics problem at each iteration for
%the unknown force denisities fm and Phi, and returns the nonlocal contribution to
%the node velocities in V.
%This function, unlike SOLVEHYDRODYNAMICS, includes the presence of the
%cell head in the nonlocal integral equations.
%
%See also REGSTOKESLETANALYTICINTEGRALS, buildTensorOp, TNBFRAME

% discretisation parameters:
N = params.NF;
Q = N-1;

% modelling parameters:
calS4 = params.calS4;
gamma = params.gamma;
q = params.q;
epsilon = params.epsilon;
c_perp = params.c_perp;
icp = 1/c_perp;
Nt = length(y)/3;
Nq = length(Y)/3;

% additional useful shorthands:
ds = 1/Q;

% head-flagellum join:
X0 = proxp(X);

% nearest-neighbour matrix between head nodes:
NN = params.NN;

%% flagellum-flagellum interactions:
% i.e. hydrodynamic effects of flagellum on itself.

% arclength discretisation:
s = linspace(0,1,N);

% arclength midpoint values:
sm = 0.5*(s(1:end-1)+s(2:end));

% arclength midpoint distances:
dxi = abs(sm-sm');

% tangents at segment midpoints:
nnm = Midpoints(nn);
ttm = Midpoints(tt);

% curve midpoints:
Xm = Midpoints(X);

% rotation matrices for each midpoint:
Rot = RotationMatrix(Xm, [1;0;0]);

% stokeslet interactions:
SXX = RegStokesletAnalyticIntegrals(Xm,Xm,ds/2,Rot,epsilon);

% inner region resistance operator onto fm:
nn_dyadic = buildTensorOp(nnm);
tt_dyadic = buildTensorOp(ttm);
FF_inner_LGL = icp*(eye(3*Q) +(gamma-1)*tt_dyadic);
% FF_inner_LGL = icp*(nn_dyadic + tt_dyadic);

% 'furthest neighbours' matrix for implementing limits of outer region
% integral operator:
fn = zeros(Q);
fn(dxi>q) = 1;
fn_rep = repmat(fn,3,3);
FF_outer_int = SXX.*fn_rep;

% flag-flag interactions operator:
AFF = -FF_inner_LGL -FF_outer_int;
% AFF = -SXX;

%% head-flagellum interactions:
% i.e. hydrodynamic effects of head acting on flagellum.

%{x
% stokeslet matrix:
SXY = RegStokeslets(Xm,Y,epsilon);

% head-flag interactions operator:
AFH = -SXY*NN;
%}

% AFH = -RegStokeslets(Xm,y,epsilon);

%% flagellum-head interactions:
% i.e. hydrodynamic effects of flagellum onto the head.

% stokeslet matrix:
SYX = RegStokesletAnalyticIntegrals(y,Xm,ds/2,Rot,epsilon);
% SYX = RegStokeslets(y,Xm,epsilon);

% flag-head interactions operator:
AHF = -SYX;

%% head-head interactions:
% i.e. hydrodynamic effects of head on itself.

%{x
% stokelset matrix:
SYY = RegStokeslets(y, Y, epsilon);

% head-head interactions operator:
AHH = -SYY*NN;
%}

% AHH = -RegStokeslets(y,y,epsilon);

%% build right hand side:

% flagellum velocities at midpoints:
UXm = Midpoints(UX);

% head velocities via y_t = U+Om^(y-X0):
UYt = kron(U, ones(Nt,1));
dyM = VectorToMatrix(y)-X0;
[o1,o2,o3] = extractComponents(Om);
cpM = [0,-o3,o2;o3,0,-o1;-o2,o1,0]*dyM;
UYr = MatrixToVector(cpM);

UY = UYt + UYr;

% right hand side of flagellar velocity equation:
bF = calS4*UXm;
[bFx,bFy,bFz] = extractComponents(bF);

% right hand side of head velocity equation:
bH = calS4*UY;
[bHx,bHy,bHz] = extractComponents(bH);

% right hand side vector - head forces first:
b = [bH; bF];
% b = [bHx; bFx; bHy; bFy; bHz; bFz];

%% build matrix and solve system of equations:

% build matrix:
AH = combineMatrices(AHH,AHF);
AF = combineMatrices(AFH,AFF);
A = [AH; AF];

%A = MergeMatrices(AHH,AFH,AHF,AFF);

%{
% improve conditioning (precautionary measure):
[A_eq, b_eq, C] = ImproveConditioning(A, b);

% solve system:
Z_eq = A_eq\b_eq;
Z = C*Z_eq;
%}

Z = A\b;

% extract fm and Phi solutions:
ZM = VectorToMatrix(Z);
Phi = MatrixToVector(ZM(:,1:Nt));
fm = MatrixToVector(ZM(:,Nt+1:end));

% outer and nonlocal velocity contributions:
Vm = -FF_outer_int*fm -AFH*Phi;
% Vm = UXm + FF_inner_LGL*fm;

% spline to obtain contributions at nodes:
[Vm1,Vm2,Vm3] = extractComponents(Vm);
V1 = spline(sm,Vm1,s);   
V2 = spline(sm,Vm2,s);     
V3 = spline(sm,Vm3,s);
V = [V1(:); V2(:); V3(:)];

end % function
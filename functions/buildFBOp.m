function [Fx,Fy,Fz] = buildFBOp(params)

% constructs the operator in each x,y,z direction for inclusion in the
% proximal EHD boundary conditions, generating FH by multiplying onto the
% unknown forces phi.

% nearest neighbour matrix between head discretisations
NN = params.NNB;

% numbers of nodes
NF = params.NF;
NB = params.NBt;
NW = params.NW;

% zero row vectors
zeNF = zeros(1,NF);
zeNB = zeros(1,NB);
zeNW = zeros(1,NW);

% build operator onto forces
if ~isempty(NN)
    [nnx,nny,nnz] = extractComponents(sum(NN));
    Fx = [zeNF,nnx,zeNW,  zeNF,zeNB,zeNW, zeNF,zeNB,zeNW];
    Fy = [zeNF,zeNB,zeNW, zeNF,nny,zeNW,  zeNF,zeNB,zeNW];
    Fz = [zeNF,zeNB,zeNW, zeNF,zeNB,zeNW, zeNF,nnz,zeNW];
else
    Fx = repmat([zeNF,zeNB,zeNW],1,3);
    Fy = Fx;
    Fz = Fx;
end

end % function
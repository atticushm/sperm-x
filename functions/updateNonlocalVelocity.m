function V = updateNonlocalVelocity(X, f, phi, psi, IXX, IXY, IXW, options)

% contribution from flagellum
flag_contrib = IXX*f;

% contribution from head
head_contrib = IXY*phi;

% contribution from wall
wall_contrib = IXW*psi;

% combined nonlocal contribution
V = flag_contrib + head_contrib + wall_contrib;

% force planarity?
if options.ForcePlanar
    NF = length(f)/3;
    switch options.PlaneOfBeat
        case 'xy'
            V(2*NF+1:3*NF) = zeros(NF,1);
        case 'xz'
            V(NF+1:2*NF) = zeros(NF,1);
    end
end

end % function
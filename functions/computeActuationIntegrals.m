function M = computeActuationIntegrals(m1, m2, m3, d1, d2, d3, t, params)

% computes the integral
%   M = int_0^1 {m1*d1 + m2*d2 + m3*d3} ds
% where m_i are the scalar values of the active torque acting in each d1,
% d2 and d3 direction respectively.

% compute distal integrals
I = computeActiveMomentDistalIntegrals(d1, d2, d3, m1, m2, m3, params);

% total moment integral is first vector entry of I
M = proxp(I);

%{
% number of nodes
N = length(m1);

% grid spacing
ds = 1/(N-1);

% modelling parameters
Gamma = params.Gamma;
calM1 = params.calM1;
calM2 = params.calM2;
calM3 = params.calM3;
calS4 = params.calS4;

%% m1 * d1 contribution

% components of d1
[d1x,d1y,d1z] = extractComponents(d1);

% components of argument
ax = m1.*d1x(:);
ay = m1.*d1y(:);
az = m1.*d1z(:);

% integrals of components by midpoint rule
I1x = sum(ds/2 * (ax(1:end-1) + ax(2:end)));
I1y = sum(ds/2 * (ay(1:end-1) + ay(2:end)));
I1z = sum(ds/2 * (az(1:end-1) + az(2:end)));

% vector integral
I1 = calS4 * calM1 * [I1x(:); I1y(:); I1z(:)];

%% m2 * d2 contribution

% components of d2
[d2x,d2y,d2z] = extractComponents(d2);

% components of argument
ax = m2.*d2x(:);
ay = m2.*d2y(:);
az = m2.*d2z(:);

% integrals of components by midpoint rule
I2x = sum(ds/2 * (ax(1:end-1) + ax(2:end)));
I2y = sum(ds/2 * (ay(1:end-1) + ay(2:end)));
I2z = sum(ds/2 * (az(1:end-1) + az(2:end)));

% vector integral
I2 = calS4 * calM2 * [I2x(:); I2y(:); I2z(:)];

%% m3 * d3 contribution

% components of d1
[d3x,d3y,d3z] = extractComponents(d3);

% components of argument
ax = m3.*d3x(:);
ay = m3.*d3y(:);
az = m3.*d3z(:);

% integrals of components by midpoint rule
I3x = sum(ds/2 * (ax(1:end-1) + ax(2:end)));
I3y = sum(ds/2 * (ay(1:end-1) + ay(2:end)));
I3z = sum(ds/2 * (az(1:end-1) + az(2:end)));

% vector integral
I3 = calS4 * calM3 * [I3x(:); I3y(:); I3z(:)];

%% combine contributions

M = I1+I2+I3;
%}

end % function
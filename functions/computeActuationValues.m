function [m1,m2,m3,m1_s,m2_s,m3_s] = computeActuationValues(t, params, options)

% computes values for the active moments per unit length driving the sperm
% flagellum.
% m1 is rotation about d1 (d2-d3 plane)
% m2 is rotation about d2 (d1-d3 plane)
% m3 is rotation about d3 (d1-d2 plane)

%% compute m1, m2, m3 depending on actuation model

switch options.ActModel
    case 'trig'
        % simple trig travelling wave
        [m1,m2,m3] = actTrig(t, params);

    case 'square'
        % square wave
        delta = 1e-4;
        [m1,m2,m3] = actSquare(t, delta, params);

    case 'smthsquare'
        % smoothed square wave
        delta = 1e-2;
        [m1,m2,m3] = actSquare(t, delta, params);

    case 'mixed'
        % smoothed square bending wave and trig twist wave
        delta = 1e-2;
        [m1,m2,~] = actSquare(t, delta, params);
        [~,~,m3]  = actTrig(t, params);
        
    otherwise
        warning('Unrecognised actuation type!')

end

%% enforce end piece model

ell = params.ell;
s = linspace(0,1,params.NF)';
if ell>1e-6
    switch options.EndPieceModel
        case 'heaviside'
            % convolve with Heaviside function
            H = (s<=(1-ell));
            m1 = m1.*H;
            m2 = m2.*H;
            m3 = m3.*H;

        case 'tanh'
            % convolve with tanh function
            smp = 500;                  % smoothing parameter
            ctf = 1.1;                  % cutoff tuning
            tnh = -1/2*(tanh(smp*s-smp*(1-ctf*ell))-1);
            m1 = m1.*tnh;
            m2 = m2.*tnh;
            m3 = m3.*tnh;

        case 'csaps'
            % convolve with Heaviside, then smooth with csaps
            H = (s<=(1-ell));
            Hs = csaps(s,H,[],s);
            m1 = m1.*Hs;
            m2 = m2.*Hs;
            m3 = m3.*Hs;

        otherwise
            warning('Unrecognised end piece model!')
            
    end

end

%% differentiate m1, m2, m3

D = finiteDifferences(linspace(0,1,params.NF), 1);
m1_s = D{1}*m1;
m2_s = D{1}*m2;
m3_s = D{1}*m3;

end % function 
function [m1,m2,m3] = actSquare(t, delta, params)

% actuation function from a travelling square wave
% delta is smoothing parameter for the square wave veritices

% parameters
NF = params.NF;
k_bend = params.k_bend;
k_twist = params.k_twist;
om_bend = params.om_bend;
om_twist = params.om_twist;

% arclength
s = linspace(0,1,NF)';

% corrected function to match trig function amplitude
amp = 1;
s_func = @(x,t,k,om,del) (amp/atan(1/del)) * atan(sin(k*x-om*t)/del); 
c_func = @(x,t,k,om,del) (amp/atan(1/del)) * atan(cos(k*x-om*t)/del); 

% square wave bending wave
m1 =  s_func(s, t, k_bend, om_bend, delta);
m2 =  c_func(s, t, k_bend, om_bend, delta);
m3 = -c_func(s, t, k_twist, om_twist, delta);

end
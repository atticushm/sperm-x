function [A, b] = buildHydrodynamicsCN(X, Y, y, Xn, yn, t, IXX, IXY, IXW,  ...
    fn, phin, psin, Omn, params, options)    

% discretisation parameters
NF = params.NF;             % # of flagellar nodes
NB = params.NBt;            % # of body nodes
NW = params.NW;             % # of wall nodes
dt = params.dt;             % time step

% modelling parameters
eps   = params.epsilon;     % reg parameter for head/tail interactions
weps  = params.wepsilon;    % reg parameter for wall interactions
calS4 = params.calS4;       % swimming parameter
cperp = params.c_perp;      % resistance coefficient
gamma = params.gamma;       % resistance ratio

% build location
proc = options.ProcType;

% wall nodes
xw = params.xw;
Xw = params.Xw;

% nearest neighbour matrices for surface integrals
NNB = params.NNB;
NNW = params.NNW;

% difference matrices
s = linspace(0,1,NF);
D = finiteDifferences(s, 1);

% tangents to flagellar curve at nodes
d3 = normalise(kron(eye(3),D{1}) * X);
d3n = normalise(kron(eye(3),D{1}) * Xn);

% coefficient from crank-nicolson
if (t-dt < dt)
    coeff = dt;
else
    coeff = dt/2;
end

% define persistent variables used to store operators from previous time
% step
persistent t_val IXXn IXYn IXWn IYXn IYYn IYWn 
if isempty(t_val)
    t_val = t;
end

%% flagellum forces

% terms onto unknown positions
AFX = calS4 * eye(3*NF);

% local operator onto force per unit length f
AXX_L = -coeff/cperp * (eye(3*(NF)) + (gamma-1)*tensorProd(d3));

% nonlocal operator onto f
AXX_NL = -coeff * IXX;

% operator onto force per unit lengths is then 
AXX = AXX_L + AXX_NL;

% operators onto head & wall forces
AXY = -coeff * IXY;
AXW = -coeff * IXW;

%% head forces

% operator onto unknown positions
op  = [ones(NB,1),zeros(NB,NF-1)];
AHX = calS4 * blkdiag(op,op,op);

% operator onto unknown angular velocity
X0 = proxp(X);
dy = y-kron(X0,ones(NB,1)); 
[dy1,dy2,dy3] = extractComponents(dy); ze  = 0*dy1;
AHO = -coeff * calS4 * [ze,-dy3,dy2; dy3,ze,-dy1; -dy2,dy1,ze];

% stokeslet/blakelet operators onto forces
if options.UseBlakelets
    IYX = regBlakeletNumericalIntegrals(y,X,eps);
    IYY = regBlakelet(y,Y,eps)*NNB;
    IYW = regBlakelet(y,Xw,weps)*NNW;
else
    IYX = regStokesletNumericalIntegrals(y,X,eps,proc);
    IYY = regStokeslet(y,Y,eps,proc)*NNB;
    IYW = regStokeslet(y,Xw,weps,proc)*NNW;
end
AYX = -coeff * IYX;
AYY = -coeff * IYY;
AYW = -coeff * IYW;

%% wall forces 

if options.UseBlakelets
    IWX = regBlakeletNumericalIntegrals(xw,X,eps);
    IWY = regBlakelet(xw,Y,eps)*NNB;
    IWW = regBlakelet(xw,Xw,weps)*NNW;    
else
    IWX = regStokesletNumericalIntegrals(xw,X,eps,proc);
    IWY = regStokeslet(xw,Y,eps,proc)*NNB;
    IWW = regStokeslet(xw,Xw,weps,proc)*NNW;
end
AWX = -coeff * IWX;
AWY = -coeff * IWY;
AWW = -coeff * IWW;

%% store current stokeslet matrices for next time step

% will trigger rewrite when time step changes (ie not on iteration)
if (t ~= t_val)
    t_val = t;
    IXXn  = IXX;
    IXYn  = IXY;
    IXWn  = IXW;
    IYXn  = IYX;
    IYYn  = IYY;
    IYWn  = IYW;
end

%% right hand sides

% from flagellum equation
bF = calS4 * Xn; 
if (t > dt)
    CN = 1/cperp*(eye(3*NF)+(gamma-1)*tensorProd(d3n)) * fn + ...
         IXXn*fn + IXYn*phin + IXWn*psin;
    bF = bF + dt/2 * CN;
end

% from head equation
bH = calS4 * kron(proxp(Xn),ones(NB,1));
if (t > dt)
    X0n = proxp(Xn);
    dyn = yn-kron(X0n,ones(NB,1)); 
    [dy1,dy2,dy3] = extractComponents(dyn); ze  = 0*dy1;
    CN = calS4*[ze,-dy3,dy2; dy3,ze,-dy1; -dy2,dy1,ze]*Omn + ...
         IYXn*fn + IYYn*phin + IYWn*psin;
    bH = bH + dt/2 * CN;
end

% from wall equation
bW = zeros(3*NW,1);
% !!! not updated for crank-nicolson yet !!!

%% build linear system 

% build matrix
A = [AFX, zeros(3*NF,NF), combineMatrices(AXX,AXY,AXW), zeros(3*NF,6),    ; ...
     AHX, zeros(3*NB,NF), combineMatrices(AYX,AYY,AYW), AHO, zeros(3*NB,3); ...
     zeros(3*NW,4*NF),    combineMatrices(AWX,AWY,AWW), zeros(3*NW,6),    ];

% augment if twist enabled
if options.MechTwist
    A = [A, zeros(3*NF+3*NB+3*NW, NF)];
end
 
% build right hand side
b = [bF; bH; bW];

end % function
function [y,Y,X0,ax] = generateBodyNodes(H_trac, H_quad, species, type, varargin)

switch species
    case 'human'
        switch type
            case 'cw1'      % via Cummins & Woodall (1985)
                L  = 4.7+47.13;
                ax = [6.11/2; 3.45/2; 3/2]/L;
            case 'cw2'
                L  = 4+38;
                ax = [4.5/2; 3.2/2; 3/2]/L;
            case 'cw3'
                L  = 6+50;
                ax = [4.5/2; 3/2; 3/2]/L;
            case 'gskb1'    % via Gaffney, Smith & Kirkman-Brown (2011)
                L  = 45;
                ax = [2;1;1.6]/L;
            case 'gskb2'  
                L  = 45;
                ax = [2;0.5;1.6]/L;
            case 'gskb3'  
                L  = 45;
                ax = [2;0.25;1.6]/L;
            case 'tdmj'     % via Montenegro-Johnson (2015)
                ax = [0.05;0.03;0.04];
            case 'sph1'     % spherical-type heads
                L  = 40;
                ax = [1.5;1.5;1.5]/L;
            case 'sph2'
                L  = 50;
                ax = [1.5;1.5;1.5]/L;
            case 'sph3'
                L  = 60;
                ax = [1.5;1.5;1.5]/L;
            case 'prl1'     % prolate-spheroidal heads
                % e ~ 0.9682
                L  = 45;
                a  = 2;
                c  = 0.5;
                ax = [a;c;c]/L;
            case 'prl2'
                % e ~ 0.866
                L  = 45;
                a  = 1;
                c  = 0.5;
                ax = [a;c;c]/L;
            case 'prl3'
                % e ~ 0.6614
                L  = 45;
                a  = 1;
                c  = 0.75;
                ax = [a;c;c]/L;
            case 'sunanda'  % via sunanda et al (2018)
                L = 54;
                ax = [2.3, 1.08, 1.3]/L;
            case 'none'
                ax = [0;0;0];
            case 'custom'
                % custom axes input
                ax = varargin{1};
        end
        
        % generate sphere discretisations
        sphere_trac = generateSphereDisc(H_trac, 1);
        sphere_quad = generateSphereDisc(H_quad, 1);
        
        % remove X0 if present
        sphere_trac = removeNode(sphere_trac, [1;0;0]);
        sphere_quad = removeNode(sphere_quad, [1;0;0]);
        
        % scale by axes
        num_trac = length(sphere_trac)/3;
        num_quad = length(sphere_quad)/3;
        y        = kron(ax(:),ones(num_trac,1)).*sphere_trac;
        Y        = kron(ax(:),ones(num_quad,1)).*sphere_quad;
        
        % join location in body coordinates
        X0 = [ax(1);0;0];
        
    case 'mouse'
        
        % H_trac and H_quad in this case represent the number of nodes in
        % each discretisation
        % generate head discretisation
        L = 45;
        [y,Y,X0] = generateMouseBodyNodes(H_trac, H_quad, L, type);
        
        % remove node close to X0
        y = removeNode(y, X0);
        Y = removeNode(Y, X0);
        
end

end % function
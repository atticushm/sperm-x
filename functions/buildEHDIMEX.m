function [A, b] = BuildEHDIMEX(X, Y, T, d1, d2, Xn, t, SXX, SXY, SXW, params, ...
    options, d10)
           
% X         - flagellar nodes
% Y         - cell body quadrature nodes
% T         - tension values
% d1        - material normals
% Xn        - nodes at current time step
% xw        - wall traction nodes
% Xw        - wall quadrature nodes
% act_next  - actuation values at t+dt
% params    - struct containing other model parameters

% extract parameters from model:
NF = params.NF;
NB = params.NBt;
NW = params.NW;

dt = params.dt;

calS4   = params.calS4;
gamma   = params.gamma;
cp      = params.c_perp;

proc    = options.ProcType;

% stiffness values:
[E,E_s,E_ss] = ComputeStiffness(d1, d2, options);

% arclength discretisation:
s = linspace(0,1,NF);

% generate finite difference schemes:
D = finiteDifferences(s, 1:4);

% build finite difference operators:
D1 = kron(eye(3),D{1});  
D2 = kron(eye(3),D{2}); 
D3 = kron(eye(3),D{3});
D4 = kron(eye(3),D{4}); 

% approximate derivatives to curve:
X_s = D1*X; X_s = Normalise(X_s);
X_ss = D2*X;

% extract components of derivatives
[x1_s,x2_s,x3_s] = extractComponents(X_s);
[x1_ss,x2_ss,x3_ss] = extractComponents(X_ss);

% compute actuation values:
[m1,m2] = ComputeActuationValues(t, params, options);
M = 0; 

% derivatives of actuation
m1_s = D1(1:NF,1:NF) * m1;
m2_s = D1(1:NF,1:NF) * m2;

% derivative of frame
d1_s = D1*d1;
d2_s = D1*d2;

%% construct matrix

% see TEST_NonlinearOperatorFunctions for verification
%  ~ operators onto unknown position
% cn  = dt/2/cp;                        % coeffieicnt for CN method
cn  = dt/cp;                            % coefficient for BE method
XX1 = calS4*eye(3*NF);
XX2 = cn * E.*D4; 
XX3 = cn * (gamma-1)*buildTensorOp(X_s)*(E.*D4); 
XX4 = cn * 2*E_s.*D3;                             
XX5 =-cn * 2*(gamma-1)*buildTensorOp(X_s,X_ss)*(E_s.*D2);   
XX6 = cn * E_ss.*D2;                                             
XX  = XX1+XX2+XX3+XX4+XX5+XX6;

% ~ operators onto forces (to compute nonlocal velocity contribution)
% contribution from flagellum
Xq = splineFlagellum(X);
NNF = NearestNeighbourMatrix(Xq,X,proc);

dxi = abs(s-s');
FN  = zeros(NF);
FN(dxi > params.q) = 1;
FN_rep = repmat(FN,3,3);
XF = dt * (FN_rep.*(SXX*NNF));

% contribution from head
NNB = params.NNB;
XH  = dt * SXY*NNB;

% contribution from wall
NNW = params.NNW;
XW  = dt * SXW*NNW;

% ~ operators onto unknown tension
XT1 = -cn*[diag(x1_ss);diag(x2_ss);diag(x3_ss)];
d_s = D1(1:NF,1:NF);
XT2 = -cn*gamma*[diag(x1_s)*d_s;diag(x2_s)*d_s;diag(x3_s)*d_s];
XT  = XT1+XT2;

% matrix to multiply onto [X;T;f;phi;psi;om0,d10]
A = [XX, XT, combineMatrices(XF,XH,XW), zeros(3*NF,6)];

%% construct right hand side

% ~~ backward euler ~~
b1 = calS4*Xn;
b2 = zeros(3*NF,1); 

b3 = cn * (repmat(m1_s,3,1).*d1 + gamma*repmat(m1,3,1).*d1_s);
b4 = cn * (repmat(m2_s,3,1).*d2 + gamma*repmat(m2,3,1).*d2_s);

b  = b1+b2+b3+b4;

%% apply boundary conditions

% matrix representation for easier boundary condtion implementation
b_mat = VectorToMatrix(b);

% zeros vectors 
zeNF  = zeros(1,NF); 
ze3   = zeros(1,3);  
ze3NF = zeros(1,3*NF); 
ze3NB = zeros(1,3*NB); 
ze3NW = zeros(1,3*NW);

% stiffness at X0
E0      = [E(1);E(NF+1);E(2*NF+1)];
E0_s    = [E_s(1);E_s(NF+1);E_s(2*NF+1)];

% stiffness at XL
EL      = [E(NF);E(2*NF);E(3*NF)];
EL_s    = [E_s(NF);E_s(2*NF);E_s(3*NF)];

% ~~ with FH and MH as operators onto f, phi ~~
% proximal force bc:
X0_s  = proxp(X_s);
d20   = proxp(d2);

d3x = E0(1)*D3(1,1:NF);
d3y = E0(2)*D3(1,1:NF);
d3z = E0(3)*D3(1,1:NF);

d2x = E0_s(1)*D2(1,1:NF);
d2y = E0_s(2)*D2(1,1:NF);
d2z = E0_s(3)*D2(1,1:NF);

[FHx,FHy,FHz] = ConstructHeadForceOperator(params);

Tds = 0;   %T(1)*D1(1,1:N);
Tx = zeNF; %[X0_s(1), zeros(1,N-1)];
Ty = zeNF; %[X0_s(2), zeros(1,N-1)];
Tz = zeNF; %[X0_s(3), zeros(1,N-1)];

A(1,:)      = [-d3x-d2x+Tds, zeNF, zeNF, Tx, FHx, ze3, 0,0,0]; 
A(NF+1,:)   = [zeNF, -d3y-d2y+Tds, zeNF, Ty, FHy, ze3, 0,0,0];
A(2*NF+1,:) = [zeNF, zeNF, -d3z-d2z+Tds, Tz, FHz, ze3, 0,0,0];
b_mat(:,1)  = zeros(3,1) - T(1)*X0_s + m1(1)*d10 + m2(1)*d20;

% distal force bc:
d3x = EL(1)*D3(NF,1:NF);
d3y = EL(2)*D3(NF,1:NF);
d3z = EL(3)*D3(NF,1:NF);

d2x = EL_s(1)*D2(NF,1:NF);
d2y = EL_s(2)*D2(NF,1:NF);
d2z = EL_s(3)*D2(NF,1:NF);

XL_s  = distp(X_s);
d1L   = distp(d1);
d2L   = distp(d2);

Tds = 0;    %T(N)*D1(N,1:N);
Tx  = zeNF; %[XL_s(1), zeros(1,N-1)];
Ty  = zeNF; %[XL_s(2), zeros(1,N-1)];
Tz  = zeNF; %[XL_s(3), zeros(1,N-1)];

A(NF,:)     = [d3x+d2x-Tds, zeNF, zeNF, -Tx, ze3NF, ze3NB, ze3NW, ze3, ze3]; 
A(2*NF,:)   = [zeNF, d3y+d2y-Tds, zeNF, -Ty, ze3NF, ze3NB, ze3NW, ze3, ze3];
A(3*NF,:)   = [zeNF, zeNF, d3z+d2z-Tds, -Tz, ze3NF, ze3NB, ze3NW, ze3, ze3];
b_mat(:,NF) = -m1(NF)*d1L -m2(NF)*d2L + T(NF)*XL_s;

% promixal moment bc:
d2x = E0(1)*D2(1,1:NF);
d2y = E0(2)*D2(1,1:NF);
d2z = E0(3)*D2(1,1:NF);

[MHx,MHy,MHz] = ConstructHeadMomentOperator(X, Y, X_s, params);

A(2,:)      = [d2x, zeNF, zeNF, zeNF, MHx, ze3, 0,0,0]; 
A(NF+2,:)   = [zeNF, d2y, zeNF, zeNF, MHy, ze3, 0,0,0];
A(2*NF+2,:) = [zeNF, zeNF, d2z, zeNF, MHz, ze3, 0,0,0];
b_mat(:,2)  = M*d10; % zeros(3,1);

% distal moment bc:
d2x = EL(1)*D2(NF,1:NF);
d2y = EL(2)*D2(NF,1:NF);
d2z = EL(3)*D2(NF,1:NF);

A(NF-1,:)     = [d2x, zeNF, zeNF, zeNF, ze3NF, ze3NB, ze3NW, ze3, ze3];
A(2*NF-1,:)   = [zeNF, d2y, zeNF, zeNF, ze3NF, ze3NB, ze3NW, ze3, ze3];
A(3*NF-1,:)   = [zeNF, zeNF, d2z, zeNF, ze3NF, ze3NB, ze3NW, ze3, ze3];
b_mat(:,NF-1) = zeros(3,1);

% convert b_mat back to vector:
b = MatrixToVector(b_mat);

end % function
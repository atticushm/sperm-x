function [y,Y,X0] = GenerateMouseBodyNodes(N_trac, N_quad, L, mousetype)

% code mostly by Gallagher, which generates a mouse cell head lying in the
% xy plane and orientated pointing in the positve y direction.

% Sperm-X code requires head to point in the negative x direction, so we
% rotate around designated X0 joint.

nBeats = 15;

%% generate head discretisation

% Length of sperm in um
L0 = L*10^(-6);

% Reguarlisation parameter
epsilon = 0.25e-6/L0;

% Initialise sperm
k = 2*pi;
phi = 0;
args{1} = struct('phase',phi,'k',k);

% nT = 100;
% nH = 100;
% discr = [nT,nH,nT*4,nH*4];
discr = [N_trac, N_trac, N_quad, N_quad];

bodySize = [NaN,NaN,NaN]; % Not used

bodyFrame{1} = RotationMatrix(0,3);

x0{1} = [0;0;0];

MouseFnc = sprintf('%sMouseSperm',mousetype);

if ~exist('mouseSwimmer.mat','file')
    swimmer = SwimmersSperm(1,str2func(MouseFnc), ...
        @WaveSpermDKAct,bodySize,discr,nBeats, ...
        x0,bodyFrame,args);
    
    save('../lib/data/mouseSwimmer.mat','swimmer');
    
else
    load('mouseSwimmer.mat','swimmer');
    swimmer{1}.fn = @MouseSperm;
    swimmer{1}.x0 = x0{1};
    swimmer{1}.b10 = bodyFrame{1}(:,1);
    swimmer{1}.b20 = bodyFrame{1}(:,2);
    swimmer{1}.model.ns = discr(1);
    swimmer{1}.model.nh = discr(2);
    swimmer{1}.model.NS = discr(3);
    swimmer{1}.model.Nh = discr(4);
    swimmer{1}.model.a1 = bodySize(1);
    swimmer{1}.model.a2 = bodySize(2);
    swimmer{1}.model.a3 = bodySize(3);
end

% Initialise sperm head
swimmer{1}.model = GenerateMouseHead(mousetype, ...
    swimmer{1}.model,epsilon,L0);

% extract nodes
y = swimmer{1}.model.xh;
Y = swimmer{1}.model.Xh;

% joint location in body coordinates
X0 = swimmer{1}.model.f0;

end % function
function [A, b] = buildKineTwist(X, Y, V, k3n, d1, d2, t, params, options)

% encodes the 'new' kinematics equations for SPX, relating k3 twist
% curvature, m3 active twist moment per unit length, and the centerline X.

% discretistation parameters
NF = params.NF;
NB = params.NBt;
NW = params.NW;
dt = params.dt;

% extract modelling parameters
calS4 = params.calS^4;
Gam_d = params.Gam_d;
Gam_s = params.Gam_s;
calM3 = params.calM3;
cperp = params.c_perp;

% generate finite difference operators
s = linspace(0,1,NF);
D = finiteDifferences(s, 1:2);
D1 = kron(eye(3),D{1});
D2 = kron(eye(3),D{2});

% compute derivatives
X_s = D1 * X;
X_ss = D2 * X;
V_s = D1 * V;

% compute actuation values and integral
[~, ~, ~, ~, ~, m3_s] = computeActuationValues(t, params, options);
[~, ~, mint] = computeActuationVector(t, d1, d2, X_s, params, options);

%% build matrix

% operators onto k3
kOp1 = calS4/dt * eye(NF);
kOp2 = -Gam_d * Gam_s * D{2};
kOp  = kOp1 + kOp2;

% operator onto flagellar forces
fOp = -1/cperp * dotDerivativeOperator(crossProdOp(X_s)*X_ss, D1);

% split by components, to multiply onto flag, body, and wall forces
FOp = [fOp(:,1:NF), zeros(NF,NB+NW), ...
       fOp(:,NF+1:2*NF), zeros(NF,NB+NW), ...
       fOp(:,2*NF+1:3*NF), zeros(NF,NB+NW)];

% build matrix
A = [zeros(NF,4*NF), FOp, zeros(NF,6), kOp];

%% build right hand side

b1 = calS4/dt * k3n;                            % from time finite difference
b2 = calS4 * calM3 * Gam_d * m3_s(:);           % active twist term
b3 = dotProduct(crossProdOp(X_s)*X_ss, V_s);    % nonlocal contribution

% build rhs
b = b1 + b2 + b3;

%% apply boundary conditions

% operator for proximal boundary condition M.Xs onto forces
MDot = buildMBDotOp(X, Y, proxp(X), X_s, params);

% insert proximal conditions
A(1,:) = [zeros(1,4*NF), MDot, zeros(1,6), -Gam_s, zeros(1,NF-1)]; 
b(1)   = -dot(mint, proxp(X_s));

% insert distal conditions
A(NF,:) = [zeros(1,4*NF+3*NB+3*NF+3*NW+6), zeros(1,NF-1), 1]; 
b(NF)   = 0;

end % function
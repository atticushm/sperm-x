function [A,b] = buildLinearSystem(X, T, V, d1, d2, k3, d10, d20, d30, Om,  ...
    y, Y, t, Xn, Tn, Vn, yn, d1n, d2n, k3n, d10n, d20n, Omn, fn, phin, psin,    ...
    IXX, IXY, IXW,     ...
    params, options)

% this function builds the monolithic SPX linear system of equations.

% linearised ehd equation and rhs
[AE, bE] = buildEHD(X, Y, T, V, d1, d2, IXX, IXY, IXW, Xn, k3, t, params, options);

% linearised tension auxillary equation and rhs
[AT, bT] = buildTension(X, V, d1, d2, k3, t, params, options);

% linearised hydrodynamics equation and rhs
[AH, bH] = buildHydrodynamics(X, Y, y, Xn, IXX, IXY, IXW, params, options);

% new kinematic equations for Om, d10, and k3
[AKO, bKO] = buildKineRot(X, Xn, d1, d2, d2n, t, params, options);
[AKD, bKD] = buildKineDir(d10, d10n, Om, params, options);
if options.MechTwist
    [AKT, bKT] = buildKineTwist(X, Y, V, k3n, d1, d2, t, params, options);
end

% left hand side
if options.MechTwist
    A = [AE; AT; AH; AKO; AKD; AKT];
else
    A = [AE; AT; AH; AKO; AKD];
end

% right hand side
if options.MechTwist
    b = [bE; bT; bH; bKO; bKD; bKT];
else
    b = [bE; bT; bH; bKO; bKD];
end

% make sparse if building on cpu
if strcmp(options.ProcType,'cpu')
    A = sparse(A);
end

end % function
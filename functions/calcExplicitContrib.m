function b = calcExplicitContrib(X, T, V, k3, d1, d2, t, params, options)

% discretisation parameters
NF = params.NF;

% modelling parameters
cperp = params.c_perp;
gamma = params.gamma;
Gamma = params.Gamma;

% bending stiffness values
[E,E_s,E_ss] = computeStiffness(d1, d2, options);

% arclength discretisation:
s = linspace(0,1,NF);

% generate finite difference schemes:
D = finiteDifferences(s, 1:4);

% build finite difference operators:
D1 = kron(eye(3),D{1});  
D2 = kron(eye(3),D{2}); 
D3 = kron(eye(3),D{3});
D4 = kron(eye(3),D{4}); 

% approximate derivatives to curve:
X_s = normalise(D1 * X);
X_ss = D2 * X;
X_sss = D3 * X;
X_ssss = D4 * X;

% extract components of tangents
[x1_s,x2_s,x3_s] = extractComponents(X_s);

% compute actuation values
[mvec, mvec_s, ~] = computeActuationVector(t, d1, d2, X_s, params, options);

% derivatives of twist
k3_s = D{1}*k3;

% derivatives of tension
T_s = D{1}*T;

%% bending terms

b1 = -E_ss.*X_ss;
b2 = -2*E_s.*X_sss;
b3 = -E.*X_ssss;
b4 = -2*(gamma-1)*E_s.*repmat(dotProduct(X_s,X_sss),3,1).*X_s;
b5 = -(gamma-1)*E.*repmat(dotProduct(X_s,X_ssss),3,1).*X_s;

bend_sum = b1+b2+b3+b4+b5;

%% tension terms

t1 = gamma*repmat(T_s,3,1).*X_s;
t2 = repmat(T,3,1).*X_ss;

ten_sum = t1+t2;

%% twist terms

if options.MechTwist
    zeN   = zeros(NF);
    XsDss = [zeN, -x3_s(:).*D{2}, x2_s(:).*D{2}; ...
             x3_s(:).*D{2}, zeN, -x1_s(:).*D{2}; ...
             -x2_s(:).*D{2}, x1_s(:).*D{2}, zeN];

    XsDsss = [zeN, -x3_s(:).*D{3}, x2_s(:).*D{3}; ...
              x3_s(:).*D{3}, zeN, -x1_s(:).*D{3}; ...
              -x2_s(:).*D{3}, x1_s(:).*D{3}, zeN];

    c1 = Gamma*repmat(k3_s,3,1).*(XsDss*X);
    c2 = Gamma*repmat(k3,3,1).*(XsDsss*X);

    twi_sum = c1+c2;
else
    twi_sum = zeros(3*NF,1);
end

%% actuation terms

% active bending terms
a1 = crossProdOp(X_ss)* mvec;
a2 = crossProdOp(X_s)* mvec_s;
a3 = (gamma-1)*tensorProd(X_s)*(crossProdOp(X_ss)*mvec);

act_sum = a1+a2+a3;

%% combine

b = V + 1/cperp*(bend_sum+ten_sum+act_sum+twi_sum);

end % function
function [A, b] = buildKineRot(X, Xn, d1, d2, d2n, t, params, options)

% encodes the kinematic relation between Om(0) and the director frame at
% the head-flagellum join.

% discretistation parameters
NF = params.NF;
NB = params.NBt;
NW = params.NW;
dt = params.dt;

% extract modelling parameters
calS4 = params.calS^4;
Gam_d = params.Gam_d;
Gam_s = params.Gam_s;
calM3 = params.calM3;

% generate finite difference operators
s = linspace(0,1,NF);
D = finiteDifferences(s, 1);
D1 = kron(eye(3),D{1});

% compute derivatives
X_s = D1 * X;
Xn_s = D1 * Xn;

% compute actuation values and integral
[~, ~, m3, ~, ~, ~] = computeActuationValues(t, params, options);

%% build matrix

% % operator onto Om
% OmOp = calS4 * eye(3);
% 
% % operator onto k3
% k3Op = -Gam_s * Gam_d * proxp(X_s) .* repmat(D1(1,1:NF), 3, 1);
% 
% % operators onto X
% ddo = dotDerivativeOperator((d2-d2n)/dt, D1);
% XOp1 = -calS4 * proxp(d1).*repmat(ddo(1,:),3,1);
% ddo = dotDerivativeOperator(d1, D1/dt);
% XOp2 = -calS4 * proxp(d2).*repmat(ddo(1,:),3,1);
% XOp = XOp1 + XOp2; 
% 
% % build matrix
% A = [XOp, zeros(3,4*NF+3*NB+3*NW), OmOp, zeros(3), k3Op];

% operator onto Om 
OmOp = calS4 * eye(3);

% operator onto X
d_s = D{1}(1,1:NF);
ze = 0*d_s;
X0_s = proxp(X_s);
XOp = -calS4/dt * [ze,-X0_s(3)*d_s,X0_s(2)*d_s; ...
                   X0_s(3)*d_s,ze,-X0_s(1)*d_s; ...
                   -X0_s(2)*d_s,X0_s(1)*d_s,ze];

% operator onto k3
k3Op = -Gam_s * Gam_d * X0_s.*repmat(d_s,3,1);

% build matrix
if options.MechTwist
    A = [XOp, zeros(3,4*NF+3*NB+3*NW), OmOp, zeros(3), k3Op];
else
    A = [XOp, zeros(3,4*NF+3*NB+3*NW), OmOp, zeros(3)];
end

%% build right hand side

% b1 = calS4 * calM3 * Gam_d * m3(1) * proxp(X_s);            % active twist term
% b2 = calS4 * proxp(d2).*dot(proxp(-Xn_s)/dt, proxp(d1));    % from Om2 term
% b  = b1 + b2;

b1 = -calS4/dt * proxp(crossProdOp(X_s)*Xn_s);       % from passive untwist
b2 = calS4 * calM3 * Gam_d * m3(1) * X0_s;          % active twist term
b  = b1 + b2;

end % function
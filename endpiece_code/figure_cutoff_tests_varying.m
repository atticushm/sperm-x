clear all; close all; clc;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
fldName  = 'cutoff-tests-varying';   % folder for saving data
alpha    = 0.01;                     % where m0 = alpha*K/calS
lvec     = linspace(0,1,61);         % range of cutoff values
lvec     = lvec(31:end);             % range to calculate over
quadDis  = 16;                        % quadrature discretisation parameter
tracDis  = 8;                        % traction discretisation parameter
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% add function paths
addpath(genpath('./'));

figure;

%% calS = 18

% load data
[Y18(:,:,:,1),VAL18(:,:,1),W18(:,:,1),model] = GetSimDataCutoffTests(18,3*pi,...
    alpha,lvec,60,quadDis,tracDis,'varying',fldName);
[Y18(:,:,:,2),VAL18(:,:,2),W18(:,:,2),model] = GetSimDataCutoffTests(18,4*pi,...
    alpha,lvec,60,quadDis,tracDis,'varying',fldName);
[Y18(:,:,:,3),VAL18(:,:,3),W18(:,:,3),model] = GetSimDataCutoffTests(18,5*pi,...
    alpha,lvec,60,quadDis,tracDis,'varying',fldName);

% plot VAL
subplot(3,3,1);
title('$\mathcal{S}=18$','Interpreter','latex');
plotVAL(VAL18,lvec);

% plot efficiency
subplot(3,3,2);
title('$\mathcal{S}=18$','Interpreter','latex');
plotEfficiency(VAL18,W18,lvec);

% plot waveforms
subplot(3,3,3);
plotWaveforms(VAL18,Y18,model);

%% calS = 13.5

% load data
[Y13pt5(:,:,:,1),VAL13pt5(:,:,1),W13pt5(:,:,1),model] = GetSimDataCutoffTests(...
    13.5,3*pi,alpha,lvec,60,quadDis,tracDis,'varying',fldName);
[Y13pt5(:,:,:,2),VAL13pt5(:,:,2),W13pt5(:,:,2),model] = GetSimDataCutoffTests(...
    13.5,4*pi,alpha,lvec,60,quadDis,tracDis,'varying',fldName);
[Y13pt5(:,:,:,3),VAL13pt5(:,:,3),W13pt5(:,:,3),model] = GetSimDataCutoffTests(...
    13.5,5*pi,alpha,lvec,60,quadDis,tracDis,'varying',fldName);

% plot VAL
subplot(3,3,4);
title('$\mathcal{S}=13.5$','Interpreter','latex');
plotVAL(VAL13pt5,lvec);

% plot efficiency
subplot(3,3,5);
title('$\mathcal{S}=13.5$','Interpreter','latex');
plotEfficiency(VAL13pt5,W13pt5,lvec);

% plot waveforms
subplot(3,3,6);
plotWaveforms(VAL13pt5,Y13pt5,model);

%% calS = 9

% load data
[Y9(:,:,:,1),VAL9(:,:,1),W9(:,:,1),model] = GetSimDataCutoffTests(9,3*pi,...
    alpha,lvec,60,quadDis,tracDis,'varying',fldName);
[Y9(:,:,:,2),VAL9(:,:,2),W9(:,:,2),model] = GetSimDataCutoffTests(9,4*pi,...
    alpha,lvec,60,quadDis,tracDis,'varying',fldName);
[Y9(:,:,:,3),VAL9(:,:,3),W9(:,:,3),model] = GetSimDataCutoffTests(9,5*pi,...
    alpha,lvec,60,quadDis,tracDis,'varying',fldName);

% plot VAL
subplot(3,3,7);
title('$\mathcal{S}=9$','Interpreter','latex');
plotVAL(VAL9,lvec);

% plot efficiency
subplot(3,3,8);
title('$\mathcal{S}=9$','Interpreter','latex');
plotEfficiency(VAL9,W9,lvec);

% plot waveforms
subplot(3,3,9);
plotWaveforms(VAL9,Y9,model);


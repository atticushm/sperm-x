function runConvergenceTestsQ(qvec,fldName)

close all; clc;

fprintf('%%%% Running Q convergence tests %%%%\n\n');

for ii=1:length(qvec)
    
    % add function paths
    addpath(genpath('./'));
    
    % create swimming structure
    model = CreateModelStructure(13.5,4*pi,0.02,...
        qvec(ii),16,8,4*pi,'varying',1,[]);
    
    % generate initial condition
    Y0     = InitCondPresolve(model,0);
    
    % solve problem
    output = SolveSwimmingProblem(model.tmax,Y0,model,0);
    
    % evaluate solution
    Y      = deval(output.sol,output.tps);
    
    % save workspace
    SaveWorkspace(Y,output,model,fldName);
    
    fprintf(['Simulation ',num2str(ii),' out of ',num2str(length(qvec)),...
        ' complete.\n']);
    
end

fprintf('\nTests complete.\n');

end
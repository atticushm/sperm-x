clear all; close all; clc;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
fldName  = 'cutoff-tests-varying';      % folder for saving data
alpha    = 0.01;                        % where m0 = alpha*K/calS
lvec     = linspace(0,1,61);            % range of cutoff values
lvec     = lvec(31:end);                % range to calculate over
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% add function paths
addpath(genpath('./'));

% calS = 13.5 tests
runCutoffSimulations(13.5,3*pi,alpha,'varying',lvec,fldName);
runCutoffSimulations(13.5,4*pi,alpha,'varying',lvec,fldName);
runCutoffSimulations(13.5,5*pi,alpha,'varying',lvec,fldName);

% calS = 9 tests
% runCutoffSimulations(9,3*pi,alpha,'varying',lvec,fldName);
% runCutoffSimulations(9,4*pi,alpha,'varying',lvec,fldName);
runCutoffSimulations(9,5*pi,alpha,'varying',lvec,fldName);

% calS = 18 tests
runCutoffSimulations(18,3*pi,alpha,'varying',lvec,fldName);
runCutoffSimulations(18,4*pi,alpha,'varying',lvec,fldName);
runCutoffSimulations(18,5*pi,alpha,'varying',lvec,fldName);

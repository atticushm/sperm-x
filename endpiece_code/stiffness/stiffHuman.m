function E = stiffHuman(model,s)

% unpack parameters
Q  = model.disc.Q;
Ed = model.swimmer.Ed;
Ep = model.swimmer.Ep;
sd = model.swimmer.sd;
L  = model.swimmer.L;
Len = model.swimmer.Len;

% scale evaluation points
s  = s*L/Len;
sd = sd;

% calculate stiffness
for jj=1:length(s)
    if (s(jj)>sd)
        E(jj) = Ed;
    else
        E(jj) = (Ep-Ed)*((s(jj)-sd)/(sd))^2+Ed;
    end
end

% non-dimensionlise
E = E/Ed;

end

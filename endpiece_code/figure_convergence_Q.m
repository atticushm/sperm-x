clear all; clc;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
fldName  = 'Q-convergence';          % folder for saving data
alpha    = 0.01;                     % where m0 = alpha*K/calS
qvec     = [10,20,30,40,50,60,70,80,90,100,110,120,130,140,140,160,170,...
    180,190,200];                    % vector of Q values
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% add function paths
addpath(genpath('./'));

%% load data
for ii=1:length(qvec)
    
    % generate filename
    fn =  ['data/',char(fldName),'/calS=13.5_k=12.5664_m0=0.02_l=1_Q=',...
        num2str(qvec(ii)),'_NQuad=1536_NTrac=384_varying.mat'];
    load(fn);
    
    % find flagellum data at final timestep
    flagellum{ii} = flag;
    
end

%% calculate sum of differences in each case

% 10 vs 20
for ii=1:200
    diff     = [flagellum{1}(ii).x,flagellum{1}(ii).y] - ...
        [flagellum{2}(ii).x(1:2:end),flagellum{2}(ii).y(1:2:end)];
    diffnorm = vecnorm(diff,2,2).^2;
    err1(ii) = sqrt(1/10*sum(diffnorm));
end

% 20 vs 40
for ii=1:200
    diff     = [flagellum{2}(ii).x,flagellum{2}(ii).y] - ...
        [flagellum{4}(ii).x(1:2:end),flagellum{4}(ii).y(1:2:end)];
    diffnorm = vecnorm(diff,2,2).^2;
    err2(ii) = sqrt(1/20*sum(diffnorm));
end

% 40 vs 80
for ii=1:200
    diff     = [flagellum{4}(ii).x,flagellum{4}(ii).y] - ...
        [flagellum{8}(ii).x(1:2:end),flagellum{8}(ii).y(1:2:end)];
    diffnorm = vecnorm(diff,2,2).^2;
    err3(ii) = sqrt(1/40*sum(diffnorm));
end

% 80 vs 160
for ii=1:200
    diff     = [flagellum{8}(ii).x,flagellum{8}(ii).y] - ...
        [flagellum{16}(ii).x(1:2:end),flagellum{16}(ii).y(1:2:end)];
    diffnorm = vecnorm(diff,2,2).^2;
    err4(ii) = sqrt(1/80*sum(diffnorm));
end

%% plot figure

figure;
title('TH');
hold on;
plot(linspace(0,4*pi,200),err1,'LineWidth',1.5);
plot(linspace(0,4*pi,200),err2,'LineWidth',1.5);
plot(linspace(0,4*pi,200),err3,'LineWidth',1.5);
plot(linspace(0,4*pi,200),err4,'LineWidth',1.5);
hold off;

axis square;
box on;
xlabel('$t$','Interpreter','latex');
xlim([0,2*pi]);
xticks([0,pi,2*pi]);
xticklabels({'0','\pi','2\pi'});
ylabel('RMSD','Interpreter','latex');
ylim([3e-4,10e-1]);
set(gca, 'YScale', 'log');
set(gca,'FontSize',14);
lgd = legend('10 v 20','20 v 40','40 v 80','80 v 160','Location','north');
lgd.NumColumns = 2;

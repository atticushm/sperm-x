function [Y,VAL,W,output,model,simStat] = runCutoffSimulations(calS,k,alpha,type,lvec,varargin)

close all; warning off;

fprintf(['%%%% Cutoff test for ',type,' stiffness \nwith calS=',...
    num2str(calS),' k=',num2str(k),' %%%%\n\n']);

% create model structure
model = CreateModelStructure(calS,k,alpha,60,4,2,2*pi,type,1,[]);

% update m0 so that m0 = alpha*k/calS (varying) or m0 = alpha*k^2/calS^4
% (constant)
switch type
    case 'varying'
        m0               = alpha*(18/calS)*(k/(3*pi));
        model.swimmer.m0 = m0;
    case 'constant'
        m0               = alpha*(18/calS)^4*(k/(3*pi))^2;
        model.swimmer.m0 = m0;
end

% error code for error tracking
errCode = 0;

% generate initial condition based on l=1 (entirely active flagellum)
try
    Y0 = InitCondPresolve(model,0);
catch
    
    % update error code if initial condition could not be calculated
    fprintf('Calculation of initial condition failed. Exiting code...\n');
    errCode = 1;
end

if errCode == 0
    
    % initial condition was calculated successfully - start running tests
    if length(lvec) == 1
        
        % if there is only one l value don't use a parfor loop
        [Y,VAL,W,output,model,simStat] = CutoffSingleSim(Y0,calS,k,...
            m0,type,lvec,varargin);
    else
        
        % loop through all l values
        parfor ii=1:length(lvec)
            [Y(:,:,ii),VAL(ii),W(ii),output(ii),model(ii),simStat(ii)] = ...
                CutoffSingleSim(Y0,calS,k,m0,type,lvec(ii),varargin);
        end
    end
else
    
    % initial condition calculation failed - set variables to empty and
    % exit
    Y = [];
    VAL = [];
    W = [];
    output = [];
    simStat = 0;
end

end

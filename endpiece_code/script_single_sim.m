clear all; clc;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
fldName  = 'single-sim';
saveVid  = 1;
saveData = 1;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% add function paths
addpath(genpath('./'));

L  = 5; % dimensionless length of flagellum
ep = 0.05; % reg stokeslet parameter
mu = 2; % fluid viscosity
% k  = 4*pi;
% calS = 13.5;
% m0 = 0.04;
k  = 4*pi;
calS = 9;
m0 = 0.07;

% create swimming structure
model = CreateModelStructure(calS,k,m0,40,4,2,8*pi,'varying',0.95,[],L,ep,mu);

% print swimming parameters
PrintSwimmingParameters(model);

% generate initial condition
Y0     = InitCondPresolve(model,1);

% solve problem
output = SolveSwimmingProblem(model.tmax,Y0,model,1);

% evaluate solution
Y      = deval(output.sol,output.tps);

% calculate video frames if requested
if (saveVid == 1)
    
    frames = GetFrames(Y,output,model);
    
    % play video
    PlayVideo(frames,20);
    
    % save video
%     SaveVideo(frames,20,model);
    
end

% save workspace if requested
if (saveData == 1)
%     SaveWorkspace(Y,output,model,fldName);
end
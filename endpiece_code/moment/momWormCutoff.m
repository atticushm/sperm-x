function [m] = momWormCutoff(t,model,s)

% unpack structure
k   = model.swimmer.k;
l   = model.swimmer.l;
L   = model.swimmer.Len;

% for each sm calculate the analytical integral of the active moment term 
% from sm to 1
for ii=1:length(s)
    if(s(ii)<l)
        m(ii) = 1/k*(sin(k*l-t) - sin(k*s(ii)-t));
    else
        m(ii) = 0;
    end
end
m = m';

end
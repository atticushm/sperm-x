function [MB] = ConstructMomentBalance(head,flag,model,NN)

% unpack structure
ds    = flag.ds;
Q     = model.disc.Q;
NTrac = head.NTrac;
NQuad = head.NQuad;

% calculate distances between head/flagellum points and head-flagellum joint
DFx   = ds*(flag.xm - flag.x(1))';
DFy   = ds*(flag.ym - flag.y(1))';

for m=1:NTrac
    DHx(1,m)   = (head.xQuad - flag.x(1))' * NN(1:NQuad,m);
    DHy(1,m)   = (head.yQuad - flag.y(1))' * NN(1:NQuad,m);
end

% form total moment balance row
MB    = [ -DHy -DFy DHx DFx zeros(1,NTrac+Q)];

end
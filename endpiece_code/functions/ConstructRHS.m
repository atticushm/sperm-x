function [b] = ConstructRHS(t,head,flag,model)

% unpack structure
Q     = model.disc.Q;
ang   = model.swimmer.ang;
calS  = model.swimmer.calS;
m0    = model.swimmer.m0;
NTrac = head.NTrac;
ds    = flag.ds;
th    = flag.th;
TH    = head.TH;

% calculate active moment
m     = model.swimmer.momFunc(t,model,flag.s);

% calculate RHS terms
switch model.swimmer.stiffness
    case 'varying'
        
        % calculate stiffness function
        E   = model.swimmer.stiffFunc(model,flag.s);
        
        mom1 = (th(1)-TH)/ds - ((calS^4*m0)./E(1))'.*m(1);
        mom  = diff(th(:)+ang)/ds - ((calS^4*m0)./E(2:end-1))'.*m(2:end-1);  
        
    case 'constant'
        mom1 = (th(1)-TH)/ds - calS^4*m0*m(1);
        mom  = diff(th(:)+ang)/ds - calS^4*m0*m(2:end-1);
end

% construct full RHS vector
b = [0; mom1; mom(:); zeros(3*Q+3*NTrac+2,1)];

end
function [Y0] = InitCondPresolve(model,indComplete)

% reduce discretisation for initial condition presolve
model.disc.quad = 4;
model.disc.trac = 2;

% generate naive initial condition
Y0 = InitCond(model);

% % solve swimming problem over one beat for an improved initial condition
% output = SolveSwimmingProblem(2*pi,Y0,model,0);
% Y0     = deval(output.sol,2*pi);
% 
% % form initial condition
% if isempty(model.swimmer.TH)
%     Y0      = [model.swimmer.x0(1); model.swimmer.x0(2); Y0(4); Y0(4:end)];
% else
%     Y0      = [model.swimmer.x0(1); model.swimmer.x0(2); model.swimmer.TH; Y0(4:end)];
% end
% 
% if (indComplete == 1)
%     fprintf('Initial condition presolve complete.\n');
% end

end
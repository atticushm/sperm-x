function [Y0] = InitCond(model)

% unpack structure
k  = model.swimmer.k;
Q  = model.disc.Q;
x0 = model.swimmer.x0;
L  = model.swimmer.Len;

% generate refined initial condition
x  = linspace(0,L,1e6);
x  = x(:);
Nx = length(x);
y  = (1/k)*(sin(k*L)*ones(Nx,1) - sin(k*x));

% spline to get correct number of segments
ds      = sqrt((x(2:end) - x(1:end-1)).^2 + (y(2:end) - y(1:end-1)).^2);
s       = [0; cumsum(ds)]/L;
delta   = max(s)/Q;
ss      = 0:delta:max(s);

yy      = spline(s,y,ss);
xx      = spline(s,x,ss);

% calculate segment theta values
dy      = diff(yy);
dx      = diff(xx);
th      = atan(dy./dx);

% form initial condition
if isempty(model.swimmer.TH)
    Y0      = [x0(1); x0(2); th(1); th(:)];
else
    Y0      = [x0(1); x0(2); model.swimmer.TH; th(:)];
end

% x0 = model.swimmer.x0;
% L  = model.swimmer.Len;

% figure;
% % get coordinate data
% [head,flag,~] = GetProblemData(Y0,model);
% 
% scatter3(head.xTrac(:),head.yTrac(:),head.zTrac(:),'k.');
% plot3(flag.x(:),flag.y(:),flag.z(:),'k-','LineWidth',1.5);
% 
% hold off;
% axis('equal');
% xlabel('x');
% ylabel('y');
% view(2);
% 
% axis([x0(1)-L/2 x0(1)+L/2+L x0(2)-L/2 x0(2)+L/2]);

end
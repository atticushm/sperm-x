function model = CreateModelStructure(calS,k,m0,Q,quadDis,tracDis,tmax,type, ...
    l,TH,Len,ep,mu,Z_int,ax)

% input checks


% create structure
model = struct( ...
    'epsilon', ep,              ... % regularisation parameter
    'tmax'   , tmax,              ... % maximum time to simulate to
    'nt'     , 100 ,              ... % numver of time points
    'disc'   ,                    ... % discretisation parameters
        struct( ...
        'quad', quadDis ,         ... % quadrature discretisation
        'trac', tracDis ,         ... % traction discretisation
        'Q'   , Q                 ... % number of segments
        ),                        ...
    'swimmer',                    ... % swimmer parameters
        struct( ...
        'x0'       , Z_int    , ... % initial head-flagellum point
        'a1'       , ax(1)*Len     , ... % head axis length 1
        'a2'       , ax(2)*Len     , ... % head axis length 2
        'a3'       , ax(3)*Len     , ... % head axis length 3
        'ang'      , 0          , ... % cell orientation angle
        'stiffness', type       , ... % stiffness type
        'momFunc'  , @momWormCutoff, ... % active moment function
        'stiffFunc', @stiffHuman, ... % stiffness function
        'calS'     , calS/Len       , ... % dimensionless swimming group
        'L'        , 6e-5       , ... % flagellum length
        'Ed'       , 2.2e-21    , ... % distal bending stiffness
        'Ep'       , 8e-21      , ... % proximal bending stiffness
        'sd'       , 3.9e-5     , ... % length of distal flagellum
        'm0'       , m0*Len^2         , ... % active moment strength
        'k'        , k/Len          , ... % active moment 
        'TH'       , TH         , ... % head orientation 
        'Len'      , Len        , ...
        'mu'       , mu         , ...
        'l'        , l*Len            ... % cutoff for actuation
        )                         ...
    );

end
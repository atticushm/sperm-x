function [Y,VAL,W,output,model,simStat] = CutoffSingleSim(Y0,calS,k,m0,type,l,varargin)

% create model structure
model = CreateModelStructure(calS,k,m0,60,16,8,8*pi,type,l,[]);

try
    % solve problem
    output = SolveSwimmingProblem(model.tmax,Y0,model,0);
    
    % evaluate solution
    Y = deval(output.sol,output.tps);
    
    % calculate VAL
    VAL = CalculateVAL(Y,output.tps);
    
    % calculate work done over one period
    W = WorkDoneOverPeriod(output.tps,Y,model);
    
    % set simulation status to successful
    simStat = 1;
    
    fprintf(['Simulation complete for l=',num2str(l),'.\n']);
    
catch
    
    % set simulation status to failed
    simStat = 0;
    Y       = 0;
    VAL     = 0;
    W       = 0;
    
    fprintf(['Simulation failed for l=',num2str(l),'.\n']);
end

% save data
fn = GenerateFilename(model);

if (isempty(varargin) == 1)
    fnData = ['data/',fn,'.mat'];
else
    if ~exist(['data/',char(varargin{1})], 'dir')
        mkdir(['data/',char(varargin{1})]);
    end
    fnData =  ['data/',char(varargin{1}),'/',fn,'.mat'];
end
save(fnData,'Y','output','model','VAL','W','simStat');

end

function SaveVideo(frames,fps,model,varargin)

% generate filename
fn = GenerateFilename(model);

if (isempty(varargin) == 1)
    fnVideo = ['videos/',fn];
else
    if ~exist(['videos/',char(varargin{1})], 'dir')
        mkdir(['videos/',char(varargin{1})]);
    end
    fnVideo =  ['videos/',char(varargin{1}),'/',fn];
end

currentDir      = cd;

% write video
vid             = VideoWriter(fnVideo,'Motion JPEG AVI');
vid.FrameRate   = fps;
vid.Quality     = 100;
open(vid);
writeVideo(vid,frames.data);
close(vid);
fprintf('Video saved.\n')
cd(currentDir);

end

function SaveWorkspace(Y,output,model,varargin)

for t = 1:model.nt
    % get coordinate data
    [head(t),flag(t),~] = GetProblemData(Y(:,t),model);   
end

% generate filename
fn = GenerateFilename(model);

if (isempty(varargin) == 1)
    fnData = ['data/',fn,'.mat'];
else
    if ~exist(['data/',char(varargin{1})], 'dir')
        mkdir(['data/',char(varargin{1})]);
    end
    fnData =  ['data/',char(varargin{1}),'/',fn,'.mat'];
end

% save workspace
save(fnData,'Y','output','model','head','flag');

end
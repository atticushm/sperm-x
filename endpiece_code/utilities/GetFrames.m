function frames = GetFrames(Y,model)

figure('visible','off'); box on;

% unpack stucture
x0 = model.swimmer.x0;
L  = model.swimmer.Len;

% time points
tps = linspace(0,model.tmax,model.nt);

% get frames
for t = 1:model.nt
    clf;
    hold on;
    
    % get coordinate data
    [head,flag,~] = GetProblemData(Y(:,t),model);
    
    scatter3(3.1+head.xTrac(:),5+head.yTrac(:),5+head.zTrac(:),'k.');
    plot3(3.1+flag.x(:),5+flag.y(:),5+flag.z(:),'k-','LineWidth',1.5);
    
    hold off;
    title(sprintf('t = %g',tps));
    axis('equal');
    xlabel('x');
    ylabel('y');
    view(2);
    
    mesh.Lx = [0,10];
    mesh.Ly = [0,10];
    mesh.Lz = [0,10];
     axis([mesh.Lx(1) mesh.Lx(2) mesh.Ly(1) mesh.Ly(2) mesh.Lz(1) mesh.Lz(2)])
%     axis([x0(1)-L/2 x0(1)+L/2+L x0(2)-L/2 x0(2)+L/2]);
    
    % save frame data
    frame           = getframe(gcf);
    frames.data(t)  = frame;
    
end

fprintf('Frames calculated.\n');

end
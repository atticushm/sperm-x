function plotVAL(VAL,lvec)

numk = size(VAL,3);

hold on;
for ii=1:numk
    plot(lvec,VAL(:,:,ii),'LineWidth',1.5);
end
hold off;

axis square; 
box on; 
xlabel('$\ell$','Interpreter','latex');
ylabel('VAL','Interpreter','latex');
set(gca,'FontSize',14);
xlim([0.5,1]);
ylim([0,8.4e-3])
legend;

end
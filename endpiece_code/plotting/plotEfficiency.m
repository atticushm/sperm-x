function plotEfficiency(VAL,W,lvec)

numk = size(VAL,3);

hold on;
for ii=1:numk
    plot(lvec,VAL(:,:,ii).^2./W(:,:,ii),'LineWidth',1.5);
end
hold off;

axis square; 
box on; 
xlim([0.5,1]);
xlabel('$\ell$','Interpreter','latex');
ylabel('$\eta$','Interpreter','latex');
ylim([0,3e-4])
set(gca,'FontSize',14);

end
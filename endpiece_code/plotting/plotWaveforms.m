function plotWaveforms(VAL,Y,model)

numk = size(VAL,3);

lvec = linspace(0,1,61);
lvec = lvec(31:end);

hold on;
for ii=1:numk
    [~,indmax]     = max(VAL(:,:,ii)); indmax = max(indmax);
    
    cutoff = ceil(lvec(indmax)*model.disc.Q);
     
    [head,flag,NN] = GetProblemData(Y(:,1,indmax,ii),model);
    plot(Y(1,:,indmax,ii),Y(2,:,indmax,ii)-(ii-2)*0.3,'Color','m')
    plot(flag.x(1:cutoff),flag.y(1:cutoff)-(ii-2)*0.3,'LineWidth',1.5,'Color',[0 0.4470 0.7410]);
    plot(flag.x(cutoff:end),flag.y(cutoff:end)-(ii-2)*0.3,'LineWidth',1.5,'Color','k');
    scatter(head.xQuad,head.yQuad-(ii-2)*0.3,'.','MarkerEdgeColor',[0 0.4470 0.7410]);
    txt = ['$k=',num2str(ii+2),'\pi$'];
    text(1,0.3*(2-ii),txt,'Interpreter','latex','FontSize',14);
end
hold off;

axis off; axis equal;

end
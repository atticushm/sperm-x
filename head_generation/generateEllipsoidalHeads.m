function [x, x0, ax, vol] = generateEllipsoidalHeads(num, H)

% generate a series of realistic ellipsoidal human sperm heads 
% in the first instance, volume and 1 of each of the 3 head axes are fixed.
% suitable heads are obtained by varying the other axes 

num = floor(sqrt(num/3));

%% compute axes assuming fixed volume
% using meshgrid means that heads generated are indexed in a way that
% promotes easy comparison when pcoloring etc.

% meshgrids can be obtained from final vector using
%   reshape(a_ax, num, num, []) etc

% mean flagellum+midpiece length from Cummins 1985
L1 = 4.7+47.13;
L2 = 4.18+56.16;
L3 = 4+48;
L  = mean([L1,L2,L3]);

% ideal length, width and topographical height from Sunanda et al 2018
a1_ideal = 4.6/L;
a2_ideal = 2.6/L;
a3_ideal = 1.03/L;   

% ideal radii and volume
r1_ideal = a1_ideal/2;
r2_ideal = a2_ideal/2;
r3_ideal = a3_ideal;
V = 4/3*pi*r1_ideal*r2_ideal*r3_ideal;

% allowable growth of each radius
perc    = 10;
r1_grow = perc/100 * r1_ideal;
r2_grow = perc/100 * r2_ideal;
r3_grow = perc/100 * r3_ideal;

% `fixed' values of each axis 
r1_min = r1_ideal-r1_grow; 
r1_max = r1_ideal+r1_grow; 
r1_fix = linspace(r1_min, r1_max, num);

r2_min = r2_ideal-r2_grow; 
r2_max = r2_ideal+r2_grow; 
r2_fix = linspace(r2_min, r2_max, num);

r3_min = r3_ideal-r3_grow; 
r3_max = r3_ideal+r3_grow;
r3_fix = linspace(r3_min, r3_max, num);

% check that short axes are never longer than longer axes
if (r3_max<r2_min) && (r2_max<r1_min)
    % all good!
else
    warning('Major/minor axes have swapped! Consider rescaling...')
    return
end

% (a) choose r1 & r2, compute r3 for fixed volume
[a_xx, a_yy] = meshgrid(r1_fix,r2_fix);
a_zz = 3*V/4/pi./a_xx./a_yy;

% (b) choose r1 & r3, compute r2
[b_xx, b_zz] = meshgrid(r1_fix,r3_fix);
b_yy = 3*V/4/pi./b_xx./b_zz;

% (c) choose r2 & r3 ,compute r1
[c_yy, c_zz] = meshgrid(r2_fix,r3_fix);
c_xx = 3*V/4/pi./c_yy./c_zz;

%% generate head discretisations

% generate spherical discretisations
x_sph = generateSphereDisc(H, 1);
N     = length(x_sph)/3;

% list axes
a_ax = [a_xx(:); a_yy(:); a_zz(:)];
b_ax = [b_xx(:); b_yy(:); b_zz(:)];
c_ax = [c_xx(:); c_yy(:); c_zz(:)];

Ax = [a_xx(:); b_xx(:); c_xx(:); ...
      a_yy(:); b_yy(:); c_yy(:); ...
      a_zz(:); b_zz(:); c_zz(:)];
  
ax_mat = mat(Ax);

% scale appropriately
num_ax = 3*num^2;
for j = 1:num_ax
    x{j}   = kron(ax_mat(:,j),ones(N,1)).*x_sph;
    ax{j}  = ax_mat(:,j);
    x0{j}  = [ax{j}(1); 0; 0];
    vol{j} = 4/3*pi*ax{j}(1)*ax{j}(2)*ax{j}(3);
end

end % function
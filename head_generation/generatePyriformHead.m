function [x,x0,dim,vol] = generatePyriformHead(H)

% parameters of conical frustrum
h   = 3.5/45;           % height 
r1  = 0;                % radius at base
r2  = 2.8/2/45;         % radius at z=h
Dr  = 8;                % slice parameter along height
Dth = 15;               % slice parameter about axis
ax  = [r2; r2; r2];   % radius of spherical tip

% generate spherical discretisation
x_sph = generateSphereDisc(H, 1);

% generate conical frustrum discretisation
x_cone = generateConicalFrustrumDisc(h, r1, r2, Dr, Dth);

% generate semispherical discretisation
tol        = 1e-5;
x_sph_mat  = mat(x_sph);
x_semi_mat = x_sph_mat(:,x_sph_mat(1,:)<tol);

num_semi  = size(x_semi_mat,2);
x_semi    = kron(ax, ones(num_semi,1)) .* vec(x_semi_mat);

% connect cone and semisphere
x_semi = x_semi-kron([h;0;0], ones(num_semi,1));

x  = [mat(x_semi), mat(x_cone)];
x0 = [0;0;0];

% remove any nodes that might be too close together
tol  = 1e-6;
x = uniquetol(vec(x)',tol,'ByRows',true);

% remove X0
x = removeNode(x,[0;0;0]);

% `squash' in a3 direction
N     = length(x)/3;
scale = [1;1;0.25];
x = kron(scale, ones(N,1)).*x;

% compute volume of semicirclular cap
vol_semi = 4/3*pi*r2^3;
vol_cone = pi*r2*(scale(3)*r2)*h/3;
vol      = vol_semi+vol_cone;

% specify dimensions of head
dim(1,1) = h+r2;          % length
dim(2,1) = r2;            % width
dim(3,1) = scale(3)*r2;   % depth

end % function
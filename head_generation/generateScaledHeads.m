function [x, x0, Ax, vol, nums] = generateScaledHeads(H, genPyri)

% generate a series of sperm heads, with size varied by prescribing changes
% in the volume whilst keeping the ratio of the axes the same

% healthy ellipsoidal cells, round cells, tapered, and pyriform cells are
% considered.
% genPyri is a logical input to enable/disable generation of pyriform type
% cell heads.

% mean flagellum+midpiece length from Cummins 1985
L1 = 4.7+47.13;
L2 = 4.18+56.16;
L3 = 4+48;
L  = mean([L1,L2,L3]);

% define volume multipliers, 1 corresponding to mean volume/average sized
% cell head i.e. 0.5 is half healthy volume, 2 is double healthy volume etc
V_scale = 0.5:0.1:2;
num = length(V_scale);

% scale for length from volume scaling
length_scale = V_scale.^(1/3);

%% healthy ellipsoidal cell

% mean radii values from sunanda et al 2018
a1_av = 4.6/2/L;
a2_av = 2.6/2/L;
a3_av = 1.03/L;

% compute axes corresponding to scaled volume
for i = 1:num
    ax(i,1) = length_scale(i)*a1_av;
    ax(i,2) = length_scale(i)*a2_av;
    ax(i,3) = length_scale(i)*a3_av;
end

nums.healthy = num;

%% round cell

% mean axes values from sunanda et al 2018
a1_av = 2.7/2/L;
a2_av = 3.2/2/L;
a3_av = 1.8/L;

% compute axes corresponding to scaled volume
for i = num+1:2*num
    ax(i,1) = length_scale(i-num)*a1_av;
    ax(i,2) = length_scale(i-num)*a2_av;
    ax(i,3) = length_scale(i-num)*a3_av;
end

nums.round = num;

%% tapered cell

% mean axes values from sunanda et al 2018
a1_av = 7.8/2/L;
a2_av = 2.6/2/L;
a3_av = 1.5/L;

% compute axes corresponding to scaled volume
for i = 2*num+1:3*num
    ax(i,1) = length_scale(i-2*num)*a1_av;
    ax(i,2) = length_scale(i-2*num)*a2_av;
    ax(i,3) = length_scale(i-2*num)*a3_av;
end

nums.tapered = num;

%% generate discretisations for ellipse-type cells

% generate spherical discretisations
x_sph = generateSphereDisc(H, 1);
N     = length(x_sph)/3;

% scale appropriately
num_ax = size(ax,1);

% extract info
for i = 1:num_ax
    x{i}   = kron(ax(i,:)', ones(N,1)) .* x_sph;
    Ax{i}  = ax(i,:)';
    x0{i}  = [ax(i,1);0;0];
    vol{i} = 4/3*pi*Ax{i}(1)*Ax{i}(2)*Ax{i}(3);
end

%% generate pyriform-type cells

if genPyri 
    % `ideal' pyriform head
    [z,z0,dim,pvol] = generatePyriformHead(H);

    % compute other pyriform heads by scaling volume
    for i = num_ax+1:num_ax+num
        x{i}   = length_scale(i-num_ax)*z;
        Ax{i}  = length_scale(i-num_ax)*dim;
        x0{i}  = z0; 
        vol{i} = V_scale(i-num_ax)*pvol;
    end

    nums.pyri = num;
end

end % function
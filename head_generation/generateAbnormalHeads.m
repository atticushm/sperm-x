function [y, Y, X0, ax] = generateAbnormalHeads(H_trac, H_quad)

% generates some examples of abnormal human sperm head shapes according to
% the WHO manual for examination of human semen handbook (see page 70)

% reference spherical heads for scaling:
sphere_trac = GenerateSphereDisc(H_trac, 1);
sphere_quad = GenerateSphereDisc(H_quad, 1);

% remove X0 if present
sphere_trac = RemoveNode(sphere_trac, [1;0;0]);
sphere_quad = RemoveNode(sphere_quad, [1;0;0]);

% number of nodes
num_trac = length(sphere_trac)/3;
num_quad = length(sphere_quad)/3;

%% round  

% radii
a1 = 3.5/2/45;    ax{1} = [a1; a1; a1];
a2 = 2.5/2/45;    ax{2} = [a2; a2; a2];

% generate discretisations
y{1} = kron(ax{1}, ones(num_trac,1)) .* sphere_trac;
Y{1} = kron(ax{1}, ones(num_quad,1)) .* sphere_quad;

y{2} = kron(ax{2}, ones(num_trac,1)) .* sphere_trac;
Y{2} = kron(ax{2}, ones(num_quad,1)) .* sphere_quad;

% join location in body coordinates
X0{1} = [ax{1}(1);0;0];
X0{2} = [ax{2}(1);0;0];

%% tapered 

% set axis
ax{3} = [2;0.5;0.5]/45;

% generate discretisation
y{3} = kron(ax{3}, ones(num_trac,1)) .* sphere_trac;
Y{3} = kron(ax{3}, ones(num_quad,1)) .* sphere_quad;

% join location in body coordinates
X0{3} = [ax{3}(1);0;0];

%% pyriform

% parameters of conical frustrum
h   = 3.5/45;           % height 
r1  = 0;                % radius at base
r2  = 2.8/2/45;         % radius at z=h
Dr  = 5;                % slice parameter along height
Dth = 10;               % slice parameter about axis
ax{4} = [r2; r2; r2];   % radius of spherical tip

% generate conical frustrum discretisations
cone_trac = GenerateConicalFrustrumDisc(h, r1, r2, Dr, Dth);
cone_quad = GenerateConicalFrustrumDisc(h, r1, r2, Dr*3, Dth*3);

% generate semispherical discretisation
tol          = 1e-5;
sph_trac     = mat(sphere_trac);
sph_quad     = mat(sphere_quad);
ssph_trac    = sph_trac(:,sph_trac(1,:)<tol);
ssph_quad    = sph_quad(:,sph_quad(1,:)<tol);

num_ssph_tr  = size(ssph_trac,2);
num_ssph_qd  = size(ssph_quad,2);

semisph_trac = kron(ax{4}, ones(num_ssph_tr,1)) .* vec(ssph_trac);
semisph_quad = kron(ax{4}, ones(num_ssph_qd,1)) .* vec(ssph_quad);

% connect cone and semisphere
tip_trac = semisph_trac-kron([h;0;0], ones(num_ssph_tr,1));
tip_quad = semisph_quad-kron([h;0;0], ones(num_ssph_qd,1));

pyri_trac = [mat(tip_trac), mat(cone_trac)];
pyri_quad = [mat(tip_quad), mat(cone_quad)];
X0{4} = [0;0;0];

% remove any nodes that might be too close together
tol  = 1e-6;
y{4} = uniquetol(vec(pyri_trac)',tol,'ByRows',true);
Y{4} = uniquetol(vec(pyri_quad)',tol,'ByRows',true);

% remove X0
y{4} = RemoveNode(y{4},[0;0;0]);
Y{4} = RemoveNode(Y{4},[0;0;0]);

end % function
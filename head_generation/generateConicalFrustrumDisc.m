function x = generateConicalFrustrumDisc(h, r1, r2, Dh, Dth)

% generate a discretisation for the curved surface of a conical frustrum,
% not including discretisations of the `caps' (top and bottom flat
% surfaces)

% h is height of conical frustrum
% r1 is radius at base 
% r2 is radius at top
% Dh is a discretisation parameter defining the number of slices up the
% frustrum
% Dth is a discretisation parameter defining number of slices around the
% axis 

% varying radius up height
radius = linspace(r1, r2, Dh);

% varying height
height = linspace(0, h, Dh);
num_h  = length(height);

% varying angle around axis
angle = linspace(0, 2*pi, Dth);
angle = angle(1:end-1);
num_a = length(angle);

% generate ring disc moving up the cone
x = [];
for i = 1:num_h
    y(2,:) = radius(i)*cos(angle);
    y(3,:) = radius(i)*sin(angle);
    y(1,:) = -repmat(height(i), 1, num_a);
    
    x = [x,y];
end 

% remove duplicates
x = unique(x','rows')';

% convert to vector
x = vec(x);

end % function
function [y, Y, X0, ax] = generateSpheroidalHeads(num, H_trac, H_quad, method)

% generate a selection of human sperm bodies/heads by fixing the volume in
% accordance with WHO data, and varying the remaining degrees of freedom
% (semi-major and semi-minor axes)

% From the WHO manual for examining sperm data, pg 68: a `normal' cell has
% median length 4.1um, median width 2.8um, median length:width 1.5

% Page 70 constains examples of various abnormal head morpohologies -- see
% GenerateAbnormalHeads.m for code to generate these

switch method
    
    case 'fix'
        % ~~ volume from median dimensions ~~
        V = 4/3 * pi * (2.8/2)^2 * (4.1/2);

        % ~~ prescribe minor axis values ~~
        % minor axis values
        % b_fix = linspace(2.5/2,3.2/2,num);      % min/max values for 95% CI from WHO
        b_fix = linspace(2/2,4/2,num);

        % find major axis values to conserve volume
        a_val = V./(4/3*pi*b_fix.^2);

        % ~~ prescribe major axis values ~~
        % major axis values
        % a_fix = linspace(3.7/2,4.1/2,num);      % min/max values for 95% CI from WHO
        a_fix = linspace(3/2,5/2,num);

        % find minor axis values to conserve volume
        b_val = sqrt(V./(4/3*pi*a_fix));

        %  ~~ (a,b) pairs ~~
        ab = [a_fix a_val; b_val, b_fix];
    
    case 'search'
        
        % vary both axis values
        a_val = linspace(2,5,num)/2;
        b_val = linspace(2,5,num)/2;
        
        % combine all possible values
        ab = combvec(a_val, b_val);        
        
end

% assuming prolate spheroidal
L  = 45+4;                              % median flagellum+endpiece length
ax = [ab; ab(2,:)]/L;                   % axes scaled by length

num_ax = size(ax,2);
for i = 1:num_ax
    [y{i},Y{i},X0{i}] = GenerateBodyNodes(H_trac,H_quad,'human','custom',ax(:,i));
end

end % function
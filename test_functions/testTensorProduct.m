% TEST FUNCTION

% A small test function to check/verify that (a dot b)*a = kron(a,a') * b.
% This identity is used in SolveHydrodynamics.m and
% SolveElastohydrodynamics.m.

clear variables; close all
disp('-----')
disp('TEST FUNCTION - kron() vs dot().')

a = randi(10,3,1);
b = randi(10,3,1);

r_1 = dot(a,b).*a;
r_2 = kron(a,a')*b;

disp('Single coordinates test:')
disp(r_1 == r_2)

%% Coordinate vector extension

M = 4;
disp(['Number of points M=',num2str(M)])

a = randi(10,3*M,1);
b = randi(10,3*M,1);

% Dot product result:
ma = VectorToMatrix(a);
mb = VectorToMatrix(b);
a_dot_b = dot(ma,mb);
k_1 = repmat(a_dot_b,1,3)';
result_1 = k_1.*a(:);

% Kron result:
k_2 = ExtractComponentDiagonals(kron(a,a'));
result_2 = k_2*b(:);

% Check results:
disp('Vector of coordinates test:')
disp(result_1 == result_2)

%% Alternative ways of computing scalar product.

a = randi(10,3*M,1);

% Dot product:
ada_1 = dot(VectorToMatrix(a),VectorToMatrix(a))';

% Kron (can't think of a way to do this!)s
ada_2 = repmat(eye(M),3,3).*kron(a,a');
% a_dot_a_2 = a_dot_a_2(:);

% Component wise multiplication:
ada_3 = sum(VectorToMatrix(a.*a),1)';

% END OF SCRIPT

%% Additional functions

function K = ExtractComponentDiagonals(A)

% Extracts the main diagonal of each of the 9 component submatrices forming
% the matrix A. A should be a 3Nx3N 3-dimensional tensor.
% This function is not really necessary but useful to keep around to
% illustrate my thinking about the problem.

M = size(A,2);
N = M/3;

if diff(size(A))
    warning('A is not square! Aborting...')
    return
end

extractor = repmat(eye(N),3,3);
K = extractor .* A;

end
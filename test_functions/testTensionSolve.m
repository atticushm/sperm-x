% TEST TensionSolve

% Dry run of solving the tension equation, namely testing the function
% Tension.m

clear all; 
disp('-----')

%% Create a filament:

Q = 10; N = Q+1; ds = 1/Q;
X = ParabolicCurve(0.5,N);
s = linspace(0,1,N);

% Choose q and epislon:
q = 1e-1;
epsilon = 1e-2;

%% Define actuation function m(s,t) and it's derivative.

% For this test function, assume t=0 (or otherwise, shouldn't matter):
tp = 0;

% Choose parameters for travelling wave function:
k = 3*pi;       % dimensionless wavenumber.
ell = 5e-2;     % inactive end piece length.

% Simple travelling wave (***no Heaviside yet***)
m = @(s,t) cos(k*s -t);
m_s = @(s,t) -(1/k)*sin(k*s -t);
m_ss = @(s,t) -(1/k^2)*cos(k*s -t);

%% Solve tension equation:

V = zeros(3*(Q+1),1);
T = Tension(X, V, q, epsilon, m, m_s, m_ss);
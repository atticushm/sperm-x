function testActuationComparison()

% compares the values of the actuation function m(s) when convolved with
% (a) heaviside function, or (b) a tanh cutoff function.

close all

% number of nodes
N = 251;

% arclength
s = linspace(0,1,N);

% travelling wave
k = 4*pi; t = 0;
twave = cos(k*s-t);

% finite differences
D = finiteDifferences(s,1);

% length of end piece
ell = 0.03;

% latex for tick labels
set(groot, 'defaultAxesTickLabelInterpreter','latex');
set(groot, 'defaultAxesFontSize',14);

%% heaviside

% values
m_hside = twave;
m_hside(s(:)>=(1-ell)) = 0;

% derivative
ms_hside = D{1}*m_hside(:);

%% tanh

% values (a is smoothing parameter)
a = 500; b = 1.1;
m_tanh = cos(k*s - t).*(-1/2*(tanh(a*s-a*(1-b*ell))-1));

% derivative
ms_tanh = k/2 * (tanh(a*s-a*(1-b*ell))-1).*sin(k*s-t) - a/2*cos(k*s-t).*(sech(a*s - a*(1-b*ell))).^2;
%ms_tanh = D{1} * m_tanh(:);

%% fit through heavidisde values

% combine values from heaviside formulation, but use smoothing spline over
% cutoff region
% identify nodes to smooth over
id = (s>=(1-2*ell));
s_id = s(id);
m_id = m_hside(id);

% cubic smoothing spline
m_smth = csaps(s_id, m_id, [], s_id);
m_csps = [m_hside(~id), m_smth];

% derivative
ms_csps = D{1}*m_csps(:);

%% plot values

nexttile; hold on; box on; grid on
plot(s(end-80:end), m_hside(end-80:end), 'LineWidth', 1.2);
plot(s(end-80:end), m_tanh(end-80:end),  'LineWidth', 1.2);
plot(s(end-80:end), m_csps(end-80:end),  'LineWidth', 1.2);

% label
xlabel('s'); 
ylabel('m_2(s)')
legend('Heaviside','tanh','csaps','FontSize',14,'Interpreter','latex', ...
    'EdgeColor','none','Location','northeast')

%% plot derivatives

nexttile; hold on; box on; grid on
plot(s(end-80:end), ms_hside(end-80:end), 'LineWidth', 1.2);
plot(s(end-80:end), ms_tanh(end-80:end),  'LineWidth', 1.2);
plot(s(end-80:end), ms_csps(end-80:end),  'LineWidth', 1.2);

% label
xlabel('s'); 
ylabel('m_2(s)')
legend('Heaviside','tanh','csaps','FontSize',14,'Interpreter','latex', ...
    'EdgeColor','none','Location','southeast')

% axes
ax = gca; ax.YAxis.Exponent = 2;

%% scale and save

% scale
set(gcf,'Position',[560 519 675 208])
save2pdf('./test_functions/figures/cutoff_comparison.pdf')
close all;

end % function
%% TEST_StokesletQuadratures.

% See I can write a code to test the regular Regularised Stokeslet Segments
% code from Dave.

clear all; close all; clc

disp('-----')

% Define a segment:
X_c = [0;0.5;0];        % segment midpoint.
ds = 1/20;              % segment length.
th = pi/4;              % tangent angle of segment.

% End points of line segment; coordinates and arclength:
X_a = X_c - (ds/2)*[cos(th), -sin(th), 0]';     s_a = -ds/2;
X_b = X_c + (ds/2)*[cos(th), -sin(th), 0]';     s_b = ds/2;

% Rotation matrix is:
RM = [cos(th), -sin(th), 0; -sin(th), -cos(th), 0; 0, 0, 1];

% Choose a field point:
X_f = X_c;
% X_f = zeros(3,1);

%% Integrate along segment analytically:

epsilon = 1e-2;
I_ana = RegStokesletAnalyticIntegrals(X_f, X_c, ds/2, RM, epsilon);
norm_ana = norm(I_ana);

% disp('Analytic integral:')
% disp(I_ana)

%% Numerical integration:

N_vec = unique(floor(logspace(1,3)));

for n = 1:length(N_vec)

    % Number of nodes for integration:
    N = N_vec(n);

    %% Midpoint rule:

    dx = (s_b-s_a)/N;
    X_q = [linspace(X_a(1),X_b(1),N); linspace(X_a(2),X_b(2),N); linspace(X_a(3),X_b(3),N)];
    XM_q = 0.5*(X_q(:,2:end) + X_q(:,1:end-1));

    sum = 0;
    for i = 1:N-1
        wait_str = sprintf('MPR, N=%.1e',N);
        textwaitbar(i,N-1,wait_str)
        sum = sum + dx * RegStokeslets(X_f, XM_q(:,i), epsilon);
    end
    I_midp{n} = sum;
    norm_midp(n) = norm(I_midp{n});

%     disp('Numerical integral (midpoint rule):')
%     disp(I_midp)

    %% Gauss-Legendre:

    % Lookup G-L nodes and weights:
    [s_i, w_i] = lgwt(N, -ds/2, ds/2);
    [s_i,idx] = sort(s_i);
    w_i = w_i(idx);
    ds_i = diff(s_i);

    % All points on the segment have the same unit tangent:
    t = [cos(th), -sin(th), 0]';

    % Compute X_i = X(s_i);
    for i = 1:N
        if i == 1
            ds_0 = abs(s_i(1)-s_a);
            X_i(:,1) = X_a + ds_0*t;
        else
            X_i(:,i) = X_i(:,i-1) + ds_i(i-1)*t;
        end
    end

    % Compute integral using precalculated weights:
    sum = 0;
    for i = 1:N
        wait_str = sprintf('GLQ, N=%.1e',N);
        textwaitbar(i,N,wait_str)
        sum = sum + w_i(i)*RegStokeslets(X_f, X_i(:,i), epsilon);
    end
    I_gl{n} = sum;
    norm_gl(n) = norm(I_gl{n});

%     disp('Numerical integral (Gauss-Legendre):')
%     disp(I_gl)
end

%% Visualise convergence:

clf;

figure(1); box on;
loglog(N_vec, norm_midp, 'LineWidth',1.2); hold on; grid on;
loglog(N_vec, norm_gl, 'LineWidth',1.2);
loglog(N_vec, repmat(norm_ana,1,length(N_vec)),'r--', 'LineWidth',1.2); 
xlabel('$N$','Interpreter','latex', 'FontSize',12)
ylabel('$\|\mathbf{I}\| = \| \int_{\tilde{s}-h}^{\tilde{s}+h} \mathbf{S}^{\varepsilon}(\mathbf{x},(s-\tilde{s})\mathbf{X}_{s}(\tilde{s}) + \mathbf{X}(\tilde{s}) )ds \|$','Interpreter','latex', 'FontSize',12)
legend('Midpoint rule','Gauss-Legendre','Analytic','Location','southeast', 'FontSize',12)


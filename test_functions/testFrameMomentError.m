% test function

% why does using a force planar frame yield better relative moment errors
% than solving for the director frame?

% in this test function, we run one reasonably high-resolution simulation
% using the 'forced-planar' frame, and another that solves a linear system
% of equations deriving from the director frame equations for d1, d2, d3.

% in previous experiments, the planar frame has been seen to produce lower
% relative moment error than the solved-for director frame. this test
% function seeks to investigate this.

% set modelling options and parameters common to both tests
problem = 'SwimmingCell';
options = setOptions(problem, ...
    'Species',      'human', ...
    'BodyType',     'gskb1', ...
    'NonLocal',     true, ...
    'MechTwist',    false, ...
    'StiffFunc',    'varying', ...
    'LocalType',    'rss', ...
    'RunProfiler',  false, ...
    'SilentMode',   false, ...
    'ProcType',     'cpu', ...   
    'UseBlakelets', false, ...
    'PlaneOfBeat',  'xy', ...
    'SaveOnFail',   false, ...
    'DrawPlots',    true ...
    );
params = setParameters(options, ...
    'ittol',        1e-4, ...
    'NF',           201, ...
    'Ht',           4, ...
    'Hq',           8, ...
    'dt',           10e-2, ...
    'q',            0.1, ...
    ...
    'calS',         14, ...
    'calM1',        0.0, ...
    'calM2',        0.015, ...
    'calM3',        0.0, ...
    'calT',         1, ...
    ...
    'Gamma',        100, ...
    'wavk',         4*pi, ...
    'ell',          0.03, ...
    'epsilon',      0.01, ...
    'lambda',       2e2, ...
    ...
    'h0',           0.4, ...
    'h',            0.5, ...
    'NumBeats',     4, ...
    'tmax',         'auto', ...
    'NumSave',      50 ...
    );    
[int,params] = setInitialCondition(options, params, 'IntCond', 'line');

% finite difference matrices
s = linspace(0,1,params.NF);
D = finiteDifferences(s, 1:2);
D_s = kron(eye(3),D{1});
D_ss = kron(eye(3),D{2});

%% use planar frame

%{
% force planar toggle will also enable other planarity-forcing checks --
% this may or may not be a problem...
options.ForcePlanar = true;

% solve swimming problem
pnr = runSPX(int, params, options); 

% compute d3
for i = 1:length(pnr)
    pnr(i).d3 = D_s * pnr(i).X;
end

% compute d1 derivatives
for i = 1:length(pnr)
    pnr(i).d1_s = D_s * pnr(i).d1;
    pnr(i).d1_ss = D_ss * pnr(i).d1;
end

% check orthonormality
for i = 1:length(pnr)
    d1i = pnr(i).d1;
    d2i = pnr(i).d2;
    d3i = pnr(i).d3;
    pnr(i).ortho = [dotProduct(d1i,d2i)'; dotProduct(d1i,d3i)'; dotProduct(d2i,d3i)'];
end
%}

%% use director frame

% disabling planar forcing will compute the director frame 
options.ForcePlanar = false;

% solve swimming problem
dir = runSPX(int, params, options);

% compute d1 derivatives
for i = 1:length(dir)
    dir(i).d1_s = D_s * dir(i).d1;
    dir(i).d1_ss = D_ss * dir(i).d1;
end

% check orthonormality
for i = 1:length(dir)
    d1i = dir(i).d1;
    d2i = dir(i).d2;
    d3i = dir(i).d3;
    dir(i).ortho = [dotProduct(d1i,d2i)'; dotProduct(d1i,d3i)'; dotProduct(d2i,d3i)'];
end

%% plots

% relative errors
figure;
nexttile;
%plot([pnr.F_relerr],'.-'); 
hold on; plot([dir.F_err],'.-')
xlabel('t')
ylabel('rel hydro force err')
% legend('planar','director')
title(sprintf('N=%g, dt=%g',params.NF,params.dt));

nexttile;
%plot([pnr.M_relerr],'.-'); 
hold on; plot([dir.M_err],'.-')
xlabel('t')
ylabel('rel hydro moment err')
% legend('planar','director')
title(sprintf('N=%g',params.NF));

%{
% d1 comparison
figure;
% at t(n):n=30 (before divergence)
nexttile;
plot(pnr(30).d1,'-'); hold on; plot(dir(30).d1,'--');
xlabel('d_1 components at t=30')
axis tight
% at t(n):n=31 (immediately after divergence)
nexttile;
plot(pnr(31).d1,'-'); hold on; plot(dir(31).d1,'--');
xlabel('d_1 components at t=31')
axis tight
% at t(n):n=41 (extremum)
nexttile;
plot(pnr(41).d1,'-'); hold on; plot(dir(41).d1,'--');
xlabel('d_1 components at t=41')
axis tight

% d2 comparison
figure;
% at t(n):n=30 (before divergence)
nexttile;
plot(pnr(30).d2,'-'); hold on; plot(dir(30).d2,'--');
xlabel('d_2 components at t=30')
axis tight
% at t(n):n=31 (immediately after divergence)
nexttile;
plot(pnr(31).d2,'-'); hold on; plot(dir(31).d2,'--');
xlabel('d_2 components at t=31')
axis tight
% at t(n):n=41 (extremum)
nexttile;
plot(pnr(41).d2,'-'); hold on; plot(dir(41).d2,'--');
xlabel('d_2 components at t=41')
axis tight

% d3 comparison
figure;
% at t(n):n=30 (before divergence)
nexttile;
plot(pnr(30).d3,'-'); hold on; plot(dir(30).d3,'--');
xlabel('d_3 components at t=30')
axis tight
% at t(n):n=31 (immediately after divergence)
nexttile;
plot(pnr(31).d3,'-'); hold on; plot(dir(31).d3,'--');
xlabel('d_3 components at t=31')
axis tight
% at t(n):n=41 (extremum)
nexttile;
plot(pnr(41).d3,'-'); hold on; plot(dir(41).d3,'--');
xlabel('d_3 components at t=41')
axis tight

% d1_s comparison
figure;
% at t(n):n=30 (before divergence)
nexttile;
plot(pnr(30).d1_s,'-'); hold on; plot(dir(30).d1_s,'--');
xlabel('d_{1,s} components at t=30')
axis tight
% at t(n):n=31 (immediately after divergence)
nexttile;
plot(pnr(31).d1_s,'-'); hold on; plot(dir(31).d1_s,'--');
xlabel('d_{1,s} components at t=31')
axis tight
% at t(n):n=41 (extremum)
nexttile;
plot(pnr(41).d1_s,'-'); hold on; plot(dir(41).d1_s,'--');
xlabel('d_{1,s} components at t=41')
axis tight

% d1_ss comparison
figure;
% at t(n):n=30 (before divergence)
nexttile;
plot(pnr(30).d1_ss,'-'); hold on; plot(dir(30).d1_ss,'--');
xlabel('d_{1,ss} components at t=30')
axis tight
% at t(n):n=31 (immediately after divergence)
nexttile;
plot(pnr(31).d1_ss,'-'); hold on; plot(dir(31).d1_ss,'--');
xlabel('d_{1,ss} components at t=31')
axis tight
% at t(n):n=41 (extremum)
nexttile;
plot(pnr(41).d1_ss,'-'); hold on; plot(dir(41).d1_ss,'--');
xlabel('d_{1,ss} components at t=41')
axis tight
%}

%% save select figures

figure(1);
save2pdf(['test_functions/figures/',sprintf('relMomentErr_NF=%g_Ht=%g_q=%g.pdf',params.NF,params.Ht,params.q)]);


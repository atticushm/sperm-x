% TEST_LocalOperator

% Test function for verifying the Local Operator Derivative code.

clear all; clc
disp('-----')

num_runs = 8;
N_vals(1) = 11;
for n = 1:num_runs-1
    N_vals(n+1) = 2*N_vals(n)-1;    % double the number of segments on each run.
end

for k = 1:2

    % Build a high resolution curve:
    if k == 1
        X = ParabolicCurve(0.5,1e4); [x,y,z] = extractComponents(X);
    elseif k == 2
        X = SinusoidalCurve(2*pi,1,1e4); [x,y,z] = extractComponents(X);
    end
    s = linspace(0,1,1e4);
    ppx = spline(s,x);  ppy = spline(s,y);  ppz = spline(s,z);
    dpx = fnder(ppx,1); dpy = fnder(ppy,1); dpz = fnder(ppz,1);
    clear X

    for n = 1:num_runs

        N = N_vals(n);      % number of nodes/field points.
        Q = N-1;            % number of segments/source points.
        ds = 1/Q;           % segment length.
        epsilon = 1e-2;     % regularisation parameter.
        
        % Choose q as L/10:
        q = 1/10;

        % Filament arclength:
        s = linspace(0,1,N); s = s(:);

        % Choose field points as nodes:
        field_id = 1:N;
        s_field = s(field_id);
        x = ppval(ppx,s); y = ppval(ppy,s); z = ppval(ppz,s); X = [x(:); y(:); z(:)];
        X_field = [x(field_id); y(field_id); z(field_id)];  XM_field = VectorToMatrix(X_field);
        T_field = [ppval(dpx,s_field); ppval(dpy,s_field); ppval(dpz,s_field)]; TM_field = VectorToMatrix(T_field);
        num_field = length(field_id);

        % Find segment midponts:
        s_midp = 0.5*(s(1:end-1)+s(2:end));
        X_midp = [ppval(ppx,s_midp); ppval(ppy,s_midp); ppval(ppz,s_midp)]; XM_midp = VectorToMatrix(X_midp);
        T_midp = [ppval(dpx,s_midp); ppval(dpy,s_midp); ppval(dpz,s_midp)];
        th_midp = acos(T_midp(1:Q));
        R_midp = RotationMatrix(th_midp);

        % Interval of integration around the field points:
        s_a = s_field-q;        s_b = s_field+q;
        s_a(s_field < q) = 0;   s_b(s_field > 1-q) = 1;
        
        % Derivative wrt s at s_a and s_b:
        ds_a = ones(N,1);       ds_b = ones(N,1);
        ds_a(s_field<q) = 0;    ds_b(s_field>1-q) = 0;
        
        % Coordinates at limits of integration are:
        X_a = [ppval(ppx,s_a); ppval(ppy,s_a); ppval(ppz,s_a)];
        X_b = [ppval(ppx,s_b); ppval(ppy,s_b); ppval(ppz,s_b)];

        %% Analytical method:
        
        local_id = abs(s_midp-s_field')<=q;
        
        % Compute local integral derivative using the Leibniz rule:
        S_a = RegStokeslets(X_field,X_a,epsilon) .* repmat(ds_a,3,3*N);
        S_b = RegStokeslets(X_field,X_b,epsilon) .* repmat(ds_b,3,3*N);
        I = RegStokesletAnalyticDerivativeIntegrals(X_field,X_midp,T_field,R_midp,ds/2,epsilon);
        I = repmat(local_id',3,3).*I;
        I = I*kron(eye(3),ones(Q,1));
        I = kron(I,ones(1,N));
        leib{n,k} = S_b - S_a + I;

        %% Numerical method:

        % Determine X(s+-h) around the field point (for differentiation):
        h = 1e-10;
        X_sph = [ppval(ppx,s_field+h); ppval(ppy,s_field+h); ppval(ppz,s_field+h)];     XM_sph = VectorToMatrix(X_sph);
        X_smh = [ppval(ppx,s_field-h); ppval(ppy,s_field-h); ppval(ppz,s_field-h)];     XM_smh = VectorToMatrix(X_smh);

        % Limits of integration are usually s+q and s-q. However, here we need to
        % integrate about s-h-q and s-h+q, and s+h-q and s+h.
        % Number of nodes for Guass-Legendre quadrature:
        num_quad = 50;
        smh_a = s_field -h -q;  smh_b = s_field -h +q;
        sph_a = s_field +h -q;  sph_b = s_field +h +q;
        [smh_i, w_smh] = lgwt(num_quad,smh_a,smh_b);        [smh_i, idx_1] =  sort(smh_i);      w_smh = w_smh(idx_1);
        [sph_i, w_sph] = lgwt(num_quad,sph_a,sph_b);        [sph_i, idx_2] =  sort(sph_i);      w_sph = w_sph(idx_2);

        % Spline to get nodes of integration for each integral:
        xi_smh = ppval(ppx,smh_i);  yi_smh = ppval(ppy,smh_i);  zi_smh = ppval(ppz,smh_i);
        xi_sph = ppval(ppx,sph_i);  yi_sph = ppval(ppy,sph_i);  zi_sph = ppval(ppz,sph_i);
        Xi_smh = [xi_smh(:); yi_smh(:); zi_smh(:)];     XMi_smh = VectorToMatrix(Xi_smh);
        Xi_sph = [xi_sph(:); yi_sph(:); zi_sph(:)];     XMi_sph = VectorToMatrix(Xi_sph);

        % Compute integrals
        int = 0;
        for m = 1:num_quad
            int = int + (w_sph(m) * RegStokeslets(X_sph,XMi_sph(:,m),epsilon) ...
                        -w_smh(m) * RegStokeslets(X_smh,XMi_smh(:,m),epsilon));
        end
        num{n,k} = 1/2/h * int;

        %% Semi-numerical method:

        s_source = s_midp(local_id);
        num_source = length(s_source);

        % Coordinates of source points for each integral:
        smh_c = s_source-h;    %smh_c = 0.5*(smh(1:end-1) + smh(2:end));
        sph_c = s_source+h;    %sph_c = 0.5*(sph(1:end-1) + sph(2:end));
        XMsmh_c = [ppval(ppx,smh_c); ppval(ppy,smh_c); ppval(ppz,smh_c)];
        XMsph_c = [ppval(ppx,sph_c); ppval(ppy,sph_c); ppval(ppz,sph_c)];
        Xsmh_c = MatrixToVector(XMsmh_c);
        Xsph_c = MatrixToVector(XMsph_c);

        % Rotation matrices for these source points:
        T_x_smh = ppval(dpx,smh_c);   th_smh = acos(T_x_smh);
        T_x_sph = ppval(dpx,sph_c);   th_sph = acos(T_x_sph);
        R_smh = RotationMatrix(th_smh);
        R_sph = RotationMatrix(th_sph);

        % Numerically differentiate:
        int_sph = RegStokesletAnalyticIntegrals(X_sph,Xsph_c,ds/2,R_sph,epsilon) ;
        int_smh = RegStokesletAnalyticIntegrals(X_smh,Xsmh_c,ds/2,R_smh,epsilon) ;
        mix{n,k} = 1/2/h * ((int_sph - int_smh)*kron(eye(3),ones(num_source,1)));

        %% Using LocalOperator.m

        [R,R_s_full] = LocalOperator(X,s,q,epsilon);
        func{n,k} = [R_s_full(field_id,:); R_s_full(field_id + N,:); R_s_full(field_id+2*N,:)];

    end % n loop.
end % k loop.

%% Inspect Matrices:

%{
disp('Leibniz rule (analytic):')
disp(R_s_leib)
disp('Fully numerical:')
disp(R_s_num)
disp('Mixed (numerical der. of analytic integrals):')
disp(R_s_mix)
disp('Using LocalOperator.m:')
disp(R_s_full)
%}

%% Plots:

close all;

% Compute errors:
for k = 1:2
    for n = 1:num_runs
        error_leib(n,k) = norm(abs(leib{n,k}-num{n,k}));
        error_func(n,k) = norm(abs(func{n,k}-num{n,k}));
        error_mix(n,k)  = norm(abs(mix{n,k} -num{n,k}));
    end
end

% Compare methods:
figure; subplot(1,2,1);
loglog(N_vals, error_leib(:,1), '.-','LineWidth',1.3); grid on; box on; hold on;
loglog(N_vals, error_func(:,1), '.:','LineWidth',1.3)
loglog(N_vals, error_mix(:,1),  '.--','LineWidth',1.3)
xlabel('$N$','Interpreter','latex')
ylabel(['Error vs. numerical method, $h=\,$',num2str(h)],'Interpreter','latex')
titlestr = ['Calculating $\mathbf{R}^{-1}(s,q)$ for $s=\,$',num2str(s_field),', $q=\,$',num2str(q),', parabolic curve'];
title(titlestr,'Interpreter','latex')
legend('Leibniz rule','LocalOperator.m','Mixed','Interpreter','latex')

subplot(1,2,2);
loglog(N_vals, error_leib(:,2), '.-','LineWidth',1.3); grid on; box on; hold on;
loglog(N_vals, error_func(:,2), '.:','LineWidth',1.3)
loglog(N_vals, error_mix(:,2),  '.--','LineWidth',1.3)
xlabel('$N$','Interpreter','latex')
ylabel(['Error vs. numerical method, $h=\,$',num2str(h)],'Interpreter','latex')
titlestr = ['Calculating $\mathbf{R}^{-1}(s,q)$ for $s=\,$',num2str(s_field),', $q=\,$',num2str(q),', sinusoidal curve'];
title(titlestr,'Interpreter','latex')
legend('Leibniz rule','LocalOperator.m','Mixed','Interpreter','latex')

set(gcf,'Position',[587 908 973 430])

% Repeat plots but remove "Leibniz rule":
figure; subplot(1,2,1);
loglog(N_vals, error_func(:,1), '.-','LineWidth',1.3); grid on; box on; hold on;
loglog(N_vals, error_mix(:,1),  '.--','LineWidth',1.3)
xlabel('$N$','Interpreter','latex')
ylabel(['Error vs. numerical method, $h=\,$',num2str(h)],'Interpreter','latex')
titlestr = ['Calculating $\mathbf{R}^{-1}(s,q)$ for $s=\,$',num2str(s_field),', $q=\,$',num2str(q),', parabolic curve'];
title(titlestr,'Interpreter','latex')
legend('Leibniz rule','Mixed','Interpreter','latex')

subplot(1,2,2);
loglog(N_vals, error_func(:,2), '.-','LineWidth',1.3); grid on; box on; hold on;
loglog(N_vals, error_mix(:,2),  '.--','LineWidth',1.3)
xlabel('$N$','Interpreter','latex')
ylabel(['Error vs. numerical method, $h=\,$',num2str(h)],'Interpreter','latex')
titlestr = ['Calculating $\mathbf{R}^{-1}(s,q)$ for $s=\,$',num2str(s_field),', $q=\,$',num2str(q),', sinusoidal curve'];
title(titlestr,'Interpreter','latex')
legend('Leibniz rule','Mixed','Interpreter','latex')

set(gcf,'Position',[587 908 973 430])


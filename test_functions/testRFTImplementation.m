% TEST FUNCTION

% In the resistive force theory of Gray & Hancock, the fluid velocity
% around a slender moving body can be described as:
%   v(x) = -1/xi_para (dot(f,units))units - 1/xi_perp(dot(f,unitn))unitn -
%          -1/xi_perp (dot(f,unitb))unitb 

% This is often rewritten as:
%   v(x) = -1/xi_perp (I + (gamma-1)units units) dot f.

% Is the way I am implementing this correct?
% By prescribing f (or alternatively v(x)), this test function aims to find
% out.

close all; clc
clearvars;
disp('-----')

M = 10; % number of nodes.
int_type = 'parabola';

% Create body shape:
switch int_type
    case 'parabola'
        X = ParabolicInitialCondition(0.3,M);
        X_q = ParabolicInitialCondition(0.3,3*M);
    case 'helix'
        X = HelicalInitialCondition(3,1,M);
        X_q = HelicalInitialCondition(3,1,3*M);
end

% Set RFT parameters:
q = 0.1;
b = 0.01;
mu = 0.14;
xi_para = 8*pi / (-2 + 4*log(2*q/b) );
xi_perp = 8*pi / ( 1 + 2*log(2*q/b) );
gamma = xi_perp/xi_para;

% Prescribe forces, to solve for v:
f_x = 8*pi*ones(M,1);
f_y = -pi*ones(M,1);
f_z = zeros(M,1);
f = [f_x; f_y; f_z];
mf = VectorToMatrix(f);

% Get slender body shape:
[x,y,z] = extractComponents(X);
s = GetArclengthNodes(X);

% Get Frenet-Serret unit vectors:
[D_s,D_ss,~,~] = BuildFiniteDifferenceMatrices(s, 'equispaced');
global Diff
Diff.s = D_s; Diff.ss = D_ss;
units = blkdiag(D_s,D_s,D_s) * X(:);
unitn = CalculateNormals(X(:));

ms = VectorToMatrix(units);
mn = VectorToMatrix(unitn);
for m = 1:size(ms,2)
    mb(:,m) = cross(ms(:,m),mn(:,m));
end
unitb = MatrixToVector(mb);

disp('-----')

%% Representation 1: per component
% fully expanded representation.

% Recall identity: (a dot b) b = tensorprod(a,a) b
for m = 1:M
    ss = kron(ms(:,m),ms(:,m)');
    nn = kron(mn(:,m),mn(:,m)');
    bb = kron(mb(:,m),mb(:,m)');
    
    mv_1(:,m) = -(1/xi_para) * ss*mf(:,m) + ...
                -(1/xi_perp) * nn*mf(:,m) + ...
                -(1/xi_perp) * bb*mf(:,m) ;
end
v_1 = MatrixToVector(mv_1);

%% Representation 2: per component (alt)
% same as rep. 1, but with dot products computed differently.

fdots = sum(VectorToMatrix(f.*units))';
fdotn = sum(VectorToMatrix(f.*unitn))';
fdotb = sum(VectorToMatrix(f.*unitb))';

v_2 = -(1/xi_para) * repmat(fdots,3,1).*units + ...
      -(1/xi_perp) * repmat(fdotn,3,1).*unitn + ...
      -(1/xi_perp) * repmat(fdotb,3,1).*unitb ;

%% Representation 3: with gamma
% simplified expression.

op_3 = -(1/xi_perp) * (eye(3*M) + (gamma-1)*repmat(eye(M),3,3).*kron(units,units'));
v_3 = op_3 * f(:);

%% Extension: nonlocal extension
% Velocity along the flagellum is v(x) = <local> + <nonlocal> with the
% local terms being approximated by RFT.
% Does the additional of the nonlocal integral term affect the above
% results? Or at least, what different ways can we implement the <local> +
% <nonlocal> problem?

Slets = RegStokeslets(X, X_q, 0.01);        % regularised stokeslets.
nu = NearestNeighbourMatrix(X_q, X);        % nearest neighbour matrix.
sig = NearestNodes(X, q, '>');              % 'furthest' neighbour matrix.
op_ext = (1/mu)*sig.*(Slets * nu);                 % operator onto vector of forces.

vext_1 = v_1 - op_ext*f;
vext_2 = v_2 - op_ext*f;
vext_3 = (op_3 - op_ext)*f;

% Check <local> + <nonlocal> solution against full reg. stokeslets.
op_fullnl = (1/mu)*Slets * nu;
v_fullnl = op_fullnl * f;

sig_inv = NearestNodes(X, q, '<=');
op_ext_inv = (1/mu)*sig_inv.*(Slets * nu);
v_fullnl_split = -op_ext_inv*f - op_ext*f;

%% Calculate errors:

e_12 = MeanSqError(v_1,v_2);
e_13 = MeanSqError(v_1,v_3);
e_23 = MeanSqError(v_2,v_3);

eext_12 = MeanSqError(vext_1,vext_2);
eext_13 = MeanSqError(vext_1,vext_3);
eext_23 = MeanSqError(vext_2,vext_3);

disp(['Error between per comp. method and per comp. alt method is ',num2str(e_12)])
disp(['Error between per comp. method and simpler method is ',num2str(e_13)])
disp(['Error between per comp. alt method and simpler method is ',num2str(e_23)])
disp('-----')
disp('Now with nonlocal extension:')
disp(['Error between per comp. method and per comp. alt method is ',num2str(eext_12)])
disp(['Error between per comp. method and simpler method is ',num2str(eext_13)])
disp(['Error between per comp. alt method and simpler method is ',num2str(eext_23)])

%% Plots

% Plot quiver:
figure; hold on;
plot3(x,y,z,'k.-','Linewidth',5)
quiver3(x,y,z,ms(1,:)',ms(2,:)',ms(3,:)')
quiver3(x,y,z,mn(1,:)',mn(2,:)',mn(3,:)')
quiver3(x,y,z,mb(1,:)',mb(2,:)',mb(3,:)')
axis equal
view(3)


disp('-----')

% END OF SCRIPT
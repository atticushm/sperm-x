% TEST_RodIntegral
% Supercedes TEST_StraightLineIntegral by considering the integrals I(s,q)
% over a local region of rod.

clear all; clc
disp('-----')

% Choose different discretisation parameters:
num_runs = 8;
N_vals(1) = 11;
for n = 1:num_runs-1
    N_vals(n+1) = 2*N_vals(n)-1;    % double the number of segments on each run.
end

% Choose local cutoff parameter:
q = 0.1;

% Choose regularisation parameter:
epsilon = 1e-2;

% Choose number of nodes for numerical integration:
% num_quad_vals = [50,100,200,400,800,1600];
num_quad_vals = 1e4;

% Choose step size for numerical differentiation:
h = 1e-6;

% Choose tangent angle of rod:
th = pi/4;
T_rod = [cos(th);sin(th);0];

% Tangent & rotation matrix is uniform along the rod:
R_rod = RotationMatrix(th);

% Choose X0:
X0 = [0;0;0];

for m = 1:length(num_quad_vals)
    
    num_quad = num_quad_vals(m);

for n = 1:num_runs
    textwaitbar(n,num_runs,['Increasing Q | N=',num2str(num_quad)])
    
    Q = N_vals(n)-1;
    ds = 1/Q;
    s = 0:ds:1;
    
    % Discretise rod via linear interp. of end points:
    X1 = X0 + T_rod;
    x = interp1([0,1],[X0(1),X1(1)],s);
    y = interp1([0,1],[X0(2),X1(2)],s);
    z = interp1([0,1],[X0(3),X1(3)],s);
    X = [x(:); y(:); z(:)];
    
    % Choose field point as an interior point a fifth of the way along
    % the rod:
    field_id = find(s==0.1);    s_field = s(field_id);
    X_field = [x(field_id); y(field_id); z(field_id)];
    
    %% method (i)
    % Fully analytical, geometrically approximate typically, but in this
    % case is exact.
    
    % Coordiantes at limits of integration:
    s_a = s_field -q;       s_b = s_field +q;
    a_id = find(s==s_a);    b_id = find(s==s_b);
    X_a = [x(a_id); y(a_id); z(a_id)];
    X_b = [x(b_id); y(b_id); z(b_id)];
    
    % Segment midpoints:
    s_midp = 0.5*(s(1:end-1)+s(2:end));
    x_midp = 0.5*(x(1:end-1)+x(2:end));
    y_midp = 0.5*(y(1:end-1)+y(2:end));
    z_midp = 0.5&(z(1:end-1)+z(2:end));
    X_midp = [x_midp(:); y_midp(:); z_midp(:)];
    XM_midp = VectorToMatrix(X_midp);
    
    % Midpoint within the local interval are source points:
    local_id = abs(s_midp-s_field)<=q;
    X_source = MatrixToVector(XM_midp(:,local_id));
    num_source = length(X_source)/3;
    R_source = kron(R_rod,ones(1,num_source));
    
    % Compute:
    i{n,m} = RegStokesletAnalyticDerivativeIntegrals(X_field,X_source,T_rod,R_source,ds/2,epsilon)*kron(eye(3),ones(num_source,1));
    
    %% method (ii)
    % Semianalytic, geometrically exact.
    
    % Determine nodes for numerical integration:
    [s_int,weights] = lgwt(num_quad,s_a,s_b);
    [s_int,idx] = sort(s_int);  weights = weights(idx);
    
    x_int = interp1([0,1],[X0(1),X1(1)],s_int);
    y_int = interp1([0,1],[X0(2),X1(2)],s_int);
    z_int = interp1([0,1],[X0(3),X1(3)],s_int);
    X_int = [x_int(:); y_int(:); z_int(:)];
    
    ii{n,m} = RegStokesletAnalyticDerivatives(X_field,X_int,epsilon,T_rod)*kron(eye(3),weights);
    
    %% method (iii)
    % fully numerical, geometrically exact.
    % iiia is second order central differences.
    % iiib is fourth order central differences.
    
    % Field nodes for numerical differentiation:
    s_diff = [s_field-2*h, s_field-h, s_field+h, s_field+2*h];
    x_diff = interp1([0,1],[X0(1),X1(1)],s_diff);
    y_diff = interp1([0,1],[X0(2),X1(2)],s_diff);
    z_diff = interp1([0,1],[X0(3),X1(3)],s_diff);
    XM_diff = [x_diff(:)'; y_diff(:)'; z_diff(:)'];
    
    iiia{n,m} = (1/2/h * (-RegStokeslets(XM_diff(:,2),X_int,epsilon) + RegStokeslets(XM_diff(:,3),X_int,epsilon))) * kron(eye(3),weights);    
    
    iiib{n,m} = (1/h * (1/12*RegStokeslets(XM_diff(:,1),X_int,epsilon) -2/3*RegStokeslets(XM_diff(:,2),X_int,epsilon) ...
                       +2/3*RegStokeslets(XM_diff(:,3),X_int,epsilon)-1/12*RegStokeslets(XM_diff(:,4),X_int,epsilon))) * kron(eye(3),weights);
    
end % n loop.

end % m loop.

%% Compute errors:

for m = 1:size(i,2)
    for n = 1:length(N_vals)
        error_i_iiia(n,m)   = norm(abs(i{n,m} - iiia{n,m}));
        error_i_iiib(n,m)   = norm(abs(i{n,m} - iiib{n,m}));
        error_ii_iiib(n,m)  = norm(abs(ii{n,m} - iiib{n,m})); 
        error_i_ii(n,m)     = norm(abs(i{n,m} - ii{n,m}));
    end
end

%% Plots

close all;
f = figure;
% f.CurrentAxes.LineStyleOrder = {'-','--','-.'};
set(gca,'LineStyleOrder',{'-','--','-.'})

for m=1:size(i,2)
   
%     l1 = loglog(N_vals,error_i_iiia(:,m)); hold on; box on;
    hold on; box on;
    l2 = loglog(N_vals,error_i_iiib(:,m));
    l3 = loglog(N_vals,error_ii_iiib(:,m));
    
end


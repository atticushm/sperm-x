% TEST FUNCTION
% Computing total force and moment upon a sphere of unit radius using
% NEAREST.

% Here we consider a Resistance problem - calculating the 
% force distribution from known velocities.

% Translation with velocity U = (1,0,0) equals force of F = (6\pi,0,0).
% Rotation with velocity Omg = (1,0,0) equals moment of M = (8\pi,0,0).

clear variables; close all;

n_f = 10;    % force discretisation paramter.
n_q = 25;    % quadrature discretisation parameter, n_q > n_f.

% Compute coarse force discretisation of unit sphere:
X_f = GenerateSphereDisc(n_f, 1);   % field points; where we aim to calculate force.
N_f = length(X_f)/3; 
[x_f, y_f, z_f] = extractComponents(X_f); % extract components.

% Compute fine quadrature discretisation of unit sphere:
X_q = GenerateSphereDisc(n_q, 1);   % source points; where the kernel is evaluated.
N_q = length(X_q)/3;
[x_q, y_q, z_q] = extractComponents(X_q); % extract components.

% Compute nearest-neighbour matrix:
NN = NearestNeighbourMatrix(X_q, X_f);

% Compute regularised stokeslet matrices (Nystrom & NEAREST):
eps = 0.01;     % regularisation parameter.
S_NN = RegStokeslets(X_f, X_q, eps);
S_Nystrom = RegStokeslets(X_f, X_f, eps);

% Print important information to command window:
disp(['Force nodes obtained via ',num2str(n_f),'x',num2str(n_f),' cubic projection'])
disp(['Quadrature nodes obtained via ',num2str(n_q),'x',num2str(n_q),' cubic projection'])
disp(['Num. of force/traction nodes is ',num2str(N_f)])
disp(['Num. of quadrature nodes is ',num2str(N_q)])
disp('-----')

%% Translating sphere: U = (1,0,0)

disp('Translating sphere resistance problem:')

% Prescribe translational velocity:
U = [1,0,0];

% Compute LHS (velocties) from rigid body motion:
LHS = repmat(U, [N_f,1]);
LHS = LHS(:);

% Solve resistance problem to find force distribution:
f_Nystrom = S_Nystrom\LHS;
f_NN = (S_NN*NN)\LHS;

% Compute total force:
F_Nystrom = sum(f_Nystrom(1:N_f));
F_NN = ComputeNNForce(f_NN,NN);

% Print errors to command window:
disp(['Nystrom F = ',num2str(F_Nystrom)])
disp(['NEAREST F = ',num2str(F_NN(1))])
disp(['Nystrom F error is ',num2str(abs(6*pi-F_Nystrom))])
disp(['NEAREST F error is ',num2str(abs(6*pi-F_NN(1)))])
disp('-----')

%% Rotating sphere: Omg = (1,0,0)

disp('Rotating sphere resistance problem:')

% Prescribe rotational velocity:
Omg = [1,0,0];

% Compute LHS (velocities) from rigid body motion:
u_omg = zeros(3,N_f); % preallocate.
for n = 1:N_f
    y = [x_f(n); y_f(n); z_f(n)];
    u_omg(:,n) = cross(Omg, y);
end
LHS = u_omg'; LHS = LHS(:);

% Solve resistance problem to find force distribution:
f_Nystrom = S_Nystrom\LHS;
f_NN = (S_NN*NN)\LHS;
[f_Nys1, f_Nys2, f_Nys3] = extractComponents(f_Nystrom);

% Compute total moment:
for n = 1:N_f
    y = [x_f(n); y_f(n); z_f(n)];
    f_Nys = [f_Nys1(n); f_Nys2(n); f_Nys3(n)];
    m_Nys(:,n) = cross(y, f_Nys);
end
M_Nystrom = sum(m_Nys(1,:));
M_NN = ComputeNNMoment(X_q, [0;0;0], f_NN, NN);

% Print errors to command window:
disp(['Nystrom M = ',num2str(M_Nystrom)])
disp(['NEAREST M = ',num2str(M_NN(1))])
disp(['Nystrom M error is ',num2str(abs(8*pi-M_Nystrom))])
disp(['NEAREST M error is ',num2str(abs(8*pi-M_NN(1)))])
disp('-----')

% END OF SCRIPT

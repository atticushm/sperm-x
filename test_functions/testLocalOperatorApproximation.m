% TEST_LocalOperatorApproximation

% Investigating how well the local operators R_ij can be described by an
% RFT-like formulation.     

clear all; close all
clc

%% Global parameter choices:

% Spatial discretisation:
Q = 100;
Qp1 = Q+1;
s = linspace(0,1,Qp1);

% Regulatisation parateter:
epsilon = 1e-2;

% Identify node at which to examine R:
id = floor(Q/4);

% Local region size:
q = 0.1;                % <~~ generalise for a few values of q.
b = 1e-2;               % b < q < L

%% Parbolic curve:

printLinebreak('#')
disp('Parabolic curve')

% iterate for parameters:
X = ParabolicCurve(0.5,Qp1);
[c_n(1),gamma(1),Rid] = BisectionForCn(X,s,q,id);

% parameters from analytical formulation:
[c_n_ana(1),gamma_ana(1),Rloc{1}] = LocalMatrix(X,s, b, q, id);
[tr_error,max_error] = MatrixError(Rloc{1},Rid);
disp(['Trace error = ',num2str(tr_error)])
disp(['Max component error = ',num2str(max_error)])
printLinebreak

%% Sinusoidal curve:

printLinebreak('#')
disp('Sinusoidal curve')

% iterate for parameters:
X = SinusoidalCurve(3*pi,0.1,Qp1);
[c_n(2),gamma(2),Rid] = BisectionForCn(X,s,q,id);

% parameters from analytical formulation:
[c_n_ana(2),gamma_ana(2),Rloc{2}] = LocalMatrix(X,s,b, q, id);
[tr_error,max_error] = MatrixError(Rloc{2},Rid);
disp(['Trace error = ',num2str(tr_error)])
disp(['Max component error = ',num2str(max_error)])
printLinebreak

%% Helical curve:

printLinebreak('#')
disp('Helical curve')

% iterate for parameters:
X = HelicalCurve(1.5*pi,0.1,Qp1);
[c_n(3),gamma(3),Rid] = BisectionForCn(X,s,q,id);

% parameters from analytical formulation:
[c_n_ana(3),gamma_ana(3),Rloc{3}] = LocalMatrix(X,s,b, q, id);
[tr_error,max_error] = MatrixError(Rloc{3},Rid);
disp(['Trace error = ',num2str(tr_error)])
disp(['Max component error = ',num2str(max_error)])
printLinebreak

%% Algorithm and other functions:

function [c_n_ana,gamma_ana,R] = LocalMatrix(X, s, b, q, id)

    XM = VectorToMatrix(X);
    Xid = XM(:,id);
    
    % Compute resistance coefficients:
%     a = 7/2;      % = 3.5
%     a = 16/5;       % = 3.2
    a = 2;
    c_n_ana = 8*pi/(1+2*log(a*q/b));     % xi_perp
    c_t_ana = 8*pi/(-2+4*log(a*q/b));    % xi_para
    gamma_ana = c_n_ana(1)/c_t_ana(1);
    
    % Get tangents:
    T = TangentsToCurve(X, s);
    TM = VectorToMatrix(T);
    Tid = TM(:,id);
    TT = buildTensorOp(Tid);
    
    % Build matrix:
    R = 1/c_n_ana * (eye(3) + (gamma_ana-1)*TT);
    
    % Print info:
    printLinebreak('#')
    disp('Analytical (LGL)')
    printLinebreak
    disp(['b = ',num2str(b)])
    disp(['q = ',num2str(q)])
    disp(['c_n = ',num2str(c_n_ana(1))])
    disp(['gamma = ',num2str(gamma_ana(1))])
    disp('Local matrix:')
    disp(R)

end % function

function [tr_error,max_error] = MatrixError(A,B)
    
    tr_error = abs(trace(A)-trace(B));
    max_error = max(max(abs(A-B)));

end

function [c_n,gamma,Rid] = BisectionForCn(X,s,q,id)
    
    Qp1 = length(s);

    XM = VectorToMatrix(X);
    Xid = XM(:,id);
    epsilon = 1e-2;
    [R,~] = LocalOperator(X,s,q,epsilon);
    Rid = [R(id,:); R(id+Qp1,:); R(id+2*Qp1,:)];
    
    % Tangents to the curve:
    T = TangentsToCurve(X,s);    
    TM = VectorToMatrix(T);
    Tid = TM(:,id);
    
    % Iterate a local approximation to find parameters producing a matrix close
    % to Rid.
    % First iterate to tune c_n (which mostly impacts diagonal entries) then
    % tune gamma (which helps with off-diagonal entries):
    gamma = 1.8;        % <~~ fix gamma whilst searching for c_n.
    tol_cn = 1e-4;
    TT = buildTensorOp(Tid);
    c_int = [0.5, 5];

    for i = 1:50
       c_n(i) = 0.5*(c_int(1)+c_int(2));
       LOp{i} = 1/c_n(i) * (eye(3) + (gamma-1)*TT);

       [tr_error,max_error] = MatrixError(LOp{i},Rid);
       if tr_error < tol_cn
           %{
            % c_n is tuned: now tune gamma
            printLinebreak('#')
            disp('c_n tuned - now tune gamma:')
            gamma_int = [1, 1.8];
            error(1) = max(max(abs(LOp{i}-Rid)));
            for j = 1:10
                gamma(j) = 0.5*(gamma_int(1)+gamma_int(2));
                LOp{i+j} = 1/c_n(i) * (eye(3) + (gamma(j)-1)*TT);
                error(j+1) = max(max(abs(LOp{i+j}-Rid)));
                if error(j) < tol_gamma
                    disp(['Desired tolerance achieved! (tol = ',num2str(tol_gamma),')'])
                    disp(['gamma = ',num2str(gamma(end))]);
                    disp(['c_n = ',num2str(c_n(end))]);
                    disp('"RFT" approximation:')
                    disp(LOp{end})
                    disp('R(X,q):')
                    disp(Rid)
                    printLinebreak('#')
                    return
                elseif error(j+1) > error(j)
                    gamma_int(1) = gamma(j);
                else
                    gamma_int(2) = gamma(j);
                end

               % checking:
               printLinebreak
               disp(['Iter. # ',num2str(i+j)])
    %            disp(['c_n = ',num2str(c_n(end))])
    %            disp(['gamma = ',num2str(gamma(j))])
                disp(['gamma_int = [',num2str(gamma_int),']'])
               disp('"RFT" approximation:')
               disp(LOp{i+j})
               disp('R(X,q):')
               disp(Rid)
               disp('max component error:')
               disp(error(j))

            end
           %}
           printLinebreak
           disp('Tuning complete!')
           disp(['Iter. # ',num2str(i)])
           disp('X = ')
           disp(Xid')
           disp(['q = ',num2str(q)])
           disp(['c_n = ',num2str(c_n(end))])
           disp(['gamma = ',num2str(gamma)])
           disp('"RFT" approximation:')
           disp(LOp{i})
           disp('R(X,q):')
           disp(Rid)
           disp(['Trace error = ',num2str(tr_error)])
           disp(['Max component error = ',num2str(max_error)])
           printLinebreak
           gamma = gamma(end);
           c_n = c_n(end);
           return
       elseif trace(LOp{i}) <= trace(Rid)
           c_int(2) = c_n(i);
       elseif trace(LOp{i}) > trace(Rid)
           c_int(1) = c_n(i);
       end
       
    end
    
    gamma = gamma(end);
    c_n = c_n(end);
    
end % function
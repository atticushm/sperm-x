% TEST FUNCTION

% Testing ways to frame both planar and three dimensional curves.

% define a curve:
N = 300;
X = InitialCondition('parabolic',N);

% arclength parametrisation:
s = linspace(0,1,N);

% compute tangents at nodes:
tt = TangentsToCurve(X, s);

%% planar curves: frenet serret:

nn{1} = NormalsToCurve(X, s, 'frenet-serret');


%% planar curves: euler angles:

nn{2} = NormalsToCurve(X, s, 'euler');
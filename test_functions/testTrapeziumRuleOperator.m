% test function 

% checking whether my trapezium rule matrix operator implementation is
% correct

% we want to compute the integral int_s^L cos(2*pi*s) ds
% we will compute integrals for 0<=s<=1 analytically and using the
% trapezium rule, implementated as a matrix operator 

% analytic functions
func = @(s) cos(2*pi*s);
func_int = @(s) 1/2/pi * sin(2*pi*s);

% s values
N = 100; 
s = linspace(0,1,N);

% segment length 
ds = 1/(N-1);

%% analytic

for n = 1:N
    Ia1(n) = func_int(s(end)) - func_int(s(n));
end

%% analytic (trapz)

for n = 1:N
    Ia2(n) = trapz(s(n:end),cos(2*pi*s(n:end)));
end

%% numerical (loop)

arg = cos(2*pi*s);
for n = 1:N
    In1(n) = 0;
    for m = n:N-1
        In1(n) = In1(n) + ds/2*(arg(m)+arg(m+1));
    end
end

%% numerical (operator)

tron = triu(ones(N));
Op   = triu(2*ones(N),1) + diag(ones(N,1)); Op(:,end) = ones(N,1);
In2  = ds/2 * Op * cos(2*pi*s(:));

arg = repmat(cos(2*pi*s),N,1);
Op  = triu(2*arg,1) + diag(arg); Op(:,end) = arg(:,end);
In3 = ds/2 * Op * ones(N,1);

%% plot

close all; figure
plot(Ia1); hold on;
plot(Ia2);
plot(In1,'--');
plot(In3,'-.');
legend('analytical','trapz','loop','matrix')
xlabel('s'); ylabel('I(s)')
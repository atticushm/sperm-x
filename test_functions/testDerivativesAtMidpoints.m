% TEST FUNCTION

% Testing the computed derivatives at nodes X vs derivatives at segment
% midpoints X_c - do they agree? Can we interpolate derivatives at nodes to
% get derivatives at midpoints?

clearvars; close all; clc
disp('-----')

%% Setup:

M = 10;             % number of nodes.
epsilon = 1e-2;     % regularisation parameter.

[X, s] = ParabolicInitialCondition(0.3, M);     % node coordinates.
[x,y,z] = extractComponents(X);
[X_c, s_c] = SegmentMidpoints(X);               % midpoint coordinates.
[x_c,y_c,z_c] = extractComponents(X_c);

% Calculate finite difference scheme for node derivatives:
[dX_s, dX_ss,~,~] = BuildFiniteDifferenceMatrices(s, 'equispaced');

% Calculate fintie difference scheme for node midpoints:
[dXc_s, dXc_ss, ~,~] = BuildFiniteDifferenceMatrices(s_c, 'equispaced');

%% Tangent vectors test:
% Compute tangent vectors at nodes using finite differences.
% Compute tangent vectors at midpoints using (a) finite differences and (b)
% computed directly using knowledge of theta and planarity.

% Tangents via finite differences:
t_X_fdm = blkdiag(dX_s,dX_s,dX_s) * X(:);
t_Xc_fdm = blkdiag(dXc_s,dXc_s,dXc_s) * X_c(:);
[tx_X_fdm,ty_X_fdm,tz_X_fdm] = extractComponents(t_X_fdm);
[tx_Xc_fdm,ty_Xc_fdm,tz_Xc_fdm] = extractComponents(t_Xc_fdm);

% Segment tangents via knowledge of theta:
theta = atan(diff(y)./diff(x)); 
t_Xc_the = [cos(theta(:)); sin(theta(:)); zeros(M-1,1)];
[tx_Xc_the,ty_Xc_the,tz_Xc_the] = extractComponents(t_Xc_the);

% Segment tangents via interpolation of node tangents:
tx_Xc_int = interp1(s, tx_X_fdm, s_c, 'spline');
ty_Xc_int = interp1(s, ty_X_fdm, s_c, 'spline');
tz_Xc_int = interp1(s, tz_X_fdm, s_c, 'spline');
t_Xc_int = vertcat(tx_Xc_int(:), ty_Xc_int(:), tz_Xc_int(:));

% Errors:
Et_direct_interp = MeanSqError(t_Xc_the, t_Xc_int);
Et_direct_fdm = MeanSqError(t_Xc_the, t_Xc_fdm);

%% Normal vectors test:
% Normal is calcualted via X_ss/|X_ss|:

% Normals via finite differences:
Xss_fdm = blkdiag(dX_ss,dX_ss,dX_ss) * X(:);
Xcss_fdm = blkdiag(dXc_ss,dXc_ss,dXc_ss) * X_c(:);
n_X_fdm = Xss_fdm./repmat(sqrt(vecdot(Xss_fdm,Xss_fdm)),3,1);
n_Xc_fdm = Xcss_fdm./repmat(sqrt(vecdot(Xcss_fdm,Xcss_fdm)),3,1);
[nx_X_fdm,ny_X_fdm,nz_X_fdm] = extractComponents(n_X_fdm);
[nx_Xc_fdm,ny_Xc_fdm,nz_Xc_fdm] = extractComponents(n_Xc_fdm);

% Segment normals via knowledge of theta:
n_Xc_the = [-sin(theta(:)); cos(theta(:)); zeros(M-1,1)];
[nx_Xc_the,ny_Xc_the,nz_Xc_the] = extractComponents(n_Xc_the);

% Segment normals via interpolation of node normals:
nx_Xc_int = interp1(s, nx_X_fdm, s_c, 'spline');
ny_Xc_int = interp1(s, ny_X_fdm, s_c, 'spline');
nz_Xc_int = interp1(s, nz_X_fdm, s_c, 'spline');
n_Xc_int = vertcat(nx_Xc_int(:), ny_Xc_int(:), nz_Xc_int(:));

% Errors:
En_direct_interp = MeanSqError(n_Xc_the, n_Xc_int);
En_direct_fdm = MeanSqError(n_Xc_the, n_Xc_fdm);

%% Plots:

% Tangent vectors, x and y components:
figure(1);
subplot(2,2,1); box on; hold on; axis equal
plot(x,y,'k-','LineWidth',2);
quiver(x,y,tx_X_fdm,ty_X_fdm,0.3,'Autoscale','off'); quiver(x_c,y_c,tx_Xc_fdm,ty_Xc_fdm,0.3,'Autoscale','off');
xlabel('x'); ylabel('y'); title('X_s (FDM) and X_{c,s} (FDM)')

subplot(2,2,2); box on; hold on; axis equal
plot(x,y,'k-','LineWidth',2);
quiver(x,y,tx_X_fdm,ty_X_fdm,0.3,'Autoscale','off'); quiver(x_c,y_c,tx_Xc_the,ty_Xc_the,0.3,'Autoscale','off');
xlabel('x'); ylabel('y'); title('X_s (FDM) and X_{c,s} (direct)')

subplot(2,2,3); box on; hold on; axis equal
plot(x,y,'k-','LineWidth',2);
quiver(x,y,tx_X_fdm,ty_X_fdm,0.3,'Autoscale','off'); quiver(x_c,y_c,tx_Xc_int,ty_Xc_int,0.3,'Autoscale','off');
xlabel('x'); ylabel('y'); title('X_s (FDM) and X_{c,s} (interp)')

sgtitle('Unit tangent comparison')

% Normal vectors, x and y components:
figure(2)
subplot(2,2,1); box on; hold on; axis equal
plot(x,y,'k-','LineWidth',2);
quiver(x,y,nx_X_fdm,ny_X_fdm,0.3,'Autoscale','off'); quiver(x_c,y_c,nx_Xc_fdm,ny_Xc_fdm,0.3,'Autoscale','off');
xlabel('x'); ylabel('y'); title('X_{ss} (FDM) and X_{c,ss} (FDM)')

subplot(2,2,2); box on; hold on; axis equal
plot(x,y,'k-','LineWidth',2);
quiver(x,y,nx_X_fdm,ny_X_fdm,0.3,'Autoscale','off'); quiver(x_c,y_c,nx_Xc_the,ny_Xc_the,0.3,'Autoscale','off');
xlabel('x'); ylabel('y'); title('X_{ss} (FDM) and X_{c,ss} (direct)')

subplot(2,2,3); box on; hold on; axis equal
plot(x,y,'k-','LineWidth',2);
quiver(x,y,nx_X_fdm,ny_X_fdm,0.3,'Autoscale','off'); quiver(x_c,y_c,nx_Xc_int,ny_Xc_int,0.3,'Autoscale','off');
xlabel('x'); ylabel('y'); title('X_{ss} (FDM) and X_{c,ss} (interp)')

sgtitle('Unit normal comparison')

% END OF SCRIPT
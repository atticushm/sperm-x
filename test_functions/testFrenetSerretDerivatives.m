% TEST_FrenetSerretDerivatives.

% A test function to verify the sanity of the X(s) derivatives produced by
% the Frenet-Serret functions.

clear all; clc
disp('-----')
disp('Derivatives of X(s) using Frenet-Serret formulas')
disp('-----')

% Create a parabolic curve:
a = 0.5;
N = 50;    
N_vals = linspace(0,1,N);
X = ParabolicCurve(a,N);
s = linspace(0,1,N);

% Derivatives from the Frenet-Serret formulas:
dX = DerivativesFromFrenetSerret(X(:));
X_s{1} = dX{1};            MX_s{1} = VectorToMatrix(X_s{1});
X_ss{1} = dX{2};           MX_ss{1} = VectorToMatrix(X_ss{1});
X_sss{1} = dX{3};          MX_sss{1} = VectorToMatrix(X_sss{1});
X_ssss{1} = dX{4};         MX_ssss{1} = VectorToMatrix(X_ssss{1});

% Derivatives from finite difference scheme:
[ds1,ds2,ds3,ds4] = BuildFiniteDifferenceMatrices(s, 'equispaced');
X_s{2} = blkdiag(ds1,ds1,ds1) * X(:);       MX_s{2} = VectorToMatrix(X_s{2});
X_ss{2} = blkdiag(ds2,ds2,ds2) * X(:);      MX_ss{2} = VectorToMatrix(X_ss{2});
X_sss{2} = blkdiag(ds3,ds3,ds3) * X(:);     MX_sss{2} = VectorToMatrix(X_sss{2});
X_ssss{2} = blkdiag(ds4,ds4,ds4) * X(:);    MX_ssss{2} = VectorToMatrix(X_ssss{2});

% Ensure vectors point the same direction:
MX_s{2}(2,:) = -MX_s{2}(2,:);
MX_ss{2}(2,:) = -MX_ss{2}(2,:);

% Compute dot products between tangents and normals:
for n = 1:N
    dot_tt(n) = dot(MX_s{1}(:,n),MX_s{2}(:,n));
    dot_tn(n) = dot(MX_s{1}(:,n),MX_ss{2}(:,n));
    dot_nt(n) = dot(MX_ss{1}(:,n),MX_s{2}(:,n));
end

%% Plots:

figure;
subplot(1,2,1);
plot(N_vals, dot_tt,'.-');
xlabel('N')
ylabel('dot(t_1,t_2)')

subplot(1,2,2);
plot(N_vals, dot_tn,'.-'); hold on;
plot(N_vals, dot_nt,'.-')
xlabel('N')
ylabel('dot(t,n)');
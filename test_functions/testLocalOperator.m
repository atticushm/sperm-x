% TEST_LocalOperator

% Test function for verifying the Local Operator Derivative code.

clear all; clc
disp('-----')

num_runs = 8;
N_vals(1) = 11;
for n = 1:num_runs-1
    N_vals(n+1) = 2*N_vals(n)-1;    % double the number of segments on each run.
end

% Choose number of nodes for quadrature:
num_quad = 4e4;

% Choose step size for numerical differentiation:
h = 1e-1;

T = 1;                              % number of runs to average wall time across.
for t = 1:T
    textwaitbar(t-1,T,'Repeat loop')
    for k = 1:2
        for n = 1:num_runs

            N = N_vals(n);      % number of nodes/field points.
            Q = N-1;            % number of segments/source points.
            ds = 1/Q;           % segment length.
            epsilon = 1e-2;     % regularisation parameter.

            % Get accurate location of nodes by sampling from hi-res curves:
            s = linspace(0,1,N); s = s(:);
            if k == 1
                X = ParabolicCurve(0,N); 
            elseif k == 2
                X = SinusoidalCurve(3*pi,0.2,N);
            end
            [x,y,z] = extractComponents(X);

            % Spline from coordinate data:
            ppx = spline(s,x);  ppy = spline(s,y);  ppz = spline(s,z);
            dpx = fnder(ppx,1); dpy = fnder(ppy,1); dpz = fnder(ppz,1);

            % Choose q as L/10:
            q = 1/10;

            % Choose field points as nodes:
            field_id = find(s==0.6);
            s_field = s(field_id);
            X_field = [x(field_id); y(field_id); z(field_id)];  XM_field = VectorToMatrix(X_field);
            [x_field, y_field, z_field] = extractComponents(X_field);
            T_field = [ppval(dpx,s_field); ppval(dpy,s_field); ppval(dpz,s_field)]; TM_field = VectorToMatrix(T_field);
            num_field = length(field_id);

            % Find segment midponts:
            s_midp = 0.5*(s(1:end-1)+s(2:end));
            X_midp = [ppval(ppx,s_midp); ppval(ppy,s_midp); ppval(ppz,s_midp)]; XM_midp = VectorToMatrix(X_midp);
            T_midp = [ppval(dpx,s_midp); ppval(dpy,s_midp); ppval(dpz,s_midp)];
            th_midp = asin(T_midp(Q+1:2*Q));
            R_midp = RotationMatrix(th_midp);

            % Interval of integration around the field point:
            s_a = s_field-q;        s_b = s_field+q;
            s_a(s_field < q) = 0;   s_b(s_field > 1-q) = 1;

            % Derivative wrt s at s_a and s_b:
            ds_a = ones(N,1);       ds_b = ones(N,1);
            ds_a(s<q) = 0;          ds_b(s>1-q) = 0;

            % Coordinates at limits of integration are:
            X_a(:,n) = [ppval(ppx,s_a); ppval(ppy,s_a); ppval(ppz,s_a)];
            X_b(:,n) = [ppval(ppx,s_b); ppval(ppy,s_b); ppval(ppz,s_b)];

            %% (i)
            % geometrically approximate.

            tic;
            local_id = abs(s_midp-s_field')<=q;

            % Compute local integral derivative using the Leibniz rule:
            S_a = RegStokeslets(X_field,X_a(:,n),epsilon) .* ds_a(field_id);
            S_b = RegStokeslets(X_field,X_b(:,n),epsilon) .* ds_b(field_id);

            s_source = s_midp(local_id);
            num_source = length(s_source);
            X_source = MatrixToVector(XM_midp(:,local_id));
            R_source = RotationMatrix(th_midp(local_id));

            I = RegStokesletAnalyticDerivativeIntegrals(X_field,X_source,T_field,R_source,ds/2,epsilon);
            fana{n,k} = S_b - S_a + I*kron(eye(3),ones(num_source,1));
            wt_fana{n,k,t} = toc;

            %% (iv) 
            % geometrically exact, using hi-res curve.

            tic;

            % Determine X(s+-h) around the field point (for differentiation):
            X_sph = [ppval(ppx,s_field+h); ppval(ppy,s_field+h); ppval(ppz,s_field+h)];
            X_smh = [ppval(ppx,s_field-h); ppval(ppy,s_field-h); ppval(ppz,s_field-h)];
            X_sp2h = [ppval(ppx,s_field+2*h); ppval(ppy,s_field+2*h); ppval(ppz,s_field+2*h)];
            X_sm2h = [ppval(ppx,s_field-2*h); ppval(ppy,s_field-2*h); ppval(ppz,s_field-2*h)];

            % Limits of integration are usually s+q and s-q. However, here we need to
            % integrate about s-h-q and s-h+q, and s+h-q and s+h.
            % Number of nodes for Guass-Legendre quadrature:
            smh_a = s_field -h -q;  smh_b = s_field -h +q;
            sm2h_a = s_field -2*h -q; sm2h_b = s_field -2*h +q;
            sph_a = s_field +h -q;  sph_b = s_field +h +q;
            sp2h_a = s_field +2*h -q; sp2h_b = s_field +2*h +q;
            [smh_i, w_smh]   = lgwt(num_quad,smh_a,smh_b);   [smh_i, idx]  =  sort(smh_i);  w_smh  = w_smh(idx);  clear idx
            [sph_i, w_sph]   = lgwt(num_quad,sph_a,sph_b);   [sph_i, idx]  =  sort(sph_i);  w_sph  = w_sph(idx);  clear idx
            [sm2h_i, w_sm2h] = lgwt(num_quad,sm2h_a,sm2h_b); [sm2h_i, idx] =  sort(sm2h_i); w_sm2h = w_sm2h(idx); clear idx
            [sp2h_i, w_sp2h] = lgwt(num_quad,sp2h_a,sp2h_b); [sp2h_i, idx] =  sort(sp2h_i); w_sp2h = w_sp2h(idx); clear idx

            % Integration nodes via splining:
            xi_smh  = ppval(ppx,smh_i);   yi_smh  = ppval(ppy,smh_i);   zi_smh  = ppval(ppz,smh_i);
            xi_sph  = ppval(ppx,sph_i);   yi_sph  = ppval(ppy,sph_i);   zi_sph  = ppval(ppz,sph_i);
            xi_sm2h = ppval(ppx,sm2h_i);  yi_sm2h = ppval(ppy,sm2h_i);  zi_sm2h = ppval(ppz,sm2h_i);
            xi_sp2h = ppval(ppx,sp2h_i);  yi_sp2h = ppval(ppy,sp2h_i);  zi_sp2h = ppval(ppz,sp2h_i);

            Xi_smh  = [xi_smh(:); yi_smh(:); zi_smh(:)];        XMi_smh  = VectorToMatrix(Xi_smh);
            Xi_sph  = [xi_sph(:); yi_sph(:); zi_sph(:)];        XMi_sph  = VectorToMatrix(Xi_sph);
            Xi_sm2h = [xi_sm2h(:); yi_sm2h(:); zi_sm2h(:)];     XMi_sm2h = VectorToMatrix(Xi_sm2h);
            Xi_sp2h = [xi_sp2h(:); yi_sp2h(:); zi_sp2h(:)];     XMi_sp2h = VectorToMatrix(Xi_sp2h);

            % Compute integrals
            % vectorised
            w_sph = repmat(w_sph',3,3);     w_sp2h = repmat(w_sp2h',3,3);
            w_smh = repmat(w_smh',3,3);     w_sm2h = repmat(w_sm2h',3,3);
            int = 1/12*w_sm2h.*RegStokeslets(X_sm2h,Xi_sm2h,epsilon) -2/3*w_smh.*RegStokeslets(X_smh,Xi_smh,epsilon) ...
                  +2/3*w_sph.*RegStokeslets(X_sph, Xi_sph, epsilon)-1/12*w_sp2h.*RegStokeslets(X_sp2h,Xi_sp2h,epsilon);
            fnum{n,k,t} = 1/h*int*kron(eye(3),ones(num_quad,1));

            %% (ii)
            % Leibniz rule with integral term computed as the numerical
            % integral of analytic derivatives. 
            % Geometrically exact.

            tic;

            % Lookup Gauss-Legendre nodes and weights in the interval of integration:
            [si,wi] = lgwt(num_quad,s_a,s_b);
            [si,idx] = sort(si);    wi = wi(idx);
            Xi = [ppval(ppx,si); ppval(ppy,si); ppval(ppz,si)]; XMi = VectorToMatrix(Xi);
            
            % Compute local integral derivative using the Leibniz rule:
            S_a = RegStokeslets(X_field,X_a(:,n),epsilon) .* ds_a(field_id);
            S_b = RegStokeslets(X_field,X_b(:,n),epsilon) .* ds_b(field_id);

            % vectorized:
            int = repmat(wi',3,3).*RegStokesletAnalyticDerivatives(X_field,Xi,epsilon,T_field);
            int = int * kron(eye(3),ones(num_quad,1));

            semq{n,k} = S_b - S_a + int;  
            wt_semq{n,k,t} = toc;

            %% (iiia) 
            % Numerical derivative of analytic integrals.
            % Geometrically approximate.

            tic;

            % Arclength points of midpoints of segments in local region:
            s_source = s_midp(local_id);
            num_source = length(s_source);

            % Determine X(s+-ah) around the field point (for differentiation):
            h = 1e-6;
            X_sph = [ppval(ppx,s_field+h); ppval(ppy,s_field+h); ppval(ppz,s_field+h)];
            X_smh = [ppval(ppx,s_field-h); ppval(ppy,s_field-h); ppval(ppz,s_field-h)];
            X_sp2h = [ppval(ppx,s_field+2*h); ppval(ppy,s_field+2*h); ppval(ppz,s_field+2*h)];
            X_sm2h = [ppval(ppx,s_field-2*h); ppval(ppy,s_field-2*h); ppval(ppz,s_field-2*h)];

            % Coordinates of source points for each integral:
            smh_c = s_source-h;     sm2h_c = s_source-2*h;
            sph_c = s_source+h;     sp2h_c = s_source+2*h;
            Xsmh_c = [ppval(ppx,smh_c(:)); ppval(ppy,smh_c(:)); ppval(ppz,smh_c(:))];
            Xsm2h_c = [ppval(ppx,sm2h_c(:)); ppval(ppy,sm2h_c(:)); ppval(ppz,sm2h_c(:))];
            Xsph_c = [ppval(ppx,sph_c(:)); ppval(ppy,sph_c(:)); ppval(ppz,sph_c(:))];
            Xsp2h_c = [ppval(ppx,sp2h_c(:)); ppval(ppy,sp2h_c(:)); ppval(ppz,sp2h_c(:))];

            % Rotation matrices for these source points:
            ty_smh = ppval(dpy,smh_c);   th_smh = asin(ty_smh);
            ty_sph = ppval(dpy,sph_c);   th_sph = asin(ty_sph);
            ty_sm2h = ppval(dpy,sm2h_c); th_sm2h = asin(ty_sm2h);
            ty_sp2h = ppval(dpy,sp2h_c); th_sp2h = asin(ty_sp2h);
            R_smh = RotationMatrix(th_smh);
            R_sph = RotationMatrix(th_sph);
            R_sm2h = RotationMatrix(th_sm2h);
            R_sp2h = RotationMatrix(th_sp2h);

            % Numerically differentiate:
            int_sph  = RegStokesletAnalyticIntegrals(X_sph,Xsph_c,ds/2,R_sph,epsilon);
            int_smh  = RegStokesletAnalyticIntegrals(X_smh,Xsmh_c,ds/2,R_smh,epsilon);
            int_sp2h = RegStokesletAnalyticIntegrals(X_sp2h,Xsp2h_c,ds/2,R_sp2h,epsilon);
            int_sm2h = RegStokesletAnalyticIntegrals(X_sm2h,Xsm2h_c,ds/2,R_sm2h,epsilon);
            semd{n,k} = (1/h * (1/12*int_sm2h -2/3*int_smh + 2/3*int_sph -1/12*int_sp2h))*kron(eye(3),ones(num_source,1));
            wt_semd{n,k,t} = toc;

            %% (iiib)
            % but with X(s+-ah) determined via linear interp rather than
            % spline functions.
            
            tic;
            
            % Arclength field points same as in (iii). 
            % Determine X(s+-ah) around field point by linear
            % approximation:
            X_sm2h = [interp1(s,x,s_field-2*h,'linear','extrap'); interp1(s,y,s_field-2*h,'linear','extrap'); interp1(s,z,s_field-2*h,'linear','extrap')];
            X_smh  = [interp1(s,x,s_field-h  ,'linear','extrap'); interp1(s,y,s_field-h  ,'linear','extrap'); interp1(s,z,s_field-h  ,'linear','extrap')];
            X_sph  = [interp1(s,x,s_field+h  ,'linear','extrap'); interp1(s,y,s_field+h  ,'linear','extrap'); interp1(s,z,s_field+h  ,'linear','extrap')];
            X_sp2h = [interp1(s,x,s_field+2*h,'linear','extrap'); interp1(s,y,s_field+2*h,'linear','extrap'); interp1(s,z,s_field+2*h,'linear','extrap')];

            % Coordinates of source points for each integral:
            smh_c = s_source-h;     sm2h_c = s_source-2*h;
            sph_c = s_source+h;     sp2h_c = s_source+2*h;
            Xsmh_c = [ppval(ppx,smh_c(:)); ppval(ppy,smh_c(:)); ppval(ppz,smh_c(:))];
            Xsm2h_c = [ppval(ppx,sm2h_c(:)); ppval(ppy,sm2h_c(:)); ppval(ppz,sm2h_c(:))];
            Xsph_c = [ppval(ppx,sph_c(:)); ppval(ppy,sph_c(:)); ppval(ppz,sph_c(:))];
            Xsp2h_c = [ppval(ppx,sp2h_c(:)); ppval(ppy,sp2h_c(:)); ppval(ppz,sp2h_c(:))];
            
            % Lookup Gauss-Legendre nodes and weights in the interval of integration:
            [si,wi] = lgwt(num_quad,s_a,s_b);
            [si,idx] = sort(si);    wi = wi(idx);
            Xi = [ppval(ppx,si); ppval(ppy,si); ppval(ppz,si)]; XMi = VectorToMatrix(Xi);
            
            % Rotation matrices for these source points:
            ty_smh = ppval(dpy,smh_c);   th_smh = asin(ty_smh);
            ty_sph = ppval(dpy,sph_c);   th_sph = asin(ty_sph);
            ty_sm2h = ppval(dpy,sm2h_c); th_sm2h = asin(ty_sm2h);
            ty_sp2h = ppval(dpy,sp2h_c); th_sp2h = asin(ty_sp2h);
            R_smh = RotationMatrix(th_smh);
            R_sph = RotationMatrix(th_sph);
            R_sm2h = RotationMatrix(th_sm2h);
            R_sp2h = RotationMatrix(th_sp2h);
            
            % Numerically differentiate:
            int_sph  = RegStokesletAnalyticIntegrals(X_sph,Xsph_c,ds/2,R_sph,epsilon);
            int_smh  = RegStokesletAnalyticIntegrals(X_smh,Xsmh_c,ds/2,R_smh,epsilon);
            int_sp2h = RegStokesletAnalyticIntegrals(X_sp2h,Xsp2h_c,ds/2,R_sp2h,epsilon);
            int_sm2h = RegStokesletAnalyticIntegrals(X_sm2h,Xsm2h_c,ds/2,R_sm2h,epsilon);
            semdl{n,k} = (1/h * (1/12*int_sm2h -2/3*int_smh + 2/3*int_sph -1/12*int_sp2h))*kron(eye(3),ones(num_source,1));
            wt_semdl{n,k,t} = toc;
            
            %% (extra) numerical leibniz:
            % geometrically exact, numerically approximate.

            tic;
            
            % Determine X(s+-ah) around the field point (for differentiation):
            X_sph = [ppval(ppx,s_field+h); ppval(ppy,s_field+h); ppval(ppz,s_field+h)];
            X_smh = [ppval(ppx,s_field-h); ppval(ppy,s_field-h); ppval(ppz,s_field-h)];
            X_sp2h = [ppval(ppx,s_field+2*h); ppval(ppy,s_field+2*h); ppval(ppz,s_field+2*h)];
            X_sm2h = [ppval(ppx,s_field-2*h); ppval(ppy,s_field-2*h); ppval(ppz,s_field-2*h)];
            
            int = repmat(wi',3,3).*(1/12*RegStokeslets(X_sm2h,Xi,epsilon) -2/3*RegStokeslets(X_smh,Xi,epsilon) ...
                                    +2/3*RegStokeslets(X_sph,Xi,epsilon) -1/12*RegStokeslets(X_sp2h,Xi,epsilon));
            int = int * kron(eye(3),ones(num_quad,1));
            nlei{n,k} = S_b - S_a + 1/h*int;
            wt_nlei{n,k,t} = toc;   
            
            %% (extra) using LocalOperator.m

            [~,R_s] = LocalOperator(X, s, q, epsilon);
            func{n,k} = [R_s(field_id,:); R_s(field_id+N,:); R_s(field_id+2*N,:)];

        end 
    end 
end

%% Plots:

close all;

% Compute errors at coinciding nodes.
for k = 1:2
    mult = 2^7;
    for n = 1:num_runs
        N = length(fnum{end,k})/3;
        idx = 1:mult:N;
        mult = mult/2;
        num_comp{1} = fnum{end,k}(idx,:);
        num_comp{2} = fnum{end,k}(N+idx,:);
        num_comp{3} = fnum{end,k}(2*N+idx,:);
        comp{n,k} = [num_comp{1}; num_comp{2}; num_comp{3}];
        error_fana(n,k) = norm(abs(fana{n,k} -comp{n,k}));
        error_semq(n,k) = norm(abs(semq{n,k} -comp{n,k}));
        error_semd(n,k) = norm(abs(semd{n,k} -comp{n,k}));
        error_semdl(n,k) = norm(abs(semdl{n,k} -comp{n,k}));
        error_func(n,k) = norm(abs(func{n,k} -fana{n,k}));
        error_nlei(n,k) = norm(abs(nlei{n,k} -comp{n,k}));
    end
end

% Parabolic curves error:
f = figure; 
subplot(2,2,1);
loglog(N_vals, error_fana(:,1), '.-','LineWidth',1.3); grid on; box on; hold on;
loglog(N_vals, error_semq(:,1), '.--','LineWidth',1.3) 
loglog(N_vals, error_semd(:,1),'.--','LineWidth',1.3)
loglog(N_vals, error_semdl(:,1),'.--','LineWidth',1.3)
xlabel('$Q+1$','Interpreter','latex')
ylabel('Error vs. numerical method','Interpreter','latex')
titlestr = ['(a) $\mathbf{R}^{-1}_s(s,q)$, $s=\,$',num2str(s_field),', $q=\,$',num2str(q),', parabolic curve'];
title(titlestr,'Interpreter','latex')
legend('(i)','(ii)','(iiia)','(iiib)','Interpreter','latex')
f.CurrentAxes.TickLabelInterpreter = 'latex';
f.CurrentAxes.FontSize = 12;

% Sinusoidal curves error:
subplot(2,2,2);
loglog(N_vals, error_fana(:,2), '.-','LineWidth',1.3); grid on; box on; hold on;
loglog(N_vals, error_semq(:,2), '.-','LineWidth',1.3) 
loglog(N_vals, error_semd(:,2),'.--','LineWidth',1.3)
loglog(N_vals, error_semdl(:,2),'.--','LineWidth',1.3)
xlabel('$Q+1$','Interpreter','latex')
ylabel('Error vs. numerical method','Interpreter','latex')
titlestr = ['(b) $\mathbf{R}^{-1}_s(s,q)$, $s=\,$',num2str(s_field),', $q=\,$',num2str(q),', sinusoidal curve'];
title(titlestr,'Interpreter','latex')
legend('(i)','(ii)','(iiia)','(iiib)','Interpreter','latex')
f.CurrentAxes.TickLabelInterpreter = 'latex';
f.CurrentAxes.FontSize = 12;
% f.CurrentAxes.YTick = [10^-8,10^-6,10^-4,10^-2,10^0];
% f.CurrentAxes.YLim  = [10^-8,10^0];

% Walltime plot (parabolic curve):
if isa(wt_fana,'cell')
    wt_fana = cell2mat(wt_fana); wt_semq = cell2mat(wt_semq); wt_semd = cell2mat(wt_semd); wt_semdl = cell2mat(wt_semdl);
    T = size(wt_fana,3);
    for t = 1:T % wall time average across T runs:
        for n = 1:num_runs
            wta_fana(n,1) = mean(wt_fana(n,1,:));  wta_fana(n,2) = mean(wt_fana(n,2,:));
            wta_semq(n,1) = mean(wt_semq(n,1,:));  wta_semq(n,2) = mean(wt_semq(n,2,:));
            wta_semd(n,1) = mean(wt_semd(n,1,:));  wta_semd(n,2) = mean(wt_semd(n,2,:));
            wta_semdl(n,1)= mean(wt_semdl(n,1,:)); wta_semdl(n,2)= mean(wt_semdl(n,2,:));
        end
    end
end
subplot(2,2,3)
loglog(N_vals, wta_fana(:,1), '.-','LineWidth',1.3); grid on; box on; hold on;
loglog(N_vals, wta_semq(:,1), '.-','LineWidth',1.3) 
loglog(N_vals, wta_semd(:,1),'.--','LineWidth',1.3)
loglog(N_vals, wta_semdl(:,1),'.--','LineWidth',1.3)
xlabel('$Q+1$','Interpreter','latex')
ylabel('Time (s)','Interpreter','latex')
legend('(i)','(ii)','(iiia)','(iiib)','Interpreter','latex')
title('(c) Average wall time, parabolic curve','Interpreter','latex')
f.CurrentAxes.TickLabelInterpreter = 'latex';
f.CurrentAxes.FontSize = 12;
f.CurrentAxes.YLim = [10^-3.5,10^-1.5];
f.CurrentAxes.Legend.Location = 'northwest';

% Walltime plot (sinusoidal curve):
subplot(2,2,4)
loglog(N_vals, wta_fana(:,2), '.-','LineWidth',1.3); grid on; box on; hold on;
loglog(N_vals, wta_semq(:,2), '.-','LineWidth',1.3) 
loglog(N_vals, wta_semd(:,2),'.--','LineWidth',1.3)
loglog(N_vals, wta_semdl(:,2),'.--','LineWidth',1.3)
xlabel('$Q+1$','Interpreter','latex')
ylabel('Time (s)','Interpreter','latex')
legend('(i)','(ii)','(iiia)','(iiib)','Interpreter','latex')
title('(d) Average wall time, sinusoidal curve','Interpreter','latex')
f.CurrentAxes.TickLabelInterpreter = 'latex';
f.CurrentAxes.FontSize = 12;
f.CurrentAxes.YLim = [10^-3.5,10^-1.5];
f.CurrentAxes.Legend.Location = 'northwest';

% Set figure size:
f.Position = [442 530 1118 808];


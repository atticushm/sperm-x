% TEST FUNCTION
% Transforming the grand resistance matrix.
% What is the correct way to transform R as the cell head moves in the lab
% frame?

% We can construct an ellipsoidal head in the body-frame (where the head is
% fixed). Does applying the rotation transformation to this grand
% resistance matrix match the grand resistance matrix computed from the
% pre-rotated cell head?

% Error reduces as disretisation levels increase.

clc
clear variables
close all

%% Setup

axes = [2/45; 1.6/45; 1/45]; % typical human sperm cell head dimensions.
eps = 0.01; % regularisation parameter.

n_t_face = 10;    % number of nodes per face in cubic projection.
n_q_face = 20;
X_t = GenerateSphereDisc(n_t_face, 1); % generate discretisation of a unit sphere.
X_q = GenerateSphereDisc(n_q_face, 1);
XM_t = MatrixToVector(X_t);     XM_q = MatrixToVector(X_q);
N_t = length(X_t)/3;            N_q = length(X_q)/3;

disp('-----')
disp(['Traction discretisation obtained from ',num2str(n_t_face),'-squared cubic projection'])
disp(['Quadrature discretisation obtained from ',num2str(n_q_face),'-squared cubic projection'])
disp('-----')

% Generate ellipsoidal discretisation:
X_t_0 = X_t.*kron(axes,ones(N_t,1)); 
X_q_0 = X_q.*kron(axes,ones(N_q,1));
disp(['# of traction nodes is ',num2str(N_t)])
disp(['# of quadrature nodes is ',num2str(N_q)])
disp('-----')

% Move ellipsoid away from origin:
X_c = [0.1; 0.2; 0];
X_t_c = X_t_0 + kron(X_c,ones(N_t,1));
X_q_c = X_q_0 + kron(X_c,ones(N_q,1));
[xc1,xc2,xc3] = extractComponents(X_t_c);

% Prescribe rotations about origin:
th_x = pi/4;    th_z = pi/4;
B_z = [cos(th_z) -sin(th_z) 0; sin(th_z) cos(th_z) 0; 0 0 1]; % rotation about z axis.
B = B_z';

% Centre of rotation:
% r_B = [0.2; 0.05; 0];
r_B = [-axes(1);0;0]+X_c;

disp(['Rotating about (',num2str(r_B(1)),',',num2str(r_B(2)),',',num2str(r_B(3)),')'])
disp('Rotation matrix is:')
disp(B)
disp('-----')

%% Compute R from rotated nodes:
% This method would require re-computing R at each time step - which would
% be expensive.

% Rotate nodes about X_c:
X_t_rot = RotatePoints(X_t_c, B, r_B); 
X_q_rot = RotatePoints(X_q_c, B, r_B);

[xr1,xr2,xr3] = extractComponents(X_t_rot);

% Calculate R for lab-frame cell head:
R_lab = ComputeR(X_t_rot, X_q_rot, r_B, eps);

% PRINT R_LAB
disp('Lab-frame R after body rotation is:')
disp(R_lab)

% Extract submatrices:
RFU_lab = R_lab(1:3,1:3);
RFO_lab = R_lab(1:3,4:6);
RMU_lab = R_lab(4:6,1:3);
RMO_lab = R_lab(4:6,4:6);

%% Compute R via transforming R0:
% This method means that calculation of R (via solving 6 resistance
% problems) only need be done once - R for the moving cell head (in the lab
% frame) can be calculated simply by a transformation.

% Calculate R for body-frame cell head:
R_body = ComputeR(X_t_c, X_q_c, r_B, eps);

% Transform R:
R_trans = TransformR(R_body, B, [0 0 0]');

% PRINT R_TRANS
disp('Body-frame R after transformations is:')
disp(R_trans)

%% View ellipsoids.


figure; hold on; box on;
scatter3(r_B(1), r_B(2), r_B(3), 'ro', 'filled')
scatter3(0,0,0,'ko','filled')

scatter3(xc1,xc2,xc3,'.');
scatter3(xr1,xr2,xr3,'.');

xlabel('x'); ylabel('y'); zlabel('z')
axis equal; view(2)

lgd = legend('origin','centre of rotation','pre-rotation','post-rotation');
lgd.NumColumns = 4;
lgd.Location = 'southoutside';
set(gcf,'Position',[517 751 1043 587])

%% Compute errors

R_diff = abs(R_trans - R_lab);
disp('R error is:')
disp(R_diff)
disp('-----')

% END OF SCRIPT.
% TEST FUNCTION
%
% APPROXIMATING HIGH ORDER DERIVATIVES USING SPLINES AND FINITE
% DIFFERENCES.
%
% There is a concern that the piecewise polynomial functions returned by
% the spline function are not high enough order to be differentiated as
% many times as I require in the Sperm-X formulation.
%
% This test function seeks to investigate this by computing derivatives to
% an analytically differentiable function, comparing to both the analytical
% solutions and those from a finite difference scheme.

clear all;
close all;
printLinebreak

%% Continouously differentiable function:

func = @(x) sin(2*pi*x);

d1_func = @(x) 2*pi*cos(2*pi*x);
d2_func = @(x) -4*pi^2*sin(2*pi*x);
d3_func = @(x) -8*pi^3*cos(2*pi*x);
d4_func = @(x) 16*pi^4*sin(2*pi*x);

%% Spatial discretisation:

% equispaced:
M = 50;
xi = linspace(0,1,M);
h = 1/(M-1);

% non-equispaced:
rand_amount = 1/2/M;
% rand_amount = 0;
xi_arb = [xi(1), xi(2:M-1) + rand_amount*rand(1,M-2), xi(M)]; xi_arb = xi_arb(:);
h_arb = diff(xi_arb);
h_arb = h_arb(:);

% Check arbitrary nodes are correctly sorted (just in case):
if xi_arb ~= sort(xi_arb)
    warning('Arbitrary nodes have become unordered! Reroll.')
end

%% Derivatives from analytical solution:

d1_an = d1_func(xi);
d2_an = d2_func(xi);
d3_an = d3_func(xi);
d4_an = d4_func(xi);

%% Derivatives from splining w/ spline:

yi = func(xi);
pp = spline(xi,yi);
d1_pp = ppval(fnder(pp,1),xi);
d2_pp = ppval(fnder(pp,2),xi);
d3_pp = ppval(fnder(pp,3),xi);
d4_pp = ppval(fnder(pp,4),xi);

%% Derivatives from splining w/ spapi:

ps = spapi(6,xi,yi);
d1_ps = fnval(fnder(ps,1),xi);
d2_ps = fnval(fnder(ps,2),xi);
d3_ps = fnval(fnder(ps,3),xi);
d4_ps = fnval(fnder(ps,4),xi);

%% Derivatives from finite differences (equispaced grid):

D_1 = -(1/2) * diag(ones(M-1,1),-1) + ...
       (1/2) * diag(ones(M-1,1), 1) ; 
D_1(1,:) = [-3/2, 2, -1/2, zeros(1,M-3)];
D_1(M,:) = -fliplr(D_1(1,:));
D_1 = 1/h * D_1;
   
D_2 = diag(ones(M-1,1),-1) + ...
      -2 * diag(ones(M,1),0) + ...
      diag(ones(M-1,1), 1) ;
D_2(1,:) = [35/12, -26/3, 19/2, -14/3, 11/12, zeros(1,M-5)];
D_2(M,:) = fliplr(D_2(1,:));
D_2 = 1/h^2 * D_2;
  
D_3 = -(1/2) * diag(ones(M-2,1),-2) + ...
      diag(ones(M-1,1),-1) + ...
      -diag(ones(M-1,1), 1) + ...
      (1/2) * diag(ones(M-2,1),2) ;
D_3(1,:) = [-17/4, 71/4, -59/2, 49/2, -41/4, 7/4, zeros(1,M-6)];
D_3(2,:) = [0, -17/4, 71/4, -59/2, 49/2, -41/4, 7/4, zeros(1,M-7)];
D_3(M,:) = -fliplr(D_3(1,:));
D_3(M-1,:) = -fliplr(D_3(2,:));
D_3 = 1/h^3 * D_3;
  
D_4 = diag(ones(M-2,1),-2) + ...
      -4 * diag(ones(M-1,1),-1) + ...
      6 * diag(ones(M,1),0) + ...
      -4 * diag(ones(M-1,1),1) + ...
      diag(ones(M-2,1),2);
D_4(1,:) = [35/6, -31, 137/2, -242/3, 107/2, -19, 17/6, zeros(1,M-7)];
D_4(2,:) = [0, 35/6, -31, 137/2, -242/3, 107/2, -19, 17/6, zeros(1,M-8)];
D_4(M,:) = fliplr(D_4(1,:));
D_4(M-1,:) = fliplr(D_4(2,:));
D_4 = 1/h^4 * D_4;

% Approximate derivatives:
yi = func(xi);
d1_fd = D_1 * yi(:);
d2_fd = D_2 * yi(:);
d3_fd = D_3 * yi(:);
d4_fd = D_4 * yi(:);

%% Derivatives from finite differences (arbitrary grid) (method #1):

% ** note: no endpoint approximation currently implemented **

% f': (via Sundqvist et al)
h = zeros(M,1); hm1 = zeros(M,1);
r = zeros(M,1); r2 = zeros(M,1);
for i = 2:M-1
    % Difference terms:
    h(i) = abs(xi_arb(i+1)-xi_arb(i));
    hm1(i) = abs(xi_arb(i)-xi_arb(i-1));
    r(i) = h(i)/hm1(i);
    r2(i) = r(i)^2; 
end
D_1 = 1./(h.*(1+r)) .* (diag(-r2(2:M).*ones(M-1,1),-1) + ...
                        diag(-(1-r2).*ones(M,1),0) + ...
                        diag(ones(M-1,1),+1));
                    
% one-sided forward difference at left boundary (approx 2nd order):
% h = abs(xi_arb(2)-xi_arb(1));
% hp1 = abs(xi_arb(3)-xi_arb(2));
% rp1 = hp1/h;
% rp12 = rp1^2;
% D_1(1,:) = 1./(hp1+h*rp12) .* [-(2*rp1+rp12), 1+2*rp1+rp12, -1, zeros(1,M-3)];

% one-sided difference (alternate h formulation):
% hp1 = abs(xi_arb(1)-xi_arb(2));
% hp2 = hp1 + abs(xi_arb(2)-xi_arb(3));
% D_1(1,:) = 1./(hp1*hp2^2 - hp1^2*hp2) .* [-(hp2^2 - hp1^2), hp2^2, -hp1^2, zeros(1,M-3)];
% % 
% hm1 = abs(xi_arb(M)-xi_arb(M-1));
% hm2 = hm1 + abs(xi_arb(M-2) - xi_arb(M-3));
% D_1(M,:) = 1./(hm2*hm1^2 - hm1*hm2^2) .* [zeros(1,M-3), -hm1^2, hm2^2, -(hm2^2-hm1^2)];

% one sided difference (alternate h formulation, higher order scheme)
hp1 = abs(xi_arb(1)-xi_arb(2));
hp2 = hp1 + abs(xi_arb(2)-xi_arb(3));
hp3 = hp2 + abs(xi_arb(3)-xi_arb(4));
c1 = 0.5 * hp2^2 * hp3^2;
c2 = 0.5 * hp1^2 * hp3^2;
c3 = - hp1^2 * hp2^2;
D_1(1,:) = 1./(c1*hp1 + c2*hp2 + c3*hp3) .* [-(c1+c2+c3), c1, c2, c3, zeros(1,M-4)];

hm1 = abs(xi_arb(M)-xi_arb(M-1));
hm2 = hp1 + abs(xi_arb(M-1)-xi_arb(M-2));
hm3 = hp2 + abs(xi_arb(M-2)-xi_arb(M-3));
c1 = 0.5 * hm2^2 * hm3^2;
c2 = 0.5 * hm1^2 * hm3^2;
c3 = - hm1^2 * hm2^2;
D_1(M,:) = -1./(c1*hm1 + c2*hm2 + c3*hm3) .* [zeros(1,M-4), c3, c2, c1, -(c1+c2+c3)];

% one-sided backward difference at right boundary:
% hm1 = abs(xi_arb(M-1)-xi_arb(M));
% hm2 = abs(xi_arb(M-2)-xi_arb(M-1));
% rm1 = hm1/hm2;
% irm1 = 1./rm1;
% irm12 = irm1^2;
% D_1(M,:) = -1./(hm2+hm1*irm12) .* [zeros(1,M-3), -1, 1+2*irm1+irm12, -(2*irm1+irm12)];

% f'': (via Sundqvist et al)
%{x
h = zeros(M,1); hm1 = zeros(M,1);
r = zeros(M,1);
for i = 2:M-1
    % Difference terms:
    h(i) = abs(xi_arb(i+1)-xi_arb(i));
    hm1(i) = abs(xi_arb(i)-xi_arb(i-1));
    r(i) = h(i)/hm1(i); 
end
D_2 = 2./(h.*hm1.*(1+r)) .* (diag(r(2:M).*ones(M-1,1),-1) + ...
                             diag(-(1+r).*ones(M,1),0) + ...
                             diag(ones(M-1,1),+1));
                         
D_2_brute = D_1 * D_1;
D_2(1,:) = D_2_brute(1,:);
D_2(M,:) = D_2_brute(M,:);
%}


% Approximate first and second derivatives:
yi_arb = func(xi_arb);
d1_fda = D_1 * yi_arb(:);
d2_fda = D_2 * yi_arb(:);

% Approximate higher order derivatives via repeated application of first
% order derivative difference scheme:
d3_fda = D_1 * D_2 * yi_arb(:);
d4_fda = D_2 * D_2 * yi_arb(:);

%% Analysis:

% Plots:
fig = figure; 
subplot(2,2,1); box on; hold on;
plot(xi,d1_an,'LineWidth',1.2);
plot(xi,d1_pp,'--');
plot(xi,d1_ps);
plot(xi,d1_fd,'-.');
plot(xi_arb,d1_fda,'.-')
ylabel('\(f_x\)','Interpreter','latex','FontSize',14)

subplot(2,2,2); box on; hold on;
plot(xi,d2_an,'LineWidth',1.2);
plot(xi,d2_pp,'--');
plot(xi,d2_ps);
plot(xi,d2_fd,'-.');
plot(xi_arb,d2_fda,'.-')
ylabel('\(f_{xx}\)','Interpreter','latex','FontSize',14)

subplot(2,2,3); box on; hold on;
plot(xi,d3_an,'LineWidth',1.2);
plot(xi,d3_pp,'--');
plot(xi,d3_ps);
plot(xi,d3_fd,'-.');
plot(xi_arb,d3_fda,'.-')
ylabel('\(f_{xxx}\)','Interpreter','latex','FontSize',14)

subplot(2,2,4); box on; hold on;
plot(xi,d4_an,'LineWidth',1.2);
plot(xi,d4_pp,'--');
plot(xi,d4_ps);
plot(xi,d4_fd,'-.');
plot(xi_arb,d4_fda,'.-')
% plot(xi_arb(3:end-2),d4_fda(3:end-2),'.-')
ylabel('\(f_{xxxx}\)','Interpreter','latex','FontSize',14)

% Labels and axes:
ax = findobj(fig,'Type','Axes');
for j = 1:length(ax)
    xlabel(ax(j),{'\(x\)'},'Interpreter','latex','FontSize',14)
%     ylabel(ax(j),{'\(y\)'},'Interpreter','latex','FontSize',14)
    legend(ax(j),{'analytic','\texttt{spline}','\texttt{spapi}','finite diffs. (equi)','finite diffs. (arb)'},'interpreter','latex','Location','northeast')
end

sgtitle('Derivatives of f(x) = sin(2\pi x)')
set(gcf,'Position',[443 638 1117 686]);

printLinebreak
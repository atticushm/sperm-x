% TEST FUNCTION

% Determining the difference between L.G.L. theory and regularised
% stokeslet segments for a single/multiply connected filament segments.

% LGL is the operator (1/c_perp)*(I+(gamma-1)tt) (identity plus dyadic)
% with the paramters c_perp and gamma found using the LGL formulas (similar
% to Gray and Hancock.

% RSL is the operator (1/C_perp)*(I+(Gamma-1)tt) with the parameters
% determined via analytically integrated a stokeslet along a straight line
% segment (see Smith 2009 App B).

% RSI is the integrated stokeslet segment int_-ds^ds S(x,X) ds, with the
% interval of integration approximated using straight line segments.

clc; close all;
printBanner('LGL vs regularised stokeslet segments','~')

% segment a curve:
N = 160;
% X = InitialCondition('parabolic', N);
X = InitialCondition('flat',N);

% arclength parametrisation:
s = linspace(0,1,N); 
ds = 1/N;

% compute tangents to the curve:
tt = TangentsToCurve(X, s);

% coordinate midpoints:
Xm = Midpoints(X);
Xm_mat = VectorToMatrix(Xm);

% tangent midpoints:
ttm = Midpoints(tt);
ttm_mat = VectorToMatrix(ttm);

% rotation matrix for each segment onto the x axis:
Rot = RotationMatrix(Xm, [1;0;0]);

% radius of filament:
b = 1e-2;

% regularisation parameter:
epsilon = 1e-2;

%% single segment:

% here we compare LGL modelling a single rod of length q=ds against the reg
% stokeslet integral across the same single segment.

disp('SINGLE SEGMENT')

% pick a segment to compare methods:
id = 14;
assert(id<=N,'segment id out of bounds!')

% choose q (size of inner region) to encompass only one segment:
q = ds;
fprintf('q = %g\n',q)

% L.G.L. parameters:
[c_perp, c_para, gamma] = CalcResistanceCoefficients(1, q, b, 'lgl');

% tangent vector for chosen segment:
ttm_id = ttm_mat(:,id);

% build L.G.L. operator for the single segment:
LGL{1} = (1/c_perp)*(eye(3) + (gamma-1)*kron(ttm_id,ttm_id'));

% rotation matrix for the chosen segment:
Rot_id = [Rot(:,id), Rot(:,id+(N-1)), Rot(:,id+2*(N-1))];

% coordinate of chosen segment midpoint:
Xm_field = Xm_mat(:,id);

% build reg stokeslet integral operator:
RSI{1} = RegStokesletAnalyticIntegrals(Xm_field, Xm_field, ds/2, Rot_id, epsilon);

% RFT but with reg stokeslet parameters (arctanh formulation):
[C_perp, C_para, Gamma] = CalcResistanceCoefficients(1, q, b, 'rss');
RSL{1} = (1/C_perp)*(eye(3) + (Gamma-1)*kron(ttm_id,ttm_id'));

% norm absolute difference:
error(1) = norm(abs(LGL{1}-RSI{1}));
error(2) = norm(abs(RSL{1}-RSI{1}));

% print to command window:
disp('LGL operator:'); disp(LGL{1})
disp('RSI operator:'); disp(RSI{1})
disp('RSL operator:'); disp(RSL{1})
disp(['LGL vs RSI: norm abs difference = ',num2str(error(1))])
disp(['RSL vs RSI: norm abs difference = ',num2str(error(2))])
printLinebreak

%% multiply connected segments:

% here we compare LGL modelling a single rod of length q=m*ds against the
% reg stokeslet integral across the length of curve of length m*ds,
% constructed of m-multiply connected segments.

disp('MULTIPLY CONNECTED SEGMENTS')

% pick a field point:
id = 14;
assert(id<=N,'segment id out of bounds!')

% choose number of segments either side of the field point to include:
id_diff = 8;
source_id = id-id_diff:id+id_diff;
num_source = length(source_id);

% choose q to encompass all source segments:
q = num_source*ds;
fprintf('q = %g\n',q)

% L.G.L. parameters:
[c_perp, ~, gamma] = CalcResistanceCoefficients(1, q, b, 'lgl');

% tangent vector for chosen segment:
ttm_id = ttm_mat(:,id);

% build L.G.L. operator for the field segment:
LGL{2} = (1/c_perp)*(eye(3) + (gamma-1)*kron(ttm_id,ttm_id'));

% rotatation matrix for the source segments:
Rot_source = [Rot(:,source_id), Rot(:,source_id+(N-1)), Rot(:,source_id+2*(N-1))];

% coordinate of field midpoint:
field_id = source_id;
Xm_field = Xm_mat(:,field_id);
Xm_id = Xm_mat(:,id);

% coordinates of source segment midpoint:
Xm_source_mat = Xm_mat(:,source_id);
Xm_source = MatrixToVector(Xm_source_mat);

% build reg stokeslet integral operator:
RSI_op = RegStokesletAnalyticIntegrals(Xm_id, Xm_source, ds/2, Rot_source, epsilon);

% sum across segments:
RSI{2} = [sum(RSI_op(:,1:num_source),2),...
          sum(RSI_op(:,num_source+1:2*num_source),2),...
          sum(RSI_op(:,2*num_source+1:3*num_source),2)  ];

% RFT but with coefficients from reg stokeslets:
[C_perp, C_para, Gamma] = CalcResistanceCoefficients(1, q, b, 'rss');
RSL{2} = (1/C_perp)*(eye(3) + (Gamma-1)*kron(ttm_id,ttm_id'));
      
% norm absolute difference:
error(3) = norm(abs(LGL{2}-RSI{2}));
error(4) = norm(abs(RSL{2}-RSI{2}));

% print to command window:
disp('LGL operator:'); disp(LGL{2})
disp('RSI operator:'); disp(RSI{2})
disp('RSL operator:'); disp(RSL{2})
disp(['LGL vs RSI: norm abs difference = ',num2str(error(3))])
disp(['RSL vs RSI: norm abs difference = ',num2str(error(4))])
printLinebreak
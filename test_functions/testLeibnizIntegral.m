% TEST_LeibnizIntegral

% Previous tests of the function RegStokesletAnalyticDerivativeIntegrals,
% which encodes the third integral term in the Leibniz rule application,
% have used arbirtary limits and field points, which could be argued to be
% an unfair or unrepresentative application of the method - in use, field
% points will always be the nodes at each end of segments, with midpoints
% of segments being the source points. Limits of the integral are dependant
% on the source point considered and not arbitrary as previously
% considered.

clear all; clc; close all
disp('-----')

num_runs = 8;
N_vals(1) = 11;
for n = 1:num_runs-1
    N_vals(n+1) = 2*N_vals(n)-1;    % double the number of segments on each run.
end

f = figure;     % create figure object for plots later.

for k = 1:2
    
    % Build a high resolution curve:
    if k == 1
        X_curve = ParabolicCurve(0.3,1e4); [x_curve,y_curve,z_curve] = extractComponents(X_curve);
    elseif k == 2
        X_curve = SinusoidalCurve(3*pi,0.2,1e4); [x_curve,y_curve,z_curve] = extractComponents(X_curve);
    end
    s_curve = linspace(0,1,1e4);
    ppx = spline(s_curve,x_curve);  ppy = spline(s_curve,y_curve);  ppz = spline(s_curve,z_curve);
    dpx = fnder(ppx,1); dpy = fnder(ppy,1); dpz = fnder(ppz,1);

    for n = 1:num_runs

        N = N_vals(n); 
        Q = N-1;
        ds = 1/Q;
        epsilon = 1e-2;

        % Choose q as a multiple of ds:
        q = 0.1;

        % Filament arclength:
        s = linspace(0,1,N); s = s(:);

        % Indentify a field point a third of the way along the arclength - by
        % choosing N odd above, this value should be consistent across runs:
        field_id = find(s==0.7);
        s_field = s(field_id);
        x = ppval(ppx,s); y = ppval(ppy,s); z = ppval(ppz,s);
        X_field = [x(field_id); y(field_id); z(field_id)];
        T_field = [ppval(dpx,s_field); ppval(dpy,s_field); ppval(dpz,s_field)];

        % Find segment midponts:
        s_midp = 0.5*(s(1:end-1)+s(2:end));
        X_midp = [ppval(ppx,s_midp); ppval(ppy,s_midp); ppval(ppz,s_midp)];
        T_midp = [ppval(dpx,s_midp); ppval(dpy,s_midp); ppval(dpz,s_midp)];
        th_midp = asin(T_midp(Q+1:2*Q));
        R_midp = RotationMatrix(th_midp);

        % Interval of integration around the field point:
        s_a = s_field -q;
        s_b = s_field +q;

        %% Analytical method:

        % Identify midpoints within the local interval of the field point:
        [x_midp,y_midp,z_midp] = extractComponents(X_midp);
        x_source = x_midp(abs(s_midp-s_field)<=q);
        y_source = y_midp(abs(s_midp-s_field)<=q);
        z_source = z_midp(abs(s_midp-s_field)<=q);
        X_source = [x_source(:); y_source(:); z_source(:)];     XM_source = VectorToMatrix(X_source);
        R_source = RotationMatrix(th_midp(abs(s_midp-s_field)<=q));
        num_source = length(x_source);

        % Compute integrals:
        int = RegStokesletAnalyticDerivativeIntegrals(X_field,X_source,T_field,R_source,ds/2,epsilon)*kron(eye(3),ones(num_source,1));
        I.ana{n,k} = int;

        %% Numerical method:
        % Numerical integral of numerical derivatives of stokeslets.

        % Parameter for numerical differentiation:
        h = 1e-6;

        % Number of nodes for numerical integration:
        num_nodes = 1e2;

        % Lookup Gauss-Legendre nodes and weights in the interval of integration:
        [si,wi] = lgwt(num_nodes,s_a,s_b);
        [si,idx] = sort(si);    wi = wi(idx);
        Xi = [ppval(ppx,si); ppval(ppy,si); ppval(ppz,si)]; XMi = VectorToMatrix(Xi);   % via splining.

        % Perturbed field nodes for numerical differentiation (fourth order
        % finite central finite differences):
        X_fph  = [ppval(ppx,s_field+h); ppval(ppy,s_field+h); ppval(ppz,s_field+h)];
        X_fmh  = [ppval(ppx,s_field-h); ppval(ppy,s_field-h); ppval(ppz,s_field-h)];
        X_fp2h = [ppval(ppx,s_field+2*h); ppval(ppy,s_field+2*h); ppval(ppz,s_field+2*h)];
        X_fm2h = [ppval(ppx,s_field-2*h); ppval(ppy,s_field-2*h); ppval(ppz,s_field-2*h)];
        
        % Compute integrals of derivatives:
        int = 0;
        for i = 1:num_nodes
            int = int + wi(i)/h * (1/12*RegStokeslets(X_fm2h,XMi(:,i),epsilon) ...
                                  -2/3 *RegStokeslets(X_fmh ,XMi(:,i),epsilon) ...
                                  +2/3 *RegStokeslets(X_fph ,XMi(:,i),epsilon) ...
                                  -1/12*RegStokeslets(X_fp2h,XMi(:,i),epsilon));
        end
        I.num{n,k} = int;
        
        %% Semi numerical method
        % Numerical integral of analytic derivatives of stokeslets.
        
        int = 0;
        for i = 1:num_nodes
            int = int + wi(i) * RegStokesletAnalyticDerivatives(X_field,XMi(:,i),epsilon,T_field);
        end
        I.mix{n,k} = int;
        
        %% Schematic plot for each problem for suitable Q:
        
        if n == 2 % Corresponds to Q = 20.
            if k == 1
               subplot(2,2,1);
               title_app = '(a)';
            elseif k == 2
               subplot(2,2,3);
               title_app = '(b)';
            end
            hold on; box on; grid on
            l1 = plot(x,y,'.-');
            l2 = scatter(X_field(1), X_field(2));
            l3 = scatter(XM_source(1,:), XM_source(2,:), 'x');
            axis equal
            f.CurrentAxes.TickLabelInterpreter = 'latex';
            f.CurrentAxes.FontSize = 12;
            xlabel('$x$','Interpreter','latex','FontSize',14)
            ylabel('$y$','Interpreter','latex','FontSize',14)
            title([title_app,' Schematic, $Q=\,$',num2str(Q)],'Interpreter','latex')
            legend([l2,l3],'field point','source segment centres','Location','northwest',...
                                                                  'Interpreter','latex', ...
                                                                  'FontSize',10);
        end

    end
end

%% Plots:

% Compute errors - compare to high resolution numerical method.
for k = 1:2
    for n = 1:num_runs
        error_ana(n,k) = norm(abs(I.ana{n,k}-I.num{end,k}));
        error_mix(n,k) = norm(abs(I.mix{n,k}-I.num{end,k}));
        error_sem(n,k) = norm(abs(I.mix{n,k}-I.ana{n,k}));
    end
end
%{
for k = 1:2
    for n = 1:num_runs
        error_ana(n,k) = norm(abs(I.ana{n,k}-I.num{n,k}));
        error_mix(n,k) = norm(abs(I.mix{n,k}-I.num{n,k}));
    end
end
%}

% Colors for plots:
cols = [0    0.4470    0.7410; 0.8500    0.3250    0.0980];

% Error plot:
% clear l*
subplot(2,2,[2,4]);
l11 = loglog(N_vals, error_ana(:,1), '.-', 'LineWidth', 1.1); grid on; box on; hold on;
l22 = loglog(N_vals, error_ana(:,2), '.-', 'LineWidth', 1.1);
l33 = loglog(N_vals, error_mix(:,1), '.--', 'color', cols(1,:), 'LineWidth', 1.1);
l44 = loglog(N_vals, error_mix(:,2), '.--', 'color', cols(2,:), 'LineWidth', 1.1);
l55 = loglog(N_vals, error_sem(:,1), '.:', 'color', cols(1,:), 'LineWidth', 1.1);
l66 = loglog(N_vals, error_sem(:,2), '.:', 'color', cols(2,:), 'LineWidth', 1.1);
xlabel('$Q+1$','Interpreter','latex','FontSize',14)
ylabel(['Error vs. numerical method, $h=\,$',num2str(h)],'Interpreter','latex','fontsize',14)
titlestr = ['(c) $I(s,q)$ with $s=\,$',num2str(s_field),', $q=\,$',num2str(q)];
title(titlestr,'Interpreter','latex')
legend('parabolic curve','sinusoidal curve','Interpreter','latex','FontSize',10)
f.CurrentAxes.TickLabelInterpreter = 'latex';
f.CurrentAxes.FontSize = 12;

f.Position = [655 395 842 540];

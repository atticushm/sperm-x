% TEST FUNCTION

% Checking the dyadic and isotropic parts of the analytical regularised
% stokeslet derivative integral code, using numerical quadrature and finite
% differences.


%% Setup

clear all; 
disp('-----')

% Approximate a curve by N points, Q=N-1 segments:
N = 101;     Q = N-1;    ds = 1/Q;
X = ParabolicCurve(0.5,N);  XM = VectorToMatrix(X);
[x,y,z] = extractComponents(X);
s = linspace(0,1,N);

% Identify a section of curve to integrate across:
m = 1;
s_a = 0.8;              s_b = s_a + m*ds;
a_id = find(s==s_a);    b_id = a_id+m;
X_a = XM(:,a_id);       X_b = XM(:,b_id);

% Field points are the nodes bound by these limits:
XM_field = XM(:,a_id:b_id);     X_field = MatrixToVector(XM_field);
s_field = s_a:ds:s_b;
N_field = length(X_field)/3;

% Source points are assumed to be the midpoints of each of the segments.
% First spline the curve:
ppx = spline(s,x);  ppy = spline(s,y);  ppz = spline(s,z);

% Evaluate a arlength midpoints to find coordinate midpoints:
s_midp = 0.5*(s(1:end-1) + s(2:end));
x_midp = ppval(ppx,s_midp);     y_midp = ppval(ppy,s_midp);     z_midp = ppval(ppz,s_midp);
% X_midp = SegmentMidpoints(X);
X_midp = [x_midp(:); y_midp(:); z_midp(:)];     
XM_midp = VectorToMatrix(X_midp);

% Midpoints inside the limits of integration are:
XM_source = XM_midp(:,a_id:b_id-1);     X_source = MatrixToVector(XM_source);
N_source = length(X_source)/3;

% To find tangents at the midpoints and nodes, differentiate the spline:
dppx = fnder(ppx,1);    dppy = fnder(ppy,1);    dppz = fnder(ppz,1);

% Tangents at nodes:
T = [ppval(dppx,s(:)); ppval(dppy,s(:)); ppval(dppz,s(:))];
TM = VectorToMatrix(T);

% Tangents at midpoints:
t_midp = [ppval(dppx,s_midp(:)); ppval(dppy,s_midp(:)); ppval(dppz,s_midp(:))];
tM_midp = VectorToMatrix(t_midp);

% Tangent angles at nodes and midpoints:
th = asin(TM(2,:));     th_midp = asin(tM_midp(2,:));
% th_midp = atan(abs(diff(y)./diff(x)));
th = interp1(s_midp,th_midp,s,'spline','extrap');

% Tangent angles at nodes and midpoints within integration limits:
th_field = th(a_id:b_id);   th_source = th_midp(a_id:b_id-1);

% Tangents at field points:
TM_field = TM(:,a_id:b_id); T_field = MatrixToVector(TM_field);

% Build rotation matrices:
R_field = RotationMatrix(th_field);     R_source = RotationMatrix(th_source);

% Choose a regularisation parameter:
epsilon = 1e-2;

%% Analytical approach:

% th_2 = atan(diff(y)./diff(x));
% th_field_2 = th_2(a_id:b_id); 
% T_field_2 = [cos(th_field_2(:)); sin(th_field_2(:)); zeros(length(th_field_2),1)];
% 
% XM_source_2 = 0.5*(XM_field(:,1:end-1)+XM_field(:,2:end));
% X_source_2  = MatrixToVector(XM_source_2);

I{1} = RegStokesletAnalyticIntegrals(X_field,X_source,ds/2,R_source,epsilon);
[D{1}, A{1}, B{1}] = RegStokesletAnalyticDerivativeIntegrals(X_field, X_source, T_field, R_source, ds/2, epsilon);

I_sum{1} = I{1} * kron(eye(3),ones(N_source,1));
D_sum{1} = D{1} * kron(eye(3),ones(N_source,1));
A_sum{1} = A{1} * kron(eye(3),ones(N_source,1));
B_sum{1} = B{1} * kron(eye(3),ones(N_source,1));

%% Numerical approach:

M = 50;         % nodes for numerical integration.
h = 1e-6;       % numerical differentiation step size.

% Lookup Gauss-Legendre nodes in the interval of integration:
[s_int, w_int] = lgwt(M, s_a, s_b);
[s_int,idx] = sort(s_int);
w_int = w_int(idx);

% Spline to find coordinates at nodes for integration:
x_int = ppval(ppx,s_int);   y_int = ppval(ppy,s_int);   z_int = ppval(ppz,s_int);
% x_int = interp1(s,x,s_int);     y_int = interp1(s,y,s_int);     z_int = interp1(s,z,s_int);
X_int = [x_int(:); y_int(:); z_int(:)];     XM_int = VectorToMatrix(X_int);

% Spline to find X(s+-h) for each field point (for differentiation):
x_fph = ppval(ppx,s_field+h);   y_fph = ppval(ppy,s_field+h);   z_fph = ppval(ppz,s_field+h);
x_fmh = ppval(ppx,s_field-h);   y_fmh = ppval(ppy,s_field-h);   z_fmh = ppval(ppz,s_field-h);
X_fph = [x_fph(:); y_fph(:); z_fph(:)];     X_fmh = [x_fmh(:); y_fmh(:); z_fmh(:)];
XM_fph = VectorToMatrix(X_fph);             XM_fmh = VectorToMatrix(X_fmh);

% Numerical integrals of numerical derivatives:
D_sum{2} = 0; A_sum{2} = 0; B_sum{2} = 0; I_sum{2} = 0;
for i = 1:M
    [S_u,A_u,B_u] = SplitStokeslets(X_fph, XM_int(:,i), epsilon);
    [S_l,A_l,B_l] = SplitStokeslets(X_fmh, XM_int(:,i), epsilon);
    
    I_sum{2} = I_sum{2} + w_int(i) * RegStokeslets(X_field,XM_int(:,i),epsilon);
    D_sum{2} = D_sum{2} + w_int(i)/2/h * (S_u - S_l);
    A_sum{2} = A_sum{2} + w_int(i)/2/h * (A_u - A_l);
    B_sum{2} = B_sum{2} + w_int(i)/2/h * (B_u - B_l);
end

%% Semi-analytical approach:
% Numerical integrals of analytic derivatives.

D_sum{3} = 0;
for i = 1:M
    D_sum{3} = D_sum{3} + w_int(i) * RegStokesletAnalyticDerivatives(X_field,XM_int(:,i),epsilon,T_field);    
end

%% Comparisons:

% Print matrices to command line:
disp('~~~~~ VALUES ~~~~~')
celldisp(D_sum);
celldisp(A_sum);
celldisp(B_sum);
celldisp(I_sum);

% Matrix differences:
disp('~~~~~ ERRORS ~~~~~')
disp(['Number of segments is Q=',num2str(Q)]);
I_sum_error = abs(I_sum{1}-I_sum{2});   disp('I error:');   disp(I_sum_error);
D_sum_error = abs(D_sum{1}-D_sum{2});   disp('D (analytic) error:');   disp(D_sum_error);
D_mix_error = abs(D_sum{3}-D_sum{2});   disp('D (semi-analytic) error:'); disp(D_mix_error)
A_sum_error = abs(A_sum{1}-A_sum{2});   disp('A error:');   disp(A_sum_error);
B_sum_error = abs(B_sum{1}-B_sum{2});   disp('B error:');   disp(B_sum_error);

disp('-----')

%% ~~~~~ FUNCTIONS ~~~~~

function [S,A,B] = SplitStokeslets(x_field,X_source,eps)

x_field=x_field(:);
X_source=X_source(:);
M=length(x_field)/3;
Q=length(X_source)/3;
r1=      x_field(1:M)*ones(1,Q)-ones(M,1)*      X_source(1:Q)';
r2=  x_field(M+1:2*M)*ones(1,Q)-ones(M,1)*  X_source(Q+1:2*Q)';
r3=x_field(2*M+1:3*M)*ones(1,Q)-ones(M,1)*X_source(2*Q+1:3*Q)';
rsq=r1.^2+r2.^2+r3.^2;
ireps3=1./(sqrt((rsq+eps^2)).^3);
isotropic=kron(eye(3),(rsq+2.0*eps^2).*ireps3);
dyadic=[r1.*r1 r1.*r2 r1.*r3; r2.*r1 r2.*r2 r2.*r3; ...
        r3.*r1 r3.*r2 r3.*r3].*kron(ones(3,3),ireps3);

A=(1.0/(8.0*pi))*(isotropic);
B=(1.0/(8.0*pi))*(dyadic);
S=(1.0/(8.0*pi))*(isotropic+dyadic);

end

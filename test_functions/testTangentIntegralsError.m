%% TEST FUNCTION : TANGENT INTEGRAL ERROR

% Assessing the error in the integral across a straight-line segment of
% curve, where the tangent angle of the integrated segment estimated in
% three ways:
%   (a) via the difference of the end nodes.
%   (b) via analytical differentiation of an interpolating spline.
%   (c) via a finite difference scheme.

clear all;
disp('-----')

% Integrate across the whole flagellum:
s_a = 0;
s_b = 1;

% Integrate across a portion of flagellum:
s_a = 0.5;
s_b = 1;

% Choose a regularisation parameter:
epsilon = 1e-2;

%% Compute integrals and local operators across the region [s_a, s_b]:

% Choose N by successive doubling of nodes (endpoints will match):
N_vals = [11,21,41,81,161,321,641];

for n = 1:length(N_vals)
%     textwaitbar(n,length(N_vals),'Refining discretisation:')
    N = N_vals(n);
    [I, D, I_tot, D_tot, E] = CompareTangentFormulations(N, s_a, s_b);
    
    for k = 1:4
        E_I(n,k) = E{k}.I;
        E_D(n,k) = E{k}.D;
    end
end

%% Plots & Outputs:

close all;

disp('R^{-1}: Integral over the segment:')
celldisp(I_tot)
disp('-----')
disp('R^{-1}_s: Derivative of integral over the segment:')
celldisp(D_tot)
disp('-----')

% Set plot colors
cols = parula(10);

% Error against high-resolution numerics:
figure; 
subplot(1,2,1); box on; 
loglog(N_vals, E_I(:,1),'.-','Color',cols(2,:),'LineWidth',1.2); hold on; grid on;
loglog(N_vals, E_I(:,2),'.-','Color',cols(5,:),'LineWidth',1.2)
loglog(N_vals, E_I(:,3),'.-','Color',cols(7,:),'LineWidth',1.2)
loglog(N_vals, E_I(:,4),'.-','Color',cols(9,:),'LineWidth',1.2)
xlabel('$N$','Interpreter','latex','FontSize',12)
ylabel('$\|E\|$','Interpreter','latex','FontSize',12)
title('Integral Error','Interpreter','latex','fontsize',12)
legend('p-w. linear + pt diff.','spline + pt diff.','spline + finite diff.','spline + p-w. poly.','Location','northeast','Interpreter','latex','FontSize',12)

subplot(1,2,2); box on; 
loglog(N_vals, E_D(:,1),'.-','Color',cols(2,:),'LineWidth',1.2); hold on; grid on;
loglog(N_vals, E_D(:,2),'.-','Color',cols(5,:),'LineWidth',1.2)
loglog(N_vals, E_D(:,3),'.-','Color',cols(7,:),'LineWidth',1.2)
loglog(N_vals, E_D(:,4),'.-','Color',cols(9,:),'LineWidth',1.2)
xlabel('$N$','Interpreter','latex','FontSize',12)
ylabel('$\|E\|$','Interpreter','latex','FontSize',12)
title('Integral of Derivative Error','Interpreter','latex','fontsize',12)
legend('p-w. linear + pt diff.','spline + pt diff.','spline + finite diff.','spline + p-w. poly.','Location','northeast','Interpreter','latex','FontSize',12)

set(gcf, 'Position', [679 1004 881 334]);

%% *** FUNCTION ****

function [I, D, I_tot, D_tot, E] = CompareTangentFormulations(N,s_a,s_b)

% Approximate a continuous curve by N nodes along the arclength:

% X = SinusoidalCurve(2*pi, 1, N);    % coordinates.
X = ParabolicCurve(0.5,N);
[x,y,z] = extractComponents(X(:));  % coordiante components.
XM = VectorToMatrix(X(:));
s = linspace(0,1,N);                % arclength nodes.

% Segment midpoints have arclength values:
s_midp = 0.5*(s(2:end)+s(1:end-1));

% Spline to find coordinates of segment midpoints:
x_midp = interp1(s, x, s_midp, 'spline');
y_midp = interp1(s, y, s_midp, 'spline');
z_midp = interp1(s, z, s_midp, 'spline');
X_midp = [x_midp(:); y_midp(:); z_midp(:)];
XM_midp = VectorToMatrix(X_midp);

% Node separation defines the size of the interval of integration:
ds = 1/(N-1);

% Identify the nodes we wish to integrate between:
[s_a_error,a_idx] = min(abs(s-s_a));    X_a = XM(:,a_idx);
[s_b_error,b_idx] = min(abs(s-s_b));    X_b = XM(:,b_idx);
X_field = MatrixToVector([X_a, X_b]);
N_field = length(X_field)/3;

disp(['N=',num2str(N),', Endpoint error:',num2str(s_a_error),',',num2str(s_b_error)])

% Choose a regularisation paramter:
epsilon = 1e-2;

%% Method 1: current method (what is currently used).

% Here, collocation is performed at the linearly interpolated segment
% midpoints, which do not necessarily lie upon the curve.

% Assume that the tangent angle at the midpoint of the segment of curve can
% be approximated by a simple finite diffence between segment end nodes:
[X_centres, ~] = SegmentMidpoints(X(:));
XM_centres = VectorToMatrix(X_centres);
theta_midp{1} = atan(abs(diff(y))./abs(diff(x)));

% Source points in this case are the midpoints of the line segment joining
% each node:
X_source = XM_centres(:,a_idx:b_idx-1);
X_source = MatrixToVector(X_source);

% Interpolate to get tangent angle at X_a and X_b:
% theta_endp{1} = interp1(s_midp, theta_midp{1}, s, 'spline','extrap');
theta_field{1} = interp1(s_midp,theta_midp{1},[s_a,s_b],'spline','extrap');

% Rotation matrix for the segment is:
R_seg{1} = RotationMatrix(theta_midp{1}(a_idx:b_idx-1));

% Tangent to the field points is:
T_field{1} = [cos(theta_field{1}(:)); sin(theta_field{1}(:)); zeros(N_field,1)];

% Integral across section of curve is:
I{1} = RegStokesletAnalyticIntegrals(X_field, X_source, ds/2, R_seg{1}, epsilon);
I_tot{1} = I{1} * kron(eye(3),ones(b_idx-a_idx,1));

% Local operator across this section of curve is:
D{1} = RegStokesletAnalyticDerivativeIntegrals(X_field, X_source, T_field{1}, R_seg{1}, ds/2, epsilon);
D_tot{1} = D{1} * kron(eye(3),ones(b_idx-a_idx,1));

%% Method 2: tangents via difference of endpoints:

% This method is the same as the current method, but the centres of
% integration are now chosen via splining the curve rather than node
% difference.

% The tangent angle at the segment midpoints are appropximated via node
% point differences as in Method 1.

% Choose source points as midpoints on the curve:
X_source = XM_midp(:,a_idx:b_idx-1);
X_source = MatrixToVector(X_source);

R_seg{2} = R_seg{1};
T_field{2} = T_field{1};

% Integral across section of curve is:
I{2} = RegStokesletAnalyticIntegrals(X_field, X_source, ds/2, R_seg{2}, epsilon);
I_tot{2} = I{2} * kron(eye(3),ones(b_idx-a_idx,1));

% Local operator is:
D{2} = RegStokesletAnalyticDerivativeIntegrals(X_field, X_source, T_field{2}, R_seg{2}, ds/2, epsilon);
D_tot{2} = D{2} * kron(eye(3),ones(b_idx-a_idx,1));

%% Method 3: tangents via finite differences:

% Create finite difference scheme: 2nd order central differences for
% interior nodes, 3rd order one-sided differences for the boundary nodes.

% We want a unified finite difference scheme, using as much information as
% possible to get accurate derivatives. As we need derivatives at the
% segment midpoints (to determine segment tangent vectors), we combine the
% arclength nodes and segment midpoint arclength nodes:
s_comb = sort(vertcat(s(:), s_midp(:)));
X_comb = [sort([x(:);x_midp(:)]); sort([y(:);y_midp(:)]); sort([z(:);z_midp(:)])];

% Get finite difference scheme:
[d_s,~,~,~] = BuildFiniteDifferenceMatrices(s_comb, 'equispaced');

% Compute derivatives of nodes and midpoints:
fd_divs = blkdiag(d_s,d_s,d_s) * X_comb;
Mfd_divs = VectorToMatrix(fd_divs);

% Extract node derivatives (odd indexed values):
Mdivs_nodes = Mfd_divs(:,1:2:end);
divs_nodes = MatrixToVector(Mdivs_nodes);

% Extract midpoint derivatives (even indexed values):
Mdivs_midp = Mfd_divs(:,2:2:end-1);
divs_midp = MatrixToVector(Mdivs_midp);

% First derivative of the curve is the tangent angle. Recall
% t=[cos(th),sin(th),0] for a planar curve. Then we can compute theta at
% both nodes and midpoints:
theta_endp{3} = real(acos(Mdivs_nodes(1,:)));
theta_midp{3} = real(acos(Mdivs_midp(1,:)));

% Build rotation matrix for source points:
R_seg{3} = RotationMatrix(theta_midp{3}(a_idx:b_idx-1));

% Tangets to field points are:
theta_field{3} = [theta_endp{3}(a_idx); theta_endp{3}(b_idx)];
T_field{3} = [cos(theta_field{3}(:)); sin(theta_field{3}(:)); zeros(N_field,1)];

% Integral across section of curve is:
I{3} = RegStokesletAnalyticIntegrals(X_field, X_source, ds/2, R_seg{3}, epsilon);
I_tot{3} = I{3} * kron(eye(3),ones(b_idx-a_idx,1));

% Local operator is:
D{3} = RegStokesletAnalyticDerivativeIntegrals(X_field, X_source, T_field{3}, R_seg{3}, ds/2, epsilon);
D_tot{3} = D{3} * kron(eye(3),ones(b_idx-a_idx,1));

%% Method 4: tangents via spline object and fndir:

% Create a spline object describing the curve:
pp_x = spline(s, x);
pp_y = spline(s, y);
pp_z = spline(s, z);

% Differentiate spline object:
dpp_x = fndir(pp_x, 1);
dpp_y = fndir(pp_y, 1);
dpp_z = fndir(pp_z, 1);

% Evaluate derivates at segment midpoints and nodes to obtain tangent vectors:
divs_nodes = [ppval(dpp_x, s(:)); ppval(dpp_y, s(:)); ppval(dpp_z, s(:))];
divs_midp  = [ppval(dpp_x, s_midp(:)); ppval(dpp_y, s_midp(:)); ppval(dpp_z, s_midp(:))];
Mdiv_nodes = VectorToMatrix(divs_nodes);
Mdiv_mipd  = VectorToMatrix(divs_midp);

% Determine theta:
theta_endp{4} = real(acos(Mdivs_nodes(1,:)));
theta_midp{4} = real(acos(Mdivs_midp(1,:)));

% Build rotation matrix for source points:
R_seg{4} = RotationMatrix(theta_midp{4}(a_idx:b_idx-1));

% Tangents to the field points are:
theta_field{4} = [theta_endp{4}(a_idx); theta_endp{4}(b_idx)];
T_field{4} = [cos(theta_field{4}(:)); sin(theta_field{4}(:)); zeros(N_field,1)];

% Integral across section of curve is:
I{4} = RegStokesletAnalyticIntegrals(X_field, X_source, ds/2, R_seg{1}, epsilon);
I_tot{4} = I{4} * kron(eye(3),ones(b_idx-a_idx,1));

% Local operator is:
D{4} = RegStokesletAnalyticDerivativeIntegrals(X_field, X_source, T_field{4}, R_seg{4}, ds/2, epsilon);
D_tot{4} = D{4} * kron(eye(3),ones(b_idx-a_idx,1));

%% Method 5: fully numerical integration and differentiation:

% Number of nodes for numerical integration:
N_int = N;

% Discretisation parameter for numerical differentiation:
h_diff = 1e-5;

% Lookup Gauss-Legendre nodes in the interval of integration:
[s_int, w_int] = lgwt(N_int, s_a, s_b);
[s_int,idx] = sort(s_int);
w_int = w_int(idx);

% Use linear interp to obtain X_int = X(s_int) (for integration).
% We use linear interpolation as we want to test, *for this segmentation*, if
% the integrals/derivatives are correct:
x_int = ppval(pp_x,s_int);
y_int = ppval(pp_y,s_int);
z_int = ppval(pp_z,s_int);
X_int = [x_int(:); y_int(:); z_int(:)];
XM_int = VectorToMatrix(X_int);
% x_int = interp1(s,x,s_int,'linear');
% y_int = interp1(s,y,s_int,'linear');
% z_int = interp1(s,y,s_int,'linear');
% X_int = [x_int(:); y_int(:); z_int(:)];
% XM_int = VectorToMatrix(X_int);

% Spline to obtain points +-h_diff around field points (for
% differentiation):
s_f = [s_a, s_b];
x_fph = ppval(pp_x,s_f+h_diff);     x_fmh = ppval(pp_x,s_f-h_diff);
y_fph = ppval(pp_y,s_f+h_diff);     y_fmh = ppval(pp_y,s_f-h_diff);
z_fph = ppval(pp_z,s_f+h_diff);     z_fmh = ppval(pp_z,s_f-h_diff);
X_fph = [x_fph(:); y_fph(:); z_fph(:)];
X_fmh = [x_fmh(:); y_fmh(:); z_fmh(:)];

% Integral and Local operator are:
int = 0;
div = 0;
for i = 1:N_int
    int = int + w_int(i) * RegStokeslets(X_field, XM_int(:,i),epsilon);
    div = div + w_int(i)/2/h_diff * (RegStokeslets(X_fph,XM_int(:,i),epsilon) - RegStokeslets(X_fmh,XM_int(:,i),epsilon));
end

% I{5} = int;
% D{5} = div;
% I_tot{5} = int * kron(eye(3),ones(N_int,1));
% D_tot{5} = div * kron(eye(3),ones(N_int,1));
I_tot{5} = int;
D_tot{5} = div;

%% Compute errors

E{1}.I = norm( I_tot{1} - I_tot{5} );
E{2}.I = norm( I_tot{2} - I_tot{5} );
E{3}.I = norm( I_tot{3} - I_tot{5} );
E{4}.I = norm( I_tot{4} - I_tot{5} );

E{1}.D = norm( D_tot{1} - D_tot{5} );
E{2}.D = norm( D_tot{2} - D_tot{5} );
E{3}.D = norm( D_tot{3} - D_tot{5} );
E{4}.D = norm( D_tot{4} - D_tot{5} );

end
% TEST FUNCTION
% ERROR IN TANGENT ANGLE AND INTEGRALS WHEN USING DIFFERENT METHODS FOR
% DETERMINING THE CURVE DERIVATIVES.

clear all; clc
disp('-----')

% Define a curve through N nodes along it's arclength;
N = 1e2;
X = SinusoidalCurve(2*pi, 1, N);
[x, y, z] = extractComponents(X(:));
s = linspace(0,1,N);

%% Method 1: determine tangent angles via point differences and interpolation.

% Determine the tangent angles of segments between nodes:
segment_tanang = atan(diff(y)./diff(x));

% Segment tangent angles are assumed to be measured at segment midpoints:
[X_centres, s_centres] = SegmentMidpoints(X(:));
[x_centres, y_centres, ~] = extractComponents(X_centres);

% Spline to get tangent angles at nodes (segment endpoints):
th_ang = interp1(s_centres, segment_tanang, s, 'linear', 'extrap');

% Tangent vectors at nodes are thus:
R = RotationMatrix(th_ang);
tanvec_ang = R(1,:)';
tanmat_ang = VectorToMatrix(tanvec_ang);

% ** or ** via Frenet-Serret formulas:
% DX = DerivativesFromFrenetSerret(X);
% tanvec_ang2 = DX{1};
% tanmat_ang2 = VectorToMatrix(tanvec_ang2);

%% Method 2: finite difference scheme using node data:

% Create finite difference scheme: second order central differences in the
% interior, third order one-sided differences at either end:
[ds,~,~,~] = BuildFiniteDifferenceMatrices(s, 'equispaced');

% Recalling that t(s) = dX(s)/ds:
tanvec_fds = blkdiag(ds,ds,ds)*X(:);
tanmat_fds = VectorToMatrix(tanvec_fds);

% Generate unit tangent vectors:
tanmat_fds = tanmat_fds./vecnorm(tanmat_fds);

% Recover tangent angles:
th_fds = acos(tanmat_fds(1,:));

%% Method 3: sampling from an interpolating spline object:

% Create splining polynomial for each dimension:
pp_x = spline(s, x);
pp_y = spline(s, y);
pp_z = spline(s, z);

% Find derivative of piecewise polynomials in each direction:
dpp_x = fndir(pp_x,1);
dpp_y = fndir(pp_y,1);
dpp_z = fndir(pp_z,1);

% Evaluate derivative functions at node arclengths:
tanvec_spl = [ppval(dpp_x, s)'; ppval(dpp_y, s)'; ppval(dpp_z, s)'];
tanmat_spl = VectorToMatrix(tanvec_spl);

% Generate unit tangent vectors:
tanmat_spl = tanmat_spl./vecnorm(tanmat_spl);

% Recover tangent angles:
th_spl = acos(tanmat_spl(1,:));

%% Plots:

% Curve from spline;
s_curve = linspace(0,1,1e5);
x_curve = ppval(pp_x,s_curve);
y_curve = ppval(pp_y,s_curve);

% Quiver plot:
figure; subplot(1,2,1); box on; hold on;
plot(x_curve,y_curve,'k','LineWidth',1.5)
scatter(x,y,'k','filled')
quiver(x,y,tanmat_ang(1,:)',tanmat_ang(2,:)');
quiver(x,y,tanmat_fds(1,:)',tanmat_fds(2,:)');
quiver(x,y,tanmat_spl(1,:)',tanmat_spl(2,:)');
legend('curve','nodes','interp. midpoint angles','finite differences','splining')
xlabel('$x$','interpreter','latex')
ylabel('$y$','interpreter','latex')
xlim([0.0674 0.2409])       % zoom in to make differences more visible.
ylim([-0.0419 0.1894])

% Angle plot:
subplot(1,2,2); box on; hold on;
plot(s, abs(th_ang));
plot(s, abs(th_fds));
plot(s, abs(th_spl));
xlabel('$s$','interpreter','latex')
ylabel('$|\theta(s)|$','interpreter','latex')
legend('interp. midpoint angles', 'finite differences', 'splining')





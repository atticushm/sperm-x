% TEST FUNCTION

% Comparing RegStokesletAnalyticDerivatives (written by me), to
% CVN_CalculateStokesletDerivative (written by Cara).

% Cara's function takes a vector of field points, and a single source
% point - as such we will use the same inputs in my function.

% Note that Cara's function differentiates with respect to X, whereas my
% objective is a derivative with respect to s.

clear all; close all; clc
disp('-----')

X_source = [0;0;0];                             % single source point at the origin.
epsilon = 1e-2;                                 % regularisation parameter.

%% Check stokeslet derivative code:

N = floor(logspace(1,3,25));

for n = 1:length(N)
    fprintf('Stokeslet derivatives; N = %g \n',N(n))          % update tracker.
    X_field = ParabolicInitialCondition(0.5, N(n));     % field points form a parabola in xy.

    % Obtain dS/dX from Cara's function:
    disp('    Computing analytical derivatives (via Cara)...')
    [~,S_X_1] = CVN_CalculateStokesletDerivatives(X_field, X_source, [0;0;0], epsilon);
    S_X_cvn{n} = S_X_1;

    % Obtain dS/dX and dS/ds from my function:
    disp('    Computing analytical derivatives (via me)...')
    [S_s_2,S_X_2] = RegStokesletAnalyticDerivatives(X_field, X_source, epsilon);
    S_X_ahm{n} = S_X_2;
    S_s_ahm{n} = S_s_2;

    % Obtain dS/ds from numerical differentiation, increasing resolution:
    disp('    Computing numerical derivatives...')
    [S_s_3,S_X_3] = RegStokesletNumericalDerivatives(X_field, X_source, epsilon);
    S_X_num{n} = S_X_3;
    S_s_num{n} = S_s_3;
end

% Compute matrix norms to measure convergence (only for rank-2 tensors...)
for n = 1:length(N)
    norm_S_s_ahm(n) = norm(S_s_ahm{n});
    norm_S_s_num(n) = norm(S_s_num{n});
end

disp('-----')

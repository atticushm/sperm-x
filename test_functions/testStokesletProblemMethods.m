% TEST FUNCTION

% Approximating the integral:
%   u(x) = int_0^1 S(x,X) f(X) ds,
% using 
%   (a) Nystrom discretisation, 
%   (b) NEAREST, the nearest-neighbour discretisation with the method of
%       regularised stokeslets,
%   (c) Regularised stokeslet segments, whereby the force is analytically
%       integrated along constituent segments and assumed piecewise
%       constant,
% and checking that the resulting velocities u(x), prescribing the force
% per unit length f, match!

clc;
clearvars; close all

N_vec = unique(floor(logspace(1,3)));
num_N = length(N_vec);

progressbarText(0);
for n = 1:num_N

    %% Problem setup:

    % Create discretisations:
    N = N_vec(n);           % traction discretisation.
    Q = 4*N;                % quadrature discretisation (for use in NEAREST).
    s(n).N = linspace(0,1,N);
    s_N = s(n).N;
    s_Q = linspace(0,1,Q);

    parab_coeff = 0.2;
    X_t = ParabolicInitialCondition(parab_coeff, N);
    X_q = ParabolicInitialCondition(parab_coeff, Q);

    epsilon = 0.01;     % stokeslet regularisation parameter.

    % Prescribe FORCE PER UNIT LENGTH:
    f_x = 1;
    f_y = -1;
    f_z = 0;

    %% Nystrom:

    ny_tic = tic;
    slets = RegStokeslets(X_t, X_t, epsilon);
    weights = 1/N;
    f_nystrom = [repmat(f_x,N,1); repmat(f_y,N,1); repmat(f_z,N,1)];
    U(n).nystrom = - weights * slets * f_nystrom;
    [U(n).ny_x, U(n).ny_y, U(n).ny_z] = extractComponents(U(n).nystrom);
    ny_time(n) = toc(ny_tic);
    
    %% NEAREST:

    ne_tic = tic;
    slets = RegStokeslets(X_t, X_q, epsilon);
    weights = 1/Q;
    nu = NearestNeighbourMatrix(X_q, X_t);
    f_nearest = [repmat(f_x,N,1); repmat(f_y,N,1); repmat(f_z,N,1)];
    U(n).nearest = - weights * (slets * nu) * f_nearest;
    [U(n).ne_x, U(n).ne_y, U(n).ne_z] = extractComponents(U(n).nearest);
    ne_time(n) = toc(ne_tic);

    %% Stokeslet segments:

    se_tic = tic;
    [x, y, z] = extractComponents(X_t);
    x_c = 0.5*(x(1:end-1) + x(2:end));  % segment centres
    y_c = 0.5*(y(1:end-1) + y(2:end));
    z_c = 0.5*(z(1:end-1) + z(2:end));
    X_c = [x_c(:); y_c(:); z_c(:)];

    s_c = 0.5*(s_N(1:end-1) + s_N(2:end));
    ds_c = s_c(2) - s_c(1);

    th = atan(diff(y)./diff(x));
    R = [cos(th)', -sin(th)', zeros(1,N-1);
         sin(th)',  cos(th)', zeros(1,N-1);
         zeros(1,N-1),zeros(1,N-1),ones(1,N-1)]; 
    seglets = RegStokesletAnalyticIntegrals(X_c,X_c,ds_c/2, R, epsilon);

    f = [repmat(f_x,N,1); repmat(f_y,N,1); repmat(f_z,N,1)];
    [fx,fy,fz] = extractComponents(f);

    f_xc = interp1(s_N, fx, s_c, 'spline');
    f_yc = interp1(s_N, fy, s_c, 'spline');
    f_zc = interp1(s_N, fz, s_c, 'spline');
    f_segments = [f_xc(:); f_yc(:); f_zc(:)];
    U(n).segments = - seglets * f_segments;
    [ux, uy, uz] = extractComponents(U(n).segments);

    U(n).se_x = interp1(s_c, ux, s_N, 'spline', 'extrap');
    U(n).se_y = interp1(s_c, uy, s_N, 'spline', 'extrap');
    U(n).se_z = interp1(s_c, uz, s_N, 'spline', 'extrap');
    U(n).segments = [U(n).se_x(:); U(n).se_y(:); U(n).se_z(:)];
    
    se_time(n) = toc(se_tic);

    %% Errors:
    
    E.ny_ne(n) = MeanSqError(U(n).nystrom, U(n).nearest);
    E.ny_se(n) = MeanSqError(U(n).nystrom, U(n).segments);
    E.ne_se(n) = MeanSqError(U(n).nearest, U(n).segments);
    
    if n > 1
        E.ny_conv(n) = ConvergenceMSE(U(n).nystrom, s(n).N, U(n-1).nystrom, s(n-1).N);
        E.ne_conv(n) = ConvergenceMSE(U(n).nearest, s(n).N, U(n-1).nearest, s(n-1).N);
        E.se_conv(n) = ConvergenceMSE(U(n).segments,s(n).N, U(n-1).segments,s(n-1).N);
    end

    progressbarText(n/num_N);
end

%% Plots:

n = floor(num_N/4);
s_N = s(n).N;

figure; 
subplot(2,3,1); box on; hold on;
plot(s_N, U(n).ny_x); plot(s_N, U(n).ne_x); plot(s_N, U(n).se_x);
xlabel('s'); ylabel({'U_x'})

subplot(2,3,2); box on; hold on;
plot(s_N, U(n).ny_y); plot(s_N, U(n).ne_y); plot(s_N, U(n).se_y);
xlabel('s'); ylabel('U_y')
title({'Shape error',['N=',num2str(n)]})

subplot(2,3,3); box on; hold on;
plot(s_N, U(n).ny_z); plot(s_N, U(n).ne_z); plot(s_N, U(n).se_z);
xlabel('s'); ylabel('U_z')
legend('Nystrom','NEAREST','Segments')

subplot(2,3,4); box on; hold on; 
loglog(N_vec, E.ny_ne); loglog(N_vec, E.ny_se); loglog(N_vec, E.ne_se);
set(gca, 'XScale', 'log', 'YScale', 'log');
xlabel('N'); ylabel('MSE')
legend('Nystrom vs NEAREST','Nystrom vs Segments','NEAREST vs Segments','Location','north')
title('Comparative error')

subplot(2,3,5); box on; hold on;
loglog(N_vec, E.ny_conv); loglog(N_vec, E.ne_conv), loglog(N_vec, E.se_conv)
set(gca, 'XScale', 'log', 'YScale', 'log');
xlabel('N'); ylabel('MSE(U(n), U(n-1))')
title('Error convergence')
legend('Nystrom','NEAREST','Segments','Location','northeast')

subplot(2,3,6); box on; hold on;
loglog(N_vec, ny_time); loglog(N_vec, ne_time); loglog(N_vec, se_time);
set(gca, 'XScale', 'log', 'YScale', 'log');
xlabel('N'); ylabel('Walltime (s)')
title('Walltime')
legend('Nystrom','NEAREST','Segments','Location','northwest')

disp('-----')

% END OF SCRIPT


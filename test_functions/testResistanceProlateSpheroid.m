% TEST FUNCTION

% Convergence for a prolate spheroid, scaled by L (the filament length)
% How many nodes do we actually need to ensure convergence of head
% integrals?

% comparing a regularised stokelet approach with analytic results from kim
% and karilla

clear all

%% define a head

% vector along the long axis
d = [1;0;0];

% length of flagellum
L = 45;

% radius of long axis
a = 2/L;

% radius of short axis
c = 1/L;

% axes
ax = [a;c;c];

%% analytical force and moment
% from kim and karilla

% compute grand resistance matrix
GRMa = resistanceProlateSpheroid(d, a, c);

%% numerical force and moment
% from solving resistance problem with regularised stokeslets

% vary traction and quadrature discretisations
Ht  = unique(floor(logspace(0,1,10)));
Hq1 = 2*Ht;
Hq2 = 3*Ht;
num_Ht = length(Ht);

% regularisation parameter for 
epsilon = 1e-2;

% solve resistance problems
printLinebreak
for n = 1:num_Ht
    fprintf('%g/%g\n',n,num_Ht);
    
    % generate discretisations
    [X_trac, X_quad1, ~,~] = generateBodyNodes(Ht(n), Hq1(n), 'human', 'custom', ax);
    [~, X_quad2, ~,~]      = generateBodyNodes(Ht(n), Hq2(n), 'human', 'custom', ax);
    
    % numbers of nodes
    Nf(n)  = length(X_trac)/3;
    Nq1(n) = length(X_quad1)/3;
    Nq2(n) = length(X_quad2)/3;
    
    % left hand sides for resistance problems
    LHS_U = kron(eye(3),ones(Nf(n),1));
    dx = X_trac(:)-kron(zeros(3,1),ones(Nf(n),1));
    dxM = mat(dx);
    LHS_Omg = zeros(3*Nf(n),3);
    for ii = 1:Nf(n)
        cp_1 = cross([1;0;0],dxM(:,ii));
        cp_2 = cross([0;1;0],dxM(:,ii));
        cp_3 = cross([0;0;1],dxM(:,ii));

        LHS_Omg(ii,1)       = cp_1(1);
        LHS_Omg(ii+Nf(n),1)     = cp_1(2);
        LHS_Omg(ii+2*Nf(n),1)   = cp_1(3);
        LHS_Omg(ii,2)       = cp_2(1);
        LHS_Omg(ii+Nf(n),2)     = cp_2(2);
        LHS_Omg(ii+2*Nf(n),2)   = cp_2(3);
        LHS_Omg(ii,3)       = cp_3(1);  
        LHS_Omg(ii+Nf(n),3)     = cp_3(2);
        LHS_Omg(ii+2*Nf(n),3)   = cp_3(3);
    end
    LHS = [LHS_U, LHS_Omg];
    
    % nearest neighbour matrices
    NN1 = nearestNeighbourMatrix(X_quad1, X_trac, 'cpu');
    NN2 = nearestNeighbourMatrix(X_quad2, X_trac, 'cpu');
    
    % stokeslet integral matrices
    I1 = regStokeslet(X_trac, X_quad1, epsilon, 'cpu')*NN1;
    I2 = regStokeslet(X_trac, X_quad2, epsilon, 'cpu')*NN2;
    
    % solve resistance problems
    f1 = -I1\LHS;
    f2 = -I2\LHS;
    
    % total force and moment
    for k = 1:6
        F1(:,k) = calcBodyForce(f1(:,k), NN1);
        F2(:,k) = calcBodyForce(f2(:,k), NN2);
        M1(:,k) = calcBodyMoment(X_quad1,zeros(3,1),f1(:,k),NN1);
        M2(:,k) = calcBodyMoment(X_quad2,zeros(3,1),f2(:,k),NN2);
    end
    
    % grand resistance matrices
    GRMn1{n} = blkdiag(F1,M1);
    GRMn2{n} = blkdiag(F2,M2);
    
end

% END OF SCRIPT
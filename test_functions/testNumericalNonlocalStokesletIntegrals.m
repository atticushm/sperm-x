% testNumericalNonlocalStokesletIntegrals

printLinebreak

% number of nodes
NF = 151;

% generate a curve
% X = parabolicCurve(0.5, NF);
xx = linspace(0,1,NF)';
X  = [xx; 0*xx; 0*xx];

% stokeslet regularisation parameter
epsilon = 1e-2;

% build location 
proc = 'cpu';

% parameters
ds = 1/(NF-1);
Q = 10;
q = Q*ds;
% q = 0.1;
b = q/10;  
[c_perp, c_para, gamma] = calcResistanceCoefficients(1,q,b,'rss');

% dummy force per unit length data
s  = linspace(0,1,NF);
fx = cos(s(:));
fy = -0.5*sin(s(:));
fz = s(:).^2;
f  = [fx; fy; fz];

%% compute numerical integral operator

% compute local region operator
D = finiteDifferences(s, 1);
d3 = kron(eye(3),D{1}) * X;
d3d3 = tensorProd(d3);
D_loc = 1/c_perp * (eye(3*NF) + (gamma-1)*d3d3);

% compute nonlocal stokeslet integrals
D_num = regStokesletNonlocalNumericalIntegrals(X, X, q, epsilon, proc);

% multiply onto force
I_num = (D_loc + D_num) * f;

%% compute analytic integral operator

% source points are midpoints of segments
X_midp   = midpoints(X);
X_source = X_midp;

% field points are midpoints
X_field = X_midp;

% midpoint values of forces
f_midp = midpoints(f);

% segment lengths are
ds = 1/(NF-1);

% determine tangent angles of segments
[x,y,z] = extractComponents(X);
theta   = atan(diff(y)./diff(x));

% build rotation matrix (requires planar curve...)
ze = 0*theta';
R  = [cos(theta'), -sin(theta'), ze ; ...
      sin(theta'), cos(theta'),  ze ; ...
      ze,          ze,  ones(1,NF-1)];

% compute 'furthest-neighbours' matrix enforcing limits of nonlocal
% integrals
s_midp = 1/2 * (s(1:end-1)+s(2:end));
dxi = abs(s_midp-s_midp');
FN  = zeros(size(dxi,1),size(dxi,2));
FN(dxi>q) = 1;
  
% compute stokeslet integrals
D_ana = regStokesletAnalyticIntegrals(X_field, X_source, ds/2, R, epsilon);

% multiply onto force midpoints
% I_ana_midp = (repmat(FN,3,3).*D_ana) * f_midp;
I_ana_midp = D_ana * f_midp;

% spline to get integral values at nodes
[Ix, Iy, Iz] = extractComponents(I_ana_midp);
s_midp  = 0.5*(s(1:end-1)+s(2:end));
I_ana_x = spline(s_midp, Ix, s);
I_ana_y = spline(s_midp, Iy, s);
I_ana_z = spline(s_midp, Iz, s);
I_ana   = [I_ana_x(:); I_ana_y(:); I_ana_z(:)];

% I_ana = D_loc*f + I_ana;

%% comparisons

% matrix representations
I_num_mat = mat(I_num);
I_ana_mat = mat(I_ana);

% plot integrals
figure; 
nexttile; hold on; box on;
plot(I_num,'-');
plot(I_ana,'-');

legend('numerical (local+nonlocal)','semi-analytic (no splitting)')
title(sprintf('N=%g',NF))

% error between analytic and numerical integrals
err = abs(I_ana-I_num)./I_ana;
nexttile;
plot(err);

% labelling
xlabel('force components')
ylabel('relative error')

% END OF SCRIPT
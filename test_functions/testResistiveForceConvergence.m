% TEST

% Investigating the resisitive force theory approximation of the
% hydrodynamic velocty as q approaches zero.

disp('-----')
clearvars; close all; clc

% Get discretisation:
N = 100;
X = ParabolicInitialCondition(0.2,N);
[x, y, z] = extractComponents(X);
[X_c,s_c] = GetMidpoints(X);
[x_c, y_c, z_c] = extractComponents(X_c);

theta = atan(diff(y)./diff(x));
unitt = [cos(theta); sin(theta); zeros(N-1,1)];
tt = repmat(eye(N-1),3,3).*kron(unitt,unitt');

% Prescribe force:
f_x = 3; f_y = -4; f_z = 0;
f = [repmat(f_x,N,1); repmat(f_y,N,1); repmat(f_z,N,1)];
[f_c,~] = GetMidpoints(f);

% Choose q and b:
q = logspace(-10,-2,500); Q = length(q);
b = 1e-2;

progressbarText(0);
for n = 1:Q
    
    %% Explore values of xi_perp:
    
    xi_perp(n) = (8*pi) / (1+2*log(2*q(n)/b));
    
    %% Velocity values:
   
    xi_para(n) = (8*pi) / (-2 + 4*log(2*q(n)/b));
    gamma(n) = xi_perp(n) / xi_para(n);
    local_op = -(1/xi_perp(n))*(eye(3*(N-1)) + (gamma(n)-1)*tt);
    
    U(:,n) = -local_op * f_c(:);
    
    progressbarText(n/Q);
end % q loop

%% Plots:

colors = parula(Q);

figure;
for n = 1:Q
    
%     subplot(2,2,3); box on; 
%     q_sel = q(1:50);
%     frac = 1./log(q_sel);
%     plot(q_sel,frac);
%     xlabel('$q$','Interpreter','latex'); ylabel('$1/\log(q)$','Interpreter','latex')
    
    subplot(2,2,[3,4]); box on; hold on
    plot(q, xi_perp); 
    xlabel('$q$','Interpreter','latex'); ylabel('$\xi_\perp$','Interpreter','latex')
    
    [u_x, u_y, ~] = extractComponents(U(:,n));
    
    subplot(2,2,1); box on; hold on
    plot(s_c, u_x, 'Color', colors(n,:));
    xlabel('$s$','Interpreter','latex'); ylabel({'$b=10^{-2}$','$U_x$'}, 'Interpreter', 'latex')
    
    subplot(2,2,2); box on; hold on
    plot(s_c, u_y, 'Color', colors(n,:));
    xlabel('$s$','Interpreter','latex'); ylabel('$U_y$','Interpreter','latex')
    
    sgtitle('Local velocity with $\mathcal{O}(10^{-4}) \leq q \leq \mathcal{O}(10^{-1})$', 'Interpreter','latex')
end

disp('-----')

% END OF SCRIPT
% TEST FUNCTION

% NONLOCAL INTEGRAL USING NEAREST
% When computing V (the flagellar velocity contributions due to nonlocal
% hydrodynamic effects) we need to compute an integral over the |s-s[m]|>q
% rather than the usual 0 to L=1.

% In this test function we develop and test extensions to NEAREST to
% incorporate this alteration to the integral.

%% RFT + NEAREST implementation
% RFT for local velocity calculation.
% Nearest-neighbour method for the nonlocal integral.

clearvars; 
disp('-----')
disp('RFT + NEAREST implementation')
disp('-----')

% Set discretisation parameters:
N = 100;         % # of traction nodes.
Q = 4*N ;       % # of quadrature nodes.
ds = 1/N;
dsQ = 1/Q;

% Set other parameters:
S = 10;         % swimming parameter.
S4 = S^4;
iS4 = 1/S4;
eps = 0.01;     % regularisation parameter.

% Get discretisations:
a = 0.1;
X_t = ParabolicInitialCondition(a,N);   % traction discretisation.
X_q = ParabolicInitialCondition(a,Q);   % quadrature discretisation.
s_t = GetArclengthNodes(X_t);
s_q = GetArclengthNodes(X_q);

s_NEAREST = s_t;

% Prescribe force per unit length at traction nodes:
f = [2*ones(N,1); ones(N,1); zeros(N,1)];

% Calculate stokeslet matrix & nearest neighbour matrix:
slets = RegStokeslets(X_t, X_q, eps);
nu = NearestNeighbourMatrix(X_q, X_t);

% Compute velocity using full NEAREST (no local/nonlocal splitting).
% ds multiplier needed as NEAREST solves for f[n]dS(x[n]):
weights = dsQ;
U_full_NEAREST = -iS4 * (slets * nu) * (weights.*f(:));

% Compute local/nonlocal split velocity for different values of q:
q_vals = [5e-3, 5e-2, 0.1];

disp('Computing finite difference matrices...')
[D_s,D_ss,~,~] = BuildFiniteDifferenceMatrices(s_t,'arbitrary');
disp('Complete!')
global Diff
Diff.s = D_s; Diff.ss = D_ss;

% Compute Frenet-Serret basis vectors:
units = blkdiag(D_s,D_s,D_s)*X_t(:);
unitn = CalculateNormals(X_t);
ms = VectorToMatrix(units);
mn = VectorToMatrix(unitn);
for m = 1:size(ms,2)
    mb(:,m) = cross(ms(:,m),mn(:,m));
end
unitb = MatrixToVector(mb);

for p = 1:length(q_vals)
    
    q = q_vals(p);
    
    % LOCAL velocity contribution:    
    b = eps;
    xi_para(p) = (8*pi) / (-2 + 4*log(2*q/b) );     % RFT coefficients.
    xi_perp(p) = (8*pi) / ( 1 + 2*log(2*q/b) );
    gamma(p) = xi_perp(p) / xi_para(p);
    
    ss = repmat(eye(N),3,3).*kron(units, units');   % dyadics.
    nn = repmat(eye(N),3,3).*kron(unitn, unitn');
    bb = repmat(eye(N),3,3).*kron(unitb, unitb');
    
    local_op = -(1/xi_perp(p))*(eye(3*N) + (gamma(p)-1)*ss); 
    local_op_alt = -(1/xi_perp(p))*(nn + bb + gamma(p)*ss);

    U_local(:,p) = iS4 * (local_op) * f(:);
    
    % NONLOCAL velocity contribution:
    sigma = NearestNodes(X_q, X_t, q, '>');
    nonlocal_op = -weights*((slets.*sigma) * nu);
    
    U_nonlocal(:,p) = iS4 * nonlocal_op * f(:);
    
    % LOCAL+NONLOCAL velocity:
    U_split(:,p) = iS4 * (local_op + nonlocal_op) * f(:);
    U_sum(:,p) = U_local(:,p) + U_nonlocal(:,p);
    
end

% Plots

figure;
[x_0,y_0,z_0] = extractComponents(U_full_NEAREST);
[x_a,y_a,z_a] = extractComponents(U_split(:,1));
[x_b,y_b,z_b] = extractComponents(U_split(:,2));
[x_c,y_c,z_c] = extractComponents(U_split(:,3));

[x_1,y_1,z_1] = extractComponents(U_local(:,1));
[x_2,y_2,z_2] = extractComponents(U_local(:,2));
[x_3,y_3,z_3] = extractComponents(U_local(:,3));

[x_l,y_l,z_l] = extractComponents(U_nonlocal(:,1));
[x_m,y_m,z_m] = extractComponents(U_nonlocal(:,2));
[x_n,y_n,z_n] = extractComponents(U_nonlocal(:,3));

subplot(3,3,1); hold on
plot(s_t, x_a); plot(s_t, x_b); plot(s_t, x_c); plot(s_t, x_0, 'k:')
xlabel('s'); ylabel({'local+nonlocal','$U_x+V_x$'},'Interpreter','latex')

subplot(3,3,2); hold on
plot(s_t, y_a); plot(s_t, y_b); plot(s_t, y_c); plot(s_t, y_0, 'k:')
xlabel('$s$', 'Interpreter', 'latex'); ylabel('$U_y+V_y$','Interpreter','latex');
subplot(3,3,3); hold on
plot(s_t, z_a); plot(s_t, z_b); plot(s_t, z_c); plot(s_t, z_0, 'k:')
xlabel('$s$', 'Interpreter', 'latex'); ylabel('$U_z+V_z$','Interpreter','latex')

subplot(3,3,4); hold on
plot(s_t, x_1); plot(s_t, x_2); plot(s_t, x_3); plot(s_t, x_0, 'k:')
xlabel('$s$', 'Interpreter', 'latex'); ylabel({'local','$U_x$'},'interpreter','latex')
subplot(3,3,5); hold on
plot(s_t, y_1); plot(s_t, y_2); plot(s_t, y_3); plot(s_t, y_0, 'k:')
xlabel('$s$', 'Interpreter', 'latex'); ylabel('$U_y$','Interpreter','latex');
subplot(3,3,6); hold on
plot(s_t, z_1); plot(s_t, z_2); plot(s_t, z_3); plot(s_t, z_0, 'k:')
xlabel('$s$', 'Interpreter', 'latex'); ylabel('$U_z$','Interpreter','latex')

subplot(3,3,7); hold on
plot(s_t, x_l); plot(s_t, x_m); plot(s_t, x_n); plot(s_t, x_0, 'k:')
xlabel('$s$', 'Interpreter', 'latex'); ylabel({'nonlocal','$V_x$'},'interpreter','latex')
subplot(3,3,8); hold on
plot(s_t, y_l); plot(s_t, y_m); plot(s_t, y_n); plot(s_t, y_0, 'k:')
xlabel('$s$', 'Interpreter', 'latex'); ylabel('$V_y$','Interpreter','latex')
subplot(3,3,9); hold on
plot(s_t, z_l); plot(s_t, z_m); plot(s_t, z_n); plot(s_t, z_0, 'k:')
xlabel('$s$', 'Interpreter', 'latex'); ylabel('$V_z$','Interpreter','latex')
legend('q=5e-4','q=0.05','q=0.1','q=0','Location','northeast')

% sgtitle(sprintf('NEAREST, $N=%g$, $Q=%g$'.[N,Q]),'Interpreter', 'latex');

disp('-----')

%% RFT + REGULARISED STOKESLET SEGMENTS
% RFT for local velocity.
% Reg stokeslet segments for nonlocal velocity.

clearvars -except U_full_NEAREST s_NEAREST

disp('-----')
disp('RFT + REG STOKESLET SEGMENTS implementation')
disp('-----')

% Set discretisation parameters:
N = 100;        % # of nodes.
Q = N-1;        % # of segments.
ds = 1/Q;       % segment length.

% Set other parameters:
S = 1;         % swimming parameter.
S4 = S^4;
iS4 = 1/S4;
eps = 0.01;     % regularisation parameter.

% Get discretisations:
a = 0.1;
X = ParabolicInitialCondition(a,N);     % discretisation.
s = GetArclengthNodes(X);               % arclength discretisation.
s_c = 0.5*(s(1:end-1)+s(2:end));        % midpoint arclength discretistion.
s_segments = s_c;

% Get X derivatives (for RFT dyadics):
disp('Computing finite difference matrices...')
[D_s,D_ss,~,~] = BuildFiniteDifferenceMatrices(s,'arbitrary');
disp('Complete!')
global Diff
Diff.s = D_s; Diff.ss = D_ss;

% Compute Frenet-Serret basis vectors:
units = blkdiag(D_s,D_s,D_s)*X(:);
unitn = CalculateNormals(X);
ms = VectorToMatrix(units);
mn = VectorToMatrix(unitn);
for m = 1:size(ms,2)
    mb(:,m) = cross(ms(:,m),mn(:,m));
end
unitb = MatrixToVector(mb);

% Prescribe f at nodes:
f = [2*ones(N,1); ones(N,1); zeros(N,1)];
[f_x,f_y,f_z] = extractComponents(f);
     
% Interp1 to get f at segment midpoints:
f_xc = interp1(s, f_x, s_c, 'spline', 'extrap');
f_yc = interp1(s, f_y, s_c, 'spline', 'extrap');
f_zc = interp1(s, f_z, s_c, 'spline', 'extrap');
f_c = [f_xc(:); f_yc(:); f_zc(:)];

% Calculate regularised stokeslet segment matrix:
[x,y,z] = extractComponents(X);
X_c = [0.5*(x(1:end-1)+x(2:end)); ...
       0.5*(y(1:end-1)+y(2:end)); ...
       0.5*(z(1:end-1)+z(2:end))];      % centres of intervals.
[x_c,y_c,z_c] = extractComponents(X_c);

th = atan(diff(y)./diff(x));            % segment tangent angles.
mt = [cos(th)'; sin(th)'; zeros(1,Q)];  % segment tangent vectors.
unitt = MatrixToVector(mt);
R = [cos(th)', -sin(th)', zeros(1,Q);
     sin(th)',  cos(th)', zeros(1,Q);
     zeros(1,Q),zeros(1,Q),ones(1,Q)];  % rotations to align segments with unitx.
slets = RegStokesletAnalyticIntegrals(X_c,X_c,ds/2,R,eps);

% Compute velocity using integral:
U_full_segments = -iS4 * slets * f_c(:);

% Compute local/nonlocal split velocity for different values of q:
q_vals = [5e-3, 5e-2, 0.1];

for p = 1:length(q_vals)
    
    q = q_vals(p);
    
    % LOCAL velocity contribution:
%     b(p) = eps;
%     xi_para = (8*pi) / (-2 + 4*log(2*q/b(p)) );     % RFT coefficients.
%     xi_perp = (8*pi) / ( 1 + 2*log(2*q/b(p)) );
%     gamma = xi_perp / xi_para;
%     ss = repmat(eye(Q),3,3) .* kron(unitt,unitt');
%     local_op = -(1/xi_perp)*(eye(3*Q) + (gamma-1)*ss); 
    b = eps;
    xi_para = (8*pi) / (-2 + 4*log(2*q/b));     % RFT coefficients.
    xi_perp = (8*pi) / ( 1 + 2*log(2*q/b));
    gamma = xi_perp/xi_para;    igamma = 1/gamma;
    tt = repmat(eye(Q),3,3) .* kron(unitt,unitt');
    op = -xi_perp * (eye(3*Q) + (igamma-1)*tt);
%     op = -xi_para * (gamma*eye(3*Q) + (1-gamma)*tt);
    local_op = inv(op);
    
    U_local(:,p) = iS4 * local_op * f_c(:);
    
    % NONLOCAL velocity contribution:
    sigma = NearestNodes(X_c, X_c, q, '>');
    nonlocal_op = -slets.*sigma;
    U_nonlocal(:,p) = iS4 * nonlocal_op * f_c(:);
    
    % LOCAL+NONLOCAL velocity:
    U_split(:,p) = U_local(:,p) + U_nonlocal(:,p);
    
end

% Plots

figure;
[x_0,y_0,z_0] = extractComponents(U_full_segments);
[x_a,y_a,z_a] = extractComponents(U_split(:,1));
[x_b,y_b,z_b] = extractComponents(U_split(:,2));
[x_c,y_c,z_c] = extractComponents(U_split(:,3));

[x_1,y_1,z_1] = extractComponents(U_local(:,1));
[x_2,y_2,z_2] = extractComponents(U_local(:,2));
[x_3,y_3,z_3] = extractComponents(U_local(:,3));

[x_l,y_l,z_l] = extractComponents(U_nonlocal(:,1));
[x_m,y_m,z_m] = extractComponents(U_nonlocal(:,2));
[x_n,y_n,z_n] = extractComponents(U_nonlocal(:,3));

subplot(3,3,1); hold on
% s_t = s(1:end-1)+s(2:end);
s_t = s_c;
plot(s_t, x_a); plot(s_t, x_b); plot(s_t, x_c); plot(s_t, x_0, 'k:')
xlabel('$s$','Interpreter','latex'); ylabel({'local+nonlocal','$U_x+V_x$'}, 'Interpreter', 'latex')
subplot(3,3,2); hold on
plot(s_t, y_a); plot(s_t, y_b); plot(s_t, y_c); plot(s_t, y_0, 'k:')
xlabel('$s$','Interpreter','latex'); ylabel('$U_y+V_y$','Interpreter','latex');
subplot(3,3,3); hold on
plot(s_t, z_a); plot(s_t, z_b); plot(s_t, z_c); plot(s_t, z_0, 'k:')
xlabel('$s$','Interpreter','latex'); ylabel('$U_z+V_z$','Interpreter','latex')

subplot(3,3,4); hold on
plot(s_t, x_1); plot(s_t, x_2); plot(s_t, x_3); plot(s_t, x_0, 'k:')
xlabel('$s$','Interpreter','latex'); ylabel({'local','$U_x$'}, 'Interpreter', 'latex')
subplot(3,3,5); hold on
plot(s_t, y_1); plot(s_t, y_2); plot(s_t, y_3); plot(s_t, y_0, 'k:')
xlabel('$s$','Interpreter','latex'); ylabel('$U_y$', 'Interpreter', 'latex')
subplot(3,3,6); hold on
plot(s_t, z_1); plot(s_t, z_2); plot(s_t, z_3); plot(s_t, z_0, 'k:')
xlabel('$s$','Interpreter','latex'); ylabel('$U_z$', 'Interpreter', 'latex')
% legend('q=5e-4','q=0.05','q=0.1','q=0','Location','northeast')

subplot(3,3,7); hold on
plot(s_t, x_l); plot(s_t, x_m); plot(s_t, x_n); plot(s_t, x_0, 'k:')
xlabel('$s$','Interpreter','latex'); ylabel({'nonlocal','$V_x$'}, 'Interpreter', 'latex')
subplot(3,3,8); hold on
plot(s_t, y_l); plot(s_t, y_m); plot(s_t, y_n); plot(s_t, y_0, 'k:')
xlabel('$s$','Interpreter','latex'); ylabel('$V_y$', 'Interpreter', 'latex')
subplot(3,3,9); hold on
plot(s_t, z_l); plot(s_t, z_m); plot(s_t, z_n); plot(s_t, z_0, 'k:')
xlabel('$s$','Interpreter','latex'); ylabel('$V_z$', 'Interpreter', 'latex')
legend('q=5e-4','q=0.05','q=0.1','q=0','Location','northeast')

sgtitle(sprintf('Regularised stokeslet segments, $N=%g$',N), 'Interpreter', 'latex')

%% COMPLETE REG STOKESLET SEGMENTS
% Use regularised stokeslet segments for both the local velocity operator
% and the nonlocal velocity term.

clearvars; 

disp('-----')
disp('FULL REG STOKESLET SEGMENTS')
disp('-----')

% Set discretisation parameters:
N = 300;        % # of nodes.
Q = N-1;        % # of segments.
ds = 1/Q;       % segment length.
ds = repmat(ds,Q,1);

% Set other parameters:
S = 1;          % swimming parameter.
S4 = S^4;
iS4 = 1/S4;
eps = 0.01;     % regularisation parameter.

% Get discretisations:
a = 0.1;
X = ParabolicInitialCondition(a,N);     % discretisation.
s = GetArclengthNodes(X);               % arclength discretisation.
s_c = 0.5*(s(1:end-1)+s(2:end));        % midpoint arclength discretistion.

% Prescribe f at nodes:
f = [2*ones(N,1); ones(N,1); zeros(N,1)];
[f_x,f_y,f_z] = extractComponents(f);
     
% Interp1 to get f at segment midpoints:
f_xc = interp1(s, f_x, s_c, 'spline', 'extrap');
f_yc = interp1(s, f_y, s_c, 'spline', 'extrap');
f_zc = interp1(s, f_z, s_c, 'spline', 'extrap');
f_c = [f_xc(:); f_yc(:); f_zc(:)];

% Calculate regularised stokeslet segment matrix:
[x,y,z] = extractComponents(X);
X_c = [0.5*(x(1:end-1)+x(2:end)); ...
       0.5*(y(1:end-1)+y(2:end)); ...
       0.5*(z(1:end-1)+z(2:end))];      % centres of intervals.
[x_c,y_c,z_c] = extractComponents(X_c);

th = atan(diff(y)./diff(x));            % segment tangent angles.
mt = [cos(th)'; sin(th)'; zeros(1,Q)];  % segment tangent vectors.
unitt = MatrixToVector(mt);
R = [cos(th)', -sin(th)', zeros(1,Q);
     sin(th)',  cos(th)', zeros(1,Q);
     zeros(1,Q),zeros(1,Q),ones(1,Q)];  % rotations to align segments with unitx.
seglets = RegStokesletAnalyticIntegrals(X,X_c,ds/2,R,eps);

% Veloctity with no splitting:
U_0 = -iS4 * seglets * f_c(:);

% Choose q values:
q = logspace(-4,-1,50); Nq = length(q);

progressbarText(0);
for n = 1:Nq
    
    % LOCAL VELOCITY:
    sigma_local = NearestNodes(X_c, X, q(n), '<=');
    qseglets = RegStokesletAnalyticIntegrals(X,X_c, ds/2, R, eps);
    local_op = -sigma_local .* qseglets;
    
    U_local(:,n) = iS4 * local_op * f_c(:);
    
    % NONLOCAL VELOCITY:
    sigma_nonlocal = NearestNodes(X_c, X, q(n), '>');
    nonlocal_op = -sigma_nonlocal .* seglets;
    U_nonlocal(:,n) = iS4 * nonlocal_op * f_c(:);
    
    % COMBINED:
    combined_op = local_op + nonlocal_op;
    U(:,n) = iS4 * combined_op * f_c(:);
    
    progressbarText(n/Nq);
end

% Plots:
% id_sel = [1, 6, 10];
id_sel = 1:length(q);
Nsel = length(id_sel);
cols = parula(Nsel);

figure
for k = 1:Nsel
    
    [u_x,u_y,u_z] = extractComponents(U(:,id_sel(k)));
    
    subplot(1,3,1); hold on; box on;
    plot(s, u_x, 'Color', cols(k,:));
    xlabel('$s$', 'Interpreter', 'latex'); ylabel('$u_x$', 'Interpreter', 'latex')

    subplot(1,3,2); hold on; box on;
    plot(s, u_y, 'Color', cols(k,:));
    xlabel('$s$', 'Interpreter', 'latex'); ylabel('$u_y$', 'Interpreter', 'latex')
    
    subplot(1,3,3); hold on; box on;
    plot(s, u_z, 'Color', cols(k,:));
    xlabel('$s$', 'Interpreter', 'latex'); ylabel('$u_z$', 'Interpreter', 'latex')
    
    if k == Nsel % plot U computed with a single integral:
        [u_x,u_y,u_z] = extractComponents(U_0);
        subplot(1,3,1); plot(s, u_x, 'k--');
        subplot(1,3,2); plot(s, u_y, 'k--');
        subplot(1,3,3); plot(s, u_z, 'k--');
    end

end



disp('-----')

% END OF SCRIPT
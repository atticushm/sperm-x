
% Testing the accuracy of code computing the analytically integrated
% analytically differentiated regularised stokeslets, against code which
% computes the numerically integrated analytically differentiated
% regularised stokeslets.

clear all; close all;

disp('-----')

% Define a segment:
X_source = [1;0.5;0];        % segment midpoint.
ds = 1/50;              % segment length.
th = pi/6;              % tangent angle of segment.

disp(['Single segment | th=',num2str(th),' ds=',num2str(ds)])

% End points of line segment; coordinates and arclength:
X_a = X_source - (ds/2)*[cos(th), sin(th), 0]';     s_a = -ds/2;
X_b = X_source + (ds/2)*[cos(th), sin(th), 0]';     s_b = ds/2;

% Tangent along the segment is:
t = [cos(th), sin(th), 0]';

% Rotation matrix for source point is:
R_source = RotationMatrix(th);       % Rotation matrix of segment.

% Collocate at a single point:
% X_f = X_a;
% R_f = R_c;

% *** or *** collocate at segment endpoints:
X_field = MatrixToVector([X_a,X_b]);
T_field = kron(t(:),ones(2,1));

%% Integrate derivative along segment analytically:

epsilon = 1e-2;
I.ana = RegStokesletAnalyticDerivativeIntegrals(X_field, X_source, T_field, R_source, ds/2, epsilon);
n.ana = norm(I.ana);

disp('Analytic integrals of analytic derivatives:')
disp(I.ana);

%% Numerical integration:

% Number of nodes for integration:
N = 50;

% Step size for numerical differentiation:
h = 1e-4;

% Lookup nodes and weights for Gauss-Legendre quadrature:
[s_i, w_i] = lgwt(N, -ds/2, ds/2);
[s_i,idx] = sort(s_i);
w_i = w_i(idx);
ds_i = diff(s_i);

% Compute X_i = X(s_i);
x_i = interp1([-ds/2,ds/2],[X_a(1),X_b(1)],s_i,'linear');
y_i = interp1([-ds/2,ds/2],[X_a(2),X_b(2)],s_i,'linear');
z_i = zeros(length(s_i),1);
X_i = [x_i(:); y_i(:); z_i(:)]; XM_i = VectorToMatrix(X_i);

% Number of field points:
N_f = length(X_field)/3;
t = kron(t,ones(N_f,1));

% Compute integral: 
sum_1 = 0;
sum_2 = 0;
for i = 1:N
    wait_str = sprintf('2nd order FD (h=%.1e) | Gauss-Legendre (N=%.1e)',h,N);
    textwaitbar(i,N,wait_str);
    sum_1 = sum_1 + w_i(i)/(2*h) * (RegStokeslets( X_field + h*t, XM_i(:,i), epsilon) - RegStokeslets(X_field - h*t, XM_i(:,i), epsilon));
    sum_2 = sum_2 + w_i(i)*RegStokesletAnalyticDerivatives(X_field, XM_i(:,i), epsilon, T_field);
end
I.gl_fd = sum_1;
I.gl = sum_2;
n.gl_fd = norm(I.gl_fd);
n.gl = norm(I.gl);

disp('Numerical integrals of numerical derivatives:')
disp(I.gl_fd);

disp('Numerical integrals of analytic derivatives:')
disp(I.gl);

%% Sanity tests:

% Check numerical derivative and analytic derivative:
[D.analytic,D.analytic_3] = RegStokesletAnalyticDerivatives(X_field, X_a, epsilon, T_field);
D.numerical = 1/(2*h) * (RegStokeslets(X_field + h*t, X_a, epsilon) - RegStokeslets(X_field - h*t, X_a, epsilon));
[~,D.cara_3] = CVN_CalculateStokesletDerivatives(X_field,X_a,zeros(3,1),epsilon);

disp('Derivative checks:')
disp('Analytic derivatives:')
disp(D.analytic)
disp('Numerical (2nd order central FD) derivatives:')
disp(D.numerical)

disp('-----')


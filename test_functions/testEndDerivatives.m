% TEST_ENDDERIVATIVES

% Test function evaluating if the endpoint derivative schemes created by
% buildDifferenceSchemes (using a [-1, 0, 1, 2, ...] stencil) are working.

clear all; close all

% Define a function to differentiate: 
k =  pi;
y = @(x) cos(k*x(:));
y_x = @(x) -k * sin(k*x(:));
y_xx = @(x) -k^2 * cos(k*x(:));
y_xxx = @(x) k^3 * sin(k*x(:));
y_xxxx = @(x) k^4 * cos(k*x(:));


% Define spatial grid:
N = 25;
x = linspace(0,pi,N); x = x(:);

%% Analytic derivatives:

Y_x_ana = y_x(x);
Y_xx_ana = y_xx(x);
Y_xxx_ana = y_xxx(x);
Y_xxxx_ana = y_xxxx(x);

%% Derivatives using one-sided scheme:

h = diff(x); h = h(1);
D_ons = finiteDifferences(x,1:4,'nohead');
D_x = D_ons{1}; D_xx = D_ons{2}; 
D_xxx = D_ons{3}; D_xxxx = D_ons{4};

Y_x_ons = D_x * y(x);
Y_xx_ons = D_xx * y(x);
Y_xxx_ons = D_xxx * y(x);
Y_xxxx_ons = D_xxxx * y(x);

%% Derivatives using additional grid point:

xm1 = [x(1)-h; x(:)];
D_ext = finiteDifferences(x,1:4,'head');
D_x = D_ext{1}; D_xx = D_ext{2}; 
D_xxx = D_ext{3}; D_xxxx = D_ext{4};

Y_x_ext = D_x * y(xm1);
Y_xx_ext = D_xx * y(xm1);
Y_xxx_ext = D_xxx * y(xm1);
Y_xxxx_ext = D_xxxx * y(xm1);

%% Error:

x_ons_error = MeanSqError(Y_x_ana,Y_x_ons);
x_ext_error = MeanSqError(Y_x_ana,Y_x_ext);

xx_ons_error = MeanSqError(Y_xx_ana,Y_xx_ons);
xx_ext_error = MeanSqError(Y_xx_ana,Y_xx_ext);

%% Plots:

figure;
subplot(2,2,1); box on; hold on;
xlabel('x'); ylabel('y');
plot(x,Y_x_ana,'-');
plot(x,Y_x_ons,':');
plot(x,Y_x_ext,'-');
title('Y_x')

subplot(2,2,2); box on; hold on;
xlabel('x'); ylabel('y');
plot(x,Y_xx_ana,'-');
plot(x,Y_xx_ons,':');
plot(x,Y_xx_ext,'-');
title('Y_{xx}')

subplot(2,2,3); box on; hold on;
xlabel('x'); ylabel('y');
plot(x,Y_xxx_ana,'-');
plot(x,Y_xxx_ons,':');
plot(x,Y_xxx_ext,'-');
title('Y_{xxx}')

subplot(2,2,4); box on; hold on;
xlabel('x'); ylabel('y');
plot(x,Y_xxx_ana,'-');
plot(x,Y_xxx_ons,':');
plot(x,Y_xxx_ext,'-');
title('Y_{xxxx}')

legend('Analytic','One-sided','Extended stencil')



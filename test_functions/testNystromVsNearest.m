function NystromVsNearest()

% Solving a resistance problem for a unit sphere using Nystrom and NEAREST.

% levels of discretisation
num     = 15;
N_trac  = round(linspace(3,16,num));
N_quad2 = 2*N_trac;
N_quad4 = 4*N_trac;

% radius of sphere
b = 1;

% centre of spheres
r = [0;0;0];

% regularisation parameter
epsilon = 0.01;

% ~~ solve resistance problems ~~
if ~exist('./NystromVsNearest.mat','file')
    fprintf('Solving resistance problems....\n')
    for n = 1:num

        fprintf('%4g / %4g\n',n,num);

        % generate discretisations of spheres
        x{n}  = GenerateSphereDisc(N_trac(n), b);
        X2{n} = GenerateSphereDisc(N_quad2(n),b);
        X4{n} = GenerateSphereDisc(N_quad4(n),b);

        % numbers of nodes
        Nx(n)  = length(x{n})/3;
        NX2(n) = length(X2{n})/3;
        NX4(n) = length(X4{n})/3;

        % solve and time resistance problems
        tic
        R_nys{n} = ComputeR(x{n},x{n}, r,epsilon,1);
        T_nys(n) = toc;
        tic
        R_nn2{n} = ComputeR(x{n},X2{n},r,epsilon,1);
        T_nn2(n) = toc;
        tic;
        R_nn4{n} = ComputeR(x{n},X4{n},r,epsilon,1);
        T_nn4(n) = toc;
    end
    save('NystromVsNearest.mat','-v7.3')
else
    load('NystromVsNearest.mat')
end

% ~~ extract F and M ~~
for i = 1:num
    F_nys(i,1) = -R_nys{i}(1,1);
    F_nn2(i,1) = -R_nn2{i}(1,1);
    F_nn4(i,1) = -R_nn4{i}(1,1);
    M_nys(i,1) = -R_nys{i}(4,4);
    M_nn2(i,1) = -R_nn2{i}(4,4);
    M_nn4(i,1) = -R_nn4{i}(4,4);
end

% ~~ compute error ~~
F_ex = 6*pi*b;
M_ex = 8*pi*b^3;

E_nys = abs(F_nys - F_ex)/F_ex;
E_nn2 = abs(F_nn2 - F_ex)/F_ex;
E_nn4 = abs(F_nn4 - F_ex)/F_ex;

% ~~ plots ~~
close all

% performance graph
nexttile; hold on; box on;
yyaxis right
ylabel('Absolute relative error','FontSize',12, 'Interpreter', 'latex');
plot(Nx, E_nys, 'LineWidth', 1.4);
yyaxis left
plot(Nx, E_nn2, 'LineWidth', 1.4);
plot(Nx, E_nn4, 'LineWidth', 1.4);
xlabel('$N_t$','FontSize', 12, 'Interpreter', 'latex')
ylabel('Absolute relative error','FontSize',12, 'Interpreter', 'latex');
legend('$N_q=N_t$','$N_q=2N_t$','$N_q=4N_t$', 'Interpreter','latex', 'FontSize', 12)

set(gcf, 'Position', [829 938 488 222])

% nexttile; hold on; box on;
% plot(Nx, T_nys, 'LineWidth', 1.4);
% plot(Nx, T_nn2, 'LineWidth', 1.4);
% plot(Nx, T_nn4, 'LineWidth', 1.4);
% xlabel('$N_t$','FontSize', 12, 'Interpreter', 'latex')
% ylabel('Wall time','FontSize',12, 'Interpreter', 'latex');
% legend('$N_q=N_t$','$N_q=2N_t$','$N_q=4N_t$', 'Interpreter','latex', 'Location', 'northwest', 'FontSize', 12)

% discretisation graph
figure; 
nexttile; hold on; box on;
[x1,x2,x3] = extractComponents(x{1});
[X1,X2,X3] = extractComponents(x{1});
scatter3(X1,X2,X3,'x');
scatter3(x1,x2,x3,100,'.');
view(2);
xlabel('$x$', 'FontSize', 12, 'Interpreter', 'latex')
ylabel('$y$', 'FontSize', 12, 'Interpreter', 'latex')

nexttile; hold on; box on;
[x1,x2,x3] = extractComponents(x{1});
[X1,X2,X3] = extractComponents(X4{1});
scatter3(x1,x2,x3,100,'.');
scatter3(X1,X2,X3,'x');
view(2);
xlabel('$x$', 'FontSize', 12, 'Interpreter', 'latex')
ylabel('$y$', 'FontSize', 12, 'Interpreter', 'latex')

set(gcf, 'Position', [1345 993 654 269]);

% ~~ save 
figure(1); save2pdf('./figures/nearest_nystrom_bench.pdf')
figure(2); save2pdf('./figures/sphere_discretisation.pdf')

end % function
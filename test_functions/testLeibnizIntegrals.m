% TEST_LeibnizIntegral

% Previous tests of the function RegStokesletAnalyticDerivativeIntegrals,
% which encodes the third integral term in the Leibniz rule application,
% have used arbirtary limits and field points, which could be argued to be
% an unfair or unrepresentative application of the method - in use, field
% points will always be the nodes at each end of segments, with midpoints
% of segments being the source points. Limits of the integral are dependant
% on the source point considered and not arbitrary as previously
% considered.

clear all; clc
disp('-----')

num_runs = 8;
N_vals(1) = 11;
for n = 1:num_runs-1
    N_vals(n+1) = 2*N_vals(n)-1;    % double the number of segments on each run.
end

for k = 1:2
    
    % Build a high resolution curve:
    if k == 1
        X = ParabolicCurve(0.5,1e4); [x,y,z] = extractComponents(X);
    elseif k == 2
        X = SinusoidalCurve(3*pi,0.2,1e4); [x,y,z] = extractComponents(X);
    end
    s = linspace(0,1,1e4);
    ppx = spline(s,x);  ppy = spline(s,y);  ppz = spline(s,z);
    dpx = fnder(ppx,1); dpy = fnder(ppy,1); dpz = fnder(ppz,1);

    for n = 1:num_runs

        N = N_vals(n); 
        Q = N-1;
        ds = 1/Q;
        epsilon = 1e-2;

        % Choose q as L/10:
        q = 1/10;

        % Filament arclength:
        s = linspace(0,1,N); s = s(:);
        s_n{n} = s;
            
        % Choose field points as nodes:
        field_id = 1:N;
        s_field = s(field_id);
        x = ppval(ppx,s); y = ppval(ppy,s); z = ppval(ppz,s);
        X_field = [x(field_id); y(field_id); z(field_id)];  XM_field = VectorToMatrix(X_field);
        T_field = [ppval(dpx,s_field); ppval(dpy,s_field); ppval(dpz,s_field)]; TM_field = VectorToMatrix(T_field);
        num_field = length(field_id);

        % Find segment midponts:
        s_midp = 0.5*(s(1:end-1)+s(2:end));
        X_midp = [ppval(ppx,s_midp); ppval(ppy,s_midp); ppval(ppz,s_midp)];
        T_midp = [ppval(dpx,s_midp); ppval(dpy,s_midp); ppval(dpz,s_midp)];
        [tx_midp,ty_midp,tz_midp] = extractComponents(T_midp);
        th_midp = asin(T_midp(Q+1:2*Q));
        R_midp = RotationMatrix(th_midp);

        % Interval of integration around the field points:
        s_a = s_field -q;       s_b = s_field +q;
        
        % Values of s_a/s_b outisde of [0,1] need to be corrected:
        s_a(s_a<0) = 0;         s_b(s_b>1.0) = 1;

        %% Analytical method:

        % Identify midpoints within the local interval of each field point:
        [x_midp,y_midp,z_midp] = extractComponents(X_midp);
        
        local_id = abs(s_midp-s_field')<=q;
        
        x_source = repmat(x_midp,1,N).*local_id;
        y_source = repmat(y_midp,1,N).*local_id;
        z_source = repmat(z_midp,1,N).*local_id;

        for m = 1:num_field
            xs = x_source(:,m); ys = y_source(:,m); zs = z_source(:,m);
            XMs = [xs';ys';zs'];
            XMs(:,~any(XMs,1)) = [];
            X_source{m} = MatrixToVector(XMs);
            R_source{m} = RotationMatrix(th_midp(local_id(:,m)));
            num_source{m} = length(X_source{m})/3;
        end

        % Compute integrals:
        for m = 1:num_field
            int{m} = RegStokesletAnalyticDerivativeIntegrals(XM_field(:,m),X_source{m},TM_field(:,m),R_source{m},ds/2,epsilon)...
                     *kron(eye(3),ones(num_source{m},1));
            I.ana{n,k}(m,:) = int{m}(1,:);
            I.ana{n,k}(m+num_field,:) = int{m}(2,:);
            I.ana{n,k}(m+2*num_field,:) = int{m}(3,:);
        end
        
        % ** vectorise:
        clear int
        int = RegStokesletAnalyticDerivativeIntegrals(X_field,X_midp,T_field,R_midp,ds/2,epsilon);
        int = repmat(local_id',3,3).*int;
        int = int * kron(eye(3),ones(Q,1));
        I.ana_v{n,k} = int;
        vec_check(n,k) = norm(abs(I.ana{n,k} - I.ana_v{n,k}));
        
        %% Numerical method:

        % Parameter for numerical differentiation:
        h = 1e-10;

        % Number of nodes for numerical integration:
        num_nodes = 1e2;
        
        % For numerical differentiation we need coordinates at s+h and s-h:
        X_fph  = [ppval(ppx,s_field+h); ppval(ppy,s_field+h); ppval(ppz,s_field+h)];
        X_fmh  = [ppval(ppx,s_field-h); ppval(ppy,s_field-h); ppval(ppz,s_field-h)];
        XM_fph = VectorToMatrix(X_fph);     XM_fmh = VectorToMatrix(X_fmh);

        % Lookup Gauss-Legendre nodes and weights in the interval of
        % integration for each field point:
        for m = 1:num_field
            clear idx
            [si{m},wi{m}] = lgwt(num_nodes,s_a(m),s_b(m));
            [si{m},idx] = sort(si{m});    wi{m} = wi{m}(idx);
            Xi{m} = [ppval(ppx,si{m}); ppval(ppy,si{m}); ppval(ppz,si{m})]; XMi{m} = VectorToMatrix(Xi{m});
        end

        % Compute integrals of derivatives:
        clear int
        for m = 1:num_field
            int{m} = 0;
            for i = 1:num_nodes
                int{m} = int{m} + wi{m}(i)/2/h * (RegStokeslets(XM_fph(:,m),XMi{m}(:,i),epsilon) - RegStokeslets(XM_fmh(:,m),XMi{m}(:,i),epsilon));
            end
            I.num{n,k}(m,:) = int{m}(1,:);
            I.num{n,k}(m+num_field,:) = int{m}(2,:);
            I.num{n,k}(m+2*num_field,:) = int{m}(3,:);
        end
        
    end
end

%% Inspect matrices:

%{
disp('Analytical integrals:')
celldisp(I.ana)
% celldisp(xycomp.ana)
% celldisp(zcomp.ana)
disp('-----')
disp('Numerical integrals:')
celldisp(I.num)
% celldisp(xycomp.num)
% celldisp(zcomp.num)
disp('-----')
%}

%% Error plots:

close all

% Compute errors at coinciding nodes between analytic method and high-resolution numerical method:
for k = 1:2
    mult = 2^7;
    for n = 1:num_runs
        N = length(I.num{end,k})/3;
        idx = 1:mult:N;
        mult = mult/2;
        num_comp{1} = I.num{end,k}(idx,:);
        num_comp{2} = I.num{end,k}(N+idx,:);
        num_comp{3} = I.num{end,k}(2*N+idx,:);
        comp{n,k} = [num_comp{1}; num_comp{2}; num_comp{3}];
        error(n,k) = norm(abs(I.ana_v{n,k}-comp{n,k}));
    end
end

figure; 
loglog(N_vals, error(:,1), '.-'); grid on; box on; hold on;
loglog(N_vals, error(:,2),'.-')
xlabel('$N$','Interpreter','latex')
ylabel(['Error vs. numerical method, $h=\,$',num2str(h)],'Interpreter','latex')
titlestr = ['Calculating $\mathbf{R}^{-1}(s,q)$ for $s\in[0,1], \,q=\,$',num2str(q)];
title(titlestr,'Interpreter','latex')
legend('parabolic curve','sinusoidal curve','Interpreter','latex')


f = [1; -2; 0];     % prescribed force.
X_c = [1;1;1];      % segment centreline midpoint.
theta = pi/4;       % filament orientation.

q = 1e-1;

%% RFT:

b = 1e-2;
xi_perp = 8*pi / (1+2*log(2*q/b));
xi_para = 8*pi / (-2+4*log(2*q/b));
gamma = xi_perp/xi_para;
unitt = [cos(theta); sin(theta); 0];
rft_op =  -(1/xi_perp)*(eye(3) + (gamma-1)*kron(unitt,unitt'));

U_rft = rft_op * f(:);

%% Reg. stokeslet segment:

epsilon = b;
R = GetRotationMatrix(theta);
rss_op = -regStokesletAnalyticIntegrals(X_c, X_c, q/2, R, epsilon);

U_rss = rss_op * f(:);
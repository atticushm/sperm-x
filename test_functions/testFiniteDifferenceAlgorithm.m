% TEST FUNCTION

% Testing ComputeFiniteDifferenceCoefficients.m and
% BuildFiniteDifferenceMatrices.m

% The above functions are testing by approximating the solution to the
% boundary value problem:
% y''(x) + y(x) = 0, y(0) = 0, y(pi/2) = 2,
% which has solution:
% y(x) = 2 sin(x).

clear variables; close all
disp('-----')

points = [10,20,40,80,160]; % number of points for each discretisation.

x_lims = [0, pi/2];

% Compute analytical solution:
x_hires = linspace(x_lims(1),x_lims(2),1e5);
y_anal = 2 * sin(x_hires);

for m = 1:length(points)
    
    no_points = points(m);
    
    % Discretised interval:
    dx = 0.5*(1/no_points); % level of randomness, less than node separation.
    r = x_lims(1) + (x_lims(2)-x_lims(1)).*rand(1,no_points-2);
    
    x_equi = linspace(x_lims(1),x_lims(2),no_points);
    x_arb = unique([x_equi(1), x_equi(2:end-1)+dx.*r, x_equi(end)]);

    %% Solve using finite difference method:

    % Equispaced grid:
    [~,D_ss_equi,~,~] = BuildFiniteDifferenceMatrices(x_equi, 'equispaced');
    b_equi = zeros(no_points,1);
    b_equi(1) = 0; b_equi(end) = 2; % apply bcs.
    A_equi = D_ss_equi + eye(no_points);
    A_equi(1,:) = [1,zeros(1,no_points-1)];
    A_equi(end,:) = [zeros(1,no_points-1),1];
    y_equi = sparse(A_equi)\b_equi;

    % Abitrary grid:
    [~,D_ss_arb,~,~] = BuildFiniteDifferenceMatrices(x_arb, 'arbitrary');
    b_arb = zeros(no_points,1);
    b_arb(1) = 0; b_arb(end) = 2;
    A_arb = D_ss_arb + eye(no_points);
    A_arb(1,:) = [1,zeros(1,no_points-1)];
    A_arb(end,:) = [zeros(1,no_points-1),1];
    y_arb = sparse(A_arb)\b_arb;
    
    % Hard-coded finite difference method:
    ds = diff(x_equi); ds = ds(1);
    M = no_points;
    D_ss_hc = 1/ds^2 * ( diag(ones(M-1,1), -1) + diag(-2*ones(M,1)) + diag(ones(M-1,1), 1) );
    D_ss_hc(1,:) = 1/ds^2 * horzcat([35/12, -26/3, 19/2, -14/3, 11/12], zeros(1,M-5));
    D_ss_hc(M,:) = fliplr(D_ss_hc(1,:));
    A_hc = D_ss_hc + eye(no_points);
    A_hc(1,:) = [1,zeros(1,no_points-1)];
    A_hc(end,:) = [zeros(1,no_points-1),1];
    b_hc = b_equi;
    y_hc = sparse(A_hc)\b_hc;
    
    % Store solution data:
    equi(m).x = x_equi(:);
    equi(m).y = y_equi(:);
    arbi(m).x = x_arb(:);
    arbi(m).y = y_arb(:);
    hc(m).x = x_equi(:);
    hc(m).y = y_hc(:);
    
    disp('-----')
    
end

%% Compute MSE

for m = 1:length(points)
    % Compute MSEs:
    equi(m).error = MeanSqError(y_anal,equi(m).y);
    arbi(m).error = MeanSqError(y_anal,arbi(m).y);
    hc(m).error = MeanSqError(y_anal,hc(m).y);
end

%% Plots

line_col = parula(length(points)+2); % setup line colours.
% line_col = line_col(3:end-2,:); % exclude extremes of colourspace.

% Plot solution vectors:
figure(1); subplot(1,3,1); box on
hold on;
title('equispaced grid'); xlabel('x'); ylabel('y')
plot(x_hires,y_anal,'k:','LineWidth',1.5)
for m = 1:length(points)
    plot(equi(m).x,equi(m).y, 'Color', line_col(m,:))
end
lgd = legend('anal.','N=40','N=80','N=160','N=320','Location','south');
lgd.NumColumns = 3;

subplot(1,3,2); box on
hold on;
title('arbitrary grid'); xlabel('x'); ylabel('y')
plot(x_hires,y_anal,'k:','LineWidth',1.5)
for m = 1:length(points)
    plot(arbi(m).x,arbi(m).y, 'Color', line_col(m,:))
end
lgd = legend('anal.','N=40','N=80','N=160','N=320','Location','south');
lgd.NumColumns = 3;

subplot(1,3,3); box on
hold on;
title('hardcoded FDM'); xlabel('x'); ylabel('y')
plot(x_hires,y_anal,'k:','LineWidth',1.5)
for m = 1:length(points)
    plot(hc(m).x,hc(m).y, 'Color', line_col(m,:))
end
lgd = legend('anal.','N=40','N=80','N=160','N=320','Location','south');
lgd.NumColumns = 3;

set(gcf, 'Position', [335 350 1384 406])

% % Plot errors:
% figure(2); box on; hold on;
% loglog(points, horzcat(equi.error),'.-')
% loglog(points, horzcat(arbi.error),'.-')
% loglog(points, horzcat(hc.error),'.-')
% lgd = legend('equi.','arbi.','hard-coded');

disp('-----')

% END OF SCRIPT
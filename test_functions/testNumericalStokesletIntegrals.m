% test function for assessing accuracy of stokeslet numerical integral code

% number of nodes
NF = 251;

% generate a curve
X = parabolicCurve(0.5, NF);

% field points are the nodes themselves
X_field = X;

% stokeslet regularisation parameter
epsilon = 1e-2;

% build location 
proc = 'cpu';

% parameters
ds = 1/(NF-1);
q = 5*ds;
b = 1e-2;  
[c_perp, c_para, gamma] = calcResistanceCoefficients(1,q,b,'rss');

% dummy force per unit length data
s  = linspace(0,1,NF);
fx = cos(s(:));
fy = -0.5*sin(s(:));
fz = s(:).^2;
f  = [fx; fy; fz];

%% numerical stokeslet integrals

% source points are field points
X_source = X_field;

% compute nonlocalstokeslet integrals
D_num = regStokesletNumericalIntegrals(X_field, X_source, epsilon, proc);

% multiply onto forces
I_num = D_num * f;

%% semi analytic stokeslet integrals

% source points are midpoints of segments
X_midp   = midpoints(X);
X_source = X_midp;

% field points are midpoints
X_field = X_midp;

% midpoint values of forces
f_midp = midpoints(f);

% segment lengths are
ds = 1/(NF-1);

% determine tangent angles of segments
[x,y,z] = extractComponents(X);
theta   = atan(diff(y)./diff(x));

% build rotation matrix (requires planar curve...)
ze = 0*theta';
R  = [cos(theta'), -sin(theta'), ze ; ...
      sin(theta'), cos(theta'),  ze ; ...
      ze,          ze,  ones(1,NF-1)];

% compute stokeslet integrals
D_ana = regStokesletAnalyticIntegrals(X_field, X_source, ds/2, R, epsilon);

% multiply onto force midpoints
I_ana_midp = D_ana * f_midp;

% spline to get integral values at nodes
[Ix, Iy, Iz] = extractComponents(I_ana_midp);
s_midp  = 0.5*(s(1:end-1)+s(2:end));
I_ana_x = spline(s_midp, Ix, s);
I_ana_y = spline(s_midp, Iy, s);
I_ana_z = spline(s_midp, Iz, s);
I_ana   = [I_ana_x(:); I_ana_y(:); I_ana_z(:)];

%% nearest-neighbour integration

% traction/force discretisation is nodes
X_trac = X;

% spline curve to get quadrature discretisation
mult   = 3;
X_quad = splineFlagellum(X, mult);

% compute nearest neighbour matrix
NN = nearestNeighbourMatrix(X_quad, X_trac, proc);

% stokeslet matrix between quad and trac discretisations
S = regStokeslet(X_trac, X_quad, epsilon, proc);

% compute stokeslet integrals
D_nn = S*NN;

% multiply onto forces 
I_nn = D_nn * f;

%% comparisons

% matrix representations
I_num_mat = mat(I_num);
I_ana_mat = mat(I_ana);
I_nn_mat  = mat(I_nn);

% plot integrals
figure; 
nexttile; hold on; box on;
plot(I_num,'-');
plot(I_ana,'--');
% plot(I_nn);
legend('numerical','semi-analytic')
title(sprintf('N=%g',NF))

% error between analytic and numerical integrals
err = abs(I_ana-I_num)./I_ana;
nexttile;
plot(err);

% labelling
xlabel('force components')
ylabel('relative error')


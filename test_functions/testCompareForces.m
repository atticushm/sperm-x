% TEST_COMPAREFORCES

% Compare the computed head and flagellar force densities between the EIF
% and Sperm-X method, with the aim of identifying where the Sperm-X method
% is incorrect.

clear all; close all;

%% general setup:

% number of flagellar segments:
Q = 60;

% dimensions of cell head:
hax = [2;1;1.6]/45;

% discretisation parameters for cell head:
Ht = 4;             % traction discr.
Hq = 2*Ht;          % quadratuture discr.

% parameters for initial condition:
grad = 0.5;

% niave initial condition - will be updated with presolve from EIF:
X_int = ParabolicCurve(grad, Q+1);

% dimensionless actuation amplitude:
m0 = 0;

% maximum time:
tmax = 1;

% number of time points:
nt = 100;

% swimming number:
calS = 2.0;

% dimensionless actuation wave number:
wavk = 4*pi;

%% sperm-x:

% local hydrodynamic region parameter:
q = 0.1;

% slender body parameters:
[c_perp,~,gamma] = CalcResistanceCoefficients(1,q,1e-2);

% time step size:
dt = 1/nt;

% presolve for a short amount of time to get an improved initial condition:
% tmax_pre = tmax/200;
% dt_pre = dt/200;
% X = SwimmingSpermNonlinear(X_int, tmax_pre, dt_pre, Q, Ht, calS, ...
%                            c_perp, gamma, m0, wavk, hax, q); 
% X_pre = X(:,end);
X_pre = X_int;
                       
% simulate cell using sperm-x, using improved initial condition:
[X,Y,t,f,phi,FH,MH] = SwimmingSpermNonlinear(X_pre, tmax, dt, Q, Ht, calS, ...
                                             c_perp, gamma, m0, wavk, hax, q); 

% extract coordinates and values at each time point:
nt = size(X,2);
for nn = 1:nt
    spermx(nn).X = X(:,nn);
    spermx(nn).Y = Y(:,nn);
    [spermx(nn).x1,spermx(nn).x2,spermx(nn).x3] = extractComponents(X(:,nn));
    [spermx(nn).y1,spermx(nn).y2,spermx(nn).y3] = extractComponents(Y(:,nn));
    spermx(nn).t = t(nn);
    spermx(nn).f = f(:,nn);
    [spermx(nn).f1,spermx(nn).f2,spermx(nn).f3] = extractComponents(f(:,nn));
    spermx(nn).phi = phi(:,nn);
    [spermx(nn).phi1,spermx(nn).phi2,spermx(nn).phi3] = extractComponents(phi(:,nn));
    spermx(nn).FH = FH(:,nn);
    spermx(nn).MH = MH(:,nn);
end
spermx = orderfields(spermx);

% clear variables:
clearvars X Y t f phi FH MH

%% eif:

printBanner('EIF')

% switch to eif directory:
SwitchDir('endpiece')

% set some additional parameters:
ell = 0; th0 = []; Ed=1; Ep=1; sd=1; L=1;
showProg = 1; type = 'constant';

% obtain m0:calS relation
calM = m0;

% convert position initial condition into X0+theta initial condition:
XM_int = VectorToMatrix(X_pre);
theta = atan(diff(XM_int(2,:))./diff(XM_int(1,:)));
X0 = XM_int(:,1);
Y_pre = [X0(1); X0(2); theta(1); theta(:)];

% simulate cell using eif and return Z0, a new initial condition
[Z,Z0,output,model] = runSingleSim(calS, wavk, calM, type, [], ell, ...
                                   th0, Q, Hq, Ht, Ed, Ep, sd, L, tmax, showProg,...
                                   X0(1:2), hax, nt, Y_pre);
                               
% Convert Z0 initial condition into new initial condition for Sperm-X:
%[~,flag,~] = GetProblemData(Z0, model);
%X_pre = [flag.x(:); flag.y(:); flag.z(:)];
                            
% extrate coordinate data from angle formulation solutions:
t = linspace(0,tmax,nt);
fprintf('Extracting data...')
for nn = 1:nt
    
    % extract data:
    [head,flag,~] = GetProblemData(Z(:,nn),model);
    
    % flagellar nodes:
    x1 = flag.x(:);
    x2 = flag.y(:);
    x3 = flag.z(:);
    eif(nn).X = [x1; x2; x3];
    eif(nn).x1 = x1;
    eif(nn).x2 = x2;
    eif(nn).x3 = x3;
    
    % head nodes:
    y1 = head.xTrac(:);
    y2 = head.yTrac(:);
    y3 = head.zTrac(:);
    eif(nn).Y = [y1; y2; y3];
    eif(nn).y1 = y1;
    eif(nn).y2 = y2;
    eif(nn).y3 = y3;
    
    % forces:
    eif(nn).t = t(nn);
    [eif(nn).f,eif(nn).phi,eif(nn).FH,eif(nn).MH] = CalculateEIFForces(eif(nn).t,Z(:,nn),model);
    [eif(nn).f1,eif(nn).f2,eif(nn).f3] = extractComponents(eif(nn).f);
    [eif(nn).phi1,eif(nn).phi2,eif(nn).phi3] = extractComponents(eif(nn).phi);
    
end
eif = orderfields(eif);
fprintf(' complete.\n')

% clear variables:
clearvars X Y t f phi FH MH

% switch back to sperm-x directory:
SwitchDir('sperm-x')

%% plots:

% configuration plots:
fig1 = figure;
subplot(1,2,1); box on; hold on;
f1 = plot(eif(1).x1,eif(1).x2,'r','LineWidth',1.4);
h1 = scatter(eif(1).y1,eif(1).y2,'r.');
f2 = plot(spermx(1).x1,spermx(1).x2,'b--','LineWidth',1.4);
h2 = scatter(spermx(1).y1,spermx(1).y2,'b.');
title('initial config.')
axis equal; axis tight
xlabel('x'); ylabel('y');
legend([f1,f2],{'eif','sperm-x'})

subplot(1,2,2); box on; hold on;
f1 = plot(eif(end).x1,eif(end).x2,'r','LineWidth',1.4);
h1 = scatter(eif(end).y1,eif(end).y2,'r.');
f2 = plot(spermx(end).x1,spermx(end).x2,'b--','LineWidth',1.4);
h2 = scatter(spermx(end).y1,spermx(end).y2,'b.');
title('final config.')
axis equal; axis tight
xlabel('x'); ylabel('y');
legend([f1,f2],{'eif','sperm-x'})

set(fig1,'Position',[434 375 1695 654]);

% force on head plots: extract time-evolving data to plot:
eif_FH1 = ExtractStruct(eif,'FH',1);
eif_FH2 = ExtractStruct(eif,'FH',2);
spermx_FH1 = ExtractStruct(spermx,'FH',1);
spermx_FH2 = ExtractStruct(spermx,'FH',2);
eif_t = ExtractStruct(eif,'t',1);
spermx_t = ExtractStruct(spermx,'t',1);

% force on head plots: plot:
fig2 = figure;
subplot(1,2,1); box on; hold on;
f1 = plot(eif_t, eif_FH1,'r','LineWidth',1.4);
f2 = plot(spermx_t, spermx_FH1,'b','LineWidth',1.4);
xlabel('t'); ylabel('FH_x');
legend([f1,f2],{'eif','sperm-x'},'Location','southeast')

subplot(1,2,2); box on; hold on;
f1 = plot(eif_t, eif_FH2,'r','LineWidth',1.4);
f2 = plot(spermx_t, spermx_FH2,'b','LineWidth',1.4);
xlabel('t'); ylabel('FH_y')
legend([f1,f2],{'eif','sperm-x'},'Location','southeast')

set(fig2,'Position',[891 552 780 300])

% force on distal flagellum plots: extract time-evolving data to plot:
eif_fd1 = ExtractStruct(eif,'f1',Q);
eif_fd2 = ExtractStruct(eif,'f2',Q);
spermx_fd1 = ExtractStruct(spermx,'f1',Q);
spermx_fd2 = ExtractStruct(spermx,'f2',Q);

% force on head plots: plot:
fig3 = figure;
subplot(1,2,1); box on; hold on;
f1 = plot(eif_t, eif_fd1,'r','LineWidth',1.4);
f2 = plot(spermx_t, spermx_fd1,'b','LineWidth',1.4);
xlabel('t'); ylabel('f_x(s=1)');
legend([f1,f2],{'eif','sperm-x'},'Location','southeast')

subplot(1,2,2); box on; hold on;
f1 = plot(eif_t, eif_fd2,'r','LineWidth',1.4);
f2 = plot(spermx_t, spermx_fd2,'b','LineWidth',1.4);
xlabel('t'); ylabel('f_y(s=1)')
legend([f1,f2],{'eif','sperm-x'},'Location','southeast')

set(fig3,'Position',[891 552 780 300])

% save presolved initial conditions for use in other tests:
%save_str = sprintf('presolved_int_parabolas_S=%g.mat',calS);
%save(save_str,'X_pre','Y_pre');
%disp('Pre-solved initial conditions saved!')


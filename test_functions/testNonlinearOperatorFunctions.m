% TEST Nonlinear Operator functions.

% test function to check if functions implementing terms in the linearised
% ehd and tension equations are correct.

clear all; close all; clc
printLinebreak;

% construct a curve:
N = 150;
X = ParabolicCurve(0.5,N);
s = linspace(0,1,N);
disp(['N is ', num2str(N)]);

% build finite difference matrices:
D = finiteDifferences(s,1:4);

% build finite difference operators:
D_s = blkdiag(D{1},D{1},D{1}); D_ss = blkdiag(D{2},D{2},D{2});
D_sss = blkdiag(D{3},D{3},D{3}); D_ssss = blkdiag(D{4},D{4},D{4});

% derivatives to the curve are:
X_s = D_s*X; X_s = Normalise(X_s);
X_ss = D_ss*X;
X_sss = D_sss*X;
X_ssss = D_ssss*X;

% generate dummy tension data, of similar shape to relaxing filament test:
T = linspace(-0.5,0.5,N).^2-0.25; T = T(:);

% generate varying stiffness values:
[E,E_s,E_ss] = varyingStiffness(s, 39/50);
Erep = repmat(E,3,1);
Esrep = repmat(E_s,3,1);
Essrep = repmat(E_ss,3,1);

% other variables:
gamma = 1.4;

% derivative of tension:
d_s = D_s(1:N,1:N);
T_s = d_s*T;
printLinebreak

%% check terms in tension equation:

disp('Terms in the tension equation:')

% Xsss.Xsss term:
exp_val_T1 = DotProduct(X_sss,X_sss);
opr_val_T1 = DotDerivativeOperator(X_sss,D_sss)*X;
T1 = RelativeError(exp_val_T1, opr_val_T1);
disp(['Xsss.Xsss error is ',num2str(T1)])

% Xss.Xssss term:
exp_val_T2 = DotProduct(X_ss, X_ssss);
opr_val_T2 = DotDerivativeOperator(X_ss,D_ssss)*X;
T2 = RelativeError(exp_val_T2, opr_val_T2);
disp(['Xss.Xssss error is ',num2str(T2)])

% 6Es(Xsss.Xsss) term:
exp_val_T3 = 6*E_s.*DotProduct(X_sss,X_sss);
opr_val_T3 = 6*DotDerivativeOperator(X_sss, Esrep.*D_sss)*X;
T3 = RelativeError(exp_val_T3, opr_val_T3);
disp(['6Es(Xsss.Xsss) error is ',num2str(T3)])

% Ess(2gamma+1)(Xss.Xss) term:
exp_val_T4 = (1+2*gamma)*E_ss.*DotProduct(X_ss,X_ss);
opr_val_T4 = (1+2*gamma)*DotDerivativeOperator(X_ss, Essrep.*D_ss)*X;
T4 = RelativeError(exp_val_T4, opr_val_T4);
disp(['Ess(2gamma+1)(Xss.Xss) error is ',num2str(T4)])

%% check terms in ehd equation:

printLinebreak
disp('Terms in the EHD equation:')

% extract components of derivatives:
[x1_s,x2_s,x3_s] = extractComponents(X_s);
[x1_ss,x2_ss,x3_ss] = extractComponents(X_ss);

% TXss term:
exp_val_E1 = repmat(T,3,1).*X_ss;
opr_val_E1 = [diag(x1_ss);diag(x2_ss);diag(x3_ss)]*T;
E1 = RelativeError(exp_val_E1, opr_val_E1);
disp(['TXss error is ',num2str(E1)])

% TsXs term:
exp_val_E2 = repmat(T_s,3,1).*X_s;
opr_val_E2 = [diag(x1_s)*d_s;diag(x2_s)*d_s;diag(x3_s)*d_s]*T;
E2 = RelativeError(exp_val_E2, opr_val_E2);
disp(['TsXs error is ',num2str(E2)])

% E(gamma-1)(Xs.Xssss)Xs term:
exp_val_E3 = (gamma-1)*Erep.*repmat(DotProduct(X_s,X_ssss),3,1).*X_s;
opr_val_E3 = (gamma-1)*buildTensorOp(X_s)*(Erep.*D_ssss)*X;
E3 = RelativeError(exp_val_E3, opr_val_E3);
disp(['E(gamma-1)(Xs.Xssss)Xs error is ',num2str(E3)])

% 2Es(gamma-1)(Xss.Xss)Xs term:
exp_val_E4 = 2*(gamma-1)*Esrep.*repmat(DotProduct(X_ss,X_ss),3,1).*X_s;
opr_val_E4 = 2*(gamma-1)*buildTensorOp(X_s,X_ss)*(Esrep.*D_ss)*X;
E4 = RelativeError(exp_val_E4, opr_val_E4);
disp(['2Es(gamma-1)(Xss.Xss)Xs error is ',num2str(E4)])

printLinebreak

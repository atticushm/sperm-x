% TEST_SingleSegmentIntegral

% Computes the integral of stokeslet derivatives across a single segment,
% computing the error against a highly-resolved numerical method for
% decreasing segment length ds.

clear all;

% Choose regularisation parameter:
epsilon = 1e-2;

% Define choices of Q:
num_Q = 50;
Q_vals = unique(floor(logspace(1,3,num_Q)));
num_Q = length(Q_vals);

% Define angle of segment:
th = pi/6;

% Tangent to segment:
T = [cos(th); sin(th); 0];

% Rotation matrix aligning segment with x-axis:
R = RotationMatrix(th);

% Source point is the segment midpoint:
X_centre = [0.5; 1; 0];

for n = 1:num_Q
    
    textwaitbar(n,num_Q,'Testing for increasing Q')
    
    Q = Q_vals(n);
    ds = 1/Q;    
    
    % End points of segment ds in length:
    X_a = X_centre - ds/2 * T;
    X_b = X_centre + ds/2 * T;
    
    % Field point is a point along the line:
    X_field = X_a;
%     X_field = X_centre;
%     X_field = MatrixToVector([X_a(:), X_b(:)]);
    num_field = length(X_field)/3;
    
    T_field = repmat(T,num_field,1);
    
    %% Analytical method:
    
    ana{n} = RegStokesletAnalyticDerivativeIntegrals(X_field,X_centre,T_field,R,ds/2,epsilon);
   
    %% Highly-resolved numerical method:
    % Using Gauss-Legendre quadrature and 2nd-order central finite
    % differences for differentiation.
    
    % Get nodes for integration between X_a and X_b:
    num_int = 1e2;
    [s_int,weights] = lgwt(num_int,-ds/2,ds/2);
    [s_int,idx] = sort(s_int);  weights = weights(idx);
    
    x_int = interp1([-ds/2;ds/2], [X_a(1);X_b(1)], s_int, 'linear', 'extrap');
    y_int = interp1([-ds/2;ds/2], [X_a(2);X_b(2)], s_int, 'linear', 'extrap');
    z_int = interp1([-ds/2;ds/2], [X_a(3);X_b(3)], s_int, 'linear', 'extrap');
    X_int = [x_int(:); y_int(:); z_int(:)]; 
    XM_int = VectorToMatrix(X_int);
    
    % Perturbed field node for numerical differentiation:
    h = 1e-6;
    Xsph = X_field + h*T_field;
    Xsmh = X_field - h*T_field;
    
    num{n} = 0;
    for i = 1:num_int
        num{n} = num{n} + weights(i)/2/h * (RegStokeslets(Xsph,XM_int(:,i),epsilon) - RegStokeslets(Xsmh,XM_int(:,i),epsilon));
    end
    
    %% Numerical method w/ higher-order finite difference method:
    % Using Gauss-Legendre quadrature as above but with a 4th order central
    % finite difference approximation (same h) for differentiation.
    
    % Additional field nodes.
    Xsp2h = X_field + 2*h*T_field;
    Xsm2h = X_field - 2*h*T_field;
    
    num_2{n} = 0;
    for i = 1:num_int
        num_2{n} = num_2{n} + weights(i)/h * (1/12*RegStokeslets(Xsm2h,XM_int(:,i),epsilon) ...
                                             -2/3 *RegStokeslets(Xsmh ,XM_int(:,i),epsilon) ...
                                             +2/3 *RegStokeslets(Xsph ,XM_int(:,i),epsilon) ...
                                             -1/12*RegStokeslets(Xsp2h,XM_int(:,i),epsilon));
    end
    
    %% Semi-analytical method:
    % Numerical integral of analytically computed derivatives.
    
    semi{n} = 0;
    for i = 1:num_int
        semi{n} = semi{n} + weights(i) * RegStokesletAnalyticDerivatives(X_field, XM_int(:,i), epsilon, T_field);
    end
    
end

%% Compute errors:

for n = 1:num_Q
    error(n,1) = norm(abs(ana{n} - num{n}));
    error(n,2) = norm(abs(semi{n} - num{n}));
    error(n,3) = norm(abs(semi{n} - ana{n}));
    error(n,4) = norm(abs(ana{n} - num_2{n}));
end
disp(error);
    
%% Plots:

close all;
f = figure;
l1 = loglog(1./Q_vals, error(:,1), 'o-','LineWidth',1.3); hold on; box on; grid on;
l2 = loglog(1./Q_vals, error(:,2), 'x--','LineWidth',1.3);
l3 = loglog(1./Q_vals, error(:,3), '.-','LineWidth',1.3);
l4 = loglog(1./Q_vals, error(:,4), '.-','LineWidth',1.3);
xlabel('$\Delta s$','Interpreter','latex','FontSize',14);
f.CurrentAxes.XDir = 'Reverse';
f.CurrentAxes.TickLabelInterpreter = 'latex';
f.CurrentAxes.FontSize = 12;
ylabel('2-Norm of matrix difference','Interpreter','latex','FontSize',14);
legend([l1,l2,l3,l4], 'Fully-analytic vs fully-numerical (2nd order F.D.)',...
                   'Semi-analytic vs fully-numerical (2nd order F.D.)',...
                   'Semi-analytic vs fully-analytic',...
                   'Fully-analytic vs fully-numerical (4th order F.D.)',...
                   'FontSize',12, 'Interpreter','latex','location','southoutside','Numcolumns',3);
f.Position = [528 845 1032 493];
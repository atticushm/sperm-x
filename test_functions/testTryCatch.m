% test function for try catch

clear all
i = 1;

exit = 'fail';
while strcmp(exit,'fail')

    try 
        % set something to fail whilst i below 10
        if i<10
            x = notafunc();
        else
            x = linspace(0,1,10);
        end
        exit = 'ok';
        fprintf('success!\n')
    catch
        fprintf('increasing i...\n')
        i = i+1;
        exit = 'fail';
    end

    % if i a certain value, exit
    if (i==9)
        fprintf('gotcha\n')
        exit = 'ok';
    end
    
end

fprintf('completion message\n')
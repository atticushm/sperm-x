% TEST Grand Resistance Matrices Sperm

% This test function is similar to
% TEST_GrandResistanceMatrixTransformations but the setup is more
% specific/reflective of the 'real world' use case of TransformR in the
% Sperm-X code.

%% Setup:

clear all; close all; 
clc
printLinebreak

% Regularisation parameter:
epsilon = 1e-2;

% Coefficient:
coeff = 1;

% Spheres centred at origin:
N_t_face = 10;
N_q_face = 20;
Sph_t = GenerateSphereDisc(N_t_face, 1);
Sph_q = GenerateSphereDisc(N_q_face, 1);
N_t = length(Sph_t)/3;
N_q = length(Sph_q)/3;

% Ellipses centred at origin:
hdims = [2; 1.6; 1]/200;
% hdims = ones(3,1); 
a = hdims(1);
E_t = Sph_t .* kron(hdims, ones(N_t,1));
E_q = Sph_q .* kron(hdims, ones(N_q,1));

%% Reference head:
% Compute the grand resistance matrix in the reference configuration, i.e.
% when the cell head is aligned with x,y,z axes and centred at the origin.

% Coordinates of reference cell head:
Y0_t = E_t;
Y0_q = E_q;

% Point about which to rotate reference head:
r0 = zeros(3,1);
% r0 = [a;0;0];

% Compute reference grand resistance matrix:
GRM_0 = ComputeR(Y0_t, Y0_q, r0, epsilon, coeff);

%% Attached head:
% Compute the grand resistance matrix for a cell head attached to a
% flagellum.

% Head-flagellum join:
X0 = [0; 0.2; 0];

% Head tangent:
th = pi/4;
tH = [cos(th); -sin(th); 0];

% Get head nodes - NB HeadNodes required GRM_0 but we will not use it in
% this test function:
Yr_t = HeadNodes(X0, E_t, hdims, tH);
Yr_q = HeadNodes(X0, E_q, hdims, tH);

% Point about which to rotate attached head:
[y1,y2,y3] = extractComponents(Yr_q);
Yr_c = [mean(y1); mean(y2); mean(y3)];
r_r = Yr_c;
% r_r = X0;

% Compute grand resistance matrix:
GRM_r = ComputeR(Yr_t, Yr_q, r_r, epsilon, coeff);

%% Tranform GRM_0 to obtain GRM_r:

% Obtain rotation matrix:
tH_0 = [1;0;0];
Reflection = @(u,n) u-2*n*(n'*u)/(n'*n);
S = Reflection(eye(3),tH_0+tH);
R = Reflection(S, tH);

% As the head is rotated about the same point on
% its surface in the body frame, dr (the distance between points of
% rotation) is zero.
% Thus, transform reference GRM:
dr = zeros(3,1);   
% dr = r_r-r_0
GRM_t = TransformGRM(GRM_0, R, dr);

%% Compute R using DJS code:
% via github @ https://github.com/djsmithbham/NearestStokeslets

% reference head:
x = Y0_t; 
X = Y0_q;
domain = 'i';
blockSize = 0.2;
[FTr1,MTr1,~,~]=SolveRigidResistance(x,X,r0,[1,0,0],[0,0,0],epsilon,domain,blockSize);
[FTr2,MTr2,~,~]=SolveRigidResistance(x,X,r0,[0,1,0],[0,0,0],epsilon,domain,blockSize);
[FTr3,MTr3,~,~]=SolveRigidResistance(x,X,r0,[0,0,1],[0,0,0],epsilon,domain,blockSize);
[FRo1,MRo1,~,~]=SolveRigidResistance(x,X,r0,[0,0,0],[1,0,0],epsilon,domain,blockSize);
[FRo2,MRo2,~,~]=SolveRigidResistance(x,X,r0,[0,0,0],[0,1,0],epsilon,domain,blockSize);
[FRo3,MRo3,~,~]=SolveRigidResistance(x,X,r0,[0,0,0],[0,0,1],epsilon,domain,blockSize);
GRM_djs_0 = [FTr1,FTr2,FTr3,FRo1,FRo2,FRo3; MTr1,MTr2,MTr3,MRo1,MRo2,MRo3];

% attached head:
x = Yr_t; 
X = Yr_q;
r0 = r_r;
domain = 'i';
blockSize = 0.2;
[FTr1,MTr1,~,~]=SolveRigidResistance(x,X,r0,[1,0,0],[0,0,0],epsilon,domain,blockSize);
[FTr2,MTr2,~,~]=SolveRigidResistance(x,X,r0,[0,1,0],[0,0,0],epsilon,domain,blockSize);
[FTr3,MTr3,~,~]=SolveRigidResistance(x,X,r0,[0,0,1],[0,0,0],epsilon,domain,blockSize);
[FRo1,MRo1,~,~]=SolveRigidResistance(x,X,r0,[0,0,0],[1,0,0],epsilon,domain,blockSize);
[FRo2,MRo2,~,~]=SolveRigidResistance(x,X,r0,[0,0,0],[0,1,0],epsilon,domain,blockSize);
[FRo3,MRo3,~,~]=SolveRigidResistance(x,X,r0,[0,0,0],[0,0,1],epsilon,domain,blockSize);
GRM_djs_r = [FTr1,FTr2,FTr3,FRo1,FRo2,FRo3; MTr1,MTr2,MTr3,MRo1,MRo2,MRo3];

% rotated reference GRM:
GRM_djs_t = TransformGRM(GRM_djs_0, R, dr);

%% View results:

disp('Reference GRM:')
disp(GRM_0)
disp('Reference DJS GRM:')
disp(GRM_djs_0)
disp('GRM of attached head:')
disp(GRM_r)
disp('DJS GRM of attached head:')
disp(GRM_djs_r)
disp('Transformed reference GRM:')
disp(GRM_t)
disp('Transformed reference DJS GRM:')
disp(GRM_djs_t)
% disp('Absolute error:')
% disp(abs(GRM_r-GRM_t))

printLinebreak

function testSphereNearWall()

% solving the resistance problem for a sphere near a boundary, to assess
% the correct blakelet implementation 

% boundary is the plane z=0

close all;
printLinebreak

% initialise sphere radius a with centre at the origin
H_trac = 10;
H_quad = 25;

a = 1;
xs = generateSphereDisc(H_trac, a);
Xs = generateSphereDisc(H_quad, a);

N_trac = length(xs)/3;
N_quad = length(Xs)/3;

% vary height of sphere surface above z=0
num_h  = 40;
h_vals = logspace(-1,1.3,num_h);

% % h vals from DJS Fortran code
% h_vals = [0.10118212352206468, 0.10177318528309698, 0.10265977792464542, ...
%           0.10398966688696820, 0.10598450033045226, 0.10897675049567845, ...
%           0.11346512574351753, 0.12019768861527624, 0.13029653292291421, ...
%           0.14544479938437127, 0.16816719907655697, 0.20225079861483541, ...
%           0.25337619792225308, 0.33006429688337935, 0.44509644532506898, ...
%           0.61764466798760354, 0.87646700198140537, 1.2647005029721079,  ...
%           1.8470507544581620,  2.7205761316872428,  4.0308641975308639,  ...
%           5.9962962962962951,  8.9444444444444429,  13.366666666666665,  ...
%           20.000000000000000 ];
% num_h = length(h_vals);

% regularisation parameter
epsilon = 0.01;

% choose code 
code = 'alhm';

% solve resistance problem using blakelets
fprintf('Solving resistance problems...\n')
for i = 1:num_h
    fprintf('  %2g / %g\n',i,num_h);
    
    % translate sphere
    Xc = [0;0;h_vals(i)+a];
    x  = xs + kron(Xc, ones(N_trac,1));
    X  = Xs + kron(Xc, ones(N_quad,1));
    
    % solve problem
    switch code
        case 'alhm'
            % compute grand resistance matrix,
            % normalised by 6/pi/a
            R{i} = -solveResistanceProblem(x, X, Xc, epsilon, 1, 'blakelets')/6/pi/a;
            
            % store values for plotting
            RFUx(i) = R{i}(1,1);
            RFUy(i) = R{i}(2,2);
            RFUz(i) = R{i}(3,3);
            RMOx(i) = R{i}(4,4);
            RMOy(i) = R{i}(5,5);
            RMOz(i) = R{i}(6,6);
            
        case 'nearest'
            % solve resistance problems
            [RF{1},RM{1},~,~] = solveResistanceProblemDJS(x,X,Xc,[1,0,0],[0,0,0],epsilon,'h',4);
            [RF{2},RM{2},~,~] = solveResistanceProblemDJS(x,X,Xc,[0,1,0],[0,0,0],epsilon,'h',4);
            [RF{3},RM{3},~,~] = solveResistanceProblemDJS(x,X,Xc,[0,0,1],[0,0,0],epsilon,'h',4);
            [RF{4},RM{4},~,~] = solveResistanceProblemDJS(x,X,Xc,[0,0,0],[1,0,0],epsilon,'h',4);
            [RF{5},RM{5},~,~] = solveResistanceProblemDJS(x,X,Xc,[0,0,0],[0,1,0],epsilon,'h',4);
            [RF{6},RM{6},~,~] = solveResistanceProblemDJS(x,X,Xc,[0,0,0],[0,0,1],epsilon,'h',4);
            
            % compile into grand resistance matrix
            R{i} = [[RF{:}]; [RM{:}]]/6/pi/a;

            % values for plotting
            RFUx(i) = R{i}(1,1);
            RFUy(i) = R{i}(2,2);
            RFUz(i) = R{i}(3,3);
            RMOx(i) = R{i}(4,4);
            RMOy(i) = R{i}(5,5);
            RMOz(i) = R{i}(6,6);            
    end
    
end

% plot diagonal components of RFU against h
nexttile; hold on; box on;
plot(h_vals, RFUx, '-', 'LineWidth', 1.2);
plot(h_vals, RFUy, '--', 'LineWidth', 1.2);
plot(h_vals, RFUz, '-', 'LineWidth', 1.2);

xlabel('seperation distance $h$', 'Interpreter', 'latex', 'FontSize', 14)
ylabel('resistance$/6\pi a$', 'Interpreter','latex', 'FontSize',14)
leg = legend('$R^{FU}_1$', '$R^{FU}_2$', '$R^{FU}_3$', 'Interpreter', 'latex', 'FontSize', 12);
set(leg, 'Box', 'off')

% plot diagonal components of RMO against h
nexttile; hold on; box on;
plot(h_vals, RMOx, '-', 'LineWidth', 1.2);
plot(h_vals, RMOy, '--', 'LineWidth', 1.2);
plot(h_vals, RMOz, '-', 'LineWidth', 1.2);

xlabel('seperation distance $h$', 'Interpreter', 'latex', 'FontSize', 14)
ylabel('resistance$/6\pi a$', 'Interpreter','latex', 'FontSize',14)
leg = legend('$R^{M\Omega}_1$', '$R^{M\Omega}_2$', '$R^{M\Omega}_3$', 'Interpreter', 'latex', 'FontSize', 12);
set(leg, 'Box', 'off')

% save figure
set(gcf, 'Position',[882 1351 625 450]);
str = sprintf('./figures/sphere_near_wall_%s.pdf',code);
save2pdf(str)
fprintf('Figure saved at %s\n', str)
printLinebreak

end % function
% TEST MATERIAL FRAME

% Test the MaterialFrame function for various curves and assess it's
% output.

clear all; 
printBanner('MATERIAL FRAME TESTING','#')

%% determine frame from prescribed position:

% note that this can cause an incompatible curve!

%{
% choose number of points (enough for accurate finite difference
% derivatives):
N = 60;
ds = 1/(N-1);

% generate a planar curve:
X = ParabolicCurve(0.5,N);
% X = ParabolicCurve(1e-10,N);

% arclength discretisations:
s = linspace(0,1,N);
s_mp = 0.5*(s(1:N-1)+s(2:N));

% compute (initial) curvatures:
[k1,k2,~,d01] = InitialCurvatures(X);

% prescribe twist via
%   k3 = -int_s^L -R(s')/E_T ds',
% where
%   R(s) = cos(kT*s);
% and analytically integrate.
ET = 1;                     % twist stiffness.
kT = 2*pi/N;                     % twist wave number.
k3 = -1/ET/kT*(sin(kT)-sin(kT*s));

% constant twist:
k3 = 0*pi*ones(N,1);

% determine material frame:
[d1,d2,d3] = MaterialFrame(X,k1,k2,k3,d01);
%}

%% determine curve and frame from prescribed curvatures:

% number of nodes:
M = 50;

% arclength values:
s = linspace(0,1,M);

% starting point of curve:
X0 = [0;0;0];

% starting directors:
d10 = [1;0;0];
d20 = [0;1;0];
d30 = [0;0;1];

% initial curvatures:
% type = 'semicirc-no-twist';
type = 'spiral';
% type = 'corkscrew';
% type = 'sinusoidal-planar';
switch type
    case 'line'
        k1 = zeros(M,1);
        k2 = zeros(M,1);
        k3 = zeros(M,1);
    case 'corkscrew'
        k1 = zeros(M,1);
        k2 = zeros(M,1);
        k3 = 2*pi*ones(M,1);
    case 'spiral'
        k1 = cos(4*pi*s);
        k2 = sin(4*pi*s);
        k3 = zeros(M,1);
    case 'semicirc-no-twist'
        k1 = zeros(M,1);
        k2 = -pi*ones(M,1);
        k3 = zeros(M,1);
    case 'semicirc-twist'   % this produces non-orthonormal frame!
        k1 = zeros(M,1);
        k2 = -pi*ones(M,1);
        k3 = 2*pi*ones(M,1);
    case 'sinusoidal-planar'
        k1 = zeros(M,1);
        k2 = sin(4*pi*s);
        k3 = zeros(M,1);
end

% build director frame:
[d1,d2,d3] = buildDirectorFrame(k1,k2,k3,d10,d20,d30);

% determine curve from directors:
X = integralAlongCurve(X0,d3);

%% test reconstruction code:

% dummy data for velocities:
X_t = zeros(3*M,1);
k3_t = zeros(M,1);

% reconstruct frame:
[d1r,d2r,d3r,k1r,k2r] = reconstructFrame(X,k3,d10);

%% check perpendicularlity:

% frames from buildDirectorFrame():
d1d2 = DotProduct(d1,d2);
d1d3 = DotProduct(d1,d3);
d2d3 = DotProduct(d2,d3);

% frames from reconstructFrame():
d1rd2r = DotProduct(d1r,d2r);
d1rd3r = DotProduct(d1r,d3r);
d2rd3r = DotProduct(d2r,d3r);

% max values:
max_d1d2 = max(d1d2);
max_d1d3 = max(d1d3);
max_d2d3 = max(d2d3);

max_d1rd2r = max(d1rd2r);
max_d1rd3r = max(d1rd3r);
max_d2rd3r = max(d2rd3r);

% print to command line:
fprintf('max(d1.d2)=%g\nmax(d1.d3)=%g\nmax(d2.d3)=%g\n',...
        max_d1d2,max_d1d3,max_d2d3);
fprintf('max(d1r.d2r)=%g\nmax(d1r.d3r)=%g\nmax(d2r.d3r)=%g\n',...
        max_d1rd2r,max_d1rd3r,max_d2rd3r);
    
%% convert to Frenet-Serret frame:

% get F-S frame:
[T,N,B,k,tau] = DirectorToFrenet(d1,d2,d3,k1,k2,k3);
% [T,N,B,k,tau] = DirectorToFrenet(d1r,d2r,d3r,k1r,k2r,k3);

% extract components:
[Tx,Ty,Tz] = extractComponents(T);
[Nx,Ny,Nz] = extractComponents(N);
[Bx,By,Bz] = extractComponents(B);

%% plots:

close all;
cp_col = [121 121 121]./255;

% quiver director frame:
% nexttile(1,[1 3]); 
subplot(5,3,[1 13]);
hold on; grid on;
[X1,X2,X3] = extractComponents(X);
plot3(X1,X2,X3,'ko','LineWidth',2,'MarkerFaceColor','k');
cp = plot(X1,X2,'k','LineWidth',2); cp.Color = cp_col;
scale = 0.75;
[d11,d12,d13] = extractComponents(d1);
[d21,d22,d23] = extractComponents(d2);
[d31,d32,d33] = extractComponents(d3);
% [d11,d12,d13] = extractComponents(d1r);
% [d21,d22,d23] = extractComponents(d2r);
% [d31,d32,d33] = extractComponents(d3r);
d1p = quiver3(X1,X2,X3,d11,d12,d13,scale,'r');
d2p = quiver3(X1,X2,X3,d21,d22,d23,scale,'b');
d3p = quiver3(X1,X2,X3,d31,d32,d33,scale,'g');
legend([d1p,d2p,d3p],{'$\bf{d}_1(s)$','$\bf{d}_2(s)$','$\bf{d}_3(s)$'},...
        'Interpreter','latex','Location','northeast');
xlabel('$x$','interpreter','latex','FontSize',14); 
ylabel('$y$','Interpreter','latex','FontSize',14); 
zlabel('$z$','Interpreter','latex','FontSize',14);
% if strcmp(type,'sinusoidal-planar')
%     view([0,90,0])
% else
    view(3);
% end
axis equal;
title('Director frame')

% quiver frenet-serret frame:
% nexttile(2,[1 3]);
subplot(5,3,[2 14]);
hold on; grid on;
cp1 = plot(X1,X2,'k','LineWidth',2); 
cp = plot(X1,X2,'k','LineWidth',2); cp.Color = cp_col;
plot3(X1,X2,X3,'ko','LineWidth',2,'MarkerFaceColor','k')
lcol = lines(3);
Tp = quiver3(X1,X2,X3,Tx,Ty,Tz,scale); Tp.Color = lcol(1,:);
Np = quiver3(X1,X2,X3,Nx,Ny,Nz,scale); Np.Color = lcol(2,:);
Bp = quiver3(X1,X2,X3,Bx,By,Bz,scale); Bp.Color = lcol(3,:);
legend([Tp,Np,Bp],{'$\bf{T}(s)$','$\bf{N}$(s)','$\bf{B}(s)$'},...
           'Interpreter','latex','Location','northeast');
xlabel('$x$','interpreter','latex','FontSize',14); 
ylabel('$y$','Interpreter','latex','FontSize',14); 
zlabel('$z$','Interpreter','latex','FontSize',14);
% if strcmp(type,'sinusoidal-planar')
%     view([0,90,0])
% else
    view(3);
% end
axis equal;
title('Frenet-Serret frame')

% plot curvatures:
% nexttile;
subplot(5,3,3);
plot(s, k1); hold on
% plot(s,k1r);
ylabel('$\kappa_1(s)$','FontSize',14,'Interpreter','latex')
xlabel('$s$','FontSize',14,'Interpreter','latex')
% nexttile
subplot(5,3,6);
plot(s, k2); hold on
% plot(s,k2r);
ylabel('$\kappa_2(s)$','FontSize',14,'Interpreter','latex')
xlabel('$s$','FontSize',14,'Interpreter','latex')
% nexttile
subplot(5,3,9);
plot(s, k3);
ylabel('$\kappa_3(s)$','FontSize',14,'Interpreter','latex')
xlabel('$s$','FontSize',14,'Interpreter','latex')

% plot frenet-serret parameters:
subplot(5,3,12)
plot(s,k);
ylabel('$k(s)$','FontSize',14,'Interpreter','latex')
xlabel('$s$','FontSize',14,'Interpreter','latex')
if strcmp(type,'spiral')
    axis([0 1 0 2])
end
subplot(5,3,15)
plot(s,tau)
ylabel('$\tau(s)$','FontSize',14,'Interpreter','latex')
xlabel('$s$','FontSize',14,'Interpreter','latex')

% figure positioning:
set(gcf,'Position',[846 414 911 689]);

% save figure:
save_str = sprintf(['./figures/MaterialFrameComparison_',type,'.pdf']);
save2pdf(save_str)
fprintf(['Figure saved to ',save_str,'!\n'])
printLinebreak('~')
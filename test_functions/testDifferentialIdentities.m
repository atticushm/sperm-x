% TEST FUNCTION

% Verifying how dot product differential identities can be applied to terms
% within the tension equation.

clear all; close all;
clear CurveDerivatives finiteDifferences

% Define a curve - identities only accurate for large Q:
Q = 1e2;
Qp1 = Q+1;      
X = ParabolicCurve(0.5,Qp1); XM = VectorToMatrix(X);
s = linspace(0,1,Qp1);

% Curve derivatives:
[X_s,X_ss,X_sss,X_ssss,X_sssss] = CurveDerivatives(X(:),s(:));
XM_s = VectorToMatrix(X_s);
XM_ss = VectorToMatrix(X_ss);
XM_sss = VectorToMatrix(X_sss);
XM_ssss = VectorToMatrix(X_ssss);
XM_sssss = VectorToMatrix(X_sssss);

% Define a symmtetric matrix to proxy for R (no R removes disc. error from
% R):
A = [1 2 3; 2 4 5; 3 5 6];

% Pick out one example for scrutiny.
id = Q/4;
X = XM(:,id);
X_s = XM_s(:,id);
X_ss = XM_ss(:,id);
X_sss = XM_sss(:,id);
X_ssss = XM_ssss(:,id);
X_sssss = XM_sssss(:,id);

%% Check identity is correctly implement when there are no R terms:

% Term by term (very inefficient):
ID1_test = dot(X_s,X_sss) + dot(X_ss,X_ss);
ID2_test = dot(X_s,X_ssss) + 3*dot(X_ss,X_sss);
ID3_test = dot(X_s,X_sssss) + 4*dot(X_ss,X_ssss) + 3*dot(X_sss,X_sss);

%% Verify properties of symmetric matrices:
% A_ij X_j = A_ji X_j equiv. A_ij X_i

% repeated index on j, so sum over j and loop i:
A_ijX_j(1) = A(1,1)*X(1) + A(1,2)*X(2) + A(1,3)*X(3);
A_ijX_j(2) = A(2,1)*X(1) + A(2,2)*X(2) + A(2,3)*X(3);
A_ijX_j(3) = A(3,1)*X(1) + A(3,2)*X(2) + A(3,3)*X(3);

% notice repeated index is now on j, and so we sum over i and loop j:
A_ijX_i(1) = A(1,1)*X(1) + A(2,1)*X(2) + A(3,1)*X(3);
% rest of terms are obviously going to be the same

% inner/dot product symmetry:
a = [1;2;3]; b = [4;5;6];
lhs = dot(A*a,b);   rhs = dot(a,A*b); % <-- lhs == rhs.

% identity with dot product:
lhs = 0; rhs = 0;
for i = 1:3
    for j = 1:3
        lhs = lhs + A(i,j)*a(j)*b(i);
        rhs = rhs + A(i,j)*a(i)*b(i);
    end
end

%% Test identities:

ID1_idx = 0;
for i = 1:3  
    for j = 1:3
        ID1_idx = ID1_idx + A(i,j)*X_sss(j)*X_s(i) + A(i,j)*X_ss(j)*X_ss(j);
    end
end

%% Manual expansion - to check:

ID1_exp = (A(1,1)*X_sss(1)+A(1,2)*X_sss(2)+A(1,3)*X_sss(3))*X_s(1) + ...
           A(1,1)*(X_ss(1)^2) + A(1,2)*(X_ss(1)^2) + A(1,3)*X_ss(1)^2 + ...
          (A(2,1)*X_sss(1)+A(2,2)*X_sss(2)+A(2,3)*X_sss(3))*X_s(2) + ...
           A(2,1)*(X_ss(2)^2) + A(2,2)*(X_ss(2)^2) + A(2,3)*X_ss(2)^2 + ...
          (A(3,1)*X_sss(1)+A(3,2)*X_sss(2)+A(3,3)*X_sss(3))*X_s(3) + ...
           A(3,1)*(X_ss(3)^2) + A(3,2)*(X_ss(3)^2) + A(3,3)*X_ss(3)^2;
         
%% Vectorisation:

% % semi-vectorized:
% for i = 1:3
%     ID{1}(i,3) = R(i,:)*X_sss(:)*X_s(i) + R(i,:)*(X_ss.*X_ss);
% end
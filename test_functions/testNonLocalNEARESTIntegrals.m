% TEST FUNCTION

% NONLOCAL INTEGRAL USING NEAREST
% When computing V (the flagellar velocity contributions due to nonlocal
% hydrodynamic effects) we need to compute an integral over the |s-s[m]|>q
% rather than the usual 0 to L=1.

% In this test function we develop and test extensions to NEAREST to
% incorporate this alteration to the integral.

% We test the reliability of the altered NEAREST code by decomposing
% solving a resistance problem for a rigid, parabolic rod:
%       U = int_0^1 S(X,X(s)) f(s) ds
% with U=e_x and comparing the resulting values for f(s) with the f computed from the
% resistance problem with a decomposed integral:
%       U = int_0^{s-q} S(X,X(s')) f(s') ds' +
%           int_{s+q}^1 S(X,X(s')) f(s') ds'
% and checking convergence as q->0.

%% Setup

clearvars; close all; clc
disp('-----')

% Set discretisation parameters:
M_t = 50;       % # of traction nodes.
M_q = 4*M_t ;       % # of quadrature nodes.
eps = 0.01;     % regularisation parameter.

% Get X_t coordinates:
X_t = ParabolicInitialCondition(0.1, M_t);
% X_t = HelicalInitialCondition(5,2,M_t);
[X_t1, X_t2, X_t3] = extractComponents(X_t);

% Interpolate to get X_q coordinates:
ds_t = 1/(M_t-1);   ds_q = 1/(M_q-1);   % node spacings.
s_t = 0:ds_t:1;     s_q = 0:ds_q:1;     % discrete arclengths at each level.
X_q1 = interp1(s_t, X_t1, s_q, 'spline');
X_q2 = interp1(s_t, X_t2, s_q, 'spline');
X_q3 = interp1(s_t, X_t3, s_q, 'spline');
X_q = [X_q1(:); X_q2(:); X_q3(:)];

%{
%% Solve standard resistance problem (single integral)

% Presribe force at each node:
f = [ones(M_t,1); zeros(2*M_t,1)];

% Compute nearest-neighbour matrix:
NN = NearestNeighbourMatrix(X_q, X_t);

% Compute stokeslet matrix:
S = RegStokeslets(X_t, X_q, eps);

% Find velocities:
U_0 = (S*NN) * f;

%% `Split' integral test

% In this test we decompose the integral into two, with the union limits of
% integration forming the limits of the original integral.
% Values for U calculated by each choice of q here should always equal U_0.

% Pick q values:
q_vals_split = linspace(0,1,100);

disp('Beginning loop over 0 <= q <= 1...')
progressbarText(0)
for n = 1:length(q_vals_split)
%     disp(['(',num2str(p),'/',num2str(length(q_vals_split)),')'])
    
    q = q_vals_split(n);
    
    % Identify nodes over q distance from each collocation point.
    % These will contribute to the force at each collocation point:
    f_id_lt = NearestNodes(X_t, q, '<');
    f_id_gt = NearestNodes(X_t, q, '>=');
    
    % Compute Stokeslet matrix:
    S = RegStokeslets(X_t, X_q, eps);
    
    % Compute nearest-neighbour method:
    NN = NearestNeighbourMatrix(X_q, X_t);
    
    % Find velocities:
    U_split(:,n) = (f_id_lt.*(S*NN))*f + (f_id_gt.*(S*NN))*f;
    
    % Update progressbar:
    progressbarText(n/length(q_vals_split))

end % q loop

E_split = vecnorm( repmat(U_0,1,length(q_vals_split)) - U_split);

% Plot 1 - decomposed integral test:
figure; subplot(1,3,1); box on;
plot(q_vals_split, E_split);
xlabel('q');   
ylabel('||U-U_0||')
title({'complimentary integrals',''})

%% Convergence test

% In this test we integrate over |s-s[m]|>q, and check convergence of the
% resulting U as q -> 0, which should tend towards U_0.

% Pick q values:
q_vals_conv = linspace(0,1e-2,100);

disp('Beginning loop over reducing q...')
progressbarText(0)
for n = 1:length(q_vals_conv)
%     disp(['(',num2str(p),'/',num2str(length(q_vals_conv)),')'])
    
    q = q_vals_conv(n);
    
    % Identify nodes over q distance from each collocation point.
    % These will contribute to the force at each collocation point:
    f_id = NearestNodes(X_t, q, '>=');
    
    % Compute Stokeslet matrix:
    S = RegStokeslets(X_t, X_q, eps);
    
    % Compute nearest-neighbour method:
    NN = NearestNeighbourMatrix(X_q, X_t);
    
    % Find velocities:
    U_conv(:,n) = (f_id.*(S*NN))*f;
    
    % Update progressbar:
    progressbarText(n/length(q_vals_conv))

end % q loop

E_conv  = vecnorm( repmat(U_0,1,length(q_vals_conv)) - U_conv);

% Plot 2 - convergence of U as q -> 0:
subplot(1,3,2); box on;
plot(q_vals_conv, E_conv);
xlabel('q');   
ylabel('||U-U_0||')

title({['M_t=',num2str(M_t),', M_q=',num2str(M_q)],'convergence'})
%}

%% Local-Nonlocal split

% In this test the integral is split into a local and nonlocal term. The
% velocity due to the local forces is computed via resistive force theory,
% and the nonlocal contribution is computed using the method of regularised
% stokeslets.

% The sum of these results is compared to the velocities computed from a
% fully nonlocal calculation of the fluid velocities;

% As q descreases, the values should converge.

% Prescribe f:
f = [2*ones(M_t,1); ones(M_t,1); -ones(M_t,1)];

% Parameter choices:
S = 1;
S4 = S^4;
iS4 = 1/S4;

% Get arclength: 
arc = GetArclengthNodes(X_t(:));

% Fully nonlocal solve (using NEAREST):
stokeslets = RegStokeslets(X_t,X_q,0.01);
nu = NearestNeighbourMatrix(X_q,X_t);
weights = sum(nu); weights = (1/M_t)*weights(:);
dSf = weights.*f(:);
v_full = - iS4 * (stokeslets * nu) * dSf;

% Choose range for q:
% q_vals = linspace(0,0.1,200);
q_vals = [5e-4,5e-2,0.1];

% Get basis vectors:
disp('Computing finite difference matrices...')
[D_s,D_ss,~,~] = BuildFiniteDifferenceMatrices(arc,'arbitrary');
disp('Complete!')

global Diff
Diff.s = D_s; Diff.ss = D_ss;

units = blkdiag(D_s,D_s,D_s)*X_t(:);
unitn = CalculateNormals(X_t);
ms = VectorToMatrix(units);
mn = VectorToMatrix(unitn);
for m = 1:size(ms,2)
    mb(:,m) = cross(ms(:,m),mn(:,m));
end
unitb = MatrixToVector(mb);

mf = VectorToMatrix(f);

disp('Computing velocities for different q... ')
progressbarText(0)
for n = 1:length(q_vals)
    
    % Set RFT terms:
    q = q_vals(n);
    
    % q should be chosen intermediate in order between b and L.
    % L is order 1, so choose b to make choice of q make sense.
    q_order = floor(log10(q));
    b_order = 2*q_order;

    b(n) = 1e-6;
    
    % RFT coefficients:
    xi_para(n) = (8*pi) / (-2 + 4*log(2*q/b(n)) );
    xi_perp(n) = (8*pi) / ( 1 + 2*log(2*q/b(n)) );
    gamma(n) = xi_perp(n) / xi_para(n);
    
    % RFT operator:
    I = eye(3*M_t);
    ss = repmat(eye(M_t),3,3).*kron(units, units');
    nn = repmat(eye(M_t),3,3).*kron(unitn, unitn');
    bb = repmat(eye(M_t),3,3).*kron(unitb, unitb');
%     local_op = -(1/xi_perp(n))*(nn + bb + gamma(n)*ss);
    local_op = -(1/xi_perp(n))*(I + (gamma(n)-1)*ss); 
    v_local(:,n) = iS4 * local_op*f(:);   % local velocity contrubution.
    

    % Nonlocal velocity:
%     sigma = NearestNodes(X_t, X_t,q, '>');
%     nonlocal_op = -sigma.*(kernel * nu);
    sigma = NearestNodes(X_q, X_t, q, '>');
    nonlocal_op = -((stokeslets.*sigma)*nu);
    v_nonlocal(:,n) = iS4* nonlocal_op * dSf;     % nonlocal velocity contribution.
    
    % Combine velocities:
    v_split(:,n) = v_local(:,n) + v_nonlocal(:,n);
%     v_split(:,n) = iS4 * (local_op + nonlocal_op) * f(:);
    
    % Update progressbar:
    progressbarText(n/length(q_vals))
    
end

E_rft   = vecnorm( repmat(v_full,1,length(q_vals)) - v_split); 

% Plot 3
figure; box on;
plot(q_vals, E_rft);
xlabel('q')
ylabel('||U-U_0||')
title('hydrodynamics equation')

% Plot 4 invesigating weirdness at end nodes.
figure;
[x_0,y_0,z_0] = extractComponents(v_full);
[x_a,y_a,z_a] = extractComponents(v_split(:,1));
[x_b,y_b,z_b] = extractComponents(v_split(:,2));
[x_c,y_c,z_c] = extractComponents(v_split(:,3));

[x_1,y_1,z_1] = extractComponents(v_local(:,1));
[x_2,y_2,z_2] = extractComponents(v_local(:,2));
[x_3,y_3,z_3] = extractComponents(v_local(:,3));

[x_l,y_l,z_l] = extractComponents(v_nonlocal(:,1));
[x_m,y_m,z_m] = extractComponents(v_nonlocal(:,2));
[x_n,y_n,z_n] = extractComponents(v_nonlocal(:,3));

subplot(3,3,1); hold on
plot(arc, x_a)
plot(arc, x_b)
plot(arc, x_c)
plot(arc, x_0, 'k:')
xlabel('s'); ylabel('U_x+V_x')
title('local+nonlocal')
subplot(3,3,2); hold on
plot(arc, y_a)
plot(arc, y_b)
plot(arc, y_c)
plot(arc, y_0, 'k:')
xlabel('s'); ylabel('U_y+V_y')
subplot(3,3,3); hold on
plot(arc, z_a)
plot(arc, z_b)
plot(arc, z_c)
plot(arc, z_0, 'k:')
xlabel('s'); ylabel('U_z+V_z')
legend('q=5e-4','q=0.05','q=0.1','q=0','Location','northeast')

subplot(3,3,4); hold on
plot(arc, x_1)
plot(arc, x_2)
plot(arc, x_3)
plot(arc, x_0, 'k:')
xlabel('s'); ylabel('U_x')
title('local')
subplot(3,3,5); hold on
plot(arc, y_1)
plot(arc, y_2)
plot(arc, y_3)
plot(arc, y_0, 'k:')
xlabel('s'); ylabel('U_y')
subplot(3,3,6); hold on
plot(arc, z_1)
plot(arc, z_2)
plot(arc, z_3)
plot(arc, z_0, 'k:')
xlabel('s'); ylabel('U_z')
legend('q=5e-4','q=0.05','q=0.1','q=0','Location','northeast')

subplot(3,3,7); hold on
plot(arc, x_l)
plot(arc, x_m)
plot(arc, x_n)
plot(arc, x_0, 'k:')
xlabel('s'); ylabel('V_x')
title('nonlocal')
subplot(3,3,8); hold on
plot(arc, y_l)
plot(arc, y_m)
plot(arc, y_n)
plot(arc, y_0, 'k:')
xlabel('s'); ylabel('V_y')
subplot(3,3,9); hold on
plot(arc, z_l)
plot(arc, z_m)
plot(arc, z_n)
plot(arc, z_0, 'k:')
xlabel('s'); ylabel('V_z')
legend('q=5e-4','q=0.05','q=0.1','q=0','Location','northeast')

disp('-----')

% END OF SCRIPT

% test function

% test operator formulation encoded (X_s \wedge X_ss) \cdot X_sss

% generate dummy shape data
NF = 201;
X  = parabolicCurve(0.5, NF);
s  = linspace(0,1,NF);

% finite difference matrices
D  = finiteDifferences(s, 1:3);
D1 = kron(eye(3), D{1});
D2 = kron(eye(3), D{2});
D3 = kron(eye(3), D{3});

% derivatives
X_s = D1 * X;
X_ss = D2 * X;
X_sss = D3 * X;

% objective
obj = dotProduct(crossProdOp(X_s)*X_ss, X_sss);

% operator form
cp = crossProdOp(X_s)*X_ss;
[x1,x2,x3] = extractComponents(cp);
op = [x1(:).*D{3}, x2(:).*D{3}, x3(:).*D{3}];
opf = op * X;

% compare
err = rmse(obj, opf);

% interestingly this result suggests that the cross product term in the
% tension equation can be removed, as both obj and opf are exactly zero.
function [traj,smoothed] = loadCellTrajectory(filepath)

% computes cell head trajectory data for simulation results saved at
% filepath.

% load simulation data
fprintf('Loading data... ')
load(filepath,'varargin')
fprintf(' complete!\n')

% assign
outs    = varargin{1}; 
options = varargin{2};
params  = varargin{3};

clear varargin;

% compute trajectory
for i = 1:length(outs)
    X       = outs{i}.X;
    X0(:,i) = proxp(X);
end
traj = X0;

% compute `smoothed' time averaged trajectory
int = params.NumSave/params.NumBeats;
smoothed = [traj(1,:); traj(2,:); movmean(traj(3,:), int)];

end % function
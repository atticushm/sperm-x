function frames = framesFromOutputs(outs, params, options)

% outs is solution cell as outputted from runSPX2
% int is a scalar indicating the interval to skip between draws

% disable docking figures
dockFiguresByDefault('off')

% hide figure whilst drawing video frames
fig = figure('visible','off','units','pixels'); 
% fig = figure;
box on; 

% set zoom
if options.Backstep
    zoom = false;
else
    zoom = true;
end

% number of frames
Nf = length(outs);

% preallocate frames structure
frames = cell(Nf,1);

% select ~15 equispaced nodes at which to draw material frame
quiv_num = 15;
quiv_int = floor(params.NF/quiv_num);
qid      = 1:quiv_int:params.NF;
qscale   = 0.4;

% time values 
% tp_full = 0:params.dt:params.tmax;

% extract wall nodes
if options.Backstep
    [xw1,xw2,xw3] = extractComponents(params.xw);
else
    xw1 = []; xw2 = []; xw3 = [];
end

% generate difference matrices
s = linspace(0,1,params.NF);
D = finiteDifferences(s,1);
D_s = kron(eye(3),D{1});

% draw frames:
disp('Generating frames:')
for i = 1:Nf
    
    % display progress
    fprintf('  %5g / %5g\n', i, Nf);
    
    % extract coordinates
    [x1,x2,x3]    = extractComponents(outs(i).X);
    [d11,d12,d13] = extractComponents(outs(i).d1);
    [d21,d22,d23] = extractComponents(outs(i).d2);
    
    d3 = D_s * outs(i).X;
    [d31,d32,d33] = extractComponents(d3);
    if isField(outs,'y') 
        [y1,y2,y3] = extractComponents(outs(i).y);
    else
        y1 = []; y2 = []; y3 = [];
    end
    
    % centre axes on midpoint of cell:
    mid_x = mean(x1);   dx = 0.7;           
    mid_y = mean(x2);   dy = 0.7;
    mid_z = mean(x3);   dz = 0.7;

    minaxx = gather(mid_x-dx);
    maxaxx = gather(mid_x+dx);
    minaxy = gather(mid_y-dy);
    maxaxy = gather(mid_y+dy);
    minaxz = gather(mid_z-dz);
    maxaxz = gather(mid_z+dz);
    
    % scatter nodes:
    wgt = 2;
    
    subplot(2,2,1); hold on; cla                % 3D plot.
    plot3(x1,x2,x3,'k-','LineWidth',wgt,'MarkerFaceColor','k');
    if ~isempty(y1)
        scatter3(y1,y2,y3,'k.')
    end
    if ~isempty(xw1)
        scatter3(xw1,xw2,xw3,'b.')
    end
    if options.DrawFrame
        quiver3(x1(qid),x2(qid),x3(qid),d11(qid),d12(qid),d13(qid),qscale,'Color','r');
        quiver3(x1(qid),x2(qid),x3(qid),d21(qid),d22(qid),d23(qid),qscale,'Color','g');
        quiver3(x1(qid),x2(qid),x3(qid),d31(qid),d32(qid),d33(qid),qscale,'Color','b');
    end
    axis equal; view(3); grid on; box on; 
    
    subplot(2,2,3); hold on; cla;               % yz plane.
    plot3(x1,x2,x3,'k-','LineWidth',wgt,'MarkerFaceColor','k');
    if ~isempty(y1)
        scatter3(y1,y2,y3,'k.')
    end
    if ~isempty(xw1)
        scatter3(xw1,xw2,xw3,'b.')
    end
    if options.DrawFrame
        quiver3(x1(qid),x2(qid),x3(qid),d11(qid),d12(qid),d13(qid),qscale,'Color','r');
        quiver3(x1(qid),x2(qid),x3(qid),d21(qid),d22(qid),d23(qid),qscale,'Color','g');
        quiver3(x1(qid),x2(qid),x3(qid),d31(qid),d32(qid),d33(qid),qscale,'Color','b');
    end
    axis equal; grid on; box on; view([90,0]); 
    
    subplot(2,2,2); hold on; cla                % xy plane.
    plot3(x1,x2,x3,'k-','LineWidth',wgt,'MarkerFaceColor','k');
    if ~isempty(y1)
        scatter3(y1,y2,y3,'k.')
    end
    if ~isempty(xw1)
        scatter3(xw1,xw2,xw3,'b.')
    end
    if options.DrawFrame
        quiver3(x1(qid),x2(qid),x3(qid),d11(qid),d12(qid),d13(qid),qscale,'Color','r');
        quiver3(x1(qid),x2(qid),x3(qid),d21(qid),d22(qid),d23(qid),qscale,'Color','g');
        quiver3(x1(qid),x2(qid),x3(qid),d31(qid),d32(qid),d33(qid),qscale,'Color','b');
    end
    axis equal; grid on; box on; view(2); 
    
    subplot(2,2,4); hold on; cla                % xz plane.
    plot3(x1,x2,x3,'k-','LineWidth',wgt,'MarkerFaceColor','k');
    if ~isempty(y1)
        scatter3(y1,y2,y3,'k.')
    end
    if ~isempty(xw1)
        scatter3(xw1,xw2,xw3,'b.')
    end
    if options.DrawFrame
        d1p=quiver3(x1(qid),x2(qid),x3(qid),d11(qid),d12(qid),d13(qid),qscale,'Color','r');
        d2p=quiver3(x1(qid),x2(qid),x3(qid),d21(qid),d22(qid),d23(qid),qscale,'Color','g');
        d3p=quiver3(x1(qid),x2(qid),x3(qid),d31(qid),d32(qid),d33(qid),qscale,'Color','b');
    end
    axis equal; grid on; box on; view([0,-1,0]);
    
    if options.DrawFrame
        legend([d1p,d2p,d3p],{'$\bf{d}_1$','$\bf{d}_2$','$\bf{d}_3$'},...
                'FontSize',12,'Interpreter','latex','Location','southeast','EdgeColor','none')
    end

    % labels and axes:
    ax = findobj(fig,'Type','Axes');
    for j = 1:length(ax)
        xlabel(ax(j),{'\(x\)'},'Interpreter','latex','FontSize',14)
        ylabel(ax(j),{'\(y\)'},'Interpreter','latex','FontSize',14)
        zlabel(ax(j),{'\(z\)'},'Interpreter','latex','FontSize',14)
        if zoom==true
            axis(ax(j),[minaxx maxaxx minaxy maxaxy minaxz maxaxz]);
        else
            % set(ax(j),'YLim',[-0.01,1]);
        end
    end
    
    % subplot title:
    title_str = sprintf('S=%g, M_1=%g, M_2=%g, M_3=%g, \\Gamma_s=%g, \\Gamma_d=%g \n t=%5.3g', ...
        params.calS, params.calM1, params.calM2, params.calM3, params.Gam_s, params.Gam_d, outs(i).t);
    sgtitle(title_str)
    
    % set figure size:
    if zoom == true
        set(gcf,'position',[464 506 693 728])
    else
        set(gcf,'position',[464 302 1191 932])
    end
    
    % save frame:
    fr = getframe(gcf);
    frames{i} = fr;
    
end 
hold off;

end % function
function vid = saveMovie(frames,fps,filename)

% Saves a video of the movie given by frames, with filename given as input.
% Framerate of video is given by input fps.
% Frames object should be struct, as if given by GetFrames().

% File name and save path:
if ~isempty(filename)
    path = [pwd,'/videos/'];
%     if ~exist(path,'dir')
%         mkdir videos
%     end
%     filename = [path,filename,'~~',datestr(now,'mm-dd-yy_HH-MM')];
else
    [name,path] = uiputfile('Save video location');
    filetype = ['.',extractAfter(name,'.')];
    name = erase(name, filetype);
    filename = [path,name,'~~',datestr(now,'mm-dd-yy_HH-MM')];
end

% Video settings:
vid = VideoWriter(filename,'MPEG-4');
vid.FrameRate = fps;
vid.Quality = 100;

% Video export:
open(vid);
writeVideo(vid,cell2mat(frames));
close(vid);
disp(['Video saved in ',path])

end

function VideoFromFile(filepath)

% generates and saves a video of the simulation data stored at filepath.

% load from loadpath
fprintf('Loading data... ')
load(filepath,'var')
fprintf(' complete!\n')
data = var{1}; clear var

% extract filename
filename = data(1).params.filename;

% set options for video
fps      = 15;           % framerate

% reorgansie for plotting code
num_t      = length(data);
for i = 1:num_t
    outs_cell{i}    = data(i).outs;
end
options = data(1).options;
params  = data(1).params;

% % determine frames
% num_frames = fps*vlength;
% if num_t > num_frames
%     int = floor(num_t/num_frames);
% else
%     int = 1;
% end

% generate frames and save
frames = FramesFromOutputs(outs_cell, params, options);
vpath  = ['./videos/',filename];  

SaveMovie(frames,fps,vpath); 
printLinebreak

end % function
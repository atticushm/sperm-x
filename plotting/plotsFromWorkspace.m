function frames = PlotsFromWorkspace(varargin)

% Draws plots from a loaded workspace.

%% Load workspace:

if ~isempty(varargin)       % ** this option not fully implemented!
    file_path = varargin{1};
else
    [name,path] = uigetfile('.mat','Select .mat file');
end

% Load position data:
load([path,name], 'X_save', 't_save')
disp([name,' loaded!'])

%% Plots:

% Shape plot:
% num_frames = 200;
% int = floor(size(X,2)/num_frames);
% frames = GetFrames(X(:,1:int:end),tstep(:,1:int:end));
frames = GetFrames(X_save,t_save);
PlayMovie(frames,15);

% Filament length:
for i = 1:size(X_save,2)
    L(i) = CheckArclength(X_save(:,i));
end
figure; box on; grid on;
plot(t_save, L, 'LineWidth', 1.2);
xlabel('\(t\)','Interpreter','latex','FontSize',14)
ylabel('\(L\)','Interpreter','latex','FontSize',14)

end
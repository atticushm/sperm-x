function frames = GetFrames(X, y, d1, d2, d3, t)

% GETFRAMES generates a structure "frames", containing each frame of a video, 
% which can be saved using SaveMovie().
%
%  X is flagellum coordinates.
%  y is head coordinates.
%  d1,d2,d3 are the material frame directors.
%  t is the time points the above are supplied at.
%
% See also SAVEMOVIE. 

% disable docking figures:
DockFiguresByDefault('off')

% hide figure whilst drawing video frames:
fig = figure('visible','off','units','pixels'); 
% fig = figure;
box on; 

% number of time points:
Nt = length(t);

% create frames structure:
frames = cell(Nt,1);

% number of nodes:
NX = length(X(:,1))/3;

% select ~15 equispaced nodes at which to draw material frame:
quiv_num = 15;
quiv_int = floor(NX/quiv_num);
qid  = 1:quiv_int:NX;
qscale = 0.4;           % scale quiver arrows.

% draw frames:
for ii = 1:Nt
    if ii==1 || mod(ii,10) == 0 || ii==Nt
        printProgressBar('Drawing frames: ',ii,Nt,'nodelete',[]);
    end
    
    % extract coordinates:
    [x1,x2,x3]    = extractComponents(X(:,ii));
    [d11,d12,d13] = extractComponents(d1(:,ii));
    [d21,d22,d23] = extractComponents(d2(:,ii));
    [d31,d32,d33] = extractComponents(d3(:,ii));
    if max(max(y))>0
        [y1,y2,y3] = extractComponents(y(:,ii));
    else
        y1 = []; y2 = []; y3 = [];
    end
    
    % scatter nodes:
    subplot(2,2,1); hold on; cla                % 3D plot.
    plot3(x1,x2,x3,'k-','LineWidth',3,'MarkerFaceColor','k');
    if ~isempty(y1)
    scatter3(y1,y2,y3,'k.')
    end
    quiver3(x1(qid),x2(qid),x3(qid),d11(qid),d12(qid),d13(qid),qscale,'Color','r');
    quiver3(x1(qid),x2(qid),x3(qid),d21(qid),d22(qid),d23(qid),qscale,'Color','g');
    quiver3(x1(qid),x2(qid),x3(qid),d31(qid),d32(qid),d33(qid),qscale,'Color','b');
    axis equal; view(3); grid on; box on; axis square
    
    subplot(2,2,3); hold on; cla;               % yz plane.
    plot3(x1,x2,x3,'k-','LineWidth',3,'MarkerFaceColor','k');
    if ~isempty(y1)
    scatter3(y1,y2,y3,'k.')
    end
    quiver3(x1(qid),x2(qid),x3(qid),d11(qid),d12(qid),d13(qid),qscale,'Color','r');
    quiver3(x1(qid),x2(qid),x3(qid),d21(qid),d22(qid),d23(qid),qscale,'Color','g');
    quiver3(x1(qid),x2(qid),x3(qid),d31(qid),d32(qid),d33(qid),qscale,'Color','b');
    axis equal; grid on; box on; view([90,0]); axis square
    
    subplot(2,2,2); hold on; cla                % xy plane.
    plot3(x1,x2,x3,'k-','LineWidth',3,'MarkerFaceColor','k');
    if ~isempty(y1)
    scatter3(y1,y2,y3,'k.')
    end
    quiver3(x1(qid),x2(qid),x3(qid),d11(qid),d12(qid),d13(qid),qscale,'Color','r');
    quiver3(x1(qid),x2(qid),x3(qid),d21(qid),d22(qid),d23(qid),qscale,'Color','g');
    quiver3(x1(qid),x2(qid),x3(qid),d31(qid),d32(qid),d33(qid),qscale,'Color','b');
    axis equal; grid on; box on; view(2); axis square
    
    subplot(2,2,4); hold on; cla                % xz plane.
    plot3(x1,x2,x3,'k-','LineWidth',3,'MarkerFaceColor','k');
    if ~isempty(y1)
    scatter3(y1,y2,y3,'k.')
    end
    d1p=quiver3(x1(qid),x2(qid),x3(qid),d11(qid),d12(qid),d13(qid),qscale,'Color','r');
    d2p=quiver3(x1(qid),x2(qid),x3(qid),d21(qid),d22(qid),d23(qid),qscale,'Color','g');
    d3p=quiver3(x1(qid),x2(qid),x3(qid),d31(qid),d32(qid),d33(qid),qscale,'Color','b');
    axis equal; grid on; box on; view([0,-1,0]); axis square
    
    legend([d1p,d2p,d3p],{'$\bf{d}_1$','$\bf{d}_2$','$\bf{d}_3$'},...
            'FontSize',12,'Interpreter','latex','Location','southeast')
    
    % centre axes on midpoint of cell:
    mid_x = mean(x1);   dx = 0.7;           
    mid_y = mean(x2);   dy = 0.7;
    mid_z = mean(x3);   dz = 0.7;
    
    % labels and axes:
    ax = findobj(fig,'Type','Axes');
    for j = 1:length(ax)
        xlabel(ax(j),{'\(x\)'},'Interpreter','latex','FontSize',14)
        ylabel(ax(j),{'\(y\)'},'Interpreter','latex','FontSize',14)
        zlabel(ax(j),{'\(z\)'},'Interpreter','latex','FontSize',14)
        axis(ax(j),[mid_x-dx, mid_x+dx, mid_y-dy, mid_y+dy, mid_z-dz, mid_z+dz]);
    end
    
    % set figure size:
    set(gcf,'position',[464 506 693 728])
    
    % save frame:
    fr = getframe(gcf);
    frames{ii} = fr;
    
end 

end % function
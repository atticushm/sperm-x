function plotTopDownSurf(x, y, fx, str, cbar_lims)

% draws a top down pcolor plot given data
% useful for VAL, work, curvature plots etc

% plot
[xx,yy] = meshgrid(x,y);
surf(xx, yy, fx, 'EdgeColor','none');
view(2);
axis tight
shading flat

% colorbar
cbar = colorbar;
cbar.Label.String = str;
cbar.Label.Interpreter = 'latex';
cbar.Label.FontSize = 14;
cbar.TickLabelInterpreter = 'latex';
if ischar(cbar_lims) && strcmpi(cbar_lims,'auto')
    
else
    cbar.Ticks = [cbar_lims(1),cbar_lims(2)];
    caxis(cbar_lims);
end

% add enclosing box
fbox = gca;
box on;
grid off;
fbox.LineWidth = 0.5;
fbox.Layer = 'top';

end % function
function vid = playMovie(frames,fps)
% Plays movie from frame struct data.
% Input "frames" should be of the form "frames.data(:)" where the field
% "data" is a struct with fields "cdata" and "colormap".a

% Extract frame size:
res_x = size(frames{1}.cdata,1);
res_y = size(frames{1}.cdata,2);

% Create figure:
vid = figure('units','pixels','position',[0,0,res_y,res_x]);

% Play movie:
movie(vid,cell2mat(frames),1,fps)

end %function
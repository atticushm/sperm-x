function frames = getFramesSimple(X,Y)

fig = figure('visible','off','units','pixels'); 
box on; 

% Nt = length(t);
Nt = size(X,2);

frames = cell(Nt,1);

for ii = 1:Nt
    printProgressBar('Drawing frames: ',ii,Nt,'delete',[]);
    
    % Extract coordinates:
    [x1,x2,x3] = extractComponents(X(:,ii));
    if ~isempty(Y)
        [y1,y2,y3] = extractComponents(Y(:,ii));
    else
        y1 = []; y2 = []; y3 = [];
    end
    
    % Scatter nodes:
    subplot(1,2,1); hold on; cla
    scatter3([x1;y1],[x2;y2],[x3;y3],'.');
    axis equal; view(2); grid on; box on;
    
    subplot(1,2,2); hold on; cla
    scatter3([x1;y1],[x2;y2],[x3;y3],'.');
    axis equal; grid on; box on; view([0,-1,0])
    
    % Centre axes on midpoint of cell:
    mid_x = mean(x1);   dx = 0.8;           
    mid_y = mean(x2);   dy = 0.5;
    mid_z = mean(x3);   dz = 0.15;
    
    % Labels and axes:
    ax = findobj(fig,'Type','Axes');
    for j = 1:length(ax)
        xlabel(ax(j),{'\(x\)'},'Interpreter','latex','FontSize',14)
        ylabel(ax(j),{'\(y\)'},'Interpreter','latex','FontSize',14)
        zlabel(ax(j),{'\(z\)'},'Interpreter','latex','FontSize',14)
        axis(ax(j),[mid_x-dx, mid_x+dx, mid_y-dy, mid_y+dy, mid_z-dz, mid_z+dz]);
    end
    
    % Set size:
    set(gcf,'position',[0,0,1280,720])
    
    % Save frame:
    fr = getframe(gcf);
    frames{ii} = fr;
    
end
function PlotCurve(X)

% Plot the 3D curve defined by node positions X.

% Grab axes;
fig = gcf;

% Extract data:
[x,y,z] = extractComponents(X);

% Plot:
plot(x,y,'.-','LineWidth',1.3);

% Labels and tweaks:
axis equal
xlabel('\(x\)','Interpreter','latex','FontSize',14)
ylabel('\(y\)','Interpreter','latex','FontSize',14)

end
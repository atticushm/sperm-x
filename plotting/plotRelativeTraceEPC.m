function plotRelativeTraceEPC(Z, model)

% plots trace of beat given solution data and model parameters
% note this function is specific to EIF/end piece method (requires angle
% formulation)

N = length(model.Y)/3;
num_tbeat = size(Z,2);

% setup colours
cols = parula(num_tbeat);

% draw traces
for i = 1:num_tbeat
    
    % get body frame tangent angles
    th_bf = Z(4:end,i)-Z(3,i);
    
    % get coordinates in head frame
    [x1,x2,~] = extractComponents(integrateTheta([0;0;0], th_bf));
    
    % plot flagellum
    box on; hold on; grid on;
    plot(x1, x2, 'r', 'LineWidth', 1.2, 'Color',cols(i,:));
    
    % on first call: draw head
    if (i==1)
        % head nodes in body frame
        Y_bf = model.Y - kron(model.X0, ones(N,1));
        [y1,y2,y3] = extractComponents(Y_bf);
        scatter3(y1,y2,y3,'k.')
    end
    
end

% axes
% axis equal; 
% axis([-0.15,1, -0.5,0.5])
% xlabel('$x$','FontSize',14,'Interpreter','latex')
% ylabel('$y$','FontSize',14,'Interpreter','latex')

end
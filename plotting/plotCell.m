function PlotCell(head, flag, varargin)

% Plots a sperm cell from head and flagellum data.

if ~isempty(varargin)
    switch varargin{1}
        case 'frames'
            figure('visible','off'); box on; hold on; grid on;
    end
else
    figure; box on; hold on; grid on;
end

% Plot head:
hull = SurfaceFromPoints(head.Y_q); % use quad. nodes for smooth head.
trisurf(hull.hull, hull.x, hull.y, hull.z); 
shading flat
[x,y,z] = extractComponents(head.Y_t);
t1 = scatter3(x,y,z,'b.');
clear x y z

% Plot flagellum:
[x,y,z] = extractComponents(flag.X_q);
plot3(x, y, z, 'r.-', 'LineWidth', 2)
clear x y z
[x,y,z] = extractComponents(flag.X_t);
t2 = scatter3(x,y,z,'bo','filled');

% Mark X_0:
scatter3(x(1), y(1), z(1), 'bo', 'filled')

% Axes:
axis equal
xlabel('x'); ylabel('y'); zlabel('z');
view(3)

% Legend:
legend([t1 t2],{'head trac. nodes','flag. trac. nodes'})

end % function
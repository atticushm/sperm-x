function plotDiscretisedSpheres()

% computes and plots (a) coarse Nystrom discr sphere, (b) fine Nystrom
% discr sphere, (c) NEAREST discr sphere

% parameters for discretisation
Ht = 3;
Hq = 6;

% (a) coarse Nystrom discretistation
[nyc1,nyc2,nyc3] = extractComponents(generateSphereDisc(Ht,1));

% (b) fine Nystrom discretisation
[nyf1,nyf2,nyf3] = extractComponents(generateSphereDisc(Hq,1));

%% plots

% latex and font size for tick labels
set(groot, 'defaultAxesTickLabelInterpreter','latex');
set(groot, 'defaultAxesFontSize',14);

% quadrature nodes in red x's
% traction nodes in blue .'s

f1 = figure;
tiledlayout('flow','TileSpacing','compact');

nexttile; 
scatter3(nyc1, nyc2, nyc3, 'o','filled', 'MarkerEdgeColor','r', 'MarkerFaceColor','r'); hold on
scatter3(nyc1, nyc2, nyc3, 'x','MarkerEdgeColor','b','LineWidth',1.2)
setXYZLabels
view(2); axis equal;
axis([-1.1 1.1, -1.1 1.1])
box on; grid off

nexttile; 
scatter3(nyf1, nyf2, nyf3, 'o','filled', 'MarkerEdgeColor','r', 'MarkerFaceColor','r'); hold on
scatter3(nyf1, nyf2, nyf3, 'x','MarkerEdgeColor','b','LineWidth',1.2)
setXYZLabels
view(2); axis equal;
axis([-1.1 1.1, -1.1 1.1])
box on; grid off

nexttile; 
scatter3(nyc1, nyc2, nyc3, 'o','filled', 'MarkerEdgeColor','r', 'MarkerFaceColor','r'); hold on
scatter3(nyf1, nyf2, nyf3, 'x','MarkerEdgeColor','b','LineWidth',1.2)
setXYZLabels
view(2); axis equal;
axis([-1.1 1.1, -1.1 1.1])
box on; grid off

% sizing
set(gcf, 'Position',[551 494 626 187])

% save
save_str = './figures/spherical discretisations.pdf';
save2pdf(save_str, f1, 300);
close all

end % function
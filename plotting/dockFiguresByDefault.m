function DockFiguresByDefault(yesorno)

% Sets figures to be docked or undocked by default.

switch yesorno
    case {'yes','on'}
        set(0,'DefaultFigureWindowStyle','docked')
        disp('Figures now docked.')
    case {'no','off'}
        set(0,'DefaultFigureWindowStyle','normal')
        disp('Figures now undocked.')
end

end
function frames = GetFramesTNB(X, Y, tH, nH, t)

DockFiguresByDefault('off')

fig = figure('visible','off','units','pixels'); 
box on; 

Nt = length(t);

frames = cell(Nt,1);

for ii = 1:Nt
    if ii==1 || mod(ii,10) == 0 || ii==Nt
        printProgressBar('Drawing frames: ',ii,Nt,'nodelete',[]);
    end
    
    % Extract coordinates:
    [x1,x2,x3] = extractComponents(X(:,ii));
    if max(max(Y))>0
        [y1,y2,y3] = extractComponents(Y(:,ii));
    else
        y1 = []; y2 = []; y3 = [];
    end
    
    % Scatter nodes:
    subplot(2,2,[1 3]); hold on; cla            % 3D plot.
    scatter3([x1;y1],[x2;y2],[x3;y3],'.');
    axis equal; view(3); grid on; box on;
    
    subplot(2,2,2); hold on; cla                % xy plane.
    scatter3([x1;y1],[x2;y2],[x3;y3],'.');
    axis equal; grid on; box on; view(2)
    
    subplot(2,2,4); hold on; cla                % xz plane.
    scatter3([x1;y1],[x2;y2],[x3;y3],'.');
    axis equal; grid on; box on; view([0,-1,0])
    
    % Quiver head frame:
    views = {[1 3],2,4};
    for kk = 1:length(views)
        id = views{kk};
        subplot(2,2,id)
        qscale = 0.1;
        bH = cross(tH(:,ii),nH(:,ii));
        quiver3(x1(1),x2(1),x3(1),tH(1,ii),tH(2,ii),tH(3,ii),qscale,'LineWidth',1.3);
        quiver3(x1(1),x2(1),x3(1),nH(1,ii),nH(2,ii),nH(3,ii),qscale,'LineWidth',1.3);
        quiver3(x1(1),x2(1),x3(1),bH(1),bH(2),bH(3),qscale,'LineWidth',1.3);
    end
    
    % Centre axes on midpoint of cell:
    mid_x = mean(x1);   dx = 0.8;           
    mid_y = mean(x2);   dy = 0.5;
    mid_z = mean(x3);   dz = 0.15;
    
    % Labels and axes:
    ax = findobj(fig,'Type','Axes');
    for j = 1:length(ax)
        xlabel(ax(j),{'\(x\)'},'Interpreter','latex','FontSize',14)
        ylabel(ax(j),{'\(y\)'},'Interpreter','latex','FontSize',14)
        zlabel(ax(j),{'\(z\)'},'Interpreter','latex','FontSize',14)
        axis(ax(j),[mid_x-dx, mid_x+dx, mid_y-dy, mid_y+dy, mid_z-dz, mid_z+dz]);
    end
    
    % Set size:
    set(gcf,'position',[0,0,1280,720])
    
    % Save frame:
    fr = getframe(gcf);
    frames{ii} = fr;
    
end
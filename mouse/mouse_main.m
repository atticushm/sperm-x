%% Setup
p1 = genpath('lib');
addpath(p1);

%%
nH = 100;
procFlag = 'cpu'; % or gpu

mousetypes = { ...
    'BALBc', ...
    'C57Bl6', ...
    'CBA', ...
    'CD1', ...
    'DBA', ...
    'FVB', ...
    'MF1YRIII', ...
    'Mmdomesticus', ...
    'Mmmusculus', ...
    'Mspretus' ...
    };

mousetype = mousetypes{2};

%%

nBeats = 15;
tRange = [0,nBeats] * 2*pi;

% Length of sperm in um
L0 = 100e-6;

% Reguarlisation parameter
epsilon = 0.25e-6/L0;

% Infinite domain = regularised stokeslets
domain='i';

% To prevent too large calculations
if strcmp(procFlag,'cpu')
    blockSize = 1;
elseif strcmp(procFlag,'gpu')
    blockSize = [4,0];
else
    error('PROCFLAG not recognised')
end


% no boundaries
boundary = [];

% Initialise sperm
k = 2*pi;
phi = 0;
args{1} = struct('phase',phi,'k',k);

nT = 100;
discr = [nT,nH,nT*4,nH*4];

bodySize = [NaN,NaN,NaN]; % Not used

bodyFrame{1} = RotationMatrix(0,3);

x0{1} = [0;0;0];

MouseFnc = sprintf('%sMouseSperm',mousetype);

if ~exist('mouseSwimmer.mat','file')
    swimmer = SwimmersSperm(1,str2func(MouseFnc), ...
        @WaveSpermDKAct,bodySize,discr,nBeats, ...
        x0,bodyFrame,args);
    
    save('../lib/data/mouseSwimmer.mat','swimmer');
    
else
    load('mouseSwimmer.mat','swimmer');
    swimmer{1}.fn = @MouseSperm;
    swimmer{1}.x0 = x0{1};
    swimmer{1}.b10 = bodyFrame{1}(:,1);
    swimmer{1}.b20 = bodyFrame{1}(:,2);
    swimmer{1}.model.ns = discr(1);
    swimmer{1}.model.nh = discr(2);
    swimmer{1}.model.NS = discr(3);
    swimmer{1}.model.Nh = discr(4);
    swimmer{1}.model.a1 = bodySize(1);
    swimmer{1}.model.a2 = bodySize(2);
    swimmer{1}.model.a3 = bodySize(3);
end

% Initialise sperm head
swimmer{1}.model = GenerateMouseHead(mousetype, ...
    swimmer{1}.model,epsilon,L0);


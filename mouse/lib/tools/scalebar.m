%% Get pixel scaling based on balbc mouse head

%% BALB/c mouse
balbc = loadsvg('../data/BALBc.svg',0.5,0);

% Only one line so extract first element of img
balbc = balbc{1};

% Flip vertically
balbc(:,2) = -balbc(:,2);

% Scale to pixel size
dy = max(balbc(:,2)) - min(balbc(:,2));
dx = max(balbc(:,1)) - min(balbc(:,1));

scaleX = 5.39e-6 / dx;
scaleY = 6.40e-6 / dy;

scale = 0.5*(scaleX+scaleY);

save('../data/scalebar.mat','scale');

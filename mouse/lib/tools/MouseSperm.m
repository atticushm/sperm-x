function [x,v,X]=MouseSperm(t,model)

% generates discretisation of a model sperm, griddedInterpolant flagellum,
%
% t - time
% model.ns - number of points along flagellum
% model.nh - head discretisation parameter. Total points are 6 nh^2
% model.a1 etc - head semi-axes
% model.F  - F{1} is x-interpolant, F{2} is y-interpolant

%% -------------------------------------------------------------------
% Check that head exists
if ~isfield(model,'xh')
    error('Head not created')
end

%% -------------------------------------------------------------------

% coarse grid - position and velocity
[xh1,xh2,xh3] = extractComponents(model.xh);

% head is stationary in body frame
vh1=0*xh1;
vh2=0*xh2;
vh3=0*xh3;

% flagellum
s=linspace(0,1,model.ns+1); % don't include point that lies on the head
% finite differences for velocity... can't be too precise as x,y are
% outputs of fsolve
dt=0.001;
tt=[t-dt/2  t  t+dt/2];

[sg,tg]=ndgrid(s,tt); % data goes in model.F
[xg,yg]=CalcxyFromPlanarInterp(sg,tg,model.F);

xt1=xg(2:end,2)   + model.f0(1);
xt2=yg(2:end,2)   + model.f0(2);
xt3=0*xg(2:end,2) + model.f0(3);

vt1=(xg(2:end,3)-xg(2:end,1))/dt;
vt2=(yg(2:end,3)-yg(2:end,1))/dt;
vt3=0*xg(2:end,2);

x1=[xh1;xt1];
x2=[xh2;xt2];
x3=[xh3;xt3];
x =[x1;x2;x3];

v1=[vh1;vt1];
v2=[vh2;vt2];
v3=[vh3;vt3];
v =[v1;v2;v3];

%-------------------------------------------------------------------------
% fine grid - position only

% Load head position
[Xh1,Xh2,Xh3] = extractComponents(model.Xh);

% flagellum
S=linspace(0,1,model.Ns+1);

[Sg,Tg]=ndgrid(S,tt(:,2));
[Xg,Yg]=CalcxyFromPlanarInterp(Sg,Tg,model.F);

Xt1 =   Xg(2:end) + model.f0(1);
Xt2 =   Yg(2:end) + model.f0(2);
Xt3 = 0*Xg(2:end) + model.f0(3);

X1=[Xh1;Xt1];
X2=[Xh2;Xt2];
X3=[Xh3;Xt3];
X =[X1;X2;X3];

end

function [xF,yF] = GetMouseFilled(Xoutline,Youtline,NPtsMIN)

%% Image limits
x0 = min(Xoutline) - 1e-6;
y0 = min(Youtline) - 1e-6;
x1 = max(Xoutline) + 1e-6;
y1 = max(Youtline) + 1e-6;

%% Initialise points
imX = 1;
dx = 1e-8 *2;
dy = 1e-8 *2;

%% While not enough points
while numel(imX) < NPtsMIN
    
    % Set resolution
    dx = dx/2;
    dy = dy/2;
    
    % Create blank image
    imX = round(x0/dx)*dx : dx : round(x1/dx)*dx;
    imY = round(y0/dy)*dy : dy : round(y1/dy)*dy;
    [imX,imY] = meshgrid(imX,imY);
    im = zeros(size(imX));
    
    % Fill in outline points
    xO = round((Xoutline - x0)/dx + 1);
    yO = round((Youtline - y0)/dy + 1);
    X0 = unique([xO,yO],'rows');
    for ii = 1 : size(X0,1)
        im(X0(ii,2),X0(ii,1)) = 1;
    end
    
    % Fill image
    im = imfill(im);
    
    % Remove empty points    
    imX(im == 0) = [];
    imY(im == 0) = [];    
end

%% Output
xF = imX(:);
yF = imY(:);

end

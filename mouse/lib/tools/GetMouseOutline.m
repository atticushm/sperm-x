function [X,Y] = GetMouseOutline(expansionFnc,rotationFnc)

th = linspace(0,2*pi,1e5+1)'; th = th(2:end);
x = cos(th);
y = sin(th);

% Calculate theta
spTh = mod(atan2(y,x),2*pi);

% Calculate r at spTh
r = fnval(expansionFnc,spTh);

% Calculate rotation dth at spTh
dth = fnval(rotationFnc,spTh);

% Scale radius in (x,y)
x = r(:).*x;
y = r(:).*y;

% Rotate points in (x,y)
X = cos(dth(:)).*x - sin(dth(:)).*y;
Y = sin(dth(:)).*x + cos(dth(:)).*y;

end

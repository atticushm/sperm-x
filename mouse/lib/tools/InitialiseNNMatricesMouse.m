function NNMatrices = InitialiseNNMatricesMouse(problemFlag,swimmer, ...
    boundary,blockSize,varargin)

if isempty(varargin)
    t = 0;
else
    t = varargin{1};
end


switch problemFlag
    case 'swimming'
        % Construct swimmer NN matrices
        nSw = length(swimmer);
        NNSwi = cell(nSw,1);
        for iSw = 1 : nSw
            
            % Get swimmer points
            [xForce,~,xQuad] = swimmer{iSw}.fn(t,swimmer{iSw}.model);
            
            % Get body frame
            b30 = cross(swimmer{iSw}.b10,swimmer{iSw}.b20);
            B = [swimmer{iSw}.b10(:), swimmer{iSw}.b20(:), b30(:)];
            
            % Rotate points
            xForce = ApplyRotationMatrix(B,xForce);
            xQuad = ApplyRotationMatrix(B,xQuad);
            
            % Translate points
            xForce = TranslatePoints(xForce,swimmer{iSw}.x0);
            xQuad = TranslatePoints(xQuad,swimmer{iSw}.x0);
            
            % Extract head and tail force points
            [xf1,xf2,xf3] = extractComponents(xForce);
            xh1 = xf1(1 : swimmer{iSw}.model.nh);
            xt1 = xf1(swimmer{iSw}.model.nh + 1 : end);
            xh2 = xf2(1 : swimmer{iSw}.model.nh);
            xt2 = xf2(swimmer{iSw}.model.nh + 1 : end);
            xh3 = xf3(1 : swimmer{iSw}.model.nh);
            xt3 = xf3(swimmer{iSw}.model.nh + 1 : end);
            
            xFH = [xh1;xh2;xh3];
            xFT = [xt1;xt2;xt3];
            
            % Extract head and tail quad points
            [xf1,xf2,xf3] = extractComponents(xQuad);
            Xh1 = xf1(1 : swimmer{iSw}.model.nh);
            Xt1 = xf1(swimmer{iSw}.model.nh + 1 : end);
            Xh2 = xf2(1 : swimmer{iSw}.model.nh);
            Xt2 = xf2(swimmer{iSw}.model.nh + 1 : end);
            Xh3 = xf3(1 : swimmer{iSw}.model.nh);
            Xt3 = xf3(swimmer{iSw}.model.nh + 1 : end);
            
            XQH = [Xh1;Xh2;Xh3];
            XQT = [Xt1;Xt2;Xt3];
            
            % Create nearestNeighbour matrix head
            NNH = NearestNeighbourMatrix(XQH(:),xFH(:),blockSize);
            NNT = NearestNeighbourMatrix(XQT(:),xFT(:),blockSize);
            
            NNSwi{iSw} = MergeNNMatrices(NNH,NNT);
        end
        
        % Merge Swimmer NN matrices
        NNSw = NNSwi{1};
        for iSw = 2 : nSw
            NNSw = MergeNNMatrices(NNSw, NNSwi{iSw});
        end
        
        % Create boundary NN Matrix
        NNBnd = [];
        if ~isempty(boundary)
            [xForce,xQuad]   = boundary.fn(boundary.model);
            
            NNBnd = NearestNeighbourMatrix(xQuad,xForce, ...
                blockSize);
        end
        
        % Create full NN Matrix
        NN = MergeNNMatrices(NNSw,NNBnd);
        
        % Output
        NNMatrices = {NN, NNSw};        

    otherwise
        error('Problem type not known')
end

end
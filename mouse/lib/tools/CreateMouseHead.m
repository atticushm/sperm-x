function CreateMouseHead(mName,showPlts,varargin)

%% Load data
% Scalebar
load('../data/scalebar.mat','scale');

% Mouse head
mouse = loadsvg(['../data/', mName,'.svg'],0.5,0);

% Only one line so extract first element of img
mouse = mouse{1};

% Flip vertically
mouse(:,2) = -mouse(:,2);

% Scale to pixels
mouse = mouse * scale;

if showPlts
    figure(1)
    clf
    subplot(3,3,1)
    plot(mouse(:,1),mouse(:,2),'linewidth',2)
    axis equal
    title('raw data')
end

%% Calculate 'first point'
if isempty(varargin)
    % Positive horizontal axis
    px = linspace(0.5, 1.5) * max(mouse(:,1));
    
    % Calculate distances
    dst = ((px - mouse(:,1)).^2 + mouse(:,2).^2).^0.5;
    [~,ind] = min(dst(:));
    [ind,~] = ind2sub(size(dst),ind);
    
    if showPlts
        hold on
        plot(px,zeros(length(px),1),'k','linewidth',2)
        plot(mouse(ind,1),mouse(ind,2),'rx')
        hold off
    end
    
else
    ind = varargin{1};
end

%% Smooth the curve
t = linspace(0,100,size(mouse,1));

% Change order for better fitting
mouse = [mouse(ind:end,:) ; mouse(1 : ind-1,:)];

% Smooth curve
bX = csaps(t,mouse(:,1),0.75,t);
bY = csaps(t,mouse(:,2),0.75,t);

mouse = [bX(:),bY(:)];

if showPlts
    subplot(3,3,2)
    plot(bX,bY,'r','linewidth',2)
    axis equal
    title('smoothed data')
    
    subplot(3,3,1)
    hold on
    plot(bX,bY,'r.')
end

%% Parameterise
% Add final point to curve
mouse = [mouse ; mouse(1,:)];

% Arclength of curve
s = mouse(2:end,:) - mouse(1:end-1,:);
s = s.^2;
s = sum(sqrt(s(:,1) + s(:,2)));

% Scale points so arclength is 2*pi
arcScale = 2*pi/s;
b = mouse .* arcScale;
s = b(2:end,:) - b(1:end-1,:);
s = s.^2;
s = (sqrt(s(:,1) + s(:,2)));
s = [0 ; cumsum(s)];

if showPlts
    subplot(3,3,4)
    plot(b(:,1),b(:,2))
    hold on
    plot(mean(b(:,1)),mean(b(:,2)),'x')
    hold off
    axis equal
    title('scaled data')
end

% Center b on [0,0]
b = b - [mean(b(:,1)),mean(b(:,2))];

% Rotate pi/2 degrees so aligned in the -ve x-direction
b = [-b(:,2),b(:,1)];

if showPlts
    subplot(3,3,5)
    plot(b(:,1),b(:,2))
    hold on
    plot(mean(b(:,1)),mean(b(:,2)),'x')
    plot(0,0,'o')
    hold off
    axis equal
    title('scaled and centered data')
end

%% Create rotation function
% Calculate angle required to set th0 = 0

% Spline points in theta
bx = csape(s,b(:,1),'periodic');
by = csape(s,b(:,2),'periodic');

% find candidate th0s
ph0 = 0 : pi/2 : 2*pi;
x0 = zeros(length(ph0),1);
y0 = zeros(length(ph0),1);
th0 = zeros(length(ph0),1);
for ii = 1 : length(ph0)
    th0(ii) = fzero(@(ph) fnval(by,ph),ph0(ii));
    x0(ii) = fnval(bx,th0(ii));
    y0(ii) = fnval(by,th0(ii));
end
th0(x0 < 0) = [];
y0(x0 < 0) = [];
[~,ii] = min(y0);
th0 = th0(ii);

s = linspace(0,2*pi,200);

% Check that th is increasing anticlockwise
y0 = fnval(by,th0 + 0.01);
if y0 < 0
    s = fliplr(s);
end
    
bxx = fnval(bx,mod(s+th0,2*pi));
byy = fnval(by,mod(s+th0,2*pi));

TH = csape(s,atan2(byy,bxx),'periodic');
th = fnval(TH,s);
dth = unwrap(th) - s;
rotationFnc = csape(s,dth,'periodic');

if showPlts
    subplot(6,3,16)
    plot(s,s + dth)
    title('angle function th(s)')
end

%% Create expansion function
% Scale points back to original size
bxx = bxx ./ arcScale;
byy = byy ./ arcScale;

% Balb/C mouse should be 54.643px wide by 64.

% Distance from origin
r = sqrt(bxx.^2 + byy.^2);

% Spline points in theta
s = linspace(0,2*pi,length(r))';
expansionFnc = csape(s,r,'periodic');

if showPlts
    subplot(6,3,13)
    plot(s,r)
    title('distance function r(s)')
end

%% Transform circle
th = linspace(0,2*pi,1e3);
dth = fnval(rotationFnc,th);
r = fnval(expansionFnc,th);

% Create circle
c = [cos(th(:)),sin(th(:))];

% Scale radius
c = r(:).*c;

% Rotate points
cx = cos(dth(:)).*c(:,1) - sin(dth(:)).*c(:,2);
cy = sin(dth(:)).*c(:,1) + cos(dth(:)).*c(:,2);

if showPlts
    subplot(3,3,8)
    plot(cx,cy)
    axis equal
    title('transformed circle')
end

%% Work out x0 for flagellum
fInd = 990;
testF0 = [cx(fInd),cy(fInd)]; 

if showPlts
    hold on; 
    h = plot(testF0(1),testF0(2),'rx');
else
    figure(1)
    clf
    plot(cx,cy)
    axis equal
    hold on
    h = plot(testF0(1),testF0(2),'rx');
end

keepF = input('Press 0 to continue, 1 to enter new flagellum point:');

while keepF ~= 0
    delete(h);
    fInd = 0;
    while fInd < 1 || fInd > 1000
        fInd = input('Please enter new i0 in [1,1000]');
    end
    
    testF0 = [cx(fInd),cy(fInd)];
    
    if showPlts
        hold on;
        h = plot(testF0(1),testF0(2),'rx');
    else
        figure(1)
        clf
        plot(cx,cy)
        axis equal
        hold on
        h = plot(testF0(1),testF0(2),'rx');
    end
    
    keepF = input('Press 0 to continue, 1 for new flagellum point:');
           
end

flagellumX0 = testF0;

if showPlts
    subplot(3,3,8)
    hold on
    plot(flagellumX0(1),flagellumX0(2),'rx','markersize',12);
    plot(flagellumX0(1)+[0,3e-6],flagellumX0(2) + [0,0],'k','linewidth',2);
    hold off   
else
    close(1)
end

%% Save output
sName = ['../data/mouse-', mName, '.mat'];
save(sName,'expansionFnc','rotationFnc','flagellumX0','fInd')

%% Construct and plot head
if showPlts
    
    % Get mouse outline
    [xx,yy] = GetMouseOutline(expansionFnc,rotationFnc);

    % Get high-resolution mouse head
    [xx,yy] = GetMouseFilled(xx,yy,model.Nh);
    
    % Reduce number of points to get desired number of force points
    minFnc = @(tol) (size(uniquetol([xx,yy],tol,'ByRows',true),1) ...
        - 1e3).^2;
    reqTol = fminbnd(minFnc,0,1);

    x = uniquetol([xx,yy],reqTol,'ByRows',true);
    [x1,x2] = extractComponents2D(x);
    
    subplot(1,3,3)
    plot(x1,x2,'.')
    
    xlabel('x')
    ylabel('y')    
    
end

end

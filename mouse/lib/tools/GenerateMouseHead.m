function model = GenerateMouseHead(mouseName,model,epsilon,L)

% Load chosen mouse
mName = sprintf('mouse-%s.mat',mouseName);
load(mName,'expansionFnc','rotationFnc','flagellumX0')

% Get mouse outline
[xx,yy] = GetMouseOutline(expansionFnc,rotationFnc);

% Get high-resolution mouse head
[xx,yy] = GetMouseFilled(xx,yy,model.Nh);

% Scale with length
xx = xx/L;
yy = yy/L;

%% Reduce number of points to get desired number of force points
minFnc = @(tol) (size(uniquetol([xx,yy],tol,'ByRows',true),1) ...
    - model.nh).^2;
reqTol = fminbnd(minFnc,0,1);

if reqTol < epsilon
    error('Required point separation < epsilon')
end

x = uniquetol([xx,yy],reqTol,'ByRows',true);
model.xh = [x(:) ; zeros(numel(x)/2,1)];

%% Reduce number of points to get desired number of quadrature points
minFnc = @(tol) (size(uniquetol([xx,yy],tol,'ByRows',true),1) ...
    - model.Nh).^2;
reqTol = fminbnd(minFnc,0,1);
Xh = uniquetol([xx,yy],reqTol,'ByRows',true);

model.Xh = [Xh(:) ; zeros(numel(Xh)/2,1)];

%% Set flagellum point
model.f0 = [flagellumX0(:) ; 0] / L;

end

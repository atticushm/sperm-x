function bsmRelaxingFilament()

% runs a high resolution relaxing filament simulation using BSM (bead and 
% spring model) code 

printLinebreak;
clear all;

% generate initial condition identical to sperm-x simulations
problem = 'RelaxingFilament';
options = setOptions(problem, ...
    'BodyType',     'none', ...
    'NonLocal',     true, ...
    'StiffFunc',    'constant', ...
    'ForcePlanar',  true, ...
    'PlaneOfBeat',  'xy' ...
);
params = setParameters(options, ...
    'ittol',        1e-5, ...
    'NF',           201, ...
    'dt',           1e-4, ...
    'calS',         1, ...
    'tmax',         0.06, ...
    'NumSave',      100 ...
);
int   = setInitialCondition(options, params, 'IntCond', 'semicirc');
Y_int = convertInitialCondition(int.X, 'eif'); 
Q     = length(Y_int)-2;

% determine tmax
tmax = params.tmax;
dt   = tmax/(params.NumSave);

% add EIF code to path
addpath(genpath('./convergence/eif/'))
spr_k = 5e6;    
model = CreateModelStruct(Q, [1,1], [0,0], 'relaxing', spr_k);

% solve relaxing problem
[sol,tps,crds] = FilamentsRelaxingFunctionBLM(model, Y_int, tmax, dt);

% save solutions to file
str = sprintf('./convergence/relaxing_filament_data/bsm_N=%g.mat',Q+1);
save(str, 'sol', 'tps', 'crds');
fprintf('Solutions saved to %s\n', str);

% reset paths
addFilesToPath;
printLinebreak;

end % function
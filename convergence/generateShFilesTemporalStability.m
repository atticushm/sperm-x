function generateShFilesTemporalStability(itot)

% generates multiple sh files for running helical beat parameter search
% simulations

printLinebreak

% make subfolder for .sh files
if ~exist('./convergence/sh/','dir')
    mkdir('./convergence/sh/')
end

% generate .sh filenames
for i = 1:itot
    fname{i} = sprintf('./convergence/sh/bbTempStab_%02g_of_%g.sh', ...
       i, itot);
end

% write to each file
for i = 1:itot
   
    % open file for writing
    fID = fopen(fname{i},'w+');
    
    % print lines to files
    fprintf(fID, '#!/bin/bash\n');
    fprintf(fID, '#SBATCH --ntasks 12\n');
    fprintf(fID, '#SBATCH --time 10-00:00:0\n');
    fprintf(fID, '#SBATCH --qos bbdefault\n');
    % fprintf(fID, '#SBATCH --time 0-00:10:0\n');
    % fprintf(fID, '#SBATCH --qos bbshort\n');
    fprintf(fID, '#SBATCH --mail-type NONE\n');
    fprintf(fID, '#SBATCH --mem 100G\n\n');
    
    fprintf(fID, 'set -e\n\n');
    fprintf(fID, 'module purge; module load bluebear\n');
    fprintf(fID, 'module load MATLAB/2020a\n\n');
    
    % cd to code on bluebear directory and run
    fprintf(fID, 'cd ~/02_SPERMX/git/sperm-x/\n');
    fprintf(fID, 'matlab -nodisplay -r "addFilesToPath;spxTemporalStability(%g,%g)"\n', i, itot);
    
    % close file
    fclose(fID);
end

fprintf('Shell files generated in ./convergence/sh/bbTempStab_...\n')
printLinebreak;

end % function
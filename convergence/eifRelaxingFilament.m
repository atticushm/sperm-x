function eifRelaxingFilament()

% runs a high resolution relaxing filament simulation using EIF code 

printLinebreak;
clear all;

% generate initial condition equal to that in spermXRelaxingConvergence()
problem = 'RelaxingFilament';
options = setOptions(problem, ...
    'BodyType',     'none', ...
    'NonLocal',     true, ...
    'StiffFunc',    'constant', ...
    'ForcePlanar',  true, ...
    'PlaneOfBeat',  'xy' ...
);
params = setParameters(options, ...
    'ittol',        1e-5, ...
    'NF',           81, ...
    'dt',           1e-4, ...
    'tmax',         0.06, ...
    'NumSave',      100 ...
);
int   = setInitialCondition(options, params, 'IntCond', 'semicirc');
Y_int = convertInitialCondition(int.X, 'eif'); 
Q     = length(Y_int)-2;

% determine tmax
tmax = params.tmax;
dt   = tmax/(params.NumSave);

% add EIF code to path
addpath(genpath('./convergence/eif/'))
model = CreateModelStruct(Q, [1,1], [0,0], 'relaxing');

% solve relaxing problem
[sol,tps,crds] = FilamentsRelaxingFunction(model, Y_int, tmax, dt);

% save solutions to file
str = sprintf('./convergence/relaxing_filament_data/eif_N=%g.mat',Q+1);
save(str, 'sol', 'tps', 'crds');
fprintf('Solutions saved to %s\n', str);

% reset paths
addFilesToPath;
printLinebreak;

end % function
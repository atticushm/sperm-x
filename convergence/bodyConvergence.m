function bodyConvergence()

% assesses the convergence of the total force and moment for a spherical
% body using regularised stokeslets in a nearest-neighbour method

printLinebreak

% length by which to scale
L = 1;
ax = [1;1;1]/L;

% sphere centroid
X0 = zeros(3,1);

% make directories
if ~exist('./convergence/body_data','dir')
    mkdir('convergence/body_data')
end

% workspace save string
str = sprintf('convergence/body_data/L=%g.mat',L);

%% analytical force and moment
% by stokes law

Fa = 6*pi*ax(1);
Ma = 8*pi*ax(1)^3;

%% numerical force and moment
% from solving resistance problem with regularised stokeslets

% vary traction and quadrature discretisations
Ht  = unique(floor(logspace(0,1.3,20)));
Ht  = Ht(5:end);
Hq1 = 1*Ht;
Hq2 = 2*Ht;
Hq3 = 3*Ht;
num_Ht = length(Ht);

% regularisation parameter
epsilon = 1e-2;

% solve resistance problems
for n = 1:num_Ht
    fprintf('%g/%g\n',n,num_Ht);

    % generate discretisations
    [X_trac, X_quad2, ~,~] = generateBodyNodes(Ht(n), Hq2(n), 'human', 'custom', ax);
    [~, X_quad3, ~,~]      = generateBodyNodes(Ht(n), Hq3(n), 'human', 'custom', ax);

    % numbers of nodes
    Nf(n)  = length(X_trac)/3;
    Nq1(n) = length(X_quad2)/3;
    Nq2(n) = length(X_quad3)/3;

    % solve problems
    tic;
    R1{n} = -solveResistanceProblem(X_trac, X_trac,  X0, epsilon, 1, 'stokeslets');
    wt_Ny(n) = toc;

    tic;
    R2{n} = -solveResistanceProblem(X_trac, X_quad2, X0, epsilon, 1, 'stokeslets');
    wt_NN1(n) = toc;

    tic;
    R3{n} = -solveResistanceProblem(X_trac, X_quad3, X0, epsilon, 1, 'stokeslets');
    wt_NN2(n) = toc;

    % extract total force and moment
    Fn(n,1) = R1{n}(1,1);   Fn(n,2) = R2{n}(1,1);   Fn(n,3) = R3{n}(1,1);
    Mn(n,1) = R1{n}(4,4);   Mn(n,2) = R2{n}(4,4);   Mn(n,3) = R3{n}(4,4);
end

% errors
for n = 1:num_Ht
    % relative error
    F_rel(n,1) = abs(Fn(n,1)-Fa)/abs(Fa);
    F_rel(n,2) = abs(Fn(n,2)-Fa)/abs(Fa);
    F_rel(n,3) = abs(Fn(n,3)-Fa)/abs(Fa);
    M_rel(n,1) = abs(Mn(n,1)-Ma)/abs(Ma);
    M_rel(n,2) = abs(Mn(n,2)-Ma)/abs(Ma);
    M_rel(n,3) = abs(Mn(n,3)-Ma)/abs(Ma);

    % rmse
    %F_rms(n,1) = rmse(Fn(n,1),Fa);
    %F_rms(n,2) = rmse(Fn(n,2),Fa);
    %M_rms(n,1) = rmse(Mn(n,1),Ma);
    %M_rms(n,2) = rmse(Mn(n,2),Ma);
end

% save and fin
save(str);
printLinebreak

end % function
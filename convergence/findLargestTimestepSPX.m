function findLargestTimestepSPX(twBool)

% given a SPX swimming cell simulation, find the largest time step that
% results in solution stability

% input twBool is a boolean TRUE or FALSE indicating whether to include
% mechanical twist terms or not

close all; 

% setup folders
if ~exist('./convergence/stability_data/','dir')
    mkdir('./convergence/stability_data/')
end

%% set options and parameters common to all tests

% options and parameters for SPX
problem = 'SwimmingCell';
options = setOptions(problem, ...
    'BodyType',     'sunanda', ...
    'MechTwist',    twBool, ...
    'NonLocal',     true, ...
    'StiffFunc',    'varying', ...
    'SilentMode',   true, ...
    'ForcePlanar',  false ...
);

% values for calM2
calM2_vals = unique(round(linspace(0.01, 0.025, 20),3));
num_calM2  = length(calM2_vals);

%% try successively smaller dt until no errors from SPX

for i = 1:num_calM2
    
    % select calM2
    calM2i = calM2_vals(i);
    
    % try and catch until success
    exit = 'fail';
    dt = 0.4;
    while strcmpi(exit,'fail')
        try
            tic;
            fprintf('dt is %g; attempting solve...\n',dt)
            
            % set parameters 
            pari = setParameters(options, ...
                'NF',           201, ...
                'Ht',           5, ...
                'Hq',           10, ...
                'calS',         14, ...
                'dt',           dt, ...
                'calM2',        calM2i, ...
                'NumBeat',      1, ...
                'tmax',         'auto', ...
                'lambda',       1e3, ...
                'NumSave',      50 ...
            );
            pari.warm_up = 0;

            % set initial condition
            int = setInitialCondition(options, pari, 'IntCond', 'line');
            
            % solve
            out = runSPX(int, pari, options);
            stime = toc;
            exit = 'ok';
            fprintf(' success!\n')
            
        catch
            dt = dt/2;
            printLinebreak('~')
            fprintf('failed, reducing dt to %g\n', dt)
            exit = 'fail';
            
        end
        
        % if dt very small, call it quits and continue
        if dt <= 1e-6
            exit = 'ok';
            out = []; stime = [];
            fprintf('Not converging for small dt -- self intersection likely\n')
        end
    end
    
    % save data
    if twBool
        str = sprintf('./convergence/stability_data/twOn_%02g_of_%02g_calS=%g.mat', ...
        i, num_calM2, pari.calS);
    else
        str = sprintf('./convergence/stability_data/twOff_%02g_of_%02g_calS=%g.mat', ...
        i, num_calM2, pari.calS);
    end
    parsave(str, out, pari, options, stime);
    
end

end % function
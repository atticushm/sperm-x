function plotLambdaConvergence()

% generate plots assessing the effects of increasing the tension damping
% parameter, lambda, in a relaxing filament simulation

close all;

% load in simulation results
files = dir('./convergence/relaxing_filament_data/lambda*.mat');
for i = 1:length(files)
    load(files(i).name,'varargin');
    outs{i}     = varargin{1};
    params{i}   = varargin{2};
    options{i}  = varargin{3};
end
num_l = length(files);

% sort data by determining lambda value 
for i = 1:num_l
    lambdas(i) = params{i}.lambda;
end
[lambdas, idx] = sort(lambdas);
outs    = outs(idx);
params  = params(idx);
options = options(idx);

% determine arclength across simulation
tps = linspace(0,params{1}.tmax, params{1}.NumSave);
num_tps = length(tps);
for i = 1:num_l
    for j = 1:num_tps

        % extract coordinate data
        X = outs{i}(j).X;
        
        % compute arclength
        L(i,j) = calculateArclength(X);

    end 
end
 
%% L at tmax

nexttile; box on;
semilogx(lambdas, L(:,end), 'bo-', 'LineWidth', 1.2); grid on;

xlabel('$\lambda$', 'FontSize',14, 'Interpreter','latex');
ylabel('$L$ at $t_{\textup{max}}$', 'FontSize',14, 'Interpreter', 'latex');

%% L across simulation for select lambdas

select  = 1:3:num_l;
num_sel = length(select);
l_sel   = lambdas(select);

nexttile; hold on; box on; 
cols = flipud(parula(num_sel));
for i = 1:num_sel
    plot(tps, L(select(i), :), 'LineWidth', 1.2, 'Color', cols(i,:))
end
xlabel('$t$', 'FontSize', 14, 'Interpreter','latex');
ylabel('$L$', 'FontSize', 14, 'Interpreter','latex');

% legend
for i = 1:num_sel
    str{i} = sprintf('$\\lambda=%.1e$',l_sel(i));
end
lgd = legend(str{1},str{2},str{3},str{4},str{5},str{6},'Interpreter','latex',...
    'Location','eastoutside', 'EdgeColor','none', 'FontSize',12);

%% save

if ~exist('./convergence/figures/','dir')
    mkdir('./convergence/figures/')
end
save_str = './convergence/figures/lambda_convergence_bench.pdf';
save2pdf(save_str);
fprintf('Figure saved at %s\n', save_str);
printLinebreak

end % function
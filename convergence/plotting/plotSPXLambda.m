function plotSPXLambda()

% load data
files = dir('./convergence/swimming_cell_data/lambda=*.mat');
for i = 1:length(files)
    load(files(i).name,'varargin');
    outs{i}     = varargin{1};
    params{i}   = varargin{2};
    options{i}  = varargin{3};
end

% number of sperm-x simulations
num_l = length(outs);
num_t = length(outs{1});

% time values
for i = 1:num_t
    t_vals(i) = outs{1}(i).t;
end

% load lambda values and sort data
for i = 1:num_l
    l_vals(i) = params{i}.lambda;
end
[l_vals,ord] = sort(l_vals);
outs = outs(ord);
params = params(ord);
options  = options(ord);

%% surf of L across time

% load custom colormap for % diff plots
load('tools/blood.mat','blood');

% compute arclength at each time point
for i = 1:num_l
    for j = 1:num_t
        arcL(i,j) = calculateArclength(outs{i}(j).X);
        rel_arcL(i,j) = abs(arcL(i,j)-1)*100;
    end
end

% plot
x = log(l_vals(1:10)); y = t_vals(40:end);
[xx,yy] = meshgrid(x,y);
surf(xx, yy, rel_arcL(1:10,40:end)', 'EdgeColor','none');
view(2);
axis tight
shading flat

% axis
xlim([0,6]);

% color axis
cax = caxis;
caxis([0, cax(2)])
colormap(blood)

% colorbar
cbar = colorbar;
cbar.Label.String = '\% $L$ growth';
cbar.Label.Interpreter = 'latex';
cbar.Label.FontSize = 14;
cbar.Ticks = [0,1,2];
cbar.TickLabelInterpreter = 'latex';
cbar.Ruler.TickLabelFormat = '%.1g';
cbar.Location = 'northoutside';

% add enclosing box
fbox = gca;
box on;
grid off;
fbox.LineWidth = 0.5;
fbox.Layer = 'top';

% labels
xlabel('$\log \lambda$','FontSize',14,'Interpreter','latex');
ylabel('$t$','FontSize',14,'Interpreter','latex');

%% range of arcL across each simulation

% get ranges
nexttile;
for i = 1:num_l
    arcL_range(i) = abs(max(arcL(i,:))-min(arcL(i,:)));
end

% plot
semilogx(l_vals, arcL_range, 'LineWidth',1.2); box on; grid on

% labels
xlabel('$\lambda$','FontSize',14,'Interpreter','latex')
ylabel('$|L_{\textup{max}} - L_{\textup{min}}|$','FontSize',14, ...
    'Interpreter','latex')

% ticks
ytickformat('%.1f')
ax = gca; ax.YAxis.Exponent = -2;


end % function
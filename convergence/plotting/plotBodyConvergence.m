function plotBodyConvergence()

% plots relative error between numerical and analytic total force and
% moment on a sphere of unit radius

close all;

%% load data

load('convergence/body_data/L=1.mat')

%% latex for tick labels

set(groot, 'defaultAxesTickLabelInterpreter','latex');
set(groot, 'defaultAxesFontSize',14);

%% set tiled layout

tiledlayout('flow')

%% force error

% plot
nexttile; box on;
l11=loglog(Nf, F_rel(:,1),'x-','LineWidth',1.2); hold on; grid on
l12=loglog(Nf, F_rel(:,2),'d-','LineWidth',1.2);
l13=loglog(Nf, F_rel(:,3),'o-','LineWidth',1.2);

% labels
xlabel('$N_t$','FontSize',14,'Interpreter','latex')
ylabel('Force rel. error','FontSize',14,'Interpreter','latex')
legend([l11,l12,l13],{'$H_q=H_t$','$H_q=2H_t$','$H_q=3H_t$'}, ...
    'Interpreter','latex','Location','southwest','EdgeColor','none')

% axes

ytickformat('%.2f')
% ax = gca; ax.YAxis.Exponent = -2;

%% moment error

% plot
nexttile; box on;
l21=loglog(Nf, M_rel(:,1),'x-','LineWidth',1.2); hold on; grid on;
l22=loglog(Nf, M_rel(:,2),'d-','LineWidth',1.2);
l23=loglog(Nf, M_rel(:,3),'o-','LineWidth',1.2);

% labels
xlabel('$N_t$','FontSize',14,'Interpreter','latex')
ylabel('Moment rel. error','FontSize',14,'Interpreter','latex')
legend([l21,l22,l23],{'$H_q=H_t$','$H_q=2H_t$','$H_q=3H_t$'}, ...
    'Interpreter','latex','Location','southwest','EdgeColor','none')

% axes

ytickformat('%.2f')
% ax = gca; ax.YAxis.Exponent = -2;

%% walltime

% plot
nexttile; box on;
l31=loglog(Nf, wt_Ny, 'x-', 'LineWidth',1.2); hold on; grid on;
l32=loglog(Nf, wt_NN1,'d-', 'LineWidth',1.2);
l33=loglog(Nf, wt_NN2,'o-', 'LineWidth',1.2);

% labels
xlabel('$N_t$','FontSize',14,'Interpreter','latex')
ylabel('wall time','FontSize',14,'Interpreter','latex')
legend([l31,l32,l33],{'$H_q=H_t$','$H_q=2H_t$','$H_q=3H_t$'}, ...
    'Interpreter','latex','Location','northwest','EdgeColor','none')

%% scale and save

set(gcf,'Position',[177 299 708 217])
save2pdf('convergence/figures/body_convergence.pdf')
close all

end % function
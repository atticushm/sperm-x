function plotRelaxingCellComparison()

% generate plots comparing relaxing filament results using Sperm-X with
% varying N to an EIF simulation result with N=161.
% data generated by the functions spermXRelaxingConvergence.m and
% eifRelaxingFilament.m respectively

close all;
printLinebreak

% load in sperm-x simulation results
files = dir('./convergence/relaxing_cell_data/N=*.mat');
for i = 1:length(files)
    load(files(i).name,'varargin');
    outs{i}     = varargin{1};
    params{i}   = varargin{2};
    options{i}  = varargin{3};
end
clear varargin

% load in eif simulation result
load('./convergence/relaxing_cell_data/eif_N=81.mat')
eif_out = sol;
eif_mdl = model;
clearvars sol model

% number of sperm-x simulations
num_N = length(outs);
num_t = length(outs{1});

% time points of solutions
tps = linspace(0, params{1}.tmax, num_t);

% load N values and sort data (dir returns file names in a strange order)
for i = 1:num_N
    N_vals(i) = params{i}.NF;
end
[N_vals,ord] = sort(N_vals);
outs = outs(ord);
params = params(ord);

% get eif coordianates at time points
addpath(genpath('./convergence/eif_ext'))
eif_sol = deval(eif_out, tps);
Q = size(eif_sol,1)-2;
for i = 1:num_t
    [xx, yy, ~] = GetFilamentCoordinates( eif_sol(:,i), 1/Q );
    X_eif(i).x = xx;
    X_eif(i).y = yy;
end
rmpath(genpath('./convergence/eif_ext'))

%% latex for tick labels

set(groot, 'defaultAxesTickLabelInterpreter','latex');
set(groot, 'defaultAxesFontSize',14);

%% set tiled layout

tiledlayout(2,3) %,'TileSpacing','compact')

%% relaxing cell trace

% number of lines (ish)
num_trace = 10;

% plot interval
int = floor(num_t/num_trace);

% data to plot
plot_spx  = outs{4}(1:int:end);     % SPX data, N=161
plot_eifX = X_eif(1:int:end);       % eif flag data

num_lines = length(plot_spx);       % actual number of lines to draw

% colours from colormaps
cols1 = winter(num_lines);
cols2 = autumn(num_lines);

% plot
nexttile([1,3]);
box on; hold on; grid on;
for i = 1:num_lines
    [x1,x2,~] = extractComponents(plot_spx(i).X);
    if (i==num_lines)
        [y1,y2,y3] = extractComponents(plot_spx(i).y);
    end
    
    l1{i} = plot(x1,x2,'-', 'LineWidth',1.2, 'Color',cols1(i,:));
    %l1{i}.Color = [0 0.4470 0.7410 i/num_lines];
    if (i==num_lines)
        % out = surfaceFromPoints(plot_spx(i).y);
        % trisurf(out.hull, out.x, out.y, out.z, 'Edgecolor','none');
        % colormap('gray'); shading interp
        scatter3(y1,y2,y3,'k.')
    end
    
    l3{i} = plot(plot_eifX(i).x, plot_eifX(i).y, '-', 'Color',cols2(i,:));
    % l3{i}.Color(4) = i/num_lines;
end

% legend
% lgd = legend([l1{end},l3{end}],{'SPX, $N=161$', 'EIF, $N=81$'}, 'Location', 'southeast','Interpreter','latex');
% lgd.EdgeColor = 'none';

% centre graph about midpoint
midp = [mean(x1); mean(x2); 0];
dx   = 0.6; dy = 0.3;
axis equal
axis([midp(1)-dx midp(1)+dx, midp(2)-dy midp(2)+dy])
%xlabel('$x$', 'FontSize', 14, 'Interpreter', 'latex');
%ylabel('$y$', 'FontSize', 14, 'Interpreter', 'latex');

%% X0 convergence with N

% centre of mass from eif
X0_eif = [X_eif(end).x(1); X_eif(end).y(1); 0];

% difference between final positions of centre of mass
for i = 1:num_N
    % determine N
    N_vals(i) = params{i}.NF;
    
    % determine centre of mass in sperm-x
    [x1,x2,x3]  = extractComponents(outs{i}(end).X);
    X0_spx(:,i) = [x1(1); x2(1); x3(1)];     
    
    % rmsd
    diff_eif(i) = rmse(X0_spx(:,i),X0_eif);
end

% plot X0 convergence
nexttile; box on; 
semilogx(N_vals, diff_eif, 'x-', 'LineWidth',1.2); grid on;

% plot RMSD convergence
% l2 = semilogx(N_vals, rmsd_flag, 'd-', 'LineWidth',1.2);

% axes
xlim([10^1, 10^3])
ytickformat('%.1f')
ax = gca; ax.YAxis.Exponent = -3;
%xlabel('$N$', 'FontSize', 14, 'Interpreter', 'latex');
%ylabel('RMSD', 'FontSize', 14, 'Interpreter', 'latex');

%% RMSD convergence with N

% RMSD between flagella shapes at all t -- requires splining
s_eif = linspace(0,1,eif_mdl.discr.Q+1);
for i = 1:num_N
    
    % arclength discretisation in SPX
    s_spx = linspace(0,1,N_vals(i));
    
    for j = 1:num_t

        % SPX coordinate components at t
        [x1,x2,x3] = extractComponents(outs{i}(j).X);

        % spline to get values at s_eif nodes
        X1 = spline(s_spx, x1, s_eif);
        X2 = spline(s_spx, x2, s_eif);
        X3 = spline(s_spx, x3, s_eif);
        X_spx = [X1(:); X2(:)];
        X_cmp = [X_eif(j).x(:); X_eif(j).y(:)];

        % calculate rmsd
        rmsd_flag(i,j) = rmse(X_spx, X_cmp);
    end
    
end

% plot mean rmsd value (in time) 
nexttile; box on;
semilogx(N_vals, mean(rmsd_flag,2), 'x-', 'LineWidth',1.2); grid on;

% axes
xlim([10,10^3]); ylim([4.6,6]*1e-3)

%% RMSD in tangent at X0

% finite difference matrix

% RMSD between tangents at X0 at all t
s_eif = linspace(0,1,eif_mdl.discr.Q+1);
for i = 1:num_N
    
    % arclength discretisation in SPX
    s_spx = linspace(0,1,N_vals(i));
    
    % finite difference matrix
    D = finiteDifferences(s_spx, 1);
    
    for j = 1:num_t

        % SPX tangent vector at s=0,t
        tanv_SPX = proxp(kron(eye(3),D{1})*outs{i}(j).X);
        
        % SPX tangent angle
        tang_SPX = atan(tanv_SPX(2)/tanv_SPX(1));
        
        % EIF tangent angle
        tang_EIF = eif_sol(3,j);

        % calculate rmsd
        rmsd_tang(i,j) = rmse(tang_SPX, tang_EIF);
    end
    
end

% plot mean rmsd value (in time)
nexttile; box on;
semilogx(N_vals, mean(rmsd_tang,2), 'x-', 'LineWidth',1.2); grid on

% axes
ax = gca; ax.YAxis.Exponent = -2;
xlim([10,10^3]);

%% save figure

% scale and resize
set(gcf, 'Position', [560 566 560 382])

% save
str = sprintf('./convergence/figures/relaxing_cell_bench.pdf');
save2pdf(str);
fprintf('Figure saved at %s\n', str);

close all;
printLinebreak

end % function
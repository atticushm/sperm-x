function plotSPXPosition()

close all;

% load data
files = dir('./convergence/swimming_cell_data/N=*.mat');
for i = 1:length(files)
    load(files(i).name,'varargin');
    outs{i}     = varargin{1};
    params{i}   = varargin{2};
    options{i}  = varargin{3};
end

% number of sperm-x simulations
num_N = length(outs);

% number of recorded time points
num_t = length(outs{end});

% load N values and sort data (dir returns file names in a strange order)
for i = 1:num_N
    N_vals(i) = params{i}.NF;
end
[N_vals,ord] = sort(N_vals);
outs = outs(ord);
params = params(ord);
options  = options(ord);

% extract traces of converged solutions
ii = 1;
for i = 1:num_N
    num_tps = length(outs{i});
    if num_tps == num_t         % simulation converged, all good
        v{ii} = extractPath([outs{i}.X]);
        ii    = ii+1;
    else                        % simulation did not converge
    end
end
num_conv = length(v);
N_conv = N_vals(end-num_conv+1:end); 

% time points
for i = 1:num_tps
    tps(i) = outs{1}(i).t;
end

%% final cell position: hi res vs low res

%{
% draw cells
nexttile; hold on; box on; grid on;
cols = parula(num_conv+1);
for i = [1,num_conv]
    
    % plot flagellum
    [x1,x2,~] = extractComponents(outs{i}(end).X);
    plot(x1,x2, 'Color',cols(i,:), 'LineWidth',1.4);
    
    % plot head
    if (i==num_conv)
        [y1,y2,y3] = extractComponents(outs{i}(end).y);
        scatter(y1,y2,'k.')
    end
    
end

% axes
axis equal
xlim([-0.2 0.9])

% label
xlabel('$x$','FontSize',14,'Interpreter','latex')
ylabel('$y$','FontSize',14,'Interpreter','latex')
%}

%% X0 trace convergence

%{
% set color of each N case
case_col = flipud(winter(num_conv));

% set transparency values
transp_val = linspace(0.3,1,num_conv);

% plot traces
hold on; box on
for i = 1:num_conv
    plot(v{i}(1,:), v{i}(2,:), 'LineWidth',1.2, ...
        'Color',[case_col(i,:),transp_val(i)]);
end
%}

%{
% rmse in time between each X0 and highest resolution simulation
for i = 1:num_conv
    for j = 1:num_t
        e_rms(i,j) = rmse(v{i}(:,j), v{end}(:,j));
        e_rel(i,j) = relErr(v{i}(:,j), v{end}(:,j));
    end
end

% plot
nexttile;
loglog(N_conv, e_rel(:,end), 'x-', 'LineWidth',1.2); grid on;

% axes
xlim([70 520])
ylim([10^(-4),1])
xticks([100,400])

%}

% rmse upon doubling number of segments
for i = 1:num_conv-1
    for j = 1:num_tps
        X1 = outs{i}(j).X;
        
        % since Q is doubled, every other point will have same arclength value
        [x1,x2,x3] = extractComponents(outs{i+1}(j).X);
        X2 = [x1(1:2:end); x2(1:2:end); x3(1:2:end)];
        
        rmsd(i,j) = rmse(X1, X2);
    end
end

% plot
nexttile; box on
for i = 1:num_conv-1
    lp{i} = semilogy(tps, rmsd(i,:), 'LineWidth',1.2); hold on; grid on;
end

% axes
axis tight

% labels
ylabel('$\log $ RMSD', 'FontSize',14,'Interpreter','latex')
xlabel('$t$', 'FontSize',14,'Interpreter','latex')
legend([lp{1},lp{2},lp{3},lp{4}], '101 v 201','201 v 401','401 v 801','801 v 1601', ...
    'FontSize',12, 'Location','southeast', 'EdgeColor','none','Interpreter','latex')


end % function
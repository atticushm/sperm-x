function p1 = plotqConvergence()

% generate plots assessing the effects of increasing the tension damping
% parameter, lambda, in a relaxing cell simulation

% load in simulation results
files = dir('./convergence/relaxing_cell_data/q*.mat');
for i = 1:length(files)
    load(files(i).name,'varargin');
    outs{i}     = varargin{1};
    params{i}   = varargin{2};
    %options{i}  = varargin{3};
end
num_q = length(files);
num_t = length(outs{1});

% load in eif simulation result
load('./convergence/relaxing_cell_data/eif_N=81.mat')
eif_out = sol;
eif_mdl = model;
clearvars sol model

% time points of solutions
tps = linspace(0, params{1}.tmax, num_t);

% sort SPX data by determining q value 
for i = 1:num_q
    q_vals(i) = params{i}.q;
end
[q_vals, idx] = sort(q_vals);
outs    = outs(idx);
params  = params(idx);

% get eif coordianates at time points
addpath(genpath('./convergence/eif_ext'))
eif_sol = deval(eif_out, tps);
Q = size(eif_sol,1)-2;
for i = 1:num_t
    [xx, yy, ~] = GetFilamentCoordinates( eif_sol(:,i), 1/Q );
    X(i).x = xx;
    X(i).y = yy;
end
rmpath(genpath('./convergence/eif_ext'))
 
%% latex for tick labels

set(groot, 'defaultAxesTickLabelInterpreter','latex');
set(groot, 'defaultAxesFontSize',14);

%% sperm-x comparison to endpiece code

% centre of mass from endpiece
% Xc_epc = [mean(X(end).x); mean(X(end).y); 0];
X0_eif = [X(end).x(1); X(end).y(1); 0];

% difference between final positions of centre of mass
for i = 1:num_q
    % determine q
    q_vals(i) = params{i}.q;
    
    % determine centre of mass in sperm-x
    [x1,x2,x3]  = extractComponents(outs{i}(end).X);
    %Xc_spx(:,i) = [mean(x1); mean(x2); mean(x3)];
    X0_spx(:,i) = [x1(1); x2(1); x3(1)];
    
    % relative difference
    % diff_epc(i) = norm(abs(Xc_spx(:,i)-Xc_epc))./norm(Xc_epc);        
    
    % rmsd
    diff_epc(i) = rmse(X0_spx(:,i),X0_eif);
end

% plot
box on; 
l1 = semilogx(q_vals, diff_epc, 'x-', 'LineWidth', 1.2); hold on;
grid on

% axes
xlim([0.01, 1])
ylim([0.044, 0.054])
ytickformat('%.2f') 
ax = gca; ax.YAxis.Exponent = -2;
xlabel('$q$', 'FontSize', 14, 'Interpreter', 'latex');
ylabel('RMSD', 'FontSize', 14, 'Interpreter', 'latex');

% legend
% lgd = legend([l1],{'v EIF ($N=81$)'}, 'Location','southeast', 'Interpreter','latex', 'FontSize',14);
% lgd.EdgeColor = 'none';

% resize
set(gcf, 'Position', [656 714 464 233]);

%% save

% if called in isolation, save figure
if isempty(dbstack(1))
    if ~exist('./convergence/figures/','dir')
        mkdir('./convergence/figures/')
    end
    save_str = './convergence/figures/q_convergence_bench.pdf';
    save2pdf(save_str);
    fprintf('Figure saved at %s\n', save_str);
    printLinebreak
end
p1 = gcf;

end % function
function plotSPXTempStab()

% loads data and plots figures for SPX temporal stability tests

close all

% load data
fnames = dir('./convergence/stability_data/twOff_*.mat');
num_files = length(fnames);
for i = 1:num_files
    
    % extract data
    load(fnames(i).name, 'varargin')
    outs{i} = varargin{1};
    params{i} = varargin{2};
    options{i} = varargin{3};
    
    % extract calM2 values
    calM2_vals(i) = params{i}.calM2;
    
    % extract dt values
    dt_vals(i) = params{i}.dt;
end

%% fit polynomial through frontier

pft = fit(calM2_vals(8:end)', dt_vals(8:end)', 'poly2');
calM2_samp = linspace(calM2_vals(8), calM2_vals(end));
dt_pft = pft(calM2_samp);

%% draw figure

% latex and font size for tick labels
set(groot, 'defaultAxesTickLabelInterpreter','latex');
set(groot, 'defaultAxesFontSize',14);

% load tricolor colormap
load('tools/tricolor2.mat','tricolor2')

% area plot
figure
area(calM2_vals, dt_vals)

% frontier plot
hold on;
%plot(calM2_samp, dt_pft, 'r', 'LineWidth',1.6)

% labels
xlabel('$\mathcal{M}_2$', 'FontSize',14, 'Interpreter','latex')
ylabel('$\Delta t$', 'FontSize',14, 'Interpreter','latex')

% axes
ax = gca;
ax.XAxis.Exponent = -2;
ax.YAxis.Exponent = -1;

% scale and save
set(gcf, 'Position',[742 789 328 158]);
save2pdf('./convergence/figures/stability_no_twist.pdf')
close all

end % function
function plotSPXSwimmingConvergence()

% generates convergence plots for swimming cell simulations using SPX

% this figure will assess 3 things:
% (i) convergence with N of solution
% (ii) inextensibility enforcement with changing lambda
% (iii) effect of changing q

close all
printLinebreak

%% setup figure

tiledlayout(1,3);

%% latex for tick labels

set(groot, 'defaultAxesTickLabelInterpreter','latex');
set(groot, 'defaultAxesFontSize',14);

%% shape plots

nexttile;
plotSPXPosition();

%% lambda plots

nexttile;
plotSPXLambda();

%% save

set(gcf, 'Position',[164 650 957 297])
save_str = './convergence/figures/swimming_convergence.pdf';
save2pdf(save_str);

fprintf('Figure saved at convergence/figures/swimming_convergence.pdf!\n')
close all
printLinebreak

end % function
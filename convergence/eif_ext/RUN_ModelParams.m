function model = RUN_ModelParams( head_axes, X0, S, act_type, act_params, discr_params, varargin)

% Creates the model structure containing modelling and discretisation
% parameters and functions for an actuated sperm problem.

% Global cell rotation can be inputted via varargin:
if ~isempty(varargin)
    th_0 = varargin{1};
else
    th_0 = 0;
end

% Check X0 dimensions and correct if required:
if length(X0) < 3
    X0(3) = 0;
end

% Construct model structure:
model = struct(...
    'a',    SpermHeadAxes(head_axes), ...   % dimensions of sperm head.
    'eps',  0.01, ...                       % regularisation parameter.
    'X0',   X0(:), ...                      % head/flagellum join location.
    'th_0', th_0, ...                       % global cell rotation.
    'S',    S, ...
    ...
    'act',   ActuationStruct(act_type, act_params), ... % act structure contains actuation function & parameters,
    ...
    'discr', struct(...                     % discr structure contains discretisation parameters:
        'Nq',   discr_params(1), ...        % head quadrature discretisation parameter.
        'Nt',   discr_params(2), ...        % head traction/force discretisation parameter; Nt < Nq.
        'Q',    discr_params(3),  ...       % flagellum discretisation parameter.
        'ds',   1/discr_params(3) ...       % flagellum segment length.
        ) ...
    );      
    
end

%% Additional functions:

function a = SpermHeadAxes(head_axes)
% Set sperm head axes.

switch class(head_axes)
    case 'double'
        a = head_axes;
    case 'char'
        if strcmp(head_axes,'default') == 1
            a = [2/45, 1/45, 1.6/45]; % typical human sperm head dimensions.
        else
            warning('Sperm head dimensions not recognised!')
        end
end
end

function act = ActuationStruct(act_type, act_params)
% Construct act structure containing cell actuation functions & parameters.

% INPUTS
% act_type identifies which actuation function to call.
% act_params is a variable-length vector of parameters specific to the
%               actuation type.

act_str = ['ACT_',act_type];
act.func = str2func( act_str ); % calls corresponding actuation function.

switch act_type
    case {'SpermConstant', 'Worm'}  % sperm w/ constant stiffness, or a worm swimmer:
        act.M = act_params(1);      % active moment dimensionless parameter, M = m0 * L^3/E. 
        act.k = act_params(2);      % active moment dimensionless wavenumber.
    case 'Magnetic' % sperm actuated by an oscillatory magnetic field:
        act.M = act_params(1);      % magneto-elastic parameter.
        act.Hx = act_params(2);     % magnetic field in x.
        act.Hy = act_params(3);     % magnetic field in y.
end
end
function vals = ActuationMagnetic(sm,tail,t,percMag,M,Hx,Hy)

% Filament actuation by a magnetic field.
% Based upon ideas by Moreau et al (2018).

% INPUTS:
% - sm:         filament segment midpoints.
% - t:          time.
% - percMag:    percentage of filament to actuate (i.e. model as ferromagnetic).

Q           = length(sm);
ds          = 1/Q;
NMag        = percMag*Q/100;
MagSwitch   = [zeros(Q-NMag,1);ones(NMag,1)];

% dx = zeros(Q-1,1);
% dy = zeros(Q-1,1);
% for q = 1:Q
%     dx(q)   = MagSwitch(q) * (tail.x(end)-tail.xm(q));
%     dy(q)   = MagSwitch(q) * (tail.y(end)-tail.ym(q)); 
% end
% 
% vals = eval(Hy).*dx - eval(Hx).*dy;
% vals = vals(:);

%% How I *think* Hermes implements the moment due to the mag field.

% for q = 1:Q
%     vals(q)     = eval(Hy) * cos(tail.th(q)) - eval(Hx) * sin(tail.th(q));
%     vals(q)     = MagSwitch(q) * vals(q);
% end
% vals = vals(:);
    
%% New way via Ishimoto & Gaffney.

for q = 1:Q
    dsin    = sin(tail.th(end)) - sin(tail.th(q));
    dcos    = cos(tail.th(end)) - cos(tail.th(q));
    vals(q) = eval(Hy) * dsin + eval(Hx) * dcos;
end
vals = vals(:);

end % function
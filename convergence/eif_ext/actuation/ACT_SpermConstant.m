function int = ACT_SpermConstant(s, t, k)

% Filament actuation: constant along arclength.
% m(s,t) = s * cos(k * s - t)

% INPUTS:
% - s:  filament segment midpoints.
% - t:  time.
% - k:  dimensionless wave number.

s = s(:);
N = length(s);

int_1 = ones(N,1)*(1/k)*sin(k-t) - (s/k).*sin(k*s-t);
int_2 = ones(N,1)*(1/k^2)*cos(k-t) - (1/k^2).*cos(k*s-t);

int = int_1 + int_2;

end % function
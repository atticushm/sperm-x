function int = ACT_Worm(s, t, k)

% Filament actuation: worm-like motion.
% m(s,t) = cos(k * s - t)

% INPUTS:
% - s:  filament segment midpoints.
% - t:  time.
% - k:  dimensionless wave number.

s = s(:);
N = length(s);

int = ones(N,1)*(1/k)*sin(k-t) - (1/k).*sin(k*s - t);

end % function
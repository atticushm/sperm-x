function frames = GetFrames(solutions, tps, model)

% Computes frames data for playback of simulation solutions.s

figure('visible','off'); box on

Ntps        = length(tps);
path.vec    = []; 

for i = 1:Ntps
    
    % Get coordinate data:
    [head,flag,~] = GetProblemData(solutions(:,i), model);
    
    % Plot head and tail:
    scatter3(head.Xtrac.x(:),head.Xtrac.y(:),head.Xtrac.z(:),'k.'); hold on
    plot(flag.x(:),flag.y(:),'b-','LineWidth',1.5);

    % Plot X0 path:
    if tps(i)>0
        path.vec = [path.vec, [flag.x(1);flag.y(1)]];
        plot(path.vec(1,:),path.vec(2,:),'r-','LineWidth',1.5);
    end
    
    % Fix axes:
    axis('equal')
    xlabel('x'); ylabel('y')
    view(2)
    axis([-0.5 1.2 -0.5 0.5])
    hold off

    title(sprintf('t = %g',tps(i)))

    % Save frame data:
    frame           = getframe(gcf);
    frames.data(i)  = frame;

end

end % function
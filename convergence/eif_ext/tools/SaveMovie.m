function vid = SaveMovie(frames,fps,varargin)
% Saves a video of the movie given by frames, with filename given as input.
% Framerate of video is given by input fps.
% Frames object should be struct, as if given by GetFrames().

if isempty(varargin) == 0
    filename    = varargin;
else
    [name,path] = uiputfile('*.mp4','Save video location');
    filename    = [path,name];
end

vid             = VideoWriter(filename,'MPEG-4');
vid.FrameRate   = fps;
vid.Quality     = 100;
open(vid);
writeVideo(vid,frames.data);
close(vid);
fprintf('   Video saved!\n')

end

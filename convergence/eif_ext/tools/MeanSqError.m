function err = MeanSqError(x,y,varargin)

% Calculate the mean squared error between 2 input vectors x and y.
% sqr is boolean to indicate whether square rooted MSE should be
% calculated.

x = x(:);
y = y(:);
N = length(x);

err = (1/N)*sum((x-y).^2);

if varargin{1} == 1
    err = err.^(1/2);
end

end % function
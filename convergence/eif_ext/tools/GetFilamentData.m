function [Q, ds, fil, all] = GetFilamentData(Y,Nfil,param)
% fil(i) is all data related to the i-th filament of the system at time t.
% all stacks filament data to be used in other calculations.
% Y is solution data from which filament data is calculated.
% Q is number of segments of each filament.
% Nfil is the total number of filaments in the system.

if ~exist('L','var')
    L = 1;
end

Y  = reshape(Y,[],Nfil)';
Q  = size(Y,2)-2;
ds = L/Q;

all.xm = [];
all.ym = [];
all.zm = [];
all.Xm = [];
all.th = [];

for n = 1:Nfil
    fil(n).label            = sprintf('Filament %g data',n);
    fil(n).input(1:Q+2,1)   = Y(n,:);
    if length(L) == Nfil
        fil(n).L    = L(n);
        fil(n).ds   = ds(n);
    else
        fil(n).L    = 1;
        fil(n).ds   = ds;
    end
    fil(n).s            = 0:fil(n).ds:fil(n).L;
    fil(n).sm           = (fil(n).s(2:end)+fil(n).s(1:end-1))/2;
    fil(n).x(1)         = fil(n).input(1);
    fil(n).y(1)         = fil(n).input(2);
    fil(n).z            = zeros(1,Q+1);
    fil(n).th           = fil(n).input(3:end)';
    fil(n).thav         = mean(fil(n).th)*ones(1,Q);
    for q = 1:Q
        fil(n).x(q+1)   = fil(n).x(q) + fil(n).ds*cos(fil(n).th(q));
        fil(n).y(q+1)   = fil(n).y(q) + fil(n).ds*sin(fil(n).th(q));
    end
    fil(n).xm           = fil(n).x(1:Q) + (fil(n).ds/2)*cos(fil(n).th);
    fil(n).ym           = fil(n).y(1:Q) + (fil(n).ds/2)*sin(fil(n).th);
    fil(n).zm           = zeros(1,Q);
    fil(n).Xm           = [fil(n).xm,fil(n).ym,fil(n).zm];
    fil(n).comx         = mean(fil(n).x);
    fil(n).comy         = mean(fil(n).y);
    
    if exist('param','var') == 1
        if length(param) == Nfil
            fil(n).param  = param(n);
        else
            fil(n).param  = param;
        end
    end
    
    all.xm  = [all.xm, fil(n).xm];
    all.ym  = [all.ym, fil(n).ym];
    all.zm  = [all.zm, fil(n).zm];
    all.th  = [all.th, fil(n).th];
end
all.Xm      = [all.xm,all.ym,all.zm];

end % function
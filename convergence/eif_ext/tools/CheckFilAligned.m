function [value,isterminal,direction] = CheckFilAligned(t,Y)

global fil_aligned 
value       = (fil_aligned == 1);
isterminal  = 1; % stop integrating
direction   = 0;

end % function
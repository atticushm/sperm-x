function [VAL,dX] = CalcVAL(sol,tps,type,varargin)
% Computes the VAL of a single swimmer given solution data sol at time points tps.
% This function assumes that the driving wave period is 2*pi.

% Find time points that are 2*pi apart:
t_mod       = mod(tps,2*pi);
[~,srt_idx] = sort(t_mod);

% Choose time points so that the filament motion will have stabilised:
if ~isempty(varargin)
    range = varargin{1};
    lower = range(1);
    upper = range(2);
else
    lower = 2;
    upper = 3;
end
    
range   = upper - lower;
idx     = [srt_idx(lower),srt_idx(upper)];
sol_sel = sol(:,idx);

Q               = size(sol_sel,1)-2;
[x_1,y_1,~]     = GetFilamentCoordinates(sol_sel(:,1),1/Q);
[x_2,y_2,~]     = GetFilamentCoordinates(sol_sel(:,2),1/Q);

switch type
    case 'x0' % compute VAL from leading point X0.
        x_sel = [x_1(1),    x_2(1);    y_1(1),    y_2(1)   ]; 
    case 'xc' % compute VAL from centre of mass XC.
        x_sel = [mean(x_1), mean(x_2); mean(y_1), mean(y_2)];     
end
dX      = norm( [diff(x_sel(1,:)); diff(x_sel(2,:))] );
VAL     = dX / (range*2*pi);

%% Check values being used are indeed a wavelength apart

%{
figure; hold on;
hpth = [];
for t = 1:length(tps)
    [x(:,t),y(:,t),~] = GetFilamentCoordinates(sol(:,t),1/40);
    hpth              = [hpth, [x(1,t);y(1,t)] ];
end
plot(hpth(1,:),hpth(2,:))
scatter(hpth(1,idx),hpth(2,idx),'rx')
%}

end % function end
function [D] = GetFilamentSeperation(fil1,fil2)
% Calculates separation D and minimum distance value and index dmin and
% dminI of two input filaments (given as coordinate data) fil1 and fil2.

x1 = fil1.x; y1 = fil1.y;
x2 = fil2.x; y2 = fil2.y;

Q  = length(x1)-1;
dQ = 100/Q;

hrx1 = x1; hry1 = y1;
hrx2 = x2; hry2 = y2;

hrX = [hrx1;hry1];
hrY = [hrx2;hry2];
Nhr = length(hrx1);

for n = 1:Nhr
    diff = hrX(:,n) - hrY;
    for m = 1:size(diff,2)
        D(n) = norm(diff(:,m));
    end
end

end % function
function [head, flag, nodes] = GetProblemData(Y, model)

% Hard coded for a single swimmer!

% Inputs:
% Y:        problem input Y=[x0 y0 th].
% model:    model parameters as defined by ModelParams().

% Outputs:
% head:     struct containing data about the head of the swimmer.
% flag:     struct containing data about the flagellum of the swimmer.

%% Flagellum data.

Q = length(Y) - 2;
ds = 1/Q;
s = 0:ds:1;
sm = (s(2:end) + s(1:end-1))/2;

x(1) = Y(1);
y(1) = Y(2);
th = Y(3:end);

for q = 1:Q
    x(q+1,1) = x(q,1) + ds * cos(th(q));
    y(q+1,1) = y(q,1) + ds * sin(th(q));
end

z = zeros(Q+1,1);

xm = x(1:end-1) + (ds/2) * cos(th);
ym = y(1:end-1) + (ds/2) * sin(th);
zm = z(1:end-1);

flag = struct(...           % Contains flagellum information.
    's',    s, ...          % Discretised arclength values.
    'sm',   sm, ...         % Segment midpoints.
    'x',    x, ...          % x, y, z joint coordinates.
    'y',    y, ...
    'z',    z, ...
    'th',   th, ...         % Segment tangent angles.
    'xm',   xm, ...         % Segment midpoint coordinates.
    'ym',   ym, ...
    'zm',   zm, ...
    'xc',   mean(x), ...    % Flagellum centre of mass.
    'yc',   mean(y), ...
    'Xm',   [ xm(:); ym(:); zm(:)] ... % Vector of midpoint components.
    );

%% Head data. 

% Head nodes are reconstructed using the flagella starting point X0.
% Nodes on the surface are calculated generally and then rotated to be in
% line with xf2-xf1.

% Additionally, using nearest neighbour discretisation means we construct 2
% head discretisations, using Nc and NC, where Nc > NC.

if norm(model.a)>0
    head.Nq     = model.discr.Nq;
    head.Nt     = model.discr.Nt;
    head.Rot    = [ cos(flag.th(1)) -sin(flag.th(1)) 0  ;
                    sin(flag.th(1))  cos(flag.th(1)) 0  ;
                    0                0               1 ];

    % Generate spheres:
    head.Xquad.vec  = GenerateSphereDisc(head.Nq,1);
    head.Xquad.mat  = reshape(head.Xquad.vec',[],3);

    head.Xtrac.vec  = GenerateSphereDisc(head.Nt,1);
    head.Xtrac.mat  = reshape(head.Xtrac.vec',[],3);

    % Create sperm head(s):
    head.Xquad.mat(:,1)     = model.a(1) * head.Xquad.mat(:,1);
    head.Xquad.mat(:,2)     = model.a(2) * head.Xquad.mat(:,2);
    head.Xquad.mat(:,3)     = model.a(3) * head.Xquad.mat(:,3);
    head.Xtrac.mat(:,1)     = model.a(1) * head.Xtrac.mat(:,1);
    head.Xtrac.mat(:,2)     = model.a(2) * head.Xtrac.mat(:,2);
    head.Xtrac.mat(:,3)     = model.a(3) * head.Xtrac.mat(:,3);

    % Rotate head(s) to be in line with flagella:
    head.Xquad.mat          = (head.Rot * head.Xquad.mat')';
    head.Xtrac.mat          = (head.Rot * head.Xtrac.mat')';

    % Translate head(s) to join flagella:
    Xmat                    = [flag.x(:), flag.y(:), flag.z(:)];
    head.T                  = (head.Rot * [model.a(1);0;0] - Xmat(1,:)')';
    head.Xquad.mat          = head.Xquad.mat - head.T;
    head.Xtrac.mat          = head.Xtrac.mat - head.T;

    head.Xquad.vec          = reshape(head.Xquad.mat,[],1);
    head.Xtrac.vec          = reshape(head.Xtrac.mat,[],1);

    % Calcuate nearest neighbour matrix:
    head.NNmat      = NearestNeighbourMatrix(head.Xquad.vec(:),head.Xtrac.vec(:));

    % Label components
    head.Xquad.x    = head.Xquad.mat(:,1);
    head.Xquad.y    = head.Xquad.mat(:,2);
    head.Xquad.z    = head.Xquad.mat(:,3);
    head.Xtrac.x    = head.Xtrac.mat(:,1);
    head.Xtrac.y    = head.Xtrac.mat(:,2);
    head.Xtrac.z    = head.Xtrac.mat(:,3);

    head.NQ = length(head.Xquad.x);
    head.NT = length(head.Xtrac.x);

    % Nodes struct for plotting:
    nodes.Xquad.vec = [ head.Xquad.x(:); flag.xm(:)     ; 
                        head.Xquad.y(:); flag.ym(:)     ; 
                        head.Xquad.z(:); flag.zm(:)    ];
    nodes.Xquad.mat = [ head.Xquad.x(:)' flag.xm(:)'    ;
                        head.Xquad.y(:)' flag.ym(:)'    ;
                        head.Xquad.z(:)' flag.zm(:)'   ];

    nodes.Xtrac.vec = [ head.Xtrac.x(:); flag.xm(:)     ; 
                        head.Xtrac.y(:); flag.ym(:)     ;
                        head.Xtrac.z(:); flag.zm(:)    ];
    nodes.Xtrac.mat = [ head.Xtrac.x(:)' flag.xm(:)'    ;
                        head.Xtrac.y(:)' flag.ym(:)'    ;
                        head.Xtrac.z(:)' flag.zm(:)'   ];

    %% Fallbacks

    if model.discr.Nq == model.discr.Nt
        head.X      = head.Xquad.vec;
        head.Xmat   = head.Xquad.mat;
        head.x      = head.Xquad.x;
        head.y      = head.Xquad.y;
        head.z      = head.Xquad.z;
    end
end

end % function
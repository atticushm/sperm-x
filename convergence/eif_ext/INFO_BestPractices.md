# Personal Coding Rules

## File naming best practices

Functions should be saved as FirstSecondThird.m etc in such a way that clearly describes the function or purpose of the code.

Rates functions should be labeled with a capitalised prefix as RATES_Function.m.

Integral scripts (i.e. scripts which should be run to complete simulations) should be labeled with a capitalised prefix as RUN_Script.m.

## File saving best practices

All Integral scripts should exist in the root directory.

Codes implementing the rates functions should be saved in the `rates` subdirectory.

Other functions and subfunctions should be saved in the `codes` subdirectory.
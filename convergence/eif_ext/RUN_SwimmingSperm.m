function [solutions, tpoints] = RUN_SwimmingSperm(model, Y0, tmax)

%% Filaments swimming due to an active moment 

% Code by A.L. Hall-McNair, University of Birmingham, 2019.
% Single sperm-like swimmer, constructed as a scalene ellipsiod head section
% connected to a flexible elastic propulsive filament modelled using the
% EIF method (Hall-McNair et al., 2019).

% INPUTS
% - model:      Model parameter data given by RUN_ModelParams().
% - Y0:         initial condition.
% - tmax:       max time.

% OUTPUTS
% - solutions:  solution data at time points tps.
% - tpoints:    vector of time points at which solution is given.
% - coords:     coordinate data for quick plotting.
% - frames:     frame data for use with PlayMovie().

close all; 
addpath('code/'); addpath('rates/');
initStr = [' || SWIMMING SPERM || S=',num2str(model.S),' || ',func2str(model.act.func),' || '];
nChar = length(initStr);
dashStr = repmat('-',1,nChar);
disp(dashStr);
disp(initStr);
disp(dashStr);

%% Solve

% Label the rates function which calculates dX0/dt and dtheta/dt:
dY = @(t,Y) RATES_SwimmingSperm(t, Y, model);

% Solver options:
options = odeset('Events',@CheckFilSelfIntersection,'OutputFcn',@odetpbar);

% Integrate rates function over [0 tmax]:
sol_data = ode15s(dY, [0 tmax], Y0, options); % returns general solution structure.

% Extract solution and time data:
tpoints = linspace(0, tmax, 120);
% solutions = deval(sol_data, tpoints); % evaluate solution structure at specified time points.
solutions = sol_data;

%% Additional outputs

%{
disp('Computing coordinates...')
Nt = length(tpoints);
Q = size(solutions,1) - 2;
coords.x = zeros( Q+1, Nt);
coords.y = zeros( Q+1, Nt);
for n = 1:Nt
    [x, y, ~] = GetFilamentCoordinates( solutions(:,n), 1/Q );
    coords.x(:,n) = x(:);
    coords.y(:,n) = y(:);
end
    
disp('Computing frames...')
frames = GetFrames(solutions, tpoints, model); % <-- needs inputs fixed
%}

disp('Simulation complete!')
disp(dashStr);
    
end % function


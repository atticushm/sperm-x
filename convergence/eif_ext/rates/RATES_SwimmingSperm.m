function [dY] = RATES_SwimmingSperm(t, Y, model)

% By Atticus Hall-McNair, University of Birmingham, 2020.
% CURRENTLY HARD CODED FOR A SINGLE SWIMMER.
% Uses Nearest Neighbour regularised stokeslets for the head forces.

% Inputs:
% - t:          time value.
% - Y:          configuration vector (x0,y0,theta1,...,thetaQ)'.
% - swimmer:    struct of swimmer physical parameters.
% - model:      struct of model parameters defied by CreateModelStruct().

% Outputs:
% - dY:         the velocity vector to be integrated by ode15s, of the form:
%               dY=[x0' y0' th1' thQ'];

%% Get problem data
% The head nodes are reconstructed from X0 in such a way that the filament
% intersects at a right angle with the head. X0 itself is embedded into the
% head and thus has velocity via rigid body mechanics.

[head, flag, ~] = GetProblemData(Y(:), model);
NT  = head.NT;
Q   = model.discr.Q;
ds  = 1/Q;
S   = model.S;

%% Hydrodynamics

% We want to use the semi-analytic method of regularised stokeslets as much
% as possible. All velocities resulting from flagella source points can be
% calculated in this way.

% Rotation matrices for each of the flagella segments:
Rot = [ cos(flag.th)' -sin(flag.th)' zeros(1,Q)  ;
        sin(flag.th)'  cos(flag.th)' zeros(1,Q)  ;
        zeros(1,Q)     zeros(1,Q)    ones(1,Q)  ];

% Calculate various hydrodynamic matrices:
HonH  = RegStokesletsDJS(head.Xtrac.vec(:), head.Xquad.vec(:), model.eps);
HonF  = RegStokesletsDJS(flag.Xm(:), head.Xquad.vec(:), model.eps);

HonH  = HonH * head.NNmat;
HonF  = HonF * head.NNmat;

FonH  = RegStokesletAnalyticIntegrals(head.Xtrac.vec(:), flag.Xm(:), ds/2, Rot, model.eps);
FonF  = RegStokesletAnalyticIntegrals(flag.Xm(:), flag.Xm(:), ds/2, Rot, model.eps);

DHonH = size(HonH)/3;  
DHonF = size(HonF)/3; 
DFonH = size(FonH)/3; 
DFonF = size(FonF)/3; 

% Rearrange individual matrices to combine correctly into
% the larger overall hydrodynamic matrix:
Hxx = [ HonH(1:DHonH(1),1:DHonH(2)) FonH(1:DFonH(1),1:DFonH(2))     ;
        HonF(1:DHonF(1),1:DHonF(2)) FonF(1:DFonF(1),1:DFonF(2))    ];

Hxy = [ HonH(1:DHonH(1),DHonH(2)+1:2*DHonH(2)) FonH(1:DFonH(1),DFonH(2)+1:2*DFonH(2)) ;
        HonF(1:DHonF(1),DHonF(2)+1:2*DHonF(2)) FonF(1:DFonF(1),DFonF(2)+1:2*DFonF(2))];
    
Hxz = [ HonH(1:DHonH(1),2*DHonH(2)+1:3*DHonH(2)) FonH(1:DFonH(1),2*DFonH(2)+1:3*DFonH(2))   ;
        HonF(1:DHonF(1),2*DHonF(2)+1:3*DHonF(2)) FonF(1:DFonF(1),2*DFonF(2)+1:3*DFonF(2))  ];
        
Hyx = [ HonH(DHonH(1)+1:2*DHonH(1),1:DHonH(2)) FonH(DFonH(1)+1:2*DFonH(1),1:DFonH(2)) ;
        HonF(DHonF(1)+1:2*DHonF(1),1:DHonF(2)) FonF(DFonF(1)+1:2*DFonF(1),1:DFonF(2))];
    
Hyy = [ HonH(DHonH(1)+1:2*DHonH(1),DHonH(2)+1:2*DHonH(2)) FonH(DFonH(1)+1:2*DFonH(1),DFonH(2)+1:2*DFonH(2)) ;
        HonF(DHonF(1)+1:2*DHonF(1),DHonF(2)+1:2*DHonF(2)) FonF(DFonF(1)+1:2*DFonF(1),DFonF(2)+1:2*DFonF(2))];
    
Hyz = [ HonH(DHonH(1)+1:2*DHonH(1),2*DHonH(2)+1:3*DHonH(2)) FonH(DFonH(1)+1:2*DFonH(1),2*DFonH(2)+1:3*DFonH(2))     ;
        HonF(DHonF(1)+1:2*DHonF(1),2*DHonF(2)+1:3*DHonF(2)) FonF(DFonF(1)+1:2*DFonF(1),2*DFonF(2)+1:3*DFonF(2))    ];
    
Hzx = [ HonH(2*DHonH(1)+1:3*DHonH(1),1:DHonH(2)) FonH(2*DFonH(1)+1:3*DFonH(1),1:DFonH(2))   ;
        HonF(2*DHonF(1)+1:3*DHonF(1),1:DHonF(2)) FonF(2*DFonF(1)+1:3*DFonF(1),1:DFonF(2))  ];
    
Hzy = [ HonH(2*DHonH(1)+1:3*DHonH(1),DHonH(2)+1:2*DHonH(2)) FonH(2*DFonH(1)+1:3*DFonH(1),DFonH(2)+1:2*DFonH(2))     ;
        HonF(2*DHonF(1)+1:3*DHonF(1),DHonF(2)+1:2*DHonF(2)) FonF(2*DFonF(1)+1:3*DFonF(1),DFonF(2)+1:2*DFonF(2))    ];
    
Hzz = [ HonH(2*DHonH(1)+1:3*DHonH(1),2*DHonH(2)+1:3*DHonH(2)) FonH(2*DFonH(1)+1:3*DFonH(1),2*DFonH(2)+1:3*DFonH(2)) ;
        HonF(2*DHonF(1)+1:3*DHonF(1),2*DHonF(2)+1:3*DHonF(2)) FonF(2*DFonF(1)+1:3*DFonF(1),2*DFonF(2)+1:3*DFonF(2))];

H   = -[ Hxx Hxy Hxz; Hyx Hyy Hyz; Hzx Hzy Hzz]; 

%% Kinematics

% Calculate kinematic rigid body head velocity terms:
Dx = head.Xtrac.x(:) - flag.x(1);
Dy = head.Xtrac.y(:) - flag.y(1);

% Compute segment midpoint velocities from tangent angle formulation:
Kx = tril(repmat(-ds*sin(flag.th(:))',Q,1),-1)+diag(-ds/2*sin(flag.th(:))') ;
Ky = tril(repmat( ds*cos(flag.th(:))',Q,1),-1)+diag( ds/2*cos(flag.th(:))') ;

% Construct kinematic matrix:
K   = [ ones(NT,1)  zeros(NT,1) -Dy(:)   zeros(NT,Q-1)  ;
        ones(Q,1)   zeros(Q,1)  Kx                 ;
        zeros(NT,1) ones(NT,1)  Dx(:)    zeros(NT,Q-1)  ;
        zeros(Q,1)  ones(Q,1)   Ky                 ;
        zeros(NT+Q,Q+2)                                ];
    
%% Elastics equations

% Calculate flagellum terms in total moment balance equation:
DFx = ds * (flag.xm(:) - flag.x(1)); DFx = DFx';
DFy = ds * (flag.ym(:) - flag.y(1)); DFy = DFy';

% Calculate head terms in total moment balance equation:
DHx = zeros(1,NT); DHy = zeros(1,NT);
for r = 1:NT
    DHx(1,r) = (head.Xquad.x(:)' - flag.x(1)) * head.NNmat(1:head.NQ, r) ;
    DHy(1,r) = (head.Xquad.y(:)' - flag.y(1)) * head.NNmat(1:head.NQ, r) ;
end

% The elastics equations along the flagella are computed via:
MF  =  -[ triu(repmat( ds*flag.y(1:Q), 1, Q))  + triu(repmat(-ds*flag.ym', Q, 1))  ...
          triu(repmat(-ds*flag.x(1:Q), 1, Q))  + triu(repmat( ds*flag.xm', Q, 1)) ];
MFx = S^4 * MF(2:end,1:Q);
MFy = S^4 * MF(2:end,Q+1:2*Q);

% Calculate head weights (due to nearest neighbour approximation) in total force balance equation:
head_w = sum(head.NNmat);
head_w = head_w(1:NT);

% Construct the elastics matrix:
E   = [ -DHy            -DFy            DHx             DFx            zeros(1,NT+Q)   ;
        zeros(Q-1,NT)   MFx             zeros(Q-1,NT)   MFy            zeros(Q-1,NT+Q) ;
        head_w          ds*ones(1,Q)    zeros(1,NT)     zeros(1,Q)     zeros(1,NT+Q)   ;
        zeros(1,NT)     zeros(1,Q)      head_w          ds*ones(1,Q)   zeros(1,NT+Q)  ];

%% Solve linear system

% Contruct LHS block matrix:
A = [ zeros(Q+2,Q+2), E; K, H ] ;

% Compute RHS terms:
act = model.act; % Contains actuation parameters.
switch func2str(act.func)
    case 'ACT_SpermConstant'
        act_terms = - act.M * act.func(flag.s(:), t, act.k);
    case 'ACT_Worm'
        act_terms = - act.M*S^4 * act.func(flag.s(:), t, act.k);
end
b = [ 0; diff(flag.th(:))/ds + act_terms(2:end-1); zeros(3*Q+3*NT +2,1) ];

% Solve the system for dZ=[Ux Uy Omgz=dot(th1) ... dot(thQ) f... g... h...]:
dZ = A\b; 
dY = dZ(1:Q+2);  

end % function


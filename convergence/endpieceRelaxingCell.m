function endpieceRelaxingCell()

% runs a high resolution relaxing cell simulation using endpiece code

printLinebreak;
clear all;
addFilesToPath

% generate initial condition equal to that in spermXRelaxingConvergence()
problem = 'RelaxingCell';
options = setOptions(problem, ...
    'BodyType',     'gskb1', ...
    'NonLocal',     true, ...
    'StiffFunc',    'constant', ...
    'ForcePlanar',  true, ...
    'PlaneOfBeat',  'xy' ...
);
params = setParameters(options, ...
    'ittol',        1e-5, ...
    'NF',           81, ...
    'Ht',           5, ...
    'Hq',           10, ...
    'dt',           1e-4, ...
    'calS',         1, ...
    'tmax',         0.06, ...
    'lambda',       5e3, ...
    'NumSave',      100 ...
);
int = setInitialCondition(options, params, 'IntCond', 'semicirc');
Z_int = convertInitialCondition(int.X, 'endpiece'); 

% add end piece code to path
addpath(genpath('./endpiece_code'));

% define parameters for endpiece code
k   = 4*pi;
ell = 1;
mu  = 1;
L   = 1;
eps = 0.01;

tmax     = params.tmax;
showProg = 1;

Q      = length(Z_int)-3;
H_trac = params.Ht;
H_quad = params.Hq;

calS = params.calS;
calM = params.calM2;

% hax = [2;1.6;1]/45;
hax = params.head_ax;

% initialise endpiece simulation
model = CreateModelStructure(calS,k,calM,Q,H_trac,H_quad,tmax,'constant',ell, ...
    [],L,eps,mu,Z_int,hax);
model.disc.quad = H_quad;
model.disc.trac = H_trac;

% % assign head matching spermXRelaxingConvergence() 
% model.y  = params.y_ref;
% model.Y  = params.Y_ref;
% model.X0 = params.X0_ref;

% solve problem 
out = SolveSwimmingProblem(tmax, Z_int, model, showProg);
sol = out.sol;

% save solutions to file
str = sprintf('./convergence/relaxing_cell_data/endpiece_N=%g.mat',Q+1);
save(str, 'sol', 'model');
fprintf('Solutions saved to %s\n', str);

% reset paths
addFilesToPath;
printLinebreak;

end % function
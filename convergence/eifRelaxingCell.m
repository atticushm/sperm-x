function eifRelaxingCell()

% runs a high resolution relaxing cell simulation using EIF code

printLinebreak;
clear all;
addFilesToPath

% generate initial condition equal to that in spermXRelaxingConvergence()
problem = 'RelaxingCell';
options = setOptions(problem, ...
    'BodyType',     'sunanda', ...
    'NonLocal',     true, ...
    'StiffModel',   'constant', ...
    'ForcePlanar',  true, ...
    'PlaneOfBeat',  'xy' ...
);
params = setParameters(options, ...
    'ittol',        1e-5, ...
    'NF',           81, ...
    'Ht',           5, ...
    'Hq',           10, ...
    'dt',           1e-4, ...
    'calS',         1, ...
    'tmax',         0.06, ...
    'lambda',       5e3, ...
    'NumSave',      100 ...
);
int = setInitialCondition(options, params, 'IntCond', 'semicirc');
Z_int = convertInitialCondition(int.X, 'eif'); 

% add EIF code to path
addpath(genpath('./convergence/eif_ext'));

% define parameters for EIF code
head_axes = params.head_ax;
X0 = int.X0;
calS = 1;
act_type = 'Worm';
act_params = [0,4*pi];
Q = length(Z_int)-2;
discr_params = [params.Hq, params.Ht, Q];

% generate model structure
model = RUN_ModelParams(head_axes, X0, calS, act_type, act_params, discr_params);

% run simulation
[sol, tps] = RUN_SwimmingSperm(model, Z_int, params.tmax);

% save solutions to file
str = sprintf('./convergence/relaxing_cell_data/eif_N=%g.mat',Q+1);
save(str, 'sol', 'model');
fprintf('Solutions saved to %s\n', str);

% reset formatting
format short

% reset paths
addFilesToPath;
printLinebreak;

end % function
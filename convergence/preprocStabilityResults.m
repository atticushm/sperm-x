function preprocStabilityResults(type)

% preprocess solution data from stability testing batch function
% type is either 'wo', or 'w', for 'without' or 'with' twist terms

printLinebreak;

switch type
    case 'wo'
        prefix = './convergence/stability_data/wotwist_';
    case 'w'
        prefix = './convergence/stability_data/wtwist_';
    otherwise
        warning('No type specified?')
        return
end

% from spxTemporalStability
calS       = 14;
dt_vals    = logspace(-4,0,40);
calM2_vals = round(linspace(0.01, 0.025, 15),3);

num_dt = length(dt_vals);           % number of dt values considered
num_calM2 = length(calM2_vals);     % number of calM values considered

fprintf('Loading data...')

%% preallocate logical matrices for convergence

% logical matrix for stability
stable = logical(zeros(num_dt, num_calM2));

% arclength values at tmax
arcL = zeros(num_dt, num_calM2);

% walltime values for each pair
wtime = zeros(num_dt, num_calM2);

%% load data
% we want to load and sort data by calM values

for i = 1:num_dt
    % zip file name
    zname{i} = sprintf([prefix,'%g.zip'], i);

    % process data
    if exist(zname{i},'file')
        
        % unzip file
        unzip(zname{i}, './convergence/stability_data')
        
        % load each .mat and sort data
        for j = 1:num_calM2
            mname = sprintf([prefix,'%g_%g_calS=%g.mat'], i,j,calS);
            load(mname,'varargin')
            out = varargin{1};
            
            % check if simulation completed successfully
            if strcmpi(out(end).exit, 'OK')  % simulation did not fail
                stable(i,j) = true;
            else
                stable(i,j) = false;
            end
            
            % record walltime (if recorded)
            if length(varargin)>3
                wtime(i,j) = varargin{4};
            end
            
            % compute arclength at t=tmax
            arcL(i,j) = calculateArclength(out(end).X);
           
        end
        
        % delete loose files (to keep folder tidy!)
        delete convergence/stability_data/*.mat
    end
end

end % function
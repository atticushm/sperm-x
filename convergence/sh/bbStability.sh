#!/bin/bash 
#SBATCH --ntasks 12
#SBATCH --time 10-00:00:0 
#SBATCH --qos bbdefault 
#SBATCH --mail-type NONE
#SBATCH --mem 100G  

set -e 

module purge; module load bluebear 
module load MATLAB/2020a 

cd ~/02_SPERMX/git/sperm-x/convergence/
matlab -nodisplay -r "spxTemporalStability"
function spxLambdaConvergence()

% runs a series of relaxing filament simulations with varying lambda
% (tension damping parameter)

% values for lambda
l_vals = [0,logspace(0,4,15)];
num_l  = length(l_vals);

% run simulations
problem = 'RelaxingFilament';
% parfor i = 1:num_l
for i = 1:num_l
    
    % set options, parameters and initial condition
    options = setOptions(problem, ...
        'BodyType',     'none', ...
        'NonLocal',     true, ...
        'StiffFunc',    'constant', ...
        'ForcePlanar',  true, ...
        'PlaneOfBeat',  'xy' ...
    );
    params = setParameters(options, ...
        'ittol',        1e-6, ...
        'NF',           201, ...
        'dt',           1e-4, ...
        'tmax',         0.06, ...
        'lambda',       l_vals(i), ...
        'NumSave',      100 ...
    );
    int = setInitialCondition(options, params, 'IntCond', 'semicirc');
    
    % run simulation
    out = runSPX(int, params, options);
    
    % bundle data and save
    str = sprintf('./convergence/figures/relaxing_filament_data/lambda=%g_tmax=%.1d.mat', l_vals(i), params.tmax);
    parsave(str, out, params, options);
    
end

end % function
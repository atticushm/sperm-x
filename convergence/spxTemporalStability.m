function spxTemporalStability(it, ittot)

% function for assessing the largest allowable time steps given fixed calS
% and varying calM2, for both method w/ and w/o twist 

close all; 

% setup folders
if ~exist('./convergence/stability_data/','dir')
    mkdir('./convergence/stability_data/')
end

%% set options and parameters common to both tests

% options and parameters for SPX
problem = 'SwimmingCell';
options = setOptions(problem, ...
    'BodyType',     'gskb1', ...
    'NonLocal',     true, ...
    'StiffFunc',    'varying', ...
    'SilentMode',   true, ...
    'ForcePlanar',  false ...
);

% values for calM2 and dt
dt_vals = logspace(-4,0,20);
calM2_vals = round(linspace(0.01, 0.025, 10),3);

% ~~~ in an effort to get some results, reduce lower bound of dt ~~~~~
dt_vals = logspace(-3,0,20);

%% split into itot batches

% combine all variables
par_combs = combvec(dt_vals, calM2_vals);
num_combs = size(par_combs,2);

% determine number of simulations per block
sims_per_block = ceil(num_combs/ittot);

%% w/o twist

% disable mechanical twist modelling
options.MechTwist = false;

% set limits of loop, depending on which block has been called
start = (it-1)*sims_per_block+1;
if it < ittot
    fin = it*sims_per_block;
else
    fin = num_combs;        % in case num_combs doesn't divide cleanly by itot
end

% run simulations 
for j = start:fin
    
    % select parameters
    dtj    = par_combs(1,j);
    calM2j = par_combs(2,j);
    
    % set parameters and initial condition
    parj = setParameters(options, ...
        'NF',           201, ...
        'Ht',           5, ...
        'Hq',           10, ...
        'calS',         16, ...
        'dt',           dtj, ...
        'calM2',        calM2j, ...
        'NumBeat',      0.5, ...
        'tmax',         'auto', ...
        'lambda',       200, ...
        'NumSave',      50 ...
    );
    parj.warm_up = 0;
    int = setInitialCondition(options, parj, 'IntCond', 'line');

    % run simulation
    tic;
    out = runSPX(int, parj, options);
    stime = toc;

    % save data
    str = sprintf('./convergence/stability_data/wotwist_%02g_of_%02g_%02g_calS=%g.mat', ...
        it, ittot, j, parj.calS);
    parsave(str, out, parj, options, stime);
 
end


%% w/ twist

% need to investigate appropriate choices for Gamma and calT first!
%{
% enable mechanical twist modelling
options.MechTwist = true;

% run simulations
parfor i = 1:num_dt
    
    % select dt
    dti = dt_vals(i);
    
    for j = 1:num_calM2
        % set parameters
        parj = params;
        parj.dt = dti;
        parj.calM2 = calM2_vals(j);
        
        % run simulation
        out = runSPX(int, parj, options);
        
        % save data
        str = sprintf('./convergence/stability_data/wtwist_%g_%g_calS=%g.mat', ...
            i, j, parj.calS);
        parsave(str, out, parj, options);
    end
end
%}

end % function
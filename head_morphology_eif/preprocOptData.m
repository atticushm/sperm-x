function preprocOptData()

% preprocess all data relating to optimisation of calM for each fixed
% volume head, at a variety of calS values

printLinebreak

% detect number of block simulations
folders = dir('./head_morphology_eif/data/findOpt*');

% using dir will pick up any zip folders in data dir -- these need
% neglected.
% in this way, the final dname will be findOpt_mean.
ii = 1;
for i = 1:length(folders)
    if ~strcmp(folders(i).name(end-3:end),'.zip')
        dname{ii} = folders(i).name;
        ii = ii+1;
    end
end
num_folders = length(dname)-1;  % minus 1 to remove findOpt_mean; will be treated seperately

%% load data

k = 1;
fprintf('Loading data... ')
for i = 1:num_folders
    
    % determine number of mat files in each folder
    files = dir(['./head_morphology_eif/data/',dname{i},'/findOpt*']);
    num_files = length(files);

    % load
    for j = 1:num_files
        % load workspace
        str = dir(sprintf(['./head_morphology_eif/data/',dname{i},'/*_%03g_*'],j));
        load(str.name, 'varargin')

        % extract values
        calMi = varargin{1};
        calSi = varargin{2};
        axi   = varargin{3};

        % store values
        opts(1,k) = calSi;
        opts(2,k) = calMi;
        opts(3,k) = axi(1);
        opts(4,k) = axi(2);
        opts(5,k) = axi(3);

        % increment
        k = k+1;
    end
end
fprintf('complete!\n')

%% load mean cell data

load('./head_morphology_eif/data/findOpt_mean.mat', 'av_optM');
mean_opts = av_optM;

%% sort data

% data is loaded so that axes are fixed for num_calS simulations 

% determine num_calS
calS_vals = unique(opts(1,:));
num_calS  = length(calS_vals);

% number of axes values
num_sims = size(opts,2);
num_ax   = num_sims/num_calS;

% sort into cell structure by calS 
[~,idx] = sort(opts(1,:));
opts = opts(:,idx);
for i = 1:num_calS
    start = (i-1)*num_ax+1;
    fin   = i*num_ax;

    % store
    data{i}.calS = calS_vals(i);
    data{i}.calM = opts(2, start:fin);
    data{i}.a1   = opts(3, start:fin);
    data{i}.a2   = opts(4, start:fin);
    data{i}.a3   = opts(5, start:fin);
end

% save
save_str = './head_morphology_eif/preproc/opt.mat';
save(save_str, 'data')
fprintf(['Preprocessed data saved at ',save_str,'!\n'])

%% completion

printLinebreak

end % function
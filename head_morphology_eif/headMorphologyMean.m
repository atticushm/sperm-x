function headMorphologyMean()

% runs the same simulations as headMorphologyBatch() but with only a mean
% sized head

% check in correct directory (bluebear call will start in subdir, so we 
% need to move up)
dir = pwd;
if strcmpi(dir(end-18:end),'head_morphology_eif')
    cd ..
end
addFilesToPath;
printLinebreak

% add end piece code to path
addpath(genpath('./endpiece_code'));

% folder for workspaces
if ~exist('./head_morphology_eif/data','dir')
    mkdir('./head_morphology_eif/data')
end

%% setup parameters

% define parameters fixed in each simulation
k   = 4*pi;
T   = 2*pi;
ell = 0.95;
mu  = 1;
eps = 0.01;

num_beats = 5;
tmax      = num_beats * T;
showProg  = 0;

Q      = 40;
H_trac = 4;
H_quad = 8;

% define calS, with calM to optimise  
calS_vals = [9, 12, 13, 14, 15, 16, 17, 18];
calM_vals = [0.0366, 0.0198, 0.018, 0.0169, 0.0162, 0.0158, 0.0155, 0.0153];

% set initial condition
X_int = parabolicCurve(1e-5, Q+1);
Z_int = convertInitialCondition(X_int, 'endpiece');

%% generate mean head

% mean flagellum+midpiece length from Cummins 1985
L = meanFlagellumLength;

% ideal length, width and topographical height from Sunanda et al 2018
a1 = 4.6/2/L;
a2 = 2.6/2/L;
a3 = 1.03/L;   
ax = [a1; a2; a3];

% generate spherical discretisations
xt_sph = generateSphereDisc(H_trac, 1);
Nt     = length(xt_sph)/3;
xq_sph = generateSphereDisc(H_quad, 1);
Nq     = length(xq_sph)/3;

% generate ellipsoidal head discretisation
Ytrac = kron(ax,ones(Nt,1)).*xt_sph;
Yquad = kron(ax,ones(Nq,1)).*xq_sph;
X0 = [ax(1);0;0];
vol = 4/3*pi*a1*a2*a3;

%% run simulations

for s = 1:length(calS_vals)
    
    if ~exist(sprintf('./head_morphology_eif/data/mean_%02g.mat',calS_vals(s)), 'file')
        % choosing swimming/actuation parameters
        calS = calS_vals(s);
        calM = calM_vals(s);
        fprintf('calS = %g, calM = %g\n', calS, calM);

        % build model structure
        model = CreateModelStructure(calS,k,calM,Q,H_trac,H_quad,tmax,'varying',ell, ...
            [],1,eps,mu,Z_int,ax);
        model.disc.quad = H_quad;
        model.disc.trac = H_trac;

        % assign head
        model.y   = Ytrac;
        model.Y   = Yquad;
        model.X0  = X0;
        model.vol = vol;

        % solve problem
        out = SolveSwimmingProblem(tmax, Z_int, model, showProg);

        % save
        str = sprintf('./head_morphology_eif/data/mean_%02g.mat', calS);
        parsave(str, out, model)
    end
        
end % calS loop

% remove endpiece code from path
addFilesToPath;

%% preprocess data 

num_calS = length(calS_vals);

% load data
for i = 1:num_calS
    load(sprintf('./head_morphology_eif/data/mean_%02g.mat',calS_vals(i)),'varargin')
    
    % assign
    sol{i} = varargin{1}.sol;
    mdl{i} = varargin{2};
    vol(i) = computeBodyVolume(mdl{i});
end

% sample over established beat
% determine coordinates at fixed time values
tmax  = mdl{1}.tmax;
tint  = 20;
tps   = 0 : 2*pi/tint : tmax;
num_t = length(tps);

% time points corresponding to start of each beat
[~,beat_idx] = find(mod(tps,2*pi) < 1e-2);

% consider data over an established beat
tbeat_idx = beat_idx(3) : beat_idx(4);
tbeat = tps(tbeat_idx);
num_tbeat = length(tbeat);

% compute coordinates of head and flagellum over the beat
fprintf('Computing coordinates... ');
addpath(genpath('./endpiece_code'))
for i = 1:num_calS

    % solution vectors at tbeat values
    Z{i} = deval(sol{i}, tbeat);

    % coordinate data at each tbeat value
    for k = 1:num_tbeat
        [head,flag,~] = GetProblemData(Z{i}(:,k), mdl{i});
        Y{i,k} = head.XQuad; 
        X{i,k} = flag.X;
    end
end
addFilesToPath;
fprintf('complete!\n')

%% compute metrics

fprintf('Computing metrics... ')
for i = 1:num_calS
        
    % compute VAL
    VAL(i) = calculateVAL(Z{i}, tbeat, 'endpiece');

    % compute average work
    addpath(genpath('./endpiece_code'))
    for l = 1:num_tbeat
        work(l) = calculateWork(tbeat(l), Z{i}(:,l), mdl{i});
    end
    Wav(i) = mean(work);
    rmpath(genpath('./endpiece_code'))

    % compute lighthill efficiency
    eta(i) = VAL(i)^2/Wav(i);

    % compute mean absolute curvature
    for l = 1:num_tbeat
        kap(:,l) = abs(calcCurvatureEPC(Z{i}(:,l)));
    end
    mkap{i} = mean(kap,2);

    % abs difference in tangent of max and min head angle
    tyaw(i) = abs(tan(max(Z{i}(3,:)))-tan(min(Z{i}(3,:))));
    
    % abs difference in max and min head angle
    ryaw(i) = max(Z{i}(3,:))- min(Z{i}(3,:));

    % solve resistance problem to compute GRM for head at t=0
    GRM{i} = solveResistanceProblem(Ytrac, Yquad, X0, 1e-2, 1, 'stokeslets');
end
fprintf('complete!\n')

% add to data
data.X   = X;
data.Y   = Y;
data.mdl = mdl;
data.Z   = Z;
data.VAL = VAL;
data.Wav = Wav;
data.eta = eta;
data.mkap = mkap; 
data.tyaw = tyaw;
data.ryaw = ryaw;
data.ax = ax;
data.Yq = Yquad;
data.GRM = GRM;

% save
save('./head_morphology_eif/preproc/avcell.mat')
printLinebreak

end % function
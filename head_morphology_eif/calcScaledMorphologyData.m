function calcScaledMorphologyData(data, str, info)

% X contains flagellum coordinates
% Y contains head coordinates
% Z contains EIF solution data
% model contains model parameters

% extract solution data
X = data.X;
Y = data.Y;
Z = data.Z;
model = data.model;
vol = data.vol;

% extract variables
num_calS = info.num_calS;
num_V = info.num_V;
tps = info.tps;
ntps = length(tps);

fprintf('Computing %s info...', str)
for i = 1:num_calS
    for j = 1:num_V
        
        % compute VAL
        VAL(i,j) = calculateVAL(Z{i,j}, tps, 'endpiece');
        
        % compute average work
        addpath(genpath('./endpiece_code'))
        for l = 1:ntps
            work(l) = calculateWork(tps(l), Z{i,j}(:,l), model{i,j});
        end
        Wav(i,j) = mean(work);
        rmpath(genpath('./endpiece_code'))
        
        % compute lighthill efficiency
        eta(i,j) = VAL(i,j)^2/Wav(i,j);
        
        % compute mean absolute curvature
        for l = 1:ntps
            kap(:,l) = abs(calcCurvatureEPC(Z{i,j}(:,l)));
        end
        mkap{i,j} = mean(kap,2);
        
        % range of yaw of head equiv. to relative angular envelope of
            % flagellum
        ryaw(i,j) = max(Z{i,j}(3,:)) - min(Z{i,j}(3,:));
        
        % abs difference in tangent of max and min head angle
        tyaw(i,j) = abs(tan(max(Z{i,j}(3,:)))-tan(min(Z{i,j}(3,:))));
        
        % input data is not at t=0 -> generate reference head from axis
        % values. use odd numbers for projection to ensure values in
        % the 0-axes.
        ax{i,j} = [model{i,j}.swimmer.a1; ...
                   model{i,j}.swimmer.a2; ...
                   model{i,j}.swimmer.a3];                        
        [Ytrac,Yquad,X0,~] = generateBodyNodes(7,19,'human','custom',ax{i,j});
        
        % save head quadrature discretisation at t=0
        Yq{i,j} = Yquad;

        % solve resistance problem to compute GRM for head at t=0
        GRM{i,j} = solveResistanceProblem(Ytrac, Yquad, X0, 1e-2, 1, 'stokeslets');
        
        % ratio measures
        R11R22(i,j) = GRM{i,j}(1,1)/GRM{i,j}(2,2);
        R11R66(i,j) = GRM{i,j}(1,1)/GRM{i,j}(6,6);
        
    end
end
fprintf(' complete!\n')

% add to data
data.VAL = VAL;
data.Wav = Wav;
data.eta = eta;
data.mkap = mkap; 
data.ryaw = ryaw; 
data.tyaw = tyaw;
data.ax = ax;
data.Yq = Yq;
data.GRM = GRM;
data.R11R22 = R11R22;
data.R11R66 = R11R66;

% save
switch str
    case 'ell'
        ell = data;
        save('./head_morphology_eif/preproc/ell.mat', 'ell');
        
    case 'rnd'
        rnd = data;
        save('./head_morphology_eif/preproc/rnd.mat', 'rnd');
        
    case 'tap'
        tap = data;
        save('./head_morphology_eif/preproc/tap.mat', 'tap');
end

end % function
function findOptimalCalM(method, it, ittot)

% finds the optimal calM for a swimming cell simulation 

% check in correct directory (bluebear call will start in subdir, so we 
% need to move up)
% dir = pwd;
% if strcmpi(dir(end-18:end),'head_morphology_eif')
%     cd ..
% end
% addFilesToPath;
printLinebreak('#')

% add end piece code to path
addpath(genpath('./endpiece_code'));

% calS values to consider
calS_vals = 9:0.5:18;
num_calS  = length(calS_vals);

% folder for workspaces
if ~exist('./head_morphology_eif/data','dir')
    mkdir('./head_morphology_eif/data')
end

% calM bounds -- guess from Neal et al
% first row is lower bounds, second row upper bounds with each column for
% each calS
calM_bnds = repmat([0.01;0.045], 1, num_calS);
         
% mean flagellum+midpiece length from Cummins 1985
L = meanFlagellumLength();

% options for EPC
H_trac = 5;

% options for fminbnd
fminbnd_opts = optimset('Display','iter');

%% average shaped head

% compute on first batch call 
if (it==1)

    fprintf('Calculating optimum calM for average cell...')

    % ideal length, width and topographical height from Sunanda et al 2018
    a1 = 4.6/2/L;
    a2 = 2.6/2/L;
    a3 = 1.03/L;   
    av_ax = [a1; a2; a3];

    % run simulations
    for i = 1:length(calS_vals)

        % select calS
        calSi = calS_vals(i);

        % set function
        func = @(x) runEPC(x, calSi, av_ax);

        % use initial guesses for calM bounds. Large values with large calS may
        % cause code to fail (self intersection) -- so, catch and reduce ub
        exit = 'fail';
        lb   = calM_bnds(1,1);
        ub   = calM_bnds(2,1);
        while strcmpi(exit,'fail')
            try
                av_optM(i) = fminbnd(func, lb, ub, fminbnd_opts);
                exit = 'ok';
    
            catch
                fprintf('Reducing upper bound...\n')
                ub = ub-0.005;
                exit = 'fail';
            end
        end
    end
    save('head_morphology_eif/data/findOpt_mean.mat','calS_vals','av_optM')
    fprintf(' complete!\n')
    
end

%% generate fixed volume ellipsoidal heads

% generate heads
fprintf('Generating head axes... ')
switch method
    case 'fixed'
        % generate heads from fixed volume
        num_per_dir  = 10;
        num_per_case = num_per_dir^2;
        num_case     = 3;
        num_heads    = num_case * num_per_case;
        [~,~,ax,~]   = generateEllipsoidalHeads(num_heads, H_trac);
        num_ax       = size(ax,2);

    case 'scaled'
        % generate heads from scaling volume (includes abnormal heads)
        [~,~,ax,~,~] = generateScaledHeads(H_trac);
        num_ax       = size(ax,2);
        
    otherwise
        warning('head generation type not specified!')
        return
end
fprintf('complete!\n')
printLinebreak

% combine variables
vars = combvec(calS_vals, [ax{:}]);
num_sims = size(vars,2);

% augment vars vector with calM bound guesses
vars = [vars; kron(ones(1,num_ax),calM_bnds)];

%% split into ittot batches

% determine number of simulations per batch
sims_per_block = ceil(num_sims/ittot);

% set limits of simulation loop depending on which block has been called
start = (it-1)*sims_per_block+1;
if it < ittot
    fin = it*sims_per_block;
else
    fin = num_sims;        % in case num_combs doesn't divide cleanly by itot
end

% set fin override (to mop up missing simulations should bluebear time out)
ovr = 34;
if ~isempty(ovr)
    fin = start+ovr;
end

%% run optimisations

parfor i = start:fin
    
    % select calS
    calSi = vars(1,i);
    
    % select axis
    axi = vars(2:4,i);
    
    % update cmd window
    fprintf('calS=%g, a1=%.3g, a2=%.3g, a3=%.3g\n', ...
        calSi, axi(1), axi(2), axi(3));
        
    % set func
    func = @(x) runEPC(x, calSi, axi);

    % use initial guesses for calM bounds. Large values with large calS may
    % cause code to fail (self intersection) -- so, catch and reduce ub
    exit = 'fail';
    lb   = vars(5);
    ub   = vars(6);
    while strcmpi(exit,'fail')
        try
            optM = fminbnd(func, lb, ub, fminbnd_opts);
            exit = 'ok';

        catch
            fprintf('Reducing upper bound...\n')
            ub = ub-0.005;
            exit = 'fail';
        end
    end
            
    % save data
    count = i-(start-1);
    str = sprintf('./head_morphology_eif/data/findOpt_%02g_of_%02g_%03g_calS=%02g.mat', ...
        it, ittot, count, calSi);
    parsave(str, optM, calSi, axi);
    printLinebreak
    
end
%}
end % function
function iVAL = runEPC(calM, calS, ax)

% runs a swimming cell simulation like in headMorphologyBatch, for a given
% calM, calS and axes, computes the VAL, and returns the inverse VAL for
% minimisation by optimisation function fminbnd

% printLinebreak

%% setup parameters

% define parameters fixed in each simulation
k   = 4*pi;
T   = 2*pi;
ell = 0.95;
mu  = 1;
eps = 0.01;

num_beats = 5;
tmax      = num_beats * T;
showProg  = 0;

Q      = 40;
H_trac = 4;
H_quad = 8;

% set initial condition
X_int = parabolicCurve(1e-4, Q+1);
Z_int = convertInitialCondition(X_int, 'endpiece');

% generate spherical discretisations
xt_sph = generateSphereDisc(H_trac, 1);
Nt     = length(xt_sph)/3;
xq_sph = generateSphereDisc(H_quad, 1);
Nq     = length(xq_sph)/3;

% generate ellipsoidal head discretisation
Ytrac = kron(ax,ones(Nt,1)).*xt_sph;
Yquad = kron(ax,ones(Nq,1)).*xq_sph;
X0 = [ax(1);0;0];
vol = 4/3*pi*ax(1)*ax(2)*ax(3);

%% setup EPC

% add end piece code to path
addpath(genpath('./endpiece_code'));

% build model structure
model = CreateModelStructure(calS,k,calM,Q,H_trac,H_quad,tmax,'varying',ell, ...
    [],1,eps,mu,Z_int,ax);
model.disc.quad = H_quad;
model.disc.trac = H_trac;

% assign head
model.y   = Ytrac;
model.Y   = Yquad;
model.X0  = X0;
model.vol = vol;

%% solve EPC

% fprintf('Solving swimming problem, calS=%g, calM=%g, a1=%.3g, a2=%.3g, a3=%.3g ...', ...
%     calS, calM, ax(1), ax(2), ax(3));
out = SolveSwimmingProblem(tmax, Z_int, model, showProg);
% fprintf(' complete!\n')

% reset path
addFilesToPath;

%% compute VAL

% solution structure from ode solver
sol = out.sol;

% sample over established beat
% determine coordinates at fixed time values
tint  = 20;
tps   = 0 : 2*pi/tint : tmax;

% time points corresponding to start of each beat
[~,beat_idx] = find(mod(tps,2*pi) < 1e-2);

% consider data over an established beat
tbeat_idx = beat_idx(3) : beat_idx(4);
tbeat = tps(tbeat_idx);

% solution vectors at tbeat values
Z = deval(sol, tbeat);

% compute VAL
% fprintf('Computing VAL...')
VAL = calculateVAL(Z, tbeat, 'endpiece');
iVAL = 1/VAL;
% fprintf(' complete!\n')
% printLinebreak

end % function
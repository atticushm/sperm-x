function generateShFilesFindOpts(method,itot,test)

% generates multiple sh files for running helical beat parameter search
% simulations

% method can be 'fixed' or 'scaled'
% itot is the number of batch functions to make
% test (bool) defines what bluebear qos to use: true=bbshort, false=bbdefault

printLinebreak

% make subfolder for .sh files
if ~exist('./head_morphology_eif/sh/','dir')
    mkdir('./head_morphology_eif/sh/')
end

% generate .sh filenames
for i = 1:itot
    fname{i} = sprintf('./head_morphology_eif/sh/bbFindOpt_%s_%02g_of_%g.sh', ...
       method, i, itot);
end

% write to each file
for i = 1:itot
   
    % open file for writing
    fID = fopen(fname{i},'w+');
    
    % print lines to files
    fprintf(fID, '#!/bin/bash\n');
    fprintf(fID, '#SBATCH --ntasks 12\n');
    if test
        fprintf(fID, '#SBATCH --time 0-00:10:0\n');
        fprintf(fID, '#SBATCH --qos bbshort\n');
    else
        % default should be 8 days -- reduce manually if using fin_ovr in
        % optimisation function (more likely to run sooner)
        fprintf(fID, '#SBATCH --time 2-00:00:0\n');
        fprintf(fID, '#SBATCH --qos bbdefault\n');
    end
    fprintf(fID, '#SBATCH --mail-type NONE\n');
    fprintf(fID, '#SBATCH --mem 100G\n\n');
    
    fprintf(fID, 'set -e\n\n');
    fprintf(fID, 'module purge; module load bluebear\n');
    fprintf(fID, 'module load MATLAB/2020a\n\n');
    
    % cd to code on bluebear directory and run
    fprintf(fID, 'cd ~/02_SPERMX/git/sperm-x/\n');
    fprintf(fID, 'matlab -nodisplay -r "addFilesToPath;findOptimalCalM(''%s'',%g,%g)"\n', method, i, itot);
    
    % close file
    fclose(fID);

    % announce to command line
    fprintf('File written to %s\n',fname{i})
end
printLinebreak;

end % function
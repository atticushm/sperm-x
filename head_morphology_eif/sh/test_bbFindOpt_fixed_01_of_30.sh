#!/bin/bash
#SBATCH --ntasks 12
#SBATCH --time 0-00:10:0
#SBATCH --qos bbshort
#SBATCH --mail-type NONE
#SBATCH --mem 100G

set -e

module purge; module load bluebear
module load MATLAB/2020a

cd ~/02_SPERMX/git/sperm-x/
matlab -nodisplay -r "addFilesToPath;findOptimalCalM('fixed',1,30)"

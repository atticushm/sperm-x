function plotFixedVolumeEcc()

printLinebreak;
close all;

calS_vals = [9,12,15,18];

% load all preprocessed data
data1 = load('head_morphology_eif/preproc/a1a2.mat');
data2 = load('head_morphology_eif/preproc/a1a3.mat');
data3 = load('head_morphology_eif/preproc/a2a3.mat');

% extract all eccentricty data, as vectors
d1_epara = [data1.msh{1}.epara(:)];
d2_epara = [data2.msh{1}.epara(:)];
d3_epara = [data3.msh{1}.epara(:)];
d1_eperp = [data1.msh{1}.eperp(:)];
d2_eperp = [data2.msh{1}.eperp(:)];
d3_eperp = [data3.msh{1}.eperp(:)];

% stack all eccentricity data
epara = [d1_epara; d2_epara; d3_epara];
eperp = [d1_eperp; d2_eperp; d3_eperp];

% ratios
d1_re = d1_epara./d1_eperp;
d2_re = d2_epara./d2_eperp;
d3_re = d3_epara./d3_eperp;
re = epara./eperp;

% extract all VAL data, as vectors, for each calS
num_S = size(data1.model,1);
for i = 1:num_S
    d1_VAL{i} = [data1.msh{i}.VAL(:)];
    d2_VAL{i} = [data2.msh{i}.VAL(:)];
    d3_VAL{i} = [data3.msh{i}.VAL(:)];
end

% stack all VAL data
for i = 1:num_S
    VAL{i} = [d1_VAL{i}; d2_VAL{i}; d3_VAL{i}];
end

% extract all Wav data, as vectors, for each calS
for i = 1:num_S
    d1_Wav{i} = [data1.msh{i}.W_av(:)];
    d2_Wav{i} = [data2.msh{i}.W_av(:)];
    d3_Wav{i} = [data3.msh{i}.W_av(:)];
end

% stack all Wav data
for i = 1:num_S
    Wav{i} = [d1_Wav{i}; d2_Wav{i}; d3_Wav{i}];
end

% extract all yaw range data, as vectors, for each calS
for i = 1:num_S
    d1_yaw{i} = [data1.msh{i}.ryaw(:)];
    d2_yaw{i} = [data2.msh{i}.ryaw(:)];
    d3_yaw{i} = [data3.msh{i}.ryaw(:)];
end

%% set figure

% latex and font size for tick labels
set(groot, 'defaultAxesTickLabelInterpreter','latex');
set(groot, 'defaultAxesFontSize',14);

% colors
d1_col = [1,0,0];
d2_col = [0,0.5,0];
d3_col = [0,0,1];

%% VAL

figure; tiledlayout(num_S, 3, 'TileSpacing','compact')
for i = 1:num_S
    % epara vs VAL
    nexttile; 
    % scatter(epara, VAL{i}, '.'); box on;
    scatter(d1_epara, d1_VAL{i}, '.'); box on; hold on;
    scatter(d2_epara, d2_VAL{i}, '.'); 
    scatter(d3_epara, d3_VAL{i}, '.');
    xlabel('$e_{\parallel}$','FontSize',14,'Interpreter','latex');
    ylabel('VAL','FontSize',14,'Interpreter','latex');
    
    % eperp vs VAL
    nexttile; 
    % scatter(eperp, VAL{i}, '.'); box on;
    scatter(d1_eperp, d1_VAL{i}, '.'); box on; hold on;
    scatter(d2_eperp, d2_VAL{i}, '.'); 
    scatter(d3_eperp, d3_VAL{i}, '.');
    xlabel('$e_{\perp}$','FontSize',14,'Interpreter','latex');
    ylabel('VAL','FontSize',14,'Interpreter','latex');
    
    % e ratio vs VAL
    nexttile; 
    % scatter(re, VAL{i}, '.'); box on;
    scatter(d1_re, d1_VAL{i}, '.'); box on; hold on;
    scatter(d2_re, d2_VAL{i}, '.'); 
    scatter(d3_re, d3_VAL{i}, '.');
    xlabel('$e_{\parallel}/e_{\perp}$','FontSize',14,'Interpreter','latex');
    ylabel('VAL','FontSize',14,'Interpreter','latex');
end

% scale and save
set(gcf,'Position',[613 178 633 769])
save2pdf('./head_morphology_eif/figures/ecc_VAL.pdf')

% 3D VAL scatter
figure; tiledlayout(1,num_S,'TileSpacing','compact');
for i = 1:num_S
    nexttile;
    
    % plots
    scatter3(d1_epara, d1_eperp, d1_VAL{i},'.'); box on; hold on;
    scatter3(d2_epara, d2_eperp, d2_VAL{i},'.'); 
    scatter3(d3_epara, d3_eperp, d3_VAL{i},'.');
    
    % labels
    xlabel('$e_{\parallel}$','FontSize',14,'Interpreter','latex');
    ylabel('$e_{\perp}$','FontSize',14,'Interpreter','latex');
    zlabel('VAL','FontSize',14,'Interpreter','latex');
    title_str = sprintf('$\\mathcal{S}=%g$',calS_vals(i));
    title(title_str, 'FontSize',14,'Interpreter','latex')
    
    % view
    set(gca,'View',[131.3804 15.5848]);
end

% scale and save
set(gcf,'Position',[134 665 1227 282]);
save2pdf('./head_morphology_eif/figures/ecc_VAL_3D.pdf')

%% W_av

figure; tiledlayout(num_S, 3, 'TileSpacing','compact')
for i = 1:num_S
    % epara vs Wav
    nexttile; 
    % scatter(epara, Wav{i}, '.'); box on;
    scatter(d1_epara, d1_Wav{i}, '.'); box on; hold on;
    scatter(d2_epara, d2_Wav{i}, '.'); 
    scatter(d3_epara, d3_Wav{i}, '.');
    xlabel('$e_{\parallel}$','FontSize',14,'Interpreter','latex');
    ylabel('$|W|$','FontSize',14,'Interpreter','latex');
    
    % eperp vs Wav
    nexttile; 
    % scatter(eperp, Wav{i}, '.'); box on;
    scatter(d1_eperp, d1_Wav{i}, '.'); box on; hold on;
    scatter(d2_eperp, d2_Wav{i}, '.'); 
    scatter(d3_eperp, d3_Wav{i}, '.');
    xlabel('$e_{\perp}$','FontSize',14,'Interpreter','latex');
    ylabel('$|W|$','FontSize',14,'Interpreter','latex');
    
    % e ratio vs Wav
    nexttile; 
    % scatter(re, Wav{i}, '.'); box on;
    scatter(d1_re, d1_Wav{i}, '.'); box on; hold on;
    scatter(d2_re, d2_Wav{i}, '.'); 
    scatter(d3_re, d3_Wav{i}, '.');
    xlabel('$e_{\parallel}/e_{\perp}$','FontSize',14,'Interpreter','latex');
    ylabel('$|W|$','FontSize',14,'Interpreter','latex');
end

% scale and save
set(gcf,'Position',[613 178 633 769])
save2pdf('./head_morphology_eif/figures/ecc_Wav.pdf')

% 3D W_av scatter
figure; tiledlayout(1,num_S,'TileSpacing','compact');
for i = 1:num_S
    nexttile;
    
    % plots
    scatter3(d1_epara, d1_eperp, d1_Wav{i},'.'); box on; hold on;
    scatter3(d2_epara, d2_eperp, d2_Wav{i},'.'); 
    scatter3(d3_epara, d3_eperp, d3_Wav{i},'.');
    
    % labels
    xlabel('$e_{\parallel}$','FontSize',14,'Interpreter','latex');
    ylabel('$e_{\perp}$','FontSize',14,'Interpreter','latex');
    zlabel('$|W|$','FontSize',14,'Interpreter','latex');
    title_str = sprintf('$\\mathcal{S}=%g$',calS_vals(i));
    title(title_str, 'FontSize',14,'Interpreter','latex')
    
    % view
    set(gca,'View',[131.3804 15.5848]);
end

% scale and save
set(gcf,'Position',[134 665 1227 282]);
save2pdf('./head_morphology_eif/figures/ecc_Wav_3D.pdf')

%% yaw range

figure; tiledlayout(num_S, 3, 'TileSpacing','compact')
for i = 1:num_S
    % epara vs yaw
    nexttile; 
    % scatter(epara, Wav{i}, '.'); box on;
    scatter(d1_epara, d1_yaw{i}, '.'); box on; hold on;
    scatter(d2_epara, d2_yaw{i}, '.'); 
    scatter(d3_epara, d3_yaw{i}, '.');
    xlabel('$e_{\parallel}$','FontSize',14,'Interpreter','latex');
    ylabel('$|\phi_{\max} - \phi_{\min}|$','FontSize',14,'Interpreter','latex');
    % xlim([1,3]); ylim([0.3, 1.3])
    
    % eperp vs yaw
    nexttile; 
    % scatter(eperp, Wav{i}, '.'); box on;
    scatter(d1_eperp, d1_yaw{i}, '.'); box on; hold on;
    scatter(d2_eperp, d2_yaw{i}, '.'); 
    scatter(d3_eperp, d3_yaw{i}, '.');
    xlabel('$e_{\perp}$','FontSize',14,'Interpreter','latex');
    ylabel('$|\phi_{\max} - \phi_{\min}|$','FontSize',14,'Interpreter','latex');
    % xlim([1,4]); ylim([0.3,1.3])
    
    % e ratio vs yaw
    nexttile; 
    % scatter(re, Wav{i}, '.'); box on;
    scatter(d1_re, d1_yaw{i}, '.'); box on; hold on;
    scatter(d2_re, d2_yaw{i}, '.'); 
    scatter(d3_re, d3_yaw{i}, '.');
    xlabel('$e_{\parallel}/e_{\perp}$','FontSize',14,'Interpreter','latex');
    ylabel('$|\phi_{\max} - \phi_{\min}|$','FontSize',14,'Interpreter','latex');
    % xlim([0,1.5]); ylim([0.3,1.3])
end

% scale and save
set(gcf,'Position',[613 178 633 769])
save2pdf('./head_morphology_eif/figures/ecc_yaw.pdf')

% 3D yaw scatter
figure; tiledlayout(1,num_S,'TileSpacing','compact');
for i = 1:num_S
    nexttile;
    
    % plots
    scatter3(d1_epara, d1_eperp, d1_yaw{i},'.'); box on; hold on;
    scatter3(d2_epara, d2_eperp, d2_yaw{i},'.'); 
    scatter3(d3_epara, d3_eperp, d3_yaw{i},'.');
    
    % labels
    xlabel('$e_{\parallel}$','FontSize',14,'Interpreter','latex');
    ylabel('$e_{\perp}$','FontSize',14,'Interpreter','latex');
    zlabel('$|\phi_{\max} - \phi_{\min}|$','FontSize',14,'Interpreter','latex');
    title_str = sprintf('$\\mathcal{S}=%g$',calS_vals(i));
    title(title_str, 'FontSize',14,'Interpreter','latex')
    
    % view
    set(gca,'View',[131.3804 15.5848]);
end

% scale and save
set(gcf,'Position',[134 665 1227 282]);
save2pdf('./head_morphology_eif/figures/ecc_yaw_3D.pdf')

%% matching extremes of eccentricity to head shapes

% max(epara) comes from a2a3 variation data
[idx,idy] = findIndex(data3.msh{1}.epara, 'max');       % find index in array of max(epara) value
max_ax = [data3.msh{1}.model{idx,idy}.swimmer.a1; ...   % axes for max(epara) head
          data3.msh{1}.model{idx,idy}.swimmer.a2; ...
          data3.msh{1}.model{idx,idy}.swimmer.a3];
max_y  = data3.msh{1}.model{idx,idy}.y;                  % (traction) nodes for max(epara) head

% two similar min(epara) values from from a1a3 (low VAL) and a2a3 (higher
% VAL). extract these and compare
[idx,idy] = findIndex(data2.msh{1}.epara, 'min');
min_ax_1 = [data2.msh{1}.model{idx,idy}.swimmer.a1; ...  
            data2.msh{1}.model{idx,idy}.swimmer.a2; ...
            data2.msh{1}.model{idx,idy}.swimmer.a3];
min_y_1  = data2.msh{1}.model{idx,idy}.y;

[idx,idy] = findIndex(data3.msh{1}.epara, 'min');
min_ax_2 = [data3.msh{1}.model{idx,idy}.swimmer.a1; ...  
            data3.msh{1}.model{idx,idy}.swimmer.a2; ...
            data3.msh{1}.model{idx,idy}.swimmer.a3];
min_y_2  = data3.msh{1}.model{idx,idy}.y;

% plot these heads

%% complete

close all;

end % function
function plotFixedVolume(id)

% plot results from data where ax1, ax2 are chosen from equispaced values,
% and ax3 is determined to ensure fixed healthy cell volume.

close all;
printLinebreak

warning off % to quell label interpreter error

%% load and extract

% load preprocessed data
path = ['./head_morphology_eif/preproc/',id,'.mat'];
if ~exist(path,'file')
    frintf('Please run preprocFixedVolResults!\n')
    return
else
    fprintf('Loading preprocessed data...');
    load(path);
    fprintf(' complete!\n');
end

% time values
tbeat = info.tbeat;

% number of calS values
num_calS = length(model);

% calS values
for i = 1:num_calS
    calS_vals(i) = model{i}{1}.swimmer.calS;
end

%% load in simulation data from cell swimming with mean head size

% load
path_avcell = './head_morphology_eif/preproc/avcell.mat';
load(path_avcell,'data');
avcell = data;

% load tricolor colormap
load('tools/tricolor2.mat','tricolor2')

%% set figure

% latex and font size for tick labels
set(groot, 'defaultAxesTickLabelInterpreter','latex');
set(groot, 'defaultAxesFontSize',14);

% folder for figures
if ~exist(['./head_morphology_eif/figures/',id], 'dir')
    mkdir(['./head_morphology_eif/figures/',id])
end

%% head shapes

% moving from bottom left of VAL plots (i.e. {1,1}) to top right (i.e.
% {end,end})

% same heads from diagonal, and present as morph, viewed from the fixed
% axes

% initiate figure
figure; tiledlayout(1,3,'TileSpacing','compact'); 
num_diag = size(msh{1}.Z,1);

% colors for rings
offset = 2;
col = turbo(num_diag+offset); col = col(2:end-1,:);

% plots
for i = 1:num_diag
    
    % extract axes
    head_ax = msh{1}.ax{i,i};

    % parametrise ellipse
    th = -pi:0.01:pi;
    
    % xy slice (side on)
    nexttile(1);
    x  = head_ax(1)*cos(th); 
    y  = head_ax(2)*sin(th);
    plot(x,y, 'LineWidth',1.2,'Color',col(i,:)); hold on; box on;
    % xlabel('x'); ylabel('y')
    
    % xz slice (top down)
    nexttile(2);
    x  = head_ax(1)*cos(th); 
    y  = head_ax(3)*sin(th);
    plot(x,y, 'LineWidth',1.2,'Color',col(i,:)); hold on; box on;
    % xlabel('x'); ylabel('z')
    
    % yz slice (front on)
    nexttile(3);
    x  = head_ax(3)*cos(th); 
    y  = head_ax(2)*sin(th);
    plot(x,y, 'LineWidth',1.2,'Color',col(i,:)); hold on; box on;
    % xlabel('z'); ylabel('y')
    
end

% scale and save
set(gcf,'Position',[105 725 1015 222]);
save2pdf(['./head_morphology_eif/figures/',id,'/morph.pdf']);
close all

%% VAL plot

% how does VAL change with head dimensions, for fixed calS and calM?

figure;
tiledlayout('flow','TileSpacing','compact');
for i = 1:num_calS
    
    % find percent difference to mean cell VAL
    rel_VAL = (msh{i}.VAL - avcell.VAL(i))/avcell.VAL(i);
    perc_VAL_diff{i} = rel_VAL*100;
    
    % plot
    nexttile;
    surf(XX, YY, perc_VAL_diff{i}, 'EdgeColor','none');
    colormap(tricolor2)
    view(2); axis tight; shading flat

    % set caxis to be symmetric about zero
    caxis([-2,2])

    % add enclosing box
    fbox = gca;
    box on;
    grid off;
    fbox.LineWidth = 0.5;
    fbox.Layer = 'top';
    
    % labels
    switch id
        case 'a1a2'
            xlabel('$a_1$','FontSize',14,'Interpreter','latex');
            ylabel('$a_2$','FontSize',14,'Interpreter','latex');
            xticks([3.8, 4.2, 4.6]*1e-2)
            yticks([2.2, 2.4, 2.6]*1e-2)
        case 'a1a3'
            xlabel('$a_1$','FontSize',14,'Interpreter','latex');
            ylabel('$a_3$','FontSize',14,'Interpreter','latex');
            xticks([3.8, 4.2, 4.6]*1e-2)
            yticks([1.8, 1.9, 2.0]*1e-2)
        case 'a2a3'
            xlabel('$a_2$','FontSize',14,'Interpreter','latex');
            ylabel('$a_3$','FontSize',14,'Interpreter','latex');
            xticks([2.2, 2.4, 2.6]*1e-2)
            yticks([1.8, 1.9, 2.0]*1e-2)
    end
    
    % axes
    xtickformat('%.2f')
    ytickformat('%.2f')
    ax = gca;
    ax.XAxis.Exponent = -2;
    ax.YAxis.Exponent = -2;
    
    % title
    title(sprintf('$\\mathcal{S}=%g$', calS_vals(i)), 'Interpreter','latex');
end

% global colorbar
cbar = colorbar;
cbar.TickLabelInterpreter = 'latex';
cbar.Layout.Tile = 'east';

% scale and save
set(gcf, 'Position', [96 537 870 366])
save2pdf(['./head_morphology_eif/figures/',id,'/VAL.pdf']);
close all

%% eta plot

% how does eta (Lighthill efficiency) change with head dimensions, for fixed calS and calM?

figure;
tiledlayout('flow','TileSpacing','compact');
for i = 1:num_calS
    
    % find percent difference to mean cell VAL
    rel_eta = (msh{i}.eta - avcell.eta(i))/avcell.eta(i);
    perc_eta_diff = rel_eta*100;
    
    % plot
    nexttile;
    surf(XX, YY, perc_eta_diff, 'EdgeColor','none');
    colormap(tricolor2)
    view(2); axis tight; shading flat
    
    % set caxis to be symmetric about zero
    caxis([-2.5,2.5])

    % add enclosing box
    fbox = gca;
    box on;
    grid off;
    fbox.LineWidth = 0.5;
    fbox.Layer = 'top';
    
    % labels
    switch id
        case 'a1a2'
            xlabel('$a_1$','FontSize',14,'Interpreter','latex');
            ylabel('$a_2$','FontSize',14,'Interpreter','latex');
            xticks([3.8, 4.2, 4.6]*1e-2)
            yticks([2.2, 2.4, 2.6]*1e-2)
        case 'a1a3'
            xlabel('$a_1$','FontSize',14,'Interpreter','latex');
            ylabel('$a_3$','FontSize',14,'Interpreter','latex');
            xticks([3.8, 4.2, 4.6]*1e-2)
            yticks([1.8, 1.9, 2.0]*1e-2)
        case 'a2a3'
            xlabel('$a_2$','FontSize',14,'Interpreter','latex');
            ylabel('$a_3$','FontSize',14,'Interpreter','latex');
            xticks([2.2, 2.4, 2.6]*1e-2)
            yticks([1.8, 1.9, 2.0]*1e-2)
    end
    
    % tidy up labels
    xtickformat('%.2f')
    ytickformat('%.2f')
    ax = gca;
    ax.XAxis.Exponent = -2;
    ax.YAxis.Exponent = -2;
    
    % title
    title(sprintf('$\\mathcal{S}=%g$', calS_vals(i)), 'Interpreter','latex');
end

% global colorbar
cbar = colorbar;
cbar.TickLabelInterpreter = 'latex';
cbar.Layout.Tile = 'east';

% scale and save
set(gcf, 'Position', [96 537 870 366])
save2pdf(['./head_morphology_eif/figures/',id,'/eta.pdf']);
close all

%% average work plot

%{
% how does average work across a beat change with head dimensions, for
% fixed calS and calM?

figure;
tiledlayout('flow','TileSpacing','compact');
for i = 1:num_calS
    
    % find percent difference to mean cell VAL
    rel_Wav = (msh{i}.W_av - avcell.Wav(i))/avcell.Wav(i);
    perc_Wav_diff = rel_Wav*100;
    
    % plot
    nexttile;
    surf(XX, YY, perc_Wav_diff, 'EdgeColor','none');
    colormap(tricolor2)
    view(2); axis tight; shading flat
    
    % colorbar
    cbar = colorbar;
    cbar.TickLabelInterpreter = 'latex';
    cbar.Location = 'southoutside';
    
    % set caxis to be symmetric about zero
    cax = caxis; mcax = max(abs(cax));
    caxis([-mcax, mcax]);
    % caxis([-6,6])
    
    % add enclosing box
    fbox = gca;
    box on;
    grid off;
    fbox.LineWidth = 0.5;
    fbox.Layer = 'top';
    
    % labels
    switch id
        case 'a1a2'
            xlabel('$a_1$','FontSize',14,'Interpreter','latex');
            ylabel('$a_2$','FontSize',14,'Interpreter','latex');
        case 'a1a3'
            xlabel('$a_1$','FontSize',14,'Interpreter','latex');
            ylabel('$a_3$','FontSize',14,'Interpreter','latex');
        case 'a2a3'
            xlabel('$a_2$','FontSize',14,'Interpreter','latex');
            ylabel('$a_3$','FontSize',14,'Interpreter','latex');
    end
    
    % tidy up labels
    xtickformat('%.2f')
    ytickformat('%.2f')
    ax = gca;
    ax.XAxis.Exponent = -2;
    ax.YAxis.Exponent = -2;
    
    % title
    title(sprintf('$\\mathcal{S}=%g$', calS_vals(i)), 'Interpreter','latex');
end

% scale and save
set(gcf, 'Position', [96 337 1160 566])
save2pdf(['./head_morphology_eif/figures/',id,'/Wav.pdf']);
close all
%}

%% fastest vs slowest cells

% how to fastest and slowest cells compare in waveform?

%{
figure; 
tiledlayout('flow','TileSpacing','compact');
for i = 1:num_calS

    % identify min and max VAL
    arr  = msh{i}.VAL;
    maxi = max(max(arr)); [idx_max,idy_max] = find(arr==maxi);
    mini = min(min(arr)); [idx_min,idy_min] = find(arr==mini);

    Z_slow   = msh{i}.Z{idx_min,idy_min};
    Z_fast   = msh{i}.Z{idx_max,idy_max};
    mdl_slow = msh{i}.model{idx_min,idy_min};
    mdl_fast = msh{i}.model{idx_max,idy_max};

    % slow trace
    nexttile;
    plotRelativeTraceEPC(Z_slow, mdl_slow);

    % fast trace
    nexttile;
    plotRelativeTraceEPC(Z_fast, mdl_fast);

    % curvatures
    kappa_slow = calcCurvatureEPC(Z_slow);
    kappa_fast = calcCurvatureEPC(Z_fast);
    s = linspace(0,1,size(Z_slow,1)-3);

    %{
    % slow lab curvature
    nexttile;
    s = linspace(0,1,size(Z_slow,1)-3);
    plotTopDownSurf(tbeat, s, kappa_slow, '$\kappa$', [-0.6,0.6]);
    ylim([0,1]); yticks([0,1])
    xlabel('$t$', 'FontSize',14, 'Interpreter','latex')
    ylabel('$s$', 'FontSize',14, 'Interpreter','latex')

    % fast lab curvature
    nexttile;
    s = linspace(0,1,size(Z_fast,1)-3);
    plotTopDownSurf(tbeat, s, kappa_fast, '$\kappa$', [-0.6,0.6]);
    ylim([0,1]); yticks([0,1])
    xlabel('$t$', 'FontSize',14, 'Interpreter','latex')
    ylabel('$s$', 'FontSize',14, 'Interpreter','latex')
    %}

    % difference in curvature
    nexttile;
    kappa_diff = kappa_fast-kappa_slow;
    cbar_lim   = [min(min(kappa_diff)), max(max(kappa_diff))];
    plotTopDownSurf(tbeat, s, kappa_diff, '$\Delta\kappa$', cbar_lim);
    xlabel('$t$', 'FontSize',14, 'Interpreter','latex')
    ylabel('$s$', 'FontSize',14, 'Interpreter','latex')

end
    
% scale and save
set(gcf,'Position',[560 1 527 947]);
save2pdf(['./head_morphology_eif/figures/',id,'/shape.pdf']);
close all
%}

%% mean absolute curvature

% how does mean absolute curvature change, sampling across the diagonal of
% fixed head axes values

figure;
tiledlayout('flow','TileSpacing','compact');

% colors 
offset = 2;
col = turbo(num_diag+offset); col = col(2:end-1,:);

s = linspace(0,1,length(msh{1}.mkap{1,1}));

for i = 1:num_calS
    
    nexttile(i); box on;
    for j = 1:num_diag
        
        % percent difference against mean cell
        rel_mkap = (msh{i}.mkap{j,j}-avcell.mkap{i})./avcell.mkap{i};
        perc_mkap_diff{i}(:,j) = rel_mkap*100;
        
        % plot
        plot(s, perc_mkap_diff{i}(:,j), 'LineWidth',1.2, 'Color',col(j,:));
        hold on;
    end
    
    % labels
    % xlabel('$s$', 'FontSize',14, 'Interpreter','latex');
    % ylabel('$<|\kappa(s)|>$', 'FontSize',14, 'Interpreter','latex');
    
    % axes
    ylim([-15 15])
    
    % title
    title(sprintf('$\\mathcal{S}=%g$', calS_vals(i)), 'Interpreter','latex');
end

% scale and save
set(gcf, 'Position', [96 537 932 366])
save2pdf(['./head_morphology_eif/figures/',id,'/mkap.pdf']);
close all

%% max mkap difference vs max VAL uplift

% does difference in curvature correspond to % VAL uplift?

figure;
tiledlayout('flow','TileSpacing','compact')

% colors
offset = 2;
col = turbo(num_calS+offset); col = col(2:end-1,:);

for i = 1:2:num_calS

    box on; hold on;
    for j = 1:num_diag
        % maximum % mkap diff
        max_mkap(i,j) = max(abs(perc_mkap_diff{i}(:,j)));
    
        % % VAL increase
        VAL_diag(i,j) = perc_VAL_diff{i}(j,j);
    end

    % plot
    plot(VAL_diag(i,:), max_mkap(i,:), 'LineWidth',1.2, 'Color',col(i,:))

end

% labels

% legend
lgd = legend('$\mathcal{S}=9$', '$\mathcal{S}=13$', '$\mathcal{S}=15$', '$\mathcal{S}=17$', ...
    'FontSize',14, 'Interpreter','latex', 'Location','northwest', 'EdgeColor','none');

% scale and save
set(gcf, 'Position', [824 714 296 233])
save2pdf(['./head_morphology_eif/figures/',id,'/mkap_VAL.pdf']);
close all

%% yaw plot

% how do head axes affect yaw range relative to yawing of normal head?

figure;
tiledlayout('flow','TileSpacing','compact')
for i = 1:num_calS
    
    % find percent difference to mean cell yaw
    rel_ryaw = (msh{i}.ryaw - avcell.ryaw(i))/avcell.ryaw(i);
    perc_ryaw_diff = rel_ryaw*100;
    
    % plot
    nexttile;
    surf(XX, YY, perc_ryaw_diff, 'EdgeColor','none');
    colormap(tricolor2)
    view(2); axis tight; shading flat
    
    % set caxis to be symmetric about zero
    caxis([-15,15])

    % add enclosing box
    fbox = gca;
    box on;
    grid off;
    fbox.LineWidth = 0.5;
    fbox.Layer = 'top';
    
    % labels
    switch id
        case 'a1a2'
            xlabel('$a_1$','FontSize',14,'Interpreter','latex');
            ylabel('$a_2$','FontSize',14,'Interpreter','latex');
        case 'a1a3'
            xlabel('$a_1$','FontSize',14,'Interpreter','latex');
            ylabel('$a_3$','FontSize',14,'Interpreter','latex');
        case 'a2a3'
            xlabel('$a_2$','FontSize',14,'Interpreter','latex');
            ylabel('$a_3$','FontSize',14,'Interpreter','latex');
    end
    
    % tidy up labels
    xtickformat('%.2f')
    ytickformat('%.2f')
    ax = gca;
    ax.XAxis.Exponent = -2;
    ax.YAxis.Exponent = -2;
    
    % title
    title(sprintf('$\\mathcal{S}=%g$', calS_vals(i)), 'Interpreter','latex');
end

% global colorbar
cbar = colorbar;
cbar.TickLabelInterpreter = 'latex';
cbar.Layout.Tile = 'east';

% scale and save
set(gcf, 'Position', [96 537 870 366])
save2pdf(['./head_morphology_eif/figures/',id,'/ryaw.pdf']);
close all

%% completion

printLinebreak

end
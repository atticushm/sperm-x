function plotFixedVolumeGlobal()

% generates plots that require loading of all 3 a1a2, a1a3, a2a3 data sets
% to be computed.

printLinebreak
close all;

%% load data

% load preprocessed data
path = './head_morphology_eif/preproc/a1a2.mat';
if ~exist(path,'file')
    frintf('Please run preprocFixedVolData!\n')
    return
else
    fprintf('Loading preprocessed data...');
    load(path);
    fprintf(' complete!\n');
end
a1a2 = msh;

path = './head_morphology_eif/preproc/a1a3.mat';
if ~exist(path,'file')
    frintf('Please run preprocFixedVolData!\n')
    return
else
    fprintf('Loading preprocessed data...');
    load(path);
    fprintf(' complete!\n');
end
a1a3 = msh;

path = './head_morphology_eif/preproc/a2a3.mat';
if ~exist(path,'file')
    frintf('Please run preprocFixedVolData!\n')
    return
else
    fprintf('Loading preprocessed data...');
    load(path);
    fprintf(' complete!\n');
end
a2a3 = msh;

%% load in simulation data from cell swimming with mean head size

% load
path_avcell = './head_morphology_eif/preproc/avcell.mat';
load(path_avcell,'data');
avcell = data;

% extract data
ax_av = avcell.ax;
GRM_av = abs(avcell.GRM{1});

%% set figure

% latex and font size for tick labels
set(groot, 'defaultAxesTickLabelInterpreter','latex');
set(groot, 'defaultAxesFontSize',14);

% load tricolor colormap
load('tools/tricolor2.mat','tricolor2')

%% horzcat some matrices per calS

% number of calS
num_calS = length(a1a2);

% calS values 
calS_vals = [9, 12, 13, 14, 15, 16, 17, 18];

for i = 1:num_calS
    
    VAL{i}  = [a1a2{i}.VAL, a1a3{i}.VAL, a2a3{i}.VAL];
    eta{i}  = [a1a2{i}.eta, a1a3{i}.eta, a2a3{i}.eta];
    GRMs{i} = [a1a2{i}.GRM, a1a3{i}.GRM, a2a3{i}.GRM];
    R11R66{i} = [a1a2{i}.R11R66, a1a3{i}.R11R66, a2a3{i}.R11R66];
    R11R22{i} = [a1a2{i}.R11R22, a1a3{i}.R11R22, a2a3{i}.R11R22];
    
    % relative VAL and eta
    rel_VAL{i} = 100*(VAL{i}-avcell.VAL(i))/avcell.VAL(i);
    rel_eta{i} = 100*(eta{i}-avcell.eta(i))/avcell.eta(i);
    
    % GRM ratios
    R11R22{i} = [a1a2{i}.R11R22, a1a3{i}.R11R22, a2a3{i}.R11R22];
    R11R66{i} = [a1a2{i}.R11R66, a1a3{i}.R11R66, a2a3{i}.R11R66];
    
    % solution data
    sol{i} = [a1a2{i}.Z, a1a3{i}.Z, a2a3{i}.Z];
    mdl{i} = [a1a2{i}.model, a1a3{i}.model, a2a3{i}.model];
    
end

% axes values
hax = [a1a2{1}.ax, a1a3{1}.ax, a2a3{1}.ax]; hax_mat = [hax{:}];

%% "star plots" mapping a1,a2,a3 axes to VAl and eta

fprintf('Making star plots (VAL and eta)... ')

% combine all axes values
a1  = hax_mat(1,:)';
a2  = hax_mat(2,:)';
a3  = hax_mat(3,:)';

% load tricolor colormap
load('tools/tricolor2.mat','tricolor2')

% set figure
figure; tiledlayout('flow','TileSpacing','compact')

for i = 1:num_calS
    
    nexttile;
    
    % vectorise VAL, eta
    vv = [rel_VAL{i}]; VAL_vec(:,i) = vv(:);
    ee = [eta{i}];     eta_vec(:,i) = ee(:);
    
    % normalise eta for use as size data
    evs = normalize(eta_vec(:,i),'range',[5,50]);
    
    % scatter nodes, coloring by VAL, size by eta
    scatter3(a1,a2,a3,evs,'filled','CData',VAL_vec(:,i)); hold on; box on;
    colormap(tricolor2)
    
    % indicate fastest cell
    [~,id] = max(VAL_vec(:,i));
    scatter3(a1(id),a2(id),a3(id),evs(id), 'ko', 'LineWidth',2)
    
    % indicate most efficient cell
    [~,id] = max(eta_vec(:,i));
    scatter3(a1(id),a2(id),a3(id),evs(id), 'kx', 'LineWidth',2);

    % indicate mean cell
    mc = scatter3(ax_av(1),ax_av(2),ax_av(3),35, 'ko','filled');
    uistack(mc, 'top')
    
    % set caxis to be symmetric about zero
    caxis([-2,2])
    
    % labels
    xlabel('$a_1$', 'FontSize',14,'Interpreter','latex'); 
    ylabel('$a_2$', 'FontSize',14,'Interpreter','latex'); 
    zlabel('$a_3$', 'FontSize',14,'Interpreter','latex');
    title(sprintf('$\\mathcal{S}=%g$', calS_vals(i)), ...
        'FontSize', 16, 'Interpreter','latex')
    ax = gca;
    ax.XAxis.Exponent = -2; ax.YAxis.Exponent = -2; ax.ZAxis.Exponent = -2;
    
    % axes
    set(gca, 'View',[100.1889 30.0000]);
    % view(2)
end

% global colorbar
cbar = colorbar;
cbar.Layout.Tile = 'south';
cbar.TickLabelInterpreter = 'latex';

% scale and save
fprintf('complete!\n')
% set(gcf, 'Position',[1 211 1270 611])
set(gcf, 'Position',[444 278 676 669])
save2pdf('./head_morphology_eif/figures/global/scatter_VAL_eta.pdf')
close all

%% scatter mapping R11/R22, R11/R66 ratios to VAL, eta

fprintf('Making pcolor plots (GRM)... ')

% load tricolor colormap
load('tools/tricolor2.mat','tricolor2')

% set figure
figure; 

for i = 1:num_calS
    
    nexttile;
    
    % vectorise R11/R22 and R11/R66
    r11r22 = [R11R22{i}];   r11r22_vec(:,i) = r11r22(:);
    r11r66 = [R11R66{i}];   r11r66_vec(:,i) = r11r66(:);
    
    % vectorise VAL, eta
    vv = [rel_VAL{i}]; VAL_vec(:,i) = vv(:);
    ee = [eta{i}];     eta_vec(:,i) = ee(:);
    re = [rel_eta{i}]; reta_vec(:,i)= re(:);
    
    % normalise eta for use as size data
    evs = normalize(eta_vec(:,i),'range',[5,70]);
    
    % scatter nodes, coloring by R11/R22, size by R11/R66
    scatter3(r11r22_vec(:,i),r11r66_vec(:,i),VAL_vec(:,i),evs,'filled','CData',VAL_vec(:,i));
    colormap(tricolor2)
    
    % set caxis to be symmetric about zero
    caxis([-2,2])
    
    % labels
    xlabel('$\mathcal{R}_{11}/\mathcal{R}_{22}$', 'FontSize',14,'Interpreter','latex'); 
    ylabel('$\mathcal{R}_{11}/\mathcal{R}_{66}$', 'FontSize',14,'Interpreter','latex'); 
    zlabel('VAL', 'FontSize',14, 'Interpreter','latex')
    title(sprintf('$\\mathcal{S}=%g$', calS_vals(i)), ...
        'FontSize', 16, 'Interpreter','latex')
    ax = gca;
    ax.YAxis.Exponent = 2;
    xlim([0.8533 0.9592])
    xticks([0.86, 0.94])

    % axes
    box on;
    view(68.4, 25.0334)
end

% scale and save
fprintf('complete!\n')
set(gcf, 'Position',[1 211 1270 611])
save2pdf('./head_morphology_eif/figures/global/scatter_GRM.pdf')
close all

%% scatter for GRM ratios against VAL, eta

fprintf('Making GRM scatter plots...')

% set figure
figure; 
tiledlayout('flow','TileSpacing','loose')

for i = 1:num_calS

    % vectorise relative eta
    ee = [rel_eta{i}];
    reta_vec(:,i) = ee(:);

    % marker size
    sz = 20;

    % id for points with approx average eta (i.e. 0% diff)
    tol = 0.05;
    zeid = abs(reta_vec(:,i))<=tol;

    % values for sampling
    r11r22_samp = linspace(0.867,0.95);
    r11r66_samp = linspace(200,380);

    % R11/R22 against VAL, color is relative eta
    nexttile;
    scatter(r11r22_vec(:,i), VAL_vec(:,i), sz, 'filled', 'CData', reta_vec(:,i))
    box on; grid on; hold on;
    colormap(tricolor2)
    caxis([-2.5,2.5])
    ylim([-2,2])

    % R11/R22 zeroline
    sf1 = fit(r11r22_vec(zeid,i), VAL_vec(zeid,i), 'poly1');
    pf1 = sf1(r11r22_samp);
    plot(r11r22_samp, pf1, 'k', 'LineWidth',1.6)

    % R11/R22 zeroline gradient
    pf1_grad(i) = (pf1(end)-pf1(1))/(r11r22_samp(end)-r11r22_samp(1));

    % labels
    xlabel('$\mathcal{R}_{11}/\mathcal{R}_{22}$', 'FontSize',14,'Interpreter','latex'); 
    % ylabel('VAL', 'FontSize',14, 'Interpreter','latex')

    % R11/R66 against VAL, color is relative eta
    nexttile;
    scatter(r11r66_vec(:,i), VAL_vec(:,i), sz, 'filled', 'CData', reta_vec(:,i))
    box on; grid on; hold on;
    colormap(tricolor2)
    caxis([-2.5,2.5])

    % R11/R66 zeroline
    sf2 = fit(r11r66_vec(zeid,i), VAL_vec(zeid,i), 'poly1');
    pf2 = sf2(r11r66_samp);
    plot(r11r66_samp, pf2, 'k', 'LineWidth',1.6)

    % R11/R22 zeroline gradient
    pf2_grad(i) = (pf2(end)-pf2(1))/(r11r66_samp(end)-r11r66_samp(1));

    % axes
    % ax = gca; ax.XAxis.Exponent = 2;
    ylim([-2,2])

    % labels
    xlabel('$\mathcal{R}_{11}/\mathcal{R}_{66}$', 'FontSize',14,'Interpreter','latex'); 
    % ylabel('VAL', 'FontSize',14, 'Interpreter','latex')
end

% global colorbar
cbar = colorbar;
cbar.Layout.Tile = 'south';
cbar.TickLabelInterpreter = 'latex';
caxis([-2.5,2.5])

% scale and save
set(gcf, 'Position',[988.0000 209 693.0000 643])
save2pdf('./head_morphology_eif/figures/global/ratios.pdf')
close all

%% ratio zeroline gradients

% R11/R22 zeroline gradients
figure;
nexttile;
tang1 = atan(pf1_grad);
plot(calS_vals, pf1_grad, 'o-','LineWidth',1.6); box on

xlim([8.1601 19.1601])
% ylim([-0.7259 1.6952])
xlabel('$\mathcal{S}$', 'FontSize',14, 'Interpreter','latex')
ylabel('$\nabla^{\textup{ZL}}_{\mathcal{R}_{11}/\mathcal{R}_{22}}$', 'FontSize',14, 'Interpreter','latex')

% R11/R66 zeroline gradients
nexttile;
tang2 = atan(pf2_grad);
plot(calS_vals, pf2_grad, 'o-','LineWidth',1.6); box on;

xlim([8.1601 19.1601])
ylim([0.0013 0.0049])
xlabel('$\mathcal{S}$', 'FontSize',14, 'Interpreter','latex')
ylabel('$\nabla^{\textup{ZL}}_{\mathcal{R}_{11}/\mathcal{R}_{66}}$', 'FontSize',14, 'Interpreter','latex')

% scale and save
set(gcf, 'Position',[1186 486 495 162])
save2pdf('./head_morphology_eif/figures/global/ratios_zl_grad.pdf')
close all

%% rotation of "zero line"

figure;

% a1 points at which to sample fitted curve
x_samp = linspace(0.035,0.045);

% colors
col = parula(num_calS+1);

for i = 1:num_calS

    % curve through values close to zero 
    % values within tol of zero
    tol  = 0.02;
    zeid = abs(VAL_vec(:,i))<=tol;
    
    % fit polynomial
    sf = fit(a1(zeid),a2(zeid),'poly1');
    
    % sample and plot
    pf(:,i) = sf(x_samp);
    lino(i) = plot(x_samp, pf(:,i), 'LineWidth',1.6, 'Color',col(i,:)); hold on; box on;
    scatter(a1(zeid), a2(zeid), 20, 'o', 'MarkerFaceColor',col(i,:), 'MarkerEdgeColor','none')
    hold on; 
    
    % labels
    xlabel('$a_1$', 'FontSize',12, 'Interpreter','latex');
    ylabel('$a_2$', 'FontSize',12, 'Interpreter','latex');

end

% legend
legend( [lino(1),lino(2),lino(3),lino(4),lino(5),lino(6),lino(7),lino(8)], ...
        {'$\mathcal{S}=9$', '$\mathcal{S}=12$', '$\mathcal{S}=13$', '$\mathcal{S}=14$', ...
       '$\mathcal{S}=15$', '$\mathcal{S}=16$', '$\mathcal{S}=17$', '$\mathcal{S}=18$'}, ...
       'Interpreter','latex', 'FontSize',12, 'EdgeColor','none', 'Location','eastoutside')
   
% axes
ylim([0.015,0.035])
ax = gca;
ax.XAxis.Exponent = -2;
ax.YAxis.Exponent = -2;

% save and scale
set(gcf, 'Position',[605 677 440 171]);
save2pdf('./head_morphology_eif/figures/global/zeroline.pdf')
close all

%% for each calS: trace that optimises VAL

figure;
tiledlayout('flow','TileSpacing','compact')

for i = 1:num_calS     % loop over calS
    
    % global VAL and eta max
    VAL_max(i) = max(max(VAL{i}));
    eta_max(i) = max(max(eta{i}));
    
    %% find data for fastest and most efficient swimmers 
    
    [vidx,vidy] = find(VAL{i}==VAL_max(i));    
    [eidx,eidy] = find(eta{i}==eta_max(i));
    
    % head axes 
    opt_ax_VAL(:,i) = hax{vidx,vidy};
    opt_ax_eta(:,i) = hax{eidx,eidy};
    
    % solutions and model structures
    opt_sol_VAL{i} = sol{i}{vidx,vidy};  opt_mdl_VAL{i} = mdl{i}{vidx,vidy};
    opt_sol_eta{i} = sol{i}{eidx,eidy};  opt_mdl_eta{i} = mdl{i}{eidx,eidy};
    
    % relative trace plots for VAL
    nexttile;
    plotRelativeTraceEPC(opt_sol_VAL{i}, opt_mdl_VAL{i});
   
    % axes tweaks
    axis equal
    xlim([-0.1 0.9])
    ylim([-0.5 0.5])

    % additional labels
    title(sprintf('$\\mathcal{S}=%g$', calS_vals(i)), ...
        'FontSize', 16, 'Interpreter','latex')
    ax = gca;
    ax.XTickLabel = {'0','','0.4','','0.8'};
    
    %{
    %% R11/R66 against VAL, eta 
    
    % stack ratios, VAL, eta
    rat_vec = R11R66{i}(:);
    VAL_vec = VAL{i}(:);
    eta_vec = eta{i}(:);
    
    % sort rats, and use indexing to sort VAL, eta
    [rat_asc, id_asc] = sort(rat_vec);
    VAL_asc = VAL_vec(id_asc);  eta_asc = eta_vec(id_asc);
    
    % 2d plot
    figure; tiledlayout(1,2);
    nexttile; scatter(rat_asc, VAL_asc, '.'); box on;
    ax = gca; ax.XAxis.Exponent = 2;
    nexttile; scatter(rat_asc, eta_asc, '.'); box on;
    ax = gca; ax.XAxis.Exponent = 2;
    set(gcf, 'Position', [458 732 662 216]);
    save2pdf(sprintf('./head_morphology_eif/figures/global/R11R66_%g.pdf',i))
    
    %% R11/R22 against VAL, eta
    
    % stack ratios, VAL, eta
    rat_vec = R11R22{i}(:);
    VAL_vec = VAL{i}(:);
    eta_vec = eta{i}(:);
    
    % sort rats, and use indexing to sort VAL, eta
    [rat_asc, id_asc] = sort(rat_vec);
    VAL_asc = VAL_vec(id_asc);  eta_asc = eta_vec(id_asc);
    
    % 2d plot
    figure; tiledlayout(1,2);
    nexttile; scatter(rat_asc, VAL_asc, '.'); box on;
    ax = gca; ax.XAxis.Exponent = -1;
    nexttile; scatter(rat_asc, eta_asc, '.'); box on;
    ax = gca; ax.XAxis.Exponent = -1;
    set(gcf, 'Position', [458 732 662 216]);
    save2pdf(sprintf('./head_morphology_eif/figures/global/R11R22_%g.pdf',i))
    %}
   
end

% scale and save
set(gcf, 'Position',[302 575 818 373]);
save2pdf('./head_morphology_eif/figures/global/waveforms_VAL.pdf')
close all

%% optimal axes as calS increases

figure;

% cols
col = lines(3);

% plot
for i = 1:num_calS
    plot(calS_vals, opt_ax_VAL(1,:), 'o-', 'Color',col(1,:), 'LineWidth',1.5); box on; hold on;
    plot(calS_vals, opt_ax_VAL(2,:), 'd-', 'Color',col(2,:), 'LineWidth',1.5);
    plot(calS_vals, opt_ax_VAL(3,:), 'x-', 'Color',col(3,:), 'LineWidth',1.5);
end

% legend
axis tight
legend('$a_1$','$a_2$','$a_3$', 'FontSize',12, 'Interpreter','latex', ...
    'EdgeColor','none', 'Location','west');

% labels
xlabel('$\mathcal{S}$', 'FontSize',14, 'Interpreter','latex')
ylabel('Dimensionless radius', 'FontSize',14, 'Interpreter','latex')

% scale and save
set(gcf, 'Position',[790 430 261 189])
save2pdf('./head_morphology_eif/figures/global/opt_axes.pdf')
close all 

%% print things to command window

%{
% print to command window
fprintf('Mean head axes are:\n');
fprintf('            [%g, %g, %g]\n', ax_av(1),ax_av(2),ax_av(3));
fprintf('\n')
fprintf('Axes maximising VAL (for each calS) are:\n');
fprintf('   calS=9:  [%g, %g, %g] (score=%g, av=%g)\n', opt_ax_VAL(1,1), opt_ax_VAL(2,1), opt_ax_VAL(3,1), max_VAL(1), av_VAL(1));
fprintf('   calS=12: [%g, %g, %g] (score=%g, av=%g)\n', opt_ax_VAL(1,2), opt_ax_VAL(2,2), opt_ax_VAL(3,2), max_VAL(2), av_VAL(2)); 
fprintf('   calS=15: [%g, %g, %g] (score=%g, av=%g)\n', opt_ax_VAL(1,3), opt_ax_VAL(2,3), opt_ax_VAL(3,3), max_VAL(3), av_VAL(3));
fprintf('   calS=18: [%g, %g, %g] (score=%g, av=%g)\n', opt_ax_VAL(1,4), opt_ax_VAL(2,4), opt_ax_VAL(3,4), max_VAL(4), av_VAL(4));
fprintf('\n')
fprintf('Axes maximising eta (for each calS) are:\n');
fprintf('   calS=9:  [%g, %g, %g] (score=%g, av=%g)\n', opt_ax_eta(1,1), opt_ax_eta(2,1), opt_ax_eta(3,1), max_eta(1), av_eta(1));
fprintf('   calS=12: [%g, %g, %g] (score=%g, av=%g)\n', opt_ax_eta(1,2), opt_ax_eta(2,2), opt_ax_eta(3,2), max_eta(2), av_eta(2)); 
fprintf('   calS=15: [%g, %g, %g] (score=%g, av=%g)\n', opt_ax_eta(1,3), opt_ax_eta(2,3), opt_ax_eta(3,3), max_eta(3), av_eta(3));
fprintf('   calS=18: [%g, %g, %g] (score=%g, av=%g)\n', opt_ax_eta(1,4), opt_ax_eta(2,4), opt_ax_eta(3,4), max_eta(4), av_eta(4));
fprintf('\n')

%% across calS: which heads are most optimal for VAL and eta individually?

% add scaled matrices
comb_sVAL = sVAL{1}+sVAL{2}+sVAL{3}+sVAL{4};
comb_seta = seta{1}+seta{2}+seta{3}+seta{4};

% maximum scores and indices
comb_VAL_max = max(max(comb_sVAL));
comb_eta_max = max(max(comb_seta));
[vidx,vidy] = find(comb_sVAL==comb_VAL_max);
[eidx,eidy] = find(comb_seta==comb_eta_max);

% overall best axes for VAL, efficiency over all calS
ax_best_VAL = hax{vidx,vidy}; ax_best_eta = hax{eidx,eidy};
fprintf('Axes maximising VAL over all calS is:\n')
fprintf('            [%g, %g, %g] (score=%g)\n', ax_best_VAL(1),ax_best_VAL(2),ax_best_VAL(3),comb_VAL_max);
fprintf('Axes maximising eta over all calS is:\n')
fprintf('            [%g, %g, %g] (score=%g)\n', ax_best_eta(1),ax_best_eta(2),ax_best_eta(3),comb_eta_max);
fprintf('\n')
%}

%% completion

printLinebreak
close all

end % function
function plotAbnormalHeads()

% plot tapered, pyriform and round head examples

close all;

% generate abnormal heads
H_trac    = 5;
H_quad    = 10;
[x,X,~,~] = GenerateAbnormalHeads(H_trac, H_quad);

% options for scatters
mkr_size = 15;

% ~~ plots ~~
% ~ tapered ~
nexttile;
box on; hold on;
[x1,x2,x3] = extractComponents(x{3});
scatter3(x1,x2,x3, mkr_size,'k.');

out = SurfaceFromPoints(X{3});
trisurf(out.hull,out.x,out.y,out.z, 'LineStyle','none');

axis equal
val = 0.06;
axis([-val val -val val -val val])
view(3);

title('tapered')

% ~ round ~
nexttile;
box on; hold on;
[x1,x2,x3] = extractComponents(x{2});
scatter3(x1,x2,x3, mkr_size,'k.');

out = SurfaceFromPoints(X{2});
trisurf(out.hull,out.x,out.y,out.z, 'LineStyle','none');

axis equal
val = 0.06;
axis([-val val -val val -val val])
view(3);

title('round')

% ~ pyriform ~
nexttile;
box on; hold on;
[x1,x2,x3] = extractComponents(x{4});
scatter3(x1,x2,x3, mkr_size,'k.');

out = SurfaceFromPoints(X{4});
trisurf(out.hull,out.x,out.y,out.z, 'LineStyle','none');

axis equal
val = 0.06;
axis([-val-0.05 val-0.05 -val val -val val])
view(3);

title('pyriform')

% scale figure and save
figure(1); set(gcf, 'Position', [1001 1063 876 276]);
save2pdf('./figures/abnormal_human_heads.pdf')

end % function
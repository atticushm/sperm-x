function plotCellHeads()

% View all the human and mouse sperm heads under consideration in cell
% swimming over backstep simulations

close all

%% human

% discretisation parameters
H_trac = 5;
H_quad = 10;

% head number parameter
num = 20;

% generate discretisations
[x1,~,~] = generateEllipsoidalHeads(num, H_trac);
[X1,~,~] = generateEllipsoidalHeads(num, H_quad);

%[x2,~,~] = generateScaledHeads(H_trac, false);
%[X2,~,~] = generateScaledHeads(H_quad, false);
x2=[]; X2=[];

x = [x1,x2]; 
X = [X1,X2];

% number of heads
num_heads = length(x1);

% setup figure
num_cols = 5;
figure; tiledlayout(ceil(num_heads/num_cols), num_cols, 'TileSpacing','compact');

% axis limit
ax_lim = 0.06;

% plots
for i = 1:length(x)
    nexttile;
    box on; hold on;
    [x1,x2,x3] = extractComponents(x{i});
    mkr_size = 15;
    scatter3(x1,x2,x3,mkr_size,'k.');
    
    out = surfaceFromPoints(X{i});
    trisurf(out.hull, out.x, out.y, out.z, 'LineStyle', 'none');
    
    axis equal
    axis([-ax_lim ax_lim -ax_lim ax_lim -ax_lim ax_lim])
    view(3);
    % axis off
end
set(gcf,'Position',[990 431 828 977])

% save figure
str = './head_morphology_eif/figures/human_study_heads.pdf';
save2pdf(str)
fprintf('Figure saved at %s\n', str);
close all

%% mouse

% mouse_types = { ...
%     'BALBc','C57Bl6','CBA','CD1','DBA','FVB','MF1YRIII','Mmdomesticus',     ...
%     'Mmmusculus','Mspretus' ...
%     };
% num_types = length(mouse_types);
% 
% % discretisation parameters
% N_trac = 100;
% N_quad = 400;
% 
% % generate discretisations
% for i = 1:num_types
%     type = mouse_types{i};
%     [x{i},X{i},~] = GenerateBodyNodes(N_trac, N_quad, 'mouse', type);
% end
% 
% % plots
% figure;
% for i = 1:num_types
%     nexttile;
%     box on; hold on;
%     [x1,x2,x3] = extractComponents(X{i});
%     mkr_size = 15;
%     scatter3(x1,x2,x3,mkr_size,'k.');
%    
%     % indicate flagellum connection point
%     scatter(0,0,200,'r.');
%     
%     axis equal
%     axis([-0.07 0.03 -0.04 0.04])
%     view(2);
%     %axis off
%     
%     title(mouse_types{i})
% end
% % set(gcf,'Position',[1001 370 492 969]);
% set(gcf,'Position',[1088 119 626 1134])
% 
% % save figure
% save2pdf('./figures/MouseSpermHeads.pdf')

close all

end % function
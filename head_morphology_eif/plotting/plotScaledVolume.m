function plotScaledVolume()

% plots examining the effect of cells with scaled volumes on swimming
% speed, efficiency, yaw etc

close all; 
printLinebreak

% load preprocessed data
path_ell = './head_morphology_eif/preproc/ell.mat';        % elliptial bodies
path_rnd = './head_morphology_eif/preproc/rnd.mat';        % round bodies
path_tap = './head_morphology_eif/preproc/tap.mat';        % tapered bodies

load(path_ell,'ell');
load(path_rnd,'rnd');
load(path_tap,'tap');

% values for calS (read from headMorphologyBatch)
calS_vals = [9,12,13,14,15,16,17,18];
num_calS  = length(calS_vals);

% number of volume scales considered
V_scale = 0.5:0.1:2;
num_V = length(V_scale);

%% set figure

% latex and font size for tick labels
set(groot, 'defaultAxesTickLabelInterpreter','latex');
set(groot, 'defaultAxesFontSize',14);

%% volume against VAL and efficiency

% initialise figure
figure; tiledlayout('flow','TileSpacing','loose');

% colors
col = lines(3);

% plots
for i = 1:num_calS
    
    % volume against VAL
    nexttile; box on; hold on
    plot( ell.vol(i,:), ell.VAL(i,:), 'LineWidth',1.2, 'Color',col(1,:));
    plot( rnd.vol(i,:), rnd.VAL(i,:), 'LineWidth',1.2, 'Color',col(2,:));
    plot( tap.vol(i,:), tap.VAL(i,:), 'LineWidth',1.2, 'Color',col(3,:));
    
    % highlight 6th entry -- when V_scale=1
    scatter(ell.vol(i,6), ell.VAL(i,6), 'MarkerEdgeColor',col(1,:), 'LineWidth',1.2)
    scatter(rnd.vol(i,6), rnd.VAL(i,6), 'MarkerEdgeColor',col(2,:), 'LineWidth',1.2)
    scatter(tap.vol(i,6), tap.VAL(i,6), 'MarkerEdgeColor',col(3,:), 'LineWidth',1.2)
    ylabel('VAL','FontSize',14,'Interpreter','latex')

    % volume against eta
    nexttile; box on; hold on;
    plot( ell.vol(i,:), ell.eta(i,:), 'LineWidth',1.2, 'Color',col(1,:));
    plot( rnd.vol(i,:), rnd.eta(i,:), 'LineWidth',1.2, 'Color',col(2,:));
    plot( tap.vol(i,:), tap.eta(i,:), 'LineWidth',1.2, 'Color',col(3,:));
    
    % highlight 6th entry
    scatter(ell.vol(i,6), ell.eta(i,6), 'MarkerEdgeColor',col(1,:), 'LineWidth',1.2)
    scatter(rnd.vol(i,6), rnd.eta(i,6), 'MarkerEdgeColor',col(2,:), 'LineWidth',1.2)
    scatter(tap.vol(i,6), tap.eta(i,6), 'MarkerEdgeColor',col(3,:), 'LineWidth',1.2)
    ylabel('$\eta$','FontSize',14,'Interpreter','latex')

    %{
    % volume against yaw
    nexttile; hold on; box on;
    plot( ell.vol(i,:), ell.ryaw(i,:), 'LineWidth',1.2, 'Color',col(1,:));
    plot( rnd.vol(i,:), rnd.ryaw(i,:), 'LineWidth',1.2, 'Color',col(2,:));
    plot( tap.vol(i,:), tap.ryaw(i,:), 'LineWidth',1.2, 'Color',col(3,:));
    
    % highlight 6th entry
    scatter(ell.vol(i,6), ell.ryaw(i,6), 'MarkerEdgeColor',col(1,:), 'LineWidth',1.2)
    scatter(rnd.vol(i,6), rnd.ryaw(i,6), 'MarkerEdgeColor',col(2,:), 'LineWidth',1.2)
    scatter(tap.vol(i,6), tap.ryaw(i,6), 'MarkerEdgeColor',col(3,:), 'LineWidth',1.2)
    %}
end

% scale and save
set(gcf, 'Position', [894 274 787 578])
save2pdf('./head_morphology_eif/figures/scaled/val_eta.pdf')
close all;

%% volume against yaw

% initialise figure
figure; tiledlayout(1,4,'TileSpacing','compact');

% colors
col = lines(3);

% plots
for i = 1:4
    
    nexttile(i); hold on; box on;
    plot( ell.vol(i,:), ell.ryaw(i,:), 'LineWidth',1.2, 'Color',col(1,:));
    plot( rnd.vol(i,:), rnd.ryaw(i,:), 'LineWidth',1.2, 'Color',col(2,:));
    plot( tap.vol(i,:), tap.ryaw(i,:), 'LineWidth',1.2, 'Color',col(3,:));
    
    % highlight 6th entry -- when V_scale=1
    scatter(ell.vol(i,6), ell.ryaw(i,6), 'MarkerEdgeColor',col(1,:), 'LineWidth',1.2)
    scatter(rnd.vol(i,6), rnd.ryaw(i,6), 'MarkerEdgeColor',col(2,:), 'LineWidth',1.2)
    scatter(tap.vol(i,6), tap.ryaw(i,6), 'MarkerEdgeColor',col(3,:), 'LineWidth',1.2)
    
    % labels
    %{
    xlabel('volume', 'FontSize',14, 'Interpreter','latex');
    if (i==1)
        ylabel('$|\tan\phi_{\max}-\tan\phi_{\min}|$', 'FontSize', 14, 'Interpreter','latex');
    end
    if (i==4)
        legend('ellipsoidal','round','tapered', 'FontSize',12, ...
            'Interpreter','latex', 'EdgeColor','none');
    end
    %}
    title(sprintf('$\\mathcal{S}=%g$', calS_vals(i)), 'Interpreter','latex', ...
        'FontSize',14);
end

% scale and save
set(gcf, 'Position', [245 703 875 244])
save2pdf('./head_morphology_eif/figures/scaled/ryaw.pdf')
close all

%% volume against GRM values

% consider only S=9, S=18 (where extremes of yaw are observed)

% initialise figure
figure;

% colors
col = lines(3);

% make vectors containing entries of RFU and RMO
for j = 1:num_V
    ell.RFU(1,j) = abs(ell.GRM{1,j}(1,1));
    ell.RFU(2,j) = abs(ell.GRM{1,j}(2,2));
    ell.RFU(3,j) = abs(ell.GRM{1,j}(3,3));
    ell.RMO(1,j) = abs(ell.GRM{1,j}(4,4));
    ell.RMO(2,j) = abs(ell.GRM{1,j}(5,5));
    ell.RMO(3,j) = abs(ell.GRM{1,j}(6,6));

    rnd.RFU(1,j) = abs(rnd.GRM{1,j}(1,1));
    rnd.RFU(2,j) = abs(rnd.GRM{1,j}(2,2));
    rnd.RFU(3,j) = abs(rnd.GRM{1,j}(3,3));
    rnd.RMO(1,j) = abs(rnd.GRM{1,j}(4,4));
    rnd.RMO(2,j) = abs(rnd.GRM{1,j}(5,5));
    rnd.RMO(3,j) = abs(rnd.GRM{1,j}(6,6));

    tap.RFU(1,j) = abs(tap.GRM{1,j}(1,1));
    tap.RFU(2,j) = abs(tap.GRM{1,j}(2,2));
    tap.RFU(3,j) = abs(tap.GRM{1,j}(3,3));
    tap.RMO(1,j) = abs(tap.GRM{1,j}(4,4));
    tap.RMO(2,j) = abs(tap.GRM{1,j}(5,5));
    tap.RMO(3,j) = abs(tap.GRM{1,j}(6,6));
end

% plots 
for i = 1:2
    for j = 1:3
    
        % plot
        nexttile; hold on; box on; grid on
        if (i==1) % plot RFU entries

            plot( ell.vol(1,:), ell.RFU(j,:), 'LineWidth',1.2, 'Color',col(1,:));
            plot( rnd.vol(1,:), rnd.RFU(j,:), 'LineWidth',1.2, 'Color',col(2,:));
            plot( tap.vol(1,:), tap.RFU(j,:), 'LineWidth',1.2, 'Color',col(3,:));
            
            % labels
            % ylabel(sprintf('$\\mathcal{R}_{%g%g}$',j,j), 'FontSize',14','Interpreter','latex');
            
            % highlight 6th entry -- when V_scale=1
            scatter(ell.vol(1,6), ell.RFU(j,6), 'MarkerEdgeColor',col(1,:), 'LineWidth',1.2)
            scatter(rnd.vol(1,6), rnd.RFU(j,6), 'MarkerEdgeColor',col(2,:), 'LineWidth',1.2)
            scatter(tap.vol(1,6), tap.RFU(j,6), 'MarkerEdgeColor',col(3,:), 'LineWidth',1.2)

        elseif (i==2) % plot RMO entries
            
            plot( ell.vol(1,:), ell.RMO(j,:), 'LineWidth',1.2, 'Color',col(1,:));
            plot( rnd.vol(1,:), rnd.RMO(j,:), 'LineWidth',1.2, 'Color',col(2,:));
            plot( tap.vol(1,:), tap.RMO(j,:), 'LineWidth',1.2, 'Color',col(3,:));
            
            % highlight 6th entry -- when V_scale=1
            scatter(ell.vol(1,6), ell.RMO(j,6), 'MarkerEdgeColor',col(1,:), 'LineWidth',1.2)
            scatter(rnd.vol(1,6), rnd.RMO(j,6), 'MarkerEdgeColor',col(2,:), 'LineWidth',1.2)
            scatter(tap.vol(1,6), tap.RMO(j,6), 'MarkerEdgeColor',col(3,:), 'LineWidth',1.2)
            
            % labels
            % xlabel('Volume','FontSize',14','Interpreter','latex');
            % ylabel(sprintf('$\\mathcal{R}_{%g%g}$',j+3,j+3), 'FontSize',14','Interpreter','latex');
            
        end
        
        % sort axes on per frame basis
        ax = gca;
        if      (i==1) && (j==1)
            ax.YAxis.Exponent = -1;
        elseif  (i==2) && ((j==2)||(j==3))
            ax.YAxis.Exponent = -2;
        end
        
    end
    
    % legend on last plot
    %{
    if (i==2)
        legend('ellipsoidal','round','tapered','FontSize',12,'Interpreter','latex', ...
            'EdgeColor','None', 'Location','northwest');
    end
    %}
    
end

% scale and save
set(gcf, 'Position', [99 473 647 295])
save2pdf('./head_morphology_eif/figures/scaled/GRM.pdf')
close all

%% R11R66 against VAL, eta

% set colors, line styles 
col  = lines(3);
lsty = {'-','--','-.',':'}; 

% alpha values to indicate calS value
alph = linspace(0.2,1,num_calS);

% plot R11R66 against:
figure; tiledlayout('flow','TileSpacing','loose')

% ... volume
nexttile; box on; hold on;
plot(ell.R11R66(i,:), ell.vol(1,:), '-', 'LineWidth',1.2, 'Color',col(1,:))
plot(rnd.R11R66(i,:), rnd.vol(1,:), '-', 'LineWidth',1.2, 'Color',col(2,:))
plot(tap.R11R66(i,:), tap.vol(1,:), '-', 'LineWidth',1.2, 'Color',col(3,:))
ax = gca; ax.XAxis.Exponent = 2;
xlabel('$\mathcal{R}_{11}/\mathcal{R}_{66}$', 'FontSize',14, 'Interpreter','latex')
ylabel('$V$', 'FontSize',14, 'Interpreter','latex')

% ... VAL
nexttile; box on; hold on;
for i = 1:num_calS
    plot(ell.R11R66(i,:), ell.VAL(i,:), '-', 'LineWidth',1.2, 'Color',[col(1,:),alph(i)], 'LineStyle',lsty{1});
    plot(rnd.R11R66(i,:), rnd.VAL(i,:), '-', 'LineWidth',1.2, 'Color',[col(2,:),alph(i)], 'LineStyle',lsty{1});
    plot(tap.R11R66(i,:), tap.VAL(i,:), '-', 'LineWidth',1.2, 'Color',[col(3,:),alph(i)], 'LineStyle',lsty{1});
    ax = gca; ax.XAxis.Exponent = 2;
end
xlabel('$\mathcal{R}_{11}/\mathcal{R}_{66}$', 'FontSize',14, 'Interpreter','latex')
ylabel('VAL', 'FontSize',14, 'Interpreter','latex')

% ... eta
nexttile; box on; hold on;
for i = 1:num_calS
    plot(ell.R11R66(i,:), ell.eta(i,:), '-', 'LineWidth',1.2, 'Color',[col(1,:),alph(i)], 'LineStyle',lsty{1});
    plot(rnd.R11R66(i,:), rnd.eta(i,:), '-', 'LineWidth',1.2, 'Color',[col(2,:),alph(i)], 'LineStyle',lsty{1});
    plot(tap.R11R66(i,:), tap.eta(i,:), '-', 'LineWidth',1.2, 'Color',[col(3,:),alph(i)], 'LineStyle',lsty{1});
    ax = gca; ax.XAxis.Exponent = 2;
end
xlabel('$\mathcal{R}_{11}/\mathcal{R}_{66}$', 'FontSize',14, 'Interpreter','latex')
ylabel('$\eta$', 'FontSize',14, 'Interpreter','latex')

% ... yaw
%{
nexttile; box on; hold on;
for i = 1:4
    plot(ell.rat, ell.ryaw(i,:), 'LineWidth',1.2, 'Color',col(1,:));
    plot(rnd.rat, rnd.ryaw(i,:), 'LineWidth',1.2, 'Color',col(2,:));
    plot(tap.rat, tap.ryaw(i,:), 'LineWidth',1.2, 'Color',col(3,:));
end
%}

% scale and save
set(gcf, 'Position',[555 638 730 192])
save2pdf('./head_morphology_eif/figures/scaled/R11R66.pdf')
close all

%% R11R22 against VAL, eta

% set colors, line styles 
col  = lines(3);
lsty = {'-','--','-.',':'}; 

% plot R11R22 against:
figure; tiledlayout(1,3)

% ... volume
nexttile; box on; hold on;
plot(ell.R11R22(i,:), ell.vol(1,:), 'LineWidth',1.2, 'Color',col(1,:))
plot(rnd.R11R22(i,:), rnd.vol(1,:), 'LineWidth',1.2, 'Color',col(2,:))
plot(tap.R11R22(i,:), tap.vol(1,:), 'LineWidth',1.2, 'Color',col(3,:))
ax = gca; ax.XAxis.Exponent = -1;

% ... VAL
nexttile; box on; hold on;
for i = 1:4
    plot(ell.R11R22(i,:), ell.VAL(i,:), 'LineWidth',1.2, 'Color',col(1,:), 'LineStyle',lsty{i});
    plot(rnd.R11R22(i,:), rnd.VAL(i,:), 'LineWidth',1.2, 'Color',col(2,:), 'LineStyle',lsty{i});
    plot(tap.R11R22(i,:), tap.VAL(i,:), 'LineWidth',1.2, 'Color',col(3,:), 'LineStyle',lsty{i});
    ax = gca; ax.XAxis.Exponent = -1;
end

% ... eta
nexttile; box on; hold on;
for i = 1:4
    lp{i} = plot(ell.R11R22(i,:), ell.eta(i,:), 'LineWidth',1.2, 'Color',col(1,:), 'LineStyle',lsty{i});
    plot(rnd.R11R22(i,:), rnd.eta(i,:), 'LineWidth',1.2, 'Color',col(2,:), 'LineStyle',lsty{i});
    plot(tap.R11R22(i,:), tap.eta(i,:), 'LineWidth',1.2, 'Color',col(3,:), 'LineStyle',lsty{i});
    ax = gca; ax.XAxis.Exponent = -1;
end

% legend for line style
legend([lp{1},lp{2},lp{3},lp{4}], {'$\mathcal{S}=9$','$\mathcal{S}=12$', ...
    '$\mathcal{S}=15$','$\mathcal{S}=18$'}, 'Interpreter','latex', 'FontSize',12, ...
    'EdgeColor','none')

% ... yaw
%{
nexttile; box on; hold on;
for i = 1:4
    plot(ell.rat, ell.ryaw(i,:), 'LineWidth',1.2, 'Color',col(1,:));
    plot(rnd.rat, rnd.ryaw(i,:), 'LineWidth',1.2, 'Color',col(2,:));
    plot(tap.rat, tap.ryaw(i,:), 'LineWidth',1.2, 'Color',col(3,:));
end
%}

% scale and save
set(gcf, 'Position',[552 776 568 171])
save2pdf('./head_morphology_eif/figures/scaled/R11R22.pdf')

%% relative differences for fixed volume

% for each normal volume, identify nearby volumes in tap and rnd, and
% compute relative VAL, eta, yaw differences

tol = 0.02;
for i = 1:num_calS
    for j = 1:num_V

        voli = ell.vol(i,j);

        % relative differences
        rd_rnd = abs(rnd.vol(i,:)-voli)/voli;
        rd_tap = abs(tap.vol(i,:)-voli)/voli;

        % min rel diffs
        [rd_rnd_min, id_rnd] = min(rd_rnd);
        [rd_tap_min, id_tap] = min(rd_tap);

        % if below tolerance - record volume and perc-diff
        if rd_rnd_min <= tol       
            rd_rnd_VAL(i,j) = abs(rnd.VAL(i,id_rnd)-ell.VAL(i,j))/abs(ell.VAL(i,j));
            rd_rnd_eta(i,j) = abs(rnd.eta(i,id_rnd)-ell.eta(i,j))/abs(ell.eta(i,j));
            rd_rnd_yaw(i,j) = abs(rnd.ryaw(i,id_rnd)-ell.ryaw(i,j))/abs(ell.ryaw(i,j));
            
            rd_rnd_R11(i,j) = abs(rnd.GRM{i,id_rnd}(1,1)-ell.GRM{i,j}(1,1))/abs(ell.GRM{i,j}(1,1));
            rd_rnd_R22(i,j) = abs(rnd.GRM{i,id_rnd}(2,2)-ell.GRM{i,j}(2,2))/abs(ell.GRM{i,j}(2,2));
            rd_rnd_R33(i,j) = abs(rnd.GRM{i,id_rnd}(3,3)-ell.GRM{i,j}(3,3))/abs(ell.GRM{i,j}(3,3));
            rd_rnd_R44(i,j) = abs(rnd.GRM{i,id_rnd}(4,4)-ell.GRM{i,j}(4,4))/abs(ell.GRM{i,j}(4,4));
            rd_rnd_R55(i,j) = abs(rnd.GRM{i,id_rnd}(5,5)-ell.GRM{i,j}(5,5))/abs(ell.GRM{i,j}(5,5));
            rd_rnd_R66(i,j) = abs(rnd.GRM{i,id_rnd}(6,6)-ell.GRM{i,j}(6,6))/abs(ell.GRM{i,j}(6,6));
        end
        if rd_tap_min <= tol
            rd_tap_VAL(i,j) = abs(tap.VAL(i,id_tap)-ell.VAL(i,j))/abs(ell.VAL(i,j));
            rd_tap_eta(i,j) = abs(tap.eta(i,id_tap)-ell.eta(i,j))/abs(ell.eta(i,j));
            rd_tap_yaw(i,j) = abs(tap.ryaw(i,id_tap)-ell.ryaw(i,j))/abs(ell.ryaw(i,j));
            
            rd_tap_R11(i,j) = abs(tap.GRM{i,id_rnd}(1,1)-ell.GRM{i,j}(1,1))/abs(ell.GRM{i,j}(1,1));
            rd_tap_R22(i,j) = abs(tap.GRM{i,id_rnd}(2,2)-ell.GRM{i,j}(2,2))/abs(ell.GRM{i,j}(2,2));
            rd_tap_R33(i,j) = abs(tap.GRM{i,id_rnd}(3,3)-ell.GRM{i,j}(3,3))/abs(ell.GRM{i,j}(3,3));
            rd_tap_R44(i,j) = abs(tap.GRM{i,id_rnd}(4,4)-ell.GRM{i,j}(4,4))/abs(ell.GRM{i,j}(4,4));
            rd_tap_R55(i,j) = abs(tap.GRM{i,id_rnd}(5,5)-ell.GRM{i,j}(5,5))/abs(ell.GRM{i,j}(5,5));
            rd_tap_R66(i,j) = abs(tap.GRM{i,id_rnd}(6,6)-ell.GRM{i,j}(6,6))/abs(ell.GRM{i,j}(6,6));
        end
    end
    
    % remove zero values
    id_rnd_nz = (rd_rnd_VAL(i,:)>0);
    id_tap_nz = (rd_tap_VAL(i,:)>0);
    
    % average over non zero values to get av relative difference, for
    % equivalent volume, for each calS
    av_rd_rnd_VAL(i) = mean(rd_rnd_VAL(i,id_rnd_nz));
    av_rd_rnd_eta(i) = mean(rd_rnd_eta(i,id_rnd_nz));
    av_rd_rnd_yaw(i) = mean(rd_rnd_yaw(i,id_rnd_nz));
    
    av_rd_tap_VAL(i) = mean(rd_tap_VAL(i,id_tap_nz));
    av_rd_tap_eta(i) = mean(rd_tap_eta(i,id_tap_nz));
    av_rd_tap_yaw(i) = mean(rd_tap_yaw(i,id_tap_nz));
    
    % as above, but for GRM values
    av_rd_rnd_R11(i) = mean(rd_rnd_R11(i,id_rnd_nz));
    av_rd_rnd_R22(i) = mean(rd_rnd_R22(i,id_rnd_nz));
    av_rd_rnd_R33(i) = mean(rd_rnd_R33(i,id_rnd_nz));
    av_rd_rnd_R44(i) = mean(rd_rnd_R44(i,id_rnd_nz));
    av_rd_rnd_R55(i) = mean(rd_rnd_R55(i,id_rnd_nz));
    av_rd_rnd_R66(i) = mean(rd_rnd_R66(i,id_rnd_nz));
    
    av_rd_tap_R11(i) = mean(rd_tap_R11(i,id_tap_nz));
    av_rd_tap_R22(i) = mean(rd_tap_R22(i,id_tap_nz));
    av_rd_tap_R33(i) = mean(rd_tap_R33(i,id_tap_nz));
    av_rd_tap_R44(i) = mean(rd_tap_R44(i,id_tap_nz));
    av_rd_tap_R55(i) = mean(rd_tap_R55(i,id_tap_nz));
    av_rd_tap_R66(i) = mean(rd_tap_R66(i,id_tap_nz));
    
end

%% finish

fprintf('Figures saved in /head_morphology_eif/figures/scaled/ !\n')
close all
printLinebreak

end % function
function preprocScaledVolData()

% preprocess solution data for use in plotScaledVolumeResults

printLinebreak;

% suppress filepath warning
warning off

% find calS values by examining folder structure
pre = './head_morphology_eif/data/';
dname = dir([pre,'scaled_data_*']);

% using dir will pick up any zip folders in data dir -- these need
% neglected
ii = 1;
for i = 1:length(dname)
    if ~strcmp(dname(i).name(end-3:end),'.zip')
        fname{ii} = dname(i).name;
        ii = ii+1;
    end
end
num_calS = length(fname);

% extract sperm numbers from file names
for i = 1:num_calS
    calS_vals(i) = sscanf(fname{i},'scaled_data_%d');
end

% number of volume scales considered
V_scale = 0.5:0.1:2;
num_V = length(V_scale);

%% load data

warning off         % suppress path warning from not having endpiece code in path
fprintf('Loading data... ')
mname_pre = './head_morphology_eif/data/';

% number of simulations per calS
num_scavol = length(dir([mname_pre, sprintf('scaled_data_%02g/*.mat', calS_vals(1))]));

% find file names
for i = 1:num_calS
    for j = 1:num_scavol
        dname = dir([mname_pre, sprintf('scaled_data_%02g/*.mat', calS_vals(i))]);
        mname{i,j} = dname(j).name;
    end
end

% load data
for i = 1:num_calS
    for j = 1:num_scavol
        
        % load 
        load([mname_pre, sprintf('scaled_data_%02g/',calS_vals(i)), mname{i,j}], 'varargin');
        
        % assign
        sol{i,j} = varargin{1}.sol;
        model{i,j} = varargin{2};
        
        % if not pyriform, compute volume
        if j<=3*num_V
            vol(i,j) = computeBodyVolume(model{i,j});
        end
    end
end
fprintf('complete!\n')

%% seperate based on shape

% first num_V results are ellipsoidal heads
ell.sol = sol(:,1:num_V);
ell.vol = vol(:,1:num_V);
ell.model = model(:,1:num_V);

% second num_V results are round cells
rnd.sol = sol(:,num_V+1:2*num_V);
rnd.vol = vol(:,num_V+1:2*num_V);
rnd.model = model(:,num_V+1:2*num_V);

% third num_V results are tapered cells
tap.sol = sol(:,2*num_V+1:3*num_V);
tap.vol = vol(:,2*num_V+1:3*num_V);
tap.model = model(:,2*num_V+1:3*num_V);

% final num_V results are pyriform cells
if num_scavol > 3*num_V
    pyr.sol = sol(:,3*num_V+1:4*num_V);
    pyr.model = model(:,3*num_V+1:4*num_V);
end

%% sample over established beat

% determine coordinates at fixed time values
tmax  = model{1,1}.tmax;
tint  = 20;
tps   = 0 : 2*pi/tint : tmax;
num_t = length(tps);

% time points corresponding to start of each beat
[~,beat_idx] = find(mod(tps,2*pi) < 1e-2);

% consider data over an established beat
tbeat_idx = beat_idx(3) : beat_idx(4);
tbeat = tps(tbeat_idx);
num_tbeat = length(tbeat);

% compute coordinates of head and flagellum over the beat
fprintf('Computing coordinates... ');
addpath(genpath('./endpiece_code'))
for i = 1:num_calS
    for j = 1:num_V
        
        % solution vectors at tbeat values
        ell.Z{i,j} = deval(ell.sol{i,j}, tbeat);
        rnd.Z{i,j} = deval(rnd.sol{i,j}, tbeat);
        tap.Z{i,j} = deval(tap.sol{i,j}, tbeat);
        if num_scavol > 3*num_V
            pyr.Z{i,j} = deval(pyr.sol{i,j}, tbeat);
        end
        
        % coordinate data at each tbeat value
        for k = 1:num_tbeat
            [ehead,eflag,~] = GetProblemData(ell.Z{i,j}(:,k), ell.model{i,j});
            [rhead,rflag,~] = GetProblemData(rnd.Z{i,j}(:,k), rnd.model{i,j});
            [thead,tflag,~] = GetProblemData(tap.Z{i,j}(:,k), tap.model{i,j});
            if num_scavol > 3*num_V
                [phead,pflag,~] = GetProblemData(pyr.Z{i,j}(:,k), pyr.model{i,j});
            end
            
            ell.Y{i,j,k} = ehead.XQuad; ell.X{i,j,k} = eflag.X;
            rnd.Y{i,j,k} = rhead.XQuad; rnd.X{i,j,k} = rflag.X;
            tap.Y{i,j,k} = thead.XQuad; tap.X{i,j,k} = tflag.X;
            if num_scavol > 3*num_V
                pyr.Y{i,j,k} = phead.XQuad; pyr.X{i,j,k} = pflag.X;
            end
        end
        
    end
end
rmpath(genpath('./endpiece_code')); format short
fprintf('complete!\n')

%% bundle useful measures into info structure

info = struct('num_calS',num_calS, 'num_V',num_V, 'num_t',num_t, 'tps', tbeat, ...
    'V_scale',V_scale);

%% send data to function to obtain measures

calcScaledMorphologyData(ell, 'ell', info);
calcScaledMorphologyData(rnd, 'rnd', info);
calcScaledMorphologyData(tap, 'tap', info);

fprintf('Finished!\n')
printLinebreak;

end % function
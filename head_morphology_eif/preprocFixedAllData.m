function preprocFixedAllData(compSep, compAll)

% preprocess simulation data for each calS
% COMPuteSEPerate data computes preprocessed data for each calS simulation.
% COMPuteALL compiles all simulation data into 3 files grouped by sampled
% axes.

printLinebreak

% find calS values by examining folder structure
pre = './head_morphology_eif/data/';
dname = dir([pre,'fixed_data_*']);

% using dir will pick up any zip folders in data dir -- these need
% neglected
ii = 1;
for i = 1:length(dname)
    if ~strcmp(dname(i).name(end-3:end),'.zip')
        fname{ii} = dname(i).name;
        ii = ii+1;
    end
end
num_calS = length(fname);

% extract sperm numbers from file names
for i = 1:num_calS
    calS_vals(i) = sscanf(fname{i},'fixed_data_%d');
end

%% process individual simulation data

if compSep
    for i = 1:num_calS
        preprocFixedVolData(calS_vals(i));
    end
end

%% combine simulation data
%  combine into global a1a2, a1a3, a2a3 groupings

% mat file prefixes
pre = './head_morphology_eif/preproc/';

% generate calS suffixes
for i = 1:num_calS
    suff{i} = [sprintf('_calS=%02g', str2num(fname{i}(end-1:end))),'.mat'];
end

if compAll
    strs = {'a1a2','a1a3','a2a3'};
    for i = 1:length(strs)   
        for j = 1:length(suff)

            % load data
            load_str{j} = [pre,sprintf('%s',strs{i}),suff{j}];
            if (j==1)
                Ldat = load(load_str{j},'XX','YY','ZZ','model','ax','msh','info');
                XX = Ldat.XX; YY = Ldat.YY; ZZ = Ldat.ZZ; info = Ldat.info;
            else
                Ldat = load(load_str{j},'model','ax','msh');
            end

            % assign to combined variables
            model{j} = Ldat.model;      % contains model parameters for each simulation
            ax{j}    = Ldat.ax;         % contains axes for each simulation
            msh{j}   = Ldat.msh;        % contains computed quantities (VAL,eta) for each simulation
        end

        % save as combined .mat file
        save_str = sprintf('./head_morphology_eif/preproc/%s.mat',strs{i});
        save(save_str, 'XX','YY','ZZ','model','ax','msh','info');
        fprintf('File saved at %s\n', save_str);

    end
end

%% completion

printLinebreak

end % function
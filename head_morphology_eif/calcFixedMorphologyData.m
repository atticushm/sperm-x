function calcFixedMorphologyData(X, Y, Z, model, ax, str, info)

% key bits from info
npd   = info.num_per_dir;
num_S = info.num_S;
tbeat = info.tbeat; 
num_tbeat = info.num_tbeat;

% determine meshgrid for pcolor plots
switch str
    case 'a1a2'
        xx = unique(ax(1,:));
        yy = unique(ax(2,:));
        zz = ax(3,:);
    case 'a1a3'
        xx = unique(ax(1,:));
        yy = unique(ax(3,:));
        zz = ax(2,:);
    case 'a2a3'
        xx = unique(ax(2,:));
        yy = unique(ax(3,:));
        zz = ax(1,:);
end
[XX, YY] = meshgrid(xx, yy);    % meshgrid of fixed axes values
ZZ = reshape(zz, npd, npd);     % corresponding grid of third axis for fixed volume

% reshape X and Y to match coordinates of meshgrid
for i = 1:num_S
   if num_S > 1
       msh{i}.Z = reshape(Z(i,:), npd, npd);
       msh{i}.X = reshape(X(i,:,:), npd, npd, []);
       msh{i}.Y = reshape(Y(i,:,:), npd, npd, []);
       msh{i}.model = reshape(model(i,:), npd, npd);
   else
       msh{i}.Z = reshape(Z(i,:), npd, npd);
       msh{i}.X = reshape(X(:,:), npd, npd, []);
       msh{i}.Y = reshape(Y(:,:), npd, npd, []);
       msh{i}.model = reshape(model(i,:), npd, npd);
   end
end

% compute quantities
fprintf('Computing %s info...', str)
for i = 1:num_S
    for j = 1:npd
        for k = 1:npd
            % compute VAL
            msh{i}.VAL(j,k) = calculateVAL(msh{i}.Z{j,k}, info.tbeat, 'endpiece');
            
            % compute average work
            addpath(genpath('./endpiece_code'))
            for l = 1:num_tbeat
                Zdat    = msh{i}.Z{j,k};
                work(l) = calculateWork(tbeat(l), Zdat(:,l), msh{i}.model{j,k});
            end
            msh{i}.W_av(j,k) = mean(work);
            rmpath(genpath('./endpiece_code'))
            
            % compute lighthill efficiency
            msh{i}.eta(j,k) = (msh{i}.VAL(j,k))^2/msh{i}.W_av(j,k);
            
            % compute mean absolute curvature
            for l = 1:num_tbeat
                Zdat = msh{i}.Z{j,k};
                kap(:,l) = abs(calcCurvatureEPC(Zdat(:,l)));
            end
            msh{i}.mkap{j,k} = mean(kap,2);
            
            % input data is not at t=0 -> generate reference head from axis
            % values. use odd numbers for projection to ensure values in
            % the 0-axes.
            msh{i}.ax{j,k} = [msh{i}.model{j,k}.swimmer.a1; ...
                              msh{i}.model{j,k}.swimmer.a2; ...
                              msh{i}.model{j,k}.swimmer.a3];                        
            [Yt,Yq,X0,~] = generateBodyNodes(7,19,'human','custom',msh{i}.ax{j,k});
            
            % save head quadrature discretisation at t=0
            msh{i}.Yq{j,k} = Yq;
            
            % solve resistance problem to compute GRM for head at t=0
            msh{i}.GRM{j,k} = solveResistanceProblem(Yt, Yq, X0, 1e-2, 1, 'stokeslets');
            
            % GRM ratio measures
            msh{i}.R11R66(j,k) = msh{i}.GRM{j,k}(1,1)/msh{i}.GRM{j,k}(6,6);
            msh{i}.R11R22(j,k) = msh{i}.GRM{j,k}(1,1)/msh{i}.GRM{j,k}(2,2);
            
            % eccentricity of tangent plane
            ax1 = msh{i}.model{j,k}.swimmer.a1;
            ax2 = msh{i}.model{j,k}.swimmer.a2;
            ax3 = msh{i}.model{j,k}.swimmer.a3;
            msh{i}.epara(j,k) = ax1/ax2; %max(ax1/ax2, ax2/ax1); ax1 should always be largest
            msh{i}.eperp(j,k) = ax2/ax3; %max(ax2/ax3, ax3/ax2); ax2 should always be largest
            
            % range of yaw of head equiv. to relative angular envelope of
                % flagellum
            msh{i}.ryaw(j,k) = max(msh{i}.Z{j,k}(3,:)) - min(msh{i}.Z{j,k}(3,:));
            
            % abs diff of tan(head_angle) 
            msh{i}.tyaw(j,k) = abs(tan(max(msh{i}.Z{j,k}(3,:))) - tan(min(msh{i}.Z{j,k}(3,:))));
        end
    end
end
fprintf(' complete!\n')

% remove large fields from msh
for i = 1:num_S
    msh{i} = rmfield(msh{i},{'X','Y'});
end

% if num_S=1, collapse
if (i==1)
    msh = [msh{:}];
end

% save
if num_S > 1
    savestr = sprintf('./head_morphology_eif/preproc/%s.mat',str);
else
    calS = model{1}.swimmer.calS;
    savestr = sprintf('./head_morphology_eif/preproc/%s_calS=%02g.mat',str,calS);
end
save(savestr, 'info','XX','YY','ZZ','model','ax','msh');
fprintf('Preprocessed %s data saved at %s\n',str,savestr);
    
end % function
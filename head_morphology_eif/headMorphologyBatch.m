function headMorphologyBatch(method)

% batch runs simulations of planarly beating cell (using Neal code) with
% different head morphologies

% check in correct directory (bluebear call will start in subdir, so we 
% need to move up)
dir = pwd;
if strcmpi(dir(end-18:end),'head_morphology_eif')
    cd ..
end
addFilesToPath;
printLinebreak

% add end piece code to path
addpath(genpath('./endpiece_code'));

% folder for workspaces
if ~exist('./head_morphology_eif/data','dir')
    mkdir('./head_morphology_eif/data')
end

%% setup parameters

% define parameters fixed in each simulation
k   = 4*pi;
T   = 2*pi;
ell = 0.95;
mu  = 1;
L   = 1;
eps = 0.01;

num_beats   = 5;
tmax        = num_beats * T;
showProg    = 0;

Q       = 40;
H_trac  = 5;
H_quad  = 10;

% define calS, with calM to optimise  
calS_vals = [9, 12, 13, 14, 15, 16, 17, 18];
calM_vals = [0.0366, 0.0198, 0.018, 0.0169, 0.0162, 0.0158, 0.0155, 0.0153];

% set initial condition
X_int = parabolicCurve(1e-5, Q+1);
Z_int = convertInitialCondition(X_int, 'endpiece');

%% generate heads

switch method
    case 'fixed'
        % generate heads from fixed volume
        num_per_dir       = 10;
        num_per_case      = num_per_dir^2;
        num_case          = 3;
        num_heads         = num_case * num_per_case;
        [y, X0, ax, vol]  = generateEllipsoidalHeads(num_heads, H_trac);
        [Y,~,~,~]         = generateEllipsoidalHeads(num_heads, H_quad);

    case 'scaled'
        % generate heads from scaling volume (includes abnormal heads)
        genPyri = false;
        [y, X0, ax, vol, ~] = generateScaledHeads(H_trac, genPyri);
        [Y,~,~,~]           = generateScaledHeads(H_quad, genPyri);
        
        num_heads    = size(ax,2);     
        num_case  	 = 4;
        num_per_case = num_heads/num_case;
        
    otherwise
        warning('head generation type not specified!')
        return
end

%% run simulations

for s = 1:length(calS_vals)
    
    % choosing swimming/actuation parameters
    calS = calS_vals(s);
    calM = calM_vals(s);
    fprintf('calS = %g, calM = %g\n', calS, calM);
    
    parfor n = 1:num_heads
        
        fprintf('  head #%g\n',n);
        hax = ax{n};
        
        % build model structure
        model = CreateModelStructure(calS,k,calM,Q,H_trac,H_quad,tmax,'varying',ell, ...
            [],L,eps,mu,Z_int,hax);
        model.disc.quad = H_quad;
        model.disc.trac = H_trac;
        
        % assign head
        model.y   = y{n};
        model.Y   = Y{n};
        model.X0  = X0{n};
        model.vol = vol{n};
        
        % solve problem
        out = SolveSwimmingProblem(tmax, Z_int, model, showProg);
        
        % flag solution to enable easy identification later on
        if (n <= num_per_case)
            flag = {'a',hax};
        elseif (n > num_per_case ) && (n <= 2*num_per_case)
            flag = {'b',hax};
        elseif (n > 2*num_per_case) && (n <= 3*num_per_case)
            flag = {'c',hax};
        else
            flag = {'d',hax};
        end
        
        % save
        str = sprintf('./head_morphology_eif/data/%s_%02g_%03g.mat',method, calS, n);
        parsave(str, out, model, flag)
        
    end % head loop
    printLinebreak
    
    % zip up and then delete loose files
    switch method
        case 'fixed'
            cd head_morphology_eif/data
            zip_str = sprintf('%s_data_%02g',method,calS);
            zip(zip_str, './fixed_*.mat');
            cd ../../
            delete head_morphology_eif/data/fixed_*.mat
        case 'scaled' 
            cd head_morphology_eif/data
            zip_str = sprintf('%s_data_%02g',method,calS);
            zip(zip_str, './scaled_*.mat');
            cd ../../
            delete head_morphology_eif/data/scaled_*.mat
    end
    
end % calS loop
printLinebreak('#')

% remove endpiece code from path
addFilesToPath;

end % function
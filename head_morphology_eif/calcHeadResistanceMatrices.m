function calcHeadResistanceMatrices()

close all;
printLinebreak

% load preprocessed data
path = './head_morphology_eif/data/preproc_fixed_vol.mat';
if ~exist(path)
    preprocFixedVolResults();
else
    fprintf('Loading preprocessed data...');
    load(path);
    fprintf(' complete!\n');
end

% make figure dir
if ~exist('./head_morphology_eif/figures','dir')
    mkdir('./head_morphology_eif/figures')
end

% regularisation parameter
epsilon = 1e-2;

% resistance matrix for a unit sphere (stokes law)
a = 1;
R_sph = blkdiag(kron(-6*pi*a, eye(3)), kron(-8*pi*a^3, eye(3)));

% compute resistance matrices
for i = 1:size(a_ax,2)
    % extract head coordinates
    xa = [a_Y{1,i,1}.xTrac; a_Y{1,i,1}.yTrac; a_Y{1,i,1}.zTrac];  
    Xa = [a_Y{1,i,1}.xQuad; a_Y{1,i,1}.yQuad; a_Y{1,i,1}.zQuad];  
    xb = [b_Y{1,i,1}.xTrac; b_Y{1,i,1}.yTrac; b_Y{1,i,1}.zTrac];  
    Xb = [b_Y{1,i,1}.xQuad; b_Y{1,i,1}.yQuad; b_Y{1,i,1}.zQuad];  
    xc = [c_Y{1,i,1}.xTrac; c_Y{1,i,1}.yTrac; c_Y{1,i,1}.zTrac];  
    Xc = [c_Y{1,i,1}.xQuad; c_Y{1,i,1}.yQuad; c_Y{1,i,1}.zQuad];  
    
    % extract head/flagellum pivots (X0)
    ra = a_mod{1,i}.X0;
    rb = b_mod{1,i}.X0;
    rc = c_mod{1,i}.X0;
    
    % compute resistance matrices
    Ra{i} = solveResistanceProblem(xa, Xa, ra, epsilon, 1, 'stokeslets');
    Rb{i} = solveResistanceProblem(xb, Xb, rb, epsilon, 1, 'stokeslets');
    Rc{i} = solveResistanceProblem(xc, Xc, rc, epsilon, 1, 'stokeslets');
    
    % procrustes score between each head and stokes law
    proc_a(i,1) = procrustes(Ra{i}, R_sph);
    proc_b(i,1) = procrustes(Rb{i}, R_sph);
    proc_c(i,1) = procrustes(Rc{i}, R_sph);
    
    % extract force-translation components
    R11a(i,1) = Ra{i}(1,1);
    R11b(i,1) = Rb{i}(1,1);
    R11c(i,1) = Rc{i}(1,1);
    R22a(i,1) = Ra{i}(2,2);
    R22b(i,1) = Rb{i}(2,2);
    R22c(i,1) = Rc{i}(2,2);
    R33a(i,1) = Ra{i}(3,3);
    R33b(i,1) = Rb{i}(3,3);
    R33c(i,1) = Rc{i}(3,3);
    
    % extract moment-rotation components
    R44a(i,1) = Ra{i}(4,4);
    R44b(i,1) = Rb{i}(4,4);
    R44c(i,1) = Rc{i}(4,4);
    R55a(i,1) = Ra{i}(5,5);
    R55b(i,1) = Rb{i}(5,5);
    R55c(i,1) = Rc{i}(5,5);
    R66a(i,1) = Ra{i}(6,6);
    R66b(i,1) = Rb{i}(6,6);
    R66c(i,1) = Rc{i}(6,6);
end

end % function
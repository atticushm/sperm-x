function preprocFixedVolData(calS)

% proprocess solution data for use in plotFixedVolumeResults() for a given
% calS value. Previous code loaded all solution data, for every calS, which
% was slow and prone to memory overflow -- this will avoid that.

printLinebreak;

% suppress filepath warning
warning off

% number of fixed volume heads considered
num_fixvol = 300;
num_per_dir = 10;

% find file names
mname_pre = sprintf('./head_morphology_eif/data/fixed_data_%02g/', calS);
for i = 1:num_fixvol
    dname = dir([mname_pre, 'fixed_*.mat']);
    fname{i} = [mname_pre, dname(i).name];
end

% load data
fprintf('Loading calS=%g data... ',calS);
for i = 1:num_fixvol
    % load
    load(fname{i}, 'varargin');
    
    % assign
    sol{i} = varargin{1}.sol;
    model{i} = varargin{2};
    head_ax{i} = varargin{3}{2};
    clearvars varargin
end
fprintf('complete!\n')

%% compute basic quantities

% determine coordinates at fixed time values
tmax  = model{1}.tmax;
tint  = 20;
tps   = 0 : 2*pi/tint : tmax;
num_t = length(tps);

% time points corresponding to start of each beat
[~,beat_idx] = find(mod(tps,2*pi) < 1e-2);

% consider data over an established beat
tbeat_idx = beat_idx(3) : beat_idx(4);
tbeat = tps(tbeat_idx);
num_tbeat = length(tbeat);

% mean flagellum+midpiece length from Cummins 1985
L = meanFlagellumLength;

% compute coordinates over a single established beat
fprintf('Computing coordinates over beat... ')
addpath(genpath('./endpiece_code'))
for i = 1:num_fixvol
    Z_data{i} = deval(sol{i}, tbeat);
    for k = 1:num_tbeat
        [Y_data{i,k}, X_data{i,k},~] = GetProblemData(Z_data{i}(:,k), model{i});
    end
end
rmpath(genpath('./endpiece_code'))
fprintf('complete!\n')

% seperate data into head varying type 
num_case = num_fixvol/3;

% make directory
if ~exist('head_morphology_eif/preproc','dir')
    mkdir('head_morphology_eif/preproc/')
end

%% build info structure

num_S = 1;
info = struct('num_S', num_S, 'num_fixvol', num_fixvol, 'num_case', num_case, ...
              'tmax', tmax, 'tps', tps, 'tbeat_idx', tbeat_idx, 'tbeat', tbeat, ...
              'num_tbeat', num_tbeat, 'num_t', num_t, 'num_per_dir', num_per_dir, ...
              'L', L, 'calS_vals', calS, 'beat_idx', beat_idx ...
              );
          
%% (a) fix a1 & a2

% isolate values
% each column has data corresponding to a cell with head axes given in the
%   same column of ax.
% each row corresponds to a cell with a different calS value
X   = X_data(1:num_case,:);
Y   = Y_data(1:num_case,:);
Z   = Z_data(1:num_case);
mdl = model(1:num_case);
ax  = [head_ax{1,1:num_case}]; 

% compute and save preprocessed data
str = 'a1a2';
calcFixedMorphologyData(X, Y, Z, mdl, ax, str, info);

%% (b) fix a1 & a3

% isolate values
% each column has data corresponding to a cell with head axes given in the
%   same column of ax.
% each row corresponds to a cell with a different calS value
X   = X_data(num_case+1:2*num_case,:);
Y   = Y_data(num_case+1:2*num_case,:);
Z   = Z_data(num_case+1:2*num_case);
mdl = model(num_case+1:2*num_case);
ax  = [head_ax{1,num_case+1:2*num_case}]; 

% compute and save preprocessed data
str = 'a1a3';
calcFixedMorphologyData(X, Y, Z, mdl, ax, str, info);

%% (c) fix a2 & a3

% isolate values
% each column has data corresponding to a cell with head axes given in the
%   same column of ax.
% each row corresponds to a cell with a different calS value
X   = X_data(2*num_case+1:3*num_case,:);
Y   = Y_data(2*num_case+1:3*num_case,:);
Z   = Z_data(2*num_case+1:3*num_case);
mdl = model(2*num_case+1:3*num_case);
ax  = [head_ax{1,2*num_case+1:3*num_case}]; 

% compute and save preprocessed data
str = 'a2a3';
calcFixedMorphologyData(X, Y, Z, mdl, ax, str, info);

%% finish

% reenable warnings
warning on

printLinebreak

end % function